package tv.ustream.viewer.document.js
{
   import flash.events.EventDispatcher;
   import tv.ustream.viewer.document.ui.SeekBar;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.Shell;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   
   public class HighlightInterface extends EventDispatcher
   {
      
      public function HighlightInterface(param1:SeekBar) {
         super();
         if(param1)
         {
            this.slider = param1;
            Shell.instance.addCallBack("showHighlightUI",this.onShowHighlightUi);
            Shell.instance.addCallBack("hideHighlightUI",this.onHideHighlightUi);
            Shell.instance.addCallBack("startPointSeek",this.onStartPointSeek);
            Shell.instance.addCallBack("endPointSeek",this.onEndPointSeek);
            Shell.instance.addCallBack("setIntervalMinimum",this.onSetIntervalMinimum);
            Shell.instance.addCallBack("startPreview",this.onStartPreview);
            Shell.instance.addCallBack("stopPreview",this.onStopPreview);
            param1.intervalMinimumSeconds = this.MIN_INTERVAL;
         }
      }
      
      private const MIN_INTERVAL:Number = 10;
      
      private var _slider:SeekBar;
      
      private function onSetIntervalMinimum(param1:Number) : void {
         if(this._slider)
         {
            this._slider.intervalMinimumSeconds = param1;
         }
      }
      
      private function onSliderChange(param1:DynamicEvent) : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         this.echo("onSliderChange");
         if(!(param1.start == undefined) || !(param1.end == undefined))
         {
            _loc2_ = 0;
            _loc3_ = 1;
            if(This.getReference(Logic.instance,"media.modules.meta.data.cut"))
            {
               _loc2_ = Logic.instance.media.modules.meta.data.cut[0];
               _loc3_ = Logic.instance.media.modules.meta.data.cut[1];
            }
         }
         if(param1.start != undefined)
         {
            param1.start = _loc2_ + (_loc3_ - _loc2_) * param1.start;
            Shell.instance.call("onStartPointSeek",param1.start);
            this.echo("cutStart: " + _loc2_ + ", cutEnd: " + _loc3_ + ", start: " + param1.start);
         }
         if(param1.end != undefined)
         {
            param1.end = _loc2_ + (_loc3_ - _loc2_) * param1.end;
            Shell.instance.call("onEndPointSeek",param1.end);
            this.echo("cutStart: " + _loc2_ + ", cutEnd: " + _loc3_ + ", end: " + param1.end);
         }
      }
      
      private function onShowHighlightUi() : void {
         this.echo("onShowHighlightUi");
         if(this._slider)
         {
            this._slider.intervalEditable = true;
            this._slider.intervalLock = false;
            dispatchEvent(new Event(Event.CHANGE));
         }
      }
      
      private function onHideHighlightUi() : void {
         this.echo("onHideHighlightUi");
         if(this._slider)
         {
            this._slider.intervalStart = 0;
            this._slider.intervalEnd = 1;
            this._slider.intervalEditable = false;
            this._slider.intervalLock = true;
            dispatchEvent(new Event(Event.CHANGE));
         }
      }
      
      private function onStartPointSeek(param1:Number) : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         var _loc4_:* = NaN;
         this.echo("onStartPointSeek");
         if(this._slider)
         {
            _loc2_ = 0;
            _loc3_ = 1;
            _loc4_ = param1;
            if(This.getReference(Logic.instance,"media.modules.meta.data.cut"))
            {
               _loc2_ = Logic.instance.media.modules.meta.data.cut[0];
               _loc3_ = Logic.instance.media.modules.meta.data.cut[1];
            }
            _loc4_ = Math.max(0,(_loc4_ - _loc2_) / (_loc3_ - _loc2_));
            this._slider.intervalStart = _loc4_;
         }
      }
      
      private function onEndPointSeek(param1:Number) : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         var _loc4_:* = NaN;
         this.echo("onEndPointSeek");
         if(this._slider)
         {
            _loc2_ = 0;
            _loc3_ = 1;
            _loc4_ = param1;
            if(This.getReference(Logic.instance,"media.modules.meta.data.cut"))
            {
               _loc2_ = Logic.instance.media.modules.meta.data.cut[0];
               _loc3_ = Logic.instance.media.modules.meta.data.cut[1];
            }
            _loc4_ = Math.max(0,(_loc4_ - _loc2_) / (_loc3_ - _loc2_));
            this._slider.intervalEnd = _loc4_;
         }
      }
      
      private function onStartPreview() : void {
         this.echo("onStartPreview");
         if(this._slider)
         {
            Logic.instance.media.seek(this._slider.intervalStart);
            Logic.instance.media.play();
         }
      }
      
      private function onStopPreview() : void {
         this.echo("onStopPreview");
         Logic.instance.media.playing = false;
      }
      
      public function get slider() : SeekBar {
         return this._slider;
      }
      
      public function set slider(param1:SeekBar) : void {
         if(this._slider)
         {
            this._slider.removeEventListener(Event.CHANGE,this.onSliderChange);
         }
         this._slider = param1;
         if(this._slider)
         {
            this._slider.addEventListener(Event.CHANGE,this.onSliderChange);
         }
      }
      
      private function echo(param1:String = "") : void {
         Debug.echo("[ HighlightInterface ] " + param1);
      }
   }
}
