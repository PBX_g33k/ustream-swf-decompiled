package tv.ustream.viewer.document.tools
{
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   
   public class TimeOut extends Object
   {
      
      public function TimeOut(param1:Number, param2:Function, param3:String = null, param4:Boolean = true, param5:Boolean = true) {
         super();
         if((param3) && (_timeOuts[param3]))
         {
            if(param5)
            {
               throw new Error("TimeOut " + param3 + " already exists!");
            }
            else
            {
               this.reset();
               delete _timeOuts[param3];
            }
         }
         _timeOutCounter++;
         if(param3 == null)
         {
            param3 = "timeout" + _timeOutCounter;
         }
         this._callback = param2;
         this._time = param1;
         this._name = param3;
         _timeOuts[param3] = this;
         if(param4)
         {
            this.start();
         }
      }
      
      private static var _timeOuts:Object = {};
      
      private static var _timeOutCounter:uint = 0;
      
      public static function create(param1:Number, param2:Function, param3:String = null) : TimeOut {
         if(!param3 || !_timeOuts[param3])
         {
         }
         return _timeOuts[param3];
      }
      
      public static function retrieve(param1:String) : TimeOut {
         return _timeOuts[param1];
      }
      
      private var _name:String;
      
      private var _time:Number;
      
      private var _callback:Function;
      
      private var _timer:Timer;
      
      private var _startTime:Number;
      
      private var _pauseTime:Number = 0;
      
      private var _totalTime:Number = 0;
      
      private function createTimer(param1:Number) : void {
         this.deleteTimer();
         this._timer = new Timer(param1,1);
         this._timer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onTimer);
      }
      
      private function deleteTimer() : void {
         if(this._timer)
         {
            this._timer.stop();
            this._timer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onTimer);
            this._timer = null;
         }
      }
      
      public function start() : void {
         if(this.started)
         {
            return;
         }
         if(this._timer)
         {
            this._totalTime = this._totalTime + (this._pauseTime - this._startTime);
            this.createTimer(this._time - this._totalTime);
         }
         else
         {
            this.createTimer(this._time);
            this._totalTime = 0;
         }
         this._startTime = new Date().getTime();
         this._timer.start();
      }
      
      public function reset() : void {
         this.deleteTimer();
         this._pauseTime = 0;
         this._totalTime = 0;
      }
      
      private function onTimer(param1:TimerEvent) : void {
         this.reset();
         if(this._callback != null)
         {
            this._callback.apply();
         }
      }
      
      public function get name() : String {
         return this._name;
      }
      
      public function get time() : Number {
         return this._time;
      }
      
      public function get progress() : Number {
         return (this._totalTime + (new Date().getTime() - this._startTime)) / this._time;
      }
      
      public function get started() : Boolean {
         return this._timer?this._timer.running:false;
      }
   }
}
