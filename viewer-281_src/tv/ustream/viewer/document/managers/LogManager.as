package tv.ustream.viewer.document.managers
{
   import flash.events.EventDispatcher;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.viewer.document.ui.panes.LogPane;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.debug.Debug;
   import tv.ustream.viewer.logic.media.Recorded;
   
   public class LogManager extends Object
   {
      
      public function LogManager(param1:EventDispatcher, param2:ViewerUI) {
         super();
         Debug.info("Logmanager","created - dispatcher: " + param1);
         this._viewerui = param2;
         this._dispatcher = param1;
         this._dispatcher.addEventListener(Recorded.LOG_SENDING_STARTED,this.onSend);
         this._dispatcher.addEventListener(Recorded.LOG_SENDING_COMPLETE,this.onComplete);
         this._dispatcher.addEventListener(Recorded.LOG_SENDING_FAILED,this.onFailed);
      }
      
      private var _dispatcher:EventDispatcher;
      
      private var _viewerui:ViewerUI;
      
      private var _logPane:LogPane;
      
      private var _nextPane:LogPane;
      
      private function onSend(param1:Event) : void {
         this.createPane(new LogPane(LogPane.STATE_SENDING));
      }
      
      private function onPaneClosed(param1:Event) : void {
         this._logPane.removeEventListener(Event.CLOSE,this.onPaneClosed);
         this._viewerui.removeLogPane(this._logPane);
         this._logPane = null;
         if(this._nextPane)
         {
            this.createPane(this._nextPane);
         }
      }
      
      private function onComplete(param1:DynamicEvent) : void {
         this.createPane(new LogPane(LogPane.STATE_SENT,param1.hash));
      }
      
      private function onFailed(param1:Event) : void {
         this.createPane(new LogPane(LogPane.STATE_FAILED));
      }
      
      private function createPane(param1:LogPane = null) : void {
         if(param1)
         {
            param1.addEventListener(Event.CLOSE,this.onPaneClosed);
         }
         if(this._logPane)
         {
            if(this._nextPane)
            {
               this._nextPane.removeEventListener(Event.CLOSE,this.onPaneClosed);
               this._nextPane = null;
            }
            this._nextPane = param1;
            this._logPane.close();
         }
         else
         {
            this._nextPane = null;
            this._logPane = param1;
            this._viewerui.createLogPane(this._logPane);
            this._logPane.open();
         }
      }
      
      public function destroy() : void {
         Debug.info("Logmanager","destroy");
         if(this._logPane)
         {
            this._logPane.removeEventListener(Event.CLOSE,this.onPaneClosed);
            this._viewerui.removeLogPane(this._logPane);
            this._logPane = null;
         }
         if(this._nextPane)
         {
            this._nextPane.removeEventListener(Event.CLOSE,this.onPaneClosed);
            this._nextPane = null;
         }
         this._viewerui = null;
         this._dispatcher.removeEventListener(Recorded.LOG_SENDING_STARTED,this.onSend);
         this._dispatcher.removeEventListener(Recorded.LOG_SENDING_COMPLETE,this.onComplete);
         this._dispatcher.removeEventListener(Recorded.LOG_SENDING_FAILED,this.onFailed);
         this._dispatcher = null;
      }
   }
}
