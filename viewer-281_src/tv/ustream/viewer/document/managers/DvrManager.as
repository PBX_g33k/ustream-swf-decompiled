package tv.ustream.viewer.document.managers
{
   import tv.ustream.viewer.logic.modules.Dvr;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tools.Shared;
   import tv.ustream.viewer.document.ui.DvrNotification;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.This;
   import tv.ustream.tools.Debug;
   
   public class DvrManager extends Object
   {
      
      public function DvrManager(param1:Dvr, param2:ViewerUI, param3:Shared) {
         super();
         this.shared = param3;
         this.viewerUI = param2;
         this._dvr = param1;
         this.create();
      }
      
      private var _dvr:Dvr;
      
      private var startStatusIsOnline:Boolean;
      
      private var _active:Boolean = true;
      
      private var viewerUI:ViewerUI;
      
      private var shared:Shared;
      
      private var _registered:Boolean = false;
      
      private function create() : void {
         this.dvr.addEventListener(Dvr.DVR_UPDATE,this.onUpdate);
         if(this.dvr.streamOnline)
         {
            this.startStatusIsOnline = this.hasLiveContent;
            this.register();
         }
      }
      
      private function register() : void {
         if(this._registered)
         {
            return;
         }
         this._registered = true;
         this.viewerUI.addEventListener(ViewerUI.SEEK_STOP_DRAG,this.onSeekStopDrag);
         this.viewerUI.addEventListener(ViewerUI.SEEK_SET,this.onDvrSeekSet);
         this.viewerUI.addEventListener(DvrNotification.DVR_PLAY,this.onPlayDvr);
         this.viewerUI.addEventListener(ViewerUI.JUMP_TO_LIVE,this.onJumpToLive);
         Logic.instance.media.addEventListener("adsPrerollDone",this.onPrerollDone);
         Logic.instance.media.addEventListener("adsVideoAdStarted",this.onAdsVideoAdStarted);
         Logic.instance.media.addEventListener("adsVideoAdFinished",this.onAdsVideoAdFinished);
         this.viewerUI.createDvr(this._dvr.contentLength,this._dvr.maximumContentLength,this._dvr.timeShift,this.startStatusIsOnline);
      }
      
      private function showNotification() : void {
         if((this.shared.dvrCIDs as Array).indexOf(Logic.instance.channel.id) >= 0)
         {
            if(!this.hasLiveContent && Math.round(this._dvr.contentLength / 60) > 0)
            {
               this.viewerUI.displayDvrNotification(this.dvr.contentLength);
            }
            else
            {
               this.viewerUI.removeDvrNotification();
            }
         }
         else
         {
            (this.shared.dvrCIDs as Array).push(Logic.instance.channel.id);
         }
         if((this.shared.dvrCIDs as Array).length > 20)
         {
            (this.shared.dvrCIDs as Array).shift();
         }
      }
      
      private function onAdsVideoAdFinished(param1:Event) : void {
         this.active = true;
      }
      
      private function onAdsVideoAdStarted(param1:Event) : void {
         this.active = false;
      }
      
      private function onJumpToLive(param1:Event) : void {
         this.seek(1);
      }
      
      private function onPrerollDone(param1:Event) : void {
         if(this.dvr.streamOnline)
         {
            this.seek(1);
         }
      }
      
      private function onUpdate(param1:DynamicEvent) : void {
         if((Logic.instance.recorded) && (!this.dvr.streamOnline) || !this._active)
         {
            return;
         }
         this.register();
         if((this.startStatusIsOnline) || (this.hasLiveContent) || !this.hasLiveContent && this.dvr.timeShift > 0)
         {
            this.viewerUI.updateDvr(this._dvr.contentLength,this._dvr.maximumContentLength,this._dvr.timeShift,this.hasLiveContent);
            if(!this.startStatusIsOnline && (this.hasLiveContent))
            {
               this.startStatusIsOnline = true;
            }
         }
         else
         {
            this.viewerUI.updateDvr(0,0,0,false);
         }
         if(!this.hasLiveContent && this.dvr.timeShift == 0)
         {
            this.showNotification();
         }
      }
      
      private function onSeekStopDrag(param1:Event) : void {
         this.seek(this.viewerUI.progress);
      }
      
      private function onDvrSeekSet(param1:DynamicEvent = null) : void {
         var _loc2_:* = NaN;
         if(Logic.instance.recorded)
         {
            Logic.instance.recorded.seek(this.viewerUI.progress,false);
         }
         else if(param1)
         {
            _loc2_ = param1.seekTo;
            if(Logic.instance.media.playing)
            {
               this.viewerUI.showLoading = true;
            }
            this.seek(_loc2_);
            param1.stopPropagation();
            param1.stopImmediatePropagation();
         }
         
      }
      
      private function onPlayDvr(param1:Event) : void {
         this.log("onPlayDvr [seek(0)]");
         this.seek(0);
      }
      
      public function get hasLiveContent() : Boolean {
         if(This.getReference(Logic.instance,"media.modules.stream"))
         {
            return Logic.instance.media.modules.stream.hasLiveContent;
         }
         return false;
      }
      
      public function seek(param1:Number) : void {
         this.log("seek = " + param1);
         if(Logic.instance.channel)
         {
            if(!Logic.instance.channel.playing && param1 == 1)
            {
               Logic.instance.channel.togglePlaying();
            }
            Logic.instance.channel.seek(param1);
         }
      }
      
      public function destroy() : void {
         this._dvr.removeEventListener(Dvr.DVR_UPDATE,this.onUpdate);
         this.viewerUI.removeEventListener(ViewerUI.SEEK_STOP_DRAG,this.onSeekStopDrag);
         this.viewerUI.removeEventListener(ViewerUI.SEEK_SET,this.onDvrSeekSet);
         this.viewerUI.removeEventListener(DvrNotification.DVR_PLAY,this.onPlayDvr);
         this.viewerUI.removeEventListener(ViewerUI.JUMP_TO_LIVE,this.onJumpToLive);
         Logic.instance.media.removeEventListener("adsPrerollDone",this.onPrerollDone);
         Logic.instance.media.removeEventListener("adsVideoAdStarted",this.onAdsVideoAdStarted);
         Logic.instance.media.removeEventListener("adsVideoAdFinished",this.onAdsVideoAdFinished);
         this.viewerUI.removeDvrNotification();
         this.viewerUI.destroyDvr();
      }
      
      public function get dvr() : Dvr {
         return this._dvr;
      }
      
      public function get active() : Boolean {
         return this._active;
      }
      
      public function set active(param1:Boolean) : void {
         this._active = param1;
      }
      
      private function log(param1:String) : void {
         Debug.echo("4:[ DVRMANAGER ] " + param1);
      }
   }
}
