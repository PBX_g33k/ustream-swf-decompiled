package tv.ustream.viewer.document
{
   import flash.text.TextField;
   import flash.filters.DropShadowFilter;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import flash.geom.Rectangle;
   import flash.system.ApplicationDomain;
   import flash.utils.getDefinitionByName;
   import flash.text.TextFormat;
   
   public class ViewerLoader extends RslLoader
   {
      
      public function ViewerLoader() {
         super("../../../api/bin/");
      }
      
      public static function get embedded() : Boolean {
         return origin == 0 || origin == SITE_TYPE_EMBED;
      }
      
      private var _width:Number;
      
      private var _height:Number;
      
      protected var debug:TextField;
      
      override protected function onAddedToStage(... rest) : void {
         addChild(this.debug = new TextField());
         this.debug.width = stage.stageWidth;
         this.debug.wordWrap = true;
         this.debug.filters = [new DropShadowFilter()];
         this.debug.visible = false;
         stage.addEventListener(Event.RESIZE,this.resize);
         stage.addEventListener(KeyboardEvent.KEY_DOWN,this.onKeyDown);
         stage.focus = stage;
         super.onAddedToStage();
      }
      
      override public function get width() : Number {
         return this._width;
      }
      
      override public function set width(param1:Number) : void {
         this._width = param1;
         this.resize(this._width,this._height);
      }
      
      override public function get height() : Number {
         return this._height;
      }
      
      override public function set height(param1:Number) : void {
         this._height = param1;
         this.resize(this._width,this._height);
      }
      
      public function resize(... rest) : void {
         if(!rest.length && ((isNaN(this._width)) || (isNaN(this._height))) || rest[0] is Event)
         {
            this._width = stage.stageWidth;
            this._height = stage.stageHeight;
         }
         else
         {
            if(!isNaN(rest[0]))
            {
               this._width = rest[0];
            }
            if(!isNaN(rest[1]))
            {
               this._height = rest[1];
            }
         }
         if(!isNaN(rest[0]) && !isNaN(rest[1]))
         {
            stage.removeEventListener(Event.RESIZE,this.resize);
            scrollRect = new Rectangle(0,0,this._width,this._height);
         }
         if(document)
         {
            document.resize(this._width,this._height);
         }
      }
      
      override protected function onRslLoaded(... rest) : void {
         var _loc2_:Class = null;
         super.onRslLoaded.apply(this,rest);
         dispatchEvent(new Event("rslLoaded",true,false));
         gotoAndStop("start");
         if(ApplicationDomain.currentDomain.hasDefinition("tv.ustream.viewer.document::Viewer"))
         {
            this.echo("ok");
            _loc2_ = getDefinitionByName("tv.ustream.viewer.document::Viewer") as Class;
            addChild(document = new _loc2_());
            this.resize();
            if(contains(this.debug))
            {
               removeChild(this.debug);
            }
            stage.removeEventListener(KeyboardEvent.KEY_DOWN,this.onKeyDown);
         }
         else
         {
            this.echo("!hasDefinition");
         }
      }
      
      override protected function echo(... rest) : void {
         this.debug.appendText(rest.join(", ") + "\n");
         this.debug.setTextFormat(new TextFormat("arial",11,16777215));
         this.debug.height = this.debug.textHeight + 5;
      }
      
      private function onKeyDown(param1:KeyboardEvent) : void {
         if((param1.ctrlKey) && param1.keyCode == 83)
         {
            this.debug.visible = !this.debug.visible;
         }
      }
      
      override protected function get rslUrl() : String {
         var _loc1_:String = null;
         var _loc2_:String = null;
         if((loaderInfo.parameters.vrsl) && origin == SITE_TYPE_STAGE)
         {
            _loc1_ = loaderInfo.parameters.vrsl;
            _loc2_ = loaderInfo.url.split("/viewer")[0] + "/viewer.rsl";
            this.echo("tempRslUrl : " + _loc2_);
            if((_loc2_) && _loc1_.length > 1)
            {
               return _loc2_ + _loc1_.substr(1) + ".swf";
            }
         }
         return super.rslUrl;
      }
   }
}
