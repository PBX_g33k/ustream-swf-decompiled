package tv.ustream.viewer.document
{
   public class ViewerVersion extends Object
   {
      
      public function ViewerVersion() {
         super();
      }
      
      public static const SVN_VERSION:String = "7645";
      
      public static const SVN_DATE:String = "2014/05/12 17:40:11";
      
      public static const BUILD_COMPUTER:String = "THISNOWALL";
      
      public static const BUILD_USER:String = "Tibor";
      
      public static const BUILD_DATE:String = "2014.05.19.  8:34:17,58";
      
      public static const BUILD_NUMBER:String = "258";
   }
}
