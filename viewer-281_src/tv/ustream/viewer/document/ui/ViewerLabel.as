package tv.ustream.viewer.document.ui
{
   import flash.utils.Timer;
   import flash.events.Event;
   import tv.ustream.tween2.Tween;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import flash.display.Shape;
   import flash.filters.DropShadowFilter;
   import flash.events.TimerEvent;
   import flash.display.DisplayObject;
   import flash.text.TextFormat;
   import tv.ustream.localization.Locale;
   import flash.text.TextField;
   
   public class ViewerLabel extends StatLabel
   {
      
      public function ViewerLabel(param1:DisplayObject = null, param2:String = null) {
         this.ViewerIcon = ViewerLabel_ViewerIcon;
         this.labels = [];
         var param1:DisplayObject = new this.ViewerIcon();
         param1.filters = [new DropShadowFilter(1,90,16777215,0.33,1,1,1,2,true),new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
         super(param1);
         switch(Locale.instance.language)
         {
            case "ja_JP":
            case "ko_KR":
            case "ru_RU":
            case "zh_CN":
            case "zh_TW":
            case "hu_HU":
               tf = new TextFormat(ViewerUI.baseFontName,13,10066329,false);
               this._embedFonts = ViewerUI.baseFontEmbedded;
               this._overSample = !ViewerUI.baseFontEmbedded;
               break;
            default:
               this._embedFonts = true;
               this._overSample = false;
         }
         var _loc3_:TextField = new TextField();
         _loc3_.embedFonts = this._embedFonts;
         _loc3_.defaultTextFormat = tf;
         _loc3_.text = " ";
         this._spaceWidth = Math.round(_loc3_.getCharBoundaries(0).width);
         mouseEnabled = true;
         mouseChildren = false;
         if(ViewerUI.touchMode)
         {
            addEventListener(MouseEvent.CLICK,this.open);
            this.idleTimer = new Timer(4000,1);
            this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         }
         else
         {
            addEventListener(MouseEvent.MOUSE_OVER,this.open);
            addEventListener(MouseEvent.MOUSE_OUT,this.close);
         }
      }
      
      private var ViewerIcon:Class;
      
      private var _width:Number = 0;
      
      private var _lblW:Number = 0;
      
      private var labels:Array;
      
      public var viewers:String;
      
      public var viewersTotal:String;
      
      public var unFormatedViewersTotal:String;
      
      private var _embedFonts:Boolean;
      
      private var _overSample:Boolean;
      
      private var _spaceWidth:Number;
      
      public var availWidth:Number = 1.7976931348623157E308;
      
      private var maxWidth:Number;
      
      private var idleTimer:Timer;
      
      private var _opened:Boolean;
      
      private var _formatedLabel:Boolean = false;
      
      private var _unformatedWidth:Number = 0;
      
      public var labelText:String;
      
      private function open(param1:Event = null) : void {
         if(this.availWidth < this.maxWidth)
         {
            return;
         }
         Tween.to(this,{"lblW":1},30 * 0.4,"easeOutQuart");
         var _loc2_:* = 0;
         while(_loc2_ < this.labels.length)
         {
            if(!this.labels[_loc2_].fixed)
            {
               Tween.to(this.labels[_loc2_].label,{"alpha":1},30 * 0.4);
            }
            _loc2_++;
         }
         if(ViewerUI.touchMode)
         {
            this.idleTimer.reset();
            this.idleTimer.start();
            this._opened = true;
            addEventListener(MouseEvent.CLICK,this.close);
         }
      }
      
      private function close(param1:Event = null) : void {
         if((param1) && (param1.type == MouseEvent.CLICK) && !this._opened)
         {
            return;
         }
         Tween.to(this,{"lblW":0},30 * 0.6,"easeOutQuart");
         var _loc2_:* = 0;
         while(_loc2_ < this.labels.length)
         {
            if(!this.labels[_loc2_].fixed)
            {
               Tween.to(this.labels[_loc2_].label,{"alpha":0},30 * 0.6);
            }
            _loc2_++;
         }
         if(ViewerUI.touchMode)
         {
            this.idleTimer.reset();
            this._opened = false;
            removeEventListener(MouseEvent.CLICK,this.close);
         }
      }
      
      public function get lblW() : Number {
         return this._lblW;
      }
      
      public function set lblW(param1:Number) : void {
         this._lblW = param1;
         this.resize();
      }
      
      override public function get text() : String {
         return _text;
      }
      
      override public function set text(param1:String) : void {
         var _loc5_:* = undefined;
         var _loc6_:* = undefined;
         var _loc7_:Array = null;
         var _loc8_:* = 0;
         var _loc9_:Shape = null;
         if(!param1)
         {
            return;
         }
         this.labelText = param1;
         var _loc2_:String = param1.split("#current#").join(this.viewers).split("#total#").join(this._formatedLabel?this.viewersTotal:this.format2(this.unFormatedViewersTotal));
         if(_text == _loc2_)
         {
            return;
         }
         _text = _loc2_;
         var _loc3_:Array = [];
         var _loc4_:Object = 
            {
               "/":"/",
               "#current#":this.viewers,
               "#total#":(this._formatedLabel?this.viewersTotal:this.format2(this.unFormatedViewersTotal))
            };
         _loc2_ = param1;
         for(_loc5_ in _loc4_)
         {
            if(param1.indexOf(_loc5_) != -1)
            {
               _loc7_ = _loc2_.split(_loc5_).join("*" + _loc5_ + "*").split("*");
               _loc8_ = 0;
               while(_loc8_ < _loc7_.length)
               {
                  _loc7_[_loc8_] = _loc7_[_loc8_].replace(new RegExp("^\\s*|\\s*$","g"),"");
                  _loc8_++;
               }
               _loc2_ = _loc7_.join("|");
            }
         }
         _loc8_ = 0;
         while(_loc8_ < _loc2_.split("|").length)
         {
            if(_loc2_.split("|")[_loc8_])
            {
               _loc3_.push(_loc2_.split("|")[_loc8_]);
            }
            _loc8_++;
         }
         if(_loc3_.length > 0 && !contains(icon))
         {
            addChild(icon);
         }
         while(this.labels.length > _loc3_.length)
         {
            _loc6_ = this.labels.pop().label;
            if((_loc6_) && (contains(_loc6_)))
            {
               removeChild(_loc6_);
            }
            if((_loc6_) && (_loc6_.mask))
            {
               if(contains(_loc6_.mask))
               {
                  removeChild(_loc6_.mask);
               }
               _loc6_.mask = null;
            }
         }
         _loc8_ = 0;
         while(_loc8_ < _loc3_.length)
         {
            if(!this.labels[_loc8_])
            {
               this.labels[_loc8_] = {};
            }
            if(!this.labels[_loc8_].label)
            {
               if((_loc4_[_loc3_[_loc8_]]) && !(_loc4_[_loc3_[_loc8_]] == "/"))
               {
                  _loc6_ = new MultiLetterLabel();
               }
               else
               {
                  _loc6_ = new Label();
               }
               addChild(_loc6_);
               _loc6_.ticker = true;
               _loc6_.textFormat = tf;
               _loc6_.embedFonts = this._embedFonts;
               _loc6_.overSample = this._overSample;
               _loc6_.filters = [new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
               this.labels[_loc8_].label = _loc6_;
            }
            else
            {
               _loc6_ = this.labels[_loc8_].label;
            }
            this.labels[_loc8_].spaceBefore = !(param1.indexOf(" " + _loc3_[_loc8_]) == -1);
            if(_loc4_[_loc3_[_loc8_]])
            {
               _loc6_.text = _loc4_[_loc3_[_loc8_]];
               _loc6_.name = _loc3_[_loc8_] == "/"?"slash":_loc3_[_loc8_] == "#current#"?"viewers":"viewersTotal";
               if(_loc6_.mask)
               {
                  if(contains(_loc6_.mask))
                  {
                     removeChild(_loc6_.mask);
                  }
                  _loc6_.mask = null;
               }
               this.labels[_loc8_].fixed = true;
            }
            else
            {
               _loc6_.text = this._embedFonts?_loc3_[_loc8_].toUpperCase():_loc3_[_loc8_];
               _loc6_.name = "";
               if(!_loc6_.mask)
               {
                  _loc9_ = new Shape();
                  _loc9_.graphics.beginFill(0);
                  _loc9_.graphics.drawRect(0,0,100,100);
                  _loc9_.graphics.endFill();
                  addChild(_loc9_);
                  _loc6_.mask = _loc9_;
               }
               this.labels[_loc8_].fixed = false;
            }
            _loc8_++;
         }
         this.height = height;
         this.resize();
      }
      
      private function format2(param1:String) : String {
         if(param1.length < 4)
         {
            return param1;
         }
         var _loc2_:* = "";
         var _loc3_:uint = 0;
         while(_loc3_ < param1.length)
         {
            _loc2_ = (_loc3_ % 3 == 2 && _loc3_ > 0 && _loc3_ < param1.length - 1?",":"") + param1.substr(-1 - _loc3_,1) + _loc2_;
            _loc3_++;
         }
         return _loc2_;
      }
      
      private function resize(param1:Event = null) : void {
         var _loc3_:* = undefined;
         this._width = 0;
         this.maxWidth = 0;
         if((icon) && (contains(icon)))
         {
            icon.x = this._width;
            this._width = this._width + (icon.width + 3);
            this.maxWidth = this.maxWidth + this._width;
         }
         var _loc2_:* = 0;
         while(_loc2_ < this.labels.length)
         {
            _loc3_ = this.labels[_loc2_].label;
            if(this.labels[_loc2_].fixed)
            {
               _loc3_.x = this._width = this._width + (this.labels[_loc2_].spaceBefore?this._spaceWidth:0) * (_loc3_.name == "viewers"?this._lblW:1);
               this._width = this._width + Math.round(_loc3_.width - 4);
            }
            else
            {
               _loc3_.mask.x = this._width = this._width + (this.labels[_loc2_].spaceBefore?this._spaceWidth:0) * this._lblW;
               _loc3_.mask.width = _loc3_.width * this._lblW;
               _loc3_.x = _loc3_.mask.x - _loc3_.width * (1 - this._lblW);
               this._width = this._width + (Math.round(_loc3_.mask.width) - 4 * this._lblW);
            }
            this.maxWidth = this.maxWidth + (Math.round(_loc3_.width - 4) + (this.labels[_loc2_].spaceBefore?this._spaceWidth:0));
            _loc2_++;
         }
         if(!this._formatedLabel)
         {
            this._unformatedWidth = this._width;
         }
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(0,0,this._width,height);
         graphics.endFill();
      }
      
      public function get unformatedWidth() : Number {
         return this._unformatedWidth;
      }
      
      public function get formatedLabel() : Boolean {
         return this._formatedLabel;
      }
      
      public function set formatedLabel(param1:Boolean) : void {
         if(param1 != this._formatedLabel)
         {
            this._formatedLabel = param1;
            this.text = this.labelText;
         }
      }
      
      override public function get width() : Number {
         return this._width * scaleX;
      }
      
      override public function destroy() : void {
         super.destroy();
         if(ViewerUI.touchMode)
         {
            this.idleTimer.reset();
            this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.close);
            removeEventListener(MouseEvent.CLICK,this.open);
            removeEventListener(MouseEvent.CLICK,this.close);
         }
         else
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.open);
            removeEventListener(MouseEvent.MOUSE_OUT,this.close);
         }
      }
      
      override public function set height(param1:Number) : void {
         var _loc3_:* = undefined;
         super.height = param1;
         icon.y = icon.y - 1;
         var _loc2_:* = 0;
         while(_loc2_ < this.labels.length)
         {
            _loc3_ = this.labels[_loc2_].label;
            _loc3_.y = Math.round((param1 - _loc3_.height) / 2);
            if(!this._embedFonts)
            {
               _loc3_.y = _loc3_.y - 1;
            }
            if(ViewerUI.baseFontName != "Arial")
            {
               _loc3_.y = _loc3_.y - 1;
            }
            _loc2_++;
         }
      }
   }
}
