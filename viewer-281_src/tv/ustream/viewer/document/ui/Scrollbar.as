package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.DisplayObject;
   import flash.utils.Timer;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   import tv.ustream.tween2.Tween;
   
   public class Scrollbar extends Sprite
   {
      
      public function Scrollbar() {
         this.dragOptions = {};
         super();
         addChild(this.bg = new Sprite());
         addChild(this.bar = new Sprite());
         this.bar.buttonMode = true;
         this.bar.mouseChildren = false;
         this.bar.y = this.padding;
         this.dragOptions = 
            {
               "l":this.bar.x,
               "t":this.bar.y,
               "r":this.bar.x
            };
         addEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
         addEventListener(MouseEvent.MOUSE_DOWN,this.handleBar);
         addEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
      }
      
      private var bg:Sprite;
      
      private var bar:Sprite;
      
      private var _target:DisplayObject;
      
      private var dragOptions:Object;
      
      private var unit:Number;
      
      private var lineNum:Number;
      
      private var isTxt:Boolean;
      
      private var hasBtns:Boolean;
      
      private var upTimer:Timer;
      
      private var downTimer:Timer;
      
      private var _barW:Number;
      
      private var _height:Number;
      
      private var bgColor:uint = 921102;
      
      private var bgAlpha:Number = 0.65;
      
      private var barColor:uint = 16777215;
      
      public var barAlpha:Number = 0.3;
      
      private var padding:Number = 5;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         addEventListener(Event.REMOVED_FROM_STAGE,this.removedFromStage);
         stage.addEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
      }
      
      private function removedFromStage(param1:Event = null) : void {
         addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.REMOVED_FROM_STAGE,this.removedFromStage);
         stage.removeEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.barRelease);
      }
      
      public function get target() : DisplayObject {
         return this._target;
      }
      
      public function set target(param1:DisplayObject) : void {
         if(this._target)
         {
            this._target.removeEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
            this._target.removeEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
         }
         this._target = param1;
         if(this._target)
         {
            this._target.addEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
            this._target.addEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
            this.dragOptions.targetY = this._target.y;
            this.setDragOptions();
         }
      }
      
      override public function set height(param1:Number) : void {
         this._height = param1;
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(0,0);
         this.bg.graphics.drawRect(0,0,15,this._height);
         this.bg.graphics.endFill();
         this.bg.graphics.beginFill(this.bgColor,this.bgAlpha);
         this.bg.graphics.drawRoundRect(5,this.padding,5,this._height - this.padding * 2,5);
         this.bg.graphics.endFill();
         this.setDragOptions();
      }
      
      private function setDragOptions() : void {
         if(!this._height || !this._target)
         {
            return;
         }
         this.unit = Math.round((this._target.height - this._height) / 25);
         this.lineNum = 0;
         this.dragOptions.barHeight = Math.max(30,Math.ceil(this._height / this._target.height * (this._height - this.padding * 2)));
         this.dragOptions.b = this.dragOptions.h = this._height - this.padding * 2 - this.dragOptions.barHeight;
         this.barW = 5;
      }
      
      public function get barW() : Number {
         return this._barW;
      }
      
      public function set barW(param1:Number) : void {
         this._barW = param1;
         this.bar.graphics.clear();
         this.bar.graphics.beginFill(0,0);
         this.bar.graphics.drawRect(0,0,15,this.dragOptions.barHeight);
         this.bar.graphics.endFill();
         this.bar.graphics.beginFill(this.barColor,this.barAlpha);
         this.bar.graphics.drawRoundRect((15 - param1) / 2,0,param1,this.dragOptions.barHeight,param1);
         this.bar.graphics.endFill();
      }
      
      private function handleBar(param1:MouseEvent) : void {
         switch(param1.type)
         {
            case MouseEvent.MOUSE_DOWN:
               if(param1.target == this.bar)
               {
                  addEventListener(Event.ENTER_FRAME,this.doScroll);
                  this.bar.startDrag(false,new Rectangle(this.dragOptions.l,this.dragOptions.t,this.dragOptions.r,this.dragOptions.b));
                  this._target.removeEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
                  this._target.removeEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
                  removeEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
                  removeEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
                  stage.addEventListener(MouseEvent.MOUSE_UP,this.barRelease);
               }
               break;
            case MouseEvent.MOUSE_OVER:
               Tween.to(this,
                  {
                     "barAlpha":0.64,
                     "barW":this._barW
                  },30 * 0.2,"easeOutExpo");
               break;
            case MouseEvent.MOUSE_OUT:
               Tween.to(this,
                  {
                     "barAlpha":0.3,
                     "barW":this._barW
                  },30 * 0.6,"easeOutExpo");
               break;
         }
      }
      
      private function barRelease(param1:MouseEvent) : void {
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.barRelease);
         removeEventListener(Event.ENTER_FRAME,this.doScroll);
         this.bar.stopDrag();
         this._target.addEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
         this._target.addEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
         addEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
         addEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
      }
      
      private function onMouseWheel(param1:MouseEvent) : void {
         if(!stage || param1.delta == 0)
         {
            return;
         }
         if(param1.delta > 0)
         {
            this.lineNum = Math.max(0,this.lineNum - 1);
         }
         else
         {
            this.lineNum = Math.min(this.lineNum + 1,this.unit);
         }
         var _loc2_:Number = 0 - Math.round(this.lineNum * (this._target.height - this._height) / this.unit);
         this.dragOptions.targetYto = this.dragOptions.targetY + _loc2_;
         if(this.target.y != this.dragOptions.targetYto)
         {
            addEventListener(Event.ENTER_FRAME,this.moveTarget);
         }
         var _loc3_:Number = this.dragOptions.h / (this._target.height - this._height);
         this.dragOptions.barY = Math.round(_loc3_ * Math.abs(_loc2_) + this.dragOptions.t);
         if(this.bar.y != this.dragOptions.barY)
         {
            addEventListener(Event.ENTER_FRAME,this.barEnterFrame);
         }
      }
      
      public function updateScroll() : void {
         var _loc1_:Number = this.dragOptions.h / (this._target.height - this._height);
         this.lineNum = Math.round(Math.abs(this.dragOptions.targetY - this._target.y) / ((this._target.height - this._height) / this.unit));
         this.dragOptions.targetYto = this._target.y;
         this.bar.y = Math.round(_loc1_ * Math.abs(this.dragOptions.targetY - this._target.y) + this.dragOptions.t);
      }
      
      private function barEnterFrame(param1:Event) : void {
         this.bar.y = this.bar.y + (this.dragOptions.barY - this.bar.y) / 3;
         this.bar.y = this.dragOptions.barY > this.bar.y?Math.ceil(this.bar.y):Math.floor(this.bar.y);
         if(this.bar.y == this.dragOptions.barY)
         {
            removeEventListener(Event.ENTER_FRAME,this.barEnterFrame);
         }
      }
      
      private function doScroll(param1:Event) : void {
         var _loc2_:Number = 0 - Math.round((this._target.height - this._height) * (this.bar.y - this.dragOptions.t) / this.dragOptions.h);
         this.lineNum = Math.round(Math.abs(_loc2_) / ((this._target.height - this._height) / this.unit));
         _loc2_ = this.dragOptions.targetY + _loc2_;
         if(this._target.y != _loc2_)
         {
            this.dragOptions.targetYto = _loc2_;
            addEventListener(Event.ENTER_FRAME,this.moveTarget);
         }
      }
      
      private function moveTarget(param1:Event) : void {
         this._target.y = this._target.y + (this.dragOptions.targetYto - this._target.y) / 6;
         this._target.y = this.dragOptions.targetYto > this._target.y?Math.ceil(this._target.y):Math.floor(this._target.y);
         dispatchEvent(new Event(Event.SCROLL));
         if(this._target.y == this.dragOptions.targetYto)
         {
            removeEventListener(Event.ENTER_FRAME,this.moveTarget);
         }
      }
      
      public function reset() : void {
         this.bar.y = this.dragOptions.t;
      }
      
      public function destroy() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.REMOVED_FROM_STAGE,this.removedFromStage);
         if(this._target)
         {
            this._target.y = this.dragOptions.targetY;
            this._target.removeEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
            this._target.removeEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
         }
         removeEventListener(Event.ENTER_FRAME,this.barEnterFrame);
         removeEventListener(Event.ENTER_FRAME,this.moveTarget);
         removeEventListener(Event.ENTER_FRAME,this.doScroll);
         removeEventListener(MouseEvent.MOUSE_OVER,this.handleBar);
         removeEventListener(MouseEvent.MOUSE_DOWN,this.handleBar);
         removeEventListener(MouseEvent.MOUSE_OUT,this.handleBar);
      }
   }
}
