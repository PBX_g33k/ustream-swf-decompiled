package tv.ustream.viewer.document.ui
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class KeyboardNotification_PlayIcon extends SpriteAsset
   {
      
      public function KeyboardNotification_PlayIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
