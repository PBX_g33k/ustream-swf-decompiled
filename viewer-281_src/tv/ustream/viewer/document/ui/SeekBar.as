package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.utils.Timer;
   import flash.events.Event;
   import tv.ustream.viewer.document.ui.icons.*;
   import flash.geom.*;
   import flash.text.TextFormat;
   import flash.filters.DropShadowFilter;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import flash.display.GradientType;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.localization.Locale;
   import flash.events.TimerEvent;
   import tv.ustream.tween2.Tween;
   import tv.ustream.tools.StringUtils;
   import tv.ustream.viewer.logic.Logic;
   
   public class SeekBar extends Sprite
   {
      
      public function SeekBar() {
         super();
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private static var dvrTipShown:Boolean = false;
      
      private var _bg:Sprite;
      
      private var _bgW:Number = 0;
      
      private var _duration:Number = 0;
      
      private var _progress:Number = 0;
      
      private var _width:Number = 0;
      
      private var _dvr:Boolean = false;
      
      public var isDragging:Boolean;
      
      public var isTweening:Boolean;
      
      private var _opened:Boolean = true;
      
      private var bufferBar:Shape;
      
      private var _bufferBarColor:uint = 16777215;
      
      private var _bufferBarAlpha:Number = 0.3;
      
      private var progressBar:Shape;
      
      private var _barHeight:Number = 6;
      
      private var seekBtn:Button;
      
      private var seekLabel:ToolTip;
      
      private var _intervalStart:Number = 0;
      
      private var _intervalEnd:Number = 1;
      
      private var _intervalLock:Boolean = false;
      
      private var _intervalEditable:Boolean;
      
      private var _intervalMinimum:Number = 0;
      
      private var _intervalMinimumSeconds:Number = 0;
      
      private var startMarker:Button;
      
      private var endMarker:Button;
      
      private var draggedMarker:Button;
      
      private var isDraggingMarkers:Boolean;
      
      private var _bufferStart:Number = 0;
      
      private var _bufferPercent:Number = 0;
      
      private var _seekBtnPaddingLeft:Number = 8;
      
      private var _seekBtnPaddingRight:Number = 8;
      
      private var dvrtip:ToolTip;
      
      private var dvrTipTimer:Timer;
      
      private function onAddedToStage(param1:Event = null) : void {
         var e:Event = param1;
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         addChild(this._bg = new Sprite());
         this._bg.buttonMode = true;
         addChild(this.bufferBar = new Shape());
         addChild(this.progressBar = new Shape());
         addChild(this.seekBtn = new Button());
         with(this.seekBtn)
         {
         }
         icon = new SeekBtnIcon(8,3);
         _hitArea = new Rectangle(-8,-8,16,16);
         noGlow = true;
         x = width / 2;
         y = -_barHeight / 2;
         }
         addChild(this.seekLabel = new ToolTip());
         this.seekLabel.embedFonts = true;
         this.seekLabel.textFormat = new TextFormat("MyriadCondBoldItalic",14,16777215);
         this.seekLabel.textFilters = [new DropShadowFilter(1,90,0,0.39,1,1,1,2)];
         this.seekLabel.padding = 6;
         this.seekLabel.bgHeight = 26;
         this.seekLabel.bgColor = ViewerUI.style.color;
         this.seekLabel.bgAlpha = 0.9;
         this.seekLabel.bgGradientOverlay = [16777215,16777215];
         this.seekLabel.bgGradientOverlayAlpha = [0.12,0];
         this.seekLabel.bgCornerRadius = 3;
         this.seekLabel.text = " ";
         this.seekLabel.x = 0;
         this.seekLabel.y = -25;
         this.seekLabel.alpha = 0;
         this.seekLabel.visible = false;
         addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         this._bg.addEventListener(MouseEvent.CLICK,this.onBgClick);
         this.seekBtn.addEventListener(MouseEvent.MOUSE_DOWN,this.onSeekBtnDown);
         this.width = stage.stageWidth;
      }
      
      override public function get width() : Number {
         return this._width;
      }
      
      override public function set width(param1:Number) : void {
         this._width = param1;
         var _loc2_:Number = this.bufferPercent;
         var _loc3_:Number = this.progress;
         this.bgW = this._width;
         this.bufferPercent = _loc2_;
         this.progress = _loc3_;
         if(this._intervalEditable)
         {
            this.positionMarkers();
         }
      }
      
      public function get bgW() : Number {
         return this._bgW;
      }
      
      public function set bgW(param1:Number) : void {
         this._bgW = param1;
         this._bg.graphics.clear();
         this._bg.graphics.beginFill(16737792,0);
         this._bg.graphics.drawRect(0,-12,param1,18);
         this._bg.graphics.endFill();
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(param1,this.barHeight,90 * Math.PI / 180,0,-this.barHeight);
         this._bg.graphics.beginGradientFill(GradientType.LINEAR,[3355443,2434341],[0.7,0.7],[0,255],_loc2_);
         this._bg.graphics.drawRect(0,-this.barHeight,param1,this.barHeight);
         this._bg.graphics.endFill();
      }
      
      public function get intervalStart() : Number {
         return this._intervalStart;
      }
      
      public function set intervalStart(param1:Number) : void {
         this._intervalStart = Math.max(0,Math.min(this._intervalEnd,param1));
         this.setSeekLabel();
         if(this._intervalEditable)
         {
            if(!this.isDraggingMarkers && (this._progress < this._intervalStart || this._progress > this._intervalEnd))
            {
               this.progress = Math.max(this._intervalStart,Math.min(this._intervalEnd,this._progress));
               dispatchEvent(new DynamicEvent(ViewerUI.SEEK_SET,true,false,
                  {
                     "seekTo":this._progress,
                     "source":"seekbar"
                  }));
            }
            this.positionMarkers();
            this.setBufferBar();
            this.progress = this._progress;
            this.updateMarkerLabels();
            this.setMarkerTooltipVisibility();
         }
      }
      
      public function get intervalEnd() : Number {
         return this._intervalEnd;
      }
      
      public function set intervalEnd(param1:Number) : void {
         this._intervalEnd = Math.max(this._intervalStart,Math.min(1,param1));
         this.setSeekLabel();
         if(this._intervalEditable)
         {
            if(!this.isDraggingMarkers && (this._progress < this._intervalStart || this._progress > this._intervalEnd))
            {
               this.progress = Math.max(this._intervalStart,Math.min(this._intervalEnd,this._progress));
               dispatchEvent(new DynamicEvent(ViewerUI.SEEK_SET,true,false,
                  {
                     "seekTo":this._progress,
                     "source":"seekbar"
                  }));
            }
            this.positionMarkers();
            this.setBufferBar();
            this.progress = this._progress;
            this.updateMarkerLabels();
            this.setMarkerTooltipVisibility();
         }
      }
      
      public function get intervalLock() : Boolean {
         return this._intervalLock;
      }
      
      public function set intervalLock(param1:Boolean) : void {
         this._intervalLock = param1;
         this.setSeekLabel();
      }
      
      public function get intervalEditable() : Boolean {
         return this._intervalEditable;
      }
      
      public function set intervalEditable(param1:Boolean) : void {
         this._intervalEditable = param1;
         if(this._intervalEditable)
         {
            this.createMarkers();
         }
         else
         {
            this.destroyMarkers();
         }
      }
      
      public function get intervalMinimum() : Number {
         return this._intervalMinimum;
      }
      
      public function set intervalMinimum(param1:Number) : void {
         this._intervalMinimum = param1;
         if(this._duration)
         {
            this._intervalMinimumSeconds = this._intervalMinimum * this._duration;
         }
      }
      
      public function get intervalMinimumSeconds() : Number {
         return this._intervalMinimumSeconds;
      }
      
      public function set intervalMinimumSeconds(param1:Number) : void {
         this._intervalMinimumSeconds = param1;
         if(this._duration)
         {
            this.intervalMinimum = param1 / this._duration;
         }
      }
      
      public function get bufferPercent() : Number {
         return this._bufferPercent;
      }
      
      public function set bufferPercent(param1:Number) : void {
         this._bufferPercent = param1;
         this.setBufferBar();
      }
      
      public function get bufferStart() : Number {
         return this._bufferStart;
      }
      
      public function set bufferStart(param1:Number) : void {
         this._bufferStart = param1;
         this.setBufferBar();
      }
      
      public function get dvr() : Boolean {
         return this._dvr;
      }
      
      public function set dvr(param1:Boolean) : void {
         this._dvr = param1;
      }
      
      public function showDvrTip() : void {
         if(!SeekBar.dvrTipShown)
         {
            if(!this.dvrtip)
            {
               this.dvrtip = new ToolTip();
               addChild(this.dvrtip);
               this.dvrtip.textFormat = new TextFormat(ViewerUI.baseFontName,11,16777215);
               this.dvrtip.embedFonts = ViewerUI.baseFontEmbedded;
               this.dvrtip.overSample = !ViewerUI.baseFontEmbedded;
               this.dvrtip.textFilters = [new DropShadowFilter(1,90,0,0.39,1,1,1,2)];
               this.dvrtip.padding = 6;
               this.dvrtip.bgHeight = 24;
               this.dvrtip.bgColor = ViewerUI.style.color;
               this.dvrtip.bgAlpha = 0.9;
               this.dvrtip.bgGradientOverlay = [16777215,16777215];
               this.dvrtip.bgGradientOverlayAlpha = [0.12,0];
               this.dvrtip.bgCornerRadius = 3;
               this.dvrtip.text = Locale.instance.label(ViewerLabels.labelDvrDragToRewind);
               this.dvrtip.x = this.bgW - 10;
               this.dvrtip.y = this.seekBtn.y - 10;
               this.dvrtip.xOffset = this._width - 10 - (this.dvrtip.x + Math.round(this.dvrtip.width / 2));
            }
            else
            {
               this.dvrtip.visible = true;
            }
            if(!this.dvrTipTimer)
            {
               this.dvrTipTimer = new Timer(3300,1);
            }
            this.dvrTipTimer.addEventListener(TimerEvent.TIMER,this.removeDvrTip);
            this.dvrTipTimer.start();
         }
      }
      
      private function removeDvrTip(param1:Event = null) : void {
         var e:Event = param1;
         if(this.dvrTipTimer)
         {
            this.dvrTipTimer.stop();
            this.dvrTipTimer.removeEventListener(TimerEvent.TIMER,this.removeDvrTip);
            this.dvrTipTimer = null;
         }
         if((this.dvrtip) && (this.dvrtip.parent) && (this.dvrtip.visible))
         {
            Tween.to(this.dvrtip,{"alpha":0},30 * 0.6,"easeOutQuart",function():void
            {
               dvrtip.visible = false;
               SeekBar.dvrTipShown = true;
               removeChild(dvrtip);
            });
         }
      }
      
      private function setBufferBar() : void {
         var _loc4_:* = NaN;
         var _loc5_:* = NaN;
         var _loc6_:* = NaN;
         var _loc1_:Number = this._bufferStart;
         var _loc2_:Number = this._bufferStart + this._bufferPercent * (1 - this._bufferStart);
         if((this._intervalLock) || (this._intervalEditable))
         {
            _loc1_ = Math.max(0,Math.min(1,(_loc1_ - this._intervalStart) / (this._intervalEnd - this._intervalStart)));
            _loc2_ = Math.max(0,Math.min(1,(_loc2_ - this._intervalStart) / (this._intervalEnd - this._intervalStart)));
         }
         var _loc3_:Rectangle = new Rectangle(0,-this._barHeight,0,this._barHeight);
         if(this._intervalEditable)
         {
            _loc4_ = this.seekBtn.scaleX;
            _loc5_ = this.startMarker.x + this._seekBtnPaddingLeft * _loc4_ + (this.endMarker.x - this.startMarker.x - (this._seekBtnPaddingLeft + this._seekBtnPaddingRight) * _loc4_) * _loc1_;
            _loc6_ = this.startMarker.x + (this.endMarker.x - this.startMarker.x) * _loc2_;
            _loc5_ = Math.max(this.startMarker.x,Math.min(this.endMarker.x,_loc5_));
            _loc6_ = Math.max(this.startMarker.x,Math.min(this.endMarker.x,_loc6_));
         }
         else
         {
            _loc4_ = (this._barHeight - 3) / 3;
            _loc5_ = this._seekBtnPaddingLeft * _loc4_ + (this._width - (this._seekBtnPaddingLeft + this._seekBtnPaddingRight) * _loc4_) * _loc1_;
            _loc6_ = this._width * _loc2_;
         }
         _loc3_.left = _loc5_;
         _loc3_.right = _loc6_;
         if(isNaN(_loc3_.x))
         {
            _loc3_.x = 0;
         }
         if(isNaN(_loc3_.width))
         {
            _loc3_.width = 0;
         }
         this.bufferBar.graphics.clear();
         this.bufferBar.graphics.beginFill(this._bufferBarColor,this._bufferBarAlpha);
         this.bufferBar.graphics.drawRect(_loc3_.x,-this.barHeight,_loc3_.width,this.barHeight);
         this.bufferBar.graphics.endFill();
      }
      
      public function get progress() : Number {
         return this._intervalLock?this._intervalStart + this._progress * (this._intervalEnd - this._intervalStart):this._progress;
      }
      
      public function set progress(param1:Number) : void {
         this._progress = param1;
         if(this._intervalLock)
         {
            this._progress = (this._progress - this._intervalStart) / (this._intervalEnd - this._intervalStart);
         }
         this._progress = Math.max(0,Math.min(1,this._progress));
         if(!this.isDragging)
         {
            if(this._intervalEditable)
            {
               this.seekBtn.x = this.startMarker.x + Math.round(this._seekBtnPaddingLeft + (this.endMarker.x - this.startMarker.x - this._seekBtnPaddingLeft - this._seekBtnPaddingRight) * (this._progress - this._intervalStart) / (this._intervalEnd - this._intervalStart));
            }
            else
            {
               this.seekBtn.x = Math.round(this._seekBtnPaddingLeft + (this._width - this._seekBtnPaddingLeft - this._seekBtnPaddingRight) * this._progress);
            }
         }
         this.setProgressBar();
         this.setSeekLabel();
      }
      
      public function setProgressBar() : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         var _loc4_:* = NaN;
         var _loc1_:Rectangle = new Rectangle(0,-this.barHeight,0,this.barHeight);
         if(this._intervalEditable)
         {
            _loc2_ = this.seekBtn.scaleX;
            _loc3_ = 0;
            _loc4_ = this.startMarker.x + this._seekBtnPaddingLeft * _loc2_ + (this.endMarker.x - this.startMarker.x - (this._seekBtnPaddingLeft + this._seekBtnPaddingRight) * _loc2_) * (this._progress - this._intervalStart) / (this._intervalEnd - this._intervalStart);
            _loc3_ = Math.max(this.startMarker.x,Math.min(this.endMarker.x,_loc3_));
            _loc4_ = Math.max(this.startMarker.x,Math.min(this.endMarker.x,_loc4_));
         }
         else
         {
            _loc2_ = (this._barHeight - 3) / 3;
            _loc3_ = 0;
            _loc4_ = this._seekBtnPaddingLeft * _loc2_ + (this._width - (this._seekBtnPaddingLeft + this._seekBtnPaddingRight) * _loc2_) * this._progress;
         }
         _loc1_.left = _loc3_;
         _loc1_.right = _loc4_;
         if(isNaN(_loc1_.x))
         {
            _loc1_.x = 0;
         }
         if(isNaN(_loc1_.width))
         {
            _loc1_.width = 0;
         }
         var _loc5_:Matrix = new Matrix();
         _loc5_.createGradientBox(_loc1_.width,_loc1_.height,90 * Math.PI / 180,0,_loc1_.y);
         this.progressBar.graphics.clear();
         this.progressBar.graphics.beginFill(ViewerUI.style.color);
         this.progressBar.graphics.drawRect(_loc1_.x,_loc1_.y,_loc1_.width,_loc1_.height);
         this.progressBar.graphics.endFill();
         this.progressBar.graphics.beginGradientFill(GradientType.LINEAR,[16777215,16777215],[0.15,0],[0,255],_loc5_);
         this.progressBar.graphics.drawRect(_loc1_.x,_loc1_.y,_loc1_.width,_loc1_.height);
         this.progressBar.graphics.endFill();
      }
      
      public function get seekX() : Number {
         return this.seekBtn.x;
      }
      
      public function set seekX(param1:Number) : void {
         this.seekBtn.x = param1;
         this.progress = this.getProgressFromPosition(param1);
      }
      
      private function getProgressFromPosition(param1:Number) : Number {
         var _loc2_:* = NaN;
         if(this._intervalEditable)
         {
            _loc2_ = (param1 - this.startMarker.x - this._seekBtnPaddingLeft) / (this.endMarker.x - this.startMarker.x - this._seekBtnPaddingLeft - this._seekBtnPaddingRight);
         }
         else
         {
            _loc2_ = (param1 - this._seekBtnPaddingLeft) / (this._width - this._seekBtnPaddingLeft - this._seekBtnPaddingRight);
         }
         if((this._intervalLock) || (this._intervalEditable))
         {
            _loc2_ = this._intervalStart + (this._intervalEnd - this._intervalStart) * _loc2_;
         }
         return _loc2_;
      }
      
      public function get barHeight() : Number {
         return this._barHeight;
      }
      
      public function set barHeight(param1:Number) : void {
         this._barHeight = param1;
         this.bgW = this.bgW;
         this.setBufferBar();
         this.setProgressBar();
         this.seekBtn.y = -this._barHeight / 2;
         if(this.startMarker)
         {
            this.startMarker.y = -this._barHeight / 2;
         }
         if(this.endMarker)
         {
            this.endMarker.y = -this._barHeight / 2;
         }
      }
      
      public function get duration() : Number {
         return this._duration;
      }
      
      public function set duration(param1:Number) : void {
         this._duration = param1;
         this.setSeekLabel();
         this.updateMarkerLabels();
      }
      
      private function setSeekLabel() : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         if(!this.seekLabel.visible)
         {
            return;
         }
         this.removeDvrTip();
         if(this.isDragging)
         {
            _loc2_ = (this.seekLabel.x - this._seekBtnPaddingLeft) / (this._width - this._seekBtnPaddingLeft - this._seekBtnPaddingRight);
         }
         else
         {
            _loc2_ = this.seekLabel.x / this._width;
         }
         if(this._dvr)
         {
            _loc3_ = Math.abs(Math.floor(this._duration - this._duration * _loc2_));
            this.seekLabel.text = _loc3_ == 0?"LIVE":"-" + this.formatTime(_loc3_);
         }
         else
         {
            _loc2_ = Math.abs((this._intervalLock?this._duration * this._intervalEnd - this._duration * this._intervalStart:this._duration) * _loc2_);
            this.seekLabel.text = this.formatTime(_loc2_);
         }
         var _loc1_:Number = 8;
         if(this.seekLabel.x - Math.round(this.seekLabel.width / 2) < _loc1_)
         {
            this.seekLabel.xOffset = _loc1_ - (this.seekLabel.x - Math.round(this.seekLabel.width / 2));
         }
         else if(this.seekLabel.x + Math.round(this.seekLabel.width / 2) > this._width - _loc1_)
         {
            this.seekLabel.xOffset = this._width - _loc1_ - (this.seekLabel.x + Math.round(this.seekLabel.width / 2));
         }
         else
         {
            this.seekLabel.xOffset = 0;
         }
         
      }
      
      private function formatTime(param1:Number, param2:Boolean = true) : String {
         var _loc3_:Date = new Date();
         _loc3_.setTime(param1 * 1000);
         _loc3_.setHours(_loc3_.getUTCHours());
         return StringUtils.getFormatDateString(param1 < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc3_);
      }
      
      public function open() : void {
         if(!this._duration)
         {
            return;
         }
         this._opened = true;
         if(!this._intervalEditable || this.endMarker.x - this.startMarker.x >= 16)
         {
            Tween.to(this.seekBtn,
               {
                  "scaleX":1,
                  "scaleY":1,
                  "alpha":1
               },30 * 0.3,"easeOutExpo");
         }
         if(this.startMarker)
         {
            Tween.to(this.startMarker,
               {
                  "scaleX":1,
                  "scaleY":1,
                  "alpha":1
               },30 * 0.3,"easeOutExpo");
         }
         if(this.endMarker)
         {
            Tween.to(this.endMarker,
               {
                  "scaleX":1,
                  "scaleY":1,
                  "alpha":1
               },30 * 0.3,"easeOutExpo");
         }
         Tween.to(this,{"barHeight":6},30 * 0.3,"easeOutExpo");
      }
      
      public function close() : void {
         if(this._intervalEditable)
         {
            return;
         }
         this._opened = false;
         this.progress = this.progress;
         Tween.to(this.seekBtn,
            {
               "scaleX":0,
               "scaleY":0,
               "alpha":0
            },30 * 0.6,"easeOutQuart");
         if(this.startMarker)
         {
            Tween.to(this.startMarker,
               {
                  "scaleX":0,
                  "scaleY":0,
                  "alpha":0
               },30 * 0.6,"easeOutQuart");
         }
         if(this.endMarker)
         {
            Tween.to(this.endMarker,
               {
                  "scaleX":0,
                  "scaleY":0,
                  "alpha":0
               },30 * 0.6,"easeOutQuart");
         }
         Tween.to(this,{"barHeight":3},30 * 0.6,"easeOutQuart");
         this.removeDvrTip();
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
      }
      
      private function onMouseOver(param1:MouseEvent) : void {
         if((this.isDragging) || !(param1.target == this._bg) && !(param1.target == this.seekBtn))
         {
            return;
         }
         Tween.kill(this.seekLabel);
         this.seekLabel.visible = true;
         Tween.to(this.seekLabel,
            {
               "alpha":1,
               "y":-15
            },30 * 0.3,"easeOutExpo");
         this.onMouseMove();
         stage.addEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
         addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
      }
      
      private function onMouseOut(param1:MouseEvent) : void {
         var e:MouseEvent = param1;
         if((this.isDragging) || e.relatedObject == this.seekBtn || e.relatedObject == this._bg)
         {
            return;
         }
         Tween.kill(this.seekLabel);
         Tween.to(this.seekLabel,
            {
               "alpha":0,
               "y":-25
            },30 * 0.6,"easeOutExpo",function():void
         {
            seekLabel.visible = false;
         });
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
      }
      
      private function onSeekBtnDown(param1:MouseEvent) : void {
         this.isDragging = true;
         var _loc2_:Rectangle = new Rectangle(this._seekBtnPaddingLeft,this.seekBtn.y,this._width - this._seekBtnPaddingLeft - this._seekBtnPaddingRight,0);
         if(this._intervalEditable)
         {
            _loc2_.left = this.startMarker.x + this._seekBtnPaddingLeft;
            _loc2_.right = this.endMarker.x - this._seekBtnPaddingRight;
         }
         this.seekBtn.startDrag(false,_loc2_);
         stage.addEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
         stage.addEventListener(MouseEvent.MOUSE_UP,this.onDragStop);
         stage.addEventListener(Event.MOUSE_LEAVE,this.onDragStop);
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         dispatchEvent(new Event(ViewerUI.SEEK_START_DRAG,true));
         this.removeDvrTip();
      }
      
      private function onBgClick(param1:MouseEvent) : void {
         var slideTo:Number = NaN;
         var e:MouseEvent = param1;
         this.isTweening = true;
         Tween.kill(this);
         if(this._intervalEditable)
         {
            slideTo = Math.max(this.startMarker.x + this._seekBtnPaddingLeft,Math.min(this.endMarker.x - this._seekBtnPaddingRight,mouseX));
         }
         else
         {
            slideTo = Math.max(this._seekBtnPaddingLeft,Math.min(this._width - this._seekBtnPaddingRight,mouseX));
         }
         dispatchEvent(new DynamicEvent(ViewerUI.SEEK_SET,true,false,
            {
               "seekTo":this.getProgressFromPosition(slideTo),
               "source":"seekbar"
            }));
         Tween.to(this,{"seekX":slideTo},30 * 0.3,"easeOutQuart",function():void
         {
            isTweening = false;
         });
      }
      
      private function onMouseMove(param1:MouseEvent = null) : void {
         var _loc2_:* = NaN;
         if(this.isDragging)
         {
            this.removeDvrTip();
            this.seekLabel.x = Math.max(0,Math.min(this._width,this.seekBtn.x));
            if(this._intervalEditable)
            {
               _loc2_ = (this.seekBtn.x - this.startMarker.x - this._seekBtnPaddingLeft) / (this.endMarker.x - this.startMarker.x - this._seekBtnPaddingLeft - this._seekBtnPaddingRight);
            }
            else
            {
               _loc2_ = (this.seekBtn.x - this._seekBtnPaddingLeft) / (this._width - this._seekBtnPaddingLeft - this._seekBtnPaddingRight);
            }
            if((this._intervalLock) || (this._intervalEditable))
            {
               _loc2_ = this._intervalStart + _loc2_ * (this._intervalEnd - this._intervalStart);
            }
            this.progress = _loc2_;
            dispatchEvent(new Event(ViewerUI.SEEK_DRAG,true));
         }
         else
         {
            this.seekLabel.x = Math.max(0,Math.min(this._width,mouseX));
            this.setSeekLabel();
         }
         if(param1 != null)
         {
            param1.updateAfterEvent();
         }
      }
      
      private function onDragStop(param1:*) : void {
         if(!this.isDragging)
         {
            return;
         }
         this.seekBtn.stopDrag();
         this.onMouseMove();
         this.isDragging = false;
         if(stage)
         {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
            stage.removeEventListener(MouseEvent.MOUSE_UP,this.onDragStop);
            stage.removeEventListener(Event.MOUSE_LEAVE,this.onDragStop);
         }
         if(param1.type == MouseEvent.MOUSE_UP && !(param1.target == this.seekBtn) || param1.type == Event.MOUSE_LEAVE)
         {
            this.onMouseOut(new MouseEvent(MouseEvent.MOUSE_OUT));
         }
         if(stage)
         {
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.attachMouseOver);
         }
         dispatchEvent(new Event(ViewerUI.SEEK_STOP_DRAG,true));
      }
      
      private function attachMouseOver(param1:MouseEvent) : void {
         addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.attachMouseOver);
      }
      
      public function destroy() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         this._bg.removeEventListener(MouseEvent.CLICK,this.onBgClick);
         this.seekBtn.removeEventListener(MouseEvent.MOUSE_DOWN,this.onSeekBtnDown);
         this.seekBtn.destroy();
         Tween.kill(this.seekBtn);
         Tween.kill(this.seekLabel);
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMouseMove);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         this.seekBtn.stopDrag();
         Tween.kill(this);
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.onDragStop);
         stage.removeEventListener(Event.MOUSE_LEAVE,this.onDragStop);
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.attachMouseOver);
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
         this.destroyMarkers();
      }
      
      private function updateStyle(... rest) : void {
         this.seekBtn.icon.colorize(ViewerUI.style.color);
         this.seekLabel.bgColor = ViewerUI.style.color;
         this.seekLabel.text = this.seekLabel.text;
         this.setProgressBar();
      }
      
      private function createMarkers() : void {
         var _loc1_:Object = null;
         if(!this.startMarker && !this.endMarker)
         {
            _loc1_ = 
               {
                  "embedFonts":true,
                  "textFormat":new TextFormat("MyriadCondBoldItalic",14,16777215)
               };
            this.startMarker = new Button();
            this.startMarker.icon = new HighlightMarkerIcon();
            this.startMarker._hitArea = new Rectangle(-11,-8,11,16);
            this.startMarker.noGlow = true;
            this.startMarker.tooltipStyle = _loc1_;
            this.startMarker.autoHideTooltip = false;
            this.startMarker.addEventListener(MouseEvent.MOUSE_DOWN,this.onMarkerDown);
            this.startMarker.addEventListener(MouseEvent.MOUSE_OVER,this.setMarkerTooltipVisibility);
            this.endMarker = new Button();
            this.endMarker.icon = new HighlightMarkerIcon();
            this.endMarker._hitArea = new Rectangle(0,-8,11,16);
            this.endMarker.noGlow = true;
            this.endMarker.tooltipStyle = _loc1_;
            this.endMarker.autoHideTooltip = false;
            this.endMarker.addEventListener(MouseEvent.MOUSE_DOWN,this.onMarkerDown);
            this.endMarker.addEventListener(MouseEvent.MOUSE_OVER,this.setMarkerTooltipVisibility);
            addChild(this.startMarker);
            addChild(this.endMarker);
            this.positionMarkers();
            this.open();
         }
      }
      
      private function destroyMarkers() : void {
         if(this.draggedMarker)
         {
            this.draggedMarker = null;
         }
         if(this.startMarker)
         {
            this.startMarker.stopDrag();
            this.startMarker.removeEventListener(MouseEvent.MOUSE_DOWN,this.onMarkerDown);
            this.startMarker.removeEventListener(MouseEvent.MOUSE_OVER,this.setMarkerTooltipVisibility);
            this.startMarker.destroy();
            if(contains(this.startMarker))
            {
               removeChild(this.startMarker);
            }
            this.startMarker = null;
         }
         if(this.endMarker)
         {
            this.endMarker.stopDrag();
            this.endMarker.removeEventListener(MouseEvent.MOUSE_DOWN,this.onMarkerDown);
            this.endMarker.removeEventListener(MouseEvent.MOUSE_OVER,this.setMarkerTooltipVisibility);
            this.endMarker.destroy();
            if(contains(this.endMarker))
            {
               removeChild(this.endMarker);
            }
            this.endMarker = null;
         }
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.onMarkerUp);
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMarkerMove);
      }
      
      private function positionMarkers() : void {
         var _loc1_:Rectangle = new Rectangle(this.startMarker.width,0,this._width - this.startMarker.width - this.endMarker.width,0);
         if(this.startMarker != this.draggedMarker)
         {
            this.startMarker.x = _loc1_.x + Math.round(this._intervalStart * _loc1_.width);
         }
         if(this.endMarker != this.draggedMarker)
         {
            this.endMarker.x = _loc1_.x + Math.round(this._intervalEnd * _loc1_.width);
         }
         this.updateMarkerLabels();
      }
      
      private function setMarkerTooltipVisibility(param1:Event = null) : void {
         var _loc2_:Button = param1?param1.currentTarget as Button:this.draggedMarker?this.draggedMarker:this.startMarker;
         var _loc3_:Number = 1;
         var _loc4_:Number = 1;
         if(this.startMarker.getTooltipBounds(stage).intersects(this.endMarker.getTooltipBounds(stage)))
         {
            switch(_loc2_)
            {
               case this.startMarker:
                  _loc4_ = 1;
                  _loc3_ = 0.4;
                  break;
               case this.endMarker:
                  _loc3_ = 0.4;
                  _loc4_ = 1;
                  break;
            }
         }
         if(this.startMarker.maxTooltipAlpha != _loc3_)
         {
            this.startMarker.maxTooltipAlpha = _loc3_;
            this.startMarker.showTooltip();
         }
         if(this.endMarker.maxTooltipAlpha != _loc4_)
         {
            this.endMarker.maxTooltipAlpha = _loc4_;
            this.endMarker.showTooltip();
         }
      }
      
      private function updateMarkerLabels() : void {
         var _loc1_:* = NaN;
         if(this.startMarker)
         {
            _loc1_ = Math.round(Math.abs(this._duration) * this._intervalStart);
            _loc1_ = Math.min(Math.abs(this._duration),Math.max(0,_loc1_));
            this.startMarker.tooltip = this.formatTime(_loc1_);
         }
         if(this.endMarker)
         {
            _loc1_ = Math.round(Math.abs(this._duration) * this._intervalEnd);
            _loc1_ = Math.min(Math.abs(this._duration),Math.max(0,_loc1_));
            this.endMarker.tooltip = this.formatTime(_loc1_);
         }
      }
      
      private function onMarkerDown(param1:MouseEvent) : void {
         var _loc2_:Rectangle = null;
         if(this._intervalEditable)
         {
            this.draggedMarker = param1.currentTarget as Button;
            this.setMarkerTooltipVisibility();
            _loc2_ = new Rectangle(this.startMarker.width,this.draggedMarker.y,this._width - this.startMarker.width - this.endMarker.width,0);
            switch(this.draggedMarker)
            {
               case this.startMarker:
                  _loc2_.width = _loc2_.width * (this._intervalEnd - this._intervalMinimum);
                  break;
               case this.endMarker:
                  _loc2_.x = _loc2_.x + _loc2_.width * (this._intervalStart + this._intervalMinimum);
                  _loc2_.width = _loc2_.width - _loc2_.width * (this._intervalStart + this._intervalMinimum);
                  break;
            }
            this.draggedMarker.startDrag(false,_loc2_);
            stage.addEventListener(MouseEvent.MOUSE_UP,this.onMarkerUp);
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.onMarkerMove);
            Tween.to(this.seekBtn,
               {
                  "scaleX":0,
                  "scaleY":0,
                  "alpha":0
               },30 * 0.3,"easeOutExpo");
            if(Logic.instance.media.playing)
            {
               Logic.instance.media.pause();
            }
            this.isDraggingMarkers = true;
         }
      }
      
      private function onMarkerUp(param1:MouseEvent) : void {
         this.isDraggingMarkers = false;
         this.onMarkerMove();
         this.setMarkerTooltipVisibility();
         this.draggedMarker.stopDrag();
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.onMarkerUp);
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onMarkerMove);
         this.draggedMarker = null;
         if(this._progress < this._intervalStart || this._progress > this._intervalEnd)
         {
            this.progress = Math.max(this._intervalStart,Math.min(this._intervalEnd,this._progress));
            dispatchEvent(new DynamicEvent(ViewerUI.SEEK_SET,true,false,
               {
                  "seekTo":this._progress,
                  "source":"seekbar"
               }));
         }
         if(this.endMarker.x - this.startMarker.x >= 16)
         {
            Tween.to(this.seekBtn,
               {
                  "scaleX":1,
                  "scaleY":1,
                  "alpha":1
               },30 * 0.3,"easeOutExpo");
         }
      }
      
      private function onMarkerMove(param1:MouseEvent = null) : void {
         var _loc2_:Rectangle = null;
         var _loc3_:* = NaN;
         if(this.draggedMarker)
         {
            _loc2_ = new Rectangle(this.startMarker.width,0,this._width - this.startMarker.width - this.endMarker.width,0);
            _loc3_ = (this.draggedMarker.x - _loc2_.x) / _loc2_.width;
            _loc3_ = Math.max(0,Math.min(1,_loc3_));
            switch(this.draggedMarker)
            {
               case this.startMarker:
                  this.intervalStart = Math.max(0,Math.min(this._intervalEnd,_loc3_));
                  dispatchEvent(new DynamicEvent(Event.CHANGE,false,false,{"start":this._intervalStart}));
                  break;
               case this.endMarker:
                  this.intervalEnd = Math.max(this._intervalStart,Math.min(1,_loc3_));
                  dispatchEvent(new DynamicEvent(Event.CHANGE,false,false,{"end":this._intervalEnd}));
                  break;
            }
            if(param1)
            {
               param1.updateAfterEvent();
            }
         }
      }
   }
}
