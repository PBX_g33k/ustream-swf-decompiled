package tv.ustream.viewer.document.ui
{
   import flash.text.TextFormat;
   import flash.geom.Rectangle;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   import flash.events.MouseEvent;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tween2.Color;
   import flash.events.Event;
   
   public class LargeButton extends Button
   {
      
      public function LargeButton(param1:Number = 14, param2:Number = 26, param3:Number = 11, param4:Number = 4) {
         this.strokeColors = [];
         super();
         this._fontSize = param1;
         this._bgHeight = param2;
         this.padding = param3;
         this._cornerRadius = param4;
         this.setStyle(this._defaultStyle);
      }
      
      private var strokeColors:Array;
      
      private var _bgGradientOverlay:Array;
      
      private var _strokeColors:Array;
      
      private var _strokeColorsOnDown:Array;
      
      private var _bgFilters:Array;
      
      private var _activeTextFormat:TextFormat;
      
      private var _textFilters:Array;
      
      private var _disabledTextFormat:TextFormat;
      
      private var _disabledTextFilters:Array;
      
      private var _defaultStyle:int = 1;
      
      private var _cornerRadius:Number;
      
      private var _fontSize:Number;
      
      private var _bgHeight:Number;
      
      private var _padding:Number;
      
      override public function set text(param1:String) : void {
         graphics.clear();
         _hitArea = null;
         super.text = param1;
         _hitArea = new Rectangle(0,0,width + 2,height + 2);
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(_hitArea.width,_hitArea.height,90 * Math.PI / 180);
         graphics.beginGradientFill(GradientType.LINEAR,this.strokeColors,[1,1],[0,255],_loc2_);
         graphics.drawRoundRect(0,0,_hitArea.width,_hitArea.height,(this._cornerRadius + 1) * 2);
         graphics.endFill();
         filters = [new DropShadowFilter(1,90,0,0.2,2,2,1,2)];
      }
      
      override public function set enabled(param1:Boolean) : void {
         super.enabled = param1;
         if(param1)
         {
            addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
            addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
            addEventListener(MouseEvent.MOUSE_DOWN,this.onMouseDown);
            addEventListener(MouseEvent.MOUSE_UP,this.onMouseUp);
            if(txtTarget)
            {
               textFilters = this._textFilters;
               textFormat = this._activeTextFormat;
            }
         }
         else
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
            removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
            removeEventListener(MouseEvent.MOUSE_DOWN,this.onMouseDown);
            removeEventListener(MouseEvent.MOUSE_UP,this.onMouseUp);
            if(txtTarget)
            {
               textFilters = this._disabledTextFilters;
               textFormat = this._disabledTextFormat;
            }
         }
      }
      
      public function get defaultStyle() : int {
         return this._defaultStyle;
      }
      
      public function set defaultStyle(param1:int) : void {
         this._defaultStyle = param1;
         this.setStyle(this._defaultStyle);
      }
      
      private function setStyle(param1:int = 1) : void {
         embedFonts = ViewerUI.baseFontEmbedded;
         overSample = !ViewerUI.baseFontEmbedded;
         switch(param1)
         {
            case 2:
               this._activeTextFormat = textFormat = new TextFormat(ViewerUI.baseFontName,this._fontSize,16777215,true);
               this._textFilters = textFilters = [new DropShadowFilter(1,90,Color.setBrightness(15895086,-0.3),1,1,1,1,2)];
               bgColor = 15895086;
               bgAlpha = 1;
               this._bgGradientOverlay = bgGradientOverlay = [16165204,15761704];
               bgGradientOverlayAlpha = [1,1];
               bgHeight = this._bgHeight;
               bgCornerRadius = this._cornerRadius;
               this._bgFilters = bgFilters = [new DropShadowFilter(1,90,16777215,0.26,1,1,1,2,true)];
               this._strokeColors = this.strokeColors = [14186798,14186798];
               this._strokeColorsOnDown = [14186798,14186798];
               this._disabledTextFormat = new TextFormat(ViewerUI.baseFontName,this._fontSize,Color.setBrightness(15895086,-0.25),true);
               this._disabledTextFilters = [new DropShadowFilter(1,90,Color.setBrightness(15895086,0.2),1,1,1,1,2)];
               break;
            case 1:
               this._activeTextFormat = textFormat = new TextFormat(ViewerUI.baseFontName,this._fontSize,16777215,true);
               this._textFilters = textFilters = [new DropShadowFilter(1,90,Color.setBrightness(ViewerUI.style.color,-0.3),1,1,1,1,2)];
               bgColor = ViewerUI.style.color;
               bgAlpha = 1;
               this._bgGradientOverlay = bgGradientOverlay = [16777215,16777215];
               bgGradientOverlayAlpha = [0.12,0];
               bgHeight = this._bgHeight;
               bgCornerRadius = this._cornerRadius;
               this._bgFilters = bgFilters = [new DropShadowFilter(1,90,16777215,0.26,1,1,1,2,true)];
               this._strokeColors = this.strokeColors = [Color.setBrightness(ViewerUI.style.color,0.2),Color.setBrightness(ViewerUI.style.color,-0.2)];
               this._strokeColorsOnDown = [Color.setBrightness(ViewerUI.style.color,-0.2),Color.setBrightness(ViewerUI.style.color,-0.2)];
               this._disabledTextFormat = new TextFormat(ViewerUI.baseFontName,this._fontSize,Color.setBrightness(ViewerUI.style.color,-0.25),true);
               this._disabledTextFilters = [new DropShadowFilter(1,90,Color.setBrightness(ViewerUI.style.color,0.2),1,1,1,1,2)];
               break;
            case 0:
               this._activeTextFormat = this._disabledTextFormat = textFormat = new TextFormat(ViewerUI.baseFontName,this._fontSize,1052688,true);
               this._textFilters = this._disabledTextFilters = textFilters = [new DropShadowFilter(1,90,16777215,0.15,1,1,1,2)];
               bgColor = 5592405;
               bgAlpha = 1;
               this._bgGradientOverlay = bgGradientOverlay = [16777215,0];
               bgGradientOverlayAlpha = [0.12,0.12];
               bgHeight = this._bgHeight;
               bgCornerRadius = this._cornerRadius;
               this._bgFilters = bgFilters = [new DropShadowFilter(1,90,16777215,0.15,1,1,1,2,true)];
               this._strokeColors = this.strokeColors = [2500134,1184531];
               this._strokeColorsOnDown = [1184531,1184531];
               break;
         }
         this.text = text;
      }
      
      override protected function handleMouseEvents(param1:MouseEvent) : void {
      }
      
      private function onMouseOver(param1:Event = null) : void {
         bgGradientOverlay = null;
         this.text = text;
      }
      
      private function onMouseOut(param1:Event = null) : void {
         bgGradientOverlay = this._bgGradientOverlay;
         this.text = text;
      }
      
      private function onMouseDown(param1:Event = null) : void {
         bgFilters = [new DropShadowFilter(1,90,0,0.12,4,4,2,2,true)];
         this.strokeColors = this._strokeColorsOnDown;
         this.text = text;
      }
      
      private function onMouseUp(param1:Event = null) : void {
         bgFilters = this._bgFilters;
         this.strokeColors = this._strokeColors;
         this.text = text;
      }
      
      override public function destroy() : void {
         super.destroy();
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         removeEventListener(MouseEvent.MOUSE_DOWN,this.onMouseDown);
         removeEventListener(MouseEvent.MOUSE_UP,this.onMouseUp);
      }
      
      override protected function updateStyle(... rest) : void {
         this.setStyle(this._defaultStyle);
         this.enabled = enabled;
      }
      
      override public function set width(param1:Number) : void {
         bgWidth = isNaN(param1)?param1:param1 - 2;
         this.text = text;
      }
   }
}
