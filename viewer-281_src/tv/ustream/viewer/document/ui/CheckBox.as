package tv.ustream.viewer.document.ui
{
   import flash.display.Shape;
   import flash.geom.Rectangle;
   import flash.events.Event;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import tv.ustream.tween.Tween;
   import flash.display.Sprite;
   import flash.filters.DropShadowFilter;
   import flash.filters.GlowFilter;
   
   public class CheckBox extends Button
   {
      
      public function CheckBox(param1:Boolean = false) {
         this._minHeight = Number.MIN_VALUE;
         super();
         noGlow = true;
         icon = new Sprite();
         icon.addChild(this.indicator = new Shape());
         textFilters = [new DropShadowFilter(1,90,0,0.7,2,2,1,2)];
         this.renderCustomContent();
         icon.filters = [new DropShadowFilter(1,90,16777215,0.15,1,1,1,2),new DropShadowFilter(1,90,0,0.36,3,3,1,2,true),new GlowFilter(0,0.05,1,1,2,2,true)];
         this.indicator.filters = [new DropShadowFilter(1,90,16777215,0.45,1,1,1,2,true),new DropShadowFilter(1,90,0,0.31,2,2,1,2)];
         this._checked = param1;
         if(!this._checked)
         {
            icon.alpha = 0.47;
            this.indicator.scaleX = this.indicator.scaleY = this.indicator.alpha = 0;
            addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
            addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         }
         addEventListener(MouseEvent.CLICK,this.onMouseClick);
      }
      
      public var indicator:Shape;
      
      private var _checked:Boolean;
      
      protected var _minHeight:Number;
      
      protected var _maxWidth:Number = 1.7976931348623157E308;
      
      protected var _iconBgColor:uint = 16777215;
      
      override public function set label(param1:String) : void {
         super.label = param1;
         if(!this._checked)
         {
            txtTarget.alpha = 0.67;
         }
      }
      
      public function get minHeight() : Number {
         return this._minHeight;
      }
      
      public function set minHeight(param1:Number) : void {
         this._minHeight = param1;
         this.render();
      }
      
      public function get maxWidth() : Number {
         return this._maxWidth;
      }
      
      public function set maxWidth(param1:Number) : void {
         this._maxWidth = param1;
         this.render();
      }
      
      override protected function render() : void {
         super.render();
         this.renderCustomContent();
         var _loc1_:Number = Math.max(this._minHeight,Math.max(icon.height,txtTarget?txtTarget.height:0));
         var _loc2_:Number = Math.min(this._maxWidth,icon.width + (txtTarget?6 + txtTarget.width:0));
         _hitArea = new Rectangle(0,0,_loc2_,_loc1_);
         icon.x = 0;
         icon.y = Math.round((_loc1_ - icon.height) / 2);
         if(txtTarget)
         {
            if(txtTarget.height > 4)
            {
               removeEventListener(Event.ENTER_FRAME,this.checkLabelHeight);
               txtTarget.x = icon.x + icon.width + 6;
               txtTarget.y = Math.round((_loc1_ - txtTarget.height) / 2);
               if(txtTarget.width > _loc2_ - txtTarget.x)
               {
                  multiline = wordWrap = true;
                  textWidth = _loc2_ - txtTarget.x;
               }
            }
            else
            {
               addEventListener(Event.ENTER_FRAME,this.checkLabelHeight);
            }
         }
      }
      
      private function checkLabelHeight(param1:Event) : void {
         if(txtTarget.height > 4)
         {
            removeEventListener(Event.ENTER_FRAME,this.checkLabelHeight);
            this.render();
         }
      }
      
      protected function renderCustomContent() : void {
         icon.graphics.clear();
         icon.graphics.beginFill(this._iconBgColor);
         icon.graphics.drawRoundRect(0,0,14,14,3);
         icon.graphics.endFill();
         this.indicator.graphics.clear();
         this.indicator.x = icon.width / 2;
         this.indicator.y = icon.height / 2;
         this.indicator.graphics.lineStyle(3,ViewerUI.style.color);
         this.indicator.graphics.moveTo(3 - 7,8 - 7);
         this.indicator.graphics.lineTo(5 - 7,10 - 7);
         this.indicator.graphics.lineTo(11 - 7,4 - 7);
         this.indicator.graphics.endFill();
      }
      
      public function get checked() : Boolean {
         return this._checked;
      }
      
      public function set checked(param1:Boolean) : void {
         if(this._checked == param1)
         {
            return;
         }
         this._checked = param1;
         if(this._checked)
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
            removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
            if(txtTarget)
            {
               Tween.to(txtTarget,{"alpha":1},30 * 0.2,"easeOutExpo");
            }
            Tween.to(icon,{"alpha":1},30 * 0.2,"easeOutExpo");
            Tween.to(this.indicator,
               {
                  "scaleX":1,
                  "scaleY":1,
                  "alpha":1
               },30 * 0.4,"easeOutExpo");
         }
         else
         {
            addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
            addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
            if(txtTarget)
            {
               Tween.to(txtTarget,{"alpha":0.67},30 * 0.4,"easeOutQuart");
            }
            Tween.to(icon,{"alpha":0.47},30 * 0.4,"easeOutQuart");
            Tween.to(this.indicator,
               {
                  "scaleX":0,
                  "scaleY":0,
                  "alpha":0
               },30 * 0.6,"easeOutExpo");
         }
      }
      
      protected function onMouseClick(param1:Event) : void {
         if(!interactive || (interactive) && !(param1.target == txtTarget))
         {
            this.checked = !this._checked;
            dispatchEvent(new Event(Event.CHANGE));
         }
      }
      
      protected function onMouseOver(param1:Event) : void {
         if(txtTarget)
         {
            Tween.to(txtTarget,{"alpha":1},30 * 0.2,"easeOutExpo");
         }
      }
      
      protected function onMouseOut(param1:Event) : void {
         if(txtTarget)
         {
            Tween.to(txtTarget,{"alpha":0.67},30 * 0.4,"easeOutQuart");
         }
      }
      
      public function get iconBgColor() : uint {
         return this._iconBgColor;
      }
      
      public function set iconBgColor(param1:uint) : void {
         this._iconBgColor = param1;
         this.renderCustomContent();
      }
      
      override public function destroy() : void {
         super.destroy();
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         removeEventListener(MouseEvent.CLICK,this.onMouseClick);
         removeEventListener(Event.ENTER_FRAME,this.checkLabelHeight);
      }
      
      override protected function updateStyle(... rest) : void {
         this.renderCustomContent();
      }
   }
}
