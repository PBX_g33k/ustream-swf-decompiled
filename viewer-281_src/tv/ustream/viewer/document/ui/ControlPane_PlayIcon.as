package tv.ustream.viewer.document.ui
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class ControlPane_PlayIcon extends SpriteAsset
   {
      
      public function ControlPane_PlayIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
