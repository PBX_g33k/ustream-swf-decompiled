package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import tv.ustream.viewer.document.ui.icons.ComboBoxArrow;
   import flash.display.Shape;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.filters.DropShadowFilter;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.display.DisplayObject;
   import tv.ustream.tween2.Tween;
   
   public class ComboBox extends Sprite
   {
      
      public function ComboBox(param1:String = null) {
         this.items = new Array();
         super();
         this._title = param1;
         addChild(this.btn = new LargeButton());
         this.btn.defaultStyle = 0;
         this.btn.padding = 8;
         this.btn.textAlign = "left";
         this.btn.text = this._title;
         this.btn.addChild(this.arrow = new ComboBoxArrow());
         this.arrow.filters = [new DropShadowFilter(1,90,16777215,0.15,1,1,1,2)];
         this.btn.addEventListener(MouseEvent.CLICK,this.toggleOpen);
         this.list = new Sprite();
         this.list.filters = [new DropShadowFilter(1,90,0,0.11,2,2,1,2)];
         this.list.addChild(this.itemList = new Sprite());
         this.list.addChild(this.itemListMask = new Shape());
         this.itemList.mask = this.itemListMask;
         this.list.addChild(this.itemList);
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      public var selectedIndex:Number = -1;
      
      public var rowLimit:Number = 15;
      
      private var _opened:Boolean;
      
      private var _title:String;
      
      private var arrow:ComboBoxArrow;
      
      private var btn:LargeButton;
      
      private var items:Array;
      
      private var list:Sprite;
      
      private var itemList:Sprite;
      
      private var itemListMask:Shape;
      
      private var scrollbar:Scrollbar;
      
      private var _width:Number;
      
      private var _listHeight:Number;
      
      private var _listBgHeight:Number;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         addEventListener(Event.REMOVED_FROM_STAGE,this.removedFromStage);
         stage.addEventListener(MouseEvent.CLICK,this.onReleaseOutside);
         stage.addEventListener(Event.RESIZE,this.setListPos);
      }
      
      private function removedFromStage(param1:Event = null) : void {
         addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.REMOVED_FROM_STAGE,this.removedFromStage);
         if(stage.contains(this.list))
         {
            stage.removeChild(this.list);
         }
         stage.removeEventListener(MouseEvent.CLICK,this.onReleaseOutside);
         stage.removeEventListener(Event.RESIZE,this.setListPos);
      }
      
      override public function set width(param1:Number) : void {
         this._width = param1;
         this.btn.bgWidth = this._width;
         this.btn.text = this.btn.text;
         this.arrow.x = param1 - 12;
         this.arrow.y = Math.floor(this.btn.height / 2);
         var _loc2_:* = 0;
         while(_loc2_ < this.items.length)
         {
            this.resizeItem(this.items[_loc2_].item,this._width);
            _loc2_++;
         }
      }
      
      override public function set height(param1:Number) : void {
         this.btn.bgHeight = param1;
         this.btn.text = this.btn.text;
      }
      
      public function get selectedItem() : Object {
         return this.selectedIndex == -1?null:this.items[this.selectedIndex].value;
      }
      
      public function addItem(... rest) : void {
         var _loc3_:Sprite = null;
         var _loc2_:* = 0;
         while(_loc2_ < rest.length)
         {
            _loc3_ = this.createItem(rest[_loc2_].label);
            _loc3_.addEventListener(MouseEvent.MOUSE_OVER,this.onItemOver);
            _loc3_.addEventListener(MouseEvent.MOUSE_OUT,this.onItemOut);
            _loc3_.addEventListener(MouseEvent.CLICK,this.onItemClick);
            this.items.push(
               {
                  "item":_loc3_,
                  "value":rest[_loc2_]
               });
            this.itemList.addChild(_loc3_);
            _loc3_.y = _loc2_?this.items[_loc2_ - 1].item.y + this.items[_loc2_ - 1].item.height:0;
            _loc2_++;
         }
      }
      
      private function createItem(param1:String) : Sprite {
         var _loc2_:Sprite = null;
         _loc2_ = new Sprite();
         var _loc3_:Label = new Label();
         var _loc4_:Shape = new Shape();
         _loc2_.addChild(_loc4_);
         _loc4_.name = "bg";
         _loc2_.addChild(_loc3_);
         _loc3_.name = "label";
         _loc3_.textFormat = new TextFormat(ViewerUI.baseFontName,14,16777215);
         _loc3_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc3_.multiline = true;
         _loc3_.wordWrap = true;
         _loc3_.textWidth = this._width - 16;
         _loc3_.text = param1;
         _loc3_.alpha = 0.64;
         _loc3_.x = 8;
         _loc3_.filters = [new DropShadowFilter(1,90,0,0.6,2,2,1,2)];
         this.resizeItem(_loc2_,this._width);
         _loc2_.mouseChildren = false;
         _loc2_.buttonMode = true;
         return _loc2_;
      }
      
      private function resizeItem(param1:Sprite, param2:Number) : void {
         var _loc3_:Label = param1.getChildByName("label") as Label;
         var _loc4_:Shape = param1.getChildByName("bg") as Shape;
         _loc3_.textWidth = param2 - 16;
         var _loc5_:Number = _loc3_.numLines > 1?Math.round(_loc3_.height) + 2 * 3:24;
         _loc3_.y = _loc3_.numLines > 1?1:Math.round((_loc5_ - _loc3_.height) / 2) - 1;
         _loc4_.graphics.clear();
         _loc4_.graphics.beginFill(ViewerUI.style.color);
         _loc4_.graphics.drawRect(0,0,param2,_loc5_);
         _loc4_.graphics.endFill();
         var _loc6_:Matrix = new Matrix();
         _loc6_.createGradientBox(param2,_loc5_,90 * Math.PI / 180);
         _loc4_.graphics.beginGradientFill(GradientType.LINEAR,[16777215,16777215],[0.23,0],[0,255],_loc6_);
         _loc4_.graphics.drawRect(0,0,param2,_loc5_);
         _loc4_.graphics.endFill();
         _loc4_.alpha = 0;
         param1.graphics.clear();
         param1.graphics.beginFill(16737792,0);
         param1.graphics.drawRect(0,0,param2,_loc5_);
         param1.graphics.endFill();
      }
      
      public function selectItem(param1:Number) : void {
         this.selectedIndex = param1;
         var _loc2_:String = ((this.items[param1].item as Sprite).getChildByName("label") as Label).text;
         this.btn.text = _loc2_;
      }
      
      private function setListPos(param1:Event = null) : void {
         if(!stage.contains(this.list))
         {
            return;
         }
         var _loc2_:Point = localToGlobal(new Point(0,this.btn.height));
         this.list.x = _loc2_.x;
         this.list.y = _loc2_.y;
         this.list.scaleX = this.gScaleX;
         this.list.scaleY = this.gScaleY;
         this.listHeight = (stage.stageHeight - _loc2_.y - 24 * this.gScaleY) / this.gScaleY;
      }
      
      public function get listHeight() : Number {
         return this._listHeight;
      }
      
      public function set listHeight(param1:Number) : void {
         this._listHeight = Math.min(this.itemList.height,param1);
         if(this.scrollbar)
         {
            this.scrollbar.destroy();
            if(this.list.contains(this.scrollbar))
            {
               this.list.removeChild(this.scrollbar);
            }
            this.scrollbar = null;
         }
         this.list.scrollRect = new Rectangle(0,0,this._width,this._listHeight);
         if(this.itemList.height > this._listHeight)
         {
            this.list.addChild(this.scrollbar = new Scrollbar());
            this.scrollbar.target = this.itemList;
            this.scrollbar.height = this._listHeight;
            this.scrollbar.x = this._width - this.scrollbar.width;
         }
         this.itemListMask.graphics.clear();
         this.itemListMask.graphics.beginFill(16737792);
         this.itemListMask.graphics.drawRoundRectComplex(0,0,this._width,this._listHeight,0,0,4,4);
         this.itemListMask.graphics.endFill();
         this.listBgHeight = this._listHeight;
      }
      
      public function get listBgHeight() : Number {
         return this._listBgHeight;
      }
      
      public function set listBgHeight(param1:Number) : void {
         this._listBgHeight = param1;
         this.list.graphics.clear();
         this.list.graphics.beginFill(1710618,1);
         this.list.graphics.drawRoundRectComplex(0,0,this._width,this._listBgHeight,0,0,4,4);
         this.list.graphics.endFill();
      }
      
      private function toggleOpen(param1:MouseEvent) : void {
         if(this._opened)
         {
            this.close();
         }
         else
         {
            this.open();
         }
      }
      
      private function onReleaseOutside(param1:MouseEvent) : void {
         if((this._opened) && !this.contains(DisplayObject(param1.target)) && !this.list.contains(DisplayObject(param1.target)) && !(param1.target == this) && !(param1.target == this.list))
         {
            this.close();
         }
      }
      
      public function open() : void {
         this._opened = true;
         if(!stage.contains(this.list))
         {
            stage.addChild(this.list);
         }
         this.setListPos();
         if(this.itemList.y == 0)
         {
            this.itemList.y = this.itemList.y - this.itemList.height;
         }
         if(this.itemListMask.y == 0)
         {
            this.itemListMask.y = this.itemListMask.y - this.itemListMask.height;
         }
         this.listBgHeight = 0;
         if((this.scrollbar) && this.scrollbar.y == 0)
         {
            this.scrollbar.y = this.scrollbar.y - this.scrollbar.height;
         }
         Tween.kill(this.itemList);
         Tween.to(this.itemList,{"y":0},30 * 0.4,"easeOutExpo");
         Tween.to(this.itemListMask,{"y":0},30 * 0.4,"easeOutExpo");
         Tween.to(this,{"listBgHeight":this._listHeight},30 * 0.4,"easeOutExpo");
         if(this.scrollbar)
         {
            Tween.kill(this.scrollbar);
            Tween.to(this.scrollbar,{"y":0},30 * 0.4,"easeOutExpo");
         }
         Tween.to(this.arrow,{"rotation":180},30 * 0.3);
      }
      
      public function close() : void {
         this._opened = false;
         var i:int = 0;
         while(i < this.items.length)
         {
            this.onItemOut(this.items[i].item);
            i++;
         }
         Tween.kill(this.itemList);
         Tween.to(this.itemList,{"y":-this.itemList.height},30 * 0.3,"easeOutExpo",function():void
         {
            if(stage.contains(list))
            {
               stage.removeChild(list);
            }
         });
         Tween.to(this.itemListMask,{"y":-this.itemListMask.height},30 * 0.3,"easeOutExpo");
         Tween.to(this,{"listBgHeight":0},30 * 0.3,"easeOutExpo");
         if(this.scrollbar != null)
         {
            Tween.kill(this.scrollbar);
            Tween.to(this.scrollbar,{"y":-this.scrollbar.height - 2},30 * 0.3,"easeOutExpo",function():void
            {
               scrollbar.reset();
            });
         }
         Tween.to(this.arrow,{"rotation":0},30 * 0.3);
      }
      
      private function onItemOver(param1:*) : void {
         var _loc2_:Sprite = (param1 is MouseEvent?param1.currentTarget:param1) as Sprite;
         Tween.to(_loc2_.getChildByName("bg"),{"alpha":1},30 * 0.2,"easeOutExpo");
         Tween.to(_loc2_.getChildByName("label"),{"alpha":1},30 * 0.2,"easeOutExpo");
      }
      
      private function onItemOut(param1:*) : void {
         var _loc2_:Sprite = (param1 is MouseEvent?param1.currentTarget:param1) as Sprite;
         Tween.to(_loc2_.getChildByName("bg"),{"alpha":0},30 * 0.6,"easeOutExpo");
         Tween.to(_loc2_.getChildByName("label"),{"alpha":0.64},30 * 0.6,"easeOutExpo");
      }
      
      private function onItemClick(param1:MouseEvent) : void {
         var _loc2_:Number = 0;
         while(_loc2_ < this.items.length)
         {
            if(param1.currentTarget == this.items[_loc2_].item)
            {
               this.selectItem(_loc2_);
               dispatchEvent(new Event(Event.CHANGE));
               this.close();
               break;
            }
            _loc2_++;
         }
      }
      
      public function get gScaleX() : Number {
         var _loc1_:Rectangle = this.getBounds(stage);
         return _loc1_.width / width;
      }
      
      public function get gScaleY() : Number {
         var _loc1_:Rectangle = this.getBounds(stage);
         return _loc1_.height / height;
      }
      
      public function destroy() : void {
         var _loc2_:Sprite = null;
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.ADDED_TO_STAGE,this.removedFromStage);
         this.btn.removeEventListener(MouseEvent.CLICK,this.toggleOpen);
         this.btn.destroy();
         removeEventListener(Event.ENTER_FRAME,this.setListPos);
         if(this.scrollbar != null)
         {
            this.scrollbar.destroy();
         }
         var _loc1_:* = 0;
         while(_loc1_ < this.items.length)
         {
            _loc2_ = this.items[_loc1_].item;
            _loc2_.removeEventListener(MouseEvent.MOUSE_OVER,this.onItemOver);
            _loc2_.removeEventListener(MouseEvent.MOUSE_OUT,this.onItemOut);
            _loc2_.removeEventListener(MouseEvent.CLICK,this.onItemClick);
            _loc1_++;
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(... rest) : void {
         var _loc2_:* = 0;
         while(_loc2_ < this.items.length)
         {
            this.resizeItem(this.items[_loc2_].item,this._width);
            _loc2_++;
         }
      }
   }
}
