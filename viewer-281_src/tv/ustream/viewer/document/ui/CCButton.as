package tv.ustream.viewer.document.ui
{
   import tv.ustream.viewer.document.ui.icons.CCIcon;
   import flash.geom.Rectangle;
   
   public class CCButton extends Button
   {
      
      public function CCButton() {
         super();
         icon = new CCIcon();
         _hitArea = new Rectangle(0,0,18,18);
         colorizable = false;
      }
      
      public function set isCCOn(param1:Boolean) : void {
         icon.enabled = param1;
      }
   }
}
