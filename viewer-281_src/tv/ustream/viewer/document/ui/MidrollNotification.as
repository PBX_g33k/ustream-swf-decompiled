package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.utils.Timer;
   import flash.events.Event;
   import flash.filters.DropShadowFilter;
   import tv.ustream.localization.Locale;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import tv.ustream.tween2.Tween;
   import flash.events.TimerEvent;
   import flash.geom.Rectangle;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.This;
   import tv.ustream.tools.Shell;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.tools.StringUtils;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.display.BitmapData;
   import tv.ustream.tools.Debug;
   
   public class MidrollNotification extends Sprite
   {
      
      public function MidrollNotification(param1:uint = 0) {
         super();
         this._time = param1;
         visible = false;
         this.idleTimer = new Timer(15000,1);
         this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private var _h:Number = 32;
      
      private var _width:Number = 0;
      
      private var _time:uint;
      
      private var offeringUrl:String = "http://www.ustream.tv/premium-membership";
      
      private var countdownText:String = "Ad begins in #time# or";
      
      private var offeringText:String;
      
      private var bg:Shape;
      
      private var holder:Sprite;
      
      private var countdownLbl:Label;
      
      private var watchNowBtn:LargeButton;
      
      private var offeringHolder:Sprite;
      
      private var offeringMask:Shape;
      
      private var offeringItems:Array;
      
      private var isOverText:Boolean;
      
      private var idleTimer:Timer;
      
      private var _opened:Boolean;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         addChild(this.bg = new Shape());
         this.bg.alpha = 0.8;
         this.bg.filters = [new DropShadowFilter(1,90,16777215,0.11,0,0,4,1,true),new DropShadowFilter(0,90,0,0.77,3,3,1,2)];
         addChild(this.holder = new Sprite());
         this.countdownText = Locale.instance.label(ViewerLabels.labelMidNotification);
         this.holder.addChild(this.countdownLbl = new Label());
         this.countdownLbl.textFormat = new TextFormat(ViewerUI.baseFontName,13,11184810);
         this.countdownLbl.embedFonts = ViewerUI.baseFontEmbedded;
         this.countdownLbl.overSample = !ViewerUI.baseFontEmbedded;
         this.countdownLbl.text = this.setTime();
         this.countdownLbl.filters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
         this.holder.addChild(this.watchNowBtn = new LargeButton(13,20,5,3));
         this.watchNowBtn.text = Locale.instance.label(ViewerLabels.labelWatchNow);
         this.watchNowBtn.enabled = false;
         this.watchNowBtn.mouseEnabled = true;
         this.holder.addChild(this.offeringHolder = new Sprite());
         this.holder.addChild(this.offeringMask = new Shape());
         this.offeringHolder.cacheAsBitmap = this.offeringMask.cacheAsBitmap = true;
         this.offeringHolder.mask = this.offeringMask;
         this.offeringText = Locale.instance.label(ViewerLabels.labelMidNotificationAd).split(" - #learnmore#").join("").split("#learnmore#").join("");
         this.offeringItems = [this.createOfferingItem(),this.createOfferingItem()];
         this.updateOfferingItems(this.offeringText,Locale.instance.label(ViewerLabels.labelMidNotificationAdLink));
         this.offeringHolder.addChild(this.offeringItems[0]);
         this.resize();
         addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         this.watchNowBtn.addEventListener(MouseEvent.CLICK,this.onWatchNowClick);
         if(ViewerUI.touchMode)
         {
            stage.addEventListener(MouseEvent.CLICK,this.open);
         }
         else
         {
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.open);
         }
         y = -this._h - 6;
         alpha = 0;
         addEventListener(Event.ENTER_FRAME,this.open);
      }
      
      private function open(param1:Event = null) : void {
         var e:Event = param1;
         removeEventListener(Event.ENTER_FRAME,this.open);
         if(!visible)
         {
            visible = true;
         }
         this.resetIdleTimer();
         if(this._opened)
         {
            return;
         }
         Tween.kill(this);
         Tween.to(this,
            {
               "y":0,
               "alpha":1
            },30 * 0.4,"easeOutExpo",null,null,function():void
         {
            y = Math.round(y);
            dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
         });
         this._opened = true;
      }
      
      public function close(param1:Event = null) : void {
         var e:Event = param1;
         if(!e)
         {
            if(ViewerUI.touchMode)
            {
               stage.removeEventListener(MouseEvent.CLICK,this.open);
            }
            else
            {
               stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.open);
            }
         }
         if(!this._opened)
         {
            return;
         }
         Tween.kill(this);
         Tween.to(this,
            {
               "y":-this._h - 6,
               "alpha":0
            },(e) && e is TimerEvent?30 * 1.5:30 * 0.8,"easeOutQuart",(e) && e is TimerEvent?null:this.dispatchClose,null,function():void
         {
            y = Math.round(y);
            dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
         });
         this._opened = false;
      }
      
      private function dispatchClose(... rest) : void {
         dispatchEvent(new Event(Event.CLOSE));
      }
      
      public function get panelHeight() : Number {
         return Math.max(0,(y + height) * scaleY);
      }
      
      private function resetIdleTimer(... rest) : void {
         this.idleTimer.reset();
         this.idleTimer.start();
      }
      
      private function createOfferingItem() : Sprite {
         var _loc3_:Button = null;
         var _loc1_:Sprite = new Sprite();
         var _loc2_:Label = new Label();
         _loc3_ = new Button();
         var _loc4_:Shape = new Shape();
         _loc1_.addChild(_loc2_);
         _loc2_.textFormat = new TextFormat(ViewerUI.baseFontName,13,11184810);
         _loc2_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc2_.overSample = !ViewerUI.baseFontEmbedded;
         _loc2_.text = this.offeringText;
         _loc2_.filters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
         _loc2_.name = "lbl";
         _loc2_.y = Math.round((this._h - _loc2_.height) / 2);
         _loc1_.addChild(_loc3_);
         _loc3_.textFormat = new TextFormat(ViewerUI.baseFontName,13,ViewerUI.style.linkColor);
         _loc3_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc3_.overSample = !ViewerUI.baseFontEmbedded;
         _loc3_.glowColor = ViewerUI.style.linkColor;
         _loc3_.filters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
         _loc3_.name = "btn";
         _loc3_.addEventListener(MouseEvent.CLICK,this.onFindOutMoreClick);
         _loc1_.addChild(_loc4_);
         _loc4_.graphics.beginFill(0,0.2);
         _loc4_.graphics.drawRect(0,0,1,this._h);
         _loc4_.graphics.endFill();
         _loc4_.graphics.beginFill(16777215,0.06);
         _loc4_.graphics.drawRect(1,0,1,this._h);
         _loc4_.graphics.endFill();
         _loc4_.name = "separator";
         return _loc1_;
      }
      
      private function updateOfferingItems(param1:String = null, param2:String = null) : void {
         var _loc4_:Label = null;
         var _loc5_:Button = null;
         var _loc6_:Shape = null;
         var _loc3_:* = 0;
         while(_loc3_ < this.offeringItems.length)
         {
            _loc4_ = this.offeringItems[_loc3_].getChildByName("lbl") as Label;
            if(param1)
            {
               _loc4_.text = param1;
            }
            _loc5_ = this.offeringItems[_loc3_].getChildByName("btn") as Button;
            if(param2)
            {
               _loc5_._hitArea = null;
               _loc5_.label = param2;
               _loc5_._hitArea = new Rectangle(0,0,_loc5_.width + 16,this._h);
            }
            _loc6_ = this.offeringItems[_loc3_].getChildByName("separator") as Shape;
            _loc4_.x = 0;
            _loc6_.x = _loc4_.x + _loc4_.width + 8;
            _loc5_.x = _loc6_.x + _loc6_.width;
            this.offeringItems[_loc3_].x = _loc3_?this.offeringItems[_loc3_ - 1].x + this.offeringItems[_loc3_ - 1].width + 8:0;
            _loc3_++;
         }
      }
      
      private function onMouseOver(param1:MouseEvent) : void {
         if(param1.target == this.watchNowBtn)
         {
            this.watchNowBtn.enabled = true;
         }
         this.isOverText = true;
      }
      
      private function onMouseOut(param1:Event = null) : void {
         this.watchNowBtn.enabled = false;
         this.watchNowBtn.mouseEnabled = true;
         this.isOverText = false;
      }
      
      private function onFindOutMoreClick(param1:Event) : void {
         this.echo("onFindOutMoreClick()");
         var _loc2_:String = Logic.instance.media.pageUrl;
         if((This.onSite(_loc2_) && !(_loc2_.indexOf("/channel/") == -1)) && (Shell.hasInstance) && (Shell.jsApiEnabled))
         {
            Shell.instance.call("onHideAdClick");
         }
         else
         {
            navigateToURL(new URLRequest(this.offeringUrl),"_blank");
         }
         if(Logic.hasInstance)
         {
            Logic.instance.media.midEvent(1);
         }
         if((stage.displayState) && !(stage.displayState == "normal"))
         {
            try
            {
               stage.displayState = "normal";
            }
            catch(e:*)
            {
            }
         }
      }
      
      private function onWatchNowClick(param1:MouseEvent) : void {
         this.echo("onWatchNowClick()");
         this.watchNowBtn.enabled = mouseEnabled = mouseChildren = false;
         if(Logic.hasInstance)
         {
            Logic.instance.media.midEvent();
         }
         dispatchEvent(new Event(ViewerUI.MIDROLL_DESTROY,true));
      }
      
      private function setTime() : String {
         var _loc1_:Date = new Date();
         _loc1_.setTime(this._time * 1000);
         _loc1_.setHours(_loc1_.getUTCHours());
         return this.countdownText.split("#time#").join(_loc1_.getMinutes() + ":" + StringUtils.leadingZero(_loc1_.getSeconds()));
      }
      
      public function set time(param1:uint) : void {
         if(this._time != param1)
         {
            this.echo("set time: " + param1);
            this._time = param1;
            if(this.countdownLbl)
            {
               this.countdownLbl.text = this.setTime();
            }
            this.resize();
         }
      }
      
      public function set offering(param1:Object) : void {
         if(!param1)
         {
            return;
         }
         this.echo("set offering >> " + [param1.ad,param1.link,param1.url]);
         if(param1.ad)
         {
            this.offeringText = param1.ad.split(" - #learnmore#").join("").split("#learnmore#").join("");
         }
         this.updateOfferingItems(this.offeringText,param1.link?param1.link:null);
         if(param1.url)
         {
            this.offeringUrl = param1.url;
         }
         this.resize();
      }
      
      private function scrollOffering(param1:Event) : void {
         if(!this.isOverText && (this._opened))
         {
            this.offeringItems[0].x = --this.offeringItems[0].x % (this.offeringItems[0].width + 16);
            this.offeringItems[1].x = this.offeringItems[0].x + this.offeringItems[0].width + 16;
         }
      }
      
      public function resize(... rest) : void {
         var _loc2_:Matrix = null;
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         scaleX = scaleY = ViewerUI.ratio;
         this._width = this._width / ViewerUI.ratio;
         this.countdownLbl.x = 8;
         this.countdownLbl.y = Math.round((this._h - this.countdownLbl.height) / 2);
         var _loc3_:Number = Math.abs(this.countdownLbl.x + this.countdownLbl.width + 8 - this.watchNowBtn.x);
         if(_loc3_ > 2)
         {
            this.watchNowBtn.x = this.countdownLbl.x + this.countdownLbl.width + 8;
         }
         this.watchNowBtn.y = Math.round((this._h - this.watchNowBtn.height) / 2);
         var _loc4_:Number = Math.max(0,this._width - (this.watchNowBtn.x + this.watchNowBtn.width + 24));
         var _loc5_:Number = this.offeringItems[0].width;
         removeEventListener(Event.ENTER_FRAME,this.scrollOffering);
         if(this.offeringHolder.contains(this.offeringItems[1]))
         {
            this.offeringHolder.removeChild(this.offeringItems[1]);
         }
         if(_loc4_ == 0)
         {
            this.offeringMask.graphics.clear();
         }
         else if(_loc5_ > _loc4_)
         {
            if(!this.offeringHolder.contains(this.offeringItems[1]))
            {
               this.offeringHolder.addChild(this.offeringItems[1]);
            }
            this.offeringHolder.x = this.offeringMask.x = this.watchNowBtn.x + this.watchNowBtn.width + 24;
            this.offeringMask.graphics.clear();
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(16,this._h,0,-16);
            this.offeringMask.graphics.beginGradientFill(GradientType.LINEAR,[0,0],[0,1],[0,255],_loc2_);
            this.offeringMask.graphics.drawRect(-16,0,16,this._h);
            this.offeringMask.graphics.endFill();
            this.offeringMask.graphics.beginFill(16737792,1);
            this.offeringMask.graphics.drawRect(0,0,_loc4_ - 16,this._h);
            this.offeringMask.graphics.endFill();
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(16,this._h,0,_loc4_ - 16);
            this.offeringMask.graphics.beginGradientFill(GradientType.LINEAR,[0,0],[1,0],[0,255],_loc2_);
            this.offeringMask.graphics.drawRect(_loc4_ - 16,0,16,this._h);
            this.offeringMask.graphics.endFill();
            addEventListener(Event.ENTER_FRAME,this.scrollOffering);
         }
         else
         {
            this.offeringHolder.x = this.offeringMask.x = this._width - _loc5_;
            this.offeringMask.graphics.clear();
            this.offeringMask.graphics.beginFill(16737792,1);
            this.offeringMask.graphics.drawRect(0,0,_loc5_,this._h);
            this.offeringMask.graphics.endFill();
            this.offeringItems[0].x = 0;
         }
         
         _loc2_ = new Matrix();
         _loc2_.createGradientBox(this._width,this._h,90 * Math.PI / 180);
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(1710618);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         var _loc6_:BitmapData = new BitmapData(2,2,true,3.42439478E9);
         _loc6_.setPixel32(0,0,3.423605008E9);
         _loc6_.setPixel32(1,1,3.423605008E9);
         this.bg.graphics.beginBitmapFill(_loc6_,new Matrix(1 / scaleX,0,0,1 / scaleY));
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         this.bg.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.11,0.11],[0,255],_loc2_);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
      }
      
      public function destroy() : void {
         var _loc2_:Button = null;
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.ENTER_FRAME,this.open);
         if(ViewerUI.touchMode)
         {
            stage.removeEventListener(MouseEvent.CLICK,this.open);
         }
         else
         {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.open);
         }
         this.idleTimer.reset();
         this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         removeEventListener(Event.ENTER_FRAME,this.scrollOffering);
         var _loc1_:* = 0;
         while(_loc1_ < this.offeringItems.length)
         {
            _loc2_ = this.offeringItems[_loc1_].getChildByName("btn") as Button;
            _loc2_.removeEventListener(MouseEvent.CLICK,this.onFindOutMoreClick);
            _loc2_.destroy();
            _loc1_++;
         }
         this.watchNowBtn.removeEventListener(MouseEvent.CLICK,this.onWatchNowClick);
         this.watchNowBtn.destroy();
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(... rest) : void {
         var _loc3_:Button = null;
         var _loc2_:* = 0;
         while(_loc2_ < this.offeringItems.length)
         {
            _loc3_ = this.offeringItems[_loc2_].getChildByName("btn") as Button;
            _loc3_.textFormat = new TextFormat(ViewerUI.baseFontName,13,ViewerUI.style.linkColor);
            _loc3_.glowColor = ViewerUI.style.linkColor;
            _loc3_._hitArea = null;
            _loc3_._hitArea = new Rectangle(0,0,_loc3_.width + 16,this._h);
            _loc2_++;
         }
      }
      
      private function echo(param1:*) : void {
         Debug.echo("[ MidrollNotification ] " + param1.toString());
      }
   }
}
