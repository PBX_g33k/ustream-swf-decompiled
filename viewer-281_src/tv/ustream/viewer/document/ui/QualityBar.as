package tv.ustream.viewer.document.ui
{
   import tv.ustream.viewer.document.ui.icons.Dot;
   import tv.ustream.tween2.Color;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.geom.Rectangle;
   import tv.ustream.tween2.Tween;
   import flash.display.Sprite;
   import flash.text.TextFormat;
   import tv.ustream.localization.Locale;
   import flash.filters.DropShadowFilter;
   import flash.events.MouseEvent;
   import flash.events.Event;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   
   public class QualityBar extends OptionsPanel
   {
      
      public function QualityBar() {
         this.qualityList = [];
         super();
         if(ViewerUI.touchMode)
         {
            triggerMode = TRIGGER_CLICK_HIDE;
         }
         else
         {
            triggerMode = TRIGGER_OVER_HIDE;
         }
      }
      
      private var qualityLabel:Button;
      
      private var _quality:uint;
      
      private var _status:int = 2;
      
      public var qualityList:Array;
      
      private var loader:LoaderBar;
      
      private var dot:Dot;
      
      private var _currentAutoQuality:int = -1;
      
      override protected function buildUI() : void {
         this.loader = new LoaderBar(9,2);
      }
      
      public function get quality() : uint {
         return this._quality;
      }
      
      public function set quality(param1:uint) : void {
         if(!this.qualityList["q" + param1])
         {
            return;
         }
         this._quality = param1;
         var _loc2_:* = 0;
         while(_loc2_ < this.qualityList.length)
         {
            this.qualityList[_loc2_].btn.mouseEnabled = !(this.qualityList[_loc2_].quality == this._quality);
            if(this.qualityList[_loc2_].quality == this._quality)
            {
               this.qualityList[_loc2_].btn.textGradientOverlay = [Color.setBrightness(ViewerUI.style.color,0.15),Color.setBrightness(ViewerUI.style.color,-0.15)];
               this.qualityList[_loc2_].btn.textGradientOverlayAlpha = [1,1];
               this.qualityList[_loc2_].btn.glowColor = Color.setBrightness(ViewerUI.style.color,0.15);
            }
            else
            {
               this.qualityList[_loc2_].btn.textGradientOverlay = [16777215,0];
               this.qualityList[_loc2_].btn.textGradientOverlayAlpha = [0.33,0.33];
               this.qualityList[_loc2_].btn.glowColor = 16777215;
            }
            this.qualityList[_loc2_].btn.text = this.qualityList[_loc2_].btn.text;
            this.qualityList[_loc2_].btn._hitArea = null;
            this.qualityList[_loc2_].btn._hitArea = new Rectangle(0,0,bg.width,this.qualityList[_loc2_].btn.height);
            _loc2_++;
         }
         if(this.qualityLabel.contains(this.loader))
         {
            this.qualityLabel.removeChild(this.loader);
         }
         this.qualityLabel.label = this.qualityList["q" + param1].label;
         this.qualityLabel._hitArea = null;
         this.qualityLabel._hitArea = new Rectangle(0,0,bg.width,this.qualityLabel.height);
         this.loader.x = Math.round(this.qualityLabel.width / 2);
         this.loader.y = Math.round(this.qualityLabel.height / 2);
         this.qualityLabel.addChild(this.loader);
      }
      
      public function get status() : int {
         return this._status;
      }
      
      public function set status(param1:int) : void {
         var _loc2_:* = 0;
         this._status = param1;
         switch(param1)
         {
            case 0:
               _loc2_ = 0;
               while(_loc2_ < this.qualityList.length)
               {
                  this.qualityList[_loc2_].btn.mouseEnabled = false;
                  _loc2_++;
               }
               this.qualityLabel.mouseEnabled = false;
               Tween.to(this.qualityLabel.txtTarget,{"alpha":0},30 * 0.3);
               Tween.to(this.loader,{"alpha":1},30 * 0.3);
               break;
            case 1:
            default:
               _loc2_ = 0;
               while(_loc2_ < this.qualityList.length)
               {
                  if(this.qualityList[_loc2_].quality != this.quality)
                  {
                     this.qualityList[_loc2_].btn.mouseEnabled = true;
                  }
                  _loc2_++;
               }
               this.qualityLabel.mouseEnabled = true;
               Tween.to(this.qualityLabel.txtTarget,{"alpha":1},30 * 0.3);
               Tween.to(this.loader,{"alpha":0},30 * 0.3);
         }
      }
      
      public function get currentAutoQuality() : int {
         return this._currentAutoQuality;
      }
      
      public function set currentAutoQuality(param1:int) : void {
         var _loc3_:Sprite = null;
         this._currentAutoQuality = param1;
         var _loc2_:uint = 0;
         while(_loc2_ < this.qualityList.length)
         {
            _loc3_ = this.qualityList[_loc2_].btn.getChildByName("dot");
            if(_loc3_)
            {
               if(this.qualityList[_loc2_].quality != this._currentAutoQuality)
               {
                  _loc3_.visible = false;
               }
               else
               {
                  _loc3_.visible = true;
               }
            }
            _loc2_++;
         }
      }
      
      public function addQualities(param1:Array) : void {
         var _loc4_:TextFormat = null;
         var _loc5_:Button = null;
         var _loc6_:Dot = null;
         this.removeQualities();
         if(!this.qualityLabel)
         {
            addChild(this.qualityLabel = new Button());
            this.qualityLabel.colorizable = true;
            switch(Locale.instance.language)
            {
               case "ja_JP":
               case "ko_KR":
               case "ru_RU":
               case "zh_CN":
               case "zh_TW":
               case "pt_BR":
               case "hu_HU":
                  _loc4_ = new TextFormat(ViewerUI.baseFontName,15,16777215);
                  if(!ViewerUI.baseFontEmbedded)
                  {
                     _loc4_.bold = true;
                  }
                  if(Locale.instance.language == "hu_HU" || Locale.instance.language == "ru_RU")
                  {
                     _loc4_.italic = true;
                  }
                  this.qualityLabel.textFormat = _loc4_;
                  this.qualityLabel.embedFonts = ViewerUI.baseFontEmbedded;
                  this.qualityLabel.overSample = !ViewerUI.baseFontEmbedded;
                  break;
               default:
                  this.qualityLabel.textFormat = new TextFormat("MyriadBoldItalic",15,16777215);
                  this.qualityLabel.embedFonts = true;
            }
            this.qualityLabel.filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
            this.qualityLabel.textGradientOverlay = [16777215,0];
            this.qualityLabel.textGradientOverlayAlpha = [0.33,0.33];
            this.qualityLabel.padding = 6;
            this.qualityLabel.bgHeight = 28;
            this.qualityLabel.bgColor = 0;
            this.qualityLabel.bgAlpha = 0;
            this.qualityLabel.bgCornerRadius = 0;
            this.qualityLabel.label = " ";
            this.qualityLabel._hitArea = new Rectangle(0,0,20,this.qualityLabel.height);
            this.qualityLabel.y = Math.round(-this.qualityLabel.height / 2);
            this.qualityLabel.name = "label";
            trigger = this.qualityLabel;
         }
         var _loc2_:* = false;
         var _loc3_:* = 0;
         while(_loc3_ < param1.length)
         {
            _loc5_ = new Button();
            addChild(_loc5_);
            _loc5_.name = "btn_" + _loc3_;
            switch(Locale.instance.language)
            {
               case "ja_JP":
               case "ko_KR":
               case "ru_RU":
               case "zh_CN":
               case "zh_TW":
               case "pt_BR":
               case "hu_HU":
                  _loc4_ = new TextFormat(ViewerUI.baseFontName,15,16777215);
                  if(!ViewerUI.baseFontEmbedded)
                  {
                     _loc4_.bold = true;
                  }
                  if(Locale.instance.language == "hu_HU" || Locale.instance.language == "ru_RU")
                  {
                     _loc4_.italic = true;
                  }
                  _loc5_.textFormat = _loc4_;
                  _loc5_.embedFonts = ViewerUI.baseFontEmbedded;
                  _loc5_.overSample = !ViewerUI.baseFontEmbedded;
                  break;
               default:
                  _loc5_.textFormat = new TextFormat("MyriadBoldItalic",15,16777215);
                  _loc5_.embedFonts = true;
            }
            _loc5_.filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
            _loc5_.textGradientOverlay = [16777215,0];
            _loc5_.textGradientOverlayAlpha = [0.33,0.33];
            _loc5_.padding = 6;
            _loc5_.bgHeight = 28;
            _loc5_.bgColor = 16737792;
            _loc5_.bgAlpha = 0;
            _loc5_.bgCornerRadius = 0;
            _loc5_.label = param1[_loc3_].label;
            if(param1[_loc3_].quality != 4096)
            {
               _loc6_ = new Dot();
               _loc6_.name = "dot";
               _loc6_.x = 5.5;
               _loc6_.y = _loc5_.height / 2;
               _loc6_.visible = false;
               _loc5_.addChild(_loc6_);
            }
            if(param1[_loc3_].quality == 4096)
            {
               _loc2_ = true;
            }
            this.qualityList.push(
               {
                  "label":param1[_loc3_].label,
                  "quality":param1[_loc3_].quality,
                  "btn":_loc5_
               });
            this.qualityList[param1[_loc3_].label] = this.qualityList["q" + param1[_loc3_].quality] = this.qualityList[this.qualityList.length - 1];
            _loc5_.addEventListener(MouseEvent.CLICK,this.onClick);
            addTweenListItem(_loc5_);
            _loc3_++;
         }
         this.setList();
         prepareTweenList();
      }
      
      private function removeQualities() : void {
         var _loc1_:Object = null;
         var _loc2_:Button = null;
         while(this.qualityList.length)
         {
            _loc1_ = this.qualityList.pop();
            delete this.qualityList[_loc1_.label];
            delete this.qualityList["q" + _loc1_.quality];
            _loc2_ = _loc1_.btn;
            removeTweenListItem(_loc2_);
            _loc2_.removeEventListener(MouseEvent.CLICK,this.onClick);
            removeChild(_loc2_);
            _loc2_.destroy();
            _loc2_ = null;
         }
         this.setList();
         prepareTweenList();
      }
      
      private function setList() : void {
         var _loc3_:Button = null;
         var _loc1_:Number = 0;
         var _loc2_:* = 0;
         while(_loc2_ < this.qualityList.length)
         {
            _loc1_ = Math.max(Math.round(this.qualityList[_loc2_].btn.width),_loc1_);
            _loc2_++;
         }
         this.qualityList.reverse();
         _loc2_ = 0;
         while(_loc2_ < this.qualityList.length)
         {
            _loc3_ = this.qualityList[_loc2_].btn;
            _loc3_.x = 0;
            _loc3_._hitArea = null;
            _loc3_._hitArea = new Rectangle(0,0,_loc1_,_loc3_.bgHeight);
            if(_loc2_ > 0)
            {
               _loc3_.y = this.qualityList[_loc2_ - 1].btn.y - _loc3_.height;
            }
            else
            {
               _loc3_.y = -Math.round(_loc3_.height / 2) - 7;
            }
            _loc2_++;
         }
         this.qualityList.reverse();
         setBg(_loc1_ + 6,(this.qualityList[0]) && (this.qualityList[0].btn)?this.qualityList[0].btn.y - 6:-6);
      }
      
      private function onClick(param1:MouseEvent) : void {
         this.quality = this.qualityList[Button(param1.currentTarget).label].quality;
         dispatchEvent(new Event(ViewerUI.MBR_CHANGE,true));
      }
      
      override public function destroy() : void {
         this.removeQualities();
         this.qualityLabel.removeEventListener(MouseEvent.MOUSE_OVER,this.onQualityLabelOver);
         super.destroy();
      }
      
      private function onQualityLabelOver(param1:Event) : void {
         this.qualityLabel.tooltip = null;
         this.qualityLabel.removeEventListener(MouseEvent.MOUSE_OVER,this.onQualityLabelOver);
      }
      
      public function blink() : void {
         this.qualityLabel.blink();
      }
      
      public function showTooltip(param1:int = 0) : void {
         this.qualityLabel.tooltip = Locale.instance.label(ViewerLabels.tooltipQuality);
         this.qualityLabel.showTooltip(param1);
         this.qualityLabel.addEventListener(MouseEvent.MOUSE_OVER,this.onQualityLabelOver);
         var _loc2_:Timer = new Timer(5000,1);
         _loc2_.addEventListener(TimerEvent.TIMER_COMPLETE,this.onBlinkTimer);
         _loc2_.start();
      }
      
      public function hideTooltip() : void {
         this.qualityLabel.hideTooltip();
      }
      
      private function onBlinkTimer(param1:TimerEvent) : void {
         param1.target.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onBlinkTimer);
         this.hideTooltip();
         var _loc2_:Timer = new Timer(600,1);
         _loc2_.addEventListener(TimerEvent.TIMER_COMPLETE,this.onBlinkDelay);
         _loc2_.start();
      }
      
      private function onBlinkDelay(param1:TimerEvent) : void {
         param1.target.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onBlinkDelay);
         this.qualityLabel.tooltip = null;
      }
      
      override protected function updateStyle(... rest) : void {
         var _loc2_:* = 0;
         while(_loc2_ < this.qualityList.length)
         {
            if(this.qualityList[_loc2_].quality == this._quality)
            {
               this.qualityList[_loc2_].btn.textGradientOverlay = [Color.setBrightness(ViewerUI.style.color,0.15),Color.setBrightness(ViewerUI.style.color,-0.15)];
               this.qualityList[_loc2_].btn.textGradientOverlayAlpha = [1,1];
               this.qualityList[_loc2_].btn.glowColor = Color.setBrightness(ViewerUI.style.color,0.15);
               this.qualityList[_loc2_].btn.text = this.qualityList[_loc2_].btn.text;
               this.qualityList[_loc2_].btn._hitArea = null;
               this.qualityList[_loc2_].btn._hitArea = new Rectangle(0,0,bg.width,this.qualityList[_loc2_].btn.height);
            }
            _loc2_++;
         }
      }
      
      override public function set visible(param1:Boolean) : void {
         if(!param1)
         {
            this.qualityLabel.blink(0);
            if(this.qualityLabel.tooltipInstance)
            {
               this.qualityLabel.hideTooltip();
            }
         }
         super.visible = param1;
      }
   }
}
