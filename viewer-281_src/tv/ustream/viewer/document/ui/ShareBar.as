package tv.ustream.viewer.document.ui
{
   import tv.ustream.viewer.logic.modules.Share;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.localization.Locale;
   import flash.filters.DropShadowFilter;
   import flash.geom.Rectangle;
   import flash.events.Event;
   import tv.ustream.viewer.document.ui.icons.SocialIcon;
   import flash.events.MouseEvent;
   import flash.net.URLVariables;
   import tv.ustream.viewer.logic.Logic;
   import flash.display.StageDisplayState;
   import flash.system.System;
   import tv.ustream.tools.This;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.tools.Debug;
   import flash.external.ExternalInterface;
   
   public class ShareBar extends OptionsPanel
   {
      
      public function ShareBar() {
         super();
         if(ViewerUI.touchMode)
         {
            triggerMode = TRIGGER_CLICK_HIDE;
         }
         else
         {
            triggerMode = TRIGGER_OVER_HIDE;
         }
      }
      
      public static const EMBEDPANE:String = "embedPane";
      
      public static const LINK:String = "link";
      
      private var _module:Share;
      
      private var shareBtn:Button;
      
      private var serviceList:Array;
      
      private var buttonList:Array;
      
      override protected function buildUI() : void {
         var _loc1_:TextFormat = null;
         addChild(this.shareBtn = new Button());
         this.shareBtn.colorizable = true;
         switch(Locale.instance.language)
         {
            case "ko_KR":
            case "ja_JP":
            case "ru_RU":
            case "zh_CN":
            case "zh_TW":
            case "hu_HU":
               _loc1_ = new TextFormat(ViewerUI.baseFontName,13,16777215);
               if(!ViewerUI.baseFontEmbedded)
               {
                  _loc1_.bold = true;
               }
               if(Locale.instance.language == "hu_HU" || Locale.instance.language == "ru_RU")
               {
                  _loc1_.italic = true;
               }
               this.shareBtn.textFormat = _loc1_;
               this.shareBtn.embedFonts = ViewerUI.baseFontEmbedded;
               this.shareBtn.overSample = !ViewerUI.baseFontEmbedded;
               break;
            default:
               this.shareBtn.textFormat = new TextFormat("MyriadBoldItalic",12,16777215);
               this.shareBtn.embedFonts = true;
         }
         this.shareBtn.filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
         this.shareBtn.textGradientOverlay = [16777215,0];
         this.shareBtn.textGradientOverlayAlpha = [0.33,0.33];
         this.shareBtn.padding = 4;
         this.shareBtn.bgHeight = 20;
         this.shareBtn.bgColor = 16737792;
         this.shareBtn.bgAlpha = 0;
         if(this.shareBtn.embedFonts)
         {
            this.shareBtn.label = Locale.instance.label(ViewerLabels.labelShare).toUpperCase();
         }
         else
         {
            this.shareBtn.label = Locale.instance.label(ViewerLabels.labelShare);
         }
         this.shareBtn._hitArea = new Rectangle(0,0,this.shareBtn.width,this.shareBtn.height);
         this.shareBtn.x = 0;
         this.shareBtn.y = -Math.round(this.shareBtn.height / 2);
         trigger = this.shareBtn;
         setBg(this.shareBtn.width,trigger.y - 12);
      }
      
      public function get module() : Share {
         return this._module;
      }
      
      public function set module(param1:Share) : void {
         if(this._module)
         {
            this._module.removeEventListener("update",this.onModuleUpdate);
         }
         this._module = param1;
         if(this._module)
         {
            this._module.addEventListener("update",this.onModuleUpdate);
            if((this._module.items) && (this._module.items.length))
            {
               this.onModuleUpdate();
            }
         }
      }
      
      private function onModuleUpdate(param1:Event = null) : void {
         var _loc2_:uint = 0;
         var _loc3_:Button = null;
         this.disposeButtons();
         if((this._module) && (this._module.items) && (this._module.items.length))
         {
            if(!this.serviceList)
            {
               this.serviceList = new Array();
            }
            if(!this.buttonList)
            {
               this.buttonList = new Array();
            }
            _loc2_ = 0;
            for(;_loc2_ < this._module.items.length;_loc2_++)
            {
               this.serviceList[this._module.items[_loc2_].name] = this._module.items[_loc2_];
               _loc3_ = new Button();
               _loc3_.name = this._module.items[_loc2_].name;
               switch(this._module.items[_loc2_].name)
               {
                  case "twitter":
                     _loc3_.icon = new SocialIcon("twitter");
                     break;
                  case "facebook":
                     _loc3_.icon = new SocialIcon("fb");
                     break;
                  case "mail":
                     _loc3_.icon = new SocialIcon("link");
                     break;
                  case "embed":
                     _loc3_.icon = new SocialIcon("embed");
                     break;
                  case "mixi":
                     _loc3_.icon = new SocialIcon("mixi");
                     break;
                  default:
                     continue;
               }
               _loc3_.addEventListener(MouseEvent.CLICK,this.onButtonClick);
               this.buttonList.push(_loc3_);
            }
            this.buttonList.reverse();
            _loc2_ = 0;
            while(_loc2_ < this.buttonList.length)
            {
               this.buttonList[_loc2_]._hitArea = new Rectangle(0,0,this.shareBtn.width,28);
               this.buttonList[_loc2_].x = Math.round((this.shareBtn.width - this.buttonList[_loc2_].width) / 2);
               if(_loc2_ > 0)
               {
                  this.buttonList[_loc2_].y = this.buttonList[_loc2_ - 1].y - this.buttonList[_loc2_].height;
               }
               else
               {
                  this.buttonList[_loc2_].y = -Math.round(this.buttonList[_loc2_].height / 2) - 7;
               }
               addChild(this.buttonList[_loc2_]);
               addTweenListItem(this.buttonList[_loc2_]);
               _loc2_++;
            }
            setBg(this.shareBtn.width,this.buttonList[this.buttonList.length - 1].y - Math.min(12,Math.round((this.shareBtn.width - this.buttonList[this.buttonList.length - 1].icon.width) / 2)));
            this.buttonList.reverse();
            prepareTweenList();
         }
      }
      
      private function disposeButtons(param1:Boolean = true) : void {
         if(!this.buttonList)
         {
            return;
         }
         var _loc2_:* = 0;
         while(_loc2_ < this.buttonList.length)
         {
            this.buttonList[_loc2_].removeEventListener(MouseEvent.CLICK,this.onButtonClick);
            (this.buttonList[_loc2_] as Button).destroy();
            removeTweenListItem(this.buttonList[_loc2_]);
            _loc2_++;
         }
         if(param1)
         {
            while((this.buttonList) && (this.buttonList.length))
            {
               if(contains(this.buttonList[0]))
               {
                  removeChild(this.buttonList[0]);
               }
               this.buttonList.splice(0,1);
            }
         }
      }
      
      private function onButtonClick(param1:MouseEvent) : void {
         var _loc2_:URLVariables = null;
         var _loc3_:String = null;
         var _loc4_:String = null;
         var _loc5_:Object = null;
         var _loc6_:uint = 0;
         var _loc11_:Array = null;
         var _loc12_:String = null;
         var _loc13_:Array = null;
         var _loc14_:String = null;
         var _loc15_:String = null;
         var _loc16_:String = null;
         var _loc17_:Array = null;
         var _loc18_:String = null;
         var _loc19_:String = null;
         var _loc20_:URLVariables = null;
         if(!Logic.hasInstance)
         {
            return;
         }
         var _loc7_:* = false;
         var _loc8_:Array = ["17155991","14765533","17697072"];
         var _loc9_:Boolean = Logic.instance.media.type == "recorded" && (Logic.instance.channel) && !(_loc8_.indexOf(Logic.instance.channel.id) == -1);
         var _loc10_:Boolean = Logic.instance.media.type == "recorded" && !(_loc8_.indexOf(Logic.instance.media.modules.meta.data.ownerChannelId) == -1);
         if(Logic.instance.media.type == "channel" || (_loc9_) || (_loc10_))
         {
            _loc7_ = true;
         }
         if(stage.displayState == StageDisplayState.FULL_SCREEN)
         {
            stage.displayState = StageDisplayState.NORMAL;
         }
         switch(param1.target.name)
         {
            case "embed":
               dispatchEvent(new Event(EMBEDPANE));
               break;
            case "mail":
               _loc11_ = [
                  {
                     "cid":["16061202"],
                     "url":"http://www.apl.tv/bigfoot"
                  },
                  {
                     "cid":["16113109"],
                     "url":"http://explore.org/sharks"
                  },
                  {
                     "cid":["12434787","12434837","12448065","12448106","13773579"],
                     "url":"http://explore.org/polarbears"
                  },
                  {
                     "cid":["15754651"],
                     "url":"http://explore.org/lucky"
                  },
                  {
                     "cid":["15593241"],
                     "url":"http://animalplanet.com/catvidfest"
                  },
                  {
                     "cid":["15498751","14510221","14510237"],
                     "url":"http://explore.org/bees"
                  },
                  {
                     "cid":["11123648"],
                     "url":"http://www.nintendo.com/nintendo-direct"
                  },
                  {
                     "cid":["15498973"],
                     "url":"http://explore.org/belugas"
                  },
                  {
                     "cid":["15419575"],
                     "url":"http://explore.org/penguinchicks"
                  },
                  {
                     "cid":["15433699"],
                     "url":"http://explore.org/belugas"
                  },
                  {
                     "cid":["14879599","15428427"],
                     "url":"http://www.apl.tv/sloths.htm"
                  },
                  {
                     "cid":["14879579","15428435"],
                     "url":"http://www.apl.tv/tamarins.htm"
                  },
                  {
                     "cid":["14812707","15006733"],
                     "url":"http://sharkweek.com/sharkcam"
                  },
                  {
                     "cid":["12762028"],
                     "url":"http://www.apl.tv/highlights.htm"
                  },
                  {
                     "cid":["13027426"],
                     "url":"http://www.apl.tv/calves.htm"
                  },
                  {
                     "cid":["13027427"],
                     "url":"http://www.apl.tv/chicks.htm"
                  },
                  {
                     "cid":["13027441"],
                     "url":"http://www.apl.tv/leaf-cutter-ants.htm"
                  },
                  {
                     "cid":["13027444"],
                     "url":"http://www.apl.tv/cockroaches.htm"
                  },
                  {
                     "cid":["13628076"],
                     "url":"http://www.apl.tv/african-penguins.htm"
                  },
                  {
                     "cid":["13027446"],
                     "url":"http://www.apl.tv/bird.htm"
                  },
                  {
                     "cid":["13628077"],
                     "url":"http://www.apl.tv/pacific-reef.htm"
                  },
                  {
                     "cid":["13027422"],
                     "url":"http://www.apl.tv/puppies.htm"
                  },
                  {
                     "cid":["13027369"],
                     "url":"http://www.apl.tv/kittens.htm"
                  },
                  {
                     "cid":["14812715","15006737"],
                     "url":"http://www.apl.tv/black-tip-reef.htm"
                  },
                  {
                     "cid":["14839273","15006743"],
                     "url":"http://www.apl.tv/parakeets.htm"
                  },
                  {
                     "cid":["14225913"],
                     "url":"http://www.apl.tv/tanked.htm"
                  },
                  {
                     "cid":["14420561"],
                     "url":"http://www.apl.tv/specials.htm"
                  }];
               _loc12_ = Logic.instance.media.url;
               if((this.serviceList.hasOwnProperty("mail")) && (this.serviceList.mail.hasOwnProperty("customUrl")))
               {
                  _loc12_ = this.serviceList.mail.customUrl;
               }
               else
               {
                  _loc6_ = 0;
                  while(_loc6_ < _loc11_.length)
                  {
                     if((_loc11_[_loc6_].cid as Array).indexOf(Logic.instance.media.id) > -1)
                     {
                        _loc12_ = _loc11_[_loc6_].url;
                     }
                     _loc6_++;
                  }
               }
               System.setClipboard(_loc12_);
               dispatchEvent(new Event(LINK));
               break;
            case "twitter":
               _loc2_ = new URLVariables();
               if(this.serviceList.twitter.hasOwnProperty("shortenedUrl"))
               {
                  _loc3_ = this.serviceList.twitter.shortenedUrl;
               }
               if(this.serviceList.twitter.hasOwnProperty("customUrl"))
               {
                  _loc3_ = this.serviceList.twitter.customUrl;
               }
               if(Logic.instance.scid)
               {
                  _loc6_ = _loc3_.lastIndexOf(".am/");
                  _loc3_ = _loc3_.substr(0,_loc6_ + 4) + "~" + Logic.instance.scid + "," + (_loc3_.indexOf("/",_loc6_ + 4) != -1?_loc3_.substring(_loc6_ + 4,_loc3_.indexOf("/",_loc6_ + 4)):_loc3_.substr(_loc6_ + 4));
               }
               if(Logic.instance.media.type == "channel")
               {
                  _loc2_.status = Locale.instance.label(ViewerLabels.messageTweetLive,
                     {
                        "BROADCASTER":Logic.instance.media.modules.meta.username,
                        "LINK":_loc3_
                     });
               }
               else
               {
                  _loc2_.status = Locale.instance.label(ViewerLabels.messageTweetRecorded,
                     {
                        "MEDIANAME":Logic.instance.media.title,
                        "LINK":_loc3_
                     });
               }
               if(Logic.instance.media.type == "channel" || Logic.instance.media.type == "recorded")
               {
                  _loc13_ = [
                     {
                        "cid":["16061202"],
                        "url":"I\'m watching Bigfoot, live on #APLIVE! Join me http://www.apl.tv/bigfoot"
                     },
                     {
                        "cid":["16113109"],
                        "url":"I’m watching the Shark Lagoon Cam on @exploreorg, streaming live from @AquariumPacific: http://explore.org/sharks"
                     },
                     {
                        "cid":["12434787","12434837","12448065","12448106","13773579"],
                        "url":"I’m watching the @exploreorg #polarbearcam, streaming live from the arctic tundra @polarbears: http://explore.org/polarbears",
                        "heritable":true
                     },
                     {
                        "cid":["15754651"],
                        "url":"Meet Lucky, the inspiration behind @dogblessyou, a community that helps pair #servicedogs with veterans: http://explore.org/lucky"
                     },
                     {
                        "cid":["15593241"],
                        "url":"I\'m watching the Internet Cat Video Festival LIVE right now. Join me: http://animalplanet.com/catvidfest via @animalplanet #catvidfest"
                     },
                     {
                        "cid":["15498751","14510221","14510237","13773579"],
                        "url":"I\'m watching Honey Bee Cam on @exploreorg, streaming live from Bavaria, Germany: http://explore.org/bees"
                     },
                     {
                        "cid":["11123648"],
                        "url":"http://www.nintendo.com/nintendo-direct"
                     },
                     {
                        "cid":["15498973"],
                        "url":"I\'m watching the @exploreorg #belugacam, streaming live from the Canadian arctic: http://explore.org/belugas"
                     },
                     {
                        "cid":["15419575"],
                        "url":"I\'m watching the #penguincam on @exploreorg, streaming live from the @aquariumpacific: http://explore.org/penguinchicks"
                     },
                     {
                        "cid":["15433699"],
                        "url":"I\'m watching the @exploreorg #belugacam, streaming live from the Canadian arctic: http://explore.org/belugas"
                     },
                     {
                        "cid":["14879599","15428427"],
                        "url":"I\'m watching Sloths, live on #APLIVE! Join me http://www.apl.tv/sloths.htm"
                     },
                     {
                        "cid":["14879579","15428435"],
                        "url":"I\'m watching Tamarins, live on #APLIVE! Join me http://www.apl.tv/tamarins.htm"
                     },
                     {
                        "cid":["14812707","15006733"],
                        "url":"I\'m watching Blacktip Sharks, LIVE on #SharkWeek! Join me: http://sharkweek.com/sharkcam"
                     },
                     {
                        "cid":["12434958","13773599"],
                        "url":"I\'m watching #bearcam on @exploreorg, streaming live from Brooks Falls, Alaska at @KatmaiNPS: http://explore.org/bears"
                     },
                     {
                        "cid":["12431893"],
                        "url":"I\'m watching #bearcam on @exploreorg, streaming live from Brooks Falls, Alaska at @KatmaiNPS: http://explore.org/live-cams/player/brown-bear-salmon-cam-lower-river"
                     },
                     {
                        "cid":["12434972","13773591"],
                        "url":"I\'m watching #bearcam on @exploreorg, streaming live from Brooks Falls, Alaska at @KatmaiNPS: http://explore.org/live-cams/player/brown-bear-salmon-cam-the-riffles"
                     },
                     {
                        "cid":["14420561","13773635"],
                        "url":"I\'m watching Specials, live on #APLIVE! Join me http://www.apl.tv/specials.htm"
                     },
                     {
                        "cid":["14225913"],
                        "url":"I\'m watching Tanked, live on #APLIVE! Join me http://www.apl.tv/tanked.htm"
                     },
                     {
                        "cid":["25275"],
                        "url":"http://szanalmas.hu",
                        "heritable":true
                     },
                     {
                        "cid":["6148198"],
                        "url":"http://9gag.com"
                     },
                     {
                        "cid":["14120595"],
                        "heritable":true,
                        "url":"Ride along for an inside look at an IZOD IndyCar Series team\'s quest for victory at the Indy 500 at #HondaRacing http://gobu.me/10dWAGd!"
                     },
                     {
                        "cid":["10863094"],
                        "url":"Willkommen zu unserer Nintendo 3DS Direct-Übertragung! Sieh dir die Präsentation an, um Neuigkeiten zum Nintendo 3DS zu erfahren! [link]"
                     },
                     {
                        "cid":["14021399"],
                        "url":"Willkommen zu unserer Nintendo 3DS Direct-Übertragung! Sieh dir die Präsentation an, um Neuigkeiten zum Nintendo 3DS zu erfahren! [link]"
                     },
                     {
                        "cid":["10863081","13773599"],
                        "url":"¡Bienvenido a nuestra presentación Nintendo 3DS Direct! ¡No te pierdas esta emisión con las últimas noticias sobre Nintendo 3DS! [link]"
                     },
                     {
                        "cid":["10863085","13773605"],
                        "url":"Bienvenue dans cette présentation Nintendo 3DS Direct ! Regardez cette diffusion pour connaître les nouveautés à venir sur Nintendo #3DS! [link]"
                     },
                     {
                        "cid":["10863087","13773607"],
                        "url":"Benvenuto al nostro Nintendo 3DS Direct! Guarda la presentazione per tutte le ultime informazioni su Nintendo 3DS! [link]"
                     },
                     {
                        "cid":["10863093","13773615"],
                        "url":"Welkom bij onze Nintendo 3DS Direct-uitzending! Kijk naar de presentatie voor de laatste informatie over Nintendo 3DS-spellen! [link]"
                     },
                     {
                        "cid":["10863082","13773623"],
                        "url":"Assiste à nova Nintendo 3DS Direct para ficares a conhecer as novidades para a Nintendo 3DS. [link]"
                     },
                     {
                        "cid":["10863090","13773627"],
                        "url":"Добро пожаловать на Nintendo 3DS Direct! Смотрите презентацию о новых играх для Nintendo 3DS! [link]"
                     },
                     {
                        "cid":["10863084","13773631"],
                        "url":"Welcome to our Nintendo 3DS Direct broadcast! Watch the presentation for the latest information on Nintendo 3DS! [link]"
                     },
                     {
                        "cid":["12762028"],
                        "url":"I\'m watching TV Highlights, live on #APLIVE! Join me http://www.apl.tv/highlights.htm"
                     },
                     {
                        "cid":["13027426"],
                        "url":"I\'m watching Calves, live on #APLIVE! Join me http://www.apl.tv/calves.htm"
                     },
                     {
                        "cid":["13027427"],
                        "url":"I\'m watching Chicks, live on #APLIVE! Join me http://www.apl.tv/chicks.htm"
                     },
                     {
                        "cid":["13027441"],
                        "url":"I\'m watching Ants, live on #APLIVE! Join me http://www.apl.tv/leaf-cutter-ants.htm"
                     },
                     {
                        "cid":["13027444"],
                        "url":"I\'m watching Cockroaches, live on #APLIVE! Join me http://www.apl.tv/cockroaches.htm"
                     },
                     {
                        "cid":["13628076"],
                        "url":"I\'m watching African Penguins, live on #APLIVE! Join me http://www.apl.tv/african-penguins.htm"
                     },
                     {
                        "cid":["13027446"],
                        "url":"I\'m watching Bird House, live on #APLIVE! Join me http://www.apl.tv/bird.htm"
                     },
                     {
                        "cid":["13628077"],
                        "url":"I\'m watching Pacific Reef, live on #APLIVE! Join me http://www.apl.tv/pacific-reef.htm"
                     },
                     {
                        "cid":["13027422"],
                        "url":"I\'m watching Puppies, live on #APLIVE! Join me http://www.apl.tv/puppies.htm"
                     },
                     {
                        "cid":["13027369"],
                        "url":"I\'m watching Kittens, live on #APLIVE! Join me http://www.apl.tv/kittens.htm"
                     },
                     {
                        "cid":["13358425","13409034"],
                        "url":"I\'m watching #GoldRushLive live at http://discovery.com/live"
                     },
                     {
                        "cid":["12623612","13106887"],
                        "url":"I\'m watching baby Gray Seals on @exploreorg\'s #sealcam, streaming live from the coast of Maine: http://explore.org/seals"
                     },
                     {
                        "cid":["12527141","13033488"],
                        "url":"I\'m watching the Waimea Bay Cam on @exploreorg, streaming live from the Hawaiian Islands: http://explore.org/live-cams/player/hawaii-waimea-bay-cam"
                     },
                     {
                        "cid":["12527097","13033484"],
                        "url":"I\'m watching the Pipeline Cam on @exploreorg, streaming live from the Hawaiian Islands: http://explore.org/live-cams/player/hawaii-pipeline-cam"
                     },
                     {
                        "cid":["12526938","13034091"],
                        "url":"I\'m watching the @WarriorCanineCn #puppycam on @exploreorg: http://explore.org/live-cams/player/service-puppy-cam"
                     },
                     {
                        "cid":["12833951","12830737"],
                        "url":"Transmisión EN VIVO del concierto de Calle 13 desde Puerto Rico - www.Calle13Live.com"
                     },
                     {
                        "cid":["12623451","12623465","12623477","12776266"],
                        "url":"I\'m watching Siku the polar bear live on @exploreorg at the Scandinavian Wildlife Park: [link] #polarbearcam"
                     },
                     {
                        "cid":["12526834","12526826","12526819","12776286"],
                        "url":"I\'m watching Luka and Lynn the polar bear twins live on @exploreorg at the Ouwehand Zoo: [link] #polarbearcam"
                     },
                     {
                        "cid":["11697871","12526746","12776285"],
                        "url":"I\'m watching highlights from @exploreorg\'s live Puffin Cam on Hog Island: [link]"
                     },
                     {
                        "cid":["11378037"],
                        "url":"I\'m watching @exploreorg\'s Osprey Cam, streaming live from Hog Island: #ospreycam [link]"
                     },
                     {
                        "cid":["12434958","12431893","12434972","12776277"],
                        "url":"I\'m watching highlights from @exploreorg\'s live #bearcam at @KatmaiNPS: [link]"
                     },
                     {
                        "cid":["12526704","12526715","10371840","12526722","12776261"],
                        "url":"I\'m watching #pandacam live on @exploreorg at the Bifengxia Panda Center in China: [link]"
                     },
                     {
                        "cid":["12685211","12140637"],
                        "url":"I\'m watching @Discovery\'s Chopper Live \'Backstage Pass\' webcast. #ChopperLive http://bit.ly/ac-live"
                     },
                     {
                        "cid":["12330618","12330597","12330624","12198657","12190212","12465859","12271335","12271341","12271343","12388256","12440802","12503075","12470176","12561802","12567074","12567084","12567088","12776446"],
                        "url":Locale.instance.label(ViewerLabels.messageTweetLive,
                           {
                              "BROADCASTER":Logic.instance.media.title,
                              "LINK":_loc3_
                           })
                     },
                     {
                        "cid":["12435130","12435182","12431938","12312559"],
                        "url":"Live Great Dane #Puppycam - Future Service Dogs for the Disabled on @exploreorg: explore.org/puppies"
                     },
                     {
                        "cid":["12431835","12431867","12452681"],
                        "url":"I\'m watching polar bears live in the arctic tundra on @exploreorg: explore.org/polarbears #polarbearcam"
                     },
                     {
                        "cid":["11569027","6148198"],
                        "url":"I’m watching the Eric & Kathy 36 Hour Radiothon on 101.9 THE MIX Chicago #EK36 http://bit.ly/P1cmSL"
                     },
                     {
                        "cid":["11910281","11977789"],
                        "url":"I\'m listening to 5 new #GLEE songs right now! Come join the GLEE Listening Party - LIVE NOW! [link] #Season4FirstListen"
                     },
                     {
                        "cid":["14812715","15006737"],
                        "url":"I\'m watching Blacktip reef, live on #APLIVE! Join me http://www.apl.tv/black-tip-reef.htm"
                     },
                     {
                        "cid":["14839273","15006743"],
                        "url":"I\'m watching Parakeets, live on #APLIVE! Join me http://www.apl.tv/parakeets.htm"
                     }];
                  _loc4_ = Logic.instance.media.id;
                  if(Logic.instance.media.type == "recorded")
                  {
                     _loc5_ = This.getReference(Logic.instance,"media.modules.meta.data");
                     _loc4_ = (_loc5_) && (_loc5_.ownerChannelId)?_loc5_.ownerChannelId:"0";
                  }
                  if((this.serviceList.hasOwnProperty("twitter") && this.serviceList.twitter.hasOwnProperty("message") && String(this.serviceList.twitter.message)) && (String(this.serviceList.twitter.message).length > 0) && (_loc7_))
                  {
                     _loc2_.status = this.serviceList.twitter.message;
                     _loc2_.status = this.linkIntoMessage(_loc2_.status,_loc3_,"twitter");
                  }
                  else
                  {
                     _loc6_ = 0;
                     while(_loc6_ < _loc13_.length)
                     {
                        if((_loc13_[_loc6_].cid as Array).indexOf(_loc4_) > -1 && ((Logic.instance.media.type == "channel") || (Logic.instance.media.type == "recorded" && _loc13_[_loc6_].heritable)))
                        {
                           _loc2_.status = _loc13_[_loc6_].url;
                           _loc2_.status = this.linkIntoMessage(_loc2_.status,_loc3_,"twitter");
                           break;
                        }
                        _loc6_++;
                     }
                  }
               }
               navigateToURL(new URLRequest("http://twitter.com/home?" + _loc2_.toString()),"_blank");
               break;
            case "facebook":
               _loc2_ = new URLVariables();
               _loc2_.u = "http://www.ustream.tv/";
               if(Logic.instance.scid)
               {
                  _loc2_.u = _loc2_.u + ("schannel/" + Logic.instance.scid + "/");
               }
               if(Logic.instance.media.type == "channel")
               {
                  _loc2_.u = _loc2_.u + ("channel/" + Logic.instance.media.id);
               }
               else if(Logic.instance.media.type == "recorded")
               {
                  _loc2_.u = _loc2_.u + ("recorded/" + Logic.instance.media.id);
               }
               
               _loc14_ = "";
               _loc15_ = "";
               _loc16_ = (this.serviceList.hasOwnProperty("facebook")) && (this.serviceList.facebook.hasOwnProperty("appId"))?this.serviceList.facebook.appId:"";
               if(Logic.instance.media.type == "channel" || Logic.instance.media.type == "recorded")
               {
                  _loc17_ = [
                     {
                        "cid":["16061202"],
                        "url":"http://www.apl.tv/bigfoot"
                     },
                     {
                        "cid":["16113109"],
                        "message":"I’m watching the explore.org Shark Lagoon Cam, streaming live from Aquarium of the Pacific: http://explore.org/sharks",
                        "url":"http://explore.org/sharks"
                     },
                     {
                        "cid":["12434787","12434837","12448065","12448106","13773579"],
                        "message":"I’m watching the explore.org #polarbearcam, streaming live from the arctic tundra: http://explore.org/polarbears",
                        "url":"http://explore.org/polarbears",
                        "heritable":true
                     },
                     {
                        "cid":["15754651"],
                        "message":"Meet Lucky, the inspiration behind Dog Bless You, a community that helps pair service dogs with veterans: http://explore.org/lucky",
                        "url":"http://explore.org/lucky"
                     },
                     {
                        "cid":["15593241"],
                        "message":"I\'m watching the Internet Cat Video Festival LIVE right now from the Minnesota State Fair at animalplanet.com. Watch with me: http://animalplanet.com/catvidfest #catvidfest",
                        "url":"http://animalplanet.com/catvidfest"
                     },
                     {
                        "cid":["15498751","14510221","14510237","13773579"],
                        "message":"I\'m watching #beecam on explore.org, streaming live from Bavaria, Germany: http://explore.org/bees",
                        "url":"http://explore.org/bees"
                     },
                     {
                        "cid":["11123648"],
                        "url":"http://www.nintendo.com/nintendo-direct"
                     },
                     {
                        "cid":["15498973"],
                        "message":"I\'m watching #belugacam on explore.org, streaming live from the Canadian arctic: http://explore.org/belugas",
                        "url":"http://explore.org/belugas"
                     },
                     {
                        "cid":["15419575"],
                        "message":"I\'m watching the Penguin Chicks live cam on @explore.org, streaming live from the @Aquarium of the Pacific.",
                        "url":"http://explore.org/penguinchicks"
                     },
                     {
                        "cid":["15433699"],
                        "message":"I\'m watching #belugacam on explore.org, streaming live from the Canadian arctic.",
                        "url":"http://explore.org/belugas"
                     },
                     {
                        "cid":["14879599","15428427"],
                        "message":"I\'m watching Sloths, live on #APLIVE!",
                        "url":"http://www.apl.tv/sloths.htm"
                     },
                     {
                        "cid":["14879579","15428435"],
                        "message":"I\'m watching Tamarins, live on #APLIVE!",
                        "url":"http://www.apl.tv/tamarins.htm"
                     },
                     {
                        "cid":["14812707","15006733"],
                        "message":"Watch Blacktip Reef Shark Cam on Sharkweek.com from Discovery.",
                        "url":"http://sharkweek.com/sharkcam"
                     },
                     {
                        "cid":["12434958","13773599"],
                        "message":"I\'m watching the Brooks Falls #bearcam on @explore.org, streaming live from @Katmai National Park & Preserve in Alaska.",
                        "url":"http://explore.org/bears"
                     },
                     {
                        "cid":["12431893"],
                        "message":"I\'m watching the Lower River #bearcam on @explore.org, streaming live from @Katmai National Park & Preserve in Alaska.",
                        "url":"http://explore.org/live-cams/player/brown-bear-salmon-cam-lower-river"
                     },
                     {
                        "cid":["12434972","13773591"],
                        "message":"I\'m watching #bearcam on @explore.org, streaming live from @Katmai National Park & Preserve in Alaska.",
                        "url":"http://explore.org/live-cams/player/brown-bear-salmon-cam-the-riffles"
                     },
                     {
                        "cid":["14420561"],
                        "url":"http://www.apl.tv/specials.htm"
                     },
                     {
                        "cid":["14225913"],
                        "url":"http://www.apl.tv/tanked.htm"
                     },
                     {
                        "cid":["25275"],
                        "message":"hármas metró az hát",
                        "url":"http://szanalmas.hu",
                        "heritable":true
                     },
                     {
                        "cid":["6148198"],
                        "message":"legyen különböző",
                        "url":"http://9gag.com"
                     },
                     {
                        "cid":["14120595"],
                        "heritable":true,
                        "message":"Ride along for an inside look at an IZOD IndyCar Series team\'s quest for victory at the Indy 500 at #HondaRacing http://gobu.me/10dWAGd!",
                        "url":"http://gobu.me/10dWAGd!"
                     },
                     {
                        "cid":["10863094"],
                        "message":"Willkommen zu unserer Nintendo 3DS Direct-Übertragung! Sieh dir die Präsentation an, um Neuigkeiten zum Nintendo 3DS zu erfahren!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["14021399"],
                        "message":"Willkommen zu unserer Nintendo 3DS Direct-Übertragung! Sieh dir die Präsentation an, um Neuigkeiten zum Nintendo 3DS zu erfahren!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863081","13773599"],
                        "message":"¡Bienvenido a nuestra presentación Nintendo 3DS Direct! ¡No te pierdas esta emisión con las últimas noticias sobre Nintendo 3DS!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863085","13773605"],
                        "message":"Bienvenue dans cette présentation Nintendo 3DS Direct ! Regardez cette diffusion pour connaître les nouveautés à venir sur Nintendo #3DS !",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863087","13773607"],
                        "message":"Benvenuto al nostro Nintendo 3DS Direct! Guarda la presentazione per tutte le ultime informazioni su Nintendo 3DS!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863093","13773615"],
                        "message":"Welkom bij onze Nintendo 3DS Direct-uitzending! Kijk naar de presentatie voor de laatste informatie over Nintendo 3DS-spellen!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863082","13773623"],
                        "message":"Assiste à nova Nintendo 3DS Direct para ficares a conhecer as novidades para a Nintendo 3DS.",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863090","13773627"],
                        "message":"Добро пожаловать на Nintendo 3DS Direct! Смотрите презентацию о новых играх для Nintendo 3DS!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["10863084","13773631"],
                        "message":"Welcome to our Nintendo 3DS Direct broadcast! Watch the presentation for the latest information on Nintendo 3DS!",
                        "url":_loc2_.u
                     },
                     {
                        "cid":["12762028"],
                        "url":"http://www.apl.tv/highlights.htm"
                     },
                     {
                        "cid":["13027426"],
                        "url":"http://www.apl.tv/calves.htm"
                     },
                     {
                        "cid":["13027427"],
                        "url":"http://www.apl.tv/chicks.htm"
                     },
                     {
                        "cid":["13027441"],
                        "url":"http://www.apl.tv/leaf-cutter-ants.htm"
                     },
                     {
                        "cid":["13027444"],
                        "url":"http://www.apl.tv/cockroaches.htm"
                     },
                     {
                        "cid":["13628076"],
                        "url":"http://www.apl.tv/african-penguins.htm"
                     },
                     {
                        "cid":["13027446"],
                        "url":"http://www.apl.tv/bird.htm"
                     },
                     {
                        "cid":["13628077"],
                        "url":"http://www.apl.tv/pacific-reef.htm"
                     },
                     {
                        "cid":["13027422"],
                        "url":"http://www.apl.tv/puppies.htm"
                     },
                     {
                        "cid":["13027369"],
                        "url":"http://www.apl.tv/kittens.htm"
                     },
                     {
                        "cid":["14812715","15006737"],
                        "url":"http://www.apl.tv/black-tip-reef.htm"
                     },
                     {
                        "cid":["14839273","15006743"],
                        "url":"http://www.apl.tv/parakeets.htm"
                     },
                     {
                        "cid":["13358425","13409034"],
                        "url":"http://discovery.com/live"
                     },
                     {
                        "cid":["12623612","13106887"],
                        "url":"http://explore.org/seals"
                     },
                     {
                        "cid":["12685211","12140637"],
                        "url":"http://dsc.discovery.com/tv-shows/american-chopper/chopper-live.htm"
                     },
                     {
                        "cid":["12435130","12435182","12431938","12312559"],
                        "url":"http://explore.org/puppies"
                     },
                     {
                        "cid":["12431835","12431867","12452681"],
                        "url":"http://explore.org/polarbears"
                     }];
                  _loc4_ = Logic.instance.media.id;
                  if(Logic.instance.media.type == "recorded")
                  {
                     _loc5_ = This.getReference(Logic.instance,"media.modules.meta.data");
                     _loc4_ = (_loc5_) && (_loc5_.ownerChannelId)?_loc5_.ownerChannelId:"0";
                  }
                  if((_loc7_) && (this.serviceList.hasOwnProperty("facebook")))
                  {
                     _loc18_ = (this.serviceList.facebook.hasOwnProperty("message")) && String(this.serviceList.facebook.message).length > 0?this.serviceList.facebook.message:"";
                     _loc19_ = (this.serviceList.facebook.hasOwnProperty("customUrl")) && String(this.serviceList.facebook.customUrl).length > 0?this.serviceList.facebook.customUrl:Logic.instance.media.url;
                     if(_loc18_.length > 0)
                     {
                        _loc15_ = this.linkIntoMessage(_loc18_,_loc19_);
                        _loc14_ = this.getFacebookUrl(_loc19_.length > 0?_loc19_:_loc2_.u,_loc15_,_loc16_);
                     }
                     else if(_loc19_.length > 0)
                     {
                        _loc2_.u = _loc19_;
                     }
                     
                  }
                  else
                  {
                     _loc6_ = 0;
                     while(_loc6_ < _loc17_.length)
                     {
                        if((_loc17_[_loc6_].cid as Array).indexOf(_loc4_) > -1 && ((Logic.instance.media.type == "channel") || (Logic.instance.media.type == "recorded" && _loc17_[_loc6_].heritable)))
                        {
                           if(_loc17_[_loc6_].hasOwnProperty("message"))
                           {
                              _loc15_ = this.linkIntoMessage(_loc17_[_loc6_].message,_loc17_[_loc6_].url);
                              _loc14_ = this.getFacebookUrl(_loc17_[_loc6_].url,_loc15_,_loc16_);
                           }
                           else
                           {
                              _loc2_.u = _loc17_[_loc6_].url;
                           }
                           break;
                        }
                        _loc6_++;
                     }
                  }
               }
               if(_loc14_.length < 1)
               {
                  _loc14_ = "http://www.facebook.com/sharer/sharer.php?" + _loc2_.toString();
               }
               Debug.echo("SHARE *** " + _loc14_);
               if(ExternalInterface.available)
               {
                  ExternalInterface.call("eval","openWindow = function() { window.open(\"" + _loc14_ + "\", \"_blank\", \"width=620, height=400\") }; openWindow();");
               }
               else
               {
                  navigateToURL(new URLRequest(_loc14_),"_blank");
               }
               break;
            default:
               if((this.serviceList[param1.target.name]) && (this.serviceList[param1.target.name].url))
               {
                  _loc20_ = new URLVariables();
                  _loc20_.u = Logic.instance.media.url;
                  navigateToURL(new URLRequest(this.serviceList[param1.target.name].url + _loc20_.toString().substr(2)),"_blank");
               }
         }
         Logic.instance.shareUse = param1.target.name;
         if(triggerMode == OptionsPanel.TRIGGER_CLICK_HIDE)
         {
            closePanel();
         }
      }
      
      private function getFacebookUrl(param1:String, param2:String, param3:String) : String {
         if(param3 == "")
         {
            return "http://www.facebook.com/sharer.php?s=100" + "&p[summary]=" + encodeURIComponent(param2) + "&p[url]=" + encodeURIComponent(param1) + (This.getReference(Logic.instance,"media.modules.meta.title")?"&p[title]=" + encodeURIComponent(Logic.instance.media.modules.meta.title):"");
         }
         return "https://www.facebook.com/dialog/feed?" + "app_id=" + param3 + "&display=popup" + (This.getReference(Logic.instance,"media.modules.meta.title")?"&caption=" + encodeURIComponent(Logic.instance.media.modules.meta.title):"") + "&link=" + encodeURIComponent(param1) + "&description=" + encodeURIComponent(param2) + "&redirect_uri=http://cdn1.ustream.tv/facebook_share/close.html";
      }
      
      private function linkIntoMessage(param1:String, param2:String, param3:String = "facebook") : String {
         var _loc4_:* = false;
         if(param1.indexOf("[link]") > -1)
         {
            param1 = param1.split("[link]").join(param2);
            _loc4_ = true;
         }
         if(param3 == "twitter" && !_loc4_)
         {
            param1 = param1 + " " + param2;
         }
         return param1;
      }
      
      override public function destroy() : void {
         super.destroy();
         this.disposeButtons();
         if(this._module)
         {
            this._module.removeEventListener("update",this.onModuleUpdate);
         }
      }
   }
}
