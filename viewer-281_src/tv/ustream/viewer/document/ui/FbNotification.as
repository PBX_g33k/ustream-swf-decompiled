package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.modules.RpinLock;
   import flash.display.Loader;
   import flash.display.Shape;
   import flash.utils.Timer;
   import flash.events.Event;
   import flash.system.LoaderContext;
   import tv.ustream.viewer.logic.Logic;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   import flash.system.SecurityDomain;
   import flash.net.URLRequest;
   import flash.display.Bitmap;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tween2.Tween;
   import flash.events.HTTPStatusEvent;
   import flash.events.TextEvent;
   import tv.ustream.localization.Locale;
   import flash.events.IOErrorEvent;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import tv.ustream.tools.Debug;
   import flash.filters.GlowFilter;
   import flash.filters.DropShadowFilter;
   import flash.text.StyleSheet;
   
   public class FbNotification extends Sprite
   {
      
      public function FbNotification(param1:RpinLock) {
         super();
         this.thumb = new Sprite();
         this.thumb.y = ViewerUI.baseH;
         this.thumb.alpha = 0;
         this.thumb.mouseChildren = false;
         this.thumb.buttonMode = true;
         this.thumb.filters = [new GlowFilter(16777215,0.04,2,2,100,1,true),new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
         this.thumb.addChild(this.loader = new Loader());
         this.loader.contentLoaderInfo.addEventListener(Event.COMPLETE,this.onProfileImgLoaded);
         this.loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
         this.loader.contentLoaderInfo.addEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         this.thumb.addChild(this.thumbMask = new Shape());
         this.loader.mask = this.thumbMask;
         addChild(this.loaderBar = new LoaderBar(9,2));
         this.loaderBar.alpha = 0;
         this.loaderBar.x = this.loaderBar.y = ViewerUI.baseH / 2;
         var _loc2_:StyleSheet = new StyleSheet();
         _loc2_.setStyle("p",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#FFFFFF"
            });
         _loc2_.setStyle("a",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#" + ViewerUI.style.linkColor.toString(16)
            });
         _loc2_.setStyle("a:hover",{"textDecoration":"underline"});
         addChild(this.label = new ScrollingLabel(null,_loc2_,true));
         this.label.embedFonts = ViewerUI.baseFontEmbedded;
         this.label.overSample = !ViewerUI.baseFontEmbedded;
         this.label.locked = true;
         this.label.addEventListener(TextEvent.LINK,this.onLink);
         this.label.text = "<p>" + Locale.instance.label(ViewerLabels.labelB4fAddingVideo) + "</p>";
         this.label.alpha = 0;
         this.label.y = ViewerUI.baseH;
         this.closeTimer = new Timer(3000,1);
         this.closeTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onCloseTimerComplete);
         addEventListener(MouseEvent.MOUSE_OVER,this.open);
         addEventListener(MouseEvent.MOUSE_MOVE,this.open);
         this._module = param1;
         this._module.addEventListener(RpinLock.PUSH_REQUESTED,this.lockText);
         this._module.addEventListener(RpinLock.PUSH_SUCCESSFUL,this.onPushSuccess);
         this._module.addEventListener(RpinLock.PUSH_FAILED,this.unlockText);
         this._module.addEventListener(RpinLock.REMOVE_REQUESTED,this.lockText);
         this._module.addEventListener(RpinLock.REMOVE_SUCCESSFUL,this.onRemoveSuccess);
         this._module.addEventListener(RpinLock.REMOVE_FAILED,this.unlockText);
         if(Logic.hasInstance)
         {
            Logic.instance.addEventListener("play",this.loadProfileImg);
         }
      }
      
      private var _module:RpinLock;
      
      private var _availWidth:Number = 0;
      
      private var _opened:Boolean;
      
      private var _profileImgUrl:String;
      
      private var thumbW:Number = 26;
      
      private var loader:Loader;
      
      private var thumbMask:Shape;
      
      private var thumb:Sprite;
      
      private var label:ScrollingLabel;
      
      private var closeTimer:Timer;
      
      private var loaderBar:LoaderBar;
      
      private var _locked:Boolean = true;
      
      private function onCloseTimerComplete(param1:Event = null) : void {
         this.closeTimer.stop();
         this.close();
      }
      
      private function loadProfileImg(param1:Event = null) : void {
         var _loc3_:LoaderContext = null;
         this.echo("loadeProfileImg");
         if(Logic.hasInstance)
         {
            Logic.instance.removeEventListener("play",this.loadProfileImg);
         }
         var _loc2_:String = "https://graph.facebook.com/me/picture?access_token=" + this._module.facebookAuthToken;
         if(_loc2_ != this._profileImgUrl)
         {
            this._profileImgUrl = _loc2_;
            _loc3_ = new LoaderContext(false,ApplicationDomain.currentDomain);
            if(Security.sandboxType != Security.LOCAL_TRUSTED)
            {
               _loc3_.securityDomain = SecurityDomain.currentDomain;
            }
            this.loader.load(new URLRequest(this._profileImgUrl),_loc3_);
         }
      }
      
      private function onProfileImgLoaded(param1:Event) : void {
         if(this.loader.content is Bitmap)
         {
            Bitmap(this.loader.content).smoothing = true;
         }
         this.loader.content.width = this.thumbW;
         this.loader.content.scaleY = this.loader.content.scaleX;
         this.loader.x = (ViewerUI.baseH - this.thumbW) / 2;
         this.loader.y = (ViewerUI.baseH - this.loader.content.height) / 2;
         if(!contains(this.thumb))
         {
            addChild(this.thumb);
            Tween.to(this.thumb,
               {
                  "y":0,
                  "alpha":1
               },30 * 0.6,"easeInOutQuart");
         }
         this.thumb.graphics.clear();
         this.thumb.graphics.beginFill(16737792,0);
         this.thumb.graphics.drawRect(0,0,ViewerUI.baseH,ViewerUI.baseH);
         this.thumb.graphics.endFill();
         this.thumbMask.x = this.thumbMask.y = (ViewerUI.baseH - this.thumbW) / 2;
         this.thumbMask.graphics.clear();
         this.thumbMask.graphics.beginFill(16737792);
         this.thumbMask.graphics.drawRoundRect(0,0,this.thumbW,this.thumbW,4);
         this.thumbMask.graphics.endFill();
         dispatchEvent(new Event(Event.RESIZE));
         this.open();
      }
      
      private function onHttpStatus(param1:HTTPStatusEvent) : void {
         this.echo(param1.toString());
      }
      
      private function onLoaderError(param1:Event) : void {
         this.echo(param1.toString());
      }
      
      override public function get width() : Number {
         return contains(this.thumb)?this.thumb.width * scaleX:0;
      }
      
      public function get labelWidth() : Number {
         return 8 + this.label.textWidth + 8;
      }
      
      public function get availWidth() : Number {
         return this._availWidth;
      }
      
      public function set availWidth(param1:Number) : void {
         this._availWidth = param1;
         this.label.x = -this._availWidth + 8;
         this.label.maskWidth = this._availWidth - 16;
      }
      
      public function get opened() : Boolean {
         return this._opened;
      }
      
      public function open(param1:Event = null) : void {
         var e:Event = param1;
         if(!contains(this.thumb))
         {
            return;
         }
         this.closeTimer.reset();
         this.closeTimer.start();
         if(this._opened)
         {
            return;
         }
         if(this._availWidth != this.labelWidth)
         {
            dispatchEvent(new Event(Event.OPEN));
         }
         Tween.kill(this.label);
         this.label.locked = false;
         Tween.to(this.label,
            {
               "y":0,
               "alpha":1
            },30 * 0.6,"easeInOutQuart",null,null,function():void
         {
            label.y = Math.round(label.y);
         });
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(-this._availWidth,0,this._availWidth + this.thumb.width,ViewerUI.baseH);
         graphics.endFill();
         this._opened = true;
      }
      
      public function close(param1:Event = null) : void {
         var e:Event = param1;
         if(!this._opened || (this._locked))
         {
            return;
         }
         dispatchEvent(new Event(Event.CLOSE));
         Tween.kill(this.label);
         Tween.to(this.label,
            {
               "y":ViewerUI.baseH,
               "alpha":0
            },30 * 0.6,"easeInOutQuart",function():void
         {
            label.locked = true;
         },null,function():void
         {
            label.y = Math.round(label.y);
         });
         graphics.clear();
         this._opened = false;
      }
      
      private function onLink(param1:TextEvent) : void {
         switch(param1.text)
         {
            case "remove":
               this._module.removeFromTimeline();
               break;
            case "add":
               this._module.pushToTimeline();
               break;
         }
      }
      
      private function onPushSuccess(param1:Event = null) : void {
         this.echo("onPushSuccsess");
         this.label.text = "<p>" + Locale.instance.label(ViewerLabels.labelB4fVideoAdded,
            {
               "a1":"<a href=\"event:remove\">",
               "a2":"</a>"
            }) + "</p>";
         this.label.reset();
         this.unlockText();
         this.open();
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function onRemoveSuccess(param1:Event = null) : void {
         this.echo("onRemoveSuccess");
         this.label.text = "<p>" + Locale.instance.label(ViewerLabels.labelB4fVideoRemoved,
            {
               "a1":"<a href=\"event:add\">",
               "a2":"</a>"
            }) + "</p>";
         this.label.reset();
         this.unlockText();
         this.open();
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function lockText(param1:Event = null) : void {
         this.label.mouseEnabled = this.label.mouseChildren = false;
         this.label.styleSheet.setStyle("a:hover",{"textDecoration":"none"});
         Tween.to(this.loaderBar,{"alpha":1},30 * 0.2);
         Tween.to(this.thumb,{"alpha":0},30 * 0.2);
         Tween.to(this.label,{"alpha":0.5},30 * 0.2);
         this._locked = true;
      }
      
      private function unlockText(param1:Event = null) : void {
         this.label.mouseEnabled = this.label.mouseChildren = true;
         this.label.styleSheet.setStyle("a:hover",{"textDecoration":"underline"});
         Tween.to(this.loaderBar,{"alpha":0},30 * 0.2);
         Tween.to(this.thumb,{"alpha":1},30 * 0.2);
         Tween.to(this.label,{"alpha":1},30 * 0.2);
         this._locked = false;
      }
      
      public function destroy() : void {
         this.loader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onProfileImgLoaded);
         this.loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
         this.loader.contentLoaderInfo.removeEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         removeEventListener(MouseEvent.MOUSE_OVER,this.open);
         removeEventListener(MouseEvent.MOUSE_MOVE,this.open);
         this.closeTimer.stop();
         this.closeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onCloseTimerComplete);
         this.label.removeEventListener(TextEvent.LINK,this.onLink);
         this.label.destroy();
         this._module.removeEventListener(RpinLock.PUSH_REQUESTED,this.lockText);
         this._module.removeEventListener(RpinLock.PUSH_SUCCESSFUL,this.onPushSuccess);
         this._module.removeEventListener(RpinLock.PUSH_FAILED,this.unlockText);
         this._module.removeEventListener(RpinLock.REMOVE_REQUESTED,this.lockText);
         this._module.removeEventListener(RpinLock.REMOVE_SUCCESSFUL,this.onRemoveSuccess);
         this._module.removeEventListener(RpinLock.REMOVE_FAILED,this.unlockText);
         this._module = null;
         if(Logic.hasInstance)
         {
            Logic.instance.removeEventListener("play",this.loadProfileImg);
         }
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ FbNotif ] " + param1);
      }
   }
}
