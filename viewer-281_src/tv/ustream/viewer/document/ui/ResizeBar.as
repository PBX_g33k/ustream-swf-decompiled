package tv.ustream.viewer.document.ui
{
   import tv.ustream.viewer.document.ui.icons.FullScreenIcon;
   import flash.geom.Rectangle;
   import tv.ustream.localization.Locale;
   import flash.events.MouseEvent;
   import flash.events.FullScreenEvent;
   import tv.ustream.viewer.document.ui.icons.TheaterModeIcon;
   import tv.ustream.viewer.document.ui.icons.PopoutIcon;
   import flash.events.Event;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.display.StageDisplayState;
   
   public class ResizeBar extends OptionsPanel
   {
      
      public function ResizeBar(param1:Boolean = true, param2:Boolean = true, param3:Boolean = true) {
         this.buttons = [];
         super();
         this.hasFullScreen = param1;
         this.hasTheaterMode = param2;
         this.hasPopout = param3;
         if(ViewerUI.touchMode)
         {
            triggerMode = TRIGGER_CLICK;
         }
         else
         {
            triggerMode = TRIGGER_OVER;
         }
      }
      
      private var fsBtn:Button;
      
      private var popoutBtn:Button;
      
      private var theaterModeBtn:Button;
      
      private var hasFullScreen:Boolean;
      
      private var hasTheaterMode:Boolean;
      
      private var hasPopout:Boolean;
      
      private var _theaterMode:Boolean;
      
      private var buttons:Array;
      
      override protected function buildUI() : void {
         if(this.hasFullScreen)
         {
            addChild(this.fsBtn = new Button());
            this.fsBtn.icon = new FullScreenIcon();
            this.fsBtn._hitArea = new Rectangle(0,0,34,28);
            this.fsBtn.x = 0;
            this.fsBtn.y = -this.fsBtn.height / 2;
            this.fsBtn.colorizable = true;
            this.fsBtn.tooltip = Locale.instance.label(ViewerLabels.labelToggleFullscreen);
            this.fsBtn.tooltipDelay = (this.hasTheaterMode) || (this.hasPopout)?30:0;
            this.fsBtn.addEventListener(MouseEvent.CLICK,this.onFsBtn);
            stage.addEventListener(FullScreenEvent.FULL_SCREEN,this.onFullScreen);
            trigger = this.fsBtn;
            this.buttons.push(this.fsBtn);
         }
         if(this.hasTheaterMode)
         {
            addChild(this.theaterModeBtn = new Button());
            this.theaterModeBtn.icon = new TheaterModeIcon();
            this.theaterModeBtn._hitArea = new Rectangle(0,0,34,28);
            this.theaterModeBtn.x = 0;
            this.theaterModeBtn.y = this.buttons.length?this.buttons[this.buttons.length - 1].y - this.theaterModeBtn.height:-this.theaterModeBtn.height / 2;
            this.theaterModeBtn.addEventListener(MouseEvent.CLICK,this.onTheaterModeBtn);
            this.theaterModeBtn.tooltip = Locale.instance.label(ViewerLabels.labelToggleTheaterMode);
            this.theaterModeBtn.tooltipDelay = (this.hasFullScreen) || (this.hasPopout)?30:0;
            this.theaterMode = loaderInfo.parameters.theater == "1";
            if(this.hasFullScreen)
            {
               addTweenListItem(this.theaterModeBtn);
            }
            else
            {
               trigger = this.theaterModeBtn;
            }
            this.buttons.push(this.theaterModeBtn);
         }
         if(this.hasPopout)
         {
            addChild(this.popoutBtn = new Button());
            this.popoutBtn.icon = new PopoutIcon();
            this.popoutBtn._hitArea = new Rectangle(0,0,34,28);
            this.popoutBtn.x = 0;
            this.popoutBtn.y = this.buttons.length?this.buttons[this.buttons.length - 1].y - this.popoutBtn.height:-this.popoutBtn.height / 2;
            this.popoutBtn.addEventListener(MouseEvent.CLICK,this.onPupoutBtn);
            this.popoutBtn.tooltip = Locale.instance.label(ViewerLabels.labelPopout);
            this.popoutBtn.tooltipDelay = (this.hasFullScreen) || (this.hasTheaterMode)?30:0;
            if((this.hasFullScreen) || (this.hasTheaterMode))
            {
               addTweenListItem(this.popoutBtn);
            }
            else
            {
               trigger = this.popoutBtn;
            }
            this.buttons.push(this.popoutBtn);
         }
         setBg(34,this.buttons[this.buttons.length - 1].y - 6);
      }
      
      public function get theaterMode() : Boolean {
         return this._theaterMode;
      }
      
      public function set theaterMode(param1:Boolean) : void {
         this._theaterMode = param1;
         if(this.theaterModeBtn)
         {
            this.theaterModeBtn.icon.opened = param1;
         }
      }
      
      private function onFsBtn(param1:MouseEvent) : void {
         if(triggerMode == TRIGGER_OVER || triggerMode == TRIGGER_CLICK && (this.buttons.length == 1 || (_opened)))
         {
            dispatchEvent(new Event(ViewerUI.TOGGLE_FULL_SCREEN,true));
            closePanel();
            param1.stopImmediatePropagation();
         }
      }
      
      private function onFullScreen(param1:FullScreenEvent) : void {
         if(param1.fullScreen)
         {
            this.fsBtn.icon.openBtn.visible = false;
            this.fsBtn.icon.closeBtn.visible = true;
         }
         else
         {
            this.fsBtn.icon.openBtn.visible = true;
            this.fsBtn.icon.closeBtn.visible = false;
         }
      }
      
      private function onTheaterModeBtn(param1:Event) : void {
         if(triggerMode == TRIGGER_OVER || triggerMode == TRIGGER_CLICK && (this.buttons.length == 1 || (_opened)))
         {
            if(stage.displayState != StageDisplayState.NORMAL)
            {
               dispatchEvent(new Event(ViewerUI.TOGGLE_FULL_SCREEN,true));
            }
            dispatchEvent(new Event(ViewerUI.TOGGLE_THEATER_MODE,true));
            this.theaterMode = !this.theaterMode;
            if(stage.displayState != StageDisplayState.NORMAL)
            {
               closePanel();
            }
            if(trigger == param1.currentTarget)
            {
               param1.stopImmediatePropagation();
            }
         }
      }
      
      private function onPupoutBtn(param1:Event) : void {
         if(triggerMode == TRIGGER_OVER || triggerMode == TRIGGER_CLICK && (this.buttons.length == 1 || (_opened)))
         {
            if(stage.displayState != StageDisplayState.NORMAL)
            {
               dispatchEvent(new Event(ViewerUI.TOGGLE_FULL_SCREEN,true));
            }
            dispatchEvent(new Event(ViewerUI.POPOUT,true));
            closePanel();
            if(trigger == param1.currentTarget)
            {
               param1.stopImmediatePropagation();
            }
         }
      }
      
      override protected function openPanel() : void {
         if(this.buttons.length > 1)
         {
            super.openPanel();
         }
      }
      
      override public function destroy() : void {
         super.destroy();
         if(this.fsBtn)
         {
            this.fsBtn.removeEventListener(MouseEvent.CLICK,this.onFsBtn);
            stage.removeEventListener(FullScreenEvent.FULL_SCREEN,this.onFullScreen);
         }
         if(this.theaterModeBtn)
         {
            this.theaterModeBtn.removeEventListener(MouseEvent.CLICK,this.onTheaterModeBtn);
         }
         if(this.popoutBtn)
         {
            this.popoutBtn.removeEventListener(MouseEvent.CLICK,this.onPupoutBtn);
         }
      }
      
      public function set onlyFullScreen(param1:Boolean) : void {
         if(param1)
         {
            if(this.theaterModeBtn)
            {
               this.theaterModeBtn.hidden = true;
            }
            if(this.popoutBtn)
            {
               this.popoutBtn.hidden = true;
            }
            hidePanel = true;
         }
         else
         {
            if(this.theaterModeBtn)
            {
               this.theaterModeBtn.hidden = false;
            }
            if(this.popoutBtn)
            {
               this.popoutBtn.hidden = false;
            }
            hidePanel = false;
         }
      }
   }
}
