package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.DisplayObject;
   import flash.utils.Timer;
   import flash.events.Event;
   import flash.filters.DropShadowFilter;
   import flash.events.MouseEvent;
   import flash.geom.Matrix;
   import flash.display.BitmapData;
   import flash.display.GradientType;
   import flash.utils.clearTimeout;
   import tv.ustream.tween2.Tween;
   import flash.utils.setTimeout;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.TimerEvent;
   import flash.geom.Rectangle;
   
   public class OptionsPanel extends Sprite
   {
      
      public function OptionsPanel() {
         this.tweenList = [];
         super();
         OptionsPanel.panels.push(this);
         mouseEnabled = false;
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
         this.idleTimer = new Timer(8000,1);
         this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onIdleTimer);
      }
      
      public static const TRIGGER_OVER:String = "triggerOver";
      
      public static const TRIGGER_CLICK:String = "triggerClick";
      
      public static const TRIGGER_OVER_HIDE:String = "triggerOverHide";
      
      public static const TRIGGER_CLICK_HIDE:String = "triggerClickHide";
      
      private static var panels:Array = [];
      
      public static function closeAll(param1:OptionsPanel = null) : void {
         var _loc3_:OptionsPanel = null;
         var _loc2_:* = 0;
         while(_loc2_ < OptionsPanel.panels.length)
         {
            _loc3_ = OptionsPanel.panels[_loc2_];
            if(param1 == null || !(_loc3_ == param1))
            {
               _loc3_.closePanel();
            }
            _loc2_++;
         }
      }
      
      public var bg:Sprite;
      
      public var tweenList:Array;
      
      private var _trigger:DisplayObject;
      
      private var _hidePanel:Boolean;
      
      public var triggerMode:String = "triggerOver";
      
      protected var _opened:Boolean;
      
      public var isDragging:Boolean;
      
      protected var idleTimer:Timer;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         addChild(this.bg = new Sprite());
         this.bg.filters = [new DropShadowFilter(0,90,0,0.3,3,3,1,2)];
         this.tweenList.push(this.bg);
         this.buildUI();
         this.prepareTweenList();
         if(this.triggerMode == TRIGGER_CLICK || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            stage.addEventListener(MouseEvent.CLICK,this.onStageClick);
         }
      }
      
      protected function buildUI() : void {
      }
      
      protected function set hidePanel(param1:Boolean) : void {
         this._hidePanel = param1;
         if(param1)
         {
            this.closePanel();
         }
      }
      
      protected function setBg(param1:Number = 28, param2:Number = 0) : void {
         var _loc3_:Number = Math.abs(param2) + 20 + 6 + 4;
         var _loc4_:Matrix = new Matrix();
         _loc4_.createGradientBox(param1,_loc3_,90 * Math.PI / 180);
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(3026478);
         this.bg.graphics.drawRoundRect(0,0,param1,_loc3_,8);
         this.bg.graphics.endFill();
         var _loc5_:BitmapData = new BitmapData(2,2,true,3.42439478E9);
         _loc5_.setPixel32(0,0,3.423605008E9);
         _loc5_.setPixel32(1,1,3.423605008E9);
         this.bg.graphics.beginBitmapFill(_loc5_);
         this.bg.graphics.drawRoundRect(0,0,param1,_loc3_,8);
         this.bg.graphics.endFill();
         this.bg.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.11,0.11],[0,255],_loc4_);
         this.bg.graphics.drawRoundRect(0,0,param1,_loc3_,8);
         this.bg.graphics.endFill();
         this.bg.y = param2;
         if(this._trigger)
         {
            graphics.clear();
            graphics.beginFill(16737792,0);
            graphics.drawRect(0,this._trigger.y,param1,this._trigger.height);
            graphics.endFill();
         }
      }
      
      protected function prepareTweenList() : void {
         var _loc2_:* = undefined;
         var _loc1_:* = 0;
         while(_loc1_ < this.tweenList.length)
         {
            _loc2_ = this.tweenList[_loc1_];
            if(!_loc2_.name)
            {
               _loc2_.name = "tween_" + _loc1_;
            }
            this.tweenList[_loc2_.name] = 
               {
                  "y":_loc2_.y,
                  "alpha":_loc2_.alpha
               };
            if(this.tweenList[_loc1_] != this.trigger)
            {
               this.tweenList[_loc1_].y = this.tweenList[_loc1_].y + Math.min(65,this.bg.height);
               this.tweenList[_loc1_].alpha = 0;
            }
            _loc1_++;
         }
      }
      
      protected function addTweenListItem(param1:*) : void {
         var _loc2_:Array = [];
         var _loc3_:* = 0;
         while(_loc3_ < this.tweenList.length)
         {
            _loc2_.push(this.tweenList[_loc3_]);
            _loc3_++;
         }
         this.tweenList = _loc2_.concat(param1);
         addChildAt(this._trigger,numChildren);
      }
      
      protected function removeTweenListItem(param1:*) : void {
         var _loc2_:Array = [];
         var _loc3_:* = 0;
         while(_loc3_ < this.tweenList.length)
         {
            if(this.tweenList[_loc3_] != param1)
            {
               _loc2_.push(this.tweenList[_loc3_]);
            }
            _loc3_++;
         }
         this.tweenList = _loc2_;
      }
      
      public function get trigger() : DisplayObject {
         return this._trigger;
      }
      
      public function set trigger(param1:DisplayObject) : void {
         if(this._trigger)
         {
            this._trigger.removeEventListener(MouseEvent.MOUSE_OVER,this.handleTrigger);
            this._trigger.removeEventListener(MouseEvent.CLICK,this.handleTrigger);
         }
         this._trigger = param1;
         addChildAt(this._trigger,numChildren);
         switch(this.triggerMode)
         {
            case OptionsPanel.TRIGGER_OVER:
            case OptionsPanel.TRIGGER_OVER_HIDE:
               this._trigger.addEventListener(MouseEvent.MOUSE_OVER,this.handleTrigger);
               break;
            default:
               this._trigger.addEventListener(MouseEvent.CLICK,this.handleTrigger);
         }
      }
      
      private function handleTrigger(param1:MouseEvent) : void {
         switch(this.triggerMode)
         {
            case OptionsPanel.TRIGGER_OVER:
            case OptionsPanel.TRIGGER_OVER_HIDE:
               addEventListener(MouseEvent.MOUSE_OUT,this.closePanel);
               stage.addEventListener(Event.MOUSE_LEAVE,this.closePanel);
               this.openPanel();
               break;
            case OptionsPanel.TRIGGER_CLICK:
            case OptionsPanel.TRIGGER_CLICK_HIDE:
               if(!this._opened)
               {
                  this.openPanel();
               }
               break;
         }
      }
      
      protected function openPanel() : void {
         var cb:Function = null;
         if(this._hidePanel)
         {
            return;
         }
         OptionsPanel.closeAll(this);
         var i:int = 0;
         while(i < this.tweenList.length)
         {
            clearTimeout(this.tweenList["to_" + i]);
            cb = function(param1:*, param2:Array, param3:int):void
            {
               param1.visible = true;
               Tween.kill(param1);
               Tween.to(param1,{"y":param2[param1.name].y},30 * 0.3,"easeOutExpo");
               if(param1 == param2[0])
               {
                  Tween.to(param1,{"alpha":1},30 * 0.3,"easeOutExpo");
               }
               else
               {
                  Tween.to(param1,{"alpha":1},30 * 0.3,"easeNone");
               }
            };
            this.tweenList["to_" + i] = setTimeout(cb,i == 0?0:1000 / 30 * 2,this.tweenList[i],this.tweenList,i);
            i++;
         }
         if(this.triggerMode == TRIGGER_OVER_HIDE || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            Tween.to(this.trigger,
               {
                  "alpha":0,
                  "y":-Math.round(this.trigger.height / 2) + this.trigger.height / 4
               },30 * 0.1,"easeNone");
         }
         if(this.triggerMode == TRIGGER_CLICK || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            this.idleTimer.reset();
            this.idleTimer.start();
         }
         this._opened = true;
         mouseEnabled = true;
      }
      
      public function closePanel(param1:* = null) : void {
         var cb:Function = null;
         var e:* = param1;
         if(!this._opened || (this.isDragging))
         {
            return;
         }
         if(e is MouseEvent && (!(e.relatedObject == null) && (contains(DisplayObject(e.relatedObject))) || e.relatedObject == this))
         {
            return;
         }
         removeEventListener(MouseEvent.MOUSE_OUT,this.closePanel);
         if(stage)
         {
            stage.removeEventListener(Event.MOUSE_LEAVE,this.closePanel);
         }
         this.tweenList.reverse();
         var i:int = 0;
         while(i < this.tweenList.length)
         {
            clearTimeout(this.tweenList["to_" + i]);
            cb = function(param1:*, param2:Array, param3:int):void
            {
               var _loc4_:Number = param1 == param2[0]?bg.height:Math.min(65,bg.height);
               Tween.kill(param1);
               Tween.to(param1,{"y":param2[param1.name].y + _loc4_},30 * 0.8,"easeOutQuart");
               if(param1 == param2[0])
               {
                  Tween.to(param1,{"alpha":0},30 * 0.8,"easeOutQuart");
               }
               else
               {
                  Tween.to(param1,{"alpha":0},30 * 0.4,"easeOutExpo");
               }
               onPanelClosed();
            };
            this.tweenList["to_" + i] = setTimeout(cb,i == this.tweenList.length - 1?55:0,this.tweenList[i],this.tweenList,i);
            i++;
         }
         this.tweenList.reverse();
         if(this.triggerMode == TRIGGER_OVER_HIDE || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            Tween.to(this.trigger,
               {
                  "alpha":1,
                  "y":-Math.round(this.trigger.height / 2)
               },30 * 0.4,"easeOutExpo");
         }
         if(this.triggerMode == TRIGGER_CLICK || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            this.idleTimer.reset();
         }
         this._opened = false;
         mouseEnabled = false;
      }
      
      protected function onPanelClosed() : void {
      }
      
      public function destroy() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         if(this._trigger)
         {
            this._trigger.removeEventListener(MouseEvent.MOUSE_OVER,this.handleTrigger);
            this._trigger.removeEventListener(MouseEvent.CLICK,this.handleTrigger);
            (this._trigger as Button).destroy();
         }
         removeEventListener(MouseEvent.MOUSE_OUT,this.closePanel);
         stage.removeEventListener(Event.MOUSE_LEAVE,this.closePanel);
         var _loc1_:* = 0;
         while(_loc1_ < this.tweenList.length)
         {
            Tween.kill(this.tweenList[_loc1_]);
            clearTimeout(this.tweenList["to_" + _loc1_]);
            _loc1_++;
         }
         _loc1_ = 0;
         while(_loc1_ < OptionsPanel.panels.length)
         {
            if(OptionsPanel.panels[_loc1_] == this)
            {
               OptionsPanel.panels.splice(_loc1_,1);
               break;
            }
            _loc1_++;
         }
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
         this.idleTimer.reset();
         this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onIdleTimer);
         if(this.triggerMode == TRIGGER_CLICK || this.triggerMode == TRIGGER_CLICK_HIDE)
         {
            stage.removeEventListener(MouseEvent.CLICK,this.onStageClick);
         }
      }
      
      override public function get alpha() : Number {
         return super.alpha;
      }
      
      override public function set alpha(param1:Number) : void {
         mouseChildren = param1 < 0.01?false:true;
         super.alpha = param1;
      }
      
      protected function updateStyle(... rest) : void {
      }
      
      protected function onIdleTimer(param1:Event = null) : void {
         this.idleTimer.reset();
         this.closePanel();
      }
      
      protected function onStageClick(param1:MouseEvent) : void {
         var _loc2_:Rectangle = null;
         if(this._opened)
         {
            _loc2_ = this.getBounds(stage);
            if(_loc2_.contains(param1.stageX,param1.stageY))
            {
               this.idleTimer.reset();
               this.idleTimer.start();
            }
            else
            {
               this.closePanel();
            }
         }
      }
   }
}
