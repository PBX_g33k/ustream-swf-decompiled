package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.Event;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tween2.Color;
   import tv.ustream.tools.Debug;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.viewer.document.ui.icons.RedSquarePlayIcon;
   import flash.filters.DropShadowFilter;
   import flash.geom.Rectangle;
   
   public class ControlPane extends Sprite
   {
      
      public function ControlPane() {
         this.PlayIcon = ControlPane_PlayIcon;
         super();
         addChild(this.bg = new Sprite());
         addChild(this.playBtn = new Button());
         this.playBtn.glowColor = ViewerUI.style.color;
         if(ViewerUI.style.controlPanePlaybuttonStyle == "redsquare")
         {
            this.playBtn.icon = new RedSquarePlayIcon();
            this.playBtn.noGlow = true;
         }
         else
         {
            this.playBtn.icon = new this.PlayIcon();
            Color.tint(this.playBtn.icon.getChildByName("icon"),ViewerUI.style.color,1);
            this.playBtn.icon.getChildByName("bg").alpha = 0.15;
            this.playBtn.icon.scaleX = this.playBtn.icon.scaleY = 80 / this.playBtn.icon.width;
         }
         this.playBtn.filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
         this.playBtn._hitArea = new Rectangle(0,0,80,80);
         this.playBtn.addEventListener(MouseEvent.CLICK,this.onPlayBtnClick);
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private var PlayIcon:Class;
      
      private var _width:Number = 0;
      
      private var _height:Number = 0;
      
      private var playBtn:Button;
      
      private var bg:Sprite;
      
      private var isEmbed:Boolean = false;
      
      private var _startMode:Boolean = true;
      
      private var _closeable:Boolean = true;
      
      public var active:Boolean = true;
      
      private function onPlayBtnClick(param1:MouseEvent = null) : void {
         if((Logic.hasInstance && Logic.instance) && (Logic.instance.media) && (Logic.instance.media.embedRedirection))
         {
            this.redirectToSite();
         }
         else
         {
            dispatchEvent(new Event(ViewerUI.CONTROL_PLAY,true));
         }
      }
      
      public function get startMode() : Boolean {
         return this._startMode;
      }
      
      public function set startMode(param1:Boolean) : void {
         this._startMode = param1;
         if(param1)
         {
            this.bg.addEventListener(MouseEvent.CLICK,this.onBgClick);
         }
         else
         {
            this.bg.removeEventListener(MouseEvent.CLICK,this.onBgClick);
         }
         this.resize();
      }
      
      public function get closeable() : Boolean {
         return this._closeable;
      }
      
      public function set closeable(param1:Boolean) : void {
         this._closeable = param1;
      }
      
      private function onBgClick(param1:Event = null) : void {
         if((Logic.hasInstance) && (Logic.instance.media.embedRedirection))
         {
            this.redirectToSite();
         }
         else
         {
            this.onPlayBtnClick();
         }
      }
      
      public function resize(... rest) : void {
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            this._height = rest[1];
         }
         var _loc2_:Number = this._height - ((parent.parent as ViewerUI).lite?0:ViewerUI.baseH * ViewerUI.ratio);
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(0,0);
         this.bg.graphics.drawRect(0,0,this._width,this._height);
         this.bg.graphics.endFill();
         this.bg.graphics.beginFill(0,0.75);
         this.bg.graphics.drawRect(0,0,this._width,_loc2_);
         this.bg.graphics.endFill();
         var _loc3_:Number = (Math.max(320,Math.min(640,this._width)) - 320) / 320;
         this.playBtn.scaleX = this.playBtn.scaleY = (0.6 + _loc3_ * 0.4) * ViewerUI.ratio;
         this.playBtn.x = Math.round((this._width - this.playBtn.width) / 2);
         this.playBtn.y = Math.round((_loc2_ - this.playBtn.height) / 2);
      }
      
      public function destroy() : void {
         this.bg.removeEventListener(MouseEvent.CLICK,this.onBgClick);
         this.playBtn.removeEventListener(MouseEvent.CLICK,this.onPlayBtnClick);
         this.playBtn.destroy();
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(param1:Event = null) : void {
         this.playBtn.glowColor = ViewerUI.style.color;
         Color.tint(this.playBtn.icon.getChildByName("icon"),ViewerUI.style.color,1);
      }
      
      private function redirectToSite() : void {
         Debug.echo("redirectToSite()");
         try
         {
            if((Logic.hasInstance) && (Logic.instance.media))
            {
               navigateToURL(new URLRequest("http://www.ustream.tv/" + Logic.instance.media.type + "/" + Logic.instance.media.id + "?utm_campaign=JPER&utm_medium=FlashPlayer&utm_source=embed"));
            }
         }
         catch(e:Error)
         {
         }
      }
   }
}
