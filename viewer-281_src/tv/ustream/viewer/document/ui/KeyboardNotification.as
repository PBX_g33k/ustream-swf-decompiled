package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.DisplayObject;
   import tv.ustream.tween2.Tween;
   import flash.display.BlendMode;
   import tv.ustream.tween2.Color;
   import flash.filters.DropShadowFilter;
   import flash.filters.BitmapFilterQuality;
   
   public class KeyboardNotification extends Sprite
   {
      
      public function KeyboardNotification() {
         this.PlayIcon = KeyboardNotification_PlayIcon;
         this.PauseIcon = KeyboardNotification_PauseIcon;
         this.MuteIcon = KeyboardNotification_MuteIcon;
         this.UnmuteIcon = KeyboardNotification_UnmuteIcon;
         super();
         this._notificationType = "";
         this.playIcon = new this.PlayIcon();
         this.playIcon.getChildByName("overlay").blendMode = BlendMode.NORMAL;
         this.playIcon.getChildByName("bg").alpha = 0.5;
         this.pauseIcon = new this.PauseIcon();
         this.pauseIcon.getChildByName("overlay").blendMode = BlendMode.NORMAL;
         this.pauseIcon.getChildByName("bg").alpha = 0.5;
         Color.tint(this.playIcon.getChildByName("icon"),16777215,1);
         Color.tint(this.pauseIcon.getChildByName("icon"),16777215,1);
         this.muteIcon = new this.MuteIcon();
         this.unmuteIcon = new this.UnmuteIcon();
         var _loc1_:DropShadowFilter = new DropShadowFilter(1,90,0,0.24,2,2,1,BitmapFilterQuality.HIGH);
         this.muteIcon.filters = [_loc1_];
         this.unmuteIcon.filters = [_loc1_];
         mouseChildren = false;
         mouseEnabled = false;
      }
      
      public static const PLAY:String = "play";
      
      public static const PAUSE:String = "pause";
      
      public static const MUTE:String = "mute";
      
      public static const UNMUTE:String = "unmute";
      
      private var PlayIcon:Class;
      
      private var PauseIcon:Class;
      
      private var MuteIcon:Class;
      
      private var UnmuteIcon:Class;
      
      private var playIcon:Sprite;
      
      private var pauseIcon:Sprite;
      
      private var muteIcon:Sprite;
      
      private var unmuteIcon:Sprite;
      
      private var _mutedReported:Boolean = false;
      
      private var _notificationType:String;
      
      public function show(param1:String) : void {
         var _loc2_:Sprite = null;
         this._notificationType = param1;
         this.removeAllChildren();
         switch(this._notificationType)
         {
            case PLAY:
               _loc2_ = this.playIcon;
               _loc2_.width = _loc2_.height = 120;
               break;
            case PAUSE:
               _loc2_ = this.pauseIcon;
               _loc2_.width = _loc2_.height = 120;
               break;
            case MUTE:
               _loc2_ = this.muteIcon;
               this._mutedReported = true;
               break;
            case UNMUTE:
               _loc2_ = this.unmuteIcon;
               break;
         }
         if(_loc2_)
         {
            addChild(_loc2_);
            _loc2_.alpha = 0.8;
            if(this._notificationType != MUTE)
            {
               this.fadeOutIcon(_loc2_);
            }
         }
      }
      
      public function hide() : void {
         var _loc1_:Sprite = null;
         if(numChildren)
         {
            _loc1_ = getChildAt(0) as Sprite;
            this.fadeOutIcon(_loc1_);
         }
      }
      
      private function fadeOutIcon(param1:DisplayObject) : void {
         if((param1) && (param1 == this.playIcon || param1 == this.pauseIcon))
         {
            Tween.to(param1,
               {
                  "alpha":0,
                  "width":80,
                  "height":80
               },30 * 1.5,"easeOutQuart",this.removeAllChildren);
         }
         else
         {
            Tween.to(param1,{"alpha":0},30 * 1.5,"easeOutQuart",this.removeAllChildren);
         }
      }
      
      private function removeAllChildren() : void {
         while(numChildren > 0)
         {
            Tween.kill(getChildAt(0));
            removeChildAt(0);
         }
      }
      
      public function get mutedReported() : Boolean {
         return this._mutedReported;
      }
      
      public function get notificationType() : String {
         return this._notificationType;
      }
   }
}
