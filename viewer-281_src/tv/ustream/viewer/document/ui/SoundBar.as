package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.events.MouseEvent;
   import flash.display.BlendMode;
   import flash.filters.DropShadowFilter;
   import tv.ustream.viewer.document.ui.icons.*;
   import flash.geom.*;
   import flash.text.TextFormat;
   import flash.filters.GlowFilter;
   import flash.display.StageDisplayState;
   import flash.events.Event;
   import flash.display.DisplayObject;
   import tv.ustream.tween2.Tween;
   import tv.ustream.viewer.document.ViewerUI;
   
   public class SoundBar extends OptionsPanel
   {
      
      public function SoundBar() {
         super();
         if(ViewerUI.touchMode)
         {
            triggerMode = OptionsPanel.TRIGGER_CLICK;
         }
         else
         {
            triggerMode = OptionsPanel.TRIGGER_OVER;
         }
         focusRect = false;
      }
      
      public static const VOLUME_CHANGE:String = "volumeChange";
      
      public static const TOGGLE_MUTE:String = "toggleMute";
      
      private var soundBtn:Button;
      
      private var _volume:Number;
      
      private var volumeSlider:Sprite;
      
      private var volumeBg:Sprite;
      
      private var volumeBar:Shape;
      
      private var volumeBtn:Button;
      
      private var volumeLabel:Label;
      
      private var _barColor:int = -1;
      
      override protected function buildUI() : void {
         addChild(this.soundBtn = new Button());
         this.soundBtn.icon = new SoundIcon();
         this.soundBtn._hitArea = new Rectangle(0,0,28,26);
         this.soundBtn.x = 0;
         this.soundBtn.y = -this.soundBtn.height / 2;
         this.soundBtn.colorizable = true;
         this.soundBtn.addEventListener(MouseEvent.CLICK,this.onSoundBtnClick);
         addChild(this.volumeSlider = new Sprite());
         this.volumeSlider.x = 28 / 2;
         this.volumeSlider.y = -13;
         this.volumeSlider.blendMode = BlendMode.LAYER;
         this.volumeSlider.addChild(this.volumeBg = new Sprite());
         this.volumeBg.buttonMode = true;
         this.volumeBg.mouseEnabled = true;
         var mtx:Matrix = new Matrix();
         mtx.createGradientBox(4,70,90 * Math.PI / 180,-2,-70);
         with(this.volumeBg)
         {
         }
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(-14,-70,28,70);
         graphics.endFill();
         graphics.beginGradientFill(GradientType.LINEAR,[7829367,6842472],[1,1],[0,255],mtx);
         graphics.drawRoundRect(-2,-70,4,70,4);
         graphics.endFill();
         }
         this.volumeBg.filters = [new DropShadowFilter(1,90,0,0.3,2,2,1,2,true)];
         this.volumeSlider.addChild(this.volumeBar = new Shape());
         this.volumeBar.filters = [new DropShadowFilter(1,90,0,0.3,2,2,1,2,true)];
         this.volumeSlider.addChild(this.volumeBtn = new Button());
         with(this.volumeBtn)
         {
         }
         icon = new SeekBtnIcon(7,2);
         _hitArea = new Rectangle(-7,-7,14,14);
         noGlow = true;
         }
         addChild(this.volumeLabel = new Label());
         this.volumeLabel.embedFonts = true;
         this.volumeLabel.textFormat = new TextFormat("MyriadCondBoldItalic",13,16777215);
         this.volumeLabel.textFilters = [new GlowFilter(0,0.18,2,2,5,1)];
         this.volumeLabel.textGradientOverlay = [16777215,0];
         this.volumeLabel.textGradientOverlayAlpha = [0.17,0.17];
         this.volumeLabel.bgHeight = 20;
         this.volumeLabel.bgColor = 0;
         this.volumeLabel.bgAlpha = 0;
         this.volumeLabel.bgCornerRadius = 0;
         this.volumeLabel.text = "68";
         this.volumeLabel.x = (28 - this.volumeLabel.width) / 2;
         this.volumeLabel.y = -13 - 70 - this.volumeLabel.height;
         this.volume = 0.7;
         tweenList.push(this.volumeLabel);
         tweenList.push(this.volumeSlider);
         trigger = this.soundBtn;
         setBg(28,this.volumeLabel.y - 4);
         this.volumeBtn.addEventListener(MouseEvent.MOUSE_DOWN,this.onMouseDown);
         this.volumeBg.addEventListener(MouseEvent.CLICK,this.setVolumeOnBar);
      }
      
      override protected function openPanel() : void {
         super.openPanel();
         addEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
      }
      
      override public function closePanel(param1:* = null) : void {
         removeEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
         super.closePanel(param1);
      }
      
      private function onMouseWheel(param1:MouseEvent) : void {
         if((stage) && stage.displayState == StageDisplayState.NORMAL)
         {
            this.volume = Math.max(0,Math.min(1,this.volume + param1.delta * 0.05));
         }
      }
      
      private function onMouseDown(param1:MouseEvent) : void {
         this.volumeBtn.startDrag(false,new Rectangle(0,-7 - (70 - 14),0,70 - 14));
         isDragging = true;
         stage.addEventListener(MouseEvent.MOUSE_MOVE,this.onSliderChange);
         stage.addEventListener(MouseEvent.MOUSE_UP,this.onStopDrag);
         stage.addEventListener(Event.MOUSE_LEAVE,this.onStopDrag);
      }
      
      private function onSliderChange(param1:MouseEvent) : void {
         this.volume = (Math.abs(this.volumeBtn.y) - 7) / (70 - 14);
         param1.updateAfterEvent();
      }
      
      private function onStopDrag(param1:*) : void {
         this.volumeBtn.stopDrag();
         isDragging = false;
         stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.onSliderChange);
         stage.removeEventListener(MouseEvent.MOUSE_UP,this.onStopDrag);
         stage.removeEventListener(Event.MOUSE_LEAVE,this.onStopDrag);
         this.volume = this.volume;
         if(param1.type == MouseEvent.MOUSE_UP && !(param1.target == null) && !contains(DisplayObject(param1.target)))
         {
            this.closePanel();
         }
      }
      
      private function setVolumeOnBar(param1:MouseEvent) : void {
         var _loc2_:Number = Math.max(0,Math.min(Math.abs(this.volumeBg.mouseY),70));
         Tween.to(this,{"volume":_loc2_ / 70},30 * 0.3,"easeOutQuart");
      }
      
      public function get volume() : Number {
         return this._volume;
      }
      
      public function set volume(param1:Number) : void {
         this.silentVolume = param1;
         dispatchEvent(new Event(SoundBar.VOLUME_CHANGE));
      }
      
      public function set silentVolume(param1:Number) : void {
         this._volume = param1;
         this.volumeLabel.text = String(Math.round(param1 * 100));
         this.volumeLabel.x = Math.round((28 - this.volumeLabel.width) / 2) - 1;
         this.createBar(this._barColor > -1?this._barColor:ViewerUI.style.color);
         if(!isDragging)
         {
            this.volumeBtn.y = -7 - Math.round((70 - 14) * param1);
         }
         this.soundBtn.icon.volume = param1;
         dispatchEvent(new Event(SoundBar.VOLUME_CHANGE));
      }
      
      private function onSoundBtnClick(param1:MouseEvent) : void {
         if(triggerMode == OptionsPanel.TRIGGER_OVER || triggerMode == OptionsPanel.TRIGGER_CLICK && (_opened))
         {
            dispatchEvent(new Event(TOGGLE_MUTE));
         }
      }
      
      override public function destroy() : void {
         super.destroy();
         this.soundBtn.removeEventListener(MouseEvent.CLICK,this.onSoundBtnClick);
      }
      
      override protected function updateStyle(... rest) : void {
         this.createBar(this._barColor > -1?this._barColor:ViewerUI.style.color);
         this.volumeBtn.icon.colorize(ViewerUI.style.color);
      }
      
      public function set barColor(param1:int) : void {
         this._barColor = param1;
         this.updateStyle();
      }
      
      private function createBar(param1:uint) : void {
         var _loc2_:Number = (this._volume) || (0);
         this.volumeBar.graphics.clear();
         this.volumeBar.graphics.beginFill(param1);
         this.volumeBar.graphics.drawRoundRect(-2,-7 - (70 - 14) * _loc2_,4,(70 - 14) * _loc2_ + 7,4);
         this.volumeBar.graphics.endFill();
      }
   }
}
