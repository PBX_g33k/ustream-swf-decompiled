package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import flash.display.Shape;
   import flash.text.TextFormat;
   import flash.text.StyleSheet;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.text.TextFieldAutoSize;
   import flash.text.AntiAliasType;
   import flash.display.DisplayObject;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.geom.Rectangle;
   import tv.ustream.tween2.Tween;
   
   public class Label extends Sprite
   {
      
      public function Label() {
         this._textFormat = new TextFormat();
         this._textFilters = [];
         this.textGradientOverlay = [];
         this.textGradientOverlayAlpha = [];
         this.bgGradientOverlay = [];
         this.bgGradientOverlayAlpha = [];
         this._bgFilters = [];
         super();
         mouseEnabled = false;
         mouseChildren = false;
      }
      
      protected var txt:TextField;
      
      protected var txtBmp:Shape;
      
      protected var _textFormat:TextFormat;
      
      protected var _styleSheet:StyleSheet;
      
      private var _textFilters:Array;
      
      public var textGradientOverlay:Array;
      
      public var textGradientOverlayAlpha:Array;
      
      private var _interactive:Boolean;
      
      private var _multiline:Boolean;
      
      private var _embedFonts:Boolean;
      
      private var _wordWrap:Boolean;
      
      public var padding:Number = 0;
      
      public var bg:Shape;
      
      public var bgHeight:Number;
      
      public var bgWidth:Number;
      
      public var bgColor:uint = 16737792;
      
      public var bgAlpha:Number = 0;
      
      public var bgGradientOverlay:Array;
      
      public var bgGradientOverlayAlpha:Array;
      
      private var _bgFilters:Array;
      
      public var bgCornerRadius:int = 0;
      
      private var _overSample:Boolean;
      
      public var ticker:Boolean;
      
      private var tickerBitmaps:Array;
      
      private var currentTickerBitmap:Bitmap;
      
      private var _textWidth:Number = 0;
      
      public var forcedHtmlText:Boolean = false;
      
      public function get text() : String {
         return this.txt?this._styleSheet?this.txt.htmlText:this.txt.text:"";
      }
      
      public function set text(param1:String) : void {
         var _loc2_:BitmapData = null;
         var _loc3_:Shape = null;
         if(!this.txt)
         {
            this.txt = new TextField();
            this.txt.selectable = false;
            this.txt.autoSize = TextFieldAutoSize.LEFT;
            this.txt.mouseEnabled = false;
            this.txt.embedFonts = this._embedFonts;
            this.txt.multiline = this._multiline;
            this.txt.wordWrap = this._wordWrap;
            if(this._embedFonts)
            {
               this.txt.antiAliasType = AntiAliasType.ADVANCED;
            }
            if(this._styleSheet)
            {
               this.txt.styleSheet = this._styleSheet;
            }
            else if(this._textFormat)
            {
               this.txt.defaultTextFormat = this._textFormat;
            }
            
            if(this._textWidth > 0 && (this._multiline) && (this._wordWrap))
            {
               this.txt.width = this._textWidth;
            }
         }
         if(this.ticker)
         {
            if(this.txt.text == param1)
            {
               return;
            }
            if(!this.tickerBitmaps)
            {
               this.tickerBitmaps = [];
            }
            if(!this.currentTickerBitmap)
            {
               addChild(this.currentTickerBitmap = new Bitmap());
            }
            if(this.txtTarget)
            {
               if(this.txtTarget.mask)
               {
                  _loc3_ = this.txtTarget.mask as Shape;
                  this.txtTarget.mask = null;
               }
               _loc2_ = new BitmapData((Math.ceil(this.txtTarget.width)) || 1,(Math.ceil(this.txtTarget.height)) || 1,true,0);
               _loc2_.draw(this.txtTarget);
               if(_loc3_)
               {
                  this.txtTarget.mask = _loc3_;
               }
               this.currentTickerBitmap.bitmapData = _loc2_;
               this.currentTickerBitmap.smoothing = true;
               this.currentTickerBitmap.alpha = 1;
            }
         }
         if((this._styleSheet) || (this.forcedHtmlText))
         {
            this.txt.htmlText = param1;
         }
         else
         {
            this.txt.text = param1;
         }
         this.txt.height = this.txt.height;
         this.render();
      }
      
      public function get styleSheet() : StyleSheet {
         return this._styleSheet;
      }
      
      public function set styleSheet(param1:StyleSheet) : void {
         this._styleSheet = param1;
         if(this.txt)
         {
            this.txt.styleSheet = this._styleSheet;
            this.text = this.text;
         }
      }
      
      public function get textFormat() : TextFormat {
         return this._textFormat;
      }
      
      public function set textFormat(param1:TextFormat) : void {
         this._textFormat = param1;
         if(this.txt)
         {
            this.txt.defaultTextFormat = this._textFormat;
            this.txt.setTextFormat(this._textFormat);
            this.text = this.text;
         }
      }
      
      public function get textWidth() : Number {
         return this._textWidth;
      }
      
      public function set textWidth(param1:Number) : void {
         this._textWidth = param1;
         if((this.txt && this._textWidth > 0) && (this._multiline) && (this._wordWrap))
         {
            this.txt.width = param1;
            this.txt.height = this.txt.height;
            this.render();
         }
      }
      
      public function get textFilters() : Array {
         return this._textFilters;
      }
      
      public function set textFilters(param1:Array) : void {
         this._textFilters = param1;
         if(this.txtTarget)
         {
            this.txtTarget.filters = this._textFilters;
         }
      }
      
      public function get multiline() : Boolean {
         return this._multiline;
      }
      
      public function set multiline(param1:Boolean) : void {
         this._multiline = param1;
         if(this.txt)
         {
            this.txt.multiline = param1;
         }
      }
      
      public function get wordWrap() : Boolean {
         return this._wordWrap;
      }
      
      public function set wordWrap(param1:Boolean) : void {
         this._wordWrap = param1;
         if(this.txt)
         {
            this.txt.wordWrap = param1;
         }
      }
      
      public function get embedFonts() : Boolean {
         return this._embedFonts;
      }
      
      public function set embedFonts(param1:Boolean) : void {
         this._embedFonts = param1;
         if(this._embedFonts)
         {
            this._overSample = false;
         }
         if(this.txt)
         {
            this.txt.embedFonts = param1;
            if(this._embedFonts)
            {
               this.txt.antiAliasType = AntiAliasType.ADVANCED;
            }
            this.render();
         }
      }
      
      public function get numLines() : int {
         return this.txt?this.txt.numLines:0;
      }
      
      public function get overSample() : Boolean {
         return this._overSample;
      }
      
      public function set overSample(param1:Boolean) : void {
         if(this._embedFonts)
         {
            this._overSample = false;
         }
         else
         {
            this._overSample = param1;
         }
         if(this.txtTarget)
         {
            this.render();
         }
      }
      
      public function get bgFilters() : Array {
         return this._bgFilters;
      }
      
      public function set bgFilters(param1:Array) : void {
         this._bgFilters = param1;
         if(this.bg)
         {
            this.bg.filters = this._bgFilters;
         }
      }
      
      public function get txtTarget() : DisplayObject {
         return this.txtBmp?this.txtBmp:(this.txt) && (contains(this.txt))?this.txt:null;
      }
      
      public function get interactive() : Boolean {
         return this._interactive;
      }
      
      public function set interactive(param1:Boolean) : void {
         this._interactive = param1;
         if(this.txtTarget)
         {
            this.render();
         }
      }
      
      override public function get height() : Number {
         return (this.ticker) && (this.txtTarget)?this.txtTarget.height:super.height;
      }
      
      protected function renderBg(param1:Number, param2:Number) : void {
         var _loc3_:Array = null;
         var _loc4_:Matrix = null;
         var _loc5_:* = 0;
         if(!this.bg)
         {
            addChildAt(this.bg = new Shape(),0);
         }
         if((this.bg) && (contains(this.bg)))
         {
            if(isNaN(param1))
            {
               param1 = 0;
            }
            if(isNaN(param2))
            {
               param2 = 0;
            }
            this.bg.graphics.clear();
            this.bg.graphics.beginFill(this.bgColor,this.bgAlpha);
            this.bg.graphics.drawRoundRect(0,0,param1,param2,this.bgCornerRadius * 2);
            this.bg.graphics.endFill();
            if((this.bgGradientOverlay) && this.bgGradientOverlay.length > 0)
            {
               if(!this.bgGradientOverlayAlpha || this.bgGradientOverlayAlpha.length == 0)
               {
                  this.bgGradientOverlayAlpha = [];
                  _loc5_ = 0;
                  while(_loc5_ < this.bgGradientOverlay.length)
                  {
                     this.bgGradientOverlayAlpha.push(0.33);
                     _loc5_++;
                  }
               }
               _loc3_ = [];
               _loc5_ = 0;
               while(_loc5_ < this.bgGradientOverlay.length)
               {
                  _loc3_.push(255 / (this.bgGradientOverlay.length - 1) * _loc5_);
                  _loc5_++;
               }
               _loc4_ = new Matrix();
               _loc4_.createGradientBox(param1,param2,90 * Math.PI / 180);
               this.bg.graphics.beginGradientFill(GradientType.LINEAR,this.bgGradientOverlay,this.bgGradientOverlayAlpha,_loc3_,_loc4_);
               this.bg.graphics.drawRoundRect(0,0,param1,param2,this.bgCornerRadius * 2);
               this.bg.graphics.endFill();
            }
            this.bg.filters = this.bgFilters || [];
         }
      }
      
      protected function render() : void {
         var bmp:BitmapData = null;
         var opaqueRect:Rectangle = null;
         var mtx:Matrix = null;
         var holder:Sprite = null;
         var _bg:Shape = null;
         var _mask:Shape = null;
         var ratios:Array = null;
         var color:uint = 0;
         var i:int = 0;
         var maskTargets:Array = null;
         var rectb:Rectangle = null;
         var msk:Shape = null;
         if(!this.txt)
         {
            return;
         }
         if(this.txtTarget)
         {
            this.txtTarget.filters = [];
         }
         var txtWidth:Number = Math.round(this.txt.width);
         var txtHeight:Number = Math.round(this.txt.height);
         var osRatio:Number = 2;
         if(this._overSample)
         {
            this.txt.scaleX = this.txt.scaleY = osRatio;
            txtWidth = Math.ceil(this.txt.width);
            txtHeight = Math.ceil(this.txt.height);
            this.txt.scaleX = this.txt.scaleY = 1;
         }
         if((this.textGradientOverlay) && this.textGradientOverlay.length > 0)
         {
            if(!this._styleSheet)
            {
               color = uint(this._textFormat.color);
               this._textFormat.color = 8421504;
               this.txt.setTextFormat(this._textFormat);
               this._textFormat.color = color;
            }
            bmp = new BitmapData(txtWidth,txtHeight,true,16777215);
            bmp.draw(this.txt,this._overSample?new Matrix(osRatio,0,0,osRatio):null);
            opaqueRect = bmp.getColorBoundsRect(4.27819008E9,0,false);
            mtx = new Matrix();
            mtx.createGradientBox(opaqueRect.width,opaqueRect.height,90 * Math.PI / 180,opaqueRect.x,opaqueRect.y);
            holder = new Sprite();
            _bg = new Shape();
            _mask = new Shape();
            if(!this.textGradientOverlayAlpha || this.textGradientOverlay.length == 0)
            {
               this.textGradientOverlayAlpha = [];
               i = 0;
               while(i < this.textGradientOverlay.length)
               {
                  this.textGradientOverlay.push(0.33);
                  i++;
               }
            }
            ratios = [];
            i = 0;
            while(i < this.textGradientOverlay.length)
            {
               ratios.push(255 / (this.textGradientOverlay.length - 1) * i);
               i++;
            }
            _bg.graphics.clear();
            if(this._styleSheet)
            {
               _bg.graphics.beginBitmapFill(bmp,null,false,true);
            }
            else
            {
               _bg.graphics.beginFill(uint(this._textFormat.color));
            }
            _bg.graphics.drawRect(0,0,bmp.width,bmp.height);
            _bg.graphics.endFill();
            _bg.graphics.beginGradientFill(GradientType.LINEAR,this.textGradientOverlay,this.textGradientOverlayAlpha,ratios,mtx);
            _bg.graphics.drawRect(opaqueRect.x,opaqueRect.y,opaqueRect.width,opaqueRect.height);
            _bg.graphics.endFill();
            _mask.graphics.clear();
            _mask.graphics.beginBitmapFill(bmp,null,false,true);
            _mask.graphics.drawRect(0,0,bmp.width,bmp.height);
            _mask.graphics.endFill();
            holder.addChild(_bg);
            holder.addChild(_mask);
            _bg.cacheAsBitmap = _mask.cacheAsBitmap = true;
            _bg.mask = _mask;
            bmp = new BitmapData(_bg.width,_bg.height,true,16777215);
            bmp.draw(holder);
            if(contains(this.txt))
            {
               removeChild(this.txt);
            }
            if(!this.txtBmp)
            {
               addChild(this.txtBmp = new Shape());
            }
            this.txtBmp.graphics.clear();
            this.txtBmp.graphics.beginBitmapFill(bmp,this._overSample?new Matrix(1 / osRatio,0,0,1 / osRatio):null,false,true);
            this.txtBmp.graphics.drawRect(0,0,Math.ceil(bmp.width / (this._overSample?osRatio:1)),Math.ceil(bmp.height / (this._overSample?osRatio:1)));
            this.txtBmp.graphics.endFill();
         }
         else if(this.ticker)
         {
            if(contains(this.txt))
            {
               removeChild(this.txt);
            }
            if(!this.txtBmp)
            {
               addChild(this.txtBmp = new Shape());
            }
            bmp = new BitmapData(txtWidth,txtHeight,true,0);
            bmp.draw(this.txt,this._overSample?new Matrix(osRatio,0,0,osRatio):null);
            opaqueRect = bmp.getColorBoundsRect(4.27819008E9,0,false);
            this.txtBmp.graphics.clear();
            this.txtBmp.graphics.beginBitmapFill(bmp,this._overSample?new Matrix(1 / osRatio,0,0,1 / osRatio):null,false,true);
            this.txtBmp.graphics.drawRect(0,0,Math.ceil(bmp.width / (this._overSample?osRatio:1)),Math.ceil(bmp.height / (this._overSample?osRatio:1)));
            this.txtBmp.graphics.endFill();
            maskTargets = [this.currentTickerBitmap,this.txtBmp];
            i = 0;
            while(i < maskTargets.length)
            {
               if(maskTargets[i].mask)
               {
                  removeChild(maskTargets[i].mask);
                  maskTargets[i].mask = null;
               }
               if(maskTargets[i] is Bitmap)
               {
                  if(Bitmap(maskTargets[i]).bitmapData)
                  {
                     rectb = Bitmap(maskTargets[i]).bitmapData.getColorBoundsRect(4.27819008E9,0,false);
                  }
                  else
                  {
                     break;
                  }
               }
               else
               {
                  rectb = opaqueRect;
               }
               msk = maskTargets[i].mask?maskTargets[i].mask:new Shape();
               msk.graphics.beginFill(0);
               msk.graphics.drawRect(Math.floor(rectb.x / (this._overSample?osRatio:1)),Math.floor(rectb.y / (this._overSample?osRatio:1)),Math.ceil(rectb.width / (this._overSample?osRatio:1)),Math.ceil(rectb.height / (this._overSample?osRatio:1)));
               msk.graphics.endFill();
               maskTargets[i].mask = msk;
               addChildAt(msk,getChildIndex(maskTargets[i]));
               i++;
            }
            this.currentTickerBitmap.y = this.txtBmp.y;
            this.txtBmp.y = this.txtBmp.y - this.txtBmp.height;
            this.txtBmp.alpha = 0;
            Tween.to(this.txtBmp,
               {
                  "y":0,
                  "alpha":1
               },30 * 0.6,"easeInOutQuart");
            Tween.kill(this.currentTickerBitmap);
            Tween.to(this.currentTickerBitmap,
               {
                  "y":this.currentTickerBitmap.height,
                  "alpha":0
               },30 * 0.6,"easeInOutQuart",function():void
            {
               if((currentTickerBitmap) && (currentTickerBitmap.bitmapData))
               {
                  currentTickerBitmap.bitmapData.dispose();
               }
            });
         }
         else if(this._overSample)
         {
            if(contains(this.txt))
            {
               removeChild(this.txt);
            }
            if(!this.txtBmp)
            {
               addChild(this.txtBmp = new Shape());
            }
            bmp = new BitmapData(txtWidth,txtHeight,true,16737792);
            bmp.draw(this.txt,new Matrix(osRatio,0,0,osRatio),null,null,null,true);
            this.txtBmp.graphics.clear();
            this.txtBmp.graphics.beginBitmapFill(bmp,new Matrix(1 / osRatio,0,0,1 / osRatio),false,true);
            this.txtBmp.graphics.drawRect(0,0,Math.ceil(txtWidth / osRatio),Math.ceil(txtHeight / osRatio));
            this.txtBmp.graphics.endFill();
         }
         else
         {
            if(!contains(this.txt))
            {
               addChild(this.txt);
            }
            if(this.txtBmp)
            {
               removeChild(this.txtBmp);
               this.txtBmp = null;
            }
         }
         
         
         var bgW:Number = (this.bgWidth) || (Math.ceil(this.txtTarget.width) + this.padding * 2);
         var bgH:Number = (this.bgHeight) || (Math.ceil(this.txtTarget.height));
         this.renderBg(bgW,bgH);
         var xOffset:Number = 0;
         var yOffset:Number = 0;
         if((this._embedFonts) && (this._textFormat) && !(this._textFormat.font.indexOf("Myriad") == -1))
         {
            xOffset = -1;
            yOffset = 1;
         }
         this.txtTarget.x = this.bgWidth?Math.round((this.bgWidth - this.txtTarget.width) / 2):this.bg?this.padding:0;
         if(this.txtTarget.x > 0)
         {
            this.txtTarget.x = this.txtTarget.x + xOffset;
         }
         this.txtTarget.y = this.bg?Math.round((bgH - this.txtTarget.height) / 2):0;
         if(this.txtTarget.y > 0)
         {
            this.txtTarget.y = this.txtTarget.y + yOffset;
         }
         this.txtTarget.filters = this.textFilters || [];
         if(this._interactive)
         {
            if(this.txtTarget != this.txt)
            {
               addChild(this.txt);
               this.txt.alpha = 0;
            }
            this.txt.mouseEnabled = mouseEnabled = mouseChildren = true;
         }
         else
         {
            this.txt.alpha = 1;
         }
      }
   }
}
