package tv.ustream.viewer.document.ui
{
   import flash.display.Shape;
   import flash.geom.Rectangle;
   import flash.events.MouseEvent;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.filters.DropShadowFilter;
   import flash.events.Event;
   import tv.ustream.tween2.Tween;
   import flash.utils.clearTimeout;
   import flash.utils.setTimeout;
   import flash.geom.Point;
   import flash.display.DisplayObject;
   import flash.filters.GlowFilter;
   import flash.geom.ColorTransform;
   import tv.ustream.tween2.Color;
   
   public class Button extends Label
   {
      
      public function Button() {
         this.tweenControl = {};
         super();
         this.enabled = true;
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private var _enabled:Boolean;
      
      private var __hitArea:Shape;
      
      private var _icon;
      
      private var tweenControl:Object;
      
      public var noGlow:Boolean;
      
      public var glowColor:uint = 16777215;
      
      public var textAlign:String = "center";
      
      private var _tooltipLabel:String;
      
      private var _tooltip:ToolTip;
      
      private var _tooltipYOffset:Number = 0;
      
      private var _tooltipStyle:Object;
      
      private var _autoHideTooltip:Boolean = true;
      
      private var _maxTooltipAlpha:Number = 1;
      
      public var tooltipDelay:int = 0;
      
      private var _blinkRepeat:int = 3;
      
      private var _blinkCount:int = 0;
      
      private var _blinkAlpha:Number = 1;
      
      private var _blinkDir:Number = -1;
      
      private var _blinking:Boolean;
      
      private var _hidden:Boolean;
      
      protected var _colorizable:Boolean = false;
      
      public function get tooltipInstance() : ToolTip {
         return this._tooltip;
      }
      
      public function get icon() : * {
         return this._icon;
      }
      
      public function set icon(param1:*) : void {
         if((this._icon) && (this.contains(this._icon)))
         {
            removeChild(this._icon);
         }
         this._icon = param1;
         addChildAt(this._icon,this.__hitArea?1:0);
         this.colorize();
      }
      
      public function get label() : String {
         return text;
      }
      
      public function set label(param1:String) : void {
         this.text = param1;
         this.colorize();
      }
      
      override public function set text(param1:String) : void {
         super.text = param1;
         this.colorize();
      }
      
      public function get _hitArea() : Rectangle {
         return this.__hitArea?new Rectangle(this.__hitArea.x,this.__hitArea.y,this.__hitArea.width,this.__hitArea.height):new Rectangle();
      }
      
      public function set _hitArea(param1:Rectangle) : void {
         var r:Rectangle = param1;
         if(!this.__hitArea)
         {
            addChildAt(this.__hitArea = new Shape(),0);
         }
         if(!r)
         {
            this.__hitArea.graphics.clear();
            return;
         }
         with(this.__hitArea)
         {
         }
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(r.x,r.y,r.width,r.height);
         graphics.endFill();
         }
         if(this.icon)
         {
            with(this.icon)
            {
            }
            x = r.x + r.width / 2;
            y = r.y + r.height / 2;
            }
         }
         if(txtTarget)
         {
            txtTarget.x = this.textAlign == "left"?padding:r.x + Math.round((r.width - txtTarget.width) / 2);
            txtTarget.y = r.y + Math.round((r.height - txtTarget.height) / 2);
         }
         if(bg)
         {
            bg.x = r.x + Math.round((r.width - bg.width) / 2);
            bg.y = r.y + Math.round((r.height - bg.height) / 2);
         }
      }
      
      override public function set interactive(param1:Boolean) : void {
         this.enabled = !param1;
         super.interactive = param1;
      }
      
      public function get enabled() : Boolean {
         return this._enabled;
      }
      
      public function set enabled(param1:Boolean) : void {
         mouseEnabled = buttonMode = useHandCursor = this._enabled = param1;
         mouseChildren = !param1;
         if(param1)
         {
            addEventListener(MouseEvent.MOUSE_OVER,this.handleMouseEvents);
            addEventListener(MouseEvent.MOUSE_OUT,this.handleMouseEvents);
         }
         else
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.handleMouseEvents);
            removeEventListener(MouseEvent.MOUSE_OUT,this.handleMouseEvents);
         }
      }
      
      public function get tooltipStyle() : Object {
         return this._tooltipStyle;
      }
      
      public function set tooltipStyle(param1:Object) : void {
         var _loc2_:String = null;
         if(!this._tooltipStyle)
         {
            this._tooltipStyle = {};
            this._tooltipStyle.textFormat = new TextFormat(ViewerUI.baseFontName,11,16777215);
            this._tooltipStyle.embedFonts = ViewerUI.baseFontEmbedded;
            this._tooltipStyle.overSample = !ViewerUI.baseFontEmbedded;
            this._tooltipStyle.textFilters = [new DropShadowFilter(1,90,0,0.39,1,1,1,2)];
            this._tooltipStyle.padding = 6;
            this._tooltipStyle.bgHeight = 24;
            this._tooltipStyle.bgColor = ViewerUI.style.color;
            this._tooltipStyle.bgAlpha = 0.9;
            this._tooltipStyle.bgGradientOverlay = [16777215,16777215];
            this._tooltipStyle.bgGradientOverlayAlpha = [0.12,0];
            this._tooltipStyle.bgCornerRadius = 3;
         }
         for(_loc2_ in param1)
         {
            this._tooltipStyle[_loc2_] = param1[_loc2_];
         }
         if(this._tooltip)
         {
            for(_loc2_ in this._tooltipStyle)
            {
               this._tooltip[_loc2_] = this._tooltipStyle[_loc2_];
            }
            this._tooltip.text = this._tooltip.text;
         }
      }
      
      public function get autoHideTooltip() : Boolean {
         return this._autoHideTooltip;
      }
      
      public function set autoHideTooltip(param1:Boolean) : void {
         this._autoHideTooltip = param1;
         if((this._tooltip) && (stage))
         {
            this.addToolTip();
         }
      }
      
      public function get tooltip() : String {
         return this._tooltipLabel;
      }
      
      public function set tooltip(param1:String) : void {
         if(this._tooltipLabel == param1)
         {
            return;
         }
         this._tooltipLabel = param1;
         if(param1)
         {
            if(!this._tooltip)
            {
               this._tooltip = new ToolTip();
               this._tooltip.visible = false;
               this._tooltip.alpha = 0;
               this.tooltipStyle = this._tooltipStyle || {};
               this.tweenControl.tooltip = 
                  {
                     "alpha":0,
                     "yOffset":-10
                  };
               if(stage)
               {
                  this.addToolTip();
               }
               addEventListener(Event.ADDED_TO_STAGE,this.addToolTip);
               addEventListener(Event.REMOVED_FROM_STAGE,this.removeToolTip);
            }
            this._tooltip.text = param1;
         }
         else if(this._tooltip)
         {
            this.destroyToolTip();
         }
         
      }
      
      private function destroyToolTip() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.addToolTip);
         removeEventListener(Event.REMOVED_FROM_STAGE,this.removeToolTip);
         Tween.kill(this.tweenControl.tooltip);
         removeEventListener(Event.ENTER_FRAME,this.positionTooltip);
         if(stage)
         {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.positionTooltip);
         }
         this.removeToolTip();
         this._tooltip = null;
      }
      
      private function addToolTip(param1:Event = null) : void {
         if(this._autoHideTooltip)
         {
            stage.addChild(this._tooltip);
         }
         else
         {
            parent.addChild(this._tooltip);
         }
         this.positionTooltip();
         if(!this._autoHideTooltip)
         {
            this.showTooltip();
         }
      }
      
      private function removeToolTip(param1:Event = null) : void {
         if(this._tooltip.parent)
         {
            this._tooltip.parent.removeChild(this._tooltip);
         }
      }
      
      private function openTooltip() : void {
         if(stage)
         {
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.positionTooltip);
         }
         Tween.kill(this.tweenControl.tooltip);
         Tween.to(this.tweenControl.tooltip,
            {
               "alpha":this._maxTooltipAlpha,
               "yOffset":0
            },30 * 0.3,"easeOutExpo",function():void
         {
            if(_autoHideTooltip)
            {
               addEventListener(Event.ENTER_FRAME,positionTooltip);
            }
         },null,this.positionTooltip);
      }
      
      public function showTooltip(param1:int = 0) : void {
         if(!this._tooltip || !this.isParentVisible(this))
         {
            return;
         }
         if(this._tooltip.parent)
         {
            this._tooltip.parent.addChild(this._tooltip);
         }
         clearTimeout(this.tweenControl.tooltip.timeout);
         if(param1)
         {
            this.tweenControl.tooltip.timeout = setTimeout(this.openTooltip,1000 / 30 * param1);
         }
         else
         {
            this.openTooltip();
         }
      }
      
      public function hideTooltip() : void {
         if(!this._tooltip || (this._tooltip) && (!this._autoHideTooltip))
         {
            return;
         }
         clearTimeout(this.tweenControl.tooltip.timeout);
         Tween.kill(this.tweenControl.tooltip);
         Tween.to(this.tweenControl.tooltip,
            {
               "alpha":0,
               "yOffset":-10
            },30 * 0.6,"easeOutExpo",function():void
         {
            if(stage)
            {
               stage.removeEventListener(MouseEvent.MOUSE_MOVE,positionTooltip);
            }
            removeEventListener(Event.ENTER_FRAME,positionTooltip);
         },null,this.positionTooltip);
         dispatchEvent(new Event("ButtonHideToolTip"));
      }
      
      private function positionTooltip(param1:Event = null) : void {
         if(!stage)
         {
            removeEventListener(Event.ENTER_FRAME,this.positionTooltip);
            return;
         }
         var _loc2_:Rectangle = this.getBounds(this);
         var _loc3_:Point = this._tooltip.parent.globalToLocal(localToGlobal(new Point(Math.round(_loc2_.x + _loc2_.width / 2),_loc2_.y - 4)));
         this._tooltip.x = _loc3_.x;
         this._tooltip.y = _loc3_.y + this.tweenControl.tooltip.yOffset;
         this._tooltip.alpha = this.tweenControl.tooltip.alpha;
         this._tooltip.visible = this._tooltip.alpha > 0.05;
         var _loc4_:Number = 8;
         if(this._tooltip.x - Math.round(this._tooltip.width / 2) < _loc4_)
         {
            this._tooltip.xOffset = _loc4_ - (this._tooltip.x - Math.round(this._tooltip.width / 2));
         }
         else if(this._tooltip.x + Math.round(this._tooltip.width / 2) > stage.stageWidth - _loc4_)
         {
            this._tooltip.xOffset = stage.stageWidth - _loc4_ - (this._tooltip.x + Math.round(this._tooltip.width / 2));
         }
         else
         {
            this._tooltip.xOffset = 0;
         }
         
         if((param1) && param1 is MouseEvent)
         {
            MouseEvent(param1).updateAfterEvent();
         }
      }
      
      public function getTooltipBounds(param1:DisplayObject) : Rectangle {
         return (this._tooltip) && (param1)?this._tooltip.getBounds(param1):null;
      }
      
      public function get maxTooltipAlpha() : Number {
         return this._maxTooltipAlpha;
      }
      
      public function set maxTooltipAlpha(param1:Number) : void {
         this._maxTooltipAlpha = Math.max(0,Math.min(1,param1));
      }
      
      override public function set visible(param1:Boolean) : void {
         if(visible == param1 || (this.hidden))
         {
            return;
         }
         super.visible = param1;
         if(!param1)
         {
            if(this._blinking)
            {
               this.blink(0);
            }
            if(this._tooltip)
            {
               this.hideTooltip();
            }
         }
      }
      
      override public function set x(param1:Number) : void {
         if(x == param1)
         {
            return;
         }
         super.x = param1;
         if(this._tooltip)
         {
            this.positionTooltip();
         }
      }
      
      override public function set y(param1:Number) : void {
         if(y == param1)
         {
            return;
         }
         super.y = param1;
         if(this._tooltip)
         {
            this.positionTooltip();
         }
      }
      
      public function blink(param1:int = 4) : void {
         if(param1 < 1)
         {
            removeEventListener(Event.ENTER_FRAME,this.onBlink);
            this.alpha = 1;
            this._blinking = false;
         }
         else
         {
            this._blinkRepeat = param1;
            this._blinkCount = 0;
            this._blinkAlpha = 1;
            this._blinkDir = -1;
            this._blinking = true;
            addEventListener(Event.ENTER_FRAME,this.onBlink);
         }
      }
      
      private function onBlink(param1:Event) : void {
         this._blinkAlpha = this._blinkAlpha + 0.1 * this._blinkDir;
         this.alpha = Math.max(0.1,Math.min(1,this._blinkAlpha));
         if(this._blinkAlpha <= 0.1)
         {
            this._blinkDir = 1;
         }
         if(this._blinkAlpha >= 1)
         {
            this._blinkDir = -1;
            this._blinkCount++;
            if(this._blinkCount == this._blinkRepeat)
            {
               removeEventListener(Event.ENTER_FRAME,this.onBlink);
               this._blinking = false;
            }
         }
      }
      
      protected function handleMouseEvents(param1:MouseEvent) : void {
         var e:MouseEvent = param1;
         if(this._tooltip)
         {
            switch(e.type)
            {
               case MouseEvent.MOUSE_OVER:
                  this.showTooltip(this.tooltipDelay);
                  break;
               case MouseEvent.MOUSE_OUT:
                  this.hideTooltip();
                  break;
            }
         }
         switch(e.type)
         {
            case MouseEvent.MOUSE_OVER:
               if(this._blinking)
               {
                  removeEventListener(Event.ENTER_FRAME,this.onBlink);
                  this.alpha = 1;
               }
               if(!this.noGlow)
               {
                  if(this.icon)
                  {
                     if(!this.tweenControl.icon)
                     {
                        this.tweenControl.icon = 
                           {
                              "filters":this.icon.filters,
                              "glowBlur":0,
                              "glowAlpha":0
                           };
                     }
                     Tween.kill(this.tweenControl.icon);
                     Tween.to(this.tweenControl.icon,
                        {
                           "glowAlpha":0.55,
                           "glowBlur":6
                        },30 * 0.2,"easeOutExpo",null,null,this.onTweenUpdate);
                     if(this.icon.hasOwnProperty("onMouseOver"))
                     {
                        this.icon.onMouseOver();
                     }
                  }
                  if(txt)
                  {
                     if(!this.tweenControl._txt)
                     {
                        this.tweenControl._txt = 
                           {
                              "filters":textFilters,
                              "glowBlur":0,
                              "glowAlpha":0
                           };
                     }
                     Tween.kill(this.tweenControl._txt);
                     Tween.to(this.tweenControl._txt,
                        {
                           "glowAlpha":0.55,
                           "glowBlur":6
                        },30 * 0.2,"easeOutExpo",null,null,this.onTweenUpdate);
                  }
               }
               if(this._colorizable)
               {
                  if(!this.tweenControl.tint)
                  {
                     this.tweenControl.tint = 1;
                  }
                  Tween.to(this.tweenControl,{"tint":0},30 * 0.2,"easeOutExpo",null,null,this.onTweenUpdate);
               }
               break;
            case MouseEvent.MOUSE_OUT:
               if(!this.noGlow)
               {
                  if(this.icon)
                  {
                     Tween.kill(this.tweenControl.icon);
                     Tween.to(this.tweenControl.icon,
                        {
                           "glowAlpha":0,
                           "glowBlur":0
                        },30 * 0.8,"easeOutQuart",function():void
                     {
                        icon.filters = tweenControl.icon.filters;
                     },null,this.onTweenUpdate);
                     if(this.icon.hasOwnProperty("onMouseOut"))
                     {
                        this.icon.onMouseOut();
                     }
                  }
                  if(txt)
                  {
                     Tween.kill(this.tweenControl._txt);
                     Tween.to(this.tweenControl._txt,
                        {
                           "glowAlpha":0,
                           "glowBlur":0
                        },30 * 0.8,"easeOutQuart",function():void
                     {
                        textFilters = tweenControl._txt.filters;
                     },null,this.onTweenUpdate);
                  }
               }
               if(this._colorizable)
               {
                  Tween.to(this.tweenControl,{"tint":1},30 * 0.8,"easeOutQuart");
               }
               break;
         }
      }
      
      private function onTweenUpdate() : void {
         var _loc1_:Array = null;
         if(this.tweenControl.icon)
         {
            _loc1_ = [].concat(this.tweenControl.icon.filters);
            _loc1_.unshift(new GlowFilter(this.glowColor,this.tweenControl.icon.glowAlpha,this.tweenControl.icon.glowBlur,this.tweenControl.icon.glowBlur,2,3));
            this.icon.filters = _loc1_;
         }
         if(this.tweenControl._txt)
         {
            _loc1_ = [].concat(this.tweenControl._txt.filters);
            _loc1_.unshift(new GlowFilter(this.glowColor,this.tweenControl._txt.glowAlpha,this.tweenControl._txt.glowBlur,this.tweenControl._txt.glowBlur,2,3));
            textFilters = _loc1_;
         }
         this.colorize();
      }
      
      public function destroy() : void {
         this.enabled = false;
         if(this.tweenControl.icon)
         {
            Tween.kill(this.tweenControl.icon);
         }
         if(this.tweenControl._txt)
         {
            Tween.kill(this.tweenControl._txt);
         }
         if(this._colorizable)
         {
            Tween.kill(this.tweenControl);
         }
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
         if(this._tooltip)
         {
            this.destroyToolTip();
         }
         removeEventListener(Event.ENTER_FRAME,this.onBlink);
      }
      
      override public function get alpha() : Number {
         return super.alpha;
      }
      
      override public function set alpha(param1:Number) : void {
         this.visible = param1 < 0.01?false:true;
         super.alpha = param1;
      }
      
      public function get colorizable() : Boolean {
         return this._colorizable;
      }
      
      public function set colorizable(param1:Boolean) : void {
         this._colorizable = param1;
         this.tweenControl.tint = 1;
         this.colorize();
      }
      
      public function get hidden() : Boolean {
         return this._hidden;
      }
      
      public function set hidden(param1:Boolean) : void {
         this._hidden = param1;
         super.visible = !param1;
         if(param1)
         {
            if(this._blinking)
            {
               this.blink(0);
            }
            if(this._tooltip)
            {
               this.hideTooltip();
            }
         }
      }
      
      private function colorize() : void {
         if(this._colorizable)
         {
            if(ViewerUI.style.color == ViewerUI.defaultStyle.color)
            {
               if(this._icon)
               {
                  DisplayObject(this._icon.hasOwnProperty("shape")?this._icon.shape:this._icon).transform.colorTransform = new ColorTransform();
               }
               if(txtTarget)
               {
                  txtTarget.transform.colorTransform = new ColorTransform();
               }
            }
            else
            {
               if(this._icon)
               {
                  Color.tint(this._icon.hasOwnProperty("shape")?this._icon.shape:this._icon,ViewerUI.style.color,this.tweenControl.tint);
               }
               if(txtTarget)
               {
                  Color.tint(txtTarget,ViewerUI.style.color,this.tweenControl.tint);
               }
            }
         }
      }
      
      protected function updateStyle(... rest) : void {
         this.colorize();
         this.tooltipStyle = {"bgColor":ViewerUI.style.color};
      }
      
      protected function isParentVisible(param1:DisplayObject) : Boolean {
         if(stage)
         {
            if(param1 == stage || (param1.parent) && (param1.parent == stage))
            {
               return true;
            }
            if((param1.parent) && (param1.parent.visible) && (param1.parent.alpha))
            {
               return this.isParentVisible(param1.parent);
            }
            return false;
         }
         return false;
      }
   }
}
