package tv.ustream.viewer.document.ui
{
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.Event;
   
   public class RadioButton extends CheckBox
   {
      
      public function RadioButton(param1:Boolean = false) {
         super(param1);
      }
      
      override protected function renderCustomContent() : void {
         icon.graphics.clear();
         icon.graphics.beginFill(_iconBgColor);
         icon.graphics.drawCircle(9,9,9);
         icon.graphics.endFill();
         indicator.graphics.clear();
         indicator.x = icon.width / 2;
         indicator.y = icon.height / 2;
         indicator.graphics.beginFill(ViewerUI.style.color);
         indicator.graphics.drawCircle(0,0,4);
         indicator.graphics.endFill();
      }
      
      override protected function onMouseClick(param1:Event) : void {
         if(!checked)
         {
            checked = true;
            dispatchEvent(new Event(Event.CHANGE));
         }
      }
   }
}
