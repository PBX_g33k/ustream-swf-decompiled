package tv.ustream.viewer.document.ui
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class StatusLabel_LogoAsset extends SpriteAsset
   {
      
      public function StatusLabel_LogoAsset() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
