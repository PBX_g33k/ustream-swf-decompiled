package tv.ustream.viewer.document.ui
{
   import flash.display.Loader;
   import flash.net.URLRequest;
   import flash.system.LoaderContext;
   import flash.events.IOErrorEvent;
   import tv.ustream.tools.Debug;
   import flash.events.Event;
   
   public class ThumbnailLoader extends Loader
   {
      
      public function ThumbnailLoader(param1:String, param2:Boolean = false) {
         super();
         this._url = param1;
         this.contentLoaderInfo.addEventListener(Event.COMPLETE,this.onComplete);
         this.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.onError);
         this.addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         if(param2)
         {
            this.start();
         }
      }
      
      private const THUMBNAIL_SITE_SIZE:Array = ["640x360","320x180","256x144","192x108","128x72"];
      
      private var _index:uint = 0;
      
      private var _url:String;
      
      public function start() : void {
         var _loc1_:String = this._url.split("#SIZE#").join(this.THUMBNAIL_SITE_SIZE[this._index]);
         var _loc2_:URLRequest = new URLRequest(_loc1_);
         var _loc3_:LoaderContext = new LoaderContext(true);
         super.load(_loc2_,_loc3_);
      }
      
      private function onError(param1:IOErrorEvent) : void {
         Debug.error("[ Viewer - ImageLoader ] " + param1.toString() + "[" + this._index + "]");
         this._index++;
         if(this._index < this.THUMBNAIL_SITE_SIZE.length)
         {
            this.start();
         }
         else
         {
            dispatchEvent(new Event(Event.UNLOAD));
         }
      }
      
      private function onComplete(param1:Event) : void {
         dispatchEvent(new Event(Event.COMPLETE));
      }
      
      private function onAddedToStage(param1:Event) : void {
         this.removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         this.addEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         removeEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         this.addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
      }
      
      private function clear() : void {
         this.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onComplete);
         this.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onError);
         this.removeEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         this.removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         this.unload();
      }
   }
}
