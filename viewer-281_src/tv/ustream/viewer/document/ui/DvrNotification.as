package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.filters.DropShadowFilter;
   import tv.ustream.localization.Locale;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tween2.Tween;
   import flash.events.TimerEvent;
   import flash.geom.Matrix;
   import flash.display.BitmapData;
   import flash.display.GradientType;
   import tv.ustream.tools.Debug;
   
   public class DvrNotification extends Sprite
   {
      
      public function DvrNotification(param1:Number = 0) {
         super();
         this._time = param1;
         visible = false;
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
      }
      
      public static const DVR_PLAY:String = "DvrNotification.dvrPlay";
      
      private var _h:Number = 32;
      
      private var _width:Number = 0;
      
      private var _time:Number;
      
      private var dvrLabelText:String = "<<< REWATCH LAST #time# MINUTES OF PREVIOUS SHOW";
      
      private var dvrText:String;
      
      private var bg:Shape;
      
      private var holder:Sprite;
      
      private var textHolder:Sprite;
      
      private var textMask:Shape;
      
      private var _opened:Boolean = false;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         this.addEventListener(MouseEvent.CLICK,this.onClick);
         useHandCursor = true;
         buttonMode = true;
         addChild(this.bg = new Shape());
         this.bg.alpha = 0.8;
         this.bg.filters = [new DropShadowFilter(1,90,16777215,0.11,0,0,4,1,true),new DropShadowFilter(0,90,0,0.77,3,3,1,2)];
         addChild(this.holder = new Sprite());
         this.holder.addChild(this.textHolder = new Sprite());
         this.holder.addChild(this.textMask = new Shape());
         this.textHolder.cacheAsBitmap = this.textMask.cacheAsBitmap = true;
         this.textHolder.mask = this.textMask;
         this.dvrText = Locale.instance.label(ViewerLabels.labelDvrRewatch,{"time":Math.round(this._time / 60)});
         var _loc2_:Label = new Label();
         this.holder.addChild(_loc2_);
         _loc2_.textFormat = new TextFormat(ViewerUI.baseFontName,13,11184810);
         _loc2_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc2_.overSample = !ViewerUI.baseFontEmbedded;
         _loc2_.text = this.dvrText;
         _loc2_.filters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
         _loc2_.name = "lbl";
         _loc2_.y = Math.round((this._h - _loc2_.height) / 2);
         _loc2_.x = 15;
         this.resize();
         y = -this._h - 6;
         alpha = 0;
         this.open();
      }
      
      private function onClick(param1:MouseEvent) : void {
         dispatchEvent(new Event(DVR_PLAY));
      }
      
      private function open(param1:Event = null) : void {
         var e:Event = param1;
         if(!visible)
         {
            visible = true;
         }
         if(this._opened)
         {
            return;
         }
         Tween.kill(this);
         Tween.to(this,
            {
               "y":0,
               "alpha":1
            },30 * 0.4,"easeOutExpo",null,null,function():void
         {
            y = Math.round(y);
            dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
         });
         this._opened = true;
      }
      
      public function close(param1:Event = null) : void {
         var e:Event = param1;
         if(!this._opened)
         {
            return;
         }
         Tween.kill(this);
         Tween.to(this,
            {
               "y":-this._h - 6,
               "alpha":0
            },(e) && e is TimerEvent?30 * 1.5:30 * 0.8,"easeOutQuart",(e) && e is TimerEvent?null:this.dispatchClose,null,function():void
         {
            y = Math.round(y);
            dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
         });
         this._opened = false;
      }
      
      private function dispatchClose(... rest) : void {
         dispatchEvent(new Event(Event.CLOSE));
      }
      
      public function get panelHeight() : Number {
         return Math.max(0,(y + height) * scaleY);
      }
      
      public function updateDvrText(param1:Number) : void {
         this._time = param1;
         this.dvrText = Locale.instance.label(ViewerLabels.labelDvrRewatch,{"time":Math.round(this._time / 60)});
         Label(this.holder.getChildByName("lbl")).text = this.dvrText;
      }
      
      public function resize(... rest) : void {
         var _loc2_:Matrix = null;
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         scaleX = scaleY = ViewerUI.ratio;
         this._width = this._width / ViewerUI.ratio;
         this.textMask.graphics.clear();
         this.textMask.graphics.beginFill(16737792,1);
         this.textMask.graphics.drawRect(0,0,this._width,this._h);
         this.textMask.graphics.endFill();
         _loc2_ = new Matrix();
         _loc2_.createGradientBox(this._width,this._h,90 * Math.PI / 180);
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(1710618);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         var _loc3_:BitmapData = new BitmapData(2,2,true,3.42439478E9);
         _loc3_.setPixel32(0,0,3.423605008E9);
         _loc3_.setPixel32(1,1,3.423605008E9);
         this.bg.graphics.beginBitmapFill(_loc3_,new Matrix(1 / scaleX,0,0,1 / scaleY));
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         this.bg.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.11,0.11],[0,255],_loc2_);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
      }
      
      public function destroy() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(MouseEvent.CLICK,this.onClick);
      }
      
      private function echo(param1:*) : void {
         Debug.echo("[ DvrNotification ] " + param1.toString());
      }
   }
}
