package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   
   public class TimeIcon extends Shape
   {
      
      public function TimeIcon() {
         super();
         var _loc1_:Matrix = new Matrix();
         _loc1_.createGradientBox(15,15,90 * Math.PI / 180);
         graphics.beginFill(4276801);
         graphics.drawRect(7,3.2,1,5);
         graphics.endFill();
         graphics.beginFill(4276801);
         graphics.drawRect(7,7,4.5,1);
         graphics.endFill();
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.22,0.22],[0,255],_loc1_);
         graphics.drawRect(7,3.2,1,5);
         graphics.endFill();
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.22,0.22],[0,255],_loc1_);
         graphics.drawRect(7,7,4.5,1);
         graphics.endFill();
         graphics.lineStyle(2,4276801);
         graphics.drawCircle(15 / 2,15 / 2,13 / 2);
         graphics.endFill();
         graphics.lineStyle(2);
         graphics.lineGradientStyle(GradientType.LINEAR,[16777215,0],[0.22,0.22],[0,255],_loc1_);
         graphics.drawCircle(15 / 2,15 / 2,13 / 2);
         graphics.endFill();
         filters = [new DropShadowFilter(1,90,16777215,0.33,1,1,1,2,true),new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
      }
   }
}
