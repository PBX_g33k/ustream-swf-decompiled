package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.filters.DropShadowFilter;
   
   public class FullScreenIcon extends Icon
   {
      
      public function FullScreenIcon() {
         super();
         var _shape:Sprite = new Sprite();
         shape = _shape;
         var w:Number = 16;
         var mtx:Matrix = new Matrix();
         mtx.createGradientBox(w,w,90 * Math.PI / 180,0,0);
         _shape.addChild(this.openBtn = new Shape());
         with(this.openBtn)
         {
         }
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.moveTo(0,16);
         graphics.lineTo(7,16);
         graphics.lineTo(0,9);
         graphics.moveTo(16,0);
         graphics.lineTo(16,7);
         graphics.lineTo(9,0);
         graphics.endFill();
         graphics.lineStyle(3);
         graphics.lineGradientStyle(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.moveTo(1.5,16 - 1.5);
         graphics.lineTo(6,10);
         graphics.moveTo(16 - 1.5,1.5);
         graphics.lineTo(10,6);
         graphics.endFill();
         }
         this.openBtn.x = this.openBtn.y = -w / 2;
         _shape.addChild(this.closeBtn = new Shape());
         with(this.closeBtn)
         {
         }
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.moveTo(7,16);
         graphics.lineTo(7,9);
         graphics.lineTo(0,9);
         graphics.moveTo(9,0);
         graphics.lineTo(9,7);
         graphics.lineTo(16,7);
         graphics.endFill();
         graphics.lineStyle(3);
         graphics.lineGradientStyle(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.moveTo(1,15);
         graphics.lineTo(7 - 1.5,9 + 1.5);
         graphics.moveTo(15,1);
         graphics.lineTo(9 + 1.5,7 - 1.5);
         graphics.endFill();
         }
         this.closeBtn.x = this.closeBtn.y = -w / 2;
         this.closeBtn.visible = false;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      public var openBtn:Shape;
      
      public var closeBtn:Shape;
   }
}
