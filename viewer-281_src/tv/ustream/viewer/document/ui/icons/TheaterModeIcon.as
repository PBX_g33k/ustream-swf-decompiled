package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.display.Bitmap;
   import tv.ustream.tween2.Tween;
   import flash.display.Sprite;
   import flash.filters.DropShadowFilter;
   
   public class TheaterModeIcon extends Icon
   {
      
      public function TheaterModeIcon() {
         this.OpenIcon = TheaterModeIcon_OpenIcon;
         this.CloseIcon = TheaterModeIcon_CloseIcon;
         super();
         var _loc1_:Sprite = new Sprite();
         _loc1_.addChild(this._bg = new Shape());
         this._bg.graphics.beginFill(0,0.5);
         this._bg.graphics.drawRect(-8,-5,16,11);
         this._bg.graphics.endFill();
         this._bg.alpha = 0;
         _loc1_.addChild(this.openBtn = new this.OpenIcon());
         this.openBtn.x = -this.openBtn.width / 2;
         this.openBtn.y = -this.openBtn.height / 2;
         this.openBtn.smoothing = true;
         _loc1_.addChild(this.closeBtn = new this.CloseIcon());
         this.closeBtn.x = -this.closeBtn.width / 2;
         this.closeBtn.y = -this.closeBtn.height / 2;
         this.closeBtn.smoothing = true;
         shape = _loc1_;
         this.opened = false;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var OpenIcon:Class;
      
      private var CloseIcon:Class;
      
      private var _bg:Shape;
      
      private var openBtn:Bitmap;
      
      private var closeBtn:Bitmap;
      
      public function get opened() : Boolean {
         return this.closeBtn.visible;
      }
      
      public function set opened(param1:Boolean) : void {
         this.closeBtn.visible = param1;
         this.openBtn.visible = !param1;
      }
      
      public function onMouseOver() : void {
         Tween.to(this._bg,{"alpha":1},30 * 0.4,"easeOutQuart");
      }
      
      public function onMouseOut() : void {
         Tween.to(this._bg,{"alpha":0},30 * 0.8,"easeOutQuart");
      }
   }
}
