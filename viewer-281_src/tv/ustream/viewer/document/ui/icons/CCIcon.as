package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.filters.DropShadowFilter;
   
   public class CCIcon extends Icon
   {
      
      public function CCIcon() {
         var _loc1_:Sprite = null;
         this.CCOnIconImg = CCIcon_CCOnIconImg;
         this.CCOffIconImg = CCIcon_CCOffIconImg;
         this.ccOnIcon = new this.CCOnIconImg();
         this.ccOffIcon = new this.CCOffIconImg();
         super();
         _loc1_ = new Sprite();
         shape = _loc1_;
         this.ccOnIcon.x = this.ccOnIcon.x - this.ccOnIcon.width / 2;
         this.ccOnIcon.y = this.ccOnIcon.y - this.ccOnIcon.height / 2;
         this.ccOnIcon.smoothing = true;
         _loc1_.addChild(this.ccOnIcon);
         this.ccOffIcon.x = this.ccOffIcon.x - this.ccOffIcon.width / 2;
         this.ccOffIcon.y = this.ccOffIcon.y - this.ccOffIcon.height / 2;
         this.ccOffIcon.smoothing = true;
         _loc1_.addChild(this.ccOffIcon);
         this.enabled = false;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var CCOnIconImg:Class;
      
      private var CCOffIconImg:Class;
      
      private var _enabled:Boolean;
      
      private var ccOnIcon:Bitmap;
      
      private var ccOffIcon:Bitmap;
      
      public function get enabled() : Boolean {
         return this._enabled;
      }
      
      public function set enabled(param1:Boolean) : void {
         this.ccOnIcon.visible = this._enabled = param1;
         this.ccOffIcon.visible = !this._enabled;
      }
   }
}
