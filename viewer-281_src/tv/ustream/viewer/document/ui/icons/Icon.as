package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Sprite;
   
   public class Icon extends Sprite
   {
      
      public function Icon() {
         super();
      }
      
      private var _shape;
      
      public function get shape() : * {
         return this._shape;
      }
      
      public function set shape(param1:*) : void {
         if(this._shape)
         {
            removeChild(this._shape);
            this._shape = null;
         }
         addChild(this._shape = param1);
      }
   }
}
