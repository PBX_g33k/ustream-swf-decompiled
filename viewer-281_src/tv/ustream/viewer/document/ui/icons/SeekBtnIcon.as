package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   import tv.ustream.viewer.document.ViewerUI;
   
   public class SeekBtnIcon extends Icon
   {
      
      public function SeekBtnIcon(param1:Number = 9, param2:Number = 3) {
         var _loc4_:Shape = null;
         super();
         this._radius = param1;
         this._innerRadius = param2;
         var _loc3_:Matrix = new Matrix();
         _loc3_.createGradientBox(param1 * 2,param1 * 2,90 * Math.PI / 180,-param1,-param1);
         graphics.beginGradientFill(GradientType.LINEAR,[15396078,12961737],[1,1],[0,255],_loc3_);
         graphics.drawCircle(0,0,param1);
         graphics.endFill();
         _loc4_ = new Shape();
         _loc4_.filters = [new DropShadowFilter(1,90,0,0.35,1,1,1,2,true),new DropShadowFilter(1,90,16777215,0.34,1,1,1,2)];
         shape = _loc4_;
         this.colorize(ViewerUI.style.color);
         filters = [new DropShadowFilter(1,90,16777215,0.5,1,1,1,2,true),new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var _radius:Number = 9;
      
      private var _innerRadius:Number = 3;
      
      public function colorize(param1:uint) : void {
         shape.graphics.clear();
         shape.graphics.beginFill(param1);
         shape.graphics.drawCircle(0,0,this._innerRadius);
         shape.graphics.endFill();
      }
   }
}
