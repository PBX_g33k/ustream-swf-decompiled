package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Bitmap;
   import flash.filters.DropShadowFilter;
   
   public class PopoutIcon extends Icon
   {
      
      public function PopoutIcon() {
         this.IconShape = PopoutIcon_IconShape;
         super();
         shape = new this.IconShape();
         shape.x = -shape.width / 2;
         shape.y = -shape.height / 2;
         Bitmap(shape).smoothing = true;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var IconShape:Class;
   }
}
