package tv.ustream.viewer.document.ui.icons
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class ChatIcon_IconShape extends SpriteAsset
   {
      
      public function ChatIcon_IconShape() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
