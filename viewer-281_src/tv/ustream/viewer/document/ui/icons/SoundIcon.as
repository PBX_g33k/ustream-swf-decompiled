package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.filters.DropShadowFilter;
   
   public class SoundIcon extends Icon
   {
      
      public function SoundIcon() {
         this.SoundIconImg3 = SoundIcon_SoundIconImg3;
         this.SoundIconImg2 = SoundIcon_SoundIconImg2;
         this.SoundIconImg1 = SoundIcon_SoundIconImg1;
         this.SoundIconImg0 = SoundIcon_SoundIconImg0;
         this.MuteIconImg = SoundIcon_MuteIconImg;
         super();
         shape = new Sprite();
         this.volume = 1;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var SoundIconImg3:Class;
      
      private var SoundIconImg2:Class;
      
      private var SoundIconImg1:Class;
      
      private var SoundIconImg0:Class;
      
      private var MuteIconImg:Class;
      
      private var _muted:Boolean;
      
      private var soundIcon:Bitmap;
      
      public function get muted() : Boolean {
         return this._muted;
      }
      
      public function set muted(param1:Boolean) : void {
         this.volume = param1?0:1;
      }
      
      public function set volume(param1:Number) : void {
         if((this.soundIcon) && (shape.contains(this.soundIcon)))
         {
            shape.removeChild(this.soundIcon);
         }
         if(param1 <= 0)
         {
            this._muted = true;
            this.soundIcon = new this.MuteIconImg();
         }
         else if(param1 <= 0.25)
         {
            this._muted = false;
            this.soundIcon = new this.SoundIconImg0();
         }
         else if(param1 <= 0.5)
         {
            this._muted = false;
            this.soundIcon = new this.SoundIconImg1();
         }
         else if(param1 <= 0.75)
         {
            this._muted = false;
            this.soundIcon = new this.SoundIconImg2();
         }
         else
         {
            this._muted = false;
            this.soundIcon = new this.SoundIconImg3();
         }
         
         
         
         this.soundIcon.x = this.soundIcon.x - this.soundIcon.width / 2;
         this.soundIcon.y = this.soundIcon.y - this.soundIcon.height / 2;
         this.soundIcon.smoothing = true;
         shape.addChild(this.soundIcon);
      }
   }
}
