package tv.ustream.viewer.document.ui.icons
{
   import flash.filters.DropShadowFilter;
   
   public class ChatIcon extends Icon
   {
      
      public function ChatIcon() {
         this.IconShape = ChatIcon_IconShape;
         super();
         shape = new this.IconShape();
         shape.x = -shape.width / 2;
         shape.y = -shape.height / 2;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var IconShape:Class;
   }
}
