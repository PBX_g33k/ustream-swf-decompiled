package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Bitmap;
   import flash.filters.DropShadowFilter;
   
   public class SocialIcon extends Icon
   {
      
      public function SocialIcon(param1:String = "fb") {
         var type:String = param1;
         this.FBIcon = SocialIcon_FBIcon;
         this.TwitterIcon = SocialIcon_TwitterIcon;
         this.LinkIcon = SocialIcon_LinkIcon;
         this.EmbedIcon = SocialIcon_EmbedIcon;
         this.MixiIcon = SocialIcon_MixiIcon;
         super();
         switch(type)
         {
            case "fb":
               this.bmp = new this.FBIcon();
               break;
            case "twitter":
               this.bmp = new this.TwitterIcon();
               break;
            case "link":
               this.bmp = new this.LinkIcon();
               break;
            case "embed":
               this.bmp = new this.EmbedIcon();
               break;
            case "mixi":
               this.bmp = new this.MixiIcon();
               break;
         }
         with(this.bmp)
         {
         }
         x = x - width / 2;
         y = y - height / 2;
         smoothing = true;
         }
         shape = this.bmp;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var FBIcon:Class;
      
      private var TwitterIcon:Class;
      
      private var LinkIcon:Class;
      
      private var EmbedIcon:Class;
      
      private var MixiIcon:Class;
      
      private var bmp:Bitmap;
   }
}
