package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   
   public class HighlightMarkerIcon extends Icon
   {
      
      public function HighlightMarkerIcon() {
         super();
         var _loc1_:Shape = new Shape();
         _loc1_.graphics.beginFill(16777215);
         _loc1_.graphics.drawRoundRect(-11 / 2,-16 / 2,11,16,6);
         _loc1_.graphics.endFill();
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(11,16,90 * Math.PI / 180,-11 / 2,-16 / 2);
         _loc1_.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.19,0.19],[0,255],_loc2_);
         _loc1_.graphics.drawRoundRect(-11 / 2,-16 / 2,11,16,6);
         _loc1_.graphics.endFill();
         _loc1_.graphics.beginFill(16777215,0.36);
         _loc1_.graphics.drawRect(-11 / 2 + 3,-16 / 2 + 4,1,9);
         _loc1_.graphics.drawRect(-11 / 2 + 5,-16 / 2 + 4,1,9);
         _loc1_.graphics.drawRect(-11 / 2 + 7,-16 / 2 + 4,1,9);
         _loc1_.graphics.endFill();
         _loc1_.graphics.beginFill(7764089);
         _loc1_.graphics.drawRect(-11 / 2 + 3,-16 / 2 + 4,1,8);
         _loc1_.graphics.drawRect(-11 / 2 + 5,-16 / 2 + 4,1,8);
         _loc1_.graphics.drawRect(-11 / 2 + 7,-16 / 2 + 4,1,8);
         _loc1_.graphics.endFill();
         shape = _loc1_;
         filters = [new DropShadowFilter(1,90,16777215,0.5,1,1,1,2,true),new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
   }
}
