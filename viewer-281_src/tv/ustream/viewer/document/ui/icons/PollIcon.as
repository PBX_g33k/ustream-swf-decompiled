package tv.ustream.viewer.document.ui.icons
{
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.events.Event;
   import tv.ustream.tween2.Tween;
   import flash.display.Shape;
   import flash.filters.DropShadowFilter;
   
   public class PollIcon extends Icon
   {
      
      public function PollIcon() {
         super();
         var _loc1_:Shape = new Shape();
         _loc1_.x = _loc1_.x - 13 / 2;
         _loc1_.y = _loc1_.y - 17 / 2;
         shape = _loc1_;
         this.h = 0;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      private var _h:Number = 0;
      
      public function get h() : Number {
         return this._h;
      }
      
      public function set h(param1:Number) : void {
         this._h = param1;
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(16,16,90 * Math.PI / 180,-8,-8);
         var _loc3_:Number = 17;
         shape.graphics.clear();
         shape.graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],_loc2_);
         shape.graphics.drawRect(0,(_loc3_ - 12) * (1 - this._h),3,_loc3_ - (_loc3_ - 12) * (1 - this._h));
         shape.graphics.drawRect(5,(_loc3_ - 11) * this._h,3,_loc3_ - (_loc3_ - 11) * this._h);
         shape.graphics.drawRect(10,3 + (_loc3_ - 3 - 6) * (1 - this._h),3,_loc3_ - 3 - (_loc3_ - 3 - 6) * (1 - this._h));
         shape.graphics.endFill();
      }
      
      public function onMouseOver(param1:Event = null) : void {
         Tween.to(this,{"h":1},30 * 0.4,"easeOutQuart");
      }
      
      public function onMouseOut(param1:Event = null) : void {
         Tween.to(this,{"h":0},30 * 0.8,"easeOutQuart");
      }
   }
}
