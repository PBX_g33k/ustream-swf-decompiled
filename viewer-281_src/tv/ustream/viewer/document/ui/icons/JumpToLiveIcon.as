package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   
   public class JumpToLiveIcon extends Icon
   {
      
      public function JumpToLiveIcon() {
         super();
         var _loc1_:Sprite = new Sprite();
         shape = _loc1_;
         _loc1_.addChild(this.btn = new Shape());
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(24,16,90 * Math.PI / 180,-12,-8);
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],_loc2_);
         graphics.moveTo(-6.5,-8);
         graphics.lineTo(7.5,0);
         graphics.lineTo(-6.5,8);
         graphics.drawRect(6.5,-7,3.5,14);
         graphics.endFill();
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      public var btn:Shape;
   }
}
