package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.filters.DropShadowFilter;
   
   public class PlayPauseIcon extends Icon
   {
      
      public function PlayPauseIcon() {
         super();
         var _shape:Sprite = new Sprite();
         shape = _shape;
         _shape.addChild(this.playBtn = new Shape());
         _shape.addChild(this.pauseBtn = new Shape());
         var mtx:Matrix = new Matrix();
         mtx.createGradientBox(24,16,90 * Math.PI / 180,-12,-8);
         with(this.playBtn)
         {
         }
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.moveTo(-6.5,-8);
         graphics.lineTo(7.5,0);
         graphics.lineTo(-6.5,8);
         graphics.endFill();
         }
         with(this.pauseBtn)
         {
         }
         graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],mtx);
         graphics.drawRect(-6.5,-8,5,16);
         graphics.drawRect(1.5,-8,5,16);
         graphics.endFill();
         visible = false;
         }
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
      
      public var playBtn:Shape;
      
      public var pauseBtn:Shape;
   }
}
