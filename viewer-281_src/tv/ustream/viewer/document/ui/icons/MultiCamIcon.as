package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import flash.filters.DropShadowFilter;
   
   public class MultiCamIcon extends Icon
   {
      
      public function MultiCamIcon() {
         var _loc4_:* = NaN;
         var _loc5_:* = NaN;
         super();
         var _loc1_:Shape = new Shape();
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(19,13,90 * Math.PI / 180,-19 / 2,-13 / 2);
         _loc1_.graphics.beginGradientFill(GradientType.LINEAR,[16777215,14079702],[1,1],[0,255],_loc2_);
         var _loc3_:* = 0;
         while(_loc3_ < 3)
         {
            _loc4_ = -19 / 2 + _loc3_ * 7;
            _loc5_ = -13 / 2;
            _loc1_.graphics.drawRect(_loc4_,_loc5_,5,3);
            _loc1_.graphics.drawRect(_loc4_,_loc5_ + 5,5,3);
            _loc1_.graphics.drawRect(_loc4_,_loc5_ + 10,5,3);
            _loc3_++;
         }
         _loc1_.graphics.endFill();
         shape = _loc1_;
         filters = [new DropShadowFilter(1,90,0,0.5,2,2,1,2)];
      }
   }
}
