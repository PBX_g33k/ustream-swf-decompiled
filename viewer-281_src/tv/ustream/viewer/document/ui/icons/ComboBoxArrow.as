package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Shape;
   
   public class ComboBoxArrow extends Icon
   {
      
      public function ComboBoxArrow() {
         super();
         var _loc1_:Shape = new Shape();
         _loc1_.graphics.beginFill(1052688);
         _loc1_.graphics.moveTo(-4.5,-2);
         _loc1_.graphics.lineTo(4.5,-2);
         _loc1_.graphics.lineTo(0,3);
         _loc1_.graphics.endFill();
         shape = _loc1_;
      }
   }
}
