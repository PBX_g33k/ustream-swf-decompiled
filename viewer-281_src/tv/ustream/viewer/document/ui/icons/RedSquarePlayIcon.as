package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Sprite;
   
   public class RedSquarePlayIcon extends Sprite
   {
      
      public function RedSquarePlayIcon() {
         super();
         graphics.beginFill(15343917);
         graphics.drawRect(-45,-45,90,90);
         graphics.endFill();
         graphics.beginFill(16777215);
         graphics.moveTo(-8.5,-13);
         graphics.lineTo(-8.5,13);
         graphics.lineTo(13,0);
         graphics.endFill();
      }
   }
}
