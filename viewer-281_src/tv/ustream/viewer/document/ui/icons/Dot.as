package tv.ustream.viewer.document.ui.icons
{
   import flash.display.Sprite;
   
   public class Dot extends Sprite
   {
      
      public function Dot(param1:Number = 3, param2:uint = 3377407) {
         super();
         graphics.beginFill(param2);
         graphics.drawCircle(0,0,param1 / 2);
         graphics.endFill();
      }
      
      public function setVisible(param1:Boolean) : void {
         super.visible = param1;
      }
   }
}
