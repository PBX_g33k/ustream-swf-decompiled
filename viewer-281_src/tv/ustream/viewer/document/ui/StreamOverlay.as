package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.display.GradientType;
   
   public class StreamOverlay extends Sprite
   {
      
      public function StreamOverlay() {
         super();
         cacheAsBitmap = true;
         mouseEnabled = false;
         mouseChildren = false;
      }
      
      public function resize(param1:Number, param2:Number) : void {
         var _loc3_:Matrix = new Matrix();
         _loc3_.createGradientBox(param1,62,90 * Math.PI / 180,0,-(ViewerUI.baseH + 80));
         var _loc4_:Number = 0.7;
         var _loc5_:uint = 0;
         var _loc6_:Array = [];
         var _loc7_:Array = [];
         var _loc8_:Array = [0,0.02,0.1,0.21,0.36,0.5,0.65,0.79,0.9,0.98,1];
         var _loc9_:* = 0;
         while(_loc9_ < _loc8_.length)
         {
            _loc8_[_loc9_] = _loc8_[_loc9_] * _loc4_;
            _loc6_.push(_loc5_);
            _loc7_.push(255 * _loc9_ / (_loc8_.length - 1));
            _loc9_++;
         }
         graphics.clear();
         graphics.beginGradientFill(GradientType.LINEAR,_loc6_,_loc8_,_loc7_,_loc3_);
         graphics.drawRect(0,-(ViewerUI.baseH + 80),param1,ViewerUI.baseH + 80);
         graphics.endFill();
      }
      
      override public function set alpha(param1:Number) : void {
         visible = param1 < 0.01?false:true;
         super.alpha = param1;
      }
   }
}
