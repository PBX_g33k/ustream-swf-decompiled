package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.utils.Dictionary;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.tween2.Tween;
   import flash.events.Event;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.MouseEvent;
   import flash.filters.DropShadowFilter;
   import flash.text.TextFormat;
   import flash.geom.Rectangle;
   
   public class StatusLabel extends Sprite
   {
      
      public function StatusLabel() {
         var _loc4_:* = undefined;
         this.LogoAsset = StatusLabel_LogoAsset;
         this.replayCIDs = ["13928427"];
         super();
         this.tweenManager = new Dictionary();
         this.logo = new Sprite();
         var _loc1_:Sprite = new this.LogoAsset();
         _loc1_.x = 9;
         _loc1_.y = Math.round((ViewerUI.baseH - _loc1_.height) / 2);
         _loc1_.name = "logoshape";
         this.logo.addChild(_loc1_);
         this.logo.graphics.beginFill(16737792,0);
         this.logo.graphics.drawRect(0,0,Math.round(_loc1_.width) + 18,ViewerUI.baseH);
         this.logo.graphics.endFill();
         this.logo.alpha = 0;
         this.logo.filters = [new DropShadowFilter(1,90,0,0.75,1,1,1,2)];
         this.logo.buttonMode = true;
         this.logo.mouseChildren = false;
         this.logo.addEventListener(MouseEvent.CLICK,this.onLogoClick);
         addChild(this.badgeHolder = new Sprite());
         this.badgeHolder.filters = [new DropShadowFilter(1,180,0,1,0,0,1,1),new DropShadowFilter(1,90,16777215,0.05,1,1,1,2,true)];
         this.live = new Label();
         this.live.embedFonts = true;
         this.live.textFormat = new TextFormat("MyriadCondBoldItalic",20,16777215);
         this.live.textGradientOverlay = [16777215,0];
         this.live.textGradientOverlayAlpha = [0.19,0.19];
         this.live.textFilters = [new DropShadowFilter(1,90,0,0.29,1,1,1,2)];
         this.live.bgHeight = ViewerUI.baseH;
         this.live.bgGradientOverlay = [15749440,15277066];
         this.live.bgGradientOverlayAlpha = [1,1];
         this.live.padding = 6;
         this.live.text = "LIVE";
         this.recorded = new Label();
         this.recorded.embedFonts = true;
         this.recorded.textFormat = new TextFormat("MyriadCondBoldItalic",14,11776947);
         this.recorded.textGradientOverlay = [16777215,0];
         this.recorded.textGradientOverlayAlpha = [0.2,0.2];
         this.recorded.textFilters = [new DropShadowFilter(1,90,0,0.8,1,1,1,2)];
         this.recorded.bgHeight = ViewerUI.baseH;
         this.recorded.bgGradientOverlay = [4013373,2829099];
         this.recorded.bgGradientOverlayAlpha = [1,1];
         this.recorded.padding = 6;
         this.recorded.text = "RECORDED LIVE";
         this.offair = new Label();
         this.offair.embedFonts = true;
         this.offair.textFormat = new TextFormat("MyriadCondBoldItalic",14,11776947);
         this.offair.textGradientOverlay = [16777215,0];
         this.offair.textGradientOverlayAlpha = [0.2,0.2];
         this.offair.textFilters = [new DropShadowFilter(1,90,0,0.8,1,1,1,2)];
         this.offair.bgHeight = ViewerUI.baseH;
         this.offair.bgGradientOverlay = [4013373,2829099];
         this.offair.bgGradientOverlayAlpha = [1,1];
         this.offair.padding = 6;
         this.offair.text = "OFF AIR";
         this.dvr = new Label();
         this.dvr.embedFonts = true;
         this.dvr.textFormat = new TextFormat("MyriadCondBoldItalic",20,16777215);
         this.dvr.textGradientOverlay = [16777215,0];
         this.dvr.textGradientOverlayAlpha = [0.2,0.2];
         this.dvr.textFilters = [new DropShadowFilter(1,90,0,0.8,1,1,1,2)];
         this.dvr.bgHeight = ViewerUI.baseH;
         this.dvr.bgGradientOverlay = [3377407,25318];
         this.dvr.bgGradientOverlayAlpha = [1,1];
         this.dvr.padding = 6;
         this.dvr.text = "DVR";
         this.replay = new Label();
         this.replay.embedFonts = true;
         this.replay.textFormat = new TextFormat("MyriadCondBoldItalic",20,16777215);
         this.replay.textGradientOverlay = [16777215,0];
         this.replay.textGradientOverlayAlpha = [0.2,0.2];
         this.replay.textFilters = [new DropShadowFilter(1,90,0,0.8,1,1,1,2)];
         this.replay.bgHeight = ViewerUI.baseH;
         this.replay.bgGradientOverlay = [3377407,25318];
         this.replay.bgGradientOverlayAlpha = [1,1];
         this.replay.padding = 6;
         this.replay.text = "REPLAY";
         this.badgeHolder.addChild(this.dvr);
         this.badgeHolder.addChild(this.replay);
         this.badgeHolder.addChild(this.offair);
         this.badgeHolder.addChild(this.recorded);
         this.badgeHolder.addChild(this.live);
         var _loc2_:Array = [this.live,this.recorded,this.offair,this.dvr,this.replay];
         var _loc3_:* = 0;
         while(_loc3_ < _loc2_.length)
         {
            _loc2_[_loc3_].x = 1;
            _loc4_ = _loc2_[_loc3_].txtTarget;
            this.tweenManager[_loc4_] = 
               {
                  "x":_loc4_.x,
                  "y":_loc4_.y
               };
            _loc2_[_loc3_].scrollRect = new Rectangle(0,0,_loc2_[_loc3_].width,_loc2_[_loc3_].height);
            _loc3_++;
         }
      }
      
      private var _type:String;
      
      private var live:Label;
      
      private var recorded:Label;
      
      private var offair:Label;
      
      private var dvr:Label;
      
      private var LogoAsset:Class;
      
      private var logo:Sprite;
      
      private var badgeHolder:Sprite;
      
      private var badge;
      
      private var prevBadge;
      
      private var tweenManager:Dictionary;
      
      private var _hasLogo:Boolean;
      
      private var _hasBadge:Boolean = true;
      
      private var replay:Label;
      
      private var replayCIDs:Array;
      
      public function get hasLogo() : Boolean {
         return this._hasLogo;
      }
      
      public function set hasLogo(param1:Boolean) : void {
         var o:Boolean = param1;
         if((This.onSite(ViewerUI.pageUrl) & This.SITE_TYPE_STAGE) || this._hasLogo == o)
         {
            return;
         }
         this._hasLogo = o;
         if(this._hasLogo)
         {
            Tween.kill(this.logo);
            if(!contains(this.logo))
            {
               addChildAt(this.logo,0);
            }
            Tween.to(this.logo,
               {
                  "x":(this.badge?-this.badge.scrollRect.width:0) - this.logo.width,
                  "alpha":1
               },30 * 0.8,"easeInOutQuart");
            this.logo.y = Math.round((ViewerUI.baseH - this.logo.height) / 2);
         }
         else
         {
            Tween.kill(this.logo);
            Tween.to(this.logo,
               {
                  "x":0,
                  "alpha":0
               },30 * 0.8,"easeInOutQuart",function():void
            {
               if(contains(logo))
               {
                  removeChild(logo);
               }
            });
         }
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      public function get hasBadge() : Boolean {
         return this._hasBadge;
      }
      
      public function set hasBadge(param1:Boolean) : void {
         this._hasBadge = param1;
         var _loc2_:String = this._type;
         this._type = "";
         this.type = _loc2_;
      }
      
      public function get type() : String {
         return this._type;
      }
      
      public function set type(param1:String) : void {
         if(this._type == param1)
         {
            return;
         }
         this._type = param1;
         if(this.badge)
         {
            this.prevBadge = this.badge;
            this.badge = null;
         }
         if(this._hasBadge)
         {
            switch(this._type)
            {
               case ViewerUI.STATUS_LIVE:
                  this.badge = this.live;
                  break;
               case ViewerUI.STATUS_DVR_LIVE:
                  this.badge = this.live;
                  break;
               case ViewerUI.STATUS_RECORDED:
               case ViewerUI.STATUS_HIGHLIGHT:
                  this.badgeHolder.addChildAt(this.badge = this.recorded,1);
                  break;
               case ViewerUI.STATUS_OFFAIR:
               case ViewerUI.STATUS_OFFAIR_CONTENT:
                  this.badgeHolder.addChildAt(this.badge = this.offair,1);
                  break;
               case ViewerUI.STATUS_DVR:
                  this.badgeHolder.addChildAt(this.badge = this.dvr,1);
                  break;
               case ViewerUI.STATUS_NONE:
            }
         }
         if((Logic.instance.channel) && (this.replayCIDs.indexOf(Logic.instance.channel.id) >= 0) && this.badge == this.live)
         {
            this.badgeHolder.addChildAt(this.badge = this.replay,1);
         }
         if((this.prevBadge) && !(this.prevBadge == this.badge))
         {
            Tween.to(this.prevBadge,{"x":1},30 * 0.8,"easeInOutQuart");
            Tween.to(this.prevBadge.txtTarget,{"alpha":0},30 * 0.8,"easeInOutQuart");
         }
         if(this.badge)
         {
            this.badge.x = this._hasLogo?Math.max(1,this.logo.x + this.logo.width):1;
            this.badge.txtTarget.x = this.tweenManager[this.badge.txtTarget].x - this.badge.scrollRect.width;
            this.badge.txtTarget.y = this.tweenManager[this.badge.txtTarget].y + ViewerUI.baseH / 2;
            this.badge.txtTarget.alpha = 0;
            Tween.to(this.badge,{"x":-this.badge.scrollRect.width},30 * 0.8,"easeInOutQuart");
            Tween.to(this.badge.txtTarget,{"x":this.tweenManager[this.badge.txtTarget].x},30 * 0.8,"easeInOutQuart");
            Tween.to(this.badge.txtTarget,
               {
                  "delay":12,
                  "y":this.tweenManager[this.badge.txtTarget].y,
                  "alpha":1
               },30 * 0.6,"easeOutBack");
         }
         if(this._hasLogo)
         {
            Tween.to(this.logo,{"x":(this.badge?-this.badge.scrollRect.width:0) - this.logo.width},30 * 0.8,"easeInOutQuart");
            this.logo.y = Math.round((ViewerUI.baseH - this.logo.height) / 2);
         }
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      override public function get width() : Number {
         return (this._hasLogo?Math.ceil(this.logo.width):0) + (this.badge?this.badge.scrollRect.width:0);
      }
      
      private function onLogoClick(param1:MouseEvent) : void {
         dispatchEvent(new Event(ViewerUI.LOGO_CLICK,true));
      }
   }
}
