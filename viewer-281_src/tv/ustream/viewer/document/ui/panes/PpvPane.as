package tv.ustream.viewer.document.ui.panes
{
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.modules.Ppv;
   import flash.display.Shape;
   import flash.display.DisplayObject;
   import tv.ustream.viewer.document.ui.ScrollingLabel;
   import tv.ustream.viewer.document.ui.LargeButton;
   import tv.ustream.viewer.document.ui.Button;
   import flash.display.Loader;
   import flash.utils.Timer;
   import flash.events.Event;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import flash.filters.DropShadowFilter;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.localization.Locale;
   import flash.geom.Rectangle;
   import tv.ustream.tools.This;
   import flash.net.URLRequest;
   import flash.system.LoaderContext;
   import flash.events.IOErrorEvent;
   import flash.display.Bitmap;
   import flash.events.TextEvent;
   import flash.net.navigateToURL;
   import flash.display.StageDisplayState;
   import flash.events.TimerEvent;
   import tv.ustream.tween2.Tween;
   import flash.geom.Matrix;
   import flash.display.BitmapData;
   import flash.display.GradientType;
   import flash.text.TextFormat;
   import flash.text.StyleSheet;
   import tv.ustream.tools.Debug;
   
   public class PpvPane extends Sprite
   {
      
      public function PpvPane(param1:Ppv = null, param2:Sprite = null) {
         this.SuccessIcon = PpvPane_SuccessIcon;
         this.AlertIcon = PpvPane_AlertIcon;
         super();
         mouseEnabled = false;
         this.bgHolder = new Sprite();
         if(param2)
         {
            param2.addChild(this.bgHolder);
         }
         else
         {
            addChild(this.bgHolder);
         }
         this.bgHolder.mouseEnabled = false;
         this.bgLoader = new Loader();
         this.bgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,this.onBgLoaderComplete);
         this.bgLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.onBgLoaderError);
         addChild(this.bg = new Shape());
         this.bg.alpha = 0;
         this.bg.filters = [new DropShadowFilter(1,90,16777215,0.11,0,0,4,1,true),new DropShadowFilter(0,90,0,0.77,3,3,1,2)];
         addChild(this.holder = new Sprite());
         this.holder.mouseEnabled = false;
         this.holder.alpha = 0;
         this.holder.addChild(this.btnHolder = new Sprite());
         this.btnHolder.mouseEnabled = false;
         this.successIcon = new this.SuccessIcon();
         this.alertIcon = new this.AlertIcon();
         this.subscribeBtn = new LargeButton();
         this.subscribeBtn.defaultStyle = 2;
         this.subscribeBtn.addEventListener(MouseEvent.CLICK,this.onSubscribeClick);
         this.buyTicketBtn = new LargeButton();
         this.buyTicketBtn.defaultStyle = 2;
         this.buyTicketBtn.addEventListener(MouseEvent.CLICK,this.onBuyTicketClick);
         this.redeemBtn = new Button();
         this.redeemBtn.textFormat = new TextFormat(ViewerUI.baseFontName,13,ViewerUI.style.linkColor);
         this.redeemBtn.embedFonts = ViewerUI.baseFontEmbedded;
         this.redeemBtn.overSample = !ViewerUI.baseFontEmbedded;
         this.redeemBtn.glowColor = ViewerUI.style.linkColor;
         this.redeemBtn.filters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
         this.redeemBtn.addEventListener(MouseEvent.CLICK,this.onRedeemClick);
         this.separator = new Shape();
         var _loc3_:StyleSheet = new StyleSheet();
         _loc3_.setStyle("p",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#FFFFFF"
            });
         _loc3_.setStyle(".info",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#aaaaaa"
            });
         _loc3_.setStyle("a",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#" + ViewerUI.style.linkColor.toString(16)
            });
         _loc3_.setStyle("a:hover",{"textDecoration":"underline"});
         this.holder.addChild(this.infoLabel = new ScrollingLabel(null,_loc3_,true));
         this.infoLabel.embedFonts = ViewerUI.baseFontEmbedded;
         this.infoLabel.overSample = !ViewerUI.baseFontEmbedded;
         this.infoLabel.locked = true;
         this.infoLabel.addEventListener(TextEvent.LINK,this.onLink);
         this.titleLabel = new ScrollingLabel(new TextFormat(ViewerUI.baseFontName,20,16777215,true),null,false,false);
         this.titleLabel.embedFonts = ViewerUI.baseFontEmbedded;
         this.titleLabel.overSample = !ViewerUI.baseFontEmbedded;
         this.idleTimer = new Timer(6000,1);
         if(ViewerUI.touchMode)
         {
            this.idleTimer.delay = 8000;
         }
         if(param1)
         {
            this.module = param1;
         }
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      public static const BG_ADDED:String = "bgAdded";
      
      public static const BG_REMOVED:String = "bgRemoved";
      
      private var _module:Ppv;
      
      private var SuccessIcon:Class;
      
      private var AlertIcon:Class;
      
      private var _h:Number = 50.0;
      
      private var bg:Shape;
      
      private var holder:Sprite;
      
      private var btnHolder:Sprite;
      
      private var _icon:DisplayObject;
      
      private var titleLabel:ScrollingLabel;
      
      private var subscribeBtn:LargeButton;
      
      private var buyTicketBtn:LargeButton;
      
      private var redeemBtn:Button;
      
      private var separator:Shape;
      
      private var infoLabel:ScrollingLabel;
      
      private var successIcon:DisplayObject;
      
      private var alertIcon:DisplayObject;
      
      private var bgLoader:Loader;
      
      private var bgHolder:Sprite;
      
      private var lastBgUrl:String;
      
      private var idleTimer:Timer;
      
      private var _opened:Boolean;
      
      private var alertMode:Boolean;
      
      private var _videoAdMode:Boolean;
      
      private var _width:Number = 0;
      
      private var _height:Number = 0;
      
      private var donateChannelId:String = "12281743";
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         visible = false;
         addEventListener(Event.ENTER_FRAME,this.open);
         if(ViewerUI.touchMode)
         {
            stage.addEventListener(MouseEvent.CLICK,this.open);
         }
         else
         {
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.open);
         }
         this.resize();
         this.bg.y = -this._h - 6;
         this.holder.y = -this._h - 6;
      }
      
      public function set icon(param1:DisplayObject) : void {
         if(this._icon)
         {
            if(this.holder.contains(this._icon))
            {
               this.holder.removeChild(this._icon);
            }
            this._icon = null;
         }
         if(param1)
         {
            this._icon = param1;
            this._icon.filters = [new DropShadowFilter(1,90,0,0.32,1,1,1,2),new DropShadowFilter(1,90,16777215,0.34,1,1,1,2,true)];
            this.holder.addChild(this._icon);
         }
         if(stage)
         {
            this.resize();
         }
      }
      
      public function get panelHeight() : Number {
         return Math.max(0,(this.bg.y + this._h) * scaleY);
      }
      
      public function get module() : Ppv {
         return this._module;
      }
      
      public function set module(param1:Ppv) : void {
         this.releaseModule();
         this._module = param1;
         if(this._module)
         {
            this.echo("module set: " + param1);
            this._module.addEventListener("update",this.onUpdateModuleInfo);
            this._module.addEventListener("freeReject",this.onFreeReject);
            this._module.addEventListener("paidReject",this.onPaidReject);
            this._module.addEventListener("goFree",this.onUpdateModuleInfo);
            if(Logic.instance.channel)
            {
               Logic.instance.channel.addEventListener("online",this.onUpdateModuleInfo);
               Logic.instance.channel.addEventListener("offline",this.onUpdateModuleInfo);
            }
            this.onUpdateModuleInfo();
         }
      }
      
      private function onUpdateModuleInfo(param1:Event = null) : void {
         var _loc2_:String = null;
         var _loc3_:String = null;
         this.echo("onUpdate - module: " + this._module);
         if((this._module) && !this.alertMode)
         {
            if(this._module.status == "paid" && this._module.auth == "reject")
            {
               this.onPaidReject();
               return;
            }
            this.icon = null;
            if(this._module.title)
            {
               this.titleLabel.text = this._module.title;
               if(!this.holder.contains(this.titleLabel))
               {
                  this.holder.addChild(this.titleLabel);
               }
            }
            else if(this.holder.contains(this.titleLabel))
            {
               this.holder.removeChild(this.titleLabel);
            }
            
            this.echo("hasTicket: " + this._module.hasTicket);
            this.echo("alreadySubscribed: " + this._module.alreadySubscribed);
            if((this._module.subscribe) && !this._module.alreadySubscribed)
            {
               if(!this.btnHolder.contains(this.subscribeBtn))
               {
                  this.btnHolder.addChild(this.subscribeBtn);
               }
               this.subscribeBtn.text = Locale.hasInstance?Locale.instance.label(ViewerLabels.labelPpvSubscribe):"Subscribe";
            }
            else if(this.btnHolder.contains(this.subscribeBtn))
            {
               this.btnHolder.removeChild(this.subscribeBtn);
            }
            
            if(this._module.buyTicket)
            {
               if(!this.btnHolder.contains(this.buyTicketBtn))
               {
                  this.btnHolder.addChild(this.buyTicketBtn);
               }
               this.echo("currency: " + this._module.currency + ", locale: labelCurrency" + this._module.currency + ", localized: " + Locale.instance.label(ViewerLabels["labelCurrency" + this._module.currency]));
               _loc3_ = Locale.instance.label(ViewerLabels.labelPpvBuyWithLocalizedCurrency,
                  {
                     "currency":Locale.instance.label(ViewerLabels["labelCurrency" + this._module.currency]),
                     "amount":this._module.price
                  });
               if(this.checkDonateChannelId())
               {
                  _loc3_ = "Donate $" + this._module.price;
               }
               this.buyTicketBtn.text = _loc3_;
            }
            else if(this.btnHolder.contains(this.buyTicketBtn))
            {
               this.btnHolder.removeChild(this.buyTicketBtn);
            }
            
            if(this._module.enterTicket)
            {
               if(!this.btnHolder.contains(this.redeemBtn))
               {
                  this.btnHolder.addChild(this.redeemBtn);
               }
               this.redeemBtn._hitArea = null;
               this.redeemBtn.text = Locale.hasInstance?Locale.instance.label(ViewerLabels.labelPpvAlreadyHaveATicket):"Already have a ticket?";
               this.redeemBtn._hitArea = new Rectangle(0,0,this.redeemBtn.width,26);
            }
            else if(this.btnHolder.contains(this.redeemBtn))
            {
               this.btnHolder.removeChild(this.redeemBtn);
            }
            
            if((this._module.hasTicket) || this._module.auth == "success")
            {
               if(this.btnHolder.contains(this.buyTicketBtn))
               {
                  this.btnHolder.removeChild(this.buyTicketBtn);
               }
               if(this.btnHolder.contains(this.redeemBtn))
               {
                  this.btnHolder.removeChild(this.redeemBtn);
               }
               if(this.btnHolder.contains(this.subscribeBtn))
               {
                  this.btnHolder.removeChild(this.subscribeBtn);
               }
            }
            _loc2_ = (this._module.hasTicket) || this._module.auth == "success"?ViewerLabels.labelPpvPayerOffair:ViewerLabels.labelPpvPreshow;
            if(Logic.instance.channel)
            {
               this.echo("we have channel");
               if(Logic.instance.channel.online)
               {
                  this.echo("it is online");
                  if(this._module.subscribe)
                  {
                     this.echo("subscribeable");
                     if(this._module.auth == "success")
                     {
                        _loc2_ = this._module.status == "free"?ViewerLabels.labelSubscriptionPayerOnairFree:ViewerLabels.labelSubscriptionPayerOnair;
                        this.icon = this.successIcon;
                     }
                     else
                     {
                        _loc2_ = this._module.status == "free"?ViewerLabels.labelSubscriptionGuestOnairFree:ViewerLabels.labelSubscriptionGuestOnair;
                     }
                  }
                  else
                  {
                     this.echo("probably ppv");
                     if(this._module.status == "free" && this._module.auth == "success")
                     {
                        _loc2_ = ViewerLabels.labelPpvPayerOnairFree;
                     }
                     else if(this._module.status == "free")
                     {
                        _loc2_ = ViewerLabels.labelPpvGuestOnairFree;
                     }
                     else if(this._module.status == "paid" && this._module.auth == "success")
                     {
                        _loc2_ = ViewerLabels.labelPpvPayerOnairPaid;
                        this.icon = this.successIcon;
                     }
                     else if(this._module.status == "paid")
                     {
                        _loc2_ = ViewerLabels.labelPpvGuestOnairPaid;
                     }
                     
                     
                     
                  }
               }
               else
               {
                  this.echo("it is offline");
                  if(This.getReference(Logic.instance,"recorded.modules.ppv"))
                  {
                     this.echo("has vod as off-air content");
                     if(this._module.auth == "success")
                     {
                        _loc2_ = this._module.subscribe?ViewerLabels.labelSubscriptionPayerVod:ViewerLabels.labelPpvPayerVod;
                        this.icon = this.successIcon;
                     }
                     else
                     {
                        _loc2_ = this._module.subscribe?ViewerLabels.labelSubscriptionGuestVod:ViewerLabels.labelPpvGuestVod;
                     }
                  }
                  else
                  {
                     if(this._module.status == "pre" && !this._module.subscribe)
                     {
                        _loc2_ = ViewerLabels.labelPpvPreshow;
                     }
                     else if(this._module.auth == "success")
                     {
                        _loc2_ = this._module.subscribe?ViewerLabels.labelSubscriptionPayerOffair:ViewerLabels.labelPpvPayerOffair;
                        this.icon = this.successIcon;
                     }
                     else if(this._module.subscribe)
                     {
                        _loc2_ = this._module.status == "free"?ViewerLabels.labelSubscriptionGuestOffairFree:ViewerLabels.labelSubscriptionGuestOffair;
                     }
                     
                     
                     if((this._module.subscribe) && (this._module.alreadySubscribed))
                     {
                        _loc2_ = this._module.status == "free"?ViewerLabels.labelSubscriptionPayerOffairFree:ViewerLabels.labelSubscriptionPayerOffair;
                        this.icon = this.successIcon;
                     }
                     else if((this._module.hasTicket) || this._module.auth == "success")
                     {
                        _loc2_ = ViewerLabels.labelPpvPayerOffair;
                        this.icon = this.successIcon;
                     }
                     
                  }
               }
            }
            else if(Logic.instance.recorded)
            {
               this.echo("we have vod");
               if(this._module.auth == "success")
               {
                  _loc2_ = this._module.subscribe?ViewerLabels.labelSubscriptionPayerVod:ViewerLabels.labelPpvPayerVod;
                  this.icon = this.successIcon;
               }
               else
               {
                  _loc2_ = this._module.subscribe?ViewerLabels.labelSubscriptionGuestVod:ViewerLabels.labelPpvGuestVod;
               }
            }
            
            if((Locale.hasInstance) && Locale.instance.language == "en_US" && (this.checkDonateChannelId()))
            {
               switch(_loc2_)
               {
                  case ViewerLabels.labelPpvPreshow:
                     _loc2_ = "The show has not yet started. Make a donation and get your ticket now!";
                     break;
                  case ViewerLabels.labelPpvGuestOnairFree:
                     _loc2_ = "You\'re watching the free pre-show; please make a donation to get a ticket or redeem your ticket code to continue watching the show.";
                     break;
                  case ViewerLabels.labelPpvGuestOnairPaid:
                     _loc2_ = "The show is now live; please make a donation to get a ticket or enter a ticket code to watch";
                     break;
                  case ViewerLabels.labelPpvGuestVod:
                     _loc2_ = "Please make a donation to watch this recorded video.";
                     break;
                  default:
                     _loc2_ = Locale.instance.label(_loc2_);
               }
            }
            else
            {
               _loc2_ = Locale.instance.label(_loc2_);
            }
            if(this._icon == this.successIcon)
            {
               _loc2_ = "<b>" + _loc2_ + "</b>";
               if(this.holder.contains(this.titleLabel))
               {
                  this.holder.removeChild(this.titleLabel);
               }
            }
            if(this._module.faqUrl)
            {
               _loc2_ = _loc2_ + ("<span class=\"info\"> (<a href=\"event:faq\">" + Locale.instance.label(ViewerLabels.labelPpvShowFaq) + "</a>)</span>");
            }
            if(this._module.guest)
            {
               _loc2_ = _loc2_ + ("<span class=\"info\"> (" + Locale.instance.label(this._module.subscribe?ViewerLabels.labelSubscriptionLogin:ViewerLabels.labelPpvLogin,
                  {
                     "a1":"<a href=\'event:login\'>",
                     "a2":"</a>"
                  }) + ")</span>");
            }
            if((this._module.hasVideo) && !(This.getReference(Logic.instance,"channel.online") as Boolean))
            {
               _loc2_ = _loc2_ + (" <span class=\"info\">" + Locale.instance.label(ViewerLabels.labelSubscriptionRecordedVideos,
                  {
                     "a1":"<a href=\'event:videospage\'>",
                     "a2":"</a>"
                  }) + "</span>");
            }
            this.infoLabel.text = "<p>" + _loc2_ + "<p>";
            if((this._module.bgUrl && this._module.status == "paid") && (this._module.auth == "failed") && ((!Logic.instance.channel) || (Logic.instance.channel && Logic.instance.channel.online)))
            {
               if(!this.bgHolder.contains(this.bgLoader))
               {
                  this.bgHolder.addChild(this.bgLoader);
               }
               if(this._module.bgUrl != this.lastBgUrl)
               {
                  this.echo("loading bgUrl");
                  this.bgLoader.load(new URLRequest(this._module.bgUrl),new LoaderContext(true));
                  this.lastBgUrl = this._module.bgUrl;
               }
               dispatchEvent(new Event(BG_ADDED));
            }
            else
            {
               if(this.bgHolder.contains(this.bgLoader))
               {
                  this.bgHolder.removeChild(this.bgLoader);
               }
               dispatchEvent(new Event(BG_REMOVED));
            }
            this.resize();
         }
      }
      
      private function checkDonateChannelId() : Boolean {
         var _loc1_:* = false;
         if((Locale.hasInstance) && Locale.instance.language == "en_US" && (Logic.hasInstance) && ((Logic.instance.channel) && (Logic.instance.channel.id == this.donateChannelId) || (!Logic.instance.channel && Logic.instance.recorded && Logic.instance.recorded.channelId) && (Logic.instance.recorded.channelId == this.donateChannelId)))
         {
            _loc1_ = true;
         }
         return _loc1_;
      }
      
      private function checkPurchaseOverlayHide() : Boolean {
         var _loc1_:Array = ["9049687","11333239"];
         if((Logic.instance.channel) && (_loc1_.indexOf(Logic.instance.channel.id) >= 0) || (!Logic.instance.channel && Logic.instance.recorded && Logic.instance.recorded.channelId) && (_loc1_.indexOf(Logic.instance.recorded.channelId) >= 0))
         {
            return true;
         }
         return false;
      }
      
      private function onBgLoaderError(param1:IOErrorEvent) : void {
         this.echo("onBgLoaderError" + param1.toString());
         this.echo("lastBgUrl: " + this.lastBgUrl);
         this.lastBgUrl = "";
      }
      
      private function onBgLoaderComplete(param1:Event) : void {
         if(this.bgLoader.content is Bitmap)
         {
            Bitmap(this.bgLoader.content).smoothing = true;
         }
         this.resize();
      }
      
      private function onLink(param1:TextEvent) : void {
         var _loc2_:String = null;
         if(param1.text == "videospage")
         {
            _loc2_ = This.getReference(Logic.instance,"channel.modules.meta.url") as String;
            if(!_loc2_ || (_loc2_) && (!_loc2_.length))
            {
               _loc2_ = "http://www.ustream.tv/channel/" + Logic.instance.channel.id;
            }
            if(_loc2_.charAt(_loc2_.length - 1) != "/")
            {
               _loc2_ = _loc2_ + "/";
            }
            navigateToURL(new URLRequest(_loc2_ + "videos/"));
         }
         if(param1.text == "login")
         {
            this.echo("linkHandler");
            this._module.ticketAction({"type":"initiateLogon"});
         }
         if(param1.text == "faq")
         {
            this.echo("linkHandler");
            if(stage.displayState != StageDisplayState.NORMAL)
            {
               try
               {
                  stage.displayState = StageDisplayState.NORMAL;
               }
               catch(e:*)
               {
               }
            }
            navigateToURL(new URLRequest(this._module.faqUrl),"_blank");
         }
      }
      
      private function onSubscribeClick(param1:Event) : void {
         this.echo("onSubscribeClick - _module.subscribe: " + this._module.subscribe);
         if(stage.displayState != StageDisplayState.NORMAL)
         {
            try
            {
               stage.displayState = StageDisplayState.NORMAL;
            }
            catch(e:*)
            {
            }
         }
         if(this._module is Ppv)
         {
            this._module.ticketAction({"type":"subscribe"});
         }
      }
      
      private function onBuyTicketClick(param1:Event) : void {
         this.echo("onBuyTicketClick - _module.buyTicket: " + this._module.buyTicket);
         if(stage.displayState != StageDisplayState.NORMAL)
         {
            try
            {
               stage.displayState = StageDisplayState.NORMAL;
            }
            catch(e:*)
            {
            }
         }
         if(this._module is Ppv)
         {
            this._module.ticketAction({"type":"buyTicket"});
         }
      }
      
      private function onRedeemClick(param1:Event) : void {
         this.echo("onRedeemClick");
         this._module.ticketAction({"type":"enterTicket"});
      }
      
      private function onPaidReject(... rest) : void {
         this.echo("onPaidReject");
         this.displayAlert(Locale.instance.label(ViewerLabels.messagePpvPaidReject));
      }
      
      private function onFreeReject(param1:Event) : void {
         this.echo("onFreeReject");
         this.displayAlert(Locale.instance.label(ViewerLabels.messagePpvFreeReject));
      }
      
      private function displayAlert(param1:String) : void {
         this.echo("alert message: " + param1);
         this.alertMode = true;
         if(this.btnHolder.contains(this.subscribeBtn))
         {
            this.btnHolder.removeChild(this.subscribeBtn);
         }
         if(this.btnHolder.contains(this.redeemBtn))
         {
            this.btnHolder.removeChild(this.redeemBtn);
         }
         if(this.btnHolder.contains(this.buyTicketBtn))
         {
            this.btnHolder.removeChild(this.buyTicketBtn);
         }
         if(this.holder.contains(this.titleLabel))
         {
            this.holder.removeChild(this.titleLabel);
         }
         this.icon = this.alertIcon;
         this.infoLabel.text = "<p><b>" + param1 + "</b><p>";
         this.resize();
         this.open();
      }
      
      private function resetIdleTimer(... rest) : void {
         this.idleTimer.reset();
         this.idleTimer.start();
      }
      
      private function open(param1:Event = null) : void {
         var e:Event = param1;
         if(hasEventListener(Event.ENTER_FRAME))
         {
            removeEventListener(Event.ENTER_FRAME,this.open);
         }
         if(!visible)
         {
            visible = true;
         }
         if(this._videoAdMode)
         {
            return;
         }
         if((this._module && (this._module.auth == "success" || this._module.hasTicket)) && (!this.alertMode) || (this.checkPurchaseOverlayHide()))
         {
            this.resetIdleTimer();
            this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         }
         else
         {
            this.idleTimer.stop();
            this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         }
         if(this._opened)
         {
            return;
         }
         Tween.kill(this.bg);
         Tween.kill(this.holder);
         Tween.to(this.bg,
            {
               "y":0,
               "alpha":0.8
            },30 * 0.4,"easeOutExpo",null,null,function():void
         {
            bg.y = Math.round(bg.y);
            dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
         });
         Tween.to(this.holder,
            {
               "y":0,
               "alpha":1
            },30 * 0.4,"easeOutExpo",null,null,function():void
         {
            holder.y = Math.round(holder.y);
         });
         this._opened = true;
         this.infoLabel.locked = false;
      }
      
      public function close(param1:Event = null) : void {
         var e:Event = param1;
         if(hasEventListener(Event.ENTER_FRAME))
         {
            removeEventListener(Event.ENTER_FRAME,this.open);
         }
         if(!e && !this._videoAdMode)
         {
            if(ViewerUI.touchMode)
            {
               stage.removeEventListener(MouseEvent.CLICK,this.open);
            }
            else
            {
               stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.open);
            }
         }
         if(((e) || (this._videoAdMode)) && !this._opened)
         {
            return;
         }
         Tween.kill(this.bg);
         Tween.kill(this.holder);
         if(!e && !this._videoAdMode)
         {
            this.dispatchClose();
         }
         else
         {
            Tween.to(this.bg,
               {
                  "y":-this._h - 6,
                  "alpha":0
               },30 * 0.8,"easeOutQuart",null,null,function():void
            {
               bg.y = Math.round(bg.y);
               dispatchEvent(new Event(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,true));
            });
            Tween.to(this.holder,
               {
                  "y":-this._h - 6,
                  "alpha":0
               },30 * 0.8,"easeOutQuart",null,null,function():void
            {
               holder.y = Math.round(holder.y);
            });
         }
         this._opened = false;
         this.infoLabel.locked = true;
      }
      
      private function dispatchClose(... rest) : void {
         dispatchEvent(new Event(Event.CLOSE));
      }
      
      public function set videoAdMode(param1:Boolean) : void {
         this._videoAdMode = param1;
         if(this._videoAdMode)
         {
            this.close();
         }
         else
         {
            this.open();
         }
         this.bgHolder.visible = !this._videoAdMode;
      }
      
      public function resize(... rest) : void {
         var _loc3_:* = NaN;
         var _loc4_:* = 0;
         var _loc5_:LargeButton = null;
         var _loc8_:* = NaN;
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            this._height = rest[1];
         }
         scaleX = scaleY = ViewerUI.ratio;
         this._width = this._width / ViewerUI.ratio;
         this._height = this._height / ViewerUI.ratio;
         var _loc2_:Number = this._width;
         this._h = 26 + 24;
         if((this.holder.contains(this.titleLabel)) || ((this.btnHolder.contains(this.buyTicketBtn)) || (this.btnHolder.contains(this.subscribeBtn))) && (this.btnHolder.contains(this.redeemBtn)))
         {
            this._h = 28 + 3 + 26 + 24;
         }
         if(this._width < 480 && ((this.btnHolder.contains(this.buyTicketBtn)) || (this.btnHolder.contains(this.redeemBtn)) || (this.btnHolder.contains(this.subscribeBtn))))
         {
            _loc3_ = this._h;
            this._h = this._h + 40;
            _loc4_ = 2;
         }
         else
         {
            _loc3_ = this._h;
            _loc4_ = 1;
         }
         this.btnHolder.y = _loc4_ > 1?_loc3_:0;
         if(this.btnHolder.contains(this.subscribeBtn))
         {
            _loc5_ = this.subscribeBtn;
            this.subscribeBtn.x = _loc2_ = _loc2_ - 12 - this.subscribeBtn.width;
            if(_loc4_ == 1)
            {
               this.subscribeBtn.y = 12;
            }
            else
            {
               this.subscribeBtn.y = Math.round((40 - this.subscribeBtn.height) / 2);
            }
         }
         if(this.btnHolder.contains(this.buyTicketBtn))
         {
            _loc5_ = this.buyTicketBtn;
            this.buyTicketBtn.x = _loc2_ = _loc2_ - 12 - this.buyTicketBtn.width;
            if(_loc4_ == 1)
            {
               this.buyTicketBtn.y = 12;
            }
            else
            {
               this.buyTicketBtn.y = Math.round((40 - this.buyTicketBtn.height) / 2);
            }
         }
         if(this.btnHolder.contains(this.redeemBtn))
         {
            if(_loc4_ == 1)
            {
               if(_loc5_)
               {
                  if(_loc5_.width < this.redeemBtn.width)
                  {
                     _loc5_.width = this.redeemBtn.width;
                     _loc2_ = this._width - 12 - this.redeemBtn.width;
                     _loc5_.x = this.redeemBtn.x = _loc2_;
                  }
                  else
                  {
                     this.redeemBtn.x = this._width - 12 - this.redeemBtn.width;
                  }
               }
               else
               {
                  this.redeemBtn.x = _loc2_ = _loc2_ - 12 - this.redeemBtn.width;
               }
               this.redeemBtn.y = _loc5_?_loc5_.y + _loc5_.height + 3 + Math.round((26 - this.redeemBtn.height) / 2):12 + Math.round((26 - this.redeemBtn.height) / 2);
            }
            else
            {
               if(_loc5_)
               {
                  _loc5_.width = Number.NaN;
                  _loc5_.x = _loc2_ = this._width - 12 - _loc5_.width;
               }
               this.redeemBtn.x = _loc2_ - 12 - this.redeemBtn.width;
               this.redeemBtn.y = Math.round((40 - this.redeemBtn.height) / 2);
            }
         }
         if((this.btnHolder.contains(this.buyTicketBtn)) || (this.btnHolder.contains(this.redeemBtn)) || (this.btnHolder.contains(this.subscribeBtn)))
         {
            if(!this.holder.contains(this.separator))
            {
               this.holder.addChild(this.separator);
            }
            this.separator.graphics.clear();
            this.separator.graphics.beginFill(0,0.2);
            if(_loc4_ == 1)
            {
               this.separator.graphics.drawRect(0,0,1,_loc3_);
            }
            else
            {
               this.separator.graphics.drawRect(0,-1,this._width,1);
            }
            this.separator.graphics.endFill();
            this.separator.graphics.beginFill(16777215,0.06);
            if(_loc4_ == 1)
            {
               this.separator.graphics.drawRect(1,0,1,_loc3_);
            }
            else
            {
               this.separator.graphics.drawRect(0,0,this._width,1);
            }
            this.separator.graphics.endFill();
            if(_loc4_ == 1)
            {
               this.separator.x = _loc2_ = _loc2_ - 12 - this.separator.width;
               this.separator.y = 0;
            }
            else
            {
               this.separator.x = 0;
               this.separator.y = _loc3_;
            }
         }
         else if(this.holder.contains(this.separator))
         {
            this.holder.removeChild(this.separator);
         }
         
         if(this.holder.contains(this.titleLabel))
         {
            this.titleLabel.x = 12;
            this.titleLabel.y = 12 + Math.round((28 - this.titleLabel.height) / 2);
            this.titleLabel.maskWidth = (_loc4_ == 1?_loc2_:this._width) - 12 - this.titleLabel.x;
         }
         if(this._icon)
         {
            this._icon.y = 12 + (this.holder.contains(this.titleLabel)?28 + 3:0) + Math.round((26 - this._icon.height) / 2);
            this._icon.x = 10;
         }
         this.infoLabel.x = this._icon?10 + this._icon.width + 10:12;
         this.infoLabel.y = 12 + (this.holder.contains(this.titleLabel)?28 + 3:0) + Math.round((26 - this.infoLabel.height) / 2);
         this.infoLabel.maskWidth = (_loc4_ == 1?_loc2_:this._width) - 12 - this.infoLabel.x;
         var _loc6_:Matrix = new Matrix();
         _loc6_.createGradientBox(this._width,this._h,90 * Math.PI / 180);
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(1710618);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         var _loc7_:BitmapData = new BitmapData(2,2,true,3.42439478E9);
         _loc7_.setPixel32(0,0,3.423605008E9);
         _loc7_.setPixel32(1,1,3.423605008E9);
         this.bg.graphics.beginBitmapFill(_loc7_,new Matrix(1 / scaleX,0,0,1 / scaleY));
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         this.bg.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.11,0.11],[0,255],_loc6_);
         this.bg.graphics.drawRect(0,0,this._width,this._h);
         this.bg.graphics.endFill();
         if(this.bgHolder.contains(this.bgLoader))
         {
            _loc8_ = this._height - ((stage) && stage.displayState == StageDisplayState.NORMAL?ViewerUI.baseH / ViewerUI.ratio:0);
            this.bgLoader.height = _loc8_;
            this.bgLoader.scaleX = this.bgLoader.scaleY;
            if(this.bgLoader.width < this._width)
            {
               this.bgLoader.width = this._width;
               this.bgLoader.scaleY = this.bgLoader.scaleX;
            }
            this.bgLoader.x = (this._width - this.bgLoader.width) / 2;
            this.bgLoader.y = (_loc8_ - this.bgLoader.height) / 2;
         }
      }
      
      private function releaseModule() : void {
         if(this._module)
         {
            this.echo("releasing module");
            this._module.removeEventListener("update",this.onUpdateModuleInfo);
            this._module.removeEventListener("freeReject",this.onFreeReject);
            this._module.removeEventListener("paidReject",this.onPaidReject);
            this._module.removeEventListener("goFree",this.onUpdateModuleInfo);
            if(Logic.instance.channel)
            {
               Logic.instance.channel.removeEventListener("online",this.onUpdateModuleInfo);
               Logic.instance.channel.removeEventListener("offline",this.onUpdateModuleInfo);
            }
         }
      }
      
      public function destroy() : void {
         if((this.bgHolder.parent) && !(this.bgHolder.parent == this))
         {
            Sprite(this.bgHolder.parent).removeChild(this.bgHolder);
         }
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.ENTER_FRAME,this.open);
         if(ViewerUI.touchMode)
         {
            stage.removeEventListener(MouseEvent.CLICK,this.open);
         }
         else
         {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.open);
         }
         this.releaseModule();
         this.bgLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onBgLoaderComplete);
         this.bgLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onBgLoaderError);
         this.bgLoader.unload();
         this.subscribeBtn.removeEventListener(MouseEvent.CLICK,this.onSubscribeClick);
         this.subscribeBtn.destroy();
         this.buyTicketBtn.removeEventListener(MouseEvent.CLICK,this.onBuyTicketClick);
         this.buyTicketBtn.destroy();
         this.redeemBtn.addEventListener(MouseEvent.CLICK,this.onRedeemClick);
         this.redeemBtn.destroy();
         this.titleLabel.destroy();
         this.infoLabel.removeEventListener(TextEvent.LINK,this.onLink);
         this.infoLabel.destroy();
         this.idleTimer.stop();
         this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.close);
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(... rest) : void {
         this.redeemBtn.textFormat = new TextFormat(ViewerUI.baseFontName,13,ViewerUI.style.linkColor);
         this.redeemBtn.glowColor = ViewerUI.style.linkColor;
         this.redeemBtn._hitArea = new Rectangle(0,0,this.redeemBtn.width,26);
         var _loc2_:StyleSheet = this.infoLabel.styleSheet;
         _loc2_.setStyle("a",
            {
               "fontFamily":ViewerUI.baseFontName,
               "fontSize":"13",
               "color":"#" + ViewerUI.style.linkColor.toString(16)
            });
         this.infoLabel.styleSheet = _loc2_;
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ PPVPANE ] " + param1);
      }
   }
}
