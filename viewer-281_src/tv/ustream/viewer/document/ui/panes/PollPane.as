package tv.ustream.viewer.document.ui.panes
{
   import tv.ustream.viewer.logic.modules.Poll;
   import tv.ustream.viewer.document.ui.LoaderBar;
   import flash.display.Sprite;
   import tv.ustream.viewer.document.ui.Scrollbar;
   import tv.ustream.viewer.document.ui.Button;
   import tv.ustream.viewer.document.ui.icons.PollIcon;
   import flash.geom.Rectangle;
   import tv.ustream.viewer.document.ui.LargeButton;
   import flash.events.MouseEvent;
   import flash.events.Event;
   import tv.ustream.tween2.Tween;
   import tv.ustream.viewer.document.ui.Label;
   import flash.display.DisplayObject;
   import tv.ustream.tween2.Color;
   import tv.ustream.localization.Locale;
   import flash.display.BitmapData;
   import flash.display.Shape;
   import flash.text.TextFormat;
   import flash.filters.DropShadowFilter;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import tv.ustream.tools.Debug;
   
   public class PollPane extends Pane
   {
      
      public function PollPane() {
         this.answerList = [];
         super(true);
         priorityId = 4;
         persist = true;
      }
      
      private var _module:Poll;
      
      private var votedFor:int = -1;
      
      private var loader:LoaderBar;
      
      private var list:Sprite;
      
      private var scrollbar:Scrollbar;
      
      private var answerList:Array;
      
      private var _started:Boolean = false;
      
      override protected function buildUI() : void {
         super.buildUI();
         footer.addChild(this.loader = new LoaderBar(9,2));
         this.loader.x = this.loader.y = 11 + 26 / 2;
         header.addChild(icon = new Button());
         Button(icon).icon = new PollIcon();
         Button(icon)._hitArea = new Rectangle(0,0,23,23);
         Button(icon).enabled = false;
         submitBtn = new LargeButton();
         submitBtn.text = "Skip voting";
         LargeButton(submitBtn).defaultStyle = 0;
         submitBtn.addEventListener(MouseEvent.CLICK,close);
         content.addChild(this.list = new Sprite());
      }
      
      private function onAnswerMouseOver(param1:Event = null) : void {
         var e:Event = param1;
         Tween.to(e.currentTarget.getChildByName("bg"),{"alpha":1},30 * 0.2,"easeOutExpo");
         Tween.to(e.currentTarget.getChildByName("label"),{"alpha":1},30 * 0.2,"easeOutExpo");
         Tween.to(Label(e.currentTarget.getChildByName("num")).bg,{"alpha":0.3},30 * 0.2,"easeOutExpo");
         Tween.to(this.answerList[e.currentTarget.name],
            {
               "r":255,
               "g":255,
               "b":255
            },30 * 0.2,"easeOutExpo",null,null,function(param1:DisplayObject, param2:Object):void
         {
            Color.tint(param1,Color.rgbToHex(param2.r,param2.g,param2.b),1);
         },[Label(e.currentTarget.getChildByName("num")).txtTarget,this.answerList[e.currentTarget.name]]);
      }
      
      private function onAnswerMouseOut(param1:Event = null) : void {
         var e:Event = param1;
         Tween.to(e.currentTarget.getChildByName("bg"),{"alpha":0},30 * 0.6,"easeOutExpo");
         Tween.to(e.currentTarget.getChildByName("label"),{"alpha":0.67},30 * 0.6,"easeOutExpo");
         Tween.to(Label(e.currentTarget.getChildByName("num")).bg,{"alpha":0.2},30 * 0.6,"easeOutExpo");
         Tween.to(this.answerList[e.currentTarget.name],
            {
               "r":55,
               "g":56,
               "b":55
            },30 * 0.6,"easeOutExpo",null,null,function(param1:DisplayObject, param2:Object):void
         {
            Color.tint(param1,Color.rgbToHex(param2.r,param2.g,param2.b),1);
         },[Label(e.currentTarget.getChildByName("num")).txtTarget,this.answerList[e.currentTarget.name]]);
      }
      
      private function onAnswerClick(param1:Event) : void {
         Tween.to(this.loader,{"alpha":1},30 * 0.2);
         this.disposeAnswers(false);
         var _loc2_:int = this.answerList[param1.currentTarget.name].index;
         this._module.vote(_loc2_);
         this.votedFor = _loc2_;
         submitBtn.text = Locale.instance.label(ViewerLabels.labelPollDone);
         resize();
      }
      
      private function createAnswers(... rest) : void {
         var _loc5_:uint = 0;
         var _loc7_:Sprite = null;
         var _loc8_:Label = null;
         Tween.to(this.loader,{"alpha":0},30 * 0.2);
         this.disposeAnswers();
         if(this.scrollbar)
         {
            this.scrollbar.destroy();
            if(content.contains(this.scrollbar))
            {
               content.removeChild(this.scrollbar);
            }
            this.scrollbar = null;
         }
         var _loc2_:uint = 0;
         var _loc3_:uint = 4.294967295E9;
         var _loc4_:uint = 0;
         if((this._module) && (this._module.votes))
         {
            _loc5_ = 0;
            while(_loc5_ < this._module.votes.length)
            {
               _loc2_ = Math.max(_loc2_,this._module.votes[_loc5_]);
               _loc3_ = Math.min(_loc3_,this._module.votes[_loc5_]);
               _loc4_ = _loc4_ + this._module.votes[_loc5_];
               _loc5_++;
            }
         }
         if(_loc3_ >= _loc2_)
         {
            _loc3_ = 0;
         }
         if((this._module) && (this._module.answers))
         {
            this.echo("buttonList");
            _loc5_ = 0;
            while(_loc5_ < this._module.answers.length)
            {
               this.echo("i : " + _loc5_);
               _loc7_ = this.createAnswerListItem(this._module.answers[_loc5_] + (!this._module.canVote?" - " + this._module.votes[_loc5_]:""),_loc5_ + 1,!this._module.canVote,this._module.votes[_loc5_],_loc4_);
               _loc7_.name = "btn_" + _loc5_;
               _loc7_.mouseEnabled = this._module.canVote;
               _loc7_.addEventListener(MouseEvent.MOUSE_OVER,this.onAnswerMouseOver);
               _loc7_.addEventListener(MouseEvent.MOUSE_OUT,this.onAnswerMouseOut);
               _loc7_.addEventListener(MouseEvent.CLICK,this.onAnswerClick);
               this.answerList.push(
                  {
                     "item":_loc7_,
                     "index":_loc5_,
                     "r":55,
                     "g":56,
                     "b":55
                  });
               this.answerList[_loc7_.name] = this.answerList[_loc5_];
               _loc7_.y = _loc5_?this.answerList[_loc5_ - 1].item.y + this.answerList[_loc5_ - 1].item.height:0;
               this.list.addChild(_loc7_);
               _loc5_++;
            }
         }
         else
         {
            this.echo("!buttonList");
         }
         content.scrollRect = null;
         var _loc6_:BitmapData = new BitmapData(1,1);
         _loc6_.draw(content);
         _loc6_.dispose();
         if(this.list.height > 130)
         {
            content.addChild(this.scrollbar = new Scrollbar());
            this.scrollbar.target = this.list;
            this.scrollbar.height = 130;
            this.scrollbar.x = bgWidth - this.scrollbar.width;
            _loc5_ = 0;
            while(_loc5_ < this.answerList.length)
            {
               _loc8_ = this.answerList[_loc5_].item.getChildByName("percent");
               if(_loc8_)
               {
                  _loc8_.x = bgWidth - this.scrollbar.width - _loc8_.width;
               }
               _loc5_++;
            }
            content.scrollRect = new Rectangle(0,0,bgWidth,130);
            _loc6_ = new BitmapData(1,1);
            _loc6_.draw(content);
            _loc6_.dispose();
         }
         resize();
         if((rest) && (rest.length > 0) && !(rest[0].type == "vote"))
         {
            dispatchEvent(new Event("pollEvent",true,true));
         }
      }
      
      private function createAnswerListItem(param1:String, param2:int = 0, param3:Boolean = false, param4:int = -1, param5:int = 0) : Sprite {
         var _loc12_:Label = null;
         var _loc6_:Sprite = new Sprite();
         var _loc7_:Label = new Label();
         var _loc8_:Label = new Label();
         var _loc9_:Shape = new Shape();
         _loc6_.addChild(_loc9_);
         _loc6_.addChild(_loc7_);
         _loc7_.textFormat = new TextFormat("MyriadBoldItalic",12,3618871);
         _loc7_.embedFonts = true;
         _loc7_.textGradientOverlay = [0];
         _loc7_.textGradientOverlayAlpha = [0];
         _loc7_.bgWidth = _loc7_.bgHeight = 16;
         _loc7_.bgCornerRadius = 8;
         _loc7_.bgColor = 16777215;
         _loc7_.bgAlpha = 1;
         _loc7_.text = String(param2);
         _loc7_.x = 12;
         _loc7_.y = Math.round((31 - _loc7_.height) / 2);
         _loc7_.bgFilters = [new DropShadowFilter(1,90,0,0.6,2,2,1,2)];
         _loc7_.name = "num";
         _loc7_.bg.alpha = 0.2;
         if((param3) && param4 > -1 && param5 > 0)
         {
            _loc12_ = new Label();
            _loc12_.textFormat = new TextFormat(ViewerUI.baseFontName,12,16777215);
            _loc12_.embedFonts = ViewerUI.baseFontEmbedded;
            _loc12_.overSample = !ViewerUI.baseFontEmbedded;
            _loc12_.text = Math.round(param4 / param5 * 1000) / 10 + "%";
            _loc12_.filters = [new DropShadowFilter(1,90,0,0.6,2,2,1,2)];
            _loc12_.y = Math.round((31 - _loc12_.height) / 2) - 1;
            _loc12_.x = bgWidth - 12 - _loc12_.width;
            _loc12_.alpha = 0.67;
            _loc12_.name = "percent";
            _loc6_.addChild(_loc12_);
         }
         _loc6_.addChild(_loc8_);
         _loc8_.textFormat = new TextFormat(ViewerUI.baseFontName,14,16777215);
         _loc8_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc8_.overSample = !ViewerUI.baseFontEmbedded;
         _loc8_.multiline = true;
         _loc8_.wordWrap = true;
         _loc8_.textWidth = bgWidth - 24 - _loc7_.width - 8 - (_loc12_?12 - _loc12_.width:0);
         _loc8_.text = param1;
         _loc8_.x = _loc7_.x + _loc7_.width + 8;
         if(!param3 || param4 < 1 || param5 == 0)
         {
            _loc8_.alpha = 0.67;
         }
         _loc8_.filters = [new DropShadowFilter(1,90,0,0.6,2,2,1,2)];
         _loc8_.name = "label";
         var _loc10_:Number = _loc8_.numLines > 1?Math.round(_loc8_.height) + 2 * 6:31;
         _loc8_.y = _loc8_.numLines > 1?5:Math.round((_loc10_ - _loc8_.height) / 2) - 1;
         _loc9_.graphics.beginFill(ViewerUI.style.color);
         _loc9_.graphics.drawRect(0,0,bgWidth,_loc10_);
         _loc9_.graphics.endFill();
         var _loc11_:Matrix = new Matrix();
         _loc11_.createGradientBox(bgWidth,_loc10_,90 * Math.PI / 180);
         _loc9_.graphics.beginGradientFill(GradientType.LINEAR,[16777215,16777215],[0.23,0],[0,255],_loc11_);
         _loc9_.graphics.drawRect(0,0,bgWidth,_loc10_);
         _loc9_.graphics.endFill();
         _loc9_.alpha = 0;
         _loc9_.name = "bg";
         _loc6_.graphics.beginFill(16737792,0);
         _loc6_.graphics.drawRect(0,0,bgWidth,_loc10_);
         _loc6_.graphics.endFill();
         if((param3) && param4 > -1 && param5 > 0)
         {
            _loc6_.graphics.beginFill(16777215,0.06);
            _loc6_.graphics.drawRect(0,0,bgWidth * param4 / param5,_loc10_ - 0.4);
            _loc6_.graphics.endFill();
         }
         _loc6_.scrollRect = new Rectangle(0,0,bgWidth,_loc10_);
         _loc6_.mouseChildren = false;
         _loc6_.buttonMode = true;
         return _loc6_;
      }
      
      private function disposeAnswers(param1:Boolean = true) : void {
         var _loc2_:* = 0;
         while(_loc2_ < this.answerList.length)
         {
            this.answerList[_loc2_].item.removeEventListener(MouseEvent.MOUSE_OVER,this.onAnswerMouseOver);
            this.answerList[_loc2_].item.removeEventListener(MouseEvent.MOUSE_OUT,this.onAnswerMouseOut);
            this.answerList[_loc2_].item.removeEventListener(MouseEvent.CLICK,this.onAnswerClick);
            _loc2_++;
         }
         if(param1)
         {
            while((this.answerList) && (this.answerList.length))
            {
               if(this.list.contains(this.answerList[0].item))
               {
                  this.list.removeChild(this.answerList[0].item);
               }
               this.answerList.splice(0,1);
            }
         }
      }
      
      override public function get notificationType() : String {
         return "";
      }
      
      override public function set notificationType(param1:String) : void {
      }
      
      public function get module() : Poll {
         return this._module;
      }
      
      public function set module(param1:Poll) : void {
         this.echo("set module");
         if(this._module)
         {
            this._module.removeEventListener("destroy",this.onModuleDestroy);
            this._module.removeEventListener("vote",this.createAnswers);
            this._module.removeEventListener("start",this.onModuleStart);
            this._module.removeEventListener("stop",this.showAnswers);
         }
         this._module = param1;
         if(this._module)
         {
            this._module.addEventListener("destroy",this.onModuleDestroy);
            this._module.addEventListener("vote",this.createAnswers);
            this._module.addEventListener("start",this.onModuleStart);
            this._module.addEventListener("stop",this.showAnswers);
            this.votedFor = -1;
            this.onModuleStart();
         }
      }
      
      private function showAnswers(param1:Event) : void {
         this.echo("fghdfgfd fghfgh fg hfgfgfggffg" + !this._started);
         if(!this._started)
         {
            this._started = true;
            title = this._module.question;
            this.echo("!_started = " + !this._started);
            this.echo(this._module.question);
         }
         this.createAnswers(param1);
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ POLLPANE ] " + param1);
      }
      
      private function onModuleStart(param1:Event = null) : void {
         this.echo("onModuleStart");
         title = this._module.question;
         submitBtn.text = this._module.canVote?Locale.instance.label(ViewerLabels.labelPollSkipVoting):Locale.instance.label(ViewerLabels.labelPollDone);
         this.createAnswers();
         dispatchEvent(new Event("pollEvent",true,true));
      }
      
      private function onModuleDestroy(param1:Event) : void {
         this.echo("onModuleDestroy");
         this.disposeAnswers();
         dispatchEvent(new Event("pollEvent",true,true));
      }
      
      override public function destroy() : void {
         super.destroy();
         this.disposeAnswers();
         this._started = false;
         if(this._module)
         {
            this._module.removeEventListener("destroy",this.onModuleDestroy);
            this._module.removeEventListener("vote",this.createAnswers);
            this._module.removeEventListener("start",this.onModuleStart);
            this._module.removeEventListener("stop",this.showAnswers);
         }
         submitBtn.removeEventListener(MouseEvent.CLICK,close);
         submitBtn.destroy();
         if(this.scrollbar)
         {
            this.scrollbar.destroy();
         }
         Tween.kill(this.loader);
         this.loader.destroy();
      }
      
      override protected function updateStyle(... rest) : void {
         var _loc3_:Shape = null;
         var _loc4_:* = NaN;
         var _loc5_:Matrix = null;
         var _loc2_:* = 0;
         while(_loc2_ < this.answerList.length)
         {
            _loc3_ = this.answerList[_loc2_].item.getChildByName("bg");
            _loc4_ = _loc3_.height;
            _loc3_.graphics.clear();
            _loc3_.graphics.beginFill(ViewerUI.style.color);
            _loc3_.graphics.drawRect(0,0,bgWidth,_loc4_);
            _loc3_.graphics.endFill();
            _loc5_ = new Matrix();
            _loc5_.createGradientBox(bgWidth,_loc4_,90 * Math.PI / 180);
            _loc3_.graphics.beginGradientFill(GradientType.LINEAR,[16777215,16777215],[0.23,0],[0,255],_loc5_);
            _loc3_.graphics.drawRect(0,0,bgWidth,_loc4_);
            _loc3_.graphics.endFill();
            _loc2_++;
         }
      }
   }
}
