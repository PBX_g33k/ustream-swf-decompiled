package tv.ustream.viewer.document.ui.panes
{
   import flash.display.Sprite;
   import tv.ustream.viewer.document.ui.Label;
   import flash.display.Shape;
   import flash.display.Loader;
   import tv.ustream.viewer.document.ui.LoaderBar;
   import flash.system.LoaderContext;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   import flash.system.SecurityDomain;
   import flash.net.URLRequest;
   import tv.ustream.tween2.Tween;
   import flash.events.Event;
   import flash.display.Bitmap;
   import flash.events.HTTPStatusEvent;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.filters.DropShadowFilter;
   import flash.filters.GlowFilter;
   import flash.events.IOErrorEvent;
   import flash.events.MouseEvent;
   import tv.ustream.tween2.Color;
   import tv.ustream.tools.Debug;
   import flash.text.TextFormat;
   import flash.display.DisplayObject;
   import flash.display.BlendMode;
   
   public class ChannelRenderer extends Sprite
   {
      
      public function ChannelRenderer() {
         this.PlayIcon = ChannelRenderer_PlayIcon;
         super();
         this.titleLabel = new Label();
         var _loc1_:TextFormat = new TextFormat(ViewerUI.baseFontName,11,16777215);
         _loc1_.leading = 1;
         this.titleLabel.textFormat = _loc1_;
         this.titleLabel.embedFonts = ViewerUI.baseFontEmbedded;
         this.titleLabel.multiline = true;
         this.titleLabel.wordWrap = true;
         this.titleLabel.textWidth = THUMB_WIDTH + 4;
         this.titleLabel.x = -2;
         this.titleLabel.y = THUMB_HEIGHT + 8;
         this.titleLabel.filters = [new DropShadowFilter(1,90,0,0.7,2,2,1,2)];
         this.titleLabel.alpha = 0.7;
         addChild(this.thumb = new Sprite());
         this.thumb.filters = [new DropShadowFilter(1,90,0,0.3,5,5,1,2)];
         this.thumbMask = new Shape();
         this.thumbLoader = new Loader();
         this.thumbLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,this.onThumbLoaded);
         this.thumbLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
         this.thumbLoader.contentLoaderInfo.addEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         this.thumbOverlay = new Sprite();
         this.thumbOverlay.graphics.beginFill(0,0.7);
         this.thumbOverlay.graphics.drawRoundRect(0,0,THUMB_WIDTH,THUMB_HEIGHT,6);
         this.thumbOverlay.graphics.endFill();
         this.thumbOverlay.alpha = 0;
         this.thumbOverlay.visible = false;
         var _loc2_:DisplayObject = this.thumbOverlay.addChild(new this.PlayIcon());
         _loc2_.x = THUMB_WIDTH / 2;
         _loc2_.y = THUMB_HEIGHT / 2 + 10;
         _loc2_.width = _loc2_.height = 32;
         _loc2_.blendMode = BlendMode.LAYER;
         _loc2_.filters = [new DropShadowFilter(1,90,0,0.3,5,5,1,2)];
         Color.tint(Sprite(this.thumbOverlay.getChildAt(0)).getChildByName("icon"),ViewerUI.style.color,1);
         this.outline = 0;
         addChild(this.loader = new LoaderBar(9,2));
         this.loader.alpha = 0;
         this.loader.x = THUMB_WIDTH / 2;
         this.loader.y = THUMB_HEIGHT / 2;
         this.liveBadge = new Label();
         this.liveBadge.embedFonts = true;
         this.liveBadge.textFormat = new TextFormat("MyriadCondBoldItalic",11,16777215);
         this.liveBadge.textGradientOverlay = [16777215,0];
         this.liveBadge.textGradientOverlayAlpha = [0.19,0.19];
         this.liveBadge.textFilters = [new DropShadowFilter(1,90,0,0.14,1,1,1,2)];
         this.liveBadge.bgHeight = 16;
         this.liveBadge.bgCornerRadius = 3;
         this.liveBadge.bgGradientOverlay = [15749440,15277066];
         this.liveBadge.bgGradientOverlayAlpha = [1,1];
         this.liveBadge.padding = 2;
         this.liveBadge.text = "LIVE";
         this.liveBadge.x = this.liveBadge.y = 3;
         this.offairBadge = new Label();
         this.offairBadge.embedFonts = true;
         this.offairBadge.textFormat = new TextFormat("MyriadCondBoldItalic",11,12632256);
         this.offairBadge.textGradientOverlay = [16777215,0];
         this.offairBadge.textGradientOverlayAlpha = [0.1,0.1];
         this.offairBadge.textFilters = [new DropShadowFilter(1,90,0,0.55,1,1,1,2)];
         this.offairBadge.bgHeight = 16;
         this.offairBadge.bgCornerRadius = 3;
         this.offairBadge.bgGradientOverlay = [4013373,2829099];
         this.offairBadge.bgGradientOverlayAlpha = [0.8,0.9];
         this.offairBadge.padding = 2;
         this.offairBadge.text = "OFF AIR";
         this.offairBadge.x = this.offairBadge.y = 3;
         mouseEnabled = true;
         mouseChildren = false;
         buttonMode = true;
         addEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         addEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
         this.onMouseOut();
      }
      
      public static const THUMB_WIDTH:Number = 120;
      
      public static const THUMB_HEIGHT:Number = 68;
      
      private var PlayIcon:Class;
      
      private var titleLabel:Label;
      
      private var thumb:Sprite;
      
      private var thumbMask:Shape;
      
      private var thumbLoader:Loader;
      
      private var thumbOverlay:Sprite;
      
      private var _thumbUrl:String;
      
      private var loader:LoaderBar;
      
      public var cid:String;
      
      private var liveBadge:Label;
      
      private var offairBadge:Label;
      
      private var badge:Label;
      
      private var _status:String;
      
      private var _outline:Number;
      
      public var glowAlpha:Number = 0;
      
      public var glowBlur:Number = 0;
      
      public function get title() : String {
         return this.titleLabel.text;
      }
      
      public function set title(param1:String) : void {
         if(!contains(this.titleLabel))
         {
            addChild(this.titleLabel);
         }
         this.titleLabel.text = param1;
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(0,0,this.width,this.height);
         graphics.endFill();
      }
      
      public function get thumbUrl() : String {
         return this._thumbUrl;
      }
      
      public function set thumbUrl(param1:String) : void {
         if(this._thumbUrl == param1)
         {
            return;
         }
         this._thumbUrl = param1;
         this.echo("set new thumbnail");
         if(this.thumb.contains(this.thumbLoader))
         {
            this.thumb.removeChild(this.thumbLoader);
         }
         this.thumbLoader.unload();
         this.thumbOverlay.visible = false;
         var _loc2_:LoaderContext = new LoaderContext(false,ApplicationDomain.currentDomain);
         if(Security.sandboxType != Security.LOCAL_TRUSTED)
         {
            _loc2_.securityDomain = SecurityDomain.currentDomain;
         }
         this.thumbLoader.load(new URLRequest(this._thumbUrl),_loc2_);
         Tween.to(this.loader,{"alpha":1},30 * 0.2);
      }
      
      private function onThumbLoaded(param1:Event) : void {
         if(this.thumbLoader.content is Bitmap)
         {
            Bitmap(this.thumbLoader.content).smoothing = true;
         }
         this.thumbLoader.width = THUMB_WIDTH;
         this.thumbLoader.scaleY = this.thumbLoader.scaleX;
         if(this.thumbLoader.height < THUMB_HEIGHT)
         {
            this.thumbLoader.height = THUMB_HEIGHT;
            this.thumbLoader.scaleX = this.thumbLoader.scaleY;
         }
         this.thumbLoader.x = Math.round((THUMB_WIDTH - this.thumbLoader.width) / 2);
         this.thumbLoader.y = Math.round((THUMB_HEIGHT - this.thumbLoader.height) / 2);
         this.thumbMask.graphics.clear();
         this.thumbMask.graphics.beginFill(16737792);
         this.thumbMask.graphics.drawRoundRect(0,0,THUMB_WIDTH,THUMB_HEIGHT,6);
         this.thumbLoader.mask = this.thumbMask;
         this.thumb.addChild(this.thumbLoader);
         this.thumb.addChild(this.thumbMask);
         this.thumb.addChild(this.thumbOverlay);
         this.thumbOverlay.visible = true;
         this.thumbLoader.alpha = 0;
         Tween.to(this.thumbLoader,{"alpha":1},30 * 0.6,"easeOutQuart");
         Tween.to(this.loader,{"alpha":0},30 * 0.6,"easeOutQuart");
      }
      
      private function onHttpStatus(param1:HTTPStatusEvent) : void {
         this.echo(param1.toString());
      }
      
      private function onLoaderError(param1:Event) : void {
         this.echo(param1.toString());
      }
      
      public function set status(param1:String) : void {
         if(this._status == param1)
         {
            return;
         }
         this._status = param1;
         this.echo("set status");
         if(this.badge)
         {
            removeChild(this.badge);
         }
         switch(this._status.toUpperCase())
         {
            case "OFFLINE":
               addChild(this.badge = this.offairBadge);
               break;
            case "LIVE":
               addChild(this.badge = this.liveBadge);
               break;
         }
      }
      
      public function get outline() : Number {
         return this._outline;
      }
      
      public function set outline(param1:Number) : void {
         this._outline = param1;
         this.thumb.graphics.clear();
         this.thumb.graphics.beginFill(ViewerUI.style.linkColor);
         this.thumb.graphics.drawRoundRect(-this._outline,-this._outline,THUMB_WIDTH + this._outline * 2,THUMB_HEIGHT + this._outline * 2,6 + this._outline * 2);
         this.thumb.graphics.endFill();
         this.thumb.graphics.beginFill(0);
         this.thumb.graphics.drawRoundRect(0,0,THUMB_WIDTH,THUMB_HEIGHT,6);
         this.thumb.graphics.endFill();
      }
      
      private function onMouseOver(param1:Event = null) : void {
         this.glow = true;
         Tween.to(this.titleLabel,{"alpha":1},30 * 0.2,"easeOutExpo");
         Tween.to(this.thumbOverlay,{"alpha":1},30 * 0.3,"easeOutQuart");
         Tween.to(this.thumbOverlay.getChildAt(0),{"y":THUMB_HEIGHT / 2},30 * 0.3,"easeOutQuart");
      }
      
      private function onMouseOut(param1:Event = null) : void {
         this.glow = false;
         Tween.to(this.titleLabel,{"alpha":0.7},30 * 0.6,"easeOutExpo");
         Tween.to(this.thumbOverlay,{"alpha":0},30 * 0.6,"easeOutExpo");
         Tween.to(this.thumbOverlay.getChildAt(0),{"y":THUMB_HEIGHT / 2 + 10},30 * 0.6,"easeOutExpo");
      }
      
      private function set glow(param1:Boolean) : void {
         var o:Boolean = param1;
         var ol:Number = o?2:0;
         var ga:Number = o?0.25:0;
         var gb:Number = o?6:0;
         var dur:Number = o?0.2:0.4;
         var ease:String = o?"easeOutExpo":"easeOutQuart";
         Tween.to(this,
            {
               "outline":ol,
               "glowAlpha":ga,
               "glowBlur":gb
            },30 * dur,ease,null,null,function():void
         {
            var _loc1_:Array = [new DropShadowFilter(1,90,0,0.3,5,5,1,2)];
            _loc1_.unshift(new GlowFilter(ViewerUI.style.linkColor,glowAlpha,glowBlur,glowBlur,2,3));
            thumb.filters = _loc1_;
         });
      }
      
      override public function get width() : Number {
         return THUMB_WIDTH;
      }
      
      override public function get height() : Number {
         return contains(this.titleLabel)?this.titleLabel.y + this.titleLabel.height:THUMB_HEIGHT;
      }
      
      public function destroy() : void {
         this.thumbLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onThumbLoaded);
         this.thumbLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
         this.thumbLoader.contentLoaderInfo.removeEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         removeEventListener(MouseEvent.MOUSE_OVER,this.onMouseOver);
         removeEventListener(MouseEvent.MOUSE_OUT,this.onMouseOut);
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(... rest) : void {
         this.outline = this._outline;
         Color.tint(Sprite(this.thumbOverlay.getChildAt(0)).getChildByName("icon"),ViewerUI.style.color,1);
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ ChannelRenderer ] " + param1);
      }
   }
}
