package tv.ustream.viewer.document.ui.panes
{
   import flash.text.TextField;
   import flash.display.Sprite;
   import tv.ustream.viewer.document.ui.Label;
   import tv.ustream.viewer.document.ui.CheckBox;
   import tv.ustream.viewer.document.ui.Button;
   import tv.ustream.viewer.document.ui.icons.SocialIcon;
   import flash.geom.Rectangle;
   import tv.ustream.localization.Locale;
   import tv.ustream.viewer.document.ui.LargeButton;
   import flash.events.MouseEvent;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.filters.DropShadowFilter;
   import flash.events.Event;
   import flash.events.FocusEvent;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.viewer.document.ui.RadioButton;
   import tv.ustream.tween2.Tween;
   import flash.display.Shape;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.Shell;
   import flash.text.TextFieldType;
   import flash.geom.Matrix;
   import flash.display.GradientType;
   import tv.ustream.tools.This;
   import flash.system.System;
   import flash.display.BitmapData;
   
   public class EmbedPane extends Pane
   {
      
      public function EmbedPane() {
         this.embedList = [];
         this.embedColorList = [];
         super(true);
         priorityId = 5;
      }
      
      private var embedText:TextField;
      
      private var embedTextHolder:Sprite;
      
      private var customEmbedWidthText:TextField;
      
      private var customEmbedHeightText:TextField;
      
      private var embedList:Array;
      
      private var embedColorLabel:Label;
      
      private var embedColorList:Array;
      
      private var embedWidth:int;
      
      private var embedHeight:int;
      
      private var embedStyle:String = "";
      
      private var embedCode:XML;
      
      private var embedSeo:String = "";
      
      private var switchToOld:Boolean;
      
      private var oldCheckb:CheckBox;
      
      private var barH:int = 32;
      
      override protected function buildUI() : void {
         var _loc3_:Sprite = null;
         var _loc4_:String = null;
         var _loc5_:String = null;
         super.buildUI();
         header.addChild(icon = new Button());
         Button(icon).icon = new SocialIcon("embed");
         Button(icon)._hitArea = new Rectangle(0,0,24,24);
         Button(icon).enabled = false;
         title = Locale.instance.label(ViewerLabels.labelEmbedPaneTitle);
         submitBtn = new LargeButton();
         submitBtn.text = Locale.instance.label(ViewerLabels.labelCopyToClipboard);
         submitBtn.addEventListener(MouseEvent.CLICK,this.onSubmitClick);
         footer.addChild(this.embedTextHolder = new Sprite());
         this.embedTextHolder.addChild(this.embedText = new TextField());
         this.embedText.selectable = true;
         this.embedText.defaultTextFormat = new TextFormat(ViewerUI.baseFontName,14,16777215);
         this.embedText.embedFonts = ViewerUI.baseFontEmbedded;
         this.embedText.y = 4;
         this.embedText.height = 28 - this.embedText.y;
         this.embedText.filters = [new DropShadowFilter(1,90,0,0.31,2,2,1,2)];
         this.embedTextHolder.x = 11;
         this.embedTextHolder.y = 11;
         drawInputBg(this.embedTextHolder,bgWidth - 11 - submitBtn.width - 11 - 11,28);
         this.embedText.x = 4;
         this.embedText.width = this.embedTextHolder.width - 2 * this.embedText.x;
         this.embedList.push(
            {
               "item":this.createEmbedSizeItem(Locale.instance.label(ViewerLabels.labelSizeSmall,{"size":360})),
               "width":360
            },
            {
               "item":this.createEmbedSizeItem(Locale.instance.label(ViewerLabels.labelSizeNormal,{"size":480}),false,true),
               "width":480
            },
            {
               "item":this.createEmbedSizeItem(Locale.instance.label(ViewerLabels.labelSizeLarge,{"size":720})),
               "width":720
            },
            {
               "item":this.createEmbedSizeItem(Locale.instance.label(ViewerLabels.labelCustom),true),
               "custom":true
            });
         var _loc1_:* = 0;
         while(_loc1_ < this.embedList.length)
         {
            content.addChild(this.embedList[_loc1_].item);
            this.embedList[_loc1_].radio = this.embedList[_loc1_].item.getChildByName("radio");
            this.embedList[_loc1_].radio.addEventListener(Event.CHANGE,this.onRadioChange);
            _loc1_++;
         }
         this.customEmbedWidthText.addEventListener(Event.CHANGE,this.onTextInput);
         this.customEmbedWidthText.addEventListener(FocusEvent.FOCUS_IN,this.onSelectCustomEmbedText);
         this.customEmbedHeightText.addEventListener(Event.CHANGE,this.onTextInput);
         this.customEmbedHeightText.addEventListener(FocusEvent.FOCUS_IN,this.onSelectCustomEmbedText);
         if((Logic.hasInstance) && !Logic.instance.scid)
         {
            content.addChild(this.oldCheckb = new CheckBox());
            this.oldCheckb.minHeight = 28;
            this.oldCheckb.embedFonts = ViewerUI.baseFontEmbedded;
            this.oldCheckb.overSample = !ViewerUI.baseFontEmbedded;
            this.oldCheckb.textFormat = new TextFormat(ViewerUI.baseFontName,12,10066329);
            this.oldCheckb.label = Locale.instance.label(ViewerLabels.labelEmbedOldCode);
            this.oldCheckb.iconBgColor = 1710618;
            this.oldCheckb.addEventListener(Event.CHANGE,this.onEmbedSwitch);
         }
         content.addChild(this.embedColorLabel = new Label());
         this.embedColorLabel.textFormat = new TextFormat(ViewerUI.baseFontName,14,16777215,true);
         this.embedColorLabel.embedFonts = ViewerUI.baseFontEmbedded;
         this.embedColorLabel.overSample = !ViewerUI.baseFontEmbedded;
         this.embedColorLabel.text = Locale.instance.label(ViewerLabels.labelCustomizeEmbedCode);
         this.embedColorLabel.filters = [new DropShadowFilter(1,90,0,0.7,2,2,1,2)];
         var _loc2_:Array = [ViewerUI.defaultStyle.color,16727331,8759553,14771895,16740874];
         _loc1_ = 0;
         while(_loc1_ < _loc2_.length)
         {
            _loc3_ = this.createEmbedColorItem(_loc2_[_loc1_]);
            content.addChild(_loc3_);
            _loc4_ = "ub" + _loc2_[_loc1_].toString(16) + ":lc" + _loc2_[_loc1_].toString(16) + ":ocffffff:ucffffff";
            _loc5_ = "ub=" + _loc2_[_loc1_].toString(16) + "&lc=" + _loc2_[_loc1_].toString(16) + "&oc=ffffff&uc=ffffff";
            this.embedColorList.push(
               {
                  "item":_loc3_,
                  "colorCode":_loc4_,
                  "colorParams":_loc5_,
                  "color":_loc2_[_loc1_]
               });
            _loc1_++;
         }
         this.selectEmbedItem(this.embedList[1].radio);
         this.selectEmbedColorItem(this.embedColorList[Math.max(0,_loc2_.indexOf(ViewerUI.style.color))].item,true);
         this.resize();
      }
      
      private function onRadioChange(param1:Event) : void {
         this.selectEmbedItem(RadioButton(param1.currentTarget));
      }
      
      private function selectEmbedItem(param1:RadioButton) : void {
         var _loc3_:RadioButton = null;
         var _loc2_:* = 0;
         while(_loc2_ < this.embedList.length)
         {
            _loc3_ = this.embedList[_loc2_].radio;
            if(_loc3_ == param1)
            {
               if(!_loc3_.checked)
               {
                  _loc3_.checked = true;
               }
               if(this.embedList[_loc2_].custom)
               {
                  Tween.to(this.customEmbedWidthText,{"alpha":1},30 * 0.2,"easeOutExpo");
                  Tween.to(this.customEmbedHeightText,{"alpha":1},30 * 0.2,"easeOutExpo");
                  this.embedWidth = (Number(this.customEmbedWidthText.text)) || 0;
                  this.embedHeight = (Number(this.customEmbedHeightText.text)) || 0;
               }
               else
               {
                  this.embedWidth = this.embedList[_loc2_].width;
                  this.embedHeight = this.embedWidth * 9 / 16;
                  if(ViewerUI.streamBounds)
                  {
                     this.embedHeight = Math.round(this.embedWidth / ViewerUI.streamBounds.width * ViewerUI.streamBounds.height);
                  }
                  this.embedHeight = this.embedHeight + this.barH;
               }
               this.updateEmbedCode();
            }
            else
            {
               _loc3_.checked = false;
               if(this.embedList[_loc2_].custom)
               {
                  Tween.to(this.customEmbedWidthText,{"alpha":0.4},30 * 0.4,"easeOutQuart");
                  Tween.to(this.customEmbedHeightText,{"alpha":0.4},30 * 0.4,"easeOutQuart");
               }
            }
            _loc2_++;
         }
      }
      
      private function embedColorItemOnMouseOver(param1:Event = null) : void {
         param1.currentTarget.addEventListener(MouseEvent.MOUSE_OUT,this.embedColorItemOnMouseOut);
         Tween.to(param1.currentTarget.getChildByName("bg"),
            {
               "width":28,
               "height":28,
               "alpha":1
            },30 * 0.2,"easeOutExpo");
      }
      
      private function embedColorItemOnMouseOut(param1:Event = null) : void {
         param1.currentTarget.removeEventListener(MouseEvent.MOUSE_OUT,this.embedColorItemOnMouseOut);
         Tween.to(param1.currentTarget.getChildByName("bg"),
            {
               "width":24,
               "height":24,
               "alpha":0
            },30 * 0.4,"easeOutQuart");
      }
      
      private function embedColorItemOnClick(param1:MouseEvent = null) : void {
         this.selectEmbedColorItem(Sprite(param1.currentTarget));
      }
      
      private function selectEmbedColorItem(param1:Sprite, param2:Boolean = false) : void {
         var _loc4_:Sprite = null;
         var _loc5_:Shape = null;
         var _loc3_:* = 0;
         while(_loc3_ < this.embedColorList.length)
         {
            _loc4_ = this.embedColorList[_loc3_].item;
            _loc5_ = _loc4_.getChildByName("bg") as Shape;
            this.embedColorList[_loc3_].checked = _loc4_ == param1;
            if(_loc4_ == param1)
            {
               _loc4_.mouseEnabled = false;
               _loc4_.removeEventListener(MouseEvent.CLICK,this.embedColorItemOnClick);
               this.embedStyle = _loc3_ > 0?this.switchToOld?this.embedColorList[_loc3_].colorCode:this.embedColorList[_loc3_].colorParams:"";
               if(param2)
               {
                  _loc5_.width = _loc5_.height = 28;
                  _loc5_.alpha = 1;
               }
               else
               {
                  _loc5_.scaleX = _loc5_.scaleY = 0.6;
                  Tween.to(_loc5_,
                     {
                        "width":28,
                        "height":28,
                        "alpha":1
                     },30 * 0.4,"easeOutBack");
               }
               dispatchEvent(new DynamicEvent(Event.CHANGE,true,false,{"style":this.embedColorList[_loc3_].colorCode}));
               if((Logic.hasInstance) && (Logic.instance.scid) && (Shell.hasInstance))
               {
                  Shell.instance.dispatch(Logic.instance.type,"styleSwitch",{"style":this.embedColorList[_loc3_].colorCode});
               }
               this.updateEmbedCode();
            }
            else
            {
               _loc4_.mouseEnabled = true;
               _loc4_.addEventListener(MouseEvent.CLICK,this.embedColorItemOnClick);
               Tween.to(_loc5_,
                  {
                     "width":24,
                     "height":24,
                     "alpha":0
                  },param2?1:30 * 0.4,"easeOutQuart");
            }
            _loc3_++;
         }
      }
      
      private function createEmbedSizeItem(param1:String, param2:Boolean = false, param3:Boolean = false) : Sprite {
         var _loc6_:Label = null;
         var _loc7_:* = 0;
         var _loc8_:Sprite = null;
         var _loc9_:TextField = null;
         var _loc10_:TextFormat = null;
         var _loc4_:Sprite = new Sprite();
         var _loc5_:RadioButton = new RadioButton(param3);
         _loc4_.addChild(_loc5_);
         _loc5_.textFormat = new TextFormat(ViewerUI.baseFontName,14,16777215);
         _loc5_.embedFonts = ViewerUI.baseFontEmbedded;
         _loc5_.overSample = !ViewerUI.baseFontEmbedded;
         _loc5_.minHeight = 28;
         _loc5_.maxWidth = (_bgWidth - 3 * 12) / 2;
         _loc5_.label = param1;
         _loc5_.name = "radio";
         if(param2)
         {
            _loc6_ = new Label();
            _loc6_.textFormat = new TextFormat(ViewerUI.baseFontName,11,5592405,true);
            _loc6_.embedFonts = ViewerUI.baseFontEmbedded;
            _loc6_.overSample = !ViewerUI.baseFontEmbedded;
            _loc6_.text = "x";
            _loc6_.filters = [new DropShadowFilter(1,90,0,0.7,2,2,1,2)];
            _loc6_.y = Math.round((28 - _loc6_.height) / 2);
            _loc4_.addChild(_loc6_);
            _loc7_ = 0;
            while(_loc7_ < 2)
            {
               _loc8_ = new Sprite();
               _loc9_ = new TextField();
               _loc8_.mouseEnabled = false;
               drawInputBg(_loc8_,48,28);
               _loc8_.x = _loc5_.x + _loc5_.width + 10 + _loc7_ * 48 + _loc7_ * 16;
               _loc8_.name = _loc7_ == 0?"widthLabel":"heightLabel";
               if(_loc7_ == 0)
               {
                  _loc6_.x = _loc8_.x + _loc8_.width + Math.round((16 - _loc6_.width) / 2);
               }
               _loc10_ = new TextFormat(ViewerUI.baseFontName,14,16777215);
               _loc10_.align = "center";
               _loc9_.embedFonts = ViewerUI.baseFontEmbedded;
               _loc9_.defaultTextFormat = _loc10_;
               _loc9_.type = TextFieldType.INPUT;
               _loc9_.maxChars = 5;
               _loc9_.restrict = "0-9";
               _loc9_.text = "";
               _loc9_.y = 4;
               _loc9_.width = _loc8_.width;
               _loc9_.height = 28 - _loc9_.y;
               _loc9_.filters = [new DropShadowFilter(1,90,0,0.7,2,2,1,2)];
               _loc9_.name = "txt";
               _loc9_.alpha = 0.4;
               if(_loc7_ == 0)
               {
                  this.customEmbedWidthText = _loc9_;
               }
               else
               {
                  this.customEmbedHeightText = _loc9_;
               }
               _loc8_.addChild(_loc9_);
               _loc4_.addChild(_loc8_);
               _loc7_++;
            }
         }
         return _loc4_;
      }
      
      private function createEmbedColorItem(param1:uint) : Sprite {
         var _loc2_:Sprite = new Sprite();
         var _loc3_:Shape = new Shape();
         var _loc4_:Shape = new Shape();
         _loc2_.addChild(_loc3_);
         _loc3_.graphics.beginFill(16777215);
         _loc3_.graphics.drawRoundRect(-14,-14,28,28,14);
         _loc3_.graphics.endFill();
         _loc3_.x = _loc3_.y = 14;
         _loc3_.width = _loc3_.height = 24;
         _loc3_.alpha = 0;
         _loc3_.name = "bg";
         _loc2_.addChild(_loc4_);
         _loc4_.graphics.beginFill(param1);
         _loc4_.graphics.drawRoundRect(0,0,28 - 4,28 - 4,10);
         _loc4_.graphics.endFill();
         var _loc5_:Matrix = new Matrix();
         _loc5_.createGradientBox(28 - 4,28 - 4,90 * Math.PI / 180);
         _loc4_.graphics.beginGradientFill(GradientType.LINEAR,[0,16777215],[0.06,0.06],[0,255],_loc5_);
         _loc4_.graphics.drawRoundRect(0,0,28 - 4,28 - 4,10);
         _loc4_.graphics.endFill();
         _loc4_.x = 2;
         _loc4_.y = 2;
         _loc2_.graphics.beginFill(16777215,0);
         _loc2_.graphics.drawRoundRect(0,0,28,28,14);
         _loc2_.graphics.endFill();
         _loc2_.filters = [new DropShadowFilter(1,90,0,0.3,5,5,1,2)];
         _loc2_.buttonMode = _loc2_.useHandCursor = true;
         return _loc2_;
      }
      
      private function onTextInput(param1:Event) : void {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         switch(param1.currentTarget)
         {
            case this.customEmbedWidthText:
               _loc2_ = int(this.customEmbedWidthText.text);
               _loc3_ = 0;
               if(_loc2_)
               {
                  _loc3_ = _loc2_ * 9 / 16;
                  if(ViewerUI.streamBounds)
                  {
                     _loc3_ = Math.round(_loc2_ / ViewerUI.streamBounds.width * ViewerUI.streamBounds.height);
                  }
                  _loc3_ = _loc3_ + this.barH;
               }
               this.customEmbedHeightText.text = String(_loc3_);
               break;
            case this.customEmbedHeightText:
               _loc3_ = int(this.customEmbedHeightText.text);
               _loc2_ = 0;
               if((_loc3_) && _loc3_ > this.barH)
               {
                  _loc2_ = (_loc3_ - this.barH) * 16 / 9;
                  if(ViewerUI.streamBounds)
                  {
                     _loc2_ = Math.round((_loc3_ - this.barH) / ViewerUI.streamBounds.height * ViewerUI.streamBounds.width);
                  }
               }
               this.customEmbedWidthText.text = String(_loc2_);
               break;
         }
         this.embedWidth = _loc2_;
         this.embedHeight = _loc3_;
         this.updateEmbedCode();
         param1.stopPropagation();
      }
      
      private function onSelectCustomEmbedText(param1:Event) : void {
         this.selectEmbedItem(this.embedList[3].radio);
      }
      
      private function updateEmbedCode() : void {
         var _loc1_:String = null;
         var _loc3_:String = null;
         var _loc4_:String = null;
         var _loc5_:String = null;
         var _loc6_:XMLList = null;
         var _loc7_:uint = 0;
         var _loc2_:Array = ["12762028","13027426","13027427","13027441","13027444","13628076","13027446","13628077","13027422","13027369","14225913","14420561","13773635","14812707","14812715","14839273","15006733","15006737","15006743"];
         if(this.switchToOld)
         {
            _loc1_ = "<object width=\"480\" height=\"297\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\">" + "<param name=\"flashvars\" value=\"\"/><param name=\"allowfullscreen\" value=\"true\"/>" + "<param name=\"allowscriptaccess\" value=\"always\"/>" + "<param name=\"src\" value=\"\"/>" + "<embed flashvars=\"\" width=\"480\" height=\"297\" allowfullscreen=\"true\" allowscriptaccess=\"always\" src=\"\" type=\"application/x-shockwave-flash\">!foo</embed></object>";
         }
         else
         {
            _loc1_ = "<iframe width=\"608\" height=\"368\" src=\"\" scrolling=\"no\" frameborder=\"0\" style=\"border: 0px none transparent;\"><other/>!foo</iframe>";
         }
         this.embedCode = new XML(_loc1_);
         this.embedCode.@width = this.embedWidth;
         this.embedCode.@height = this.embedHeight;
         if(this.switchToOld)
         {
            this.embedCode.embed[0].@width = this.embedWidth;
            this.embedCode.embed[0].@height = this.embedHeight;
         }
         if(Logic.hasInstance)
         {
            if((this.embedSeo.length) && _loc2_.indexOf(Logic.instance.media.id) > -1)
            {
               this.embedSeo = "<br /><a href=\"http://www.apl.tv/\" style=\"padding: 2px 0px 4px; width: " + this.embedWidth + "px; background: #000000; display: block; color: #ffffff; font-weight: normal; font-family: Arial, sans-serif; font-size: 10px; text-decoration: underline; text-align: center;\" target=\"_blank\">Live video by Animal Planet L!ve</a>";
            }
            else
            {
               if(Logic.instance.scid)
               {
                  this.embedCode.@height = Number(this.embedCode.@height) + 147;
               }
               _loc4_ = Logic.hasInstance?This.getReference(Logic.instance,"media.modules.meta.embeds.media") as String:null;
               if((_loc4_) && _loc4_.indexOf("<a href") > -1)
               {
                  this.embedSeo = "<br />" + _loc4_.substr(_loc4_.indexOf("<a href"));
               }
               else
               {
                  this.embedSeo = "<br /><a href=\"http://www.ustream.tv\" style=\"font-size: 12px; line-height: 20px; font-weight: normal; text-align: left;\" target=\"_blank\">Broadcast live streaming video on Ustream</a>";
               }
            }
         }
         if(this.switchToOld)
         {
            _loc3_ = "http://www.ustream.tv/flash/viewer.swf";
            _loc5_ = "";
            if(Logic.instance.channel)
            {
               _loc5_ = _loc5_ + ("cid=" + Logic.instance.channel.mediaId);
            }
            else if(Logic.instance.media.type == "recorded")
            {
               _loc5_ = _loc5_ + ("vid=" + Logic.instance.media.id);
               if(Logic.instance.media.highlightId)
               {
                  _loc5_ = _loc5_ + ("&hid=" + Logic.instance.media.highlightId);
               }
               else if((Logic.loaderInfo) && (Logic.loaderInfo.parameters.hid))
               {
                  _loc5_ = _loc5_ + ("&hid=" + Logic.loaderInfo.parameters.hid);
               }
               
            }
            
            if(_loc2_.indexOf(Logic.instance.media.id) > -1)
            {
               _loc5_ = _loc5_ + "&autoplay=true";
            }
            else
            {
               _loc5_ = _loc5_ + "&autoplay=false";
            }
            if(this.embedStyle)
            {
               _loc5_ = _loc5_ + ("&style=" + this.embedStyle);
            }
            if((Locale.hasInstance) && !(Locale.instance.language == "en_US"))
            {
               _loc5_ = _loc5_ + ("&locale=" + Locale.instance.language);
            }
            _loc6_ = this.embedCode.param;
            _loc7_ = 0;
            while(_loc7_ < _loc6_.length())
            {
               if(_loc6_[_loc7_].@name == "flashvars")
               {
                  _loc6_[_loc7_].@value = _loc5_;
               }
               if(_loc6_[_loc7_].@name == "src")
               {
                  _loc6_[_loc7_].@value = _loc3_;
               }
               _loc7_++;
            }
            this.embedCode.embed[0].@flashvars = _loc5_;
            this.embedCode.embed[0].@src = _loc3_;
         }
         else
         {
            if(Logic.instance.scid)
            {
               _loc3_ = "http://www.ustream.tv/embed/schannel/" + Logic.instance.scid;
            }
            else if(Logic.instance.channel)
            {
               _loc3_ = "http://www.ustream.tv/embed/" + Logic.instance.channel.mediaId;
            }
            else if(Logic.instance.media.type == "recorded")
            {
               _loc3_ = "http://www.ustream.tv/embed/recorded/" + Logic.instance.media.id;
               if(Logic.instance.media.highlightId)
               {
                  _loc3_ = _loc3_ + ("/highlight/" + Logic.instance.media.highlightId);
               }
            }
            
            
            if(this.embedStyle)
            {
               _loc3_ = _loc3_ + ("?" + this.embedStyle);
            }
            _loc3_ = _loc3_ + ((this.embedStyle?"&":"?") + "v=3&wmode=direct");
            if(_loc2_.indexOf(Logic.instance.media.id) > -1)
            {
               _loc3_ = _loc3_ + "&autoplay=true";
            }
            this.embedCode[0].@src = _loc3_;
         }
         this.embedText.text = String(this.embedCode + this.embedSeo).split("<other/>").join("").split("\n").join("").split("!foo").join("");
      }
      
      private function onSubmitClick(param1:MouseEvent = null) : void {
         if((this.embedText) && !(this.embedText.text == ""))
         {
            System.setClipboard(this.switchToOld?this.embedCode.toString().split("<other/>").join("").split("!foo").join("") + String.fromCharCode(13) + this.embedSeo:this.embedCode.toString().split("<other/>").join("").split("!foo").join("").split("\n").join("") + String.fromCharCode(13) + this.embedSeo);
         }
      }
      
      private function onEmbedSwitch(param1:Event) : void {
         var _loc2_:* = 0;
         this.switchToOld = this.oldCheckb.checked;
         if(this.embedStyle)
         {
            _loc2_ = 0;
            while(_loc2_ < this.embedColorList.length)
            {
               if(this.embedColorList[_loc2_].checked)
               {
                  this.embedStyle = this.switchToOld?this.embedColorList[_loc2_].colorCode:this.embedColorList[_loc2_].colorParams;
                  break;
               }
               _loc2_++;
            }
         }
         this.updateEmbedCode();
      }
      
      override public function get notificationType() : String {
         return "";
      }
      
      override public function set notificationType(param1:String) : void {
      }
      
      override public function destroy() : void {
         submitBtn.removeEventListener(MouseEvent.CLICK,this.onSubmitClick);
         super.destroy();
         var _loc1_:* = 0;
         while(_loc1_ < this.embedList.length)
         {
            this.embedList[_loc1_].radio.removeEventListener(Event.CHANGE,this.onRadioChange);
            RadioButton(this.embedList[_loc1_].radio).destroy();
            _loc1_++;
         }
         _loc1_ = 0;
         while(_loc1_ < this.embedColorList.length)
         {
            this.embedColorList[_loc1_].item.removeEventListener(MouseEvent.CLICK,this.embedColorItemOnClick);
            _loc1_++;
         }
         if(this.oldCheckb)
         {
            this.oldCheckb.removeEventListener(Event.CHANGE,this.onEmbedSwitch);
            this.oldCheckb.destroy();
         }
         this.customEmbedWidthText.removeEventListener(Event.CHANGE,this.onTextInput);
         this.customEmbedWidthText.removeEventListener(FocusEvent.FOCUS_IN,this.onSelectCustomEmbedText);
         this.customEmbedHeightText.removeEventListener(Event.CHANGE,this.onTextInput);
         this.customEmbedHeightText.removeEventListener(FocusEvent.FOCUS_IN,this.onSelectCustomEmbedText);
      }
      
      override public function resize(... rest) : void {
         var _loc2_:* = 0;
         var _loc3_:BitmapData = null;
         var _loc4_:Sprite = null;
         if(this.embedList.length)
         {
            _loc2_ = 0;
            while(_loc2_ < this.embedList.length)
            {
               this.embedList[_loc2_].item.x = 12;
               this.embedList[_loc2_].item.y = _loc2_ > 0?this.embedList[_loc2_ - 1].item.y + this.embedList[_loc2_ - 1].item.height + (this.embedList[_loc2_ - 1].item.height > 28?4:0):this.embedList[_loc2_].item.height > 28?14:10;
               _loc2_++;
            }
            if(this.oldCheckb)
            {
               this.oldCheckb.x = 12;
               this.oldCheckb.y = this.embedList[this.embedList.length - 1].item.y + 28 + 14;
               if(this.oldCheckb.width > _bgWidth - 2 * 12)
               {
                  this.oldCheckb.maxWidth = _bgWidth - 2 * 12;
               }
            }
            this.embedColorLabel.x = 12 + 202 + 12;
            this.embedColorLabel.y = 14;
            if(this.embedColorLabel.width > (_bgWidth - 3 * 12) / 2)
            {
               this.embedColorLabel.multiline = this.embedColorLabel.wordWrap = true;
               this.embedColorLabel.textWidth = (_bgWidth - 3 * 12) / 2;
            }
            _loc2_ = 0;
            while(_loc2_ < this.embedColorList.length)
            {
               _loc4_ = this.embedColorList[_loc2_].item;
               _loc4_.x = this.embedColorLabel.x + _loc2_ * 8 + _loc2_ * 28;
               _loc4_.y = this.embedColorLabel.y + this.embedColorLabel.height + 6;
               _loc2_++;
            }
            content.scrollRect = null;
            _loc3_ = new BitmapData(1,1);
            _loc3_.draw(content);
            _loc3_.dispose();
            content.scrollRect = new Rectangle(0,0,bgWidth,content.height + this.embedList[0].item.y + 10);
         }
         super.resize.apply(this,rest);
      }
   }
}
