package tv.ustream.viewer.document.ui.panes
{
   import tv.ustream.viewer.document.ui.Label;
   import tv.ustream.viewer.document.ui.ComboBox;
   import tv.ustream.viewer.document.ui.LargeButton;
   import flash.events.MouseEvent;
   import tv.ustream.localization.Locale;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.viewer.logic.modules.AgeVerification;
   import tv.ustream.viewer.logic.modules.AgeLock;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.filters.DropShadowFilter;
   
   public class AgePane extends Pane
   {
      
      public function AgePane(param1:* = null) {
         this.birthDate = new Date();
         this._module = param1;
         super(false);
         priorityId = 2;
      }
      
      private var _module;
      
      private var bodyLabel:Label;
      
      protected var birthDate:Date;
      
      private var dayCombo:ComboBox;
      
      private var monthCombo:ComboBox;
      
      private var yearCombo:ComboBox;
      
      override protected function buildUI() : void {
         var _loc1_:uint = 0;
         var _loc2_:Array = null;
         var _loc7_:ComboBox = null;
         super.buildUI();
         submitBtn = new LargeButton();
         submitBtn.enabled = false;
         submitBtn.addEventListener(MouseEvent.CLICK,this.onSubmitClick);
         submitBtn.text = Locale.instance.label(ViewerLabels.labelAgePaneSubmit);
         var _loc3_:Array = ["443086","11232192","11232197","13355653","13355657","13355692","11232200","13285980","13293463","13369161","16428958","13434778","13434793","13434812"];
         if((Logic.hasInstance) && (Logic.instance.channel) && !(_loc3_.indexOf(Logic.instance.channel.mediaId) == -1))
         {
            title = "Please enter your date of birth";
         }
         else
         {
            title = Locale.instance.label(ViewerLabels.labelAgePaneHeader);
         }
         if((this._module) && !((Logic.hasInstance) && (Logic.instance.channel) && !(_loc3_.indexOf(Logic.instance.channel.mediaId) == -1)))
         {
            this.body = Locale.instance.label(ViewerLabels.labelAgePaneTitle,{"minage":(this._module as AgeVerification || this._module as AgeLock).minage}).split("\\n").join(" ");
         }
         footer.addChild(this.monthCombo = new ComboBox(Locale.instance.label(ViewerLabels.labelMonth)));
         footer.addChild(this.dayCombo = new ComboBox(Locale.instance.label(ViewerLabels.labelDay)));
         footer.addChild(this.yearCombo = new ComboBox(Locale.instance.label(ViewerLabels.labelYear)));
         var _loc4_:Array = [this.monthCombo,this.dayCombo,this.yearCombo];
         var _loc5_:Array = [120,60,80];
         _loc1_ = 0;
         while(_loc1_ < _loc4_.length)
         {
            _loc7_ = _loc4_[_loc1_];
            _loc7_.y = 10;
            _loc7_.x = (_loc1_ > 0?_loc4_[_loc1_ - 1].x + _loc4_[_loc1_ - 1].width:6) + 6;
            _loc7_.width = _loc5_[_loc1_];
            _loc7_.height = submitBtn.height;
            _loc7_.addEventListener(Event.CHANGE,this.onItemSelect);
            _loc1_++;
         }
         var _loc6_:Array = new Array(Locale.instance.label(ViewerLabels.monthJanuary),Locale.instance.label(ViewerLabels.monthFebruary),Locale.instance.label(ViewerLabels.monthMarch),Locale.instance.label(ViewerLabels.monthApril),Locale.instance.label(ViewerLabels.monthMay),Locale.instance.label(ViewerLabels.monthJune),Locale.instance.label(ViewerLabels.monthJuly),Locale.instance.label(ViewerLabels.monthAugust),Locale.instance.label(ViewerLabels.monthSeptember),Locale.instance.label(ViewerLabels.monthOctober),Locale.instance.label(ViewerLabels.monthNovember),Locale.instance.label(ViewerLabels.monthDecember));
         _loc2_ = [];
         _loc1_ = 0;
         while(_loc1_ < _loc6_.length)
         {
            _loc2_.push(
               {
                  "label":_loc6_[_loc1_],
                  "value":_loc1_
               });
            _loc1_++;
         }
         this.monthCombo.addItem.apply(this.monthCombo,_loc2_);
         _loc2_ = [];
         _loc1_ = 0;
         while(_loc1_ < 31)
         {
            _loc2_.push(
               {
                  "label":String(_loc1_ + 1),
                  "value":_loc1_ + 1
               });
            _loc1_++;
         }
         this.dayCombo.addItem.apply(this.dayCombo,_loc2_);
         _loc2_ = [];
         _loc1_ = new Date().getFullYear();
         while(_loc1_ >= new Date().getFullYear() - 100)
         {
            _loc2_.push(
               {
                  "label":String(_loc1_),
                  "value":_loc1_
               });
            _loc1_--;
         }
         this.yearCombo.addItem.apply(this.yearCombo,_loc2_);
      }
      
      override public function get notificationType() : String {
         return "";
      }
      
      override public function set notificationType(param1:String) : void {
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ AGEPANE ] " + param1);
      }
      
      public function get body() : String {
         return this.bodyLabel?this.bodyLabel.text:"";
      }
      
      public function set body(param1:String) : void {
         var _loc2_:TextFormat = null;
         if(!param1)
         {
            return;
         }
         if(!this.bodyLabel)
         {
            _loc2_ = new TextFormat(ViewerUI.baseFontName,14,12632256,false);
            _loc2_.leading = 4;
            content.addChild(this.bodyLabel = new Label());
            this.bodyLabel.textFormat = _loc2_;
            this.bodyLabel.multiline = true;
            this.bodyLabel.wordWrap = true;
            this.bodyLabel.textFilters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
            this.bodyLabel.embedFonts = ViewerUI.baseFontEmbedded;
            this.bodyLabel.overSample = !ViewerUI.baseFontEmbedded;
         }
         this.bodyLabel.x = this.bodyLabel.y = 12;
         this.bodyLabel.textWidth = bgWidth - 24;
         this.bodyLabel.multiline = true;
         this.bodyLabel.wordWrap = true;
         this.bodyLabel.text = param1;
         content.graphics.clear();
         content.graphics.beginFill(0,0);
         content.graphics.drawRect(0,0,bgWidth,this.bodyLabel.height + this.bodyLabel.y * 2);
         content.graphics.endFill();
         resize();
      }
      
      private function onItemSelect(param1:Event) : void {
         submitBtn.enabled = (this.dayCombo.selectedItem) && (this.monthCombo.selectedItem) && (this.yearCombo.selectedItem);
         this.echo("submitBtn.enabled : " + submitBtn.enabled);
         switch(param1.currentTarget)
         {
            case this.dayCombo:
               this.birthDate.date = this.dayCombo.selectedItem.value;
               break;
            case this.monthCombo:
               this.birthDate.month = this.monthCombo.selectedItem.value;
               break;
            case this.yearCombo:
               this.birthDate.fullYear = this.yearCombo.selectedItem.value;
               break;
         }
      }
      
      private function onSubmitClick(param1:MouseEvent) : void {
         if(this._module is AgeVerification)
         {
            Debug.echo("_module is AgeVerification: ");
            (this._module as AgeVerification).setAge(this.birthDate.getTime());
         }
         if(this._module is AgeLock)
         {
            Debug.echo("_module is AgeLock:  ");
            (this._module as AgeLock).setAge(this.birthDate.getTime());
         }
         submitBtn.enabled = false;
      }
      
      override public function destroy() : void {
         super.destroy();
         submitBtn.removeEventListener(MouseEvent.CLICK,close);
         submitBtn.destroy();
         this.dayCombo.removeEventListener(Event.CHANGE,this.onItemSelect);
         this.dayCombo.destroy();
         this.monthCombo.removeEventListener(Event.CHANGE,this.onItemSelect);
         this.monthCombo.destroy();
         this.yearCombo.removeEventListener(Event.CHANGE,this.onItemSelect);
         this.yearCombo.destroy();
      }
   }
}
