package tv.ustream.viewer.document.ui.panes
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class ChannelRenderer_PlayIcon extends SpriteAsset
   {
      
      public function ChannelRenderer_PlayIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
