package tv.ustream.viewer.document.ui.panes
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class PpvPane_AlertIcon extends SpriteAsset
   {
      
      public function PpvPane_AlertIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
