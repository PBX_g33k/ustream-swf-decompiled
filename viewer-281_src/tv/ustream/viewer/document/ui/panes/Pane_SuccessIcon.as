package tv.ustream.viewer.document.ui.panes
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class Pane_SuccessIcon extends SpriteAsset
   {
      
      public function Pane_SuccessIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
