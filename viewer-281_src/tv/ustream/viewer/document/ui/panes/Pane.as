package tv.ustream.viewer.document.ui.panes
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.display.Bitmap;
   import tv.ustream.viewer.document.ui.Button;
   import tv.ustream.viewer.document.ui.Label;
   import flash.display.DisplayObject;
   import flash.events.Event;
   import flash.filters.DropShadowFilter;
   import flash.geom.Rectangle;
   import flash.events.MouseEvent;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   import flash.geom.PerspectiveProjection;
   import tv.ustream.tween2.Tween;
   import flash.geom.Point;
   import flash.display.GradientType;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.viewer.document.ui.LoaderBar;
   import flash.filters.GlowFilter;
   
   public class Pane extends Sprite
   {
      
      public function Pane(param1:Boolean = true, param2:DisplayObject = null) {
         this.SuccessIcon = Pane_SuccessIcon;
         this.AlertIcon = Pane_AlertIcon;
         super();
         this.closeable = param1;
         visible = false;
         this.buildUI();
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      public static const INIT_OPEN:String = "initOpen";
      
      public static const INIT_CLOSE:String = "initClose";
      
      private var SuccessIcon:Class;
      
      private var AlertIcon:Class;
      
      private var closeable:Boolean;
      
      private var autoOpen:Boolean;
      
      protected var _bgWidth:Number = 440;
      
      protected var _bgHeight:Number = 48;
      
      protected var _minBgWidth:Number = 440;
      
      protected var overlay:Shape;
      
      protected var paneHolder:Sprite;
      
      private var paneHolderImg:Bitmap;
      
      protected var bg:Shape;
      
      protected var header:Sprite;
      
      protected var content:Sprite;
      
      protected var footer:Sprite;
      
      protected var exitBtn:Button;
      
      protected var titleLabel:Label;
      
      private var _notificationType:String;
      
      protected var icon:DisplayObject;
      
      private var _submitBtn:Button;
      
      public var priorityId:int = 3;
      
      private var _wasOpened:Boolean;
      
      protected var _width:Number = 0;
      
      protected var _height:Number = 0;
      
      protected var autoRefreshBitmap:Boolean;
      
      public var persist:Boolean;
      
      protected function onAddedToStage(param1:Event = null) : void {
         visible = false;
         this.resize();
         addEventListener(Event.ENTER_FRAME,this.open);
         addEventListener(Event.ENTER_FRAME,this.refreshBitmap);
      }
      
      protected function buildUI() : void {
         var _loc1_:Shape = null;
         addChild(this.overlay = new Shape());
         addChild(this.paneHolder = new Sprite());
         this.paneHolder.addChild(this.bg = new Shape());
         this.bg.filters = [new DropShadowFilter(1,90,0,0.77,4,4,1,2),new DropShadowFilter(1,90,16777215,0.11,1,1,1,1,true)];
         this.paneHolder.addChild(this.header = new Sprite());
         this.paneHolder.addChild(this.content = new Sprite());
         this.paneHolder.addChild(this.footer = new Sprite());
         if(this.closeable)
         {
            this.header.addChild(this.exitBtn = new Button());
            _loc1_ = new Shape();
            _loc1_.graphics.lineStyle(4,16777215);
            _loc1_.graphics.moveTo(2 - 7,2 - 7);
            _loc1_.graphics.lineTo(7 - 2,7 - 2);
            _loc1_.graphics.moveTo(2 - 7,7 - 2);
            _loc1_.graphics.lineTo(7 - 2,2 - 7);
            _loc1_.graphics.endFill();
            _loc1_.width = _loc1_.height = 12;
            _loc1_.filters = [new DropShadowFilter(1,90,0,0.7,2,2,2,2)];
            this.exitBtn.icon = _loc1_;
            this.exitBtn._hitArea = new Rectangle(0,0,20,20);
            this.exitBtn.addEventListener(MouseEvent.CLICK,this.close);
         }
      }
      
      private function drawBitmap() : BitmapData {
         var _loc1_:Rectangle = this.paneHolder.getBounds(this);
         _loc1_.inflate(20,20);
         var _loc2_:BitmapData = new BitmapData(Math.ceil(_loc1_.width),Math.ceil(_loc1_.height),true,16737792);
         this.overlay.visible = false;
         if(this.paneHolderImg)
         {
            this.paneHolderImg.visible = false;
         }
         this.paneHolder.visible = true;
         _loc2_.draw(this,new Matrix(1,0,0,1,-_loc1_.x,-_loc1_.y));
         this.overlay.visible = true;
         if(this.paneHolderImg)
         {
            this.paneHolderImg.visible = true;
         }
         this.paneHolder.visible = false;
         return _loc2_;
      }
      
      private function refreshBitmap(param1:Event = null) : void {
         if((this.paneHolderImg) && (this.autoRefreshBitmap))
         {
            this.paneHolderImg.bitmapData.dispose();
            this.paneHolderImg.bitmapData = this.drawBitmap();
            this.paneHolderImg.smoothing = true;
         }
      }
      
      private function getPaneHolderAsBitmap() : Bitmap {
         var _loc1_:Rectangle = this.paneHolder.getBounds(this);
         _loc1_.inflate(20,20);
         var _loc2_:Bitmap = new Bitmap(this.drawBitmap(),"auto",true);
         _loc2_.x = _loc1_.x;
         _loc2_.y = _loc1_.y;
         return _loc2_;
      }
      
      public function open(param1:Event = null) : void {
         var e:Event = param1;
         this._wasOpened = true;
         dispatchEvent(new Event(Pane.INIT_OPEN));
         this.resize();
         removeEventListener(Event.ENTER_FRAME,this.open);
         if(this.paneHolderImg)
         {
            Tween.kill(this.paneHolderImg);
            removeChild(this.paneHolderImg);
            this.paneHolderImg.bitmapData.dispose();
            this.paneHolderImg = null;
         }
         addChild(this.paneHolderImg = this.getPaneHolderAsBitmap());
         visible = true;
         this.overlay.alpha = 1;
         this.overlay.y = this.overlay.y + stage.stageHeight;
         var px:Number = this.paneHolderImg.x;
         var py:Number = this.paneHolderImg.y;
         this.paneHolderImg.y = this.paneHolderImg.y + this.paneHolderImg.height * 0.9;
         this.paneHolderImg.alpha = 0;
         this.paneHolderImg.scaleX = this.paneHolderImg.width / (this.paneHolderImg.width + 1);
         this.paneHolderImg.scaleY = this.paneHolderImg.height / (this.paneHolderImg.height + 1);
         var pp:PerspectiveProjection = new PerspectiveProjection();
         pp.projectionCenter = localToGlobal(new Point(this.paneHolderImg.x + this.paneHolderImg.width / 2,this.paneHolderImg.y + this.paneHolderImg.height / 2));
         pp.focalLength = 1000;
         this.paneHolderImg.transform.perspectiveProjection = pp;
         this.paneHolderImg.rotationX = 90;
         this.paneHolderImg.z = this.paneHolderImg.height * 2;
         Tween.kill(this.overlay);
         Tween.to(this.overlay,{"y":0},30 * 0.6,"easeOutExpo");
         Tween.to(this.paneHolderImg,
            {
               "x":px,
               "y":py,
               "alpha":1,
               "rotationX":0,
               "z":0,
               "easeParams":[0,30 * 0.98],
               "delay":5
            },30,"easeOutElastic",function():void
         {
            if(contains(paneHolderImg))
            {
               removeChild(paneHolderImg);
            }
            paneHolderImg.bitmapData.dispose();
            paneHolderImg = null;
            paneHolder.visible = true;
            dispatchEvent(new Event(Event.OPEN,true,true));
         });
      }
      
      public function close(param1:Event = null) : void {
         var e:Event = param1;
         removeEventListener(Event.ENTER_FRAME,this.open);
         dispatchEvent(new Event(Pane.INIT_CLOSE));
         if(!stage || !this._wasOpened)
         {
            dispatchEvent(new Event(Event.CLOSE));
            return;
         }
         if(this.paneHolderImg)
         {
            Tween.kill(this.paneHolderImg);
            removeChild(this.paneHolderImg);
            this.paneHolderImg.bitmapData.dispose();
            this.paneHolderImg = null;
         }
         addChild(this.paneHolderImg = this.getPaneHolderAsBitmap());
         Tween.kill(this.overlay);
         Tween.to(this.overlay,{"alpha":0},30 * 0.3);
         Tween.to(this.paneHolderImg,{"alpha":0},30 * 0.3,"easeNone",function():void
         {
            if(contains(paneHolderImg))
            {
               removeChild(paneHolderImg);
            }
            paneHolderImg.bitmapData.dispose();
            paneHolderImg = null;
            paneHolder.visible = true;
            dispatchEvent(new Event(Event.CLOSE));
         });
      }
      
      public function get bgWidth() : Number {
         return this._bgWidth;
      }
      
      public function set bgWidth(param1:Number) : void {
         this._bgWidth = Math.max(48,param1);
         this.drawBg();
      }
      
      public function get bgHeight() : Number {
         return this._bgHeight;
      }
      
      public function set bgHeight(param1:Number) : void {
         this._bgHeight = Math.max(48,param1);
         this.drawBg();
      }
      
      private function drawBg() : void {
         this.bg.graphics.clear();
         this.bg.graphics.beginFill(4473924);
         this.bg.graphics.drawRoundRect(0,0,this._bgWidth,this._bgHeight,20);
         this.bg.graphics.endFill();
         var _loc1_:BitmapData = new BitmapData(2,2,true,2.434539036E9);
         _loc1_.setPixel32(0,0,2.433749264E9);
         _loc1_.setPixel32(1,1,2.433749264E9);
         this.bg.graphics.beginBitmapFill(_loc1_,new Matrix(1 / this.paneHolder.scaleX,0,0,1 / this.paneHolder.scaleY));
         this.bg.graphics.drawRoundRect(0,0,this._bgWidth,this._bgHeight,20);
         this.bg.graphics.endFill();
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(this._bgWidth,this._bgHeight,90 * Math.PI / 180);
         this.bg.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.11,0.11],[0,255],_loc2_);
         this.bg.graphics.drawRoundRect(0,0,this._bgWidth,this._bgHeight,20);
         this.bg.graphics.endFill();
      }
      
      public function get title() : String {
         return this.titleLabel?this.titleLabel.text:"";
      }
      
      public function set title(param1:String) : void {
         var _loc2_:TextFormat = null;
         if(!param1)
         {
            return;
         }
         if(!this.titleLabel)
         {
            _loc2_ = new TextFormat(ViewerUI.baseFontName,16,16777215,true);
            _loc2_.leading = 4;
            this.header.addChild(this.titleLabel = new Label());
            this.titleLabel.textFormat = _loc2_;
            this.titleLabel.multiline = true;
            this.titleLabel.wordWrap = true;
            this.titleLabel.textFilters = [new DropShadowFilter(1,90,0,0.7,1,1,1,2)];
            this.titleLabel.embedFonts = ViewerUI.baseFontEmbedded;
            this.titleLabel.overSample = !ViewerUI.baseFontEmbedded;
         }
         if(this._notificationType)
         {
            this.titleLabel.multiline = false;
            this.titleLabel.wordWrap = false;
         }
         this.titleLabel.text = param1;
         this.resize();
      }
      
      public function get notificationType() : String {
         return this._notificationType;
      }
      
      public function set notificationType(param1:String) : void {
         if(param1 == this._notificationType)
         {
            return;
         }
         if(this.icon)
         {
            if(this.icon is LoaderBar)
            {
               LoaderBar(this.icon).destroy();
            }
            this.header.removeChild(this.icon);
            this.icon = null;
         }
         this._notificationType = param1;
         switch(param1)
         {
            case ViewerUI.NOTIFICATION_ALERT:
               this.header.addChild(this.icon = new this.AlertIcon());
               break;
            case ViewerUI.NOTIFICATION_SUCCESS:
               this.header.addChild(this.icon = new this.SuccessIcon());
               break;
            case ViewerUI.NOTIFICATION_LOAD:
               this.header.addChild(this.icon = new LoaderBar(12,2));
               this.icon.alpha = 1;
               this.autoRefreshBitmap = true;
               break;
            default:
               this._notificationType = null;
         }
         if(this.icon)
         {
            this.icon.filters = [new DropShadowFilter(1,90,0,0.32,1,1,1,2),new DropShadowFilter(1,90,16777215,0.34,1,1,1,2,true)];
         }
         this.resize();
      }
      
      public function get submitBtn() : Button {
         return this._submitBtn;
      }
      
      public function set submitBtn(param1:Button) : void {
         if(this._submitBtn)
         {
            if(contains(this._submitBtn))
            {
               this.footer.removeChild(this._submitBtn);
            }
            this._submitBtn.destroy();
            this._submitBtn = null;
         }
         this._submitBtn = param1;
         if(this._submitBtn)
         {
            this._submitBtn.noGlow = true;
         }
         if(this._submitBtn)
         {
            this.footer.addChild(this._submitBtn);
         }
         this.resize();
      }
      
      private function resizeHeader(param1:Number = 48) : void {
         this.header.graphics.clear();
         this.header.graphics.beginFill(16777215,0);
         this.header.graphics.drawRect(0,0,this._bgWidth,param1);
         this.header.graphics.endFill();
         if(this.content.height > 0)
         {
            this.header.graphics.beginFill(0,0.2);
            this.header.graphics.drawRect(0,param1 - 2,this._bgWidth,1);
            this.header.graphics.endFill();
            this.header.graphics.beginFill(16777215,0.05);
            this.header.graphics.drawRect(0,param1 - 1,this._bgWidth,1);
            this.header.graphics.endFill();
         }
         this.header.scrollRect = new Rectangle(0,0,this._bgWidth,param1);
      }
      
      private function resizeFooter(param1:Number = 50) : void {
         this.footer.graphics.clear();
         this.footer.graphics.beginFill(0,0.15);
         this.footer.graphics.drawRoundRectComplex(0,0,this._bgWidth,param1,0,0,10,10);
         this.footer.graphics.endFill();
         this.footer.graphics.beginFill(16777215,0.06);
         this.footer.graphics.drawRect(0,0,this._bgWidth,1);
         this.footer.graphics.endFill();
      }
      
      protected function get minHeaderWidth() : Number {
         return (this.icon?10 + this.icon.width + 8:12) + this.titleLabel.width + (this.exitBtn?42 + 12:12);
      }
      
      protected function get headerHeight() : Number {
         return this.titleLabel.numLines > 1?this.titleLabel.height + 15 * 2:48;
      }
      
      protected function resetTitleLabel() : void {
         if(this.titleLabel.multiline)
         {
            this.titleLabel.multiline = false;
            this.titleLabel.wordWrap = false;
            this.titleLabel.text = this.titleLabel.text;
         }
      }
      
      public function resize(... rest) : void {
         var _loc5_:* = NaN;
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            this._height = rest[1];
         }
         var _loc2_:Number = this._height - ViewerUI.baseH;
         this.overlay.graphics.clear();
         this.overlay.graphics.beginFill(0,0.85);
         this.overlay.graphics.drawRect(0,0,this._width,this._height);
         this.overlay.graphics.endFill();
         if(this.icon)
         {
            this.icon.x = 10 + (this.icon is LoaderBar?12:0);
            this.icon.y = this.icon is LoaderBar?24:Math.floor((48 - this.icon.height) / 2);
         }
         if(this.titleLabel)
         {
            this.titleLabel.x = this.icon?10 + this.icon.width + 8:12;
            if(!this._notificationType)
            {
               this.titleLabel.textWidth = this._bgWidth - this.titleLabel.x - (this.exitBtn?42 + 12:12);
            }
            else
            {
               this.resetTitleLabel();
               _loc5_ = this.titleLabel.width + this.titleLabel.x + (this.exitBtn?42 + 12:12);
               if(_loc5_ > this._minBgWidth)
               {
                  this.titleLabel.multiline = true;
                  this.titleLabel.wordWrap = true;
                  this.titleLabel.text = this.titleLabel.text;
                  this.titleLabel.textWidth = this._minBgWidth - this.titleLabel.x - (this.exitBtn?42 + 12:12);
               }
               this.bgWidth = Math.min(this._minBgWidth,_loc5_);
            }
            if(this.titleLabel.numLines > 1)
            {
               this.titleLabel.y = 12 + 1;
               this.resizeHeader(this.titleLabel.height + 15 * 2);
            }
            else
            {
               this.titleLabel.y = Math.round((48 - this.titleLabel.height) / 2) + 1;
               this.resizeHeader();
            }
         }
         if(this.exitBtn)
         {
            this.exitBtn.x = this._bgWidth - 42 + Math.round((42 - this.exitBtn.width) / 2);
            this.exitBtn.y = Math.round((48 - this.exitBtn.height) / 2);
         }
         var _loc3_:Number = this.content.scrollRect?this.content.scrollRect.height:this.content.height;
         this.content.y = this.header.height;
         this.footer.y = this.header.height + _loc3_;
         if(this._submitBtn)
         {
            this._submitBtn.x = this._bgWidth - this._submitBtn.width - 11;
            this._submitBtn.y = 11;
            this.resizeFooter();
         }
         this.bgHeight = this.header.height + _loc3_ + this.footer.height;
         var _loc4_:Number = 1;
         if(this._bgWidth > this._width - 24 || this._bgHeight > _loc2_ - 24)
         {
            _loc4_ = Math.min((this._width - 24) / this._bgWidth,(_loc2_ - 24) / this._bgHeight);
         }
         this.paneHolder.scaleX = this.paneHolder.scaleY = _loc4_;
         this.paneHolder.x = Math.round((this._width - this._bgWidth * _loc4_) / 2);
         this.paneHolder.y = Math.round((_loc2_ - this._bgHeight * _loc4_) / 2);
         graphics.clear();
         graphics.beginFill(16737792,0);
         graphics.drawRect(0,0,this._width,this._height);
         graphics.endFill();
      }
      
      protected function drawInputBg(param1:*, param2:Number, param3:Number, param4:uint = 2236962, param5:int = 5) : void {
         if(!(param1 is Sprite || param1 is Shape))
         {
            return;
         }
         param1.graphics.clear();
         param1.graphics.beginFill(param4);
         param1.graphics.drawRoundRect(0,0,param2,param3,param5 * 2);
         param1.graphics.endFill();
         if(param1.filters.length == 0)
         {
            param1.filters = [new DropShadowFilter(1,90,16777215,0.1,1,1,1,2),new DropShadowFilter(1,90,0,0.36,3,3,1,2,true),new GlowFilter(0,0.05,1,1,2,2,true)];
         }
      }
      
      public function destroy() : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         removeEventListener(Event.ENTER_FRAME,this.open);
         removeEventListener(Event.ENTER_FRAME,this.refreshBitmap);
         if(this.exitBtn)
         {
            this.exitBtn.removeEventListener(MouseEvent.CLICK,this.close);
         }
         if(this.submitBtn)
         {
            this.submitBtn.destroy();
         }
         if((this.icon) && this.icon is LoaderBar)
         {
            LoaderBar(this.icon).destroy();
         }
         if(this.paneHolderImg)
         {
            this.paneHolderImg.bitmapData.dispose();
            Tween.kill(this.paneHolderImg);
         }
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      protected function updateStyle(... rest) : void {
      }
   }
}
