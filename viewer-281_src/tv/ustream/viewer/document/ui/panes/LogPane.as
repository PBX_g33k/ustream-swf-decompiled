package tv.ustream.viewer.document.ui.panes
{
   import tv.ustream.viewer.document.ui.LargeButton;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import flash.system.System;
   
   public class LogPane extends Pane
   {
      
      public function LogPane(param1:uint, param2:String = null) {
         this._state = param1;
         this._hash = param2;
         super(param1 > 0);
      }
      
      public static const STATE_SENDING:uint = 0;
      
      public static const STATE_SENT:uint = 1;
      
      public static const STATE_FAILED:uint = 2;
      
      private var _state:uint;
      
      private var _hash:String;
      
      override protected function buildUI() : void {
         var _loc1_:LargeButton = null;
         super.buildUI();
         switch(this._state)
         {
            case 0:
               notificationType = ViewerUI.NOTIFICATION_LOAD;
               title = "Sending logs";
               break;
            case 1:
               notificationType = ViewerUI.NOTIFICATION_SUCCESS;
               title = "Logs sent";
               _loc1_ = new LargeButton();
               _loc1_.addEventListener(MouseEvent.CLICK,this.onCopyClick);
               _loc1_.text = "Copy log ID";
               submitBtn = _loc1_;
               break;
            case 2:
               notificationType = ViewerUI.NOTIFICATION_ALERT;
               title = "Error while sending logs";
               break;
         }
         resize();
      }
      
      private function onCopyClick(param1:MouseEvent) : void {
         if(this._hash)
         {
            System.setClipboard(this._hash);
         }
      }
   }
}
