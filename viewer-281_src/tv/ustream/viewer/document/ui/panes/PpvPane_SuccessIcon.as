package tv.ustream.viewer.document.ui.panes
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class PpvPane_SuccessIcon extends SpriteAsset
   {
      
      public function PpvPane_SuccessIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
