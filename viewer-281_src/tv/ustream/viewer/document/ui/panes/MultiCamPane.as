package tv.ustream.viewer.document.ui.panes
{
   import tv.ustream.viewer.logic.modules.MultiCam;
   import flash.display.Sprite;
   import tv.ustream.viewer.document.ui.Scrollbar;
   import tv.ustream.net.Connection;
   import flash.net.Responder;
   import flash.utils.Timer;
   import tv.ustream.viewer.document.ui.Button;
   import tv.ustream.viewer.document.ui.icons.MultiCamIcon;
   import flash.geom.Rectangle;
   import tv.ustream.localization.Locale;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.MouseEvent;
   import tv.ustream.tools.This;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.viewer.document.ViewerUI;
   import tv.ustream.modules.Module;
   import flash.events.TimerEvent;
   import flash.events.NetStatusEvent;
   import tv.ustream.tools.Debug;
   import flash.net.URLVariables;
   
   public class MultiCamPane extends Pane
   {
      
      public function MultiCamPane(param1:MultiCam) {
         super(true);
         priorityId = 6;
         autoRefreshBitmap = true;
         persist = true;
         this.camList = [];
         this._module = param1;
         this._module.addEventListener(Module.UPDATE,this.onModuleUpdate);
         this.gwUrl = "http://cdngw.ustream.tv/";
         this.gwUrl = this.gwUrl + "Viewer.getStream".replace(new RegExp("\\.","g"),"/");
         this.gwUrl = this.gwUrl + ("/" + Logic.instance.media.brandId + "/" + Logic.instance.media.mediaId + ".amf?");
         var _loc2_:URLVariables = new URLVariables();
         _loc2_.encoded = 1;
         this.gwUrl = this.gwUrl + _loc2_.toString();
         this.gw = new Connection("CGW-C");
         this.gw.addEventListener("callFailed",this.onGwCallFailed);
         this.gw.addEventListener("netStatus",this.onGwNetStatus);
         this.gwResponder = new Responder(this.onGwResult,this.onGwStatus);
         this.gw.connect(this.gwUrl);
         this.gwTimer = new Timer(20000,1);
         this.gwTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onGwTimer);
         this.onModuleUpdate();
      }
      
      private static const PADDING:Number = 16;
      
      private static const COLGAP:Number = 16;
      
      private static const ROWGAP:Number = 20;
      
      private static const MIN_COL:Number = 2;
      
      private static const MAX_COL:Number = 3;
      
      private var _module:MultiCam;
      
      private var camList:Array;
      
      private var list:Sprite;
      
      private var scrollBar:Scrollbar;
      
      private var colNum:Number = 1;
      
      private var rowNum:Number = 1;
      
      private var gw:Connection;
      
      private var gwResponder:Responder;
      
      private var gwTimer:Timer;
      
      private var gwUrl:String;
      
      override protected function buildUI() : void {
         super.buildUI();
         header.addChild(icon = new Button());
         Button(icon).icon = new MultiCamIcon();
         Button(icon)._hitArea = new Rectangle(0,0,25,25);
         Button(icon).enabled = false;
         title = Locale.instance.label(ViewerLabels.labelMultiCamPaneTitle);
         content.addChild(this.list = new Sprite());
      }
      
      private function onModuleUpdate(... rest) : void {
         var _loc2_:* = 0;
         var _loc3_:ChannelRenderer = null;
         var _loc4_:Array = null;
         if((rest.length) && rest[0] is Array)
         {
            this.echo("info from gw");
            _loc4_ = [].concat(rest[0] as Array);
         }
         else
         {
            this.echo("info from ums");
            _loc4_ = [].concat(this._module.channels);
         }
         _loc2_ = 0;
         while(_loc2_ < _loc4_.length)
         {
            if(String(_loc4_[_loc2_].cid) == Logic.instance.channel.mediaId)
            {
               _loc4_.splice(_loc2_,1);
               break;
            }
            _loc2_++;
         }
         if((this.camList.length) && (_loc4_.length))
         {
            while(this.camList.length > _loc4_.length)
            {
               _loc3_ = this.camList.pop() as ChannelRenderer;
               if(this.list.contains(_loc3_))
               {
                  this.list.removeChild(_loc3_);
               }
               _loc3_.removeEventListener(MouseEvent.CLICK,this.onCamClick);
               _loc3_.destroy();
            }
         }
         _loc2_ = 0;
         while(_loc2_ < _loc4_.length)
         {
            if(!this.camList[_loc2_])
            {
               _loc3_ = new ChannelRenderer();
               this.list.addChild(_loc3_);
               this.camList[_loc2_] = _loc3_;
               _loc3_.addEventListener(MouseEvent.CLICK,this.onCamClick);
            }
            else
            {
               _loc3_ = this.camList[_loc2_];
            }
            _loc3_.title = _loc4_[_loc2_].title;
            _loc3_.cid = String(_loc4_[_loc2_].cid);
            _loc3_.thumbUrl = This.secure?_loc4_[_loc2_].thumbnailSecureUrl:_loc4_[_loc2_].thumbnailUrl;
            _loc3_.status = _loc3_.cid == "13663180"?"LIVE":_loc4_[_loc2_].status;
            _loc2_++;
         }
         this.resize();
         this.gwTimer.reset();
         this.gwTimer.start();
      }
      
      private function onCamClick(param1:Event) : void {
         var _loc2_:String = ChannelRenderer(param1.currentTarget).cid;
         if(_loc2_ != Logic.instance.channel.mediaId)
         {
            dispatchEvent(new DynamicEvent(ViewerUI.CHANNEL_SWITCH,true,false,{"cid":_loc2_}));
         }
         else
         {
            close();
         }
      }
      
      override public function destroy() : void {
         var _loc2_:ChannelRenderer = null;
         super.destroy();
         this._module.removeEventListener(Module.UPDATE,this.onModuleUpdate);
         if(this.scrollBar)
         {
            this.scrollBar.destroy();
            content.removeChild(this.scrollBar);
            this.scrollBar = null;
         }
         var _loc1_:* = 0;
         while(_loc1_ < this.camList.length)
         {
            _loc2_ = this.camList[_loc1_];
            _loc2_.removeEventListener(MouseEvent.CLICK,this.onCamClick);
            _loc2_.destroy();
            _loc1_++;
         }
         this.gw.removeEventListener("callFailed",this.onGwCallFailed);
         this.gw.removeEventListener("netStatus",this.onGwNetStatus);
         this.gw.kill();
         this.gwTimer.reset();
         this.gwTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onGwTimer);
      }
      
      override public function resize(... rest) : void {
         var _loc2_:* = 0;
         var _loc6_:* = NaN;
         var _loc7_:* = NaN;
         if(!this._module)
         {
            return;
         }
         if(!isNaN(rest[0]))
         {
            _width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            _height = rest[1];
         }
         resetTitleLabel();
         if(this.camList.length == 1)
         {
            this.colNum = 1;
         }
         else if(this.camList.length == 4)
         {
            this.colNum = 2;
         }
         else
         {
            this.colNum = Math.min(MAX_COL,Math.max(MIN_COL,this.camList.length));
         }
         
         var _loc3_:Number = Math.max(minHeaderWidth,this.colNum * ChannelRenderer.THUMB_WIDTH + (this.colNum - 1) * COLGAP + PADDING * 2);
         while(_loc3_ > _width - PADDING * 2 - 10 && this.colNum > MIN_COL)
         {
            _loc3_ = _loc3_ - (ChannelRenderer.THUMB_WIDTH + COLGAP);
            this.colNum = this.colNum - 1;
            if(_loc3_ < minHeaderWidth)
            {
               _loc3_ = minHeaderWidth;
               break;
            }
         }
         var _loc4_:Number = 0;
         _loc2_ = 0;
         while(_loc2_ < this.camList.length)
         {
            _loc4_ = Math.max(_loc4_,this.camList[_loc2_].height);
            _loc2_++;
         }
         _loc2_ = 0;
         while(_loc2_ < this.camList.length)
         {
            _loc6_ = Math.floor(_loc2_ / this.colNum);
            _loc7_ = _loc2_ - _loc6_ * this.colNum;
            this.camList[_loc2_].x = PADDING + ChannelRenderer.THUMB_WIDTH * _loc7_ + COLGAP * _loc7_;
            this.camList[_loc2_].y = PADDING + _loc4_ * _loc6_ + ROWGAP * _loc6_;
            _loc2_++;
         }
         var _loc5_:Number = PADDING + ROWGAP + headerHeight;
         this.rowNum = Math.ceil(this.camList.length / this.colNum);
         _loc5_ = _loc5_ + (this.rowNum * _loc4_ + (this.rowNum - 1) * ROWGAP);
         this.list.graphics.clear();
         this.list.graphics.beginFill(16737792,0);
         this.list.graphics.drawRect(0,0,_loc3_,_loc5_ - headerHeight);
         this.list.graphics.endFill();
         while(_loc5_ > _height - (PADDING + ROWGAP) - ViewerUI.baseH)
         {
            _loc5_ = _loc5_ - (_loc4_ + ROWGAP);
         }
         _loc5_ = Math.max(headerHeight + PADDING + ROWGAP + _loc4_,_loc5_);
         if(this.list.y < _loc5_ - headerHeight - this.list.height)
         {
            this.list.y = _loc5_ - headerHeight - this.list.height;
         }
         if(this.list.height > _loc5_ - headerHeight)
         {
            if(!this.scrollBar)
            {
               this.scrollBar = new Scrollbar();
               this.scrollBar.target = this.list;
               content.addChild(this.scrollBar);
            }
            _loc3_ = _loc3_ + (this.scrollBar.width - 5);
            this.scrollBar.height = _loc5_ - headerHeight;
            this.scrollBar.x = _loc3_ - this.scrollBar.width;
            this.scrollBar.updateScroll();
         }
         else if(this.scrollBar)
         {
            this.scrollBar.destroy();
            content.removeChild(this.scrollBar);
            this.scrollBar = null;
         }
         
         _bgWidth = _loc3_;
         content.scrollRect = new Rectangle(0,0,_loc3_,_loc5_ - headerHeight);
         super.resize.apply(this,rest);
      }
      
      protected function onGwTimer(... rest) : void {
         this.echo("onGwTimer");
         var _loc2_:Object = 
            {
               "brandId":Logic.instance.media.brandId,
               "channelId":Logic.instance.media.mediaId,
               "channelCode":Logic.instance.media.mediaId
            };
         this.gw.connect(this.gwUrl);
         this.gw.call("Viewer.getStream",this.gwResponder,_loc2_);
      }
      
      private function onGwCallFailed(param1:Event = null) : void {
         this.echo("onGwCallFailed");
         this.gwTimer.reset();
         this.gwTimer.start();
      }
      
      private function onGwNetStatus(param1:NetStatusEvent) : void {
         if(param1.info.code == "NetConnection.Call.Failed")
         {
            this.onGwCallFailed();
         }
      }
      
      private function onGwResult(param1:Object) : void {
         this.echo("onGwResult");
         if((param1) && (param1.moduleConfig) && (param1.moduleConfig.multiCam))
         {
            Debug.explore(param1.moduleConfig.multiCam);
            this.onModuleUpdate(param1.moduleConfig.multiCam);
         }
      }
      
      private function onGwStatus(param1:Object) : void {
         this.echo("onGwStatus");
         Debug.explore(param1);
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ MultiCamPane ] " + param1);
      }
   }
}
