package tv.ustream.viewer.document.ui.panes
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import tv.ustream.viewer.document.ui.LargeButton;
   import tv.ustream.localization.Locale;
   import flash.filters.DropShadowFilter;
   import flash.text.TextFieldType;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.geom.Rectangle;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.KeyboardEvent;
   import tv.ustream.tools.DynamicEvent;
   
   public class PasswordPane extends Pane
   {
      
      public function PasswordPane() {
         super(false);
         priorityId = 1;
         addEventListener(Event.OPEN,this.setFocus);
      }
      
      private var passwordHolder:Sprite;
      
      private var password:TextField;
      
      private var submit:LargeButton;
      
      override protected function buildUI() : void {
         super.buildUI();
         title = Locale.instance.label(ViewerLabels.labelPasswordTitle);
         content.addChild(this.submit = new LargeButton());
         this.submit.enabled = false;
         this.submit.text = Locale.instance.label(ViewerLabels.labelPasswordSend);
         this.submit.filters = [new DropShadowFilter(1,90,0,0.2,2,2,1,2)];
         content.addChild(this.passwordHolder = new Sprite());
         this.passwordHolder.addChild(this.password = new TextField());
         this.password.selectable = true;
         this.password.type = TextFieldType.INPUT;
         this.password.displayAsPassword = true;
         this.password.defaultTextFormat = new TextFormat(ViewerUI.baseFontName,25,16777215);
         this.password.embedFonts = ViewerUI.baseFontEmbedded;
         this.password.text = "";
         this.password.y = 3;
         this.password.height = 28 - this.password.y;
         this.password.filters = [new DropShadowFilter(1,90,0,0.31,2,2,1,2)];
         this.submit.x = bgWidth - 11 - this.submit.width;
         this.submit.y = 11;
         this.passwordHolder.x = 11;
         this.passwordHolder.y = 11;
         drawInputBg(this.passwordHolder,bgWidth - 11 - this.submit.width - 11 - 11,28);
         this.password.x = 4;
         this.password.width = this.passwordHolder.width - 2 * this.password.x;
         content.scrollRect = new Rectangle(0,0,bgWidth,50);
         this.password.addEventListener(Event.CHANGE,this.onPasswordChange);
         this.submit.addEventListener(MouseEvent.CLICK,this.onSubmit);
      }
      
      override public function get notificationType() : String {
         return "";
      }
      
      override public function set notificationType(param1:String) : void {
      }
      
      private function onSubmit(param1:MouseEvent = null) : void {
         if((this.password) && !(this.password.text == ""))
         {
            this.password.removeEventListener(KeyboardEvent.KEY_UP,this.onInputKeyUp);
            this.password.mouseEnabled = false;
            this.submit.enabled = false;
            dispatchEvent(new DynamicEvent("password",true,true,{"password":this.password.text}));
            close();
         }
      }
      
      private function onInputKeyUp(param1:KeyboardEvent) : void {
         if(param1.charCode == 13)
         {
            this.onSubmit();
         }
      }
      
      private function onPasswordChange(param1:Event) : void {
         this.submit.enabled = (this.password) && (this.password.text)?true:false;
      }
      
      private function setFocus(param1:Event = null) : void {
         if(stage)
         {
            stage.focus = this.password;
         }
         this.password.text = "";
         this.password.mouseEnabled = true;
         this.password.addEventListener(KeyboardEvent.KEY_UP,this.onInputKeyUp);
      }
      
      override public function destroy() : void {
         super.destroy();
         removeEventListener(Event.OPEN,this.setFocus);
         this.password.removeEventListener(KeyboardEvent.KEY_UP,this.onInputKeyUp);
         this.password.removeEventListener(Event.CHANGE,this.onPasswordChange);
         this.submit.removeEventListener(MouseEvent.CLICK,this.onSubmit);
         this.submit.destroy();
      }
   }
}
