package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.DisplayObject;
   import flash.text.TextFormat;
   import flash.events.Event;
   import tv.ustream.tween2.Tween;
   import flash.filters.DropShadowFilter;
   
   public class StatLabel extends Sprite
   {
      
      public function StatLabel(param1:DisplayObject = null, param2:String = null) {
         super();
         this.tf = new TextFormat("MyriadCondBoldItalic",13,10066329);
         this.icon = param1;
         this.text = param2 || "";
      }
      
      protected var _icon:DisplayObject;
      
      protected var _text:String;
      
      protected var _height:Number = 0;
      
      protected var txt:Label;
      
      protected var tf:TextFormat;
      
      public function get icon() : DisplayObject {
         return this._icon;
      }
      
      public function set icon(param1:DisplayObject) : void {
         if(!param1)
         {
            return;
         }
         this._icon = param1;
         this._icon.addEventListener(Event.ADDED_TO_STAGE,this.onIconAdded);
         if(this.txt)
         {
            this.txt.x = this._icon.width + 2;
         }
      }
      
      private function onIconAdded(param1:Event) : void {
         this._icon.alpha = 0;
         this._icon.removeEventListener(Event.ADDED_TO_STAGE,this.onIconAdded);
         Tween.to(this._icon,{"alpha":1},30 * 0.6,"easeOutQuart");
      }
      
      public function get text() : String {
         return this._text;
      }
      
      public function set text(param1:String) : void {
         this._text = param1;
         if(!this._text)
         {
            return;
         }
         if(!this.txt)
         {
            addChild(this.txt = new Label());
            this.txt.textFormat = this.tf;
            this.txt.embedFonts = true;
            if(this._icon)
            {
               this.txt.x = this._icon.width + 5;
            }
            addChild(this._icon);
            this.txt.filters = [new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
         }
         this.txt.text = this._text;
         this.height = this._height;
      }
      
      override public function get width() : Number {
         return Math.ceil(super.width);
      }
      
      override public function get height() : Number {
         return this._height;
      }
      
      override public function set height(param1:Number) : void {
         this._height = param1;
         if(this._icon)
         {
            this._icon.y = Math.floor((this._height - this.icon.height) / 2);
         }
         if(this.txt)
         {
            this.txt.y = Math.ceil((this._height - this.txt.height) / 2);
         }
      }
      
      public function destroy() : void {
         if(this._icon)
         {
            this._icon.removeEventListener(Event.ADDED_TO_STAGE,this.onIconAdded);
         }
      }
   }
}
