package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.utils.Timer;
   import flash.text.StyleSheet;
   import flash.text.TextFormat;
   import flash.geom.Matrix;
   import flash.events.Event;
   import flash.display.GradientType;
   import tv.ustream.viewer.document.ViewerUI;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.filters.DropShadowFilter;
   
   public class ScrollingLabel extends Sprite
   {
      
      public function ScrollingLabel(param1:TextFormat = null, param2:StyleSheet = null, param3:Boolean = false, param4:Boolean = true, param5:Boolean = false) {
         super();
         addChild(this.labelHolder = new Sprite());
         this.labelHolder.addChild(this.label = new Label());
         this.label2 = new Label();
         if(param5)
         {
            this.label.forcedHtmlText = true;
            this.label2.forcedHtmlText = true;
         }
         if(param2)
         {
            this.label.styleSheet = this.label2.styleSheet = param2;
         }
         else
         {
            this.label.textFormat = this.label2.textFormat = param1 || new TextFormat(ViewerUI.baseFontName,13,16777215,true);
         }
         this.label.embedFonts = this.label2.embedFonts = this.embedFonts;
         if(!param3)
         {
            this.label.overSample = this.label2.overSample = this.overSample;
            if(param4)
            {
               this.label.textGradientOverlay = this.label2.textGradientOverlay = [16777215,0];
               this.label.textGradientOverlayAlpha = this.label2.textGradientOverlayAlpha = [0.33,0.33];
            }
         }
         else
         {
            this.label.overSample = this.label2.overSample = false;
            this.label.interactive = this.label2.interactive = true;
         }
         this.labelHolder.filters = [new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
         addChild(this.msk = new Shape());
         this.labelHolder.cacheAsBitmap = this.msk.cacheAsBitmap = true;
         this.labelHolder.mask = this.msk;
         addEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         if(ViewerUI.touchMode)
         {
            addEventListener(MouseEvent.CLICK,this.onOver);
            this.idleTimer = new Timer(4000,1);
            this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onOut);
         }
         else
         {
            addEventListener(MouseEvent.MOUSE_OVER,this.onOver);
            addEventListener(MouseEvent.MOUSE_OUT,this.onOut);
         }
      }
      
      private var label:Label;
      
      private var label2:Label;
      
      private var labelHolder:Sprite;
      
      private var _text:String;
      
      private var msk:Shape;
      
      private var _maskWidth:Number;
      
      private var _lastLabelWidth:Number;
      
      private var isOver:Boolean;
      
      public var locked:Boolean;
      
      public var autoCollapse:Boolean;
      
      private var idleTimer:Timer;
      
      public function get text() : String {
         return this._text;
      }
      
      public function set text(param1:String) : void {
         if(!param1 || this._text == param1)
         {
            return;
         }
         this._lastLabelWidth = this.label.width;
         this.label.text = this.label2.text = this._text = param1;
         this.label2.x = this.label.x + this.label.width + 16;
         this.label.y = this.label2.y = Math.floor((32 - this.label.height) / 2);
         if(isNaN(this._maskWidth))
         {
            this.maskWidth = this.label.width;
         }
      }
      
      public function get embedFonts() : Boolean {
         return this.label.embedFonts;
      }
      
      public function set embedFonts(param1:Boolean) : void {
         this.label.embedFonts = this.label2.embedFonts = param1;
      }
      
      public function get overSample() : Boolean {
         return this.label.overSample;
      }
      
      public function set overSample(param1:Boolean) : void {
         if(this.label.interactive)
         {
            this.label.overSample = this.label2.overSample = false;
         }
         else
         {
            this.label.overSample = this.label2.overSample = param1;
         }
      }
      
      public function get styleSheet() : StyleSheet {
         return this.label.styleSheet;
      }
      
      public function set styleSheet(param1:StyleSheet) : void {
         this.label.styleSheet = this.label2.styleSheet = param1;
      }
      
      public function get textFormat() : TextFormat {
         return this.label.textFormat;
      }
      
      public function set textFormat(param1:TextFormat) : void {
         this.label.textFormat = this.label2.textFormat = param1;
      }
      
      public function reset() : void {
         this.label.x = 0;
         this.label2.x = this.label.x + this.label.width + 16;
      }
      
      override public function get width() : Number {
         return Math.min(this.label.width,this._maskWidth) * scaleX;
      }
      
      public function get textWidth() : Number {
         return this.label.width;
      }
      
      public function get maskWidth() : Number {
         return this._maskWidth;
      }
      
      public function set maskWidth(param1:Number) : void {
         var _loc2_:Matrix = null;
         if(this.label.width == this._lastLabelWidth && this._maskWidth == param1)
         {
            return;
         }
         this._maskWidth = param1;
         if(this._maskWidth >= this.label.width)
         {
            if(hasEventListener(Event.ENTER_FRAME))
            {
               removeEventListener(Event.ENTER_FRAME,this.scroll);
            }
            if(this.labelHolder.contains(this.label2))
            {
               this.labelHolder.removeChild(this.label2);
            }
            this.label.x = 0;
            this.msk.graphics.clear();
            this.msk.graphics.beginFill(16737792);
            this.msk.graphics.drawRect(0,0,this.label.width,32);
            this.msk.graphics.endFill();
         }
         else if(!this.autoCollapse || (this.autoCollapse) && this._maskWidth > 24)
         {
            if(!this.labelHolder.contains(this.label2))
            {
               this.labelHolder.addChild(this.label2);
            }
            if(!hasEventListener(Event.ENTER_FRAME))
            {
               addEventListener(Event.ENTER_FRAME,this.scroll);
            }
            this.msk.graphics.clear();
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(16,32,0,-8);
            this.msk.graphics.beginGradientFill(GradientType.LINEAR,[0,0],[0,1],[0,255],_loc2_);
            this.msk.graphics.drawRect(-8,0,16,32);
            this.msk.graphics.endFill();
            this.msk.graphics.beginFill(16737792,1);
            this.msk.graphics.drawRect(8,0,this._maskWidth - 16 - 8,32);
            this.msk.graphics.endFill();
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(16,32,0,this._maskWidth - 16);
            this.msk.graphics.beginGradientFill(GradientType.LINEAR,[0,0],[1,0],[0,255],_loc2_);
            this.msk.graphics.drawRect(this._maskWidth - 16,0,16,32);
            this.msk.graphics.endFill();
            this.scroll(null);
         }
         else
         {
            if(hasEventListener(Event.ENTER_FRAME))
            {
               removeEventListener(Event.ENTER_FRAME,this.scroll);
            }
            if(this.labelHolder.contains(this.label2))
            {
               this.labelHolder.removeChild(this.label2);
            }
            this.label.x = 0;
            this.msk.graphics.clear();
            this._maskWidth = 0;
         }
         
      }
      
      public function get textGradientOverlay() : Array {
         return this.label.textGradientOverlay;
      }
      
      public function set textGradientOverlay(param1:Array) : void {
         this.label.textGradientOverlay = this.label2.textGradientOverlay = param1;
      }
      
      public function get textGradientOverlayAlpha() : Array {
         return this.label.textGradientOverlayAlpha;
      }
      
      public function set textGradientOverlayAlpha(param1:Array) : void {
         this.label.textGradientOverlayAlpha = this.label2.textGradientOverlayAlpha = param1;
      }
      
      private function onOver(param1:Event) : void {
         if(ViewerUI.touchMode)
         {
            if((param1) && param1.type == MouseEvent.CLICK)
            {
               this.isOver = !this.isOver;
            }
            this.idleTimer.reset();
            if(this.isOver)
            {
               this.idleTimer.start();
            }
         }
         else
         {
            this.isOver = true;
         }
      }
      
      private function onOut(param1:Event) : void {
         this.isOver = false;
      }
      
      private function scroll(param1:Event) : void {
         if((this.isOver) || (this.locked))
         {
            return;
         }
         this.label.x = --this.label.x % (this.label.width + 16);
         this.label2.x = this.label.x + this.label.width + 16;
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         removeEventListener(Event.ENTER_FRAME,this.scroll);
      }
      
      public function destroy(... rest) : void {
         removeEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         removeEventListener(Event.ENTER_FRAME,this.scroll);
         if(ViewerUI.touchMode)
         {
            removeEventListener(MouseEvent.CLICK,this.onOver);
            this.idleTimer.reset();
            this.idleTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onOut);
         }
         else
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.onOver);
            removeEventListener(MouseEvent.MOUSE_OUT,this.onOut);
         }
      }
   }
}
