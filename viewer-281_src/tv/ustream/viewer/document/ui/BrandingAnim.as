package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.utils.Timer;
   import flash.geom.Matrix;
   import flash.events.Event;
   import flash.geom.Point;
   import flash.display.GradientType;
   import tv.ustream.tween2.Tween;
   import flash.filters.BlurFilter;
   import flash.events.TimerEvent;
   
   public class BrandingAnim extends Sprite
   {
      
      public function BrandingAnim() {
         this.Logo = BrandingAnim_Logo;
         this.colors = [3377407,45752,3355537,3377407,10168063,3355537,3377407,45752,3355537,3377407,10168063,3355537];
         this.colorsLength = this.colors.length;
         this.mtx = new Matrix();
         super();
         addChild(this.logo = new this.Logo());
         this.logo.cacheAsBitmap = true;
         this.logo.filters = [new BlurFilter(1.1,1.1,3)];
         addChild(this.waveHolder = new Shape());
         this.waveHolder.cacheAsBitmap = true;
         this.waveHolder.mask = this.logo;
         addChild(this.loaderBar = new LoaderBar(9,2));
         this.loaderTimer = new Timer(4000,1);
         this.loaderTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onLoaderTimer);
         this.ratios = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.colorsLength)
         {
            this.ratios.push(255 / this.colorsLength * _loc1_);
            _loc1_++;
         }
         this.reset();
      }
      
      private var Logo:Class;
      
      private var logo:Sprite;
      
      private var waves:Array;
      
      private var waveHolder:Shape;
      
      private const WAVE_NUM:int = 7;
      
      private var _running:Boolean;
      
      private var fillOffset:Number = 0;
      
      private var loaderBar:LoaderBar;
      
      private var loaderTimer:Timer;
      
      private var colors:Array;
      
      private var colorsLength:int;
      
      private var ratios:Array;
      
      private var alphas:Array;
      
      private var mtx:Matrix;
      
      private function drawWaves(param1:Event = null) : void {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         var _loc4_:Object = null;
         var _loc5_:Point = null;
         var _loc6_:Point = null;
         var _loc7_:Point = null;
         var _loc8_:* = NaN;
         this.fillOffset = this.fillOffset + 4;
         if(this.fillOffset > 960)
         {
            this.fillOffset = 0;
         }
         this.alphas = new Array();
         _loc2_ = 0;
         while(_loc2_ < this.colorsLength)
         {
            this.alphas.push(0.25);
            _loc2_++;
         }
         this.mtx.createGradientBox(1920,1920,0,-80 - this.fillOffset);
         this.waveHolder.graphics.clear();
         this.waveHolder.graphics.beginGradientFill(GradientType.LINEAR,this.colors,this.alphas,this.ratios,this.mtx);
         this.waveHolder.graphics.drawRect(-80,-16,160,32);
         this.waveHolder.graphics.endFill();
         _loc2_ = 0;
         while(_loc2_ < this.WAVE_NUM)
         {
            _loc4_ = this.waves[_loc2_];
            _loc5_ = new Point(_loc4_.x,_loc4_.y);
            _loc6_ = new Point(_loc4_.x,_loc4_.y);
            _loc7_ = new Point(_loc4_.x,_loc4_.y);
            _loc8_ = _loc4_.width / 5 * 2;
            this.waveHolder.graphics.beginFill(0,0.3);
            this.waveHolder.graphics.moveTo(_loc4_.x - _loc4_.width / 2,_loc4_.y);
            _loc5_ = new Point(_loc4_.x,_loc4_.y);
            _loc5_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90) * Math.PI / 180) * _loc4_.innerThickness;
            _loc5_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90) * Math.PI / 180) * _loc4_.innerThickness;
            _loc6_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90 + 270) * Math.PI / 180) * _loc8_;
            _loc6_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90 + 270) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc6_.x,_loc6_.y,_loc5_.x,_loc5_.y);
            _loc7_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90 + 90) * Math.PI / 180) * _loc8_;
            _loc7_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90 + 90) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc7_.x,_loc7_.y,_loc4_.x + _loc4_.width / 2,_loc4_.y);
            _loc5_ = new Point(_loc4_.x,_loc4_.y);
            _loc5_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90) * Math.PI / 180) * _loc4_.innerThickness;
            _loc5_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90) * Math.PI / 180) * _loc4_.innerThickness;
            _loc6_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90 + 270) * Math.PI / 180) * _loc8_;
            _loc6_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90 + 270) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc6_.x,_loc6_.y,_loc5_.x,_loc5_.y);
            _loc7_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90 + 90) * Math.PI / 180) * _loc8_;
            _loc7_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90 + 90) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc7_.x,_loc7_.y,_loc4_.x - _loc4_.width / 2,_loc4_.y);
            this.waveHolder.graphics.endFill();
            this.alphas = new Array();
            _loc3_ = 0;
            while(_loc3_ < this.colorsLength)
            {
               this.alphas.push(_loc4_.alpha);
               _loc3_++;
            }
            this.mtx.createGradientBox(1920,1920,0,-80 - this.fillOffset - _loc4_.fillOffset);
            this.waveHolder.graphics.beginGradientFill(GradientType.LINEAR,this.colors,this.alphas,this.ratios,this.mtx);
            this.waveHolder.graphics.moveTo(_loc4_.x - _loc4_.width / 2,_loc4_.y);
            _loc5_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90) * Math.PI / 180) * _loc4_.thickness / 2;
            _loc5_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90) * Math.PI / 180) * _loc4_.thickness / 2;
            _loc6_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90 + 270) * Math.PI / 180) * _loc8_;
            _loc6_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90 + 270) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc6_.x,_loc6_.y,_loc5_.x,_loc5_.y);
            _loc7_.x = _loc5_.x + Math.cos((_loc4_.rotation - 90 + 90) * Math.PI / 180) * _loc8_;
            _loc7_.y = _loc5_.y + Math.sin((_loc4_.rotation - 90 + 90) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc7_.x,_loc7_.y,_loc4_.x + _loc4_.width / 2,_loc4_.y);
            _loc5_ = new Point(_loc4_.x,_loc4_.y);
            _loc5_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90) * Math.PI / 180) * _loc4_.thickness / 2;
            _loc5_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90) * Math.PI / 180) * _loc4_.thickness / 2;
            _loc6_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90 + 270) * Math.PI / 180) * _loc8_;
            _loc6_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90 + 270) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc6_.x,_loc6_.y,_loc5_.x,_loc5_.y);
            _loc7_.x = _loc5_.x + Math.cos((_loc4_.rotation + 90 + 90) * Math.PI / 180) * _loc8_;
            _loc7_.y = _loc5_.y + Math.sin((_loc4_.rotation + 90 + 90) * Math.PI / 180) * _loc8_;
            this.waveHolder.graphics.curveTo(_loc7_.x,_loc7_.y,_loc4_.x - _loc4_.width / 2,_loc4_.y);
            this.waveHolder.graphics.endFill();
            _loc2_++;
         }
      }
      
      private function animateWave(param1:Object) : void {
         var wave:Object = param1;
         Tween.to(wave,{"rotation":-wave.defaultRotation},30 * 2,"easeInOutSine",function(param1:Object):void
         {
            var wave:Object = param1;
            Tween.to(wave,{"rotation":wave.defaultRotation},30 * 2,"easeInOutSine",function(param1:Object):void
            {
               animateWave(param1);
            },[wave]);
         },[wave]);
      }
      
      private function onLoaderTimer(param1:Event) : void {
         this.showLoader();
      }
      
      private function reset() : void {
         var _loc1_:* = 0;
         removeEventListener(Event.ENTER_FRAME,this.drawWaves);
         this.waveHolder.visible = false;
         this.logo.visible = false;
         this.loaderBar.visible = false;
         this.loaderBar.alpha = 0;
         alpha = 0;
         this.showLoader(false);
         if(this.waves)
         {
            _loc1_ = 0;
            while(_loc1_ < this.WAVE_NUM)
            {
               Tween.kill(this.waves[_loc1_]);
               _loc1_++;
            }
         }
         this.loaderTimer.reset();
         this.fillOffset = 0;
         this._running = false;
      }
      
      public function start() : void {
         var _loc2_:* = NaN;
         this.reset();
         this.waves = new Array();
         var _loc1_:* = 0;
         while(_loc1_ < this.WAVE_NUM)
         {
            _loc2_ = 10 + Math.round(Math.random() * 20);
            this.waves.push(
               {
                  "x":Math.round(Math.random() * 60) - 30,
                  "y":Math.round(Math.random() * 40) - 20,
                  "width":240,
                  "thickness":30,
                  "innerThickness":2 + Math.round(Math.random() * 4),
                  "rotation":_loc2_,
                  "fillOffset":Math.random() * 120,
                  "defaultRotation":_loc2_,
                  "alpha":0.1 + Math.random() * 0.1
               });
            _loc1_++;
         }
         this.drawWaves();
         this.waveHolder.visible = true;
         this.logo.visible = true;
         this.loaderBar.visible = true;
         Tween.to(this,{"alpha":1},30 * 0.4);
         _loc1_ = 0;
         while(_loc1_ < this.WAVE_NUM)
         {
            this.animateWave(this.waves[_loc1_]);
            _loc1_++;
         }
         addEventListener(Event.ENTER_FRAME,this.drawWaves);
         this.loaderTimer.start();
         this._running = true;
      }
      
      public function stop() : void {
         Tween.kill(this);
         Tween.to(this,{"alpha":0},30 * 0.2,"easeOutQuart",function():void
         {
            reset();
         });
      }
      
      public function get running() : Boolean {
         return this._running;
      }
      
      public function resize(param1:Number, param2:Number) : void {
         this.loaderBar.y = 16 + (param2 / 2 / scaleY - 16) * (1 - 0.618);
         this.loaderBar.scaleX = 1 / scaleX;
         this.loaderBar.scaleY = 1 / scaleY;
      }
      
      public function showLoader(param1:Boolean = true) : void {
         if(param1)
         {
            Tween.to(this.loaderBar,{"alpha":0.8},30 * 0.4);
         }
         else
         {
            Tween.to(this.loaderBar,{"alpha":0},30 * 0.4);
         }
      }
      
      public function startLoaderTimer() : void {
         this.loaderTimer.reset();
         this.loaderTimer.start();
      }
   }
}
