package tv.ustream.viewer.document.ui
{
   import flash.utils.Timer;
   import flash.geom.Rectangle;
   import flash.events.Event;
   import flash.geom.Matrix;
   import flash.display.Shape;
   import flash.display.GradientType;
   import flash.events.TimerEvent;
   
   public class ToolTip extends Label
   {
      
      public function ToolTip() {
         this._labelBounds = new Rectangle();
         super();
         mouseEnabled = false;
         mouseChildren = false;
         addEventListener(Event.ADDED_TO_STAGE,this.onAddedTostage);
      }
      
      private static var toolTips:Array = new Array();
      
      private static function addToolTip(param1:ToolTip) : void {
         if(getIndex(param1) < 0)
         {
            toolTips.push(param1);
         }
      }
      
      private static function removeToolTip(param1:ToolTip) : void {
         var _loc2_:int = getIndex(param1);
         if(_loc2_ >= 0)
         {
            toolTips.splice(_loc2_,1);
         }
      }
      
      private static function getIndex(param1:ToolTip) : int {
         var _loc2_:int = toolTips.length;
         var _loc3_:* = 0;
         while(_loc3_ < _loc2_)
         {
            if(toolTips[_loc3_] == param1)
            {
               return _loc3_;
            }
            _loc3_++;
         }
         return -1;
      }
      
      private static function isOverlap(param1:ToolTip, param2:ToolTip) : Boolean {
         if((param1.stage && param1.getBounds(param1.stage).intersects(param2.getBounds(param1.stage)) && param1.visible && param1.alpha >= 1) && (param2.visible) && param2.alpha >= 1)
         {
            return true;
         }
         return false;
      }
      
      public static function getOverlaps(param1:ToolTip) : Array {
         var _loc2_:* = 0;
         var _loc3_:int = toolTips.length;
         var _loc4_:Array = new Array();
         _loc2_ = 0;
         while(_loc2_ < _loc3_)
         {
            if(toolTips[_loc2_] != param1)
            {
               if(isOverlap(param1,toolTips[_loc2_]))
               {
                  _loc4_.push(toolTips[_loc2_]);
               }
            }
            _loc2_++;
         }
         return _loc4_;
      }
      
      private var _xOffset:Number = 0;
      
      private var _xLabelOffset:Number = 0;
      
      private var reposTimer:Timer;
      
      private var _checkOverlap:Boolean;
      
      private var _labelBounds:Rectangle;
      
      private function onAddedTostage(param1:Event) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedTostage);
         addEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         if(this.reposTimer)
         {
            this.reposTimer.start();
         }
         ToolTip.addToolTip(this);
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         removeEventListener(Event.REMOVED_FROM_STAGE,this.onRemovedFromStage);
         addEventListener(Event.ADDED_TO_STAGE,this.onAddedTostage);
         if(this.reposTimer)
         {
            this.reposTimer.stop();
         }
         ToolTip.removeToolTip(this);
      }
      
      override protected function renderBg(param1:Number, param2:Number) : void {
         var _loc3_:* = NaN;
         var _loc4_:* = NaN;
         var _loc5_:Array = null;
         var _loc6_:Matrix = null;
         var _loc7_:* = 0;
         if(!bg)
         {
            addChildAt(bg = new Shape(),0);
         }
         if((bg) && (contains(bg)))
         {
            if(isNaN(param1))
            {
               param1 = 0;
            }
            if(isNaN(param2))
            {
               param2 = 0;
            }
            bg.graphics.clear();
            bg.graphics.beginFill(bgColor,bgAlpha);
            bg.graphics.drawRoundRect(-Math.round(param1 / 2) + this._xOffset + this._xLabelOffset,-6 - param2,param1,param2,bgCornerRadius * 2);
            _loc3_ = -Math.round(param1 / 2) + this._xOffset + 9;
            _loc4_ = _loc3_ + param1 - 18;
            bg.graphics.moveTo(Math.max(_loc3_,Math.min(-7,_loc4_ - 14)),-6);
            bg.graphics.lineTo(Math.max(_loc3_ + 14,Math.min(7,_loc4_)),-6);
            bg.graphics.lineTo(Math.max(_loc3_,Math.min(0,_loc4_)),0);
            bg.graphics.endFill();
            if((bgGradientOverlay) && bgGradientOverlay.length > 0)
            {
               if(!bgGradientOverlayAlpha || bgGradientOverlayAlpha.length == 0)
               {
                  bgGradientOverlayAlpha = [];
                  _loc7_ = 0;
                  while(_loc7_ < bgGradientOverlay.length)
                  {
                     bgGradientOverlayAlpha.push(0.33);
                     _loc7_++;
                  }
               }
               _loc5_ = [];
               _loc7_ = 0;
               while(_loc7_ < bgGradientOverlay.length)
               {
                  _loc5_.push(255 / (bgGradientOverlay.length - 1) * _loc7_);
                  _loc7_++;
               }
               _loc6_ = new Matrix();
               _loc6_.createGradientBox(param1,param2 + 6,90 * Math.PI / 180,-Math.round(param1 / 2) + this._xOffset,-6 - param2);
               bg.graphics.beginGradientFill(GradientType.LINEAR,bgGradientOverlay,bgGradientOverlayAlpha,_loc5_,_loc6_);
               bg.graphics.drawRoundRect(-Math.round(param1 / 2) + this._xOffset + this._xLabelOffset,-6 - param2,param1,param2,bgCornerRadius * 2);
               bg.graphics.moveTo(Math.max(_loc3_,Math.min(-7,_loc4_ - 14)),-6);
               bg.graphics.lineTo(Math.max(_loc3_ + 14,Math.min(7,_loc4_)),-6);
               bg.graphics.lineTo(Math.max(_loc3_,Math.min(0,_loc4_)),0);
               bg.graphics.endFill();
            }
            bg.filters = bgFilters || [];
            if(stage)
            {
               this._labelBounds = bg.getBounds(stage);
            }
         }
      }
      
      override protected function render() : void {
         super.render();
         txtTarget.x = -Math.round(txtTarget.width / 2) + this._xOffset - ((padding) && (embedFonts)?1:0) + this._xLabelOffset;
         txtTarget.y = -6 - bgHeight + Math.round((bgHeight - txtTarget.height) / 2);
         if(embedFonts)
         {
            txtTarget.y = txtTarget.y + 1;
         }
      }
      
      public function get xOffset() : Number {
         return this._xOffset;
      }
      
      public function set xOffset(param1:Number) : void {
         this._xOffset = param1;
         if(txtTarget)
         {
            this.render();
         }
      }
      
      public function get xLabelOffset() : Number {
         return this._xLabelOffset;
      }
      
      public function set xLabelOffset(param1:Number) : void {
         this._xLabelOffset = param1;
         if(txtTarget)
         {
            this.render();
         }
      }
      
      public function set checkOverlap(param1:Boolean) : void {
         this._checkOverlap = param1;
         if(this._checkOverlap)
         {
            this.reposTimer = new Timer(100);
            this.reposTimer.addEventListener(TimerEvent.TIMER,this.onCheckOverlap);
            this.reposTimer.start();
         }
         else
         {
            this.reposTimer.stop();
            this.reposTimer.removeEventListener(TimerEvent.TIMER,this.onCheckOverlap);
            this.reposTimer = null;
         }
      }
      
      public function get labelBounds() : Rectangle {
         return this._labelBounds;
      }
      
      protected function onCheckOverlap(param1:TimerEvent) : void {
         var _loc3_:Rectangle = null;
         if(!stage)
         {
            return;
         }
         var _loc2_:Array = ToolTip.getOverlaps(this);
         if(_loc2_.length == 1)
         {
            _loc3_ = this.labelBounds.intersection(ToolTip(_loc2_[0]).labelBounds);
            this._xLabelOffset = ToolTip(_loc2_[0]).labelBounds.x >= this.labelBounds.x?-8 - _loc3_.width:_loc3_.width + 8;
         }
         else if(_loc2_.length <= 1)
         {
            if(!(this._xLabelOffset == 0) && ToolTip.toolTips.length == 1)
            {
               this._xLabelOffset = 0;
            }
         }
         
         this.render();
      }
   }
}
