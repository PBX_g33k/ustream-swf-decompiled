package tv.ustream.viewer.document.ui
{
   import mx.core.SpriteAsset;
   import flash.display.DisplayObject;
   
   public class KeyboardNotification_MuteIcon extends SpriteAsset
   {
      
      public function KeyboardNotification_MuteIcon() {
         super();
      }
      
      public var overlay:DisplayObject;
      
      public var bg:DisplayObject;
      
      public var icon:DisplayObject;
   }
}
