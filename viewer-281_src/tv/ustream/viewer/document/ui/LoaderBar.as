package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.events.Event;
   import flash.geom.Point;
   import tv.ustream.viewer.document.ViewerUI;
   
   public class LoaderBar extends Sprite
   {
      
      public function LoaderBar(param1:Number = 18, param2:Number = 3) {
         super();
         this.rad = param1;
         this.lw = param2;
         mouseEnabled = false;
         mouseChildren = false;
         addChild(this.ring = new Sprite());
         this.ring.addChild(this.ringFg = new Shape());
         this.ringFg.graphics.lineStyle(this.lw,7829367,0.45);
         this.ringFg.graphics.drawCircle(0,0,this.rad - this.lw / 2);
         this.ringFg.graphics.endFill();
         this.ring.addChild(this.node = new Shape());
         this.node.graphics.lineStyle(this.lw,ViewerUI.style.color,1);
         this.drawArc(this.node,0,0,0,60 * Math.PI / 180,this.rad - this.lw / 2,1);
         this.drawArc(this.node,0,0,180 * Math.PI / 180,(180 + 60) * Math.PI / 180,this.rad - this.lw / 2,1);
         this.node.graphics.endFill();
         addEventListener(Event.ENTER_FRAME,this.roll);
         this.alpha = 0;
         ViewerUI.instance.addEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      public var ring:Sprite;
      
      private var ringBg:Shape;
      
      private var ringFg:Shape;
      
      private var node:Shape;
      
      private var rad:Number;
      
      private var lw:Number;
      
      override public function get width() : Number {
         return this.rad * 2;
      }
      
      override public function get height() : Number {
         return this.rad * 2;
      }
      
      private function roll(param1:Event) : void {
         if(!visible || alpha < 0.04)
         {
            return;
         }
         this.node.rotation = this.node.rotation + 15;
      }
      
      override public function set alpha(param1:Number) : void {
         this.ring.visible = param1 < 0.04?false:true;
         super.alpha = param1;
      }
      
      private function drawArc(param1:*, param2:Number, param3:Number, param4:Number, param5:Number, param6:Number, param7:Number = 1) : void {
         var _loc13_:Point = null;
         var _loc14_:Point = null;
         var _loc8_:Number = Math.abs(param5 - param4);
         var _loc9_:Number = Math.floor(_loc8_ / (Math.PI / 4)) + 1;
         var _loc10_:Number = param7 * _loc8_ / (2 * _loc9_);
         var _loc11_:Number = param6 / Math.cos(_loc10_);
         param1.graphics.moveTo(param2 + Math.cos(param4) * param6,param3 + Math.sin(param4) * param6);
         var _loc12_:Number = 0;
         while(_loc12_ < _loc9_)
         {
            param5 = param4 + _loc10_;
            param4 = param5 + _loc10_;
            _loc13_ = new Point(param2 + Math.cos(param5) * _loc11_,param3 + Math.sin(param5) * _loc11_);
            _loc14_ = new Point(param2 + Math.cos(param4) * param6,param3 + Math.sin(param4) * param6);
            param1.graphics.curveTo(_loc13_.x,_loc13_.y,_loc14_.x,_loc14_.y);
            _loc12_++;
         }
      }
      
      public function destroy() : void {
         removeEventListener(Event.ENTER_FRAME,this.roll);
         ViewerUI.instance.removeEventListener(ViewerUI.STYLE_CHANGE,this.updateStyle);
      }
      
      private function updateStyle(... rest) : void {
         this.node.graphics.clear();
         this.node.graphics.lineStyle(this.lw,ViewerUI.style.color,1);
         this.drawArc(this.node,0,0,0,60 * Math.PI / 180,this.rad - this.lw / 2,1);
         this.drawArc(this.node,0,0,180 * Math.PI / 180,(180 + 60) * Math.PI / 180,this.rad - this.lw / 2,1);
         this.node.graphics.endFill();
      }
   }
}
