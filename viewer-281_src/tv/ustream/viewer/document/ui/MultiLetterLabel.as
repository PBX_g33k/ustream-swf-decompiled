package tv.ustream.viewer.document.ui
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.text.AntiAliasType;
   import flash.text.TextFieldAutoSize;
   
   public class MultiLetterLabel extends Sprite
   {
      
      public function MultiLetterLabel() {
         super();
      }
      
      public var letters:Array;
      
      private var controlText:TextField;
      
      private var _text:String;
      
      private var _ticker:Boolean;
      
      private var _textFormat:TextFormat;
      
      private var _embedFonts:Boolean;
      
      private var _overSample:Boolean;
      
      private var _textGradientOverlay:Array;
      
      private var _textGradientOverlayAlpha:Array;
      
      public function get text() : String {
         return this._text;
      }
      
      public function set text(param1:String) : void {
         var _loc5_:Label = null;
         if(this._text == param1)
         {
            return;
         }
         this._text = param1;
         if(!this.letters)
         {
            this.letters = new Array();
         }
         if(!this.controlText)
         {
            this.controlText = new TextField();
            this.controlText.selectable = false;
            this.controlText.antiAliasType = AntiAliasType.ADVANCED;
            this.controlText.autoSize = TextFieldAutoSize.LEFT;
            this.controlText.mouseEnabled = false;
            this.controlText.defaultTextFormat = this._textFormat;
            this.controlText.embedFonts = this._embedFonts;
         }
         this.controlText.text = this._text;
         this.controlText.height = this.controlText.height;
         while(this._text.length < this.letters.length)
         {
            removeChild(this.letters[this.letters.length - 1]);
            this.letters.splice(this.letters.length - 1,1);
         }
         var _loc2_:Number = this.controlText.getCharBoundaries(0).x;
         var _loc3_:Number = 0;
         var _loc4_:* = 0;
         while(_loc4_ < this._text.length)
         {
            if(this.letters[_loc4_])
            {
               _loc5_ = this.letters[_loc4_];
            }
            else
            {
               _loc5_ = this.letters[_loc4_] = new Label();
               _loc5_.ticker = this._ticker;
               _loc5_.textFormat = this._textFormat;
               _loc5_.embedFonts = this._embedFonts;
               _loc5_.overSample = this._overSample;
               _loc5_.textGradientOverlay = this._textGradientOverlay;
               _loc5_.textGradientOverlayAlpha = this._textGradientOverlayAlpha;
               addChild(_loc5_);
            }
            _loc5_.text = this._text.substr(_loc4_,1);
            if(this.controlText.getCharBoundaries(_loc4_))
            {
               _loc3_ = this.controlText.getCharBoundaries(_loc4_).x;
            }
            _loc5_.x = Math.floor(_loc3_ - _loc2_);
            _loc4_++;
         }
      }
      
      public function get ticker() : Boolean {
         return this._ticker;
      }
      
      public function set ticker(param1:Boolean) : void {
         this._ticker = param1;
         this.setLabelStyles();
      }
      
      public function get textFormat() : TextFormat {
         return this._textFormat;
      }
      
      public function set textFormat(param1:TextFormat) : void {
         this._textFormat = param1;
         this.setLabelStyles();
      }
      
      public function get embedFonts() : Boolean {
         return this._embedFonts;
      }
      
      public function set embedFonts(param1:Boolean) : void {
         this._embedFonts = param1;
         this.setLabelStyles();
      }
      
      public function get overSample() : Boolean {
         return this._overSample;
      }
      
      public function set overSample(param1:Boolean) : void {
         this._overSample = param1;
         this.setLabelStyles();
      }
      
      public function get textGradientOverlay() : Array {
         return this._textGradientOverlay;
      }
      
      public function set textGradientOverlay(param1:Array) : void {
         this._textGradientOverlay = param1;
         this.setLabelStyles();
      }
      
      public function get textGradientOverlayAlpha() : Array {
         return this._textGradientOverlayAlpha;
      }
      
      public function set textGradientOverlayAlpha(param1:Array) : void {
         this._textGradientOverlayAlpha = param1;
         this.setLabelStyles();
      }
      
      override public function get height() : Number {
         return (this.letters) && (this.letters[0])?this.letters[0].height:super.height;
      }
      
      private function setLabelStyles() : void {
         var _loc2_:Label = null;
         if(this.controlText)
         {
            this.controlText.defaultTextFormat = this._textFormat;
            this.controlText.embedFonts = this._embedFonts;
            this.controlText.text = this.controlText.text;
            this.controlText.height = this.controlText.height;
         }
         if(!this.letters || this.letters.length == 0)
         {
            return;
         }
         var _loc1_:* = 0;
         while(_loc1_ < this.letters.length)
         {
            _loc2_ = this.letters[_loc1_];
            _loc2_.ticker = this._ticker;
            _loc2_.textFormat = this._textFormat;
            _loc2_.embedFonts = this._embedFonts;
            _loc2_.overSample = this._overSample;
            _loc2_.textGradientOverlay = this._textGradientOverlay;
            _loc2_.textGradientOverlayAlpha = this._textGradientOverlayAlpha;
            _loc2_.text = _loc2_.text;
            _loc1_++;
         }
      }
   }
}
