package tv.ustream.viewer.document
{
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.Debug;
   import flash.external.ExternalInterface;
   import tv.ustream.tools.Shared;
   import flash.utils.Timer;
   import tv.ustream.viewer.logic.modules.Quality;
   import tv.ustream.modules.Module;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.viewer.logic.modules.Poll;
   import tv.ustream.viewer.document.managers.DvrManager;
   import tv.ustream.viewer.document.managers.LogManager;
   import flash.events.Event;
   import tv.ustream.localization.Locale;
   import flash.text.Font;
   import tv.ustream.tools.UncaughtErrorHandler;
   import tv.ustream.tools.This;
   import tv.ustream.tools.Shell;
   import tv.ustream.tools.KeyboardManager;
   import flash.ui.Keyboard;
   import flash.events.ContextMenuEvent;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.TimerEvent;
   import flash.system.Capabilities;
   import tv.ustream.viewer.document.ui.KeyboardNotification;
   import tv.ustream.viewer.document.tools.TimeOut;
   import flash.ui.ContextMenu;
   import flash.ui.ContextMenuItem;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.viewer.logic.modules.Logo;
   import tv.ustream.tween2.Tween;
   import tv.ustream.viewer.logic.media.SlideShow;
   import flash.geom.Rectangle;
   import tv.ustream.viewer.logic.modules.Dvr;
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.viewer.document.ui.ThumbnailLoader;
   import flash.events.MouseEvent;
   import tv.ustream.viewer.document.ui.panes.Pane;
   import flash.display.DisplayObject;
   import tv.ustream.viewer.logic.modules.Viewers;
   import tv.ustream.viewer.logic.modules.AllViewers;
   import tv.ustream.viewer.logic.modules.AllTimeTotal;
   import tv.ustream.viewer.logic.modules.Stream;
   import tv.ustream.gui2.Alert;
   import tv.ustream.viewer.logic.modules.Share;
   import tv.ustream.viewer.logic.modules.MultiCam;
   import flash.display.StageDisplayState;
   import tv.ustream.viewer.logic.modules.Ppv;
   import tv.ustream.viewer.logic.modules.Meta;
   
   public class Viewer extends Sprite
   {
      
      public function Viewer() {
         this.qualityNames = 
            {
               "low":"Low",
               "medium":"Med",
               "high":"High",
               "hd":"Best",
               "auto":"Auto"
            };
         this.mediaEventList = 
            {
               "play":this.onTogglePlaying,
               "pause":this.onTogglePlaying,
               "createStream":this.onCreateStream,
               "getStreamSize":this.onGetStreamSize,
               "audio":this.onAudio,
               "video":this.onVideo,
               "rejected":this.onChannelRejected,
               "ioError":this.onHideBar,
               "peerAssistedNetworkingRequired":this.onHideBar,
               "p2pOldPlayer":this.onHideBar,
               "password":this.onPassword,
               "meta":this.onMeta,
               "adsVideoAdStarted":this.onVideoAdStarted,
               "adsVideoAdFinished":this.onVideoAdFinished,
               "adsMidrollNotification":this.onMidrollNotification,
               "adsBlockStatus":this.onAdBlockStatus,
               "adsPrerollDone":this.onPrerollDone,
               "moduleLock":this.onModuleLock,
               "bufferFull":this.onBufferFull,
               "bufferEmpty":this.onBufferEmpty,
               "forceAutoplay":this.onForceAutoplay
            };
         this.forceKeepPanes = [];
         this.startedRecordeds = [];
         this.testChatChannels = ["5970606","12761226","10348458","7650561","679575","9161875","9408562","12034538","7262640","661529","2819050","2846797","10314057","13159568","11347693"];
         this.seamlessSwitchDestroyList = new Array();
         super();
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
      }
      
      public static function gaTrack(param1:String, param2:String, param3:String = null, param4:int = 0) : void {
         var category:String = param1;
         var action:String = param2;
         var label:String = param3;
         var value:int = param4;
         if((Logic.hasInstance) && (Logic.instance.media) && (Logic.instance.media.disabledGA))
         {
            return;
         }
         Debug.echo("gaTrack: " + category + ", " + action + (label?", " + label:"") + (value?", " + value + " (type: " + "number" + ")":""));
         if(ExternalInterface.available)
         {
            try
            {
               ExternalInterface.call("ustream.track",category,action,label,value);
            }
            catch(e:*)
            {
               Debug.echo("gaTrack error: " + e);
            }
         }
         else
         {
            Debug.echo("!ExternalInterface.available");
         }
      }
      
      private var viewerUI:ViewerUI;
      
      private var qualityNames:Object;
      
      private var mediaEventList:Object;
      
      private var volume:Number = 0.7;
      
      private var shared:Shared;
      
      private var status:String = "";
      
      private var params:Object;
      
      private var wasStreamResizeInFs:Boolean = false;
      
      private var isVideoAdPlaying:Boolean = false;
      
      private var _width:Number = 0;
      
      private var _height:Number = 0;
      
      private var playBeforeSeek:Boolean;
      
      private var _mobile:Boolean;
      
      private var adBlockArguments:Object;
      
      private var adBlockTimer:Timer;
      
      private var qualityManager:Quality;
      
      private var passwordMediaType:String;
      
      private var passwordMediaId:String = "";
      
      private var _logoPaddingTop:Number = 0;
      
      private var _logoPaddingBottom:Number = 0;
      
      private var inHighlight:Boolean;
      
      private var ageModule:Module;
      
      private var _sharedAgeLockAgeOk:Boolean;
      
      private var forceKeepPanes:Array;
      
      private var customRejectUrl:String;
      
      private var rpinAuthUrl:String;
      
      private var rpinAuthTimer:Timer;
      
      private var streamSizeReported:Boolean;
      
      private var allowBranding:Boolean;
      
      public var brandingAnimHolder:Sprite;
      
      private var logoModuleReady:Boolean;
      
      private var isSeeking:Boolean;
      
      private var startedRecordeds:Array;
      
      private var testChatChannels:Array;
      
      private var displayChatReported:Boolean;
      
      private var setChatPosition:Boolean;
      
      private var highlightStart:Number = 0;
      
      private var highlightEnd:Number = 1;
      
      private var deepLink:Number = -1;
      
      private var krCustomFontTracked:Boolean;
      
      private var activeChannelInstance:Channel;
      
      private var seamlessSwitchOldChannel:Channel;
      
      private var seamlessSwitchDestroyList:Array;
      
      private var delayedPoll:Poll;
      
      private var isPrerollDone:Boolean;
      
      private var autoplayStatus:int = 0;
      
      private var enableStageVideo:Boolean = true;
      
      private var qualityAutoSelected:Boolean = false;
      
      private var autoMbrSelected:Boolean = false;
      
      private var dvrManager:DvrManager;
      
      private var logManager:LogManager;
      
      private var cutter:Boolean = false;
      
      private var progressStorageTimer:Timer;
      
      private function onAddedToStage(... rest) : void {
         var _loc2_:* = false;
         var _loc3_:Array = null;
         var _loc4_:* = 0;
         var _loc5_:* = 0;
         var _loc6_:Array = null;
         var _loc7_:* = 0;
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         Locale.init("flash.viewer",loaderInfo?loaderInfo.parameters.locale:"");
         if(Locale.instance.language == "ko_KR")
         {
            _loc3_ = Font.enumerateFonts(true);
            _loc4_ = _loc3_.length;
            _loc5_ = 0;
            _loc6_ = ["Malgun Gothic","MalgunGothic","Nanum Gothic","NanumGothic"];
            _loc7_ = 0;
            _loc7_ = 0;
            while(_loc7_ < _loc6_.length)
            {
               _loc5_ = 0;
               while(_loc5_ < _loc4_)
               {
                  if(_loc3_[_loc5_].fontName.indexOf(_loc6_[_loc7_]) != -1)
                  {
                     this.log(_loc6_[_loc7_] + ": available as device font");
                     ViewerUI.baseFontName = _loc6_[_loc7_];
                     ViewerUI.baseFontEmbedded = false;
                     _loc2_ = true;
                     break;
                  }
                  _loc5_++;
               }
               if(_loc2_)
               {
                  break;
               }
               _loc7_++;
            }
            if(!_loc2_)
            {
               this.log("no custom font is available, use Arial instead");
            }
         }
         Locale.instance.addEventListener(Event.COMPLETE,this.init);
      }
      
      private function resetAutoPlay(... rest) : void {
         this.autoplayStatus = 0;
         ExternalInterface.call("console.log","resetautoplay: " + this.autoplayStatus);
         this.log("resetautoplay: " + this.autoplayStatus);
      }
      
      private function init(... rest) : void {
         var _loc4_:String = null;
         var _loc5_:uint = 0;
         var _loc6_:* = NaN;
         var _loc7_:* = NaN;
         Locale.instance.removeEventListener(Event.COMPLETE,this.init);
         UncaughtErrorHandler.instance.add(loaderInfo,this.toString());
         this.cutter = loaderInfo.parameters.cutter == "true";
         if(ViewerVersion)
         {
            this.log("viewer build info: " + ViewerVersion.BUILD_NUMBER + " (" + ViewerVersion.BUILD_USER + ", " + ViewerVersion.BUILD_DATE + ")");
         }
         if(This.pageUrl.split("#to")[1])
         {
            _loc4_ = This.pageUrl.split("#to")[1];
            Debug.echo("deepLink #to : " + _loc4_);
            if(_loc4_.indexOf(":") == -1)
            {
               _loc5_ = 0;
               while(_loc5_ < _loc4_.length)
               {
                  if(isNaN(Number(_loc4_.substr(_loc5_,1))))
                  {
                     _loc4_ = _loc4_.substr(0,_loc5_);
                     break;
                  }
                  _loc5_++;
               }
               this.deepLink = Number(_loc4_);
            }
            else
            {
               _loc6_ = Number(_loc4_.split(":")[0]);
               _loc7_ = Number(_loc4_.split(":")[1].substr(0,2));
               if(!isNaN(_loc6_) && !isNaN(_loc7_))
               {
                  this.deepLink = _loc6_ * 60 + _loc7_;
               }
            }
            Debug.echo("deepLink autoSeek : " + this.deepLink);
         }
         addChild(this.brandingAnimHolder = new Sprite());
         this.brandingAnimHolder.mouseEnabled = false;
         addChild(this.viewerUI = new ViewerUI());
         this.viewerUI.addEventListener(ViewerUI.VOLUME,this.onVolume);
         this.viewerUI.addEventListener(ViewerUI.TOGGLE_PLAYING,this.togglePlaying);
         this.viewerUI.addEventListener(ViewerUI.CONTROL_PLAY,this.onControlPlay);
         this.viewerUI.addEventListener(ViewerUI.CONTROL_OFFSET,this.onUIControlOffset);
         this.viewerUI.addEventListener(ViewerUI.LOGO_CLICK,this.onUILogoClick);
         this.viewerUI.addEventListener(ViewerUI.CC_CLICK,this.onCCClick);
         this.viewerUI.addEventListener(ViewerUI.TOP_PANEL_HEIGHT_CHANGE,this.onUITopPanelHeightChange);
         this.viewerUI.addEventListener(ViewerUI.TOGGLE_THEATER_MODE,this.onToggleTheaterMode);
         this.viewerUI.addEventListener(ViewerUI.POPOUT,this.onPopout);
         if(loaderInfo.parameters.controls != undefined)
         {
            this.viewerUI.chromeless = loaderInfo.parameters.controls == "false";
            Logic.keepLogoOverlay = this.viewerUI.chromeless;
         }
         if(loaderInfo.parameters.showtitle != undefined)
         {
            this.viewerUI.showtitle = !(loaderInfo.parameters.showtitle == "false");
         }
         if(loaderInfo.parameters.referrer)
         {
            This.referrer = loaderInfo.parameters.referrer;
         }
         if(loaderInfo.parameters.hidefeatures != undefined)
         {
            this.viewerUI.hideFeatures = !(loaderInfo.parameters.hidefeatures == "false");
         }
         if(loaderInfo.parameters.hideshare != undefined)
         {
            this.viewerUI.hideShare = !(loaderInfo.parameters.hideshare == "false");
         }
         if(loaderInfo.parameters.lite != undefined)
         {
            this.viewerUI.lite = loaderInfo.parameters.lite == "true";
         }
         var _loc2_:* = true;
         if(!(loaderInfo.parameters.autoplay == undefined) && !this.cutter)
         {
            _loc2_ = !(loaderInfo.parameters.autoplay == "false");
         }
         this.autoplayStatus = _loc2_?1:0;
         if(loaderInfo.parameters.style)
         {
            this.viewerUI.updateStyle(loaderInfo.parameters.style);
         }
         var _loc3_:* = "";
         if(This.onSite(ViewerUI.pageUrl) > 1)
         {
            _loc3_ = loaderInfo.parameters.password;
         }
         Shell.registerEntryPoint("viewer",this);
         Logic.instance.seamlessSwitch = This.pageUrl.split("#seamless=")[1] == "true";
         KeyboardManager.getInstance().addShortKeys(Keyboard.F,false,false,false,this.switchFullScreen);
         KeyboardManager.getInstance().addShortKeys(Keyboard.HOME,false,false,false,this.seekToPercent,"0");
         KeyboardManager.getInstance().addShortKeys(Keyboard.LEFT,false,false,false,this.seekLeft);
         KeyboardManager.getInstance().addShortKeys(Keyboard.RIGHT,false,false,false,this.seekRight);
         KeyboardManager.getInstance().addShortKeys(Keyboard.R,false,true,false,this.reloadStream);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_0,false,false,false,this.seekToPercent,"0");
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_1,false,false,false,this.seekToPercent,1);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_2,false,false,false,this.seekToPercent,2);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_3,false,false,false,this.seekToPercent,3);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_4,false,false,false,this.seekToPercent,4);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_5,false,false,false,this.seekToPercent,5);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_6,false,false,false,this.seekToPercent,6);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_7,false,false,false,this.seekToPercent,7);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_8,false,false,false,this.seekToPercent,8);
         KeyboardManager.getInstance().addShortKeys(Keyboard.NUMPAD_9,false,false,false,this.seekToPercent,9);
         KeyboardManager.getInstance().addShortKeys(Keyboard.SPACE,false,false,false,this.playingToggle);
         KeyboardManager.getInstance().addShortKeys(Keyboard.UP,false,false,false,this.setVolume,1);
         KeyboardManager.getInstance().addShortKeys(Keyboard.DOWN,false,false,false,this.setVolume,-1);
         Object(parent).logic = Logic.instance;
         if(loaderInfo.parameters.sessionid != undefined)
         {
            Logic.instance.sessionId = loaderInfo.parameters.sessionid;
         }
         if(loaderInfo.parameters.application != undefined)
         {
            Logic.instance.application = loaderInfo.parameters.application;
         }
         Logic.instance.addEventListener("createChannel",this.onCreateChannel);
         Logic.instance.addEventListener("createRecorded",this.onCreateRecorded);
         Logic.instance.addEventListener("createSlideShow",this.onCreateSlideShow);
         Logic.instance.addEventListener(ContextMenuEvent.MENU_SELECT,this.onOpenContextMenu);
         dispatchEvent(new DynamicEvent("createLogic",true,false,{"logic":Logic.instance}));
         addChildAt(Logic.instance.display,getChildIndex(this.viewerUI));
         this.shared = new Shared(
            {
               "volume":this.viewerUI.volume,
               "isMuted":false,
               "dvrCIDs":[]
            });
         Logic.instance.volume = this.viewerUI.volume = this.shared.isMuted?0:this.shared.volume;
         if(loaderInfo.parameters.volume)
         {
            Logic.instance.volume = this.viewerUI.volume = Number(loaderInfo.parameters.volume);
         }
         this.progressStorageTimer = new Timer(1000);
         this.progressStorageTimer.addEventListener(TimerEvent.TIMER,this.onProgressStorageTimer);
         this.resize();
         if(loaderInfo.parameters.campaign == "facebook" && Capabilities.playerType == "ActiveX" || (this.cutter))
         {
            this.log("stageVideo is disabled on Facebook in IE (wmode=opaque with stageVideo issues)(or cutter player)");
            this.enableStageVideo = false;
         }
         this.params = 
            {
               "viewerVersion":3,
               "enableStageVideo":this.enableStageVideo,
               "cutter":this.cutter
            };
         if(this.cutter)
         {
            this.params.ignoreCut = true;
         }
         if(loaderInfo.parameters.adfree)
         {
            this.params.adFree = Boolean(Number(loaderInfo.parameters.adfree == "1"));
         }
         if(loaderInfo.parameters.campaign)
         {
            this.params.campaignId = loaderInfo.parameters.campaign;
         }
         if(loaderInfo.parameters.hid)
         {
            this.params.highlightId = loaderInfo.parameters.hid;
         }
         if(loaderInfo.parameters.user)
         {
            this.params.user = loaderInfo.parameters.user;
         }
         if(loaderInfo.parameters.pass)
         {
            this.params.pass = loaderInfo.parameters.pass;
         }
         if(loaderInfo.parameters.userid)
         {
            this.params.userId = loaderInfo.parameters.userid;
         }
         if(loaderInfo.parameters.scid)
         {
            this.params.scid = loaderInfo.parameters.scid;
         }
         if(loaderInfo.parameters.hash)
         {
            this.params.hash = loaderInfo.parameters.hash;
         }
         if(loaderInfo.parameters.sessionSource)
         {
            this.params.sessionSource = loaderInfo.parameters.sessionSource;
         }
         if(loaderInfo.parameters.embedredirection)
         {
            this.params.embedredirection = true;
         }
         if(ViewerVersion)
         {
            this.params.viewerBuildInfo = ViewerVersion.BUILD_NUMBER + " (" + ViewerVersion.BUILD_USER + ", " + ViewerVersion.BUILD_DATE + ")";
         }
         if(loaderInfo.parameters.cid)
         {
            Logic.instance.createChannel(loaderInfo.parameters.cid,_loc2_,_loc3_,this.params);
         }
         else if(loaderInfo.parameters.vid)
         {
            Logic.instance.createRecorded(loaderInfo.parameters.vid,_loc2_,_loc3_,this.params);
         }
         
         if(Capabilities.version.substr(0,3) == "AND")
         {
            this.mobile = true;
         }
         if((Shell.hasInstance) && (Shell.jsApiEnabled))
         {
            Shell.instance.dispatch("viewer","ready");
            Shell.instance.addCallBack("updateViewerStyle",this.viewerUI.updateStyle);
            Shell.instance.addCallBack("setCutData",this.setCutData);
         }
      }
      
      private function switchFullScreen() : void {
         this.viewerUI.onToggleFullScreen();
      }
      
      private function expandView() : void {
         if(Logic.hasInstance)
         {
            this.theaterMode = !this.theaterMode;
         }
      }
      
      private function seekLeft() : void {
         if((Logic.hasInstance) && Logic.instance.media.type == "recorded" && Logic.instance.recorded.progress > 0)
         {
            Logic.instance.recorded.seek(Math.max(0,Logic.instance.recorded.progress - 10 / Logic.instance.recorded.duration));
         }
      }
      
      private function seekRight() : void {
         if((Logic.hasInstance) && Logic.instance.media.type == "recorded" && Logic.instance.recorded.progress < 1)
         {
            Logic.instance.recorded.seek(Math.min(1,Logic.instance.recorded.progress + 10 / Logic.instance.recorded.duration));
         }
      }
      
      private function seekToPercent(param1:Object) : void {
         if((Logic.hasInstance) && Logic.instance.media.type == "recorded")
         {
            Logic.instance.recorded.seek(Number(param1) / 10);
         }
      }
      
      private function playingToggle() : void {
         if(!this.viewerUI.startMode)
         {
            this.viewerUI.playKeyBoardNotification(Logic.instance.media.playing?KeyboardNotification.PAUSE:KeyboardNotification.PLAY);
         }
         this.togglePlaying();
      }
      
      private function setVolume(param1:Object) : void {
         this.viewerUI.volume = Math.min(1,Math.max(0,this.viewerUI.volume + 0.02 * Number(param1)));
      }
      
      private function onDisplayClick(param1:Event) : void {
         var _loc2_:TimeOut = TimeOut.retrieve("doubleClick");
         if(_loc2_ == null)
         {
            _loc2_ = new TimeOut(200,this.onDisplaySingleClick,"doubleClick");
            _loc2_.start();
         }
         else if(_loc2_.started)
         {
            _loc2_.reset();
            this.switchFullScreen();
         }
         else
         {
            _loc2_.reset();
            _loc2_.start();
         }
         
      }
      
      private function onDisplaySingleClick() : void {
         if((Logic.hasInstance) && (Logic.instance.display.visible) && ((Logic.instance.media.type == "recorded") || (Logic.instance.channel && Logic.instance.channel.modules.dvr)))
         {
            this.togglePlaying();
            this.viewerUI.playKeyBoardNotification(Logic.instance.media.playing?KeyboardNotification.PLAY:KeyboardNotification.PAUSE);
         }
      }
      
      private function createContextMenu(param1:ContextMenu) : void {
         var _loc2_:Array = null;
         var _loc3_:* = false;
         var _loc4_:Array = null;
         var _loc5_:ContextMenuItem = null;
         var _loc6_:ContextMenuItem = null;
         var _loc7_:ContextMenuItem = null;
         if(!param1)
         {
            this.log("createContextMenu - Error: contextMenu = " + param1);
            return;
         }
         if((Logic.hasInstance) && !(This.getReference(Logic.instance,"media.modules.meta.data.broadcaster.proPackage") == null))
         {
            _loc2_ = This.getReference(Logic.instance,"media.modules.meta.data.broadcaster.benefits")?This.getReference(Logic.instance,"media.modules.meta.data.broadcaster.benefits") as Array:[];
            _loc3_ = _loc2_.indexOf("disable_flash_menu_links") > -1;
            _loc4_ = ["12569870","12703625","12685211"];
            if(This.getReference(Logic.instance,"media.brandId") == "1" && !((Logic.instance.channel) && _loc4_.indexOf(Logic.instance.channel.mediaId) > -1 || _loc4_.indexOf(Logic.instance.media.channelId) > -1))
            {
               if(!Logic.instance || (Logic.instance && This.getReference(Logic.instance,"media.modules.meta.data.broadcaster.proPackage") as uint <= 3) && (!_loc3_))
               {
                  _loc5_ = new ContextMenuItem("Ustream");
                  _loc5_.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,this.onUstreamClick);
                  _loc6_ = new ContextMenuItem(Locale.instance.label(ViewerLabels.labelViewProfile),true);
                  _loc6_.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,this.onProfileClick);
                  param1.customItems.push(_loc5_);
                  param1.customItems.push(_loc6_);
               }
               if((Logic.hasInstance) && (This.getReference(Logic.instance,"recorded.hasKeyData")))
               {
                  _loc7_ = new ContextMenuItem(Locale.instance.label(ViewerLabels.labelProgressive));
                  _loc7_.enabled = false;
                  param1.customItems.push(_loc7_);
               }
            }
         }
      }
      
      private function onOpenContextMenu(param1:ContextMenuEvent) : void {
         this.createContextMenu(param1.contextMenuOwner.contextMenu);
         gaTrack("Player","Context menu","Open");
      }
      
      private function onUstreamClick(param1:ContextMenuEvent) : void {
         try
         {
            navigateToURL(new URLRequest("http://www.ustream.tv"));
            gaTrack("Player","Context menu","Click on Ustream");
         }
         catch(e:Error)
         {
         }
      }
      
      private function onProfileClick(param1:ContextMenuEvent) : void {
         var _loc2_:* = This.getReference(Logic.instance,"media.modules.meta.username");
         if((_loc2_) && !(_loc2_ == ""))
         {
            this.log("onProfileClick: " + _loc2_);
            navigateToURL(new URLRequest("http://ustream.tv/user/" + _loc2_),"_blank");
         }
         else if(This.getReference(Logic.instance,"media.mediaId"))
         {
            navigateToURL(new URLRequest("http://ustream.tv/user/" + Logic.instance.media.mediaId),"_blank");
         }
         
         gaTrack("Player","Context menu","Click on View Profile");
      }
      
      private function onUIControlOffset(param1:Event = null) : void {
         var _loc2_:Recorded = this.seamlessSwitchOldChannel || Logic.instance.media;
         var _loc3_:Logo = (_loc2_) && (_loc2_.modules) && (_loc2_.modules.logo)?_loc2_.modules.logo:null;
         this.log("controlOffset: " + this.viewerUI.controlOffset + ", media: " + _loc2_ + ", logoModule: " + _loc3_);
         if(_loc2_)
         {
            _loc2_.adOffset = this.viewerUI.controlOffset;
         }
         if((_loc3_) && (_loc3_.hasOwnProperty("padding")) || (This.getReference(Logic.instance,"slideShow.manager.logo.padding")))
         {
            Tween.to(this,{"logoPaddingBottom":this.viewerUI.controlOffset},30 * 0.5,"easeOutQuart");
         }
      }
      
      private function onUITopPanelHeightChange(param1:Event = null) : void {
         this.logoPaddingTop = Math.max(0,Math.min(this.viewerUI.topPanelHeight,this.viewerUI.topPanelHeight - this.viewerUI.streamBounds.y));
      }
      
      public function get logoPaddingBottom() : Number {
         return this._logoPaddingBottom;
      }
      
      public function set logoPaddingBottom(param1:Number) : void {
         if(param1 == this._logoPaddingBottom)
         {
            return;
         }
         this._logoPaddingBottom = param1;
         this.setLogoPadding();
      }
      
      public function get logoPaddingTop() : Number {
         return this._logoPaddingTop;
      }
      
      public function set logoPaddingTop(param1:Number) : void {
         if(param1 == this._logoPaddingTop)
         {
            return;
         }
         this._logoPaddingTop = param1;
         this.setLogoPadding();
      }
      
      private function setLogoPadding() : void {
         var _loc1_:Object = null;
         var _loc2_:Recorded = this.seamlessSwitchOldChannel || Logic.instance.media;
         var _loc3_:Logo = (_loc2_) && (_loc2_.modules) && (_loc2_.modules.logo)?_loc2_.modules.logo:null;
         if((_loc3_) && (_loc3_.hasOwnProperty("padding")))
         {
            _loc1_ = _loc3_.padding;
            _loc1_.top = this._logoPaddingTop;
            _loc1_.bottom = this._logoPaddingBottom;
            _loc3_.padding = _loc1_;
         }
         if(This.getReference(Logic.instance,"slideShow.manager.logo.padding"))
         {
            _loc1_ = Logic.instance.slideShow.manager.logo.padding;
            _loc1_.top = this._logoPaddingTop;
            _loc1_.bottom = this._logoPaddingBottom;
            Logic.instance.slideShow.manager.logo.padding = _loc1_;
         }
      }
      
      private function onHideBar(param1:Event = null) : void {
         this.log("onHideBar");
         this.viewerUI.startMode = false;
      }
      
      private function onVolume(param1:Event) : void {
         this.log("onVolume");
         Logic.instance.volume = this.shared.volume = this.viewerUI.volume;
         Logic.instance.muted = this.shared.isMuted = this.viewerUI.isMuted;
      }
      
      private function onVolumeChange(param1:DynamicEvent) : void {
         this.log("onVolumeChange: " + param1["volume"]);
         if(param1.hasOwnProperty("volume"))
         {
            if(this.viewerUI.volume == param1.volume)
            {
               return;
            }
            this.shared.volume = this.viewerUI.volume = param1.volume;
            this.shared.isMuted = this.viewerUI.isMuted;
         }
      }
      
      public function togglePlaying(... rest) : void {
         this.autoplayStatus = 2;
         this.log("togglePlaying");
         this.viewerUI.startMode = false;
         if((Logic.instance.playing) && (this.streamSizeReported))
         {
            this.viewerUI.showLoading = false;
            this.viewerUI.showBranding = false;
         }
         Logic.instance.playing = !Logic.instance.playing;
         if((this.viewerUI.showChatBtn) && !this.viewerUI.startMode && !this.displayChatReported)
         {
            if(this.viewerUI.chatOnTop)
            {
               gaTrack("Player","Chat icon","Displayed on top - " + Logic.instance.channel.mediaId);
            }
            else
            {
               gaTrack("Player","Chat icon","Displayed in controlbar - " + Logic.instance.channel.mediaId);
            }
            this.displayChatReported = true;
         }
      }
      
      private function onTogglePlaying(... rest) : void {
         this.log("onTogglePlaying " + Logic.instance.playing + "    " + rest);
         if(Logic.instance.playing)
         {
            if(this.viewerUI.startMode)
            {
               this.viewerUI.startMode = false;
               if((!this.streamSizeReported) && (This.getReference(Logic,"instance.media.video")) && (this.logoModuleReady))
               {
                  if(this.allowBranding)
                  {
                     this.viewerUI.showBranding = true;
                  }
                  else
                  {
                     this.viewerUI.showLoading = true;
                  }
               }
            }
            else if(this.streamSizeReported)
            {
               this.viewerUI.showLoading = false;
               this.viewerUI.showBranding = false;
            }
            
            if((Logic.instance.recorded) && this.startedRecordeds.indexOf(Logic.instance.recorded.mediaId) == -1)
            {
               this.startedRecordeds.push(Logic.instance.recorded.mediaId);
            }
         }
         this.viewerUI.playing = Logic.instance.playing;
      }
      
      private function onCreateSlideShow(param1:Event) : void {
         var _loc2_:SlideShow = Logic.instance.slideShow;
         this.log("onCreateSlideShow: " + _loc2_);
         this.viewerUI.controlBgTarget = this.viewerUI;
         _loc2_.addEventListener("destroy",this.onDestroySlideShow);
         this._logoPaddingBottom = this._logoPaddingTop = 0;
         Tween.to(this,
            {
               "logoPaddingBottom":this.viewerUI.controlOffset,
               "logoPaddingTop":this.viewerUI.topPanelHeight
            },30 * 0.5,"easeOutQuart");
      }
      
      private function onDestroySlideShow(param1:Event) : void {
         var _loc2_:SlideShow = param1.target as SlideShow;
         this.log("onDestroySlideShow");
         this.viewerUI.controlBgTarget = (Logic.hasInstance) && (Logic.instance.channel)?Logic.instance.channel:this.viewerUI;
         _loc2_.removeEventListener("destroy",this.onDestroySlideShow);
      }
      
      private function onCreateChannel(... rest) : void {
         var _loc3_:* = 0;
         var _loc4_:* = 0;
         var _loc2_:Channel = Logic.instance.channel;
         this.log("onCreateChannel: " + _loc2_);
         if((Logic.instance.seamlessSwitch) && (this.activeChannelInstance))
         {
            _loc3_ = this.seamlessSwitchDestroyList.length;
            _loc4_ = 0;
            while(_loc4_ < _loc3_)
            {
               if(this.seamlessSwitchDestroyList[_loc4_].listener is Function)
               {
                  this.seamlessSwitchDestroyList[_loc4_].listener.call(null,this.seamlessSwitchDestroyList[_loc4_].target);
               }
               _loc4_++;
            }
            this.seamlessSwitchOldChannel = this.activeChannelInstance;
            this.seamlessSwitchOldChannel.addEventListener("destroy",this.onSeamlessSwitchDone);
            this.onChannelDestroy(this.seamlessSwitchOldChannel);
            this.viewerUI.hideUIOnSeamlessSwitch = true;
            this.viewerUI.showSwitching = true;
         }
         else
         {
            this.viewerUI.controlBgTarget = _loc2_;
            this.log("STATUS_NONE");
            this.viewerUI.status = ViewerUI.STATUS_NONE;
         }
         this.viewerUI.closeAllPanes(true);
         if(_loc2_)
         {
            _loc2_.addEventListener("online",this.onOnline);
            _loc2_.addEventListener("offline",this.onOffline);
            _loc2_.addEventListener("status",this.onMediaStatus);
            _loc2_.addEventListener("destroy",this.onChannelDestroy,false,0,true);
            _loc2_.addEventListener("getStreamSize",this.onStreamSize);
            this.onCreateMedia(_loc2_);
         }
         this.activeChannelInstance = _loc2_;
         this.resize();
      }
      
      private function onMediaStatus(param1:Event) : void {
         if(param1.target.hasOwnProperty("status"))
         {
            this.log("onMediaStatus: status = " + param1.target.status);
            switch(param1.target.status)
            {
               case Channel.STATUS_DVR:
                  this.viewerUI.status = ViewerUI.STATUS_DVR;
                  break;
               case Channel.STATUS_LIVE:
                  this.viewerUI.status = ViewerUI.STATUS_LIVE;
                  break;
               case Channel.STATUS_OFFAIR:
                  this.viewerUI.status = ViewerUI.STATUS_OFFAIR;
                  break;
               case Channel.STATUS_WAITING:
                  this.viewerUI.status = ViewerUI.STATUS_NONE;
                  break;
            }
         }
      }
      
      private function onSeamlessSwitchDone(param1:Event) : void {
         this.log("onSeamlessSwitchDone, destroyed channel: " + param1.target);
         param1.target.removeEventListener("destroy",this.onSeamlessSwitchDone);
         this.viewerUI.hideUIOnSeamlessSwitch = false;
         this.viewerUI.showSwitching = false;
         this.viewerUI.controlBgTarget = (Logic.instance.channel) && (Logic.instance.channel.online)?Logic.instance.channel:Logic.instance.media;
         this.seamlessSwitchOldChannel = null;
         this._logoPaddingBottom = this._logoPaddingTop = -1;
         this.onUIControlOffset();
         this.onUITopPanelHeightChange();
      }
      
      private function onCreateStream(param1:Event = null) : void {
         this.log("onCreateStream");
         this.viewerUI.hasVideo = true;
         this.viewerUI.hasAudio = Logic.instance.media.audio;
         this.resize();
         this.onTogglePlaying();
      }
      
      private function onGetStreamSize(... rest) : void {
         var _loc2_:Rectangle = null;
         var _loc3_:Dvr = null;
         if((Logic.instance.media && !(Logic.instance.media.type == "channel")) && (!Logic.instance.media.playing) && this.autoplayStatus == 2)
         {
            if(!((loaderInfo.parameters.hasOwnProperty("style")) && loaderInfo.parameters.style == "1159"))
            {
               Logic.instance.media.playing = true;
            }
         }
         this.log("onGetStreamSize");
         if(this.cutter)
         {
            Logic.instance.playing = false;
         }
         this.streamSizeReported = true;
         this.viewerUI.showLoading = false;
         this.viewerUI.showBranding = false;
         if(Shell.instance.ready)
         {
            if(stage.displayState == "fullScreen")
            {
               this.wasStreamResizeInFs = true;
            }
            else
            {
               if((Logic.instance.channel) && (Logic.instance.channel.streamRect) && (Logic.instance.channel.online))
               {
                  _loc2_ = Logic.instance.channel.streamRect;
               }
               else if((Logic.instance.recorded) && (Logic.instance.recorded.streamRect))
               {
                  _loc2_ = Logic.instance.recorded.streamRect;
               }
               
               if(_loc2_)
               {
                  Shell.instance.resize(stage.stageWidth,stage.stageWidth * _loc2_.height / _loc2_.width + (this.viewerUI.lite?0:ViewerUI.baseH));
               }
            }
         }
         if(!this.viewerUI.playing)
         {
            this.onCreateStream();
         }
         if((Logic.instance.channel) && (Logic.instance.channel.online))
         {
            _loc3_ = This.getReference(Logic.instance,"media.modules.dvr") as Dvr;
            this.echo();
         }
         this.resize();
      }
      
      private function onAudio(param1:Event) : void {
         this.log("onAudio - hasVideo: " + Logic.instance.media.video + ", hasAudio: " + Logic.instance.media.audio);
         this.viewerUI.hasAudio = Logic.instance.media.audio;
         this.viewerUI.hasVideo = (Logic.instance.media.audio) || (Logic.instance.media.video);
         if((Logic.instance.channel) && !Logic.instance.channel.online)
         {
            this.viewerUI.hasVideo = this.viewerUI.hasAudio = false;
         }
         if((Logic.instance.channel) && (Logic.instance.channel.online) || (Logic.instance.recorded))
         {
            if((Logic.instance.media.audio) && !Logic.instance.media.video)
            {
               this.viewerUI.showLoading = false;
               this.viewerUI.showBranding = false;
            }
         }
      }
      
      private function onVideo(param1:Event) : void {
         this.log("onVideo");
         if((Logic.instance.channel && Logic.instance.channel.online) && (Logic.instance.channel.audio) && !Logic.instance.channel.video)
         {
            this.echo();
         }
         if((Logic.instance.channel) && (Logic.instance.channel.online) || (Logic.instance.recorded))
         {
            if((Logic.instance.media.audio) && !Logic.instance.media.video)
            {
               this.viewerUI.showLoading = false;
               this.viewerUI.showBranding = false;
            }
         }
      }
      
      private function onCreateModules(param1:*) : void {
         var _loc2_:Recorded = param1 as Recorded;
         if((!_loc2_) && (param1) && (param1.target))
         {
            _loc2_ = param1.target as Recorded;
         }
         this.log("onCreateModules: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.modules.addEventListener("createLogo",this.onLogoCreate);
            _loc2_.modules.addEventListener("createViewers",this.onCreateViewers);
            _loc2_.modules.addEventListener("createAllViewers",this.onCreateAllViewers);
            _loc2_.modules.addEventListener("createAllTimeTotal",this.onCreateAllTimeTotal);
            _loc2_.modules.addEventListener("createMeta",this.onCreateMeta);
            _loc2_.modules.addEventListener("createAgeVerification",this.onCreateAgeLock);
            _loc2_.modules.addEventListener("createAgeLock",this.onCreateAgeLock);
            _loc2_.modules.addEventListener("createRpinLock",this.onCreateRpinLock);
            _loc2_.modules.addEventListener("destroyRpinLock",this.onDestroyRpinLock);
            _loc2_.modules.addEventListener("createDvr",this.onCreateDvr);
            _loc2_.modules.addEventListener("destroyDvr",this.onDestroyDvr);
            if(!this.viewerUI.hideFeatures)
            {
               _loc2_.modules.addEventListener("createPoll",this.onCreatePoll);
               _loc2_.modules.addEventListener("destroyPoll",this.onDestroyPoll);
               _loc2_.modules.addEventListener("createQuality",this.onCreateQuality);
               _loc2_.modules.addEventListener("destroyQuality",this.onDestroyQuality);
               _loc2_.modules.addEventListener("createShare",this.onCreateShare);
               _loc2_.modules.addEventListener("createPpv",this.onCreatePpv);
               _loc2_.modules.addEventListener("destroyPpv",this.onDestroyPpv);
               _loc2_.modules.addEventListener("createMultiCam",this.onCreateMultiCam);
               _loc2_.modules.addEventListener("destroyMulticam",this.onDestroyMultiCam);
               _loc2_.modules.addEventListener("createClosedCaption",this.onCreateClosedCaption);
               _loc2_.modules.addEventListener("destroyClosedcaption",this.onDestroyClosedCaption);
            }
            _loc2_.modules.addEventListener(Module.LOCK,this.onModuleLock);
         }
      }
      
      private function onDestroyModules(param1:*) : void {
         var _loc2_:Recorded = param1 as Recorded;
         if((!_loc2_) && (param1) && (param1.target))
         {
            _loc2_ = param1.target as Recorded;
         }
         this.log("onDestroyModules: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.modules.removeEventListener("createLogo",this.onLogoCreate);
            _loc2_.modules.removeEventListener("createViewers",this.onCreateViewers);
            _loc2_.modules.removeEventListener("createAllViewers",this.onCreateAllViewers);
            _loc2_.modules.removeEventListener("createAllTimeTotal",this.onCreateAllTimeTotal);
            _loc2_.modules.removeEventListener("createMeta",this.onCreateMeta);
            _loc2_.modules.removeEventListener("createAgeVerification",this.onCreateAgeLock);
            _loc2_.modules.removeEventListener("createAgeLock",this.onCreateAgeLock);
            _loc2_.modules.removeEventListener("createRpinLock",this.onCreateRpinLock);
            _loc2_.modules.removeEventListener("destroyRpinLock",this.onDestroyRpinLock);
            _loc2_.modules.addEventListener("createDvr",this.onCreateDvr);
            _loc2_.modules.addEventListener("destroyDvr",this.onDestroyDvr);
            if(!this.viewerUI.hideFeatures)
            {
               _loc2_.modules.removeEventListener("createPoll",this.onCreatePoll);
               _loc2_.modules.removeEventListener("destroyPoll",this.onDestroyPoll);
               _loc2_.modules.removeEventListener("createQuality",this.onCreateQuality);
               _loc2_.modules.removeEventListener("destroyQuality",this.onDestroyQuality);
               _loc2_.modules.removeEventListener("createShare",this.onCreateShare);
               _loc2_.modules.removeEventListener("createPpv",this.onCreatePpv);
               _loc2_.modules.removeEventListener("destroyPpv",this.onDestroyPpv);
               _loc2_.modules.removeEventListener("createMultiCam",this.onCreateMultiCam);
               _loc2_.modules.removeEventListener("destroyMulticam",this.onDestroyMultiCam);
               _loc2_.modules.removeEventListener("createClosedCaption",this.onCreateClosedCaption);
               _loc2_.modules.removeEventListener("destroyClosedcaption",this.onDestroyClosedCaption);
            }
            _loc2_.modules.removeEventListener(Module.LOCK,this.onModuleLock);
         }
      }
      
      private function onCreateDvr(param1:Event) : void {
         this.dvrManager = new DvrManager(param1.target.dvr,this.viewerUI,this.shared);
         this.seamlessSwitchDestroyList.push({"listener":this.onDestroyDvr});
      }
      
      private function onDestroyDvr(param1:Event) : void {
         if(this.dvrManager)
         {
            this.dvrManager.destroy();
         }
      }
      
      private function onCreateClosedCaption(param1:Event) : void {
         this.log("onCreateClosedCaption");
         ViewerUI.instance.createCC();
         this.seamlessSwitchDestroyList.push({"listener":this.onDestroyClosedCaption});
         param1.target["closedCaption"].addEventListener(Module.UPDATE,this.onCCUpdate);
         this.updateCCButton();
      }
      
      private function onCCUpdate(param1:Event) : void {
         this.updateCCButton();
      }
      
      private function updateCCButton() : void {
         var _loc1_:Object = This.getReference(Logic.instance,"media.modules.closedCaption");
         if(_loc1_)
         {
            ViewerUI.instance.setCCOn(_loc1_["enabled"]);
         }
      }
      
      private function onDestroyClosedCaption(... rest) : void {
         var _loc2_:Event = null;
         var _loc3_:Dispatcher = null;
         this.log("onDestroyClosedCaption");
         if(rest.length == 1 && rest[0] is Event)
         {
            _loc2_ = rest[0] as Event;
            _loc3_ = _loc2_.target as Dispatcher;
            if(_loc3_)
            {
               _loc3_.removeEventListener(Module.UPDATE,this.onCCUpdate);
            }
         }
         ViewerUI.instance.removeCC();
      }
      
      private function onCCClick(param1:Event) : void {
         var _loc2_:Object = This.getReference(Logic.instance,"media.modules.closedCaption");
         if(_loc2_)
         {
            _loc2_["enabled"] = !_loc2_["enabled"];
         }
         this.updateCCButton();
      }
      
      private function onModuleLock(param1:Event) : void {
         var _loc2_:* = false;
         if(param1.currentTarget is Recorded)
         {
            _loc2_ = (param1.currentTarget as Recorded).modules.lock;
         }
         else
         {
            _loc2_ = (param1.currentTarget as ViewerModuleManager).lock;
         }
         if(_loc2_)
         {
            this.viewerUI.qualities = null;
         }
         else if(this.qualityManager)
         {
            this.onQualityUpdate();
         }
         
      }
      
      private function onCreateAgeLock(param1:Event) : void {
         var _loc3_:Module = null;
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            _loc3_ = param1.type == "createAgeLock"?_loc2_.ageLock:_loc2_.ageVerification;
            if(_loc3_)
            {
               _loc3_.addEventListener("lock",this.onAgeLockLock);
               _loc3_.addEventListener("ageOk",this.onAgeLockOk);
               _loc3_.addEventListener("badAge",this.onAgeLockBadAge);
               _loc3_.addEventListener("destroy",this.onDestroyAgeLock);
               this.seamlessSwitchDestroyList.push(
                  {
                     "target":_loc3_,
                     "listener":this.onDestroyAgeLock
                  });
            }
         }
      }
      
      private function onAgeLockLock(param1:Event) : void {
         var _loc2_:Module = param1.target as Module;
         this.viewerUI.createAge(_loc2_);
         _loc2_.removeEventListener("lock",this.onAgeLockLock);
      }
      
      private function onDestroyAgeLock(... rest) : void {
         var _loc2_:Module = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as Module;
            }
            else if(rest[0] is Module)
            {
               _loc2_ = rest[0] as Module;
            }
            
         }
         this.log("onDestroyAgeLock: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("lock",this.onAgeLockLock);
            _loc2_.removeEventListener("ageOk",this.onAgeLockOk);
            _loc2_.removeEventListener("destroy",this.onDestroyAgeLock);
            _loc2_.removeEventListener("badAge",this.onAgeLockBadAge);
         }
         this.viewerUI.removeAge();
      }
      
      private function onAgeLockOk(param1:Event) : void {
         this.log("onAgeLockOk");
         this.viewerUI.removeAge();
      }
      
      private function onAgeLockBadAge(param1:Event) : void {
         var _loc5_:String = null;
         var _loc2_:* = param1.target;
         this.viewerUI.removeAge();
         var _loc3_:Array = ["443086","11232192","11232197","13355653","13355657","13355692","11232200","13285980","13293463","13369161"];
         var _loc4_:Array = ["16428958","443086","13434778","13434793","13434812"];
         if((Logic.hasInstance) && (Logic.instance.channel) && !(_loc3_.indexOf(Logic.instance.channel.mediaId) == -1))
         {
            _loc5_ = "We\'re sorry, but this feature is unavailable.";
         }
         else if((Logic.hasInstance) && (Logic.instance.channel) && !(_loc4_.indexOf(Logic.instance.channel.mediaId) == -1))
         {
            _loc5_ = "We\'re sorry. You may not access this content.";
         }
         else
         {
            _loc5_ = Locale.instance.label(ViewerLabels.labelAgePaneBadAge,{"minage":_loc2_.minage}).split("\\n").join(" ");
         }
         
         this.viewerUI.displayAlert(_loc5_,ViewerUI.NOTIFICATION_ALERT);
         Logic.instance.channel.destroy();
      }
      
      private function onCreateQuality(param1:Event) : void {
         var _loc3_:Object = null;
         var _loc4_:String = null;
         var _loc5_:String = null;
         this.log("onCreateQuality()");
         this.qualityAutoSelected = false;
         this.autoMbrSelected = false;
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            if((Logic.hasInstance) && (Logic.instance.media) && !Logic.instance.media.hasEventListener(Channel.TRANSITION_STARTED))
            {
               Logic.instance.media.addEventListener(Channel.TRANSITION_STARTED,this.onTransitionStarted);
            }
            if((Logic.hasInstance) && (Logic.instance.media) && !Logic.instance.media.hasEventListener(Channel.TRANSITION_COMPLETED))
            {
               Logic.instance.media.addEventListener(Channel.TRANSITION_COMPLETED,this.onTransitionCompleted);
            }
            if(Locale.hasInstance)
            {
               _loc3_ = {};
               for(_loc5_ in this.qualityNames)
               {
                  _loc4_ = _loc5_ == "hd"?"HD":_loc5_.substr(0,1).toUpperCase() + _loc5_.substr(1);
                  if(Locale.instance.label(ViewerLabels["labelQuality" + _loc4_]) != "!" + ViewerLabels["labelQuality" + _loc4_])
                  {
                     _loc3_[_loc5_] = Locale.instance.label(ViewerLabels["labelQuality" + _loc4_]);
                  }
                  else
                  {
                     _loc3_[_loc5_] = this.qualityNames[_loc5_];
                  }
               }
            }
            if(_loc3_)
            {
               for(_loc5_ in _loc3_)
               {
                  this.qualityNames[_loc5_] = _loc3_[_loc5_];
               }
            }
            if(_loc2_.quality)
            {
               this.qualityManager = _loc2_.quality;
               _loc2_.quality.addEventListener("update",this.onQualityUpdate);
               _loc2_.quality.addEventListener(Event.CHANGE,this.onQualityAutoChange);
               if((_loc2_.quality.availableQualities) && (_loc2_.quality.availableQualities.length))
               {
                  this.onQualityUpdate();
               }
            }
            if(this.mobile)
            {
               _loc2_.quality.desiredQuality = Quality.LOW;
            }
            this.viewerUI.addEventListener(ViewerUI.MBR_CHANGE,this.onUiMbrChange);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_,
                  "listener":this.onDestroyQuality
               });
         }
         else
         {
            this.log("event target is not viewermodulemanager");
         }
      }
      
      private function onQualityAutoChange(param1:Event) : void {
         if(this.viewerUI.currentQuality == 4096)
         {
            this.viewerUI.currentAutoQuality = this.qualityManager.desiredQuality;
         }
      }
      
      private function onDestroyQuality(... rest) : void {
         var _loc2_:ViewerModuleManager = null;
         if((Logic.hasInstance) && (Logic.instance.media) && (Logic.instance.media.hasEventListener(Channel.TRANSITION_STARTED)))
         {
            Logic.instance.media.removeEventListener(Channel.TRANSITION_STARTED,this.onTransitionStarted);
         }
         if((Logic.hasInstance) && (Logic.instance.media) && (Logic.instance.media.hasEventListener(Channel.TRANSITION_COMPLETED)))
         {
            Logic.instance.media.removeEventListener(Channel.TRANSITION_COMPLETED,this.onTransitionCompleted);
         }
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as ViewerModuleManager;
            }
            else if(rest[0] is ViewerModuleManager)
            {
               _loc2_ = rest[0] as ViewerModuleManager;
            }
            
         }
         this.log("onDestroyQuality: " + _loc2_);
         if((_loc2_) && (_loc2_.quality))
         {
            _loc2_.quality.removeEventListener("update",this.onQualityUpdate);
         }
         this.qualityManager = null;
         this.viewerUI.qualities = null;
         this.viewerUI.removeEventListener(ViewerUI.MBR_CHANGE,this.onUiMbrChange);
      }
      
      private function onUiMbrChange(param1:Event) : void {
         if(this.viewerUI.currentQuality == 4096)
         {
            this.qualityManager.adaptiveMbr = true;
            this.viewerUI.currentAutoQuality = this.qualityManager.desiredQuality;
         }
         else
         {
            this.qualityManager.adaptiveMbr = false;
            this.qualityManager.desiredQuality = this.viewerUI.currentQuality;
            this.viewerUI.currentAutoQuality = -1;
         }
      }
      
      private function onQualityUpdate(... rest) : void {
         var _loc5_:Object = null;
         var _loc8_:Object = null;
         var _loc9_:* = 0;
         var _loc10_:* = false;
         this.log("onQualityUpdate");
         var _loc2_:Vector.<int> = new Vector.<int>();
         var _loc3_:Number = Number.MAX_VALUE;
         var _loc4_:Array = [];
         var _loc6_:uint = 0;
         while(_loc6_ < this.qualityManager.availableQualities.length)
         {
            _loc5_ = new Object();
            _loc8_ = this.qualityManager.getQualityProperties(_loc6_);
            if((_loc8_) && (_loc8_.description))
            {
               _loc5_.label = _loc8_.description;
            }
            else
            {
               switch(this.qualityManager.availableQualities[_loc6_])
               {
                  case Quality.LOW:
                     _loc5_.label = this.qualityNames.low;
                     break;
                  case Quality.MEDIUM:
                     _loc5_.label = this.qualityNames.medium;
                     break;
                  case Quality.HIGH:
                     _loc5_.label = this.qualityNames.high;
                     break;
                  case Quality.HD:
                     _loc5_.label = this.qualityNames.hd;
                     break;
               }
            }
            if((_loc8_) && (_loc8_.width) && (_loc8_.height))
            {
               _loc5_.width = _loc8_.width;
               _loc5_.height = _loc8_.height;
               if(Math.abs(_loc5_.height - height) < _loc3_)
               {
                  if(_loc5_.height >= this._height - ViewerUI.baseH)
                  {
                     _loc2_.unshift(this.qualityManager.availableQualities[_loc6_]);
                  }
                  else
                  {
                     _loc2_.push(this.qualityManager.availableQualities[_loc6_]);
                  }
                  _loc3_ = Math.abs(_loc5_.height - height);
               }
            }
            _loc5_.quality = this.qualityManager.availableQualities[_loc6_];
            Debug.echo("\tqObj.label: " + _loc5_.label);
            Debug.echo("\tqObj.quality: " + _loc5_.quality);
            _loc4_.push(_loc5_);
            _loc6_++;
         }
         if(this.qualityManager.adaptiveAvailable)
         {
            _loc5_ = new Object();
            _loc5_.label = this.qualityNames.auto;
            _loc5_.quality = 4096;
            _loc4_.push(_loc5_);
         }
         var _loc7_:Array = ["14883821","14883745","15022215","14986029","14986053","15022223","15098465","15098485","15098477","15098491","15428435","8914332"];
         if((loaderInfo.parameters.abs && loaderInfo.parameters.abs == "1" || Logic.hasInstance && Logic.instance.channel && !(_loc7_.indexOf(Logic.instance.channel.mediaId) == -1)) && (!this.autoMbrSelected) && (this.qualityManager.adaptiveAvailable))
         {
            this.qualityManager.adaptiveMbr = true;
            this.autoMbrSelected = true;
         }
         if(!this.qualityAutoSelected && (_loc2_.length))
         {
            _loc9_ = _loc2_[0];
            this.qualityManager.desiredQuality = _loc9_;
            this.qualityAutoSelected = true;
         }
         if((Logic.hasInstance) && !Logic.instance.media.modules.lock)
         {
            if((this.viewerUI.qualities) && this.viewerUI.qualities.length == _loc4_.length)
            {
               _loc6_ = 0;
               while(_loc6_ < _loc4_.length)
               {
                  if(!(_loc4_[_loc6_].label == this.viewerUI.qualities[_loc6_].label) || !(_loc4_[_loc6_].quality == this.viewerUI.qualities[_loc6_].quality))
                  {
                     _loc10_ = true;
                     break;
                  }
                  _loc6_++;
               }
            }
            else
            {
               _loc10_ = true;
            }
            if(_loc10_)
            {
               this.viewerUI.qualities = _loc4_;
            }
            if((this.qualityManager.adaptiveAvailable) && (this.qualityManager.adaptiveMbr))
            {
               this.viewerUI.currentQuality = 4096;
            }
            else
            {
               this.viewerUI.currentQuality = this.qualityManager.desiredQuality;
            }
         }
         if((this.qualityManager.adaptiveAvailable) && (this.qualityManager.adaptiveMbr))
         {
            this.viewerUI.currentAutoQuality = this.qualityManager.desiredQuality;
         }
         else
         {
            this.viewerUI.currentAutoQuality = -1;
         }
      }
      
      private function onTransitionStarted(param1:Event) : void {
         this.viewerUI.qualityStatus = 0;
      }
      
      private function onTransitionCompleted(param1:Event) : void {
         this.viewerUI.qualityStatus = 1;
      }
      
      private function onVideoAdStarted(param1:DynamicEvent) : void {
         this.log("onVideoAdStarted() - disableControlBar: " + param1.disableControlBar);
         this.isVideoAdPlaying = true;
         this.echo();
         if(param1.disableControlBar)
         {
            this.viewerUI.videoAdMode = true;
         }
      }
      
      private function onVideoAdFinished(param1:DynamicEvent) : void {
         this.log("onVideoAdFinished() - enableControlBar: " + param1.enableControlBar);
         this.isVideoAdPlaying = false;
         if(param1.enableControlBar)
         {
            this.viewerUI.videoAdMode = false;
         }
      }
      
      private function onMidrollNotification(param1:DynamicEvent) : void {
         this.log("onMidrollNotification()");
         Debug.explore(param1.data);
         if(param1.data.start)
         {
            this.viewerUI.displayMidNotification(uint(param1.data.duration));
            this.viewerUI.midNotificationOffering = param1.data.customOffering;
            if(!this.viewerUI.hasEventListener(ViewerUI.MIDROLL_DESTROY))
            {
               this.viewerUI.addEventListener(ViewerUI.MIDROLL_DESTROY,this.onMidrollNotificationDestroy);
            }
         }
         if(param1.data.time)
         {
            this.viewerUI.midNotificationTime = uint(param1.data.duration - param1.data.time);
         }
         if(param1.data.remove)
         {
            this.onMidrollNotificationDestroy();
         }
      }
      
      private function onMidrollNotificationDestroy(param1:Event = null) : void {
         this.log("onMidrollNotificationDestroy()");
         this.viewerUI.removeEventListener(ViewerUI.MIDROLL_DESTROY,this.onMidrollNotificationDestroy);
         this.viewerUI.closeMidNotification();
      }
      
      private function removeAdBlock() : void {
         if(this.adBlockTimer)
         {
            this.adBlockTimer.stop();
            this.adBlockTimer.removeEventListener(TimerEvent.TIMER,this.echo);
            this.adBlockTimer = null;
         }
         this.adBlockArguments = null;
         this.viewerUI.adBlockStatus = null;
      }
      
      private function onAdBlockStatus(param1:DynamicEvent) : void {
         var _loc2_:uint = 0;
         var _loc3_:Array = null;
         var _loc4_:uint = 0;
         this.log("onAdBlockStatus() - index: " + param1.blockIndex + " - total: " + param1.blockTotal + " - remove: " + param1.remove);
         if(param1.remove)
         {
            this.removeAdBlock();
         }
         else
         {
            this.adBlockArguments = 
               {
                  "index":param1.blockIndex,
                  "total":param1.blockTotal
               };
            if(!this.adBlockTimer)
            {
               _loc2_ = 0;
               _loc3_ = param1.blockDuration.split(",");
               if(param1.currentAdDuration)
               {
                  _loc2_ = param1.currentAdDuration + uint(_loc3_[0]) + 2;
               }
               else
               {
                  _loc4_ = 0;
                  while(_loc4_ < _loc3_.length)
                  {
                     _loc2_ = _loc2_ + (uint(_loc3_[_loc4_]) + 2);
                     _loc4_++;
                  }
               }
               this.adBlockTimer = new Timer(1000,_loc2_);
               this.adBlockTimer.addEventListener(TimerEvent.TIMER,this.echo);
               this.adBlockTimer.start();
            }
         }
         this.echo();
      }
      
      private function onChannelRejected(param1:Event) : void {
         this.log("onChannelRejected");
         this.log("STATUS_NONE");
         this.viewerUI.status = ViewerUI.STATUS_NONE;
         this.viewerUI.startMode = false;
      }
      
      private function onCreateRecorded(param1:Event) : void {
         var _loc2_:Recorded = Logic.instance.recorded;
         this.log("onCreateRecorded: " + _loc2_);
         this.progressStorageTimer.reset();
         this.progressStorageTimer.start();
         this.viewerUI.closeAllPanes(true);
         this.viewerUI.closePasswordPane();
         this.passwordMediaId = "";
         this.viewerUI.controlBgTarget = _loc2_;
         if(_loc2_)
         {
            _loc2_.addEventListener("destroy",this.onDestroyRecorded);
            _loc2_.addEventListener("finish",this.onFinishRecorded);
            _loc2_.addEventListener("getProgressive",this.onIsProgressive);
            _loc2_.addEventListener("hasHighlight",this.onHasHighlight);
            _loc2_.addEventListener(Recorded.SEEK_FAILED,this.onSeekFailed);
            _loc2_.addEventListener(Recorded.INVALID_TIME,this.onSeekInvalidTime);
            if(!_loc2_.channelId)
            {
               addEventListener(Event.ENTER_FRAME,this.waitForChannelId);
            }
         }
         this.onCreateMedia(_loc2_);
         this.log(Logic.instance.media.highlightId?"STATUS_HIGHLIGHT":"STATUS_RECORDED");
         this.viewerUI.status = Logic.instance.media.highlightId?ViewerUI.STATUS_HIGHLIGHT:ViewerUI.STATUS_RECORDED;
         this.viewerUI.duration = Logic.instance.recorded.duration;
         this.viewerUI.addEventListener(ViewerUI.SEEK_START_DRAG,this.onSeekStartDrag);
         this.viewerUI.addEventListener(ViewerUI.SEEK_STOP_DRAG,this.onSeekStopDrag);
         this.viewerUI.addEventListener(ViewerUI.SEEK_DRAG,this.onSeekDrag);
         this.viewerUI.addEventListener(ViewerUI.SEEK_SET,this.onSeekSet);
         addEventListener(Event.ENTER_FRAME,this.updateSeekBar);
         if((Logic.hasInstance) && !Logic.instance.media.autoPlay && !(this.autoplayStatus == 2))
         {
            this.viewerUI.startMode = !this.cutter;
            this.viewerUI.showLoading = false;
            this.viewerUI.showBranding = false;
         }
         this.resize();
         if(!Logic.instance.channel && this.autoplayStatus > 0 && !((loaderInfo.parameters.hasOwnProperty("style")) && loaderInfo.parameters.style == "1159"))
         {
            Logic.instance.playing = true;
         }
         if((this.shared) && (this.shared.savedProgress) && this.shared.savedProgress.mediaId == _loc2_.mediaId)
         {
            _loc2_.seek(this.shared.savedProgress.progress);
         }
      }
      
      public function setCutData(param1:Number, param2:Number) : void {
         this.viewerUI.intervalStart = param1;
         this.viewerUI.intervalEnd = param2;
      }
      
      private function onCutData(param1:DynamicEvent) : void {
         this.setCutData(param1.data.cutStart,param1.data.cutEnd);
      }
      
      private function onSeekDrag(param1:Event) : void {
         this.viewerUI.showLoading = true;
         this.isSeeking = true;
         Logic.instance.media.seek(this.viewerUI.progress,false);
      }
      
      private function onSeekStartDrag(param1:Event) : void {
         this.playBeforeSeek = Logic.instance.media.playing;
         if(Logic.instance.recorded)
         {
            Logic.instance.media.pause();
         }
      }
      
      private function onSeekStopDrag(param1:Event) : void {
         if(this.playBeforeSeek)
         {
            Logic.instance.media.play();
         }
         this.isSeeking = true;
         if(Logic.instance.recorded)
         {
            Logic.instance.recorded.seek(this.viewerUI.progress,false);
         }
      }
      
      private function onSeekSet(param1:DynamicEvent = null) : void {
         var _loc3_:* = NaN;
         var _loc4_:* = false;
         this.log("onSeekSet: " + (param1?param1.seekTo:""));
         var _loc2_:Boolean = (param1) && param1.source == "keyboard";
         if(param1)
         {
            _loc3_ = param1.seekTo;
            this.isSeeking = true;
            if(Logic.instance.recorded)
            {
               this.viewerUI.showLoading = true;
               _loc4_ = Logic.instance.recorded.seek(_loc3_,false);
               if(!_loc4_ && (_loc2_))
               {
                  this.viewerUI.progress = _loc3_;
               }
            }
            param1.stopPropagation();
            param1.stopImmediatePropagation();
         }
      }
      
      private function onSeekFailed(param1:Event) : void {
         this.isSeeking = false;
         this.viewerUI.showLoading = false;
      }
      
      private function onSeekInvalidTime(param1:Event) : void {
         this.isSeeking = false;
         this.onSeekSet();
      }
      
      private function onBufferEmpty(param1:Event) : void {
         if((!this.viewerUI.showBranding) && (This.getReference(Logic,"instance.media.video")) && (This.getReference(Logic,"instance.media.playing")))
         {
            this.viewerUI.showLoading = true;
         }
      }
      
      private function onBufferFull(param1:Event) : void {
         if(this.isSeeking)
         {
            this.isSeeking = false;
         }
         this.viewerUI.showLoading = false;
         this.viewerUI.showBranding = false;
      }
      
      private function onForceAutoplay(param1:DynamicEvent) : void {
         if(this.viewerUI)
         {
            this.viewerUI.startMode = !param1.data.autoplay;
         }
      }
      
      private function updateSeekBar(param1:Event = null) : void {
         if(!this.isSeeking)
         {
            this.viewerUI.progress = Logic.instance.media.progress;
         }
         this.viewerUI.duration = Logic.instance.recorded.duration?Logic.instance.recorded.duration:0;
         if((Logic.instance.recorded) && !(Logic.instance.recorded.progress == -1))
         {
            if(Logic.instance.media.progress > this.viewerUI.intervalStart && Logic.instance.media.progress < this.viewerUI.intervalEnd && (this.viewerUI.intervalStart > 0 || this.viewerUI.intervalEnd < 1))
            {
               this.inHighlight = true;
            }
            if(Logic.instance.media.progress > this.viewerUI.intervalEnd && (this.inHighlight))
            {
               gaTrack("Player","Recorded watched until the end","highlight - " + Logic.instance.media.mediaId + " hid: " + Logic.instance.media.highlightId + " - " + Math.round(Logic.instance.media.duration));
               Logic.instance.media.pause();
               Logic.instance.recorded.seek(this.viewerUI.intervalStart);
               this.inHighlight = false;
            }
         }
         this.viewerUI.bufferStart = Logic.instance.recorded.bufferStart;
         this.viewerUI.bufferPercent = Logic.instance.recorded.bufferProgress?Logic.instance.recorded.bufferProgress:0;
      }
      
      private function onDestroyRecorded(param1:Event) : void {
         var _loc2_:Recorded = param1.target as Recorded;
         this.log("onDestroyRecorded: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("destroy",this.onDestroyRecorded);
            _loc2_.removeEventListener("finish",this.onFinishRecorded);
            _loc2_.removeEventListener("getProgressive",this.onIsProgressive);
            _loc2_.removeEventListener("hasHighlight",this.onHasHighlight);
            _loc2_.removeEventListener(Recorded.SEEK_FAILED,this.onSeekFailed);
            _loc2_.removeEventListener(Recorded.INVALID_TIME,this.onSeekInvalidTime);
         }
         this.onDestroyMedia(_loc2_);
         removeEventListener(Event.ENTER_FRAME,this.updateSeekBar);
         removeEventListener(Event.ENTER_FRAME,this.waitForChannelId);
         this.progressStorageTimer.reset();
      }
      
      private function onFinishRecorded(param1:Event) : void {
         this.log("onFinishRecorded");
         if(this.startedRecordeds.indexOf(param1.currentTarget.mediaId) != -1)
         {
            if(Logic.instance.channel)
            {
               gaTrack("Player","Recorded watched until the end","off air - " + param1.currentTarget.mediaId + " - " + Math.round(param1.currentTarget.duration));
            }
            else if(!this.viewerUI.intervalEditable)
            {
               gaTrack("Player","Recorded watched until the end","recorded - " + param1.currentTarget.mediaId + " - " + Math.round(param1.currentTarget.duration));
            }
            
            this.startedRecordeds.splice(this.startedRecordeds.indexOf(param1.currentTarget.mediaId),1);
         }
         if((Logic.instance.recorded) && !Logic.instance.slideShow)
         {
            if(this.viewerUI.intervalEditable)
            {
               Logic.instance.recorded.seek(this.viewerUI.intervalStart);
            }
            else
            {
               Logic.instance.recorded.seek(0);
            }
            if(this.viewerUI.showLoading)
            {
               this.viewerUI.showLoading = false;
            }
         }
      }
      
      private function onStreamSize(param1:Event) : void {
         this.log("onStreamSize");
         this.streamSizeReported = true;
         this.viewerUI.showLoading = false;
         this.viewerUI.showBranding = false;
         this.resize();
      }
      
      private function onPassword(param1:Event) : void {
         this.log("onPassword");
         this.viewerUI.startMode = false;
         this.passwordMediaType = param1.target.type;
         this.passwordMediaId = param1.target.id;
         this.viewerUI.createPasswordPane();
         this.viewerUI.addEventListener("password",this.onSetPassword);
      }
      
      private function onSetPassword(param1:DynamicEvent) : void {
         this.log("onSetPassword");
         if(this.passwordMediaType == "channel")
         {
            Logic.instance.createChannel(this.passwordMediaId,true,param1.password,this.params);
         }
         else
         {
            Logic.instance.createRecorded(this.passwordMediaId,true,param1.password,this.params);
         }
      }
      
      private function onIsProgressive(param1:Event) : void {
      }
      
      private function onHasHighlight(param1:DynamicEvent) : void {
         var _loc2_:* = NaN;
         this.log("onHasHighlight");
         if(param1.data)
         {
            this.log("intervalStart: " + param1.data[0]);
            this.log("intervalEnd: " + param1.data[1]);
            this.viewerUI.intervalStart = this.highlightStart = param1.data[0];
            this.viewerUI.intervalEnd = this.highlightEnd = param1.data[1];
            this.viewerUI.intervalLock = true;
            _loc2_ = 0;
            if((this.shared) && (this.shared.savedProgress) && this.shared.savedProgress.mediaId == Logic.instance.recorded.mediaId)
            {
               _loc2_ = this.shared.savedProgress.progress;
            }
            if(this.deepLink == -1 && _loc2_ < this.highlightStart)
            {
               Logic.instance.media.seek(this.highlightStart);
            }
         }
      }
      
      private function onCreateMedia(param1:Recorded) : void {
         var _loc2_:String = null;
         this.log("onCreateMedia: " + param1);
         if(param1)
         {
            if(!this.logManager)
            {
               this.logManager = new LogManager(param1,this.viewerUI);
            }
            Logic.instance.display.visible = true;
            param1.addEventListener("rejected",this.onMediaRejected);
            param1.addEventListener("ioError",this.onIoError);
            param1.addEventListener(Recorded.VOLUME_CHANGE,this.onVolumeChange);
            param1.addEventListener("onVideoClicked",this.onDisplayClick);
            this.onCreateModules(param1);
            for(_loc2_ in this.mediaEventList)
            {
               param1.addEventListener(_loc2_,this.mediaEventList[_loc2_]);
            }
            if((param1.autoPlay) && (this.logoModuleReady))
            {
               if(this.allowBranding)
               {
                  this.viewerUI.showBranding = true;
               }
               else
               {
                  this.viewerUI.showLoading = true;
               }
            }
         }
         this.viewerUI.updateResizeBar(Logic.instance.media.id);
      }
      
      private function onMediaRejected(param1:DynamicEvent) : void {
         var _loc2_:String = null;
         var _loc3_:ThumbnailLoader = null;
         this.log("onMediaRejected");
         Debug.explore(param1);
         this.viewerUI.startMode = false;
         Logic.instance.display.visible = false;
         if((param1.geoLock) && (param1.geoLock.messageToken) && (param1.geoLock.customLabels) && (param1.geoLock.customLabels.length))
         {
            this.viewerUI.displayAlert(Locale.instance.label(param1.geoLock.messageToken),ViewerUI.NOTIFICATION_ALERT,0,Locale.instance.label(param1.geoLock.customLabels[0]));
            if((param1.rejectUrl) && (this.viewerUI.alertPane.submitBtn))
            {
               this.customRejectUrl = param1.rejectUrl;
               this.viewerUI.alertPane.submitBtn.addEventListener(MouseEvent.CLICK,this.onCustomAlertSubmitClick);
               this.viewerUI.alertPane.addEventListener(Pane.INIT_CLOSE,this.onCustomAlertClose);
            }
         }
         else if((param1.geoLock) && (param1.geoLock.messageToken))
         {
            this.viewerUI.displayAlert(Locale.instance.label(param1.geoLock.messageToken),ViewerUI.NOTIFICATION_ALERT);
         }
         else if(param1.referrerLock)
         {
            _loc2_ = param1.referrerLock.messageToken?Locale.instance.label(param1.referrerLock.messageToken):param1.reason;
            if((param1.referrerLock.redirectUrl) && (param1.referrerLock.thumbnailSecureUrl) && (param1.referrerLock.thumbnailUrl))
            {
               this.customRejectUrl = param1.referrerLock.redirectUrl;
               _loc3_ = new ThumbnailLoader(This.pageUrl.indexOf("https://") >= 0 && (param1.referrerLock.thumbnailSecureUrl)?param1.referrerLock.thumbnailSecureUrl:param1.referrerLock.thumbnailUrl);
               _loc3_.addEventListener(Event.COMPLETE,this.onReferrerLockThumbnailLoaded);
               _loc3_.start();
            }
            else if((param1.rejectUrl) || (param1.referrerLock.redirectUrl))
            {
               this.viewerUI.displayAlert(_loc2_,ViewerUI.NOTIFICATION_ALERT,0,Locale.instance.label(ViewerLabels.labelOk));
               if(this.viewerUI.alertPane.submitBtn)
               {
                  this.customRejectUrl = param1.rejectUrl || param1.referrerLock.redirectUrl;
                  this.viewerUI.alertPane.submitBtn.addEventListener(MouseEvent.CLICK,this.onCustomAlertSubmitClick);
                  this.viewerUI.alertPane.addEventListener(Pane.INIT_CLOSE,this.onCustomAlertClose);
               }
            }
            else
            {
               this.viewerUI.displayAlert(_loc2_,ViewerUI.NOTIFICATION_ALERT);
            }
            
         }
         else
         {
            this.viewerUI.displayAlert(param1.reason,ViewerUI.NOTIFICATION_ALERT);
         }
         
         
         this.forceKeepPanes.push(this.viewerUI.alertPane);
      }
      
      private function onReferrerLockThumbnailLoaded(param1:Event) : void {
         this.viewerUI.addEventListener(ViewerUI.CONTROL_PLAY,this.onThumbControlPlay);
         this.viewerUI.setThumbnail(param1.target as DisplayObject);
      }
      
      private function onThumbControlPlay(param1:Event) : void {
         navigateToURL(new URLRequest(this.customRejectUrl));
      }
      
      private function onCustomAlertSubmitClick(param1:Event) : void {
         if(this.customRejectUrl)
         {
            navigateToURL(new URLRequest(this.customRejectUrl),"_blank");
         }
      }
      
      private function onCustomAlertClose(param1:Event) : void {
         if(this.viewerUI.alertPane)
         {
            if(this.viewerUI.alertPane.submitBtn)
            {
               this.viewerUI.alertPane.submitBtn.removeEventListener(MouseEvent.CLICK,this.onCustomAlertSubmitClick);
            }
            this.viewerUI.alertPane.removeEventListener(Pane.INIT_CLOSE,this.onCustomAlertClose);
         }
      }
      
      private function onIoError(param1:DynamicEvent) : void {
         this.log("onIoError");
         Debug.explore(param1);
         Logic.instance.display.visible = false;
         this.viewerUI.displayAlert(param1.message,ViewerUI.NOTIFICATION_ALERT);
         this.forceKeepPanes.push(this.viewerUI.alertPane);
      }
      
      private function onDestroyMedia(param1:Recorded) : void {
         var _loc2_:String = null;
         this.log("onDestroyMedia: " + param1);
         if(this.logManager)
         {
            this.logManager.destroy();
            this.logManager = null;
         }
         if(param1)
         {
            param1.removeEventListener("rejected",this.onMediaRejected);
            param1.removeEventListener("ioError",this.onIoError);
            for(_loc2_ in this.mediaEventList)
            {
               param1.removeEventListener(_loc2_,this.mediaEventList[_loc2_]);
            }
            if(param1.modules)
            {
               this.onDestroyModules(param1);
            }
         }
         if(this.allowBranding)
         {
            this.viewerUI.showBranding = !(Logic.instance.media == null);
         }
         else
         {
            this.viewerUI.showLoading = !(Logic.instance.media == null);
         }
         this.viewerUI.showSwitching = false;
         this.viewerUI.closeAllPanes.apply(this.viewerUI,[true].concat(this.forceKeepPanes));
         this.forceKeepPanes = [];
         if(this.passwordMediaId == "")
         {
            this.viewerUI.closePasswordPane();
         }
         this.viewerUI.controlBgTarget = this.viewerUI;
         if(!Logic.instance.seamlessSwitch)
         {
            this.viewerUI.hasVideo = this.viewerUI.hasAudio = false;
            this.log("STATUS_NONE");
            this.viewerUI.status = ViewerUI.STATUS_NONE;
         }
         this.viewerUI.intervalStart = 0;
         this.viewerUI.intervalEnd = 1;
         this.viewerUI.intervalLock = false;
         this.streamSizeReported = false;
         this.logoModuleReady = false;
         this.highlightStart = 0;
         this.highlightEnd = 1;
         this.deepLink = -1;
      }
      
      private function onControlPlay(param1:Event) : void {
         this.viewerUI.startMode = false;
         this.autoplayStatus = 2;
         if((!this.streamSizeReported) && (This.getReference(Logic,"instance.media.video")) && (this.logoModuleReady))
         {
            if(this.allowBranding)
            {
               this.viewerUI.showBranding = true;
            }
            else
            {
               this.viewerUI.showLoading = true;
            }
         }
         Logic.instance.playing = true;
         if((this.viewerUI.showChatBtn) && !this.viewerUI.startMode && !this.displayChatReported)
         {
            if(this.viewerUI.chatOnTop)
            {
               gaTrack("Player","Chat icon","Displayed on top - " + Logic.instance.channel.mediaId);
            }
            else
            {
               gaTrack("Player","Chat icon","Displayed in controlbar - " + Logic.instance.channel.mediaId);
            }
            this.displayChatReported = true;
         }
      }
      
      private function onCreateViewers(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            _loc2_.viewers.addEventListener("update",this.echo);
            _loc2_.viewers.addEventListener("destroy",this.onDestroyViewers);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_.viewers,
                  "listener":this.onDestroyViewers
               });
         }
         this.echo();
      }
      
      private function onDestroyViewers(... rest) : void {
         var _loc2_:Viewers = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as Viewers;
            }
            else if(rest[0] is Viewers)
            {
               _loc2_ = rest[0] as Viewers;
            }
            
         }
         this.log("onDestroyViewers: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",this.echo);
            _loc2_.removeEventListener("destroy",this.onDestroyViewers);
         }
         this.echo();
      }
      
      private function onCreateAllViewers(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            _loc2_.allViewers.addEventListener("update",this.echo);
            _loc2_.allViewers.addEventListener("destroy",this.onDestroyAllViewers);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_.allViewers,
                  "listener":this.onDestroyAllViewers
               });
         }
         this.echo();
      }
      
      private function onDestroyAllViewers(... rest) : void {
         var _loc2_:AllViewers = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as AllViewers;
            }
            else if(rest[0] is AllViewers)
            {
               _loc2_ = rest[0] as AllViewers;
            }
            
         }
         this.log("onDestroyAllViewers: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",this.echo);
            _loc2_.removeEventListener("destroy",this.onDestroyAllViewers);
         }
         this.echo();
      }
      
      private function onCreateAllTimeTotal(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            _loc2_.allTimeTotal.addEventListener("update",this.echo);
            _loc2_.allTimeTotal.addEventListener("destroy",this.onDestroyAllTimeTotal);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_.allTimeTotal,
                  "listener":this.onDestroyAllTimeTotal
               });
         }
         this.echo();
      }
      
      private function onDestroyAllTimeTotal(... rest) : void {
         var _loc2_:AllTimeTotal = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as AllTimeTotal;
            }
            else if(rest[0] is AllTimeTotal)
            {
               _loc2_ = rest[0] as AllTimeTotal;
            }
            
         }
         this.log("onDestroyAllTimeTotal: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",this.echo);
            _loc2_.removeEventListener("destroy",this.onDestroyAllTimeTotal);
         }
         this.echo();
      }
      
      private function onOnline(... rest) : void {
         var _loc2_:Stream = null;
         this.log("onOnline");
         this.viewerUI.closePasswordPane();
         this.passwordMediaId = "";
         this.viewerUI.closeAlertPane();
         Logic.instance.display.visible = true;
         Alert.clear();
         if(this.logoModuleReady)
         {
            if(this.allowBranding)
            {
               this.viewerUI.showBranding = true;
            }
            else
            {
               this.viewerUI.showLoading = true;
            }
         }
         if((Logic.hasInstance) && !Logic.instance.media.autoPlay)
         {
            this.viewerUI.startMode = !this.cutter;
            this.viewerUI.showLoading = false;
            this.viewerUI.showBranding = false;
         }
         if(Logic.instance.channel)
         {
            _loc2_ = Logic.instance.channel.modules.stream;
         }
         if(!(this.testChatChannels.indexOf(Logic.instance.channel.mediaId) == -1) && Locale.instance.language == "en_US" && !(This.onSite(ViewerUI.pageUrl) & This.SITE_TYPE_STAGE) && !this.viewerUI.chromeless)
         {
            if(!this.setChatPosition)
            {
               this.viewerUI.chatOnTop = Math.random() > 0.5?true:false;
               this.setChatPosition = true;
            }
            this.viewerUI.showChatBtn = true;
            if(!this.viewerUI.startMode)
            {
               if(this.viewerUI.chatOnTop)
               {
                  gaTrack("Player","Chat icon","Displayed on top - " + Logic.instance.channel.mediaId);
               }
               else
               {
                  gaTrack("Player","Chat icon","Displayed in controlbar - " + Logic.instance.channel.mediaId);
               }
               this.displayChatReported = true;
            }
         }
         this.echo();
      }
      
      private function onOffline(param1:Event) : void {
         this.log("onOffline");
         this.viewerUI.closePasswordPane();
         this.passwordMediaId = "";
         if((this.adBlockArguments) || (this.adBlockTimer))
         {
            this.removeAdBlock();
         }
         this.onMidrollNotificationDestroy();
         this.echo();
         if(Logic.instance.media.type != "recorded")
         {
            this.viewerUI.showLoading = false;
            this.viewerUI.showBranding = false;
            this.log("STATUS_OFFAIR");
            this.viewerUI.status = ViewerUI.STATUS_OFFAIR;
         }
         else
         {
            if(!this.viewerUI.startMode && (this.logoModuleReady))
            {
               if(this.allowBranding)
               {
                  this.viewerUI.showBranding = true;
               }
               else
               {
                  this.viewerUI.showLoading = true;
               }
            }
            else
            {
               this.viewerUI.showLoading = false;
               this.viewerUI.showBranding = false;
            }
            this.log("STATUS_OFFAIR_CONTENT");
            this.viewerUI.status = ViewerUI.STATUS_OFFAIR_CONTENT;
         }
         if((Logic.hasInstance) && (Logic.instance.channel) && !(this.testChatChannels.indexOf(Logic.instance.channel.mediaId) == -1))
         {
            this.viewerUI.showChatBtn = false;
         }
         this.resize();
      }
      
      private function onCreateShare(param1:Event) : void {
         if(!this.viewerUI.hideShare && (Logic.hasInstance) && !Logic.instance.isLimitedChannel)
         {
            this.viewerUI.createShare();
            this.viewerUI.shareModule = param1.target.share;
            (param1.target.share as Share).addEventListener("destroy",this.onDestroyShare);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":param1.target.share,
                  "listener":this.onDestroyShare
               });
         }
      }
      
      private function onDestroyShare(... rest) : void {
         var _loc2_:Share = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as Share;
            }
            else if(rest[0] is Share)
            {
               _loc2_ = rest[0] as Share;
            }
            
         }
         this.log("onDestroyShare: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("destroy",this.onDestroyShare);
         }
         this.viewerUI.shareModule = null;
         this.viewerUI.removeShare();
      }
      
      private function onCreatePoll(param1:Event) : void {
         this.log("onCreatePoll");
         var _loc2_:Poll = (param1.target as ViewerModuleManager).poll;
         if(this.isPrerollDone)
         {
            this.createPoll(_loc2_);
         }
         else
         {
            this.delayedPoll = _loc2_;
         }
      }
      
      private function createPoll(param1:Poll) : void {
         if(param1)
         {
            this.viewerUI.createPoll();
            this.viewerUI.pollModule = param1;
            this.viewerUI.addEventListener("pollEvent",this.onPollEvent);
            this.seamlessSwitchDestroyList.push({"listener":this.onDestroyPoll});
         }
      }
      
      private function onPollEvent(param1:Event) : void {
         param1.stopImmediatePropagation();
         param1.stopPropagation();
      }
      
      private function onDestroyPoll(... rest) : void {
         this.log("onDestroyPoll");
         this.viewerUI.removeEventListener("pollEvent",this.onPollEvent);
         this.viewerUI.removePoll();
      }
      
      private function onCreateMultiCam(param1:Event) : void {
         var _loc8_:Object = null;
         var _loc9_:uint = 0;
         var _loc10_:uint = 0;
         var _loc2_:String = ViewerUI.pageUrl;
         if((Logic.hasInstance) && (["13928427","13928441"] as Array).indexOf(Logic.instance.channel.id) > -1 && (This.onSite(_loc2_)))
         {
            return;
         }
         if((Logic.hasInstance) && (["14883745","14883821"] as Array).indexOf(Logic.instance.channel.id) > -1)
         {
            return;
         }
         var _loc3_:Array = _loc2_.indexOf("://") == -1?_loc2_.split("/"):String(_loc2_.split("://")[1]).split("/");
         var _loc4_:String = _loc3_[0];
         var _loc5_:String = _loc3_.length > 1?_loc3_[1]:"";
         var _loc6_:Array = [
            {
               "cidList":["14986029","14986053"],
               "siteList":[{"site":"kurier.at"},{"site":"stadtkinder.com"},
                  {
                     "site":"facebook.com",
                     "page":"LastfmScrobbler"
                  }]
            },
            {
               "cidList":["13755171","15361857"],
               "siteList":[{"site":"ustream.tv"},{"site":"sziget.hu"},{"site":"origo.hu"}]
            },
            {
               "cidList":["15592051","15592063","15593281","15593289"],
               "siteList":[
                  {
                     "site":"ustream.tv",
                     "page":"telekom-nagyonbalaton"
                  },
                  {
                     "site":"telekom.hu",
                     "page":"fesztivalok"
                  },
                  {
                     "site":"facebook.com",
                     "page":"tmobilehungary"
                  },
                  {
                     "site":"facebook.com",
                     "page":"TelekomHU"
                  },{"site":"www.sziget.hu"},{"site":"www.origo.hu"},{"site":"quart.hu"},{"site":"szeretlekmagyarorszag.hu"}]
            }];
         if(Logic.hasInstance)
         {
            _loc9_ = 0;
            while(_loc9_ < _loc6_.length)
            {
               if((_loc6_[_loc9_].cidList as Array).indexOf(Logic.instance.channel.id) > -1)
               {
                  _loc10_ = 0;
                  while(_loc10_ < _loc6_[_loc9_].siteList.length)
                  {
                     _loc8_ = _loc6_[_loc9_].siteList[_loc10_];
                     if(_loc4_.indexOf(_loc8_.site) > -1 && (!_loc8_.page || _loc5_.indexOf(_loc8_.page as String) > -1))
                     {
                        return;
                     }
                     _loc10_++;
                  }
               }
               _loc9_++;
            }
         }
         this.log("onCreateMulticam");
         var _loc7_:MultiCam = (param1.target as ViewerModuleManager).multiCam;
         if(_loc7_)
         {
            this.viewerUI.createMultiCam();
            this.viewerUI.addEventListener(ViewerUI.CHANNEL_SWITCH,this.onChannelSwitch);
            this.seamlessSwitchDestroyList.push({"listener":this.onDestroyMultiCam});
         }
      }
      
      private function onDestroyMultiCam(... rest) : void {
         this.log("onDestroyMulticam");
         this.viewerUI.removeMultiCam();
         if(this.viewerUI.hasEventListener(ViewerUI.CHANNEL_SWITCH))
         {
            this.viewerUI.removeEventListener(ViewerUI.CHANNEL_SWITCH,this.onChannelSwitch);
         }
      }
      
      private function onChannelSwitch(param1:DynamicEvent) : void {
         gaTrack("Player","Multicam","Switch media");
         if((!Logic.instance.seamlessSwitch) && (Logic.instance.media) && !(Logic.instance.media.mediaId == param1.cid))
         {
            Logic.instance.media.destroy();
         }
         if(param1.cid)
         {
            Logic.instance.createChannel(param1.cid,true,null,{"enableStageVideo":this.enableStageVideo});
         }
      }
      
      private function onPrerollDone(param1:Event) : void {
         this.log("onPrerollDone");
         this.isPrerollDone = true;
         if(this.delayedPoll)
         {
            this.createPoll(this.delayedPoll);
            this.delayedPoll = null;
         }
         var _loc2_:Timer = new Timer(1000,1);
         _loc2_.addEventListener(TimerEvent.TIMER_COMPLETE,this.startBlinking);
         _loc2_.start();
      }
      
      private function startBlinking(param1:Event = null) : void {
         if((param1) && param1 is TimerEvent)
         {
            TimerEvent(param1).target.removeEventListener(TimerEvent.TIMER_COMPLETE,this.startBlinking);
         }
         if((stage) && stage.stageWidth < 300)
         {
            this.viewerUI.blinkMultiCamBtn(8000);
            this.viewerUI.addEventListener("MultiCamBtnToolTipDone",this.onDelayedQualityBlink);
         }
         else
         {
            this.viewerUI.blinkMultiCamBtn();
            this.viewerUI.blinkQualitySelector();
         }
      }
      
      private function onDelayedQualityBlink(param1:Event) : void {
         this.viewerUI.blinkQualitySelector();
         this.viewerUI.removeEventListener("MultiCamBtnToolTipDone",this.onDelayedQualityBlink);
      }
      
      private function onCreatePpv(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if((_loc2_) && (_loc2_.ppv))
         {
            if(Logic.instance.recorded)
            {
               Logic.instance.recorded.buffer = 0;
            }
            if(This.getReference(stage,"loaderInfo.parameters.hasTicket") == "true")
            {
               _loc2_.ppv.hasTicket = true;
            }
            this.viewerUI.createPpvPane(_loc2_.ppv);
            _loc2_.ppv.addEventListener("update",this.onPpvUpdate);
            _loc2_.ppv.addEventListener("buyTicket",this.onPpvInteraction);
            _loc2_.ppv.addEventListener("enterTicket",this.onPpvInteraction);
            _loc2_.ppv.addEventListener("paidReject",this.onPpvPaidReject);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_,
                  "listener":this.onDestroyPpv
               });
         }
      }
      
      private function onPpvPaidReject(param1:Event) : void {
         this.forceKeepPanes.push(this.viewerUI.ppvPane);
      }
      
      private function onPpvInteraction(param1:Event) : void {
         stage.displayState = StageDisplayState.NORMAL;
         this.viewerUI.lite = false;
         this.resize();
      }
      
      private function onPpvUpdate(param1:Event) : void {
         var _loc2_:Ppv = param1.target as Ppv;
         if(this.allowBranding)
         {
            if((_loc2_) && (this.viewerUI.showBranding))
            {
               this.viewerUI.showBranding = (this.viewerUI.showBranding) && !_loc2_.lock;
            }
         }
         else if((_loc2_) && (this.viewerUI.showLoading))
         {
            this.viewerUI.showLoading = (this.viewerUI.showLoading) && !_loc2_.lock;
         }
         
      }
      
      private function onDestroyPpv(... rest) : void {
         var _loc2_:ViewerModuleManager = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as ViewerModuleManager;
            }
            else if(rest[0] is ViewerModuleManager)
            {
               _loc2_ = rest[0] as ViewerModuleManager;
            }
            
         }
         this.log("onDestroyPpv: " + _loc2_);
         if((_loc2_) && (_loc2_.ppv))
         {
            _loc2_.ppv.removeEventListener("update",this.onPpvUpdate);
            _loc2_.ppv.removeEventListener("buyTicket",this.onPpvInteraction);
            _loc2_.ppv.removeEventListener("enterTicket",this.onPpvInteraction);
            _loc2_.ppv.removeEventListener("paidReject",this.onPpvPaidReject);
            if(this.forceKeepPanes.indexOf(this.viewerUI.ppvPane) == -1)
            {
               this.viewerUI.closePpvPane();
            }
         }
         else
         {
            this.log("onDestroyPpv: !target or !target.ppv");
         }
      }
      
      private function echo(param1:Event = null) : void {
         var _loc3_:uint = 0;
         var _loc4_:String = null;
         var _loc5_:String = null;
         var _loc6_:String = null;
         var _loc7_:ViewerModuleManager = null;
         var _loc8_:Viewers = null;
         var _loc2_:* = "";
         if(this.adBlockArguments)
         {
            if(!((Logic.instance.media) && Logic.instance.media.type == "channel" && !(Logic.instance.media.mediaId == "7343051" || Logic.instance.media.mediaId == "7377223")))
            {
               _loc3_ = this.adBlockTimer.repeatCount - this.adBlockTimer.currentCount;
               if(_loc3_ <= 0)
               {
                  _loc2_ = Locale.instance.label(ViewerLabels.labelAdBlockResuming,
                     {
                        "currentAd":this.adBlockArguments.index + 1,
                        "totalAd":this.adBlockArguments.total
                     });
               }
               else
               {
                  _loc2_ = Locale.instance.label(ViewerLabels.labelAdBlockRemaining,
                     {
                        "currentAd":this.adBlockArguments.index + 1,
                        "totalAd":this.adBlockArguments.total,
                        "remainingTime":_loc3_
                     });
               }
               this.viewerUI.adBlockStatus = _loc2_;
            }
         }
         else if((Logic.instance.media) && (Logic.instance.media.type == "channel") && !this.isVideoAdPlaying)
         {
            if((Logic.instance.channel.online) && !This.getReference(Logic.instance,"channel.modules.ppv"))
            {
               _loc7_ = This.getReference(Logic.instance,"media.modules") as ViewerModuleManager;
               if((_loc7_) && (Logic.instance.channel.brandId == "1" || !(Logic.instance.channel.brandId == "1") && !(This.getReference(loaderInfo,"parameters.viewcount") == "false")))
               {
                  switch(Locale.instance.language)
                  {
                     case "ja_JP":
                     case "ko_KR":
                        _loc8_ = _loc7_.allViewers;
                        break;
                     default:
                        _loc8_ = _loc7_.hasOwnProperty("allTimeTotal")?_loc7_.allTimeTotal:_loc7_.allViewers;
                  }
                  if((_loc8_ && _loc7_.viewers) && (_loc8_.toString()) && (_loc7_.viewers.toString()))
                  {
                     _loc4_ = _loc7_.viewers.toString();
                     _loc5_ = this.getFormattedViewCount(_loc8_.toString());
                     _loc6_ = _loc8_.toString();
                     _loc2_ = Locale.instance.label("labelViewersWithTotals");
                  }
                  else if((_loc7_.viewers) && (_loc7_.viewers.toString()))
                  {
                     _loc4_ = _loc7_.viewers.toString();
                     _loc2_ = "#current# " + Locale.instance.label(ViewerLabels.labelViewers);
                  }
                  else
                  {
                     _loc4_ = "";
                     _loc5_ = "";
                     _loc6_ = "";
                  }
                  
                  if(Logic.instance.channel.mediaId == "8914332" && (_loc8_))
                  {
                     _loc2_ = "#total# total views";
                     _loc4_ = "";
                     _loc5_ = this.getFormattedViewCount(_loc8_.toString());
                     _loc6_ = _loc8_.toString();
                  }
               }
            }
            this.viewerUI.setViewerLabel(_loc2_,_loc4_,_loc5_,_loc6_);
         }
         
      }
      
      private function getFormattedViewCount(param1:String) : String {
         var _loc3_:uint = 0;
         var _loc2_:String = param1;
         if(Locale.instance.language == "en_US")
         {
            _loc3_ = uint(_loc2_);
            if(_loc3_ > 1000000)
            {
               _loc2_ = Math.round(_loc3_ / 100000) / 10 + "M";
            }
            else if(_loc3_ > 100000)
            {
               _loc2_ = Math.round(_loc3_ / 1000) + "K";
               if(_loc2_ == "1000K")
               {
                  _loc2_ = "1M";
               }
            }
            
         }
         return _loc2_;
      }
      
      public function get mobile() : Boolean {
         return this._mobile;
      }
      
      public function set mobile(param1:Boolean) : void {
         this._mobile = param1;
         this.resize();
      }
      
      public function resize(... rest) : void {
         var _loc2_:* = NaN;
         this.log("resize");
         if(!(stage.displayState == "fullScreen") && (this.wasStreamResizeInFs))
         {
            this.wasStreamResizeInFs = false;
            this.onGetStreamSize();
         }
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            this._height = rest[1];
         }
         if(this.viewerUI)
         {
            this.viewerUI.resize(this._width,this._height);
         }
         if(Logic.hasInstance)
         {
            Logic.instance.display.width = this._width;
            if(stage.displayState == StageDisplayState.FULL_SCREEN && this._width == stage.stageWidth && this._height == stage.stageHeight || (this.viewerUI.lite))
            {
               _loc2_ = this._height;
            }
            else
            {
               _loc2_ = this._height - ViewerUI.baseH * ViewerUI.ratio;
            }
            Logic.instance.display.height = _loc2_;
            addEventListener(Event.ENTER_FRAME,this.setStreamBounds);
         }
         if((this.viewerUI) && loaderInfo.parameters.campaign == "facebook")
         {
            this.drawCustomBg(this.viewerUI.streamBounds);
         }
      }
      
      private function setStreamBounds(param1:Event = null) : void {
         var _loc2_:Rectangle = null;
         if((this.seamlessSwitchOldChannel) && (this.seamlessSwitchOldChannel.streamBounds))
         {
            _loc2_ = this.seamlessSwitchOldChannel.streamBounds;
         }
         else if((Logic.instance.channel) && (Logic.instance.channel.streamBounds) && (Logic.instance.channel.online))
         {
            _loc2_ = Logic.instance.channel.streamBounds;
         }
         else if((Logic.instance.recorded) && (Logic.instance.recorded.streamBounds))
         {
            _loc2_ = Logic.instance.recorded.streamBounds;
         }
         else
         {
            _loc2_ = new Rectangle();
         }
         
         
         if(_loc2_.height > 0 && _loc2_.height <= this._height)
         {
            this.viewerUI.streamBounds = _loc2_;
            removeEventListener(Event.ENTER_FRAME,this.setStreamBounds);
         }
         if(loaderInfo.parameters.campaign == "facebook")
         {
            this.drawCustomBg(_loc2_);
         }
      }
      
      private function drawCustomBg(param1:Rectangle = null) : void {
         graphics.clear();
         graphics.beginFill(0);
         if((this.streamSizeReported) && (param1) && (This.getReference(Logic.instance,"media.stageVideo")))
         {
            if(param1.y)
            {
               graphics.drawRect(0,0,this._width,param1.y + 1);
               graphics.endFill();
            }
            if(!(param1.bottom == this._height) && !(param1.bottom == this._height - ViewerUI.baseH))
            {
               graphics.beginFill(0);
               graphics.drawRect(0,param1.bottom - 1,this._width,this._height - (param1.bottom - 1));
               graphics.endFill();
            }
            if(param1.x)
            {
               graphics.beginFill(0);
               graphics.drawRect(0,0,param1.x + 1,this._height);
               graphics.endFill();
            }
            if(param1.right != this._width)
            {
               graphics.beginFill(0);
               graphics.drawRect(param1.right - 1,0,this._width - (param1.right - 1),this._height);
               graphics.endFill();
            }
         }
         else
         {
            graphics.drawRect(0,0,this._width,this._height);
            graphics.endFill();
         }
      }
      
      private function onMeta(param1:Event) : void {
         var _loc2_:* = NaN;
         if(Logic.instance.recorded)
         {
            this.updateSeekBar();
            if(!(this.deepLink == -1) && (Logic.instance.recorded.duration))
            {
               Debug.echo("onMeta deepLink seek");
               _loc2_ = this.highlightStart + this.deepLink / Logic.instance.media.duration;
               if(_loc2_ < this.highlightEnd)
               {
                  Logic.instance.media.seek(_loc2_);
               }
               else
               {
                  Logic.instance.media.seek(this.highlightStart);
               }
               this.deepLink = -1;
            }
         }
      }
      
      private function onCreateMeta(param1:Event) : void {
         var _loc3_:Array = null;
         this.log("onCreateMeta");
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_)
         {
            _loc2_.meta.addEventListener("update",this.onMetaUpdate);
            _loc2_.meta.addEventListener("destroy",this.onDestroyMeta);
            this.seamlessSwitchDestroyList.push(
               {
                  "target":_loc2_.meta,
                  "listener":this.onDestroyMeta
               });
            if(This.onSite(This.pageUrl) < 2)
            {
               _loc3_ = ["14878989","16098889"];
               if(This.getReference(_loc2_,"meta.data.ownerChannelId"))
               {
                  if(_loc3_.indexOf(_loc2_.meta.data.ownerChannelId) > -1)
                  {
                     this.viewerUI.telekomizalas(_loc2_.meta.data.ownerChannelId);
                  }
               }
               else if(Logic.instance.channel)
               {
                  if(_loc3_.indexOf(Logic.instance.channel.id) > -1)
                  {
                     this.viewerUI.telekomizalas(loaderInfo.parameters.cid);
                  }
               }
               
            }
         }
      }
      
      private function onDestroyMeta(... rest) : void {
         var _loc2_:Meta = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as Meta;
            }
            else if(rest[0] is Meta)
            {
               _loc2_ = rest[0] as Meta;
            }
            
         }
         this.log("onDestroyMeta: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",this.onMetaUpdate);
            _loc2_.removeEventListener("destroy",this.onDestroyMeta);
         }
      }
      
      private function onMetaUpdate(param1:Event) : void {
         this.log("onMetaUpdate");
         var _loc2_:Meta = param1.target as Meta;
         if(_loc2_.title)
         {
            this.viewerUI.title = _loc2_.title;
         }
         if((Locale.instance.language == "ko_KR") && (_loc2_.countryCode) && !this.krCustomFontTracked)
         {
            gaTrack("Player","Korean custom font",(ViewerUI.baseFontName != "Arial"?ViewerUI.baseFontName:"not available") + " - " + _loc2_.countryCode);
            this.krCustomFontTracked = true;
         }
      }
      
      private function onChannelDestroy(... rest) : void {
         var _loc2_:Recorded = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest.target as Recorded;
            }
            if(rest[0] is Recorded)
            {
               _loc2_ = rest[0] as Recorded;
            }
         }
         this.log("onChannelDestroy: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("online",this.onOnline);
            _loc2_.removeEventListener("offline",this.onOffline);
            _loc2_.removeEventListener("destroy",this.onChannelDestroy);
            _loc2_.removeEventListener("getStreamSize",this.onStreamSize);
            if(this.testChatChannels.indexOf(_loc2_.mediaId) != -1)
            {
               this.viewerUI.showChatBtn = false;
            }
            this.setChatPosition = false;
            this.displayChatReported = false;
         }
         this.onDestroyMedia(_loc2_);
         this.isVideoAdPlaying = false;
         this.echo();
         this.activeChannelInstance = null;
         this.seamlessSwitchDestroyList = [];
      }
      
      private function onLogoCreate(param1:Event) : void {
         this.log("onLogoCreate");
         this._logoPaddingBottom = this._logoPaddingTop = 0;
         Tween.to(this,
            {
               "logoPaddingBottom":this.viewerUI.controlOffset,
               "logoPaddingTop":this.viewerUI.topPanelHeight
            },30 * 0.5,"easeOutQuart");
         var _loc2_:Logo = (param1.target as ViewerModuleManager).logo;
         _loc2_.addEventListener("update",this.onLogoUpdate);
         _loc2_.addEventListener("destroy",this.onLogoDestroy);
         this.seamlessSwitchDestroyList.push(
            {
               "target":_loc2_,
               "listener":this.onLogoDestroy
            });
      }
      
      private function onLogoUpdate(param1:Event) : void {
         this.log("onLogoUpdate");
         var _loc2_:Logo = param1.target as Logo;
         this.viewerUI.hasLogo = _loc2_.onStatusBar;
         if(Capabilities.version.substr(0,3).toUpperCase() == "AND" || Capabilities.version.substr(0,3).toUpperCase() == "GTV")
         {
            this.allowBranding = false;
         }
         else
         {
            this.allowBranding = (_loc2_.onStatusBar) && !_loc2_.logos.length;
         }
         if(!this.logoModuleReady)
         {
            this.logoModuleReady = true;
            if(!this.viewerUI.startMode && !this.streamSizeReported)
            {
               if(this.allowBranding)
               {
                  this.viewerUI.showBranding = true;
               }
               else
               {
                  this.viewerUI.showLoading = true;
               }
            }
         }
      }
      
      private function onLogoDestroy(... rest) : void {
         var _loc2_:Logo = null;
         if((rest.length) && (rest[0]))
         {
            if(rest[0] is Event)
            {
               _loc2_ = rest[0].target as Logo;
            }
            if(rest[0] is Logo)
            {
               _loc2_ = rest[0] as Logo;
            }
         }
         this.log("onLogoDestroy: " + _loc2_);
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",this.onLogoUpdate);
            _loc2_.removeEventListener("destroy",this.onLogoDestroy);
         }
      }
      
      private function reloadStream() : void {
         var _loc1_:String = null;
         var _loc2_:String = null;
         if(Logic.instance.media)
         {
            Logic.reloading = true;
            _loc1_ = Logic.instance.media.mediaId;
            _loc2_ = Logic.instance.media.type;
            if(!(_loc2_ == "channel") && !(Logic.instance.channel == null))
            {
               _loc2_ = "channel";
               _loc1_ = Logic.instance.channel.id;
            }
            if(Logic.instance.media.brandId != "1")
            {
               _loc1_ = Logic.instance.media.brandId + "/" + _loc1_;
            }
            if(_loc2_ == "recorded")
            {
               Logic.instance.recorded.seek(this.viewerUI.intervalStart);
               if(Logic.instance.recorded.autoPlay)
               {
                  Logic.instance.recorded.play();
               }
            }
            else
            {
               Logic.instance.channel.destroy();
               Logic.instance.createChannel(_loc1_,true,null,{"enableStageVideo":this.enableStageVideo});
            }
         }
      }
      
      private function onUILogoClick(param1:Event) : void {
         if((This.getReference(Logic.instance,"media.url")) && !(Logic.instance.media.pageUrl == Logic.instance.media.url))
         {
            gaTrack("Player","Navigate to site","Logo");
            Logic.instance.media.pause();
            navigateToURL(new URLRequest(Logic.instance.media.url),"_blank");
         }
      }
      
      private function onCreateRpinLock(param1:Event = null) : void {
         this.log("onCreateRpinLock");
         this.viewerUI.customBadge = true;
         this.seamlessSwitchDestroyList.push({"listener":this.onDestroyRpinLock});
      }
      
      private function onDestroyRpinLock(param1:Event = null) : void {
         this.log("onDestroyRpinLock");
         this.viewerUI.customBadge = false;
      }
      
      private function onToggleTheaterMode(param1:Event) : void {
         this.log("onToggleTheaterMode");
         if((Shell.hasInstance) && (Shell.jsApiEnabled))
         {
            Shell.instance.dispatch("viewer","toggleTheaterMode");
         }
      }
      
      private function onPopout(param1:Event) : void {
         this.log("onPopout");
         if((Shell.hasInstance) && (Shell.jsApiEnabled))
         {
            Shell.instance.dispatch("viewer","popout");
         }
      }
      
      public function get theaterMode() : Boolean {
         return this.viewerUI.theaterMode;
      }
      
      public function set theaterMode(param1:Boolean) : void {
         this.viewerUI.theaterMode = param1;
      }
      
      private function waitForChannelId(param1:Event) : void {
         if((Logic.hasInstance) && (Logic.instance.media.channelId))
         {
            if(Logic.instance.media.channelId == "12569870" || Logic.instance.media.channelId == "12703625")
            {
            }
            removeEventListener(Event.ENTER_FRAME,this.waitForChannelId);
         }
      }
      
      private function log(param1:String) : void {
         Debug.echo("[ Viewer ] " + param1);
      }
      
      private function onProgressStorageTimer(param1:TimerEvent) : void {
         if(this.shared)
         {
            if((Logic.hasInstance) && (Logic.instance.recorded))
            {
               this.shared.savedProgress = 
                  {
                     "mediaId":Logic.instance.recorded.mediaId,
                     "progress":Logic.instance.recorded.progress
                  };
            }
         }
         else
         {
            this.progressStorageTimer.reset();
         }
      }
   }
}
