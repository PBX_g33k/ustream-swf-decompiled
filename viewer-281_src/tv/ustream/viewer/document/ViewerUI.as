package tv.ustream.viewer.document
{
   import flash.display.Sprite;
   import flash.geom.Rectangle;
   import tv.ustream.tools.This;
   import tv.ustream.tween2.Color;
   import flash.system.Capabilities;
   import flash.system.TouchscreenType;
   import tv.ustream.viewer.document.ui.StreamOverlay;
   import tv.ustream.viewer.document.ui.Button;
   import tv.ustream.viewer.document.ui.SoundBar;
   import tv.ustream.viewer.document.ui.SeekBar;
   import tv.ustream.viewer.document.ui.StatusLabel;
   import tv.ustream.viewer.document.ui.ShareBar;
   import tv.ustream.viewer.document.ui.CCButton;
   import tv.ustream.viewer.document.ui.QualityBar;
   import tv.ustream.viewer.document.ui.ResizeBar;
   import tv.ustream.viewer.document.ui.ScrollingLabel;
   import tv.ustream.viewer.document.ui.StatLabel;
   import tv.ustream.viewer.document.ui.ViewerLabel;
   import tv.ustream.viewer.document.ui.LoaderBar;
   import flash.utils.Timer;
   import tv.ustream.viewer.document.ui.ControlPane;
   import tv.ustream.viewer.document.ui.panes.EmbedPane;
   import tv.ustream.viewer.document.ui.panes.PollPane;
   import tv.ustream.viewer.document.ui.panes.AgePane;
   import tv.ustream.viewer.document.ui.panes.PasswordPane;
   import tv.ustream.viewer.document.ui.panes.Pane;
   import tv.ustream.viewer.document.ui.MidrollNotification;
   import tv.ustream.viewer.document.ui.panes.PpvPane;
   import tv.ustream.viewer.document.ui.FbNotification;
   import tv.ustream.viewer.document.ui.Label;
   import tv.ustream.viewer.document.ui.panes.MultiCamPane;
   import tv.ustream.viewer.document.js.HighlightInterface;
   import tv.ustream.viewer.document.ui.BrandingAnim;
   import tv.ustream.viewer.document.ui.KeyboardNotification;
   import tv.ustream.viewer.document.ui.DvrNotification;
   import flash.events.Event;
   import tv.ustream.viewer.document.tools.MouseWheelTrap;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import tv.ustream.viewer.document.ui.icons.PlayPauseIcon;
   import flash.system.Security;
   import flash.events.FullScreenEvent;
   import tv.ustream.tween2.Tween;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.viewer.document.ui.icons.TimeIcon;
   import tv.ustream.tools.Shell;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.text.TextFormat;
   import tv.ustream.viewer.document.ui.icons.JumpToLiveIcon;
   import tv.ustream.localization.Locale;
   import tv.ustream.viewer.logic.modules.Share;
   import flash.display.StageDisplayState;
   import tv.ustream.viewer.document.ui.icons.PollIcon;
   import tv.ustream.viewer.logic.modules.Poll;
   import tv.ustream.viewer.document.ui.panes.LogPane;
   import tv.ustream.modules.Module;
   import tv.ustream.viewer.document.ui.LargeButton;
   import tv.ustream.viewer.document.ui.icons.MultiCamIcon;
   import tv.ustream.viewer.logic.modules.Ppv;
   import tv.ustream.viewer.logic.modules.RpinLock;
   import flash.filters.DropShadowFilter;
   import flash.geom.Point;
   import flash.geom.Matrix;
   import flash.display.BitmapData;
   import flash.display.GradientType;
   import flash.ui.Mouse;
   import tv.ustream.viewer.document.ui.OptionsPanel;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.StringUtils;
   import tv.ustream.tools.DynamicEvent;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import flash.display.BlendMode;
   import flash.display.Shape;
   import tv.ustream.viewer.document.ui.icons.ChatIcon;
   
   public class ViewerUI extends Sprite
   {
      
      public function ViewerUI() {
         this.notitle = ["12058847","12706821","14878989","16098889"];
         this._streamBounds = new Rectangle();
         this.paneQueue = new Array();
         super();
         if(!instance)
         {
            instance = this;
         }
         mouseEnabled = false;
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
      }
      
      public static var MyriadBoldItalic:Class = ViewerUI_MyriadBoldItalic;
      
      public static var MyriadCondBoldItalic:Class = ViewerUI_MyriadCondBoldItalic;
      
      public static var baseFontName:String = "Arial";
      
      public static var baseFontEmbedded:Boolean = false;
      
      public static const STATUS_NONE:String = "statusNone";
      
      public static const STATUS_LIVE:String = "live";
      
      public static const STATUS_RECORDED:String = "recorded";
      
      public static const STATUS_HIGHLIGHT:String = "highlight";
      
      public static const STATUS_OFFAIR:String = "offair";
      
      public static const STATUS_OFFAIR_CONTENT:String = "offairContent";
      
      public static const STATUS_DVR_LIVE:String = "dvrLive";
      
      public static const STATUS_DVR:String = "dvr";
      
      public static const TOGGLE_PLAYING:String = "togglePlaying";
      
      public static const JUMP_TO_LIVE:String = "jumpToLive";
      
      public static const VOLUME:String = "volume";
      
      public static const SEEK_START_DRAG:String = "seekStartDrag";
      
      public static const SEEK_DRAG:String = "seekDrag";
      
      public static const SEEK_STOP_DRAG:String = "seekStopDrag";
      
      public static const SEEK_SET:String = "seekSet";
      
      public static const MBR_CHANGE:String = "mbrChange";
      
      public static const LOGO_CLICK:String = "logoClick";
      
      public static const CC_CLICK:String = "ccClick";
      
      public static const NOTIFICATION_ALERT:String = "notificationAlert";
      
      public static const NOTIFICATION_SUCCESS:String = "notificationSuccess";
      
      public static const NOTIFICATION_LOAD:String = "notificationLoad";
      
      public static const CONTROL_PLAY:String = "controlPlay";
      
      public static const CONTROL_REPLAY:String = "controlReplay";
      
      public static const CONTROL_CONTINUE:String = "controlContinue";
      
      public static const CONTROL_JUMP_TO_LIVE:String = "jumpToLive";
      
      public static const CONTROL_OFFSET:String = "controlOffset";
      
      public static const MIDROLL_DESTROY:String = "midrollDestroy";
      
      public static const STYLE_CHANGE:String = "styleChange";
      
      public static const TOP_PANEL_HEIGHT_CHANGE:String = "topPanelHeightChange";
      
      public static const CHANNEL_SWITCH:String = "channelSwitch";
      
      public static const TOGGLE_FULL_SCREEN:String = "toggleFullScreen";
      
      public static const TOGGLE_THEATER_MODE:String = "toggleTheaterMode";
      
      public static const POPOUT:String = "popout";
      
      public static var baseH:Number = 32;
      
      public static var ratio:Number = 1;
      
      public static var minPlayerSize:Object = 
         {
            "w":320,
            "h":180 + ViewerUI.baseH
         };
      
      public static var streamBounds:Rectangle;
      
      public static var instance:ViewerUI;
      
      public static var defaultStyle:Object = 
         {
            "color":3377407,
            "linkColor":Color.setBrightness(3377407,0.2)
         };
      
      public static var style:Object = defaultStyle;
      
      public static var touchMode:Boolean = !(Capabilities.touchscreenType == TouchscreenType.NONE);
      
      public static function get pageUrl() : String {
         return (This.referrer) && This.referrer.length > 0?This.referrer:This.pageUrl;
      }
      
      private var notitle:Array;
      
      private const KEYBOARD_SEEK_DIFF:Number = 5;
      
      private var _minSize:Boolean;
      
      private var _totalControlHeight:int = 0;
      
      private var _controlOffset:int = 0;
      
      private var _controlsOpened:int = -1;
      
      private var volumeIsTweening:Boolean;
      
      private var draggingSeek:Boolean;
      
      private var _width:Number = 0;
      
      private var _height:Number = 0;
      
      private var _controlBgTarget;
      
      private var _controlBgTargetLock:Boolean;
      
      private var _controlBgAdded:Boolean;
      
      private var streamOverlay:StreamOverlay;
      
      private var largeControls:Button;
      
      private var controlsWrapper:Sprite;
      
      private var controls:Sprite;
      
      private var basicControlsWrapper:Sprite;
      
      private var basicControls:Sprite;
      
      private var statusBarWrapper:Sprite;
      
      private var statusBar:Sprite;
      
      private var statusInfoHolder:Sprite;
      
      private var ppBtn:Button;
      
      private var jumpToLiveBtn:Button;
      
      private var soundBar:SoundBar;
      
      private var seekBarWrapper:Sprite;
      
      private var seekBar:SeekBar;
      
      private var _intervalStart:Number = 0;
      
      private var _intervalEnd:Number = 1;
      
      private var _intervalLock:Boolean = false;
      
      private var statusLabel:StatusLabel;
      
      private var pollBtn:Button;
      
      private var shareBar:ShareBar;
      
      private var ccButton:CCButton;
      
      private var qualityBar:QualityBar;
      
      private var _qualities:Array;
      
      private var resizeBar:ResizeBar;
      
      private var titleLabel:ScrollingLabel;
      
      private var timeLabel:StatLabel;
      
      private var viewerLabel:ViewerLabel;
      
      private var loaderBar:LoaderBar;
      
      private var idleTimer:Timer;
      
      private var controlling:Boolean;
      
      public var lite:Boolean;
      
      private var _lastVolume:Number = 0;
      
      public var isMuted:Boolean;
      
      private var _status:String;
      
      private var _streamBounds:Rectangle;
      
      private var _showLoading:Boolean;
      
      private var _mbr:Boolean;
      
      private var _hasAudio:Boolean;
      
      private var _hasVideo:Boolean;
      
      public var _hideFeatures:Boolean;
      
      public var _hideShare:Boolean;
      
      private var _startMode:Boolean;
      
      private var startModeSet:Boolean;
      
      private var controlPaneHolder:Sprite;
      
      private var controlPane:ControlPane;
      
      private var paneHolder:Sprite;
      
      private var embedPane:EmbedPane;
      
      private var pollPane:PollPane;
      
      private var agePane:AgePane;
      
      private var passwordPane:PasswordPane;
      
      public var alertPane:Pane;
      
      private var alertPaneTimer:Timer;
      
      private var paneQueue:Array;
      
      private var _videoAdMode:Boolean;
      
      private var midNotificationHolder:Sprite;
      
      private var midNotification:MidrollNotification;
      
      private var adBlockLabel:ScrollingLabel;
      
      private var ppvHolder:Sprite;
      
      private var ppvBgHolder:Sprite;
      
      public var ppvPane:PpvPane;
      
      public var _thumbnailHolder:Sprite;
      
      private var fbNotification:FbNotification;
      
      private var _customBadge:Boolean;
      
      private var customLiveBadge:Label;
      
      private var multiCamBtn:Button;
      
      private var multiCamPane:MultiCamPane;
      
      private var multiCamBlinked:Boolean = false;
      
      private var multiCamTooltipTimer:Timer;
      
      private var qualityBlinked:Boolean = false;
      
      private var highlightInterface:HighlightInterface;
      
      private var brandingAnim:BrandingAnim;
      
      private var _showBranding:Boolean;
      
      private var keyboardNotification:KeyboardNotification;
      
      private var customBtn:Button;
      
      private var customBtnUrl:String;
      
      private var chatBtn:Button;
      
      private var largeChatBtn:Button;
      
      private var _showChatBtn:Boolean;
      
      private var _chatOnTop:Boolean;
      
      private var _chromeless:Boolean;
      
      private var switchAlert:Sprite;
      
      private var _showSwitching:Boolean;
      
      private var dvrNotification:DvrNotification;
      
      private var _alwaysShowFullScreen:Boolean = false;
      
      private var _ladygaga1159:Boolean = false;
      
      public var showtitle:Boolean = true;
      
      private function onAddedToStage(... rest) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         this.buildUI();
         this.resize();
         if(This.onSite(ViewerUI.pageUrl) > 1)
         {
            MouseWheelTrap.setup(stage);
         }
         this.volume = this.soundBar.volume = 0.7;
         this.soundBar.addEventListener(SoundBar.VOLUME_CHANGE,this.onVolume);
         this.soundBar.addEventListener(SoundBar.TOGGLE_MUTE,this.toggleMute);
         this.ppBtn.addEventListener(MouseEvent.CLICK,this.togglePlaying);
         this.idleTimer = new Timer(3000,1);
         this.idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.hideControls);
         this.idleTimer.start();
         if(touchMode)
         {
            stage.addEventListener(MouseEvent.CLICK,this.showControls);
         }
         else
         {
            stage.addEventListener(Event.MOUSE_LEAVE,this.resetIdleTimer);
            stage.addEventListener(MouseEvent.MOUSE_MOVE,this.showControls);
            addEventListener(MouseEvent.MOUSE_OVER,this.handleMouseOnControls);
            addEventListener(MouseEvent.MOUSE_OUT,this.handleMouseOnControls);
         }
         this.status = ViewerUI.STATUS_NONE;
      }
      
      private function buildUI() : void {
         Viewer(parent).brandingAnimHolder.addChild(this.brandingAnim = new BrandingAnim());
         this.brandingAnim.mouseEnabled = this.brandingAnim.mouseChildren = false;
         addChild(this.loaderBar = new LoaderBar());
         addChild(this.midNotificationHolder = new Sprite());
         this.midNotificationHolder.mouseEnabled = false;
         addChild(this.ppvBgHolder = new Sprite());
         this.ppvBgHolder.mouseEnabled = false;
         addChild(this._thumbnailHolder = new Sprite());
         this._thumbnailHolder.visible = false;
         this._thumbnailHolder.mouseEnabled = false;
         addChild(this.ppvHolder = new Sprite());
         this.ppvHolder.mouseEnabled = false;
         this._controlBgTarget = this;
         this.streamOverlay = new StreamOverlay();
         addChild(this.controlsWrapper = new Sprite());
         this.controlsWrapper.mouseEnabled = false;
         addChild(this.basicControlsWrapper = new Sprite());
         this.basicControlsWrapper.mouseEnabled = false;
         this.basicControlsWrapper.addChild(this.basicControls = new Sprite());
         this.basicControls.mouseEnabled = false;
         this.basicControls.addChild(this.soundBar = new SoundBar());
         this.soundBar.alpha = 0;
         this.basicControls.addChild(this.ppBtn = new Button());
         this.ppBtn.icon = new PlayPauseIcon();
         this.ppBtn._hitArea = new Rectangle(0,0,27,26);
         this.ppBtn.alpha = 0;
         this.ppBtn.colorizable = true;
         this.controlsWrapper.addChild(this.controls = new Sprite());
         this.controls.mouseEnabled = false;
         var _loc1_:* = true;
         if(!this._alwaysShowFullScreen && ((loaderInfo.parameters.fullscreen && !(loaderInfo.parameters.fullscreen == "true" || loaderInfo.parameters.fullscreen == "1")) || (stage.hasOwnProperty("allowsFullScreen") && !stage["allowsFullScreen"])))
         {
            _loc1_ = false;
         }
         var _loc2_:Boolean = loaderInfo.parameters.sv == "6" || Security.sandboxType == Security.LOCAL_TRUSTED;
         var _loc3_:Boolean = loaderInfo.parameters.sv == "6" || Security.sandboxType == Security.LOCAL_TRUSTED;
         if(!(This.onSite(ViewerUI.pageUrl) & This.SITE_TYPE_STAGE) && loaderInfo.parameters.cid == "12402722")
         {
            _loc3_ = true;
         }
         if((_loc1_) || (_loc2_) || (_loc3_))
         {
            this.basicControls.addChild(this.resizeBar = new ResizeBar(_loc1_,_loc2_,_loc3_));
            this.resizeBar.addEventListener(ViewerUI.TOGGLE_FULL_SCREEN,this.onToggleFullScreen);
            stage.addEventListener(FullScreenEvent.FULL_SCREEN,this.onFullScreen);
         }
         addChild(this.statusBarWrapper = new Sprite());
         this.statusBarWrapper.mouseEnabled = false;
         addChild(this.seekBarWrapper = new Sprite());
         this.seekBarWrapper.mouseEnabled = false;
         this.statusBarWrapper.addChild(this.statusBar = new Sprite());
         this.statusBar.addChild(this.statusInfoHolder = new Sprite());
         this.statusBar.addChild(this.statusLabel = new StatusLabel());
         this.statusLabel.addEventListener(Event.RESIZE,this.positionStatusBarLabels);
         addChild(this.paneHolder = new Sprite());
         this.paneHolder.mouseEnabled = false;
         addChild(this.keyboardNotification = new KeyboardNotification());
         addChild(this.controlPaneHolder = new Sprite());
         this.controlPaneHolder.mouseEnabled = false;
      }
      
      public function telekomizalas(param1:String, param2:uint = 1296810) : void {
         if(this.soundBar)
         {
            this.soundBar.barColor = param2;
         }
         if(this.statusLabel)
         {
            this.statusLabel.hasBadge = false;
         }
         if(this.notitle.indexOf(param1) == -1)
         {
            this.notitle.push(param1);
         }
         this.title = null;
      }
      
      override public function get width() : Number {
         return this._width;
      }
      
      override public function set width(param1:Number) : void {
         this._width = param1;
         this.resize();
      }
      
      override public function get height() : Number {
         return this._height;
      }
      
      override public function set height(param1:Number) : void {
         this._height = param1;
         this.resize();
      }
      
      public function get volume() : Number {
         return this.soundBar.volume;
      }
      
      public function set volume(param1:Number) : void {
         if((this.soundBar) && !(this.soundBar.volume == param1))
         {
            this.soundBar.volume = param1;
         }
      }
      
      private function toggleMute(param1:Event) : void {
         var e:Event = param1;
         this.volumeIsTweening = true;
         this.isMuted = this.volume?true:false;
         if(this.volume)
         {
            this._lastVolume = this.volume;
            Tween.to(this.soundBar,{"volume":0},30 * 0.3,"easeOutQuart",function():void
            {
               volumeIsTweening = false;
            });
         }
         else
         {
            Tween.to(this.soundBar,{"volume":Math.max(this._lastVolume,0.1)},30 * 0.3,"easeOutQuart",function():void
            {
               volumeIsTweening = false;
            });
         }
      }
      
      private function onVolume(... rest) : void {
         if(this.soundBar.volume == 0)
         {
            this.isMuted = true;
         }
         else
         {
            if((this.isMuted) && this.keyboardNotification.notificationType == KeyboardNotification.MUTE)
            {
               this.keyboardNotification.show(KeyboardNotification.UNMUTE);
            }
            this.isMuted = false;
            if(!this.volumeIsTweening)
            {
               this._lastVolume = this.volume;
            }
         }
         dispatchEvent(new Event(ViewerUI.VOLUME));
      }
      
      public function get playing() : Boolean {
         return this.ppBtn.icon.pauseBtn.visible;
      }
      
      public function set playing(param1:Boolean) : void {
         this.ppBtn.icon.pauseBtn.visible = param1;
         this.ppBtn.icon.playBtn.visible = !param1;
      }
      
      private function togglePlaying(... rest) : void {
         dispatchEvent(new Event(ViewerUI.TOGGLE_PLAYING));
         this.playKeyBoardNotification(Logic.instance.media.playing?KeyboardNotification.PLAY:KeyboardNotification.PAUSE);
         if(this.lite)
         {
            this.resetIdleTimer();
         }
      }
      
      public function playKeyBoardNotification(param1:String) : void {
         this.keyboardNotification.show(param1);
         if((this.lite) && (this._controlsOpened))
         {
            this.resetIdleTimer();
         }
         else
         {
            this.showControls();
         }
      }
      
      private function onJumpToLive(... rest) : void {
         dispatchEvent(new Event(ViewerUI.JUMP_TO_LIVE));
      }
      
      public function get hasAudio() : Boolean {
         return this._hasAudio;
      }
      
      public function set hasAudio(param1:Boolean) : void {
         if((Logic.instance.channel) && Logic.instance.channel.mediaId == "13663180")
         {
            param1 = false;
         }
         this._hasAudio = param1;
         Tween.to(this.soundBar,{"alpha":(this._hasAudio?1:0)},30 * 0.6,"easeOutQuart");
         if(this._hasAudio)
         {
            this.showControls();
         }
      }
      
      public function get hasVideo() : Boolean {
         return this._hasVideo;
      }
      
      public function set hasVideo(param1:Boolean) : void {
         if((Logic.instance.channel) && Logic.instance.channel.mediaId == "13663180")
         {
            param1 = false;
         }
         this._hasVideo = param1;
         Tween.to(this.ppBtn,{"alpha":(this._hasVideo?1:0)},30 * 0.6,"easeOutQuart");
         if(this._hasVideo)
         {
            this.showControls();
         }
      }
      
      public function get hideFeatures() : Boolean {
         return this._hideFeatures;
      }
      
      public function set hideFeatures(param1:Boolean) : void {
         this._hideFeatures = param1;
         if(this._hideFeatures)
         {
            if(this.pollBtn)
            {
               this.removePoll();
            }
            if(this.shareBar)
            {
               this.shareBar.visible = false;
            }
            this.qualities = null;
         }
         else if((this._hideShare) && (this.shareBar))
         {
            this.shareBar.visible = true;
         }
         
         this.resize();
      }
      
      public function get hideShare() : Boolean {
         return this._hideShare;
      }
      
      public function set hideShare(param1:Boolean) : void {
         this._hideShare = param1;
         if(this._hideShare)
         {
            if(this.shareBar)
            {
               this.shareBar.visible = false;
            }
         }
         else if(!this._hideFeatures && (this.shareBar))
         {
            this.shareBar.visible = false;
         }
         
         this.resize();
      }
      
      public function get bufferStart() : Number {
         return this.seekBar?this.seekBar.bufferStart:0;
      }
      
      public function set bufferStart(param1:Number) : void {
         if(this.seekBar)
         {
            this.seekBar.bufferStart = param1;
         }
      }
      
      public function get bufferPercent() : Number {
         return this.seekBar?this.seekBar.bufferPercent:0;
      }
      
      public function set bufferPercent(param1:Number) : void {
         if(this.seekBar)
         {
            this.seekBar.bufferPercent = param1;
         }
      }
      
      public function get duration() : Number {
         return this.seekBar?this.seekBar.duration:0;
      }
      
      public function set duration(param1:Number) : void {
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         var _loc4_:String = null;
         if(!this.timeLabel && param1 > 0 && (!(this._status == STATUS_HIGHLIGHT) || (this._intervalLock)) && !(this._status == STATUS_DVR) && !(this._status == STATUS_LIVE))
         {
            this.statusInfoHolder.addChild(this.timeLabel = new StatLabel(new TimeIcon()));
            this.timeLabel.alpha = 0;
            Tween.to(this.timeLabel,{"alpha":1},30 * 0.6,"easeOutQuart");
            this.resize();
         }
         if(param1 > 0)
         {
            if(!this.seekBar)
            {
               this.seekBarWrapper.addChild(this.seekBar = new SeekBar());
               this.seekBar.duration = param1;
               this.seekBar.intervalEnd = this._intervalEnd;
               this.seekBar.intervalStart = this._intervalStart;
               this.seekBar.intervalLock = this._intervalLock;
               if((Shell.hasInstance) && (Shell.instance.ready))
               {
                  this.initHighlightJs();
               }
               else
               {
                  Shell.callBack = this.initHighlightJs;
               }
               this.seekBar.visible = false;
               if((this.lite) && !this._controlsOpened)
               {
                  this.seekBar.close();
                  this.seekBar.y = baseH * ratio;
               }
            }
            if(!this._startMode && !this._videoAdMode && !this.seekBar.visible && ((Logic.instance.recorded && !(Logic.instance.recorded.progress == -1)) || (This.getReference(Logic.instance,"channel.modules.dvr"))))
            {
               this.seekBar.visible = true;
               this.resize();
               this.showControls();
            }
            if(this.seekBar.duration != param1)
            {
               this.seekBar.duration = param1;
            }
            _loc2_ = param1;
            _loc3_ = _loc2_ * this.seekBar.progress;
            if(this.seekBar.intervalLock)
            {
               _loc2_ = param1 * this.seekBar.intervalEnd - param1 * this.seekBar.intervalStart;
               _loc3_ = _loc2_ * (this.seekBar.progress - this.seekBar.intervalStart) / (this.seekBar.intervalEnd - this.seekBar.intervalStart);
            }
            if(this.timeLabel)
            {
               _loc4_ = this.formatTime(_loc3_,_loc2_) + "  /  " + this.formatTime(_loc2_);
               if(_loc4_ != this.timeLabel.text)
               {
                  this.timeLabel.text = _loc4_;
                  this.positionStatusBarLabels();
               }
            }
         }
      }
      
      public function get progress() : Number {
         return this.seekBar?this.seekBar.progress:0;
      }
      
      public function set progress(param1:Number) : void {
         if((this.seekBar) && (!this.seekBar.isDragging) && !this.seekBar.isTweening)
         {
            this.seekBar.progress = param1;
         }
      }
      
      public function get intervalStart() : Number {
         return this.seekBar?this.seekBar.intervalStart:this._intervalStart;
      }
      
      public function set intervalStart(param1:Number) : void {
         this._intervalStart = param1;
         if(this.seekBar)
         {
            this.seekBar.intervalStart = this._intervalStart;
         }
      }
      
      public function get intervalEnd() : Number {
         return this.seekBar?this.seekBar.intervalEnd:this._intervalEnd;
      }
      
      public function set intervalEnd(param1:Number) : void {
         this._intervalEnd = param1;
         if(this.seekBar)
         {
            this.seekBar.intervalEnd = this._intervalEnd;
         }
      }
      
      public function get intervalLock() : Boolean {
         return this.seekBar?this.seekBar.intervalLock:this._intervalLock;
      }
      
      public function set intervalLock(param1:Boolean) : void {
         this._intervalLock = param1;
         if(this.seekBar)
         {
            this.seekBar.intervalLock = this._intervalLock;
         }
      }
      
      public function get intervalEditable() : Boolean {
         return this.seekBar?this.seekBar.intervalEditable:false;
      }
      
      public function get status() : String {
         return this._status;
      }
      
      public function set status(param1:String) : void {
         if(this._status == param1)
         {
            return;
         }
         this.echo("set status = " + param1);
         this._status = param1;
         if(this.timeLabel)
         {
            this.timeLabel.destroy();
            this.statusInfoHolder.removeChild(this.timeLabel);
            this.timeLabel = null;
         }
         if(this.seekBar)
         {
            this.destroyHighlightJs();
            this.seekBar.destroy();
            this.seekBarWrapper.removeChild(this.seekBar);
            this.seekBar = null;
         }
         this.controls.visible = this.basicControls.visible = true;
         if(this._customBadge)
         {
            this.removeCustomLiveBadge();
         }
         this.addControlBg();
         switch(this._status)
         {
            case ViewerUI.STATUS_RECORDED:
               this.hideViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_OFFAIR_CONTENT:
               this.hideViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_DVR_LIVE:
               this.showViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_LIVE:
               this.showViewerLabel();
               if(this._customBadge)
               {
                  this.addCustomLiveBadge();
               }
               this.showControls();
               break;
            case ViewerUI.STATUS_OFFAIR:
               this.hasAudio = this.hasVideo = false;
               this.hideViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_HIGHLIGHT:
               this.hideViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_DVR:
               this.hideViewerLabel();
               this.showControls();
               break;
            case ViewerUI.STATUS_NONE:
            default:
               this._status = ViewerUI.STATUS_NONE;
               this.controls.visible = this.basicControls.visible = this.hasAudio = this.hasVideo = false;
               this.title = null;
               this.removeControlBg();
               this.hideViewerLabel();
               this.hideControls();
         }
         this.statusLabel.type = this._status;
         dispatchEvent(new Event(CONTROL_OFFSET));
         this.resize();
      }
      
      private function showViewerLabel() : void {
         if(!this.viewerLabel)
         {
            this.statusInfoHolder.addChild(this.viewerLabel = new ViewerLabel());
            this.viewerLabel.alpha = 0;
            Tween.to(this.viewerLabel,{"alpha":1},30 * 0.6,"easeOutQuart");
         }
      }
      
      private function hideViewerLabel() : void {
         if(this.viewerLabel)
         {
            this.viewerLabel.destroy();
            this.statusInfoHolder.removeChild(this.viewerLabel);
            this.viewerLabel = null;
         }
      }
      
      public function set statusBadgeType(param1:String) : void {
         if(this.statusLabel)
         {
            this.statusLabel.type = param1;
         }
      }
      
      private function addControlBg() : void {
         if(!this._chromeless)
         {
            this.echo("addControlBg, target: " + this._controlBgTarget + ", targetLock: " + this._controlBgTargetLock);
            if((this._controlBgTarget is Recorded) && (this._controlBgTarget.hasOwnProperty("addOverlay")) && !this._controlBgTargetLock)
            {
               (this._controlBgTarget as Recorded).addOverlay(this.streamOverlay);
            }
            else if(!contains(this.streamOverlay))
            {
               addChildAt(this.streamOverlay,getChildIndex(this.controlsWrapper));
            }
            
            this._controlBgAdded = true;
         }
      }
      
      private function removeControlBg() : void {
         this.echo("removeControlBg");
         addChildAt(this.streamOverlay,getChildIndex(this.controlsWrapper));
         removeChild(this.streamOverlay);
         this._controlBgAdded = false;
      }
      
      public function set controlBgTarget(param1:*) : void {
         if(!param1)
         {
            this._controlBgTarget = this;
         }
         else
         {
            this._controlBgTarget = param1;
         }
         if(this._controlBgAdded)
         {
            this.addControlBg();
         }
      }
      
      public function get showLoading() : Boolean {
         return this._showLoading;
      }
      
      public function set showLoading(param1:Boolean) : void {
         this.echo("showLoading: " + param1);
         this._showLoading = param1;
         if(this._showLoading)
         {
            this.showBranding = false;
            Tween.to(this.loaderBar,{"alpha":1},30 * 0.3);
         }
         else
         {
            Tween.to(this.loaderBar,{"alpha":0},30 * 0.3);
         }
      }
      
      public function get showBranding() : Boolean {
         return this._showBranding;
      }
      
      public function set showBranding(param1:Boolean) : void {
         this.echo("showBranding: " + param1);
         this._showBranding = param1;
         if(this._showBranding)
         {
            this.showLoading = false;
            if(!this.brandingAnim.running)
            {
               this.brandingAnim.start();
            }
         }
         else
         {
            this.brandingAnim.stop();
            if((!this.keyboardNotification.mutedReported) && (loaderInfo.parameters.hometype) && loaderInfo.parameters.hometype == "new")
            {
               Logic.instance.muted = true;
               if(this.soundBar)
               {
                  this.soundBar.silentVolume = 0;
               }
               this.keyboardNotification.show(KeyboardNotification.MUTE);
            }
         }
      }
      
      public function get qualities() : Array {
         return this._qualities;
      }
      
      public function set qualities(param1:Array) : void {
         if(this._hideFeatures)
         {
            return;
         }
         this._qualities = param1;
         if((this._qualities) && this._qualities.length > 1)
         {
            if(!this.qualityBar)
            {
               this.controls.addChild(this.qualityBar = new QualityBar());
               this.qualityBar.alpha = 0;
               Tween.to(this.qualityBar,{"alpha":1},30 * 0.6,"easeOutQuart");
            }
            this.qualityBar.addQualities(this._qualities);
            this.showControls();
         }
         else if(this.qualityBar)
         {
            this.qualityBar.destroy();
            this.controls.removeChild(this.qualityBar);
            this.qualityBar = null;
            this._qualities = null;
         }
         
         this.resize();
      }
      
      public function blinkQualitySelector() : void {
         if((this.qualityBar && !this.qualityBlinked) && (!this._minSize) && !this._chromeless)
         {
            this.qualityBlinked = true;
            if(!touchMode)
            {
               this.idleTimer.delay = 6000;
            }
            this.showControls();
            if((this.qualityBar.visible && this.paneHolder.numChildren == 0) && (this.controlPaneHolder.numChildren == 0 || this.controlPane && !this.controlPane.active) && (this.seekBar) && (!this.seekBar.dvr))
            {
               this.qualityBar.showTooltip(this._controlsOpened?0:12);
            }
            this.qualityBar.blink();
         }
      }
      
      public function get currentQuality() : uint {
         return !this.qualityBar?0:this.qualityBar.quality;
      }
      
      public function set currentQuality(param1:uint) : void {
         if(this.qualityBar)
         {
            this.qualityBar.quality = param1;
         }
      }
      
      public function get currentAutoQuality() : int {
         return !this.qualityBar?-1:this.qualityBar.currentAutoQuality;
      }
      
      public function set currentAutoQuality(param1:int) : void {
         if(this.qualityBar)
         {
            this.qualityBar.currentAutoQuality = param1;
         }
      }
      
      public function get qualityStatus() : int {
         return !this.qualityBar?0:this.qualityBar.status;
      }
      
      public function set qualityStatus(param1:int) : void {
         if(this.qualityBar)
         {
            this.qualityBar.status = param1;
            if(this.qualityBar.quality != 4096)
            {
               this.showControls();
            }
         }
      }
      
      public function get title() : String {
         return this.titleLabel?this.titleLabel.text:null;
      }
      
      public function set title(param1:String) : void {
         if((This.onSite(This.pageUrl) & This.SITE_TYPE_STAGE) || (!(this.notitle.indexOf(loaderInfo.parameters.cid) == -1)) || (Logic.instance.channel) && (!(this.notitle.indexOf(Logic.instance.channel.id) == -1)))
         {
            if(this.titleLabel)
            {
               this.titleLabel.destroy();
               if(this.statusInfoHolder.contains(this.titleLabel))
               {
                  this.statusInfoHolder.removeChild(this.titleLabel);
               }
               this.titleLabel = null;
            }
            return;
         }
         if((param1) && (this.showtitle))
         {
            if(!this.titleLabel)
            {
               this.statusInfoHolder.addChild(this.titleLabel = new ScrollingLabel(new TextFormat(baseFontName,13,16777215,true),null,false,true,true));
               this.titleLabel.embedFonts = baseFontEmbedded;
               this.titleLabel.overSample = !baseFontEmbedded;
               if(ViewerUI.style.color != ViewerUI.defaultStyle.color)
               {
                  this.titleLabel.textGradientOverlay = [Color.setBrightness(ViewerUI.style.color,0.15),Color.setBrightness(ViewerUI.style.color,-0.15)];
                  this.titleLabel.textGradientOverlayAlpha = [1,1];
               }
               this.titleLabel.autoCollapse = true;
               this.titleLabel.alpha = 0;
               Tween.to(this.titleLabel,{"alpha":1},30 * 0.6,"easeOutQuart");
            }
            if(this.titleLabel.text != param1)
            {
               this.titleLabel.text = param1;
            }
         }
         else if(this.titleLabel)
         {
            this.titleLabel.destroy();
            if(this.statusInfoHolder.contains(this.titleLabel))
            {
               this.statusInfoHolder.removeChild(this.titleLabel);
            }
            this.titleLabel = null;
         }
         
         this.positionStatusBarLabels();
      }
      
      public function setViewerLabel(param1:String, param2:String = null, param3:String = null, param4:String = null) : void {
         if(param2 == "" && param3 == "")
         {
            this.hideViewerLabel();
         }
         else
         {
            this.showViewerLabel();
            if((param1) && (this.viewerLabel) && (!(this.viewerLabel.viewers == param2) || !(this.viewerLabel.viewersTotal == param3)))
            {
               this.viewerLabel.viewers = param2 || "";
               this.viewerLabel.viewersTotal = param3 || "";
               this.viewerLabel.unFormatedViewersTotal = param4 || "";
               this.viewerLabel.text = param1;
               this.positionStatusBarLabels();
            }
         }
      }
      
      private function addControlPane(param1:Boolean = true) : void {
         if(!this.controlPane)
         {
            this.controlPaneHolder.addChild(this.controlPane = new ControlPane());
            this.controlPane.alpha = 0;
            addChild(this.ppvHolder);
            this.resize();
         }
         this.controlPane.closeable = param1;
         Tween.kill(this.controlPane);
         Tween.to(this.controlPane,{"alpha":1},30 * 0.6,"easeOutQuart");
         this.hideControls();
      }
      
      private function removeControlPane() : void {
         if((this.controlPane) && (this.controlPane.closeable))
         {
            this.controlPane.active = false;
            Tween.kill(this.controlPane);
            Tween.to(this.controlPane,{"alpha":0},30 * 0.3,"easeNone",function():void
            {
               addChildAt(ppvHolder,getChildIndex(ppvBgHolder) + 1);
               if((paneQueue) && (paneQueue.length))
               {
                  addPane(paneQueue[0]);
               }
               controlPane.destroy();
               if(controlPaneHolder.contains(controlPane))
               {
                  controlPaneHolder.removeChild(controlPane);
               }
               controlPane = null;
            });
            this.showControls();
         }
      }
      
      public function get startMode() : Boolean {
         return this._startMode;
      }
      
      public function set startMode(param1:Boolean) : void {
         this._startMode = param1;
         if((this._startMode) && (!Logic.instance.seamlessSwitch || (Logic.instance.seamlessSwitch) && !this.startModeSet))
         {
            if(!this.controlPane)
            {
               this.addControlPane();
            }
            this.controlPane.startMode = this._startMode;
            this.startModeSet = true;
         }
         else
         {
            this.removeControlPane();
         }
      }
      
      public function get videoAdMode() : Boolean {
         return this._videoAdMode;
      }
      
      public function set videoAdMode(param1:Boolean) : void {
         var _loc2_:Pane = null;
         this._videoAdMode = param1;
         this.echo("videoAdMode: " + this._videoAdMode);
         if(this._videoAdMode)
         {
            if(this.paneHolder.numChildren)
            {
               _loc2_ = this.paneHolder.getChildAt(0) as Pane;
               this.paneHolder.removeChild(_loc2_);
               if(this.paneQueue.indexOf(_loc2_) == -1)
               {
                  this.paneQueue.unshift(_loc2_);
               }
            }
            this.hideControls();
            this._controlOffset = 0;
            dispatchEvent(new Event(ViewerUI.CONTROL_OFFSET));
            if((this._showBranding) && (this.brandingAnim.running))
            {
               this.brandingAnim.showLoader(false);
            }
            this.showSwitching = false;
         }
         else
         {
            this.showControls();
            this.addNextPane();
            if((this._showBranding) && (this.brandingAnim.running))
            {
               this.brandingAnim.startLoaderTimer();
            }
         }
         this.controls.mouseChildren = this.basicControls.mouseChildren = !this._videoAdMode;
         if(this.seekBar)
         {
            this.seekBar.visible = !this._videoAdMode;
         }
         this.loaderBar.visible = !this._videoAdMode;
         this.brandingAnim.visible = !this._videoAdMode;
         if(this.ppvPane)
         {
            this.ppvPane.videoAdMode = this._videoAdMode;
         }
      }
      
      public function set adBlockStatus(param1:String) : void {
         var str:String = param1;
         if(!str || str == "")
         {
            if(this.adBlockLabel)
            {
               Tween.kill(this.adBlockLabel);
               Tween.to(this.adBlockLabel,
                  {
                     "y":-baseH,
                     "alpha":0
                  },30 * 0.6,"easeInOutQuart",function():void
               {
                  adBlockLabel.destroy();
                  if(statusBar.contains(adBlockLabel))
                  {
                     statusBar.removeChild(adBlockLabel);
                  }
                  adBlockLabel = null;
               },null,function():void
               {
                  adBlockLabel.y = Math.round(adBlockLabel.y);
               });
            }
            Tween.to(this.statusInfoHolder,
               {
                  "y":0,
                  "alpha":1
               },30 * 0.6,"easeInOutQuart",null,null,function():void
            {
               statusInfoHolder.y = Math.round(statusInfoHolder.y);
            });
            if(this.fbNotification)
            {
               Tween.to(this.fbNotification,
                  {
                     "y":0,
                     "alpha":1
                  },30 * 0.6,"easeInOutQuart",null,null,function():void
               {
                  fbNotification.y = Math.round(fbNotification.y);
               });
            }
         }
         else
         {
            if(!this.adBlockLabel)
            {
               this.statusBar.addChild(this.adBlockLabel = new ScrollingLabel(new TextFormat(baseFontName,13,11184810)));
               this.adBlockLabel.embedFonts = baseFontEmbedded;
               this.adBlockLabel.overSample = !baseFontEmbedded;
               this.adBlockLabel.y = -baseH;
               this.adBlockLabel.alpha = 0;
            }
            this.adBlockLabel.text = str;
            Tween.kill(this.adBlockLabel);
            Tween.to(this.adBlockLabel,
               {
                  "y":0,
                  "alpha":1
               },30 * 0.6,"easeInOutQuart",null,null,function():void
            {
               adBlockLabel.y = Math.round(adBlockLabel.y);
            });
            Tween.to(this.statusInfoHolder,
               {
                  "y":baseH,
                  "alpha":0
               },30 * 0.6,"easeInOutQuart",null,null,function():void
            {
               statusInfoHolder.y = Math.round(statusInfoHolder.y);
            });
            if(this.fbNotification)
            {
               Tween.to(this.fbNotification,
                  {
                     "y":baseH,
                     "alpha":0
                  },30 * 0.6,"easeInOutQuart",null,null,function():void
               {
                  fbNotification.y = Math.round(fbNotification.y);
               });
            }
         }
         this.positionStatusBarLabels();
      }
      
      public function createDvr(param1:Number = 0, param2:Number = 0, param3:Number = 0, param4:Boolean = false) : void {
         if(param1 > 0)
         {
            if(param4)
            {
               this.duration = param1;
               this.progress = 1;
               this.showJumpToLiveButton(false);
            }
         }
      }
      
      public function destroyDvr() : void {
         this.duration = 0;
         this.progress = 0;
         this.showJumpToLiveButton(false);
      }
      
      public function updateDvr(param1:Number, param2:Number, param3:Number, param4:Boolean = false, param5:Boolean = false) : void {
         if(param1 > 0)
         {
            if(this.seekBar)
            {
               this.seekBar.showDvrTip();
               this.seekBar.dvr = true;
            }
            if(param3 <= 0 && (param4))
            {
               this.duration = param1;
               this.showJumpToLiveButton(false);
               this.progress = 1;
               this.removeDvrNotification();
            }
            else if(param3 > 0 && (this.status == STATUS_LIVE || this.status == STATUS_DVR))
            {
               this.duration = param1;
               this.progress = 1 - param3 / param1;
               this.showJumpToLiveButton(param4);
               this.removeDvrNotification();
            }
            else if(!param4)
            {
               this.status = STATUS_OFFAIR;
               this.showJumpToLiveButton(false);
            }
            
            
         }
         else
         {
            this.showJumpToLiveButton(false);
         }
      }
      
      private function showJumpToLiveButton(param1:Boolean = true) : void {
         if((param1) && this.jumpToLiveBtn == null)
         {
            this.basicControls.addChild(this.jumpToLiveBtn = new Button());
            this.jumpToLiveBtn.icon = new JumpToLiveIcon();
            this.jumpToLiveBtn.tooltip = Locale.instance.label(ViewerLabels.labelDvrJumpToLive);
            this.jumpToLiveBtn._hitArea = new Rectangle(0,0,27,26);
            this.jumpToLiveBtn.colorizable = true;
            this.jumpToLiveBtn.addEventListener(MouseEvent.CLICK,this.onJumpToLive);
         }
         else if(!param1 && (this.jumpToLiveBtn))
         {
            this.basicControls.removeChild(this.jumpToLiveBtn);
            this.jumpToLiveBtn.removeEventListener(MouseEvent.CLICK,this.onJumpToLive);
            this.jumpToLiveBtn = null;
         }
         
         this.resize();
      }
      
      public function displayDvrNotification(param1:Number) : void {
         if(this.dvrNotification)
         {
            return;
         }
         this.dvrNotification = new DvrNotification(param1);
         this.dvrNotification.addEventListener(DvrNotification.DVR_PLAY,this.onDvrPlay);
         this.midNotificationHolder.addChild(this.dvrNotification);
         this.resize();
      }
      
      private function onDvrPlay(param1:Event) : void {
         dispatchEvent(param1);
      }
      
      public function removeDvrNotification() : void {
         if(this.dvrNotification)
         {
            this.dvrNotification.removeEventListener(DvrNotification.DVR_PLAY,this.onDvrPlay);
            this.midNotificationHolder.removeChild(this.dvrNotification);
            this.dvrNotification = null;
         }
      }
      
      private function updateDvrNotification(param1:Number) : void {
         if(this.dvrNotification)
         {
            if(param1 > 0)
            {
               this.dvrNotification.updateDvrText(param1);
            }
            else
            {
               this.removeDvrNotification();
            }
         }
      }
      
      public function createCC() : void {
         if(this.ccButton)
         {
            return;
         }
         this.ccButton = new CCButton();
         this.ccButton.y = 0 - this.ccButton.height / 2 + 3;
         this.controls.addChild(this.ccButton);
         this.ccButton.addEventListener(MouseEvent.CLICK,this.onCCClick);
         this.ccButton.alpha = 0;
         Tween.to(this.ccButton,{"alpha":1},30 * 0.6,"easeOutQuart");
         this.showControls();
         this.resize();
      }
      
      public function removeCC() : void {
         if(this.ccButton)
         {
            this.ccButton.removeEventListener(MouseEvent.CLICK,this.onCCClick);
            if(this.controls.contains(this.ccButton))
            {
               this.controls.removeChild(this.ccButton);
            }
            this.ccButton = null;
         }
         this.resize();
      }
      
      public function setCCOn(param1:Boolean) : void {
         if(this.ccButton)
         {
            this.ccButton.isCCOn = param1;
         }
      }
      
      private function onCCClick(param1:MouseEvent) : void {
         dispatchEvent(new Event(CC_CLICK));
      }
      
      public function createShare() : void {
         if((this.shareBar) || (this._hideShare))
         {
            return;
         }
         this.controls.addChild(this.shareBar = new ShareBar());
         this.shareBar.addEventListener(ShareBar.EMBEDPANE,this.onEmbedPane);
         this.shareBar.addEventListener(ShareBar.LINK,this.onShareLink);
         this.shareBar.alpha = 0;
         Tween.to(this.shareBar,{"alpha":1},30 * 0.6,"easeOutQuart");
         this.showControls();
         this.resize();
      }
      
      public function get shareModule() : Share {
         return this.shareBar?this.shareBar.module:null;
      }
      
      public function set shareModule(param1:Share) : void {
         if(this.shareBar)
         {
            this.shareBar.module = param1;
         }
      }
      
      public function removeShare() : void {
         if(this.shareBar)
         {
            Tween.to(this.shareBar,{"alpha":0},30 * 0.6,"easeOutQuart");
            this.shareBar.removeEventListener(ShareBar.EMBEDPANE,this.onEmbedPane);
            this.shareBar.removeEventListener(ShareBar.LINK,this.onShareLink);
            this.shareBar.destroy();
            if(this.controls.contains(this.shareBar))
            {
               this.controls.removeChild(this.shareBar);
            }
            this.shareBar = null;
         }
         this.resize();
      }
      
      private function onShareLink(param1:Event) : void {
         this.displayAlert(Locale.instance.label(ViewerLabels.messageMediaUrlCopied),ViewerUI.NOTIFICATION_SUCCESS,3);
      }
      
      public function get streamBounds() : Rectangle {
         return this._streamBounds;
      }
      
      public function set streamBounds(param1:Rectangle) : void {
         this._streamBounds = ViewerUI.streamBounds = param1;
         this.echo("streamBounds: " + param1.toString());
         this.setControlOffset();
         dispatchEvent(new Event(TOP_PANEL_HEIGHT_CHANGE));
         this.resize();
      }
      
      public function get controlOffset() : int {
         return this._controlOffset;
      }
      
      private function setControlOffset() : void {
         if((!this._streamBounds || this._streamBounds.height <= 0 || this._streamBounds.height > this._height) && !This.getReference(Logic,"instance.slideShow"))
         {
            return;
         }
         var _loc1_:int = this._controlOffset;
         var _loc2_:* = 0;
         if(!This.getReference(Logic,"instance.slideShow"))
         {
            _loc2_ = this._streamBounds.height > 0?this._height - (this._streamBounds.y + this._streamBounds.height):0;
         }
         else if(!this._showSwitching && (stage.displayState == StageDisplayState.FULL_SCREEN && this._width == stage.stageWidth && this._height == stage.stageHeight || (this.lite)))
         {
            _loc2_ = 0;
         }
         else
         {
            _loc2_ = baseH;
         }
         
         if(!this._chromeless)
         {
            this._controlOffset = Math.max(0,Math.min(78,this._totalControlHeight - _loc2_));
         }
         else
         {
            this._controlOffset = 0;
         }
         if(!(this._controlOffset == _loc1_) && !this._videoAdMode && !(this._status == STATUS_NONE))
         {
            dispatchEvent(new Event(ViewerUI.CONTROL_OFFSET));
         }
      }
      
      public function get hasLogo() : Boolean {
         return this.statusLabel.hasLogo;
      }
      
      public function set hasLogo(param1:Boolean) : void {
         this.statusLabel.hasLogo = param1;
      }
      
      private function addPane(param1:Pane) : void {
         if(!param1)
         {
            return;
         }
         if(this.paneQueue.indexOf(param1) == -1)
         {
            this.paneQueue.push(param1);
         }
         this.paneQueue.sortOn("priorityId",Array.NUMERIC);
         if(!this._startMode && !this.paneHolder.numChildren && !this._videoAdMode)
         {
            this.addNextPane();
         }
      }
      
      private function addNextPane() : void {
         var _loc1_:Pane = null;
         if(!this.paneQueue.length)
         {
            return;
         }
         if(!this._minSize || _loc1_ == this.passwordPane || _loc1_ == this.alertPane)
         {
            _loc1_ = this.paneQueue[0];
            this.echo("adding pane: " + _loc1_);
            this.paneQueue.splice(0,1);
            this.paneHolder.addChild(_loc1_);
            if(this._showBranding)
            {
               this.showBranding = false;
            }
            this.resize();
         }
      }
      
      private function removePane(param1:Pane) : void {
         this.echo("removing pane: " + param1);
         if(this.paneQueue.indexOf(param1) != -1)
         {
            this.paneQueue.splice(this.paneQueue.indexOf(param1),1);
         }
         if((param1) && (this.paneHolder.contains(param1)))
         {
            this.paneHolder.removeChild(param1);
            if(this.paneQueue.length)
            {
               this.addPane(this.paneQueue[0]);
            }
         }
      }
      
      public function closeAllPanes(param1:Boolean = false, ... rest) : void {
         var _loc4_:Pane = null;
         this.echo("closeAllPanes() - forceAll: " + param1 + (rest.length?", manualOverride: " + rest:""));
         var _loc3_:* = 0;
         while(_loc3_ < this.paneHolder.numChildren)
         {
            _loc4_ = this.paneHolder.getChildAt(_loc3_) as Pane;
            if(!(_loc4_ == this.passwordPane || !param1 && (_loc4_ == this.alertPane || _loc4_ == this.agePane) || (rest.length) && (!(rest.indexOf(_loc4_) == -1))))
            {
               if(this.paneQueue.indexOf(_loc4_) != -1)
               {
                  this.paneQueue.splice(this.paneQueue.indexOf(_loc4_),1);
               }
               if(param1)
               {
                  _loc4_.persist = false;
               }
               _loc4_.close();
            }
            _loc3_++;
         }
      }
      
      private function onEmbedPane(param1:Event) : void {
         if(this.embedPane)
         {
            return;
         }
         this.addPane(this.embedPane = new EmbedPane());
         this.embedPane.addEventListener(Event.CHANGE,this.onSkinUpdate);
         this.embedPane.addEventListener(Event.CLOSE,this.onEmbedPanelClosed);
      }
      
      private function onEmbedPanelClosed(param1:Event) : void {
         if(!this.embedPane)
         {
            return;
         }
         this.removePane(this.embedPane);
         this.embedPane.removeEventListener(Event.CHANGE,this.onSkinUpdate);
         this.embedPane.removeEventListener(Event.CLOSE,this.onEmbedPanelClosed);
         this.embedPane.destroy();
         this.embedPane = null;
      }
      
      public function createPoll() : void {
         if(this.pollPane)
         {
            this.removePollPane();
         }
         if((this.pollBtn) || (this._hideFeatures))
         {
            return;
         }
         this.controls.addChild(this.pollBtn = new Button());
         this.pollBtn.icon = new PollIcon();
         this.pollBtn._hitArea = new Rectangle(0,0,25,25);
         this.pollBtn.colorizable = true;
         if(Locale.instance.label(ViewerLabels.labelPollTooltip).indexOf("!") == -1)
         {
            this.pollBtn.tooltip = Locale.instance.label(ViewerLabels.labelPollTooltip);
         }
         this.pollBtn.addEventListener(MouseEvent.CLICK,this.onPollBtnClick);
         this.resize();
         if(!this._minSize)
         {
            this.pollBtn.alpha = 0;
            Tween.to(this.pollBtn,{"alpha":1},30 * 0.6,"easeOutQuart");
            this.showControls();
         }
         this.addPane(this.pollPane = new PollPane());
         this.pollPane.addEventListener(Event.CLOSE,this.onPollPanelClosed);
      }
      
      public function get pollModule() : Poll {
         return this.pollPane?this.pollPane.module:null;
      }
      
      public function set pollModule(param1:Poll) : void {
         if(this.pollPane)
         {
            this.pollPane.module = param1;
         }
      }
      
      private function onPollBtnClick(param1:MouseEvent) : void {
         this.addPane(this.pollPane);
      }
      
      private function onPollPanelClosed(param1:Event) : void {
         this.removePollPane();
      }
      
      private function removePollPane() : void {
         if(this.pollPane)
         {
            this.removePane(this.pollPane);
            if(!this.pollPane.persist)
            {
               this.echo("destroying PollPane");
               this.pollPane.removeEventListener(Event.CLOSE,this.onPollPanelClosed);
               this.pollPane.destroy();
               this.pollPane = null;
            }
         }
      }
      
      public function removePoll() : void {
         if(this.pollBtn)
         {
            if(this.controls.contains(this.pollBtn))
            {
               this.controls.removeChild(this.pollBtn);
            }
            this.pollBtn.removeEventListener(MouseEvent.CLICK,this.onPollBtnClick);
            this.pollBtn.destroy();
            this.pollBtn = null;
            this.resize();
         }
         if(this.pollPane)
         {
            this.pollPane.persist = false;
            this.pollPane.close();
         }
      }
      
      public function createLogPane(param1:LogPane) : void {
         this.addPane(param1);
      }
      
      public function removeLogPane(param1:LogPane) : void {
         this.removePane(param1);
      }
      
      public function createAge(param1:Module) : void {
         if(this.agePane)
         {
            return;
         }
         this.addPane(this.agePane = new AgePane(param1));
         this.agePane.addEventListener(Event.CLOSE,this.onAgePanelClosed);
      }
      
      public function removeAge() : void {
         if(this.agePane)
         {
            this.agePane.close();
         }
      }
      
      private function onAgePanelClosed(param1:Event = null) : void {
         this.removePane(this.agePane);
         this.agePane.removeEventListener(Event.CLOSE,this.onPollPanelClosed);
         this.agePane.destroy();
         this.agePane = null;
      }
      
      public function createPasswordPane() : void {
         if(!this.passwordPane)
         {
            this.addPane(this.passwordPane = new PasswordPane());
            this.passwordPane.addEventListener(Event.CLOSE,this.onPasswordPaneClose);
         }
         else if(this.paneHolder.contains(this.passwordPane))
         {
            this.passwordPane.open();
         }
         else
         {
            this.addPane(this.passwordPane);
         }
         
      }
      
      public function closePasswordPane() : void {
         if(this.passwordPane)
         {
            this.passwordPane.close();
         }
      }
      
      private function onPasswordPaneClose(param1:Event = null) : void {
         if(!this.passwordPane)
         {
            return;
         }
         this.passwordPane.removeEventListener(Event.CLOSE,this.onPasswordPaneClose);
         this.passwordPane.destroy();
         this.removePane(this.passwordPane);
         this.passwordPane = null;
      }
      
      public function displayAlert(param1:String, param2:String = "notificationAlert", param3:int = 0, param4:String = null) : void {
         var _loc5_:LargeButton = null;
         if(!this.alertPane)
         {
            this.alertPane = new Pane(param3 == 0 || param3 == 2?false:true);
         }
         this.alertPane.notificationType = param2;
         this.alertPane.title = param1;
         this.alertPane.addEventListener(Event.CLOSE,this.onAlertRemoved);
         if(param3 > 1)
         {
            this.alertPane.addEventListener(Event.OPEN,this.onAlertOpened);
         }
         if(param3 == 3)
         {
            this.alertPane.addEventListener(Pane.INIT_CLOSE,this.stopAlertPaneTimer);
         }
         if(param4)
         {
            _loc5_ = new LargeButton();
            _loc5_.text = param4;
            this.alertPane.submitBtn = _loc5_;
         }
         this.addPane(this.alertPane);
      }
      
      private function onAlertOpened(param1:Event) : void {
         if(!this.alertPaneTimer)
         {
            this.alertPaneTimer = new Timer(1500,1);
            this.alertPaneTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.closeAlertPane);
         }
         else
         {
            this.stopAlertPaneTimer();
         }
         this.alertPaneTimer.start();
      }
      
      private function stopAlertPaneTimer(param1:Event = null) : void {
         if(this.alertPaneTimer)
         {
            this.alertPaneTimer.stop();
            this.alertPaneTimer.reset();
         }
      }
      
      public function closeAlertPane(param1:Event = null) : void {
         this.stopAlertPaneTimer();
         if(this.alertPane)
         {
            this.alertPane.close();
         }
      }
      
      private function onAlertRemoved(param1:Event) : void {
         if(this.alertPaneTimer)
         {
            this.stopAlertPaneTimer();
            this.alertPaneTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.closeAlertPane);
            this.alertPaneTimer = null;
         }
         if(this.alertPane)
         {
            this.alertPane.removeEventListener(Event.CLOSE,this.onAlertRemoved);
            this.alertPane.removeEventListener(Event.OPEN,this.onAlertOpened);
            this.alertPane.addEventListener(Pane.INIT_CLOSE,this.stopAlertPaneTimer);
            this.removePane(this.alertPane);
            this.alertPane.destroy();
            this.alertPane = null;
         }
      }
      
      public function createMultiCam() : void {
         if((this.multiCamBtn) || (this._hideFeatures))
         {
            return;
         }
         this.controls.addChild(this.multiCamBtn = new Button());
         this.multiCamBtn.icon = new MultiCamIcon();
         this.multiCamBtn._hitArea = new Rectangle(0,0,25,25);
         this.multiCamBtn.colorizable = true;
         this.multiCamBtn.tooltip = Locale.instance.label(ViewerLabels.labelMultiCamTooltip);
         this.multiCamBtn.addEventListener(MouseEvent.CLICK,this.onMultiCamBtnClick);
         this.multiCamBtn.tooltipInstance.checkOverlap = true;
         this.resize();
         if(!this._minSize)
         {
            this.multiCamBtn.alpha = 0;
            Tween.to(this.multiCamBtn,{"alpha":1},30 * 0.6,"easeOutQuart");
            this.showControls();
         }
      }
      
      private function onMultiCamBtnClick(param1:Event) : void {
         if(!this.multiCamPane)
         {
            this.addPane(this.multiCamPane = new MultiCamPane(Logic.instance.channel.modules.multiCam));
            this.multiCamPane.addEventListener(Event.CLOSE,this.onMultiCamPaneClosed);
         }
         else
         {
            this.addPane(this.multiCamPane);
         }
         if((Logic.hasInstance) && (Logic.instance.channel))
         {
            Viewer.gaTrack("Player","Multicam","Open panel");
         }
      }
      
      public function removeMultiCam() : void {
         if(this.multiCamBtn)
         {
            if(this.controls.contains(this.multiCamBtn))
            {
               this.controls.removeChild(this.multiCamBtn);
            }
            this.multiCamBtn.removeEventListener(MouseEvent.CLICK,this.onMultiCamBtnClick);
            this.multiCamBtn.destroy();
            this.multiCamBtn = null;
            this.resize();
         }
         if(this.multiCamPane)
         {
            this.multiCamPane.persist = false;
            this.multiCamPane.close();
         }
         this.onMultiCamTooltipTimer();
      }
      
      private function onMultiCamPaneClosed(param1:Event) : void {
         if(this.multiCamPane)
         {
            this.removePane(this.multiCamPane);
            if(!this.multiCamPane.persist)
            {
               this.echo("destroying MultiCamPane");
               this.multiCamPane.removeEventListener(Event.CLOSE,this.onMultiCamPaneClosed);
               this.multiCamPane.destroy();
               this.multiCamPane = null;
            }
         }
      }
      
      public function blinkMultiCamBtn(param1:int = 6000) : void {
         if(!(this.multiCamBtn && this.multiCamBtn.visible) || (this.multiCamBlinked) || (this._minSize) || (this._chromeless))
         {
            return;
         }
         if(!touchMode)
         {
            this.idleTimer.delay = param1;
         }
         this.showControls();
         if(this.paneHolder.numChildren == 0 && ((this.controlPaneHolder.numChildren == 0) || (this.controlPane && !this.controlPane.active)))
         {
            if((this.seekBar) && !this.seekBar.dvr)
            {
               this.multiCamBtn.showTooltip(this._controlsOpened?0:12);
               this.multiCamBtn.addEventListener("ButtonHideToolTip",this.onMultiCamButtonToolTipHide);
            }
            else
            {
               dispatchEvent(new Event("MultiCamBtnToolTipDone"));
            }
         }
         this.multiCamBtn.blink();
         this.multiCamBlinked = true;
         if(!this.multiCamTooltipTimer)
         {
            this.multiCamTooltipTimer = new Timer(6000,1);
            this.multiCamTooltipTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onMultiCamTooltipTimer);
            this.multiCamTooltipTimer.start();
         }
      }
      
      private function onMultiCamButtonToolTipHide(param1:Event) : void {
         this.multiCamBtn.removeEventListener("ButtonHideToolTip",this.onMultiCamButtonToolTipHide);
         dispatchEvent(new Event("MultiCamBtnToolTipDone"));
      }
      
      private function onMultiCamTooltipTimer(param1:Event = null) : void {
         if(this.multiCamTooltipTimer)
         {
            this.multiCamTooltipTimer.stop();
            this.multiCamTooltipTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onMultiCamTooltipTimer);
            this.multiCamTooltipTimer = null;
         }
         if(this.multiCamBtn)
         {
            this.multiCamBtn.tooltipInstance.xLabelOffset = 0;
            this.multiCamBtn.hideTooltip();
         }
      }
      
      public function createPpvPane(param1:Ppv) : void {
         if(this.ppvPane)
         {
            return;
         }
         this.ppvHolder.addChild(this.ppvPane = new PpvPane(param1,this.ppvBgHolder));
         this.ppvPane.addEventListener(TOP_PANEL_HEIGHT_CHANGE,this.onTopPanelHeightChange);
         this.ppvPane.addEventListener(Event.CLOSE,this.onPpvPaneClosed);
         this.ppvPane.addEventListener(PpvPane.BG_ADDED,this.onPpvBg);
         this.ppvPane.addEventListener(PpvPane.BG_REMOVED,this.onPpvBg);
         this.resize();
      }
      
      private function onPpvBg(param1:Event) : void {
         this._controlBgTargetLock = param1.type == PpvPane.BG_ADDED;
         if(this._controlBgAdded)
         {
            this.addControlBg();
         }
      }
      
      public function closePpvPane() : void {
         if(this.ppvPane)
         {
            this.ppvPane.close();
         }
      }
      
      private function onPpvPaneClosed(param1:Event) : void {
         if(!this.ppvPane)
         {
            return;
         }
         this.ppvPane.destroy();
         if(this.ppvHolder.contains(this.ppvPane))
         {
            this.ppvHolder.removeChild(this.ppvPane);
         }
         this.ppvPane.removeEventListener(TOP_PANEL_HEIGHT_CHANGE,this.onTopPanelHeightChange);
         this.ppvPane.removeEventListener(Event.CLOSE,this.onPpvPaneClosed);
         this.ppvPane.removeEventListener(PpvPane.BG_ADDED,this.onPpvBg);
         this.ppvPane.removeEventListener(PpvPane.BG_REMOVED,this.onPpvBg);
         this.ppvPane = null;
         dispatchEvent(new Event(TOP_PANEL_HEIGHT_CHANGE));
      }
      
      public function displayMidNotification(param1:uint) : void {
         if(this.midNotification)
         {
            return;
         }
         if(this.dvrNotification)
         {
            this.dvrNotification.visible = false;
         }
         this.midNotificationHolder.addChild(this.midNotification = new MidrollNotification(param1));
         this.midNotification.addEventListener(Event.CLOSE,this.onMidNotifClosed);
         this.resize();
      }
      
      public function closeMidNotification() : void {
         if(this.midNotification)
         {
            this.midNotification.close();
         }
         if(this.dvrNotification)
         {
            this.dvrNotification.visible = true;
         }
      }
      
      private function onMidNotifClosed(param1:Event) : void {
         if(!this.midNotification)
         {
            return;
         }
         this.midNotification.destroy();
         this.midNotificationHolder.removeChild(this.midNotification);
         this.midNotification.removeEventListener(Event.CLOSE,this.onMidNotifClosed);
         this.midNotification = null;
         dispatchEvent(new Event(TOP_PANEL_HEIGHT_CHANGE));
      }
      
      public function set midNotificationOffering(param1:Object) : void {
         if((this.midNotification) && (param1))
         {
            this.midNotification.offering = param1;
         }
      }
      
      public function set midNotificationTime(param1:uint) : void {
         if(this.midNotification)
         {
            this.midNotification.time = param1;
         }
      }
      
      public function createFbNotification(param1:RpinLock) : void {
         if(this.fbNotification)
         {
            return;
         }
         this.echo("createFbNotification");
         this.statusBar.addChild(this.fbNotification = new FbNotification(param1));
         this.fbNotification.addEventListener(Event.RESIZE,this.positionStatusBarLabels);
         this.fbNotification.addEventListener(Event.OPEN,this.onOpenFbNotification);
         this.fbNotification.addEventListener(Event.CLOSE,this.onCloseFbNotification);
         this.positionStatusBarLabels();
      }
      
      public function closeFbNotification() : void {
         if(!this.fbNotification)
         {
            return;
         }
         this.echo("closeFbNotification");
         if(this.statusBar.contains(this.fbNotification))
         {
            this.statusBar.removeChild(this.fbNotification);
         }
         this.fbNotification.dispatchEvent(new Event(Event.CLOSE));
         this.fbNotification.removeEventListener(Event.RESIZE,this.positionStatusBarLabels);
         this.fbNotification.removeEventListener(Event.OPEN,this.onOpenFbNotification);
         this.fbNotification.removeEventListener(Event.CLOSE,this.onCloseFbNotification);
         this.fbNotification.destroy();
         this.fbNotification = null;
         this.positionStatusBarLabels();
      }
      
      private function onOpenFbNotification(param1:Event) : void {
         var e:Event = param1;
         Tween.to(this.statusInfoHolder,
            {
               "y":-baseH,
               "alpha":0
            },30 * 0.6,"easeInOutQuart",null,null,function():void
         {
            statusInfoHolder.y = Math.round(statusInfoHolder.y);
         });
      }
      
      private function onCloseFbNotification(param1:Event) : void {
         var e:Event = param1;
         if(!(this.statusInfoHolder.y == 0) || !(this.statusInfoHolder.alpha == 1))
         {
            Tween.to(this.statusInfoHolder,
               {
                  "y":0,
                  "alpha":1
               },30 * 0.6,"easeInOutQuart",null,null,function():void
            {
               statusInfoHolder.y = Math.round(statusInfoHolder.y);
            });
         }
      }
      
      public function get customBadge() : Boolean {
         return this._customBadge;
      }
      
      public function set customBadge(param1:Boolean) : void {
         if(this._customBadge == param1)
         {
            return;
         }
         this._customBadge = param1;
         this.echo("customBadge: " + this._customBadge);
         this.statusLabel.hasBadge = !this._customBadge;
         if(this._customBadge)
         {
            if(!this.customLiveBadge)
            {
               this.customLiveBadge = new Label();
               this.customLiveBadge.embedFonts = true;
               this.customLiveBadge.textFormat = new TextFormat("MyriadCondBoldItalic",16,16777215);
               this.customLiveBadge.textGradientOverlay = [16777215,0];
               this.customLiveBadge.textGradientOverlayAlpha = [0.19,0.19];
               this.customLiveBadge.textFilters = [new DropShadowFilter(1,90,0,0.29,1,1,1,2)];
               this.customLiveBadge.bgHeight = 25;
               this.customLiveBadge.bgCornerRadius = 4;
               this.customLiveBadge.bgGradientOverlay = [15749440,15277066];
               this.customLiveBadge.bgGradientOverlayAlpha = [1,1];
               this.customLiveBadge.padding = 4;
               this.customLiveBadge.text = "LIVE";
               this.customLiveBadge.txtTarget.y = this.customLiveBadge.txtTarget.y - 1;
            }
            if(this._status == STATUS_LIVE)
            {
               this.addCustomLiveBadge();
            }
         }
         else
         {
            this.removeCustomLiveBadge();
         }
         this.resize();
      }
      
      private function addCustomLiveBadge() : void {
         if(!this.customLiveBadge || (contains(this.customLiveBadge)))
         {
            return;
         }
         addChildAt(this.customLiveBadge,0);
         this.customLiveBadge.alpha = 0;
         Tween.to(this.customLiveBadge,{"alpha":1},30 * 0.2,"easeOutQuart");
      }
      
      private function removeCustomLiveBadge() : void {
         if(!this.customLiveBadge || !contains(this.customLiveBadge))
         {
            return;
         }
         Tween.to(this.customLiveBadge,{"alpha":0},30 * 0.2,"easeOutQuart",function(param1:*):void
         {
            param1.parent.removeChild(param1);
         },[this.customLiveBadge]);
         this.customLiveBadge = null;
      }
      
      private function onTopPanelHeightChange(param1:Event) : void {
         this.midNotificationHolder.y = this.ppvPane?this.ppvPane.panelHeight:0;
      }
      
      public function get topPanelHeight() : Number {
         return (this.midNotification?this.midNotification.panelHeight:0) + (this.ppvPane?this.ppvPane.panelHeight:0);
      }
      
      public function resize(... rest) : void {
         var _loc2_:Number = this._width;
         var _loc3_:Number = this._height;
         if(!isNaN(rest[0]))
         {
            this._width = rest[0];
         }
         if(!isNaN(rest[1]))
         {
            this._height = rest[1];
         }
         ratio = 1;
         if(this._width < minPlayerSize.w || this._height < minPlayerSize.h)
         {
            ratio = Math.max(0.1,Math.min(this._width / minPlayerSize.w,this._height / minPlayerSize.h));
         }
         if((This.onSite(ViewerUI.pageUrl) & This.SITE_TYPE_STAGE) && (!(this._width == _loc2_) || this._width <= minPlayerSize.w || this._height <= minPlayerSize.h))
         {
            this.closeAllPanes();
         }
         var _loc4_:* = 0;
         while(_loc4_ < this.paneHolder.numChildren)
         {
            (this.paneHolder.getChildAt(_loc4_) as Pane).resize(this._width,this._height);
            _loc4_++;
         }
         if(this.ppvPane)
         {
            this.ppvPane.resize(this._width,this._height);
         }
         this.midNotificationHolder.scrollRect = new Rectangle(0,0,this._width,this._height);
         if(this.midNotification)
         {
            this.midNotification.resize(this._width);
         }
         if(this.dvrNotification)
         {
            this.dvrNotification.resize(this._width);
         }
         if(this.controlPane)
         {
            this.controlPane.resize(this._width,this._height);
         }
         if(this.streamOverlay.parent)
         {
            this.streamOverlay.x = this.streamOverlay.parent.globalToLocal(localToGlobal(new Point(0,0))).x;
            this.streamOverlay.y = this.streamOverlay.parent.globalToLocal(localToGlobal(new Point(0,this._height))).y;
         }
         this.streamOverlay.resize(this._width,this._totalControlHeight);
         this.controlsWrapper.y = this.basicControlsWrapper.y = this._height - baseH * ratio - 20 * ratio;
         if((this.seekBar) && (this.seekBar.visible))
         {
            this.controlsWrapper.y = this.controlsWrapper.y - 6 * ratio;
            this.basicControlsWrapper.y = this.controlsWrapper.y;
         }
         this.controls.scaleX = this.controls.scaleY = this.basicControls.scaleX = this.basicControls.scaleY = ratio;
         this.ppBtn.x = 5;
         this.ppBtn.y = -this.ppBtn.height / 2;
         if(this.jumpToLiveBtn)
         {
            this.jumpToLiveBtn.x = this.ppBtn.x + 27;
            this.jumpToLiveBtn.y = -this.jumpToLiveBtn.height / 2;
         }
         this.soundBar.x = this.jumpToLiveBtn?this.jumpToLiveBtn.x + 27:this.ppBtn.x + 27;
         var _loc5_:Number = this._width / ratio;
         if(this.resizeBar)
         {
            _loc5_ = _loc5_ - 4;
            this.resizeBar.x = _loc5_ - this.resizeBar.width;
            _loc5_ = this.resizeBar.x - 1;
            if((this._alwaysShowFullScreen) && (this._width <= minPlayerSize.w || this._height <= minPlayerSize.h))
            {
               this.resizeBar.visible = true;
               this.resizeBar.onlyFullScreen = true;
            }
            else
            {
               this.resizeBar.onlyFullScreen = false;
               this.resizeBar.visible = !(this._width <= minPlayerSize.w || this._height <= minPlayerSize.h);
            }
         }
         else
         {
            _loc5_ = _loc5_ - 8;
         }
         if((This.onSite(ViewerUI.pageUrl) & This.SITE_TYPE_STAGE) && (this._width <= minPlayerSize.w || this._height <= minPlayerSize.h) || (this._hideFeatures))
         {
            this._minSize = true;
            if(this.qualityBar)
            {
               this.qualityBar.visible = false;
            }
            if(this.shareBar)
            {
               this.shareBar.visible = false;
            }
            if(this.pollBtn)
            {
               this.pollBtn.visible = false;
            }
            if(this.multiCamBtn)
            {
               this.multiCamBtn.visible = false;
            }
         }
         else
         {
            this._minSize = false;
            if(this.qualityBar)
            {
               this.qualityBar.visible = true;
            }
            if(!this._hideShare && (this.shareBar))
            {
               this.shareBar.visible = true;
            }
            if(this.pollBtn)
            {
               this.pollBtn.visible = true;
            }
            if(this.multiCamBtn)
            {
               this.multiCamBtn.visible = true;
            }
            if(this.qualityBar)
            {
               this.qualityBar.x = _loc5_ - this.qualityBar.width;
               _loc5_ = this.qualityBar.x - 1;
            }
            if(!this._hideShare && (this.shareBar))
            {
               this.shareBar.x = _loc5_ - this.shareBar.width;
               _loc5_ = this.shareBar.x - 6;
            }
            if(this.pollBtn)
            {
               this.pollBtn.x = _loc5_ - this.pollBtn.width;
               this.pollBtn.y = -Math.floor(this.pollBtn.height / 2) - 1;
               _loc5_ = this.pollBtn.x - 6;
            }
            if(this.multiCamBtn)
            {
               this.multiCamBtn.x = _loc5_ - this.multiCamBtn.width;
               this.multiCamBtn.y = -Math.floor(this.multiCamBtn.height / 2) - 1;
               _loc5_ = this.multiCamBtn.x - 6;
            }
         }
         if(this.ccButton)
         {
            this.ccButton.x = this._minSize?this._width - this.ccButton.width - 6:_loc5_ - this.ccButton.width;
            _loc5_ = this.ccButton.x - 6;
         }
         if((this.chatBtn) && (this.controls.contains(this.chatBtn)))
         {
            this.chatBtn.x = _loc5_ - this.chatBtn.width;
            this.chatBtn.y = -Math.floor(this.chatBtn.height / 2) - 1;
            _loc5_ = this.chatBtn.x - 6;
         }
         if((this.largeChatBtn) && (contains(this.largeChatBtn)))
         {
            this.largeChatBtn.scaleX = this.largeChatBtn.scaleY = ratio;
            this.largeChatBtn.x = this._width - this.largeChatBtn.width - 8 * ratio;
         }
         this.seekBarWrapper.y = this.statusBarWrapper.y = this._height - baseH * ratio;
         if((this.seekBar) && (this.seekBar.visible))
         {
            this.seekBar.width = this._width / ratio;
            this.seekBar.scaleX = this.seekBar.scaleY = ratio;
         }
         var _loc6_:Matrix = new Matrix();
         _loc6_.createGradientBox(this._width,baseH * ratio,90 * Math.PI / 180);
         this.statusBar.graphics.clear();
         this.statusBar.graphics.beginFill(0);
         this.statusBar.graphics.drawRect(0,0,this._width,baseH * ratio);
         this.statusBar.graphics.endFill();
         var _loc7_:BitmapData = new BitmapData(2,2,true,3.42439478E9);
         _loc7_.setPixel32(0,0,3.423605008E9);
         _loc7_.setPixel32(1,1,3.423605008E9);
         this.statusBar.graphics.beginBitmapFill(_loc7_);
         this.statusBar.graphics.drawRect(0,0,this._width,baseH * ratio);
         this.statusBar.graphics.endFill();
         this.statusBar.graphics.beginGradientFill(GradientType.LINEAR,[16777215,0],[0.07,0.07],[0,255],_loc6_);
         this.statusBar.graphics.drawRect(0,0,this._width,baseH * ratio);
         this.statusBar.graphics.endFill();
         this.statusBar.graphics.beginFill(3092527,0.5);
         this.statusBar.graphics.drawRect(0,0,this._width,1);
         this.statusBar.graphics.endFill();
         this.statusBar.scrollRect = new Rectangle(0,0,this._width,baseH * ratio);
         this.statusLabel.scaleX = this.statusLabel.scaleY = ratio;
         this.statusLabel.x = this._width;
         this.statusLabel.y = 0;
         this.positionStatusBarLabels();
         this.loaderBar.ring.x = Math.round(this._width / 2);
         this.loaderBar.ring.y = Math.round((this._height - baseH * ratio) / 2);
         this.loaderBar.ring.scaleX = this.loaderBar.ring.scaleY = ratio;
         var _loc8_:Number = (Math.max(320,Math.min(640,this._width)) - 320) / 320;
         this.brandingAnim.scaleX = this.brandingAnim.scaleY = (0.8 + _loc8_ * 0.2) * ratio;
         this.brandingAnim.x = Math.round(this._width / 2);
         this.brandingAnim.y = Math.round((this._height - baseH * ratio) / 2);
         this.brandingAnim.resize(this._width,this._height - baseH * ratio);
         this.keyboardNotification.x = Math.round(this._width / 2);
         this.keyboardNotification.y = Math.round((this._height - baseH * ratio) / 2);
         this.keyboardNotification.scaleX = this.keyboardNotification.scaleY = (0.6 + _loc8_ * 0.4) * ratio;
         if(this.customLiveBadge)
         {
            this.customLiveBadge.x = Math.round(this._streamBounds.x + 5);
            this.customLiveBadge.y = Math.round(this._streamBounds.y + 5);
         }
         if((this.switchAlert) && (contains(this.switchAlert)))
         {
            this.switchAlert.scaleX = this.switchAlert.scaleY = ratio;
            this.switchAlert.x = 0;
            this.switchAlert.y = this._height - this.switchAlert.height;
         }
         if((this._thumbnailHolder.visible) && (this._thumbnailHolder.width) && (this._thumbnailHolder.height))
         {
            this._thumbnailHolder.height = this._height;
            this._thumbnailHolder.scaleX = this._thumbnailHolder.scaleY;
            if(this._thumbnailHolder.width > this._width)
            {
               this._thumbnailHolder.width = this._width;
               this._thumbnailHolder.scaleY = this._thumbnailHolder.scaleX;
            }
         }
         this._thumbnailHolder.x = (this._width - this._thumbnailHolder.width) / 2;
         this._thumbnailHolder.y = (this._height - this._thumbnailHolder.height) / 2;
      }
      
      private function positionStatusBarLabels(param1:Event = null) : void {
         var _loc3_:* = NaN;
         var _loc2_:Number = this.statusLabel.x - this.statusLabel.width * ratio;
         if(this.fbNotification)
         {
            this.fbNotification.scaleX = this.fbNotification.scaleY = ratio;
            _loc2_ = this.fbNotification.x = _loc2_ - this.fbNotification.width - (!this.statusLabel.hasLogo && (this.statusLabel.hasBadge)?1:0);
         }
         if(this.customBtn)
         {
            this.customBtn.visible = true;
            this.customBtn.y = (baseH * ratio - this.customBtn.height) / 2;
            this.customBtn.scaleX = this.customBtn.scaleY = ratio;
            if(this.hasLogo)
            {
               this.customBtn.x = 8 * ratio;
            }
            else
            {
               this.customBtn.x = _loc2_ = _loc2_ - 8 * ratio - this.customBtn.width;
               if(this.customBtn.x < 8 * ratio)
               {
                  this.customBtn.visible = false;
               }
            }
         }
         _loc2_ = _loc2_ - 16 * ratio;
         if(this.titleLabel)
         {
            this.titleLabel.scaleX = this.titleLabel.scaleY = ratio;
            this.titleLabel.x = ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio;
            if(!this.timeLabel && !this.viewerLabel)
            {
               this.titleLabel.maskWidth = (_loc2_ - this.titleLabel.x) / ratio;
            }
         }
         if(this.timeLabel)
         {
            this.timeLabel.visible = true;
            if(this.titleLabel)
            {
               this.timeLabel.x = this.titleLabel.x + this.titleLabel.textWidth * ratio + 8 * ratio;
            }
            else
            {
               this.timeLabel.x = ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio;
            }
            this.timeLabel.height = baseH;
            this.timeLabel.scaleX = this.timeLabel.scaleY = ratio;
            if(this.timeLabel.x + this.timeLabel.width > _loc2_)
            {
               this.timeLabel.x = Math.max(((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio,_loc2_ - this.timeLabel.width);
            }
            if(this.titleLabel)
            {
               this.titleLabel.maskWidth = (this.timeLabel.x - ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) - 16 * ratio) / ratio;
               if(this.titleLabel.maskWidth == 0)
               {
                  this.timeLabel.x = ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio;
               }
            }
            if(this.timeLabel.x + this.timeLabel.width > _loc2_)
            {
               this.timeLabel.visible = false;
            }
         }
         if((this.customBtn) && (this.hasLogo) && this.customBtn.x + this.customBtn.width > _loc2_)
         {
            this.customBtn.visible = false;
         }
         if((this.viewerLabel) && this.viewerLabel.width > 0)
         {
            this.viewerLabel.visible = true;
            if(this.titleLabel)
            {
               this.viewerLabel.x = this.titleLabel.x + this.titleLabel.textWidth * ratio + 8 * ratio;
            }
            else
            {
               this.viewerLabel.x = ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio;
            }
            this.viewerLabel.height = baseH;
            this.viewerLabel.scaleX = this.viewerLabel.scaleY = ratio;
            if(this.viewerLabel.x + this.viewerLabel.width > _loc2_)
            {
               this.viewerLabel.x = Math.max(((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio,_loc2_ - this.viewerLabel.width);
            }
            if(this.titleLabel)
            {
               this.titleLabel.maskWidth = (this.viewerLabel.x - ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) - 16 * ratio) / ratio;
               if(this.titleLabel.maskWidth == 0)
               {
                  this.viewerLabel.x = ((this.customBtn) && (this.hasLogo)?this.customBtn.x + this.customBtn.width:0) + 8 * ratio;
               }
            }
            this.viewerLabel.availWidth = Math.max(this.viewerLabel.width,_loc2_ - this.viewerLabel.x) / ratio;
            if(this.viewerLabel.x + this.viewerLabel.width > _loc2_ && (this.viewerLabel.formatedLabel))
            {
               this.viewerLabel.visible = false;
            }
            else if(this.viewerLabel.x + this.viewerLabel.unformatedWidth > _loc2_)
            {
               this.viewerLabel.formatedLabel = true;
            }
            else
            {
               this.viewerLabel.formatedLabel = false;
            }
            
         }
         if(this.adBlockLabel)
         {
            this.adBlockLabel.scaleX = this.adBlockLabel.scaleY = ratio;
            this.adBlockLabel.x = 8 * ratio;
            this.adBlockLabel.maskWidth = _loc2_ / ratio;
         }
         if(this.fbNotification)
         {
            _loc3_ = this.fbNotification.x - (this.statusInfoWidth?this.statusInfoWidth + 8 * ratio:0);
            this.fbNotification.availWidth = _loc3_ / ratio > this.fbNotification.labelWidth?this.fbNotification.labelWidth:this.fbNotification.x / ratio;
            if(_loc3_ / ratio < this.fbNotification.labelWidth && (this.fbNotification.opened))
            {
               this.statusInfoHolder.y = -baseH;
               this.statusInfoHolder.alpha = 0;
            }
            if(_loc3_ / ratio >= this.fbNotification.labelWidth && (this.fbNotification.opened))
            {
               this.statusInfoHolder.y = 0;
               this.statusInfoHolder.alpha = 1;
            }
         }
      }
      
      private function get statusInfoWidth() : Number {
         var _loc1_:Number = 0;
         if(this.titleLabel)
         {
            _loc1_ = Math.max(_loc1_,this.titleLabel.x + this.titleLabel.width);
         }
         if(this.viewerLabel)
         {
            _loc1_ = Math.max(_loc1_,this.viewerLabel.x + this.viewerLabel.width);
         }
         if(this.timeLabel)
         {
            _loc1_ = Math.max(_loc1_,this.timeLabel.x + this.timeLabel.width);
         }
         return _loc1_;
      }
      
      private function showControls(param1:Event = null) : void {
         Mouse.show();
         if((this._startMode) || (this._videoAdMode) || (this.lite) && param1 == null)
         {
            return;
         }
         this._totalControlHeight = 40 + ((this.seekBar) && (this.seekBar.visible)?6:0) + baseH;
         this.setControlOffset();
         this.streamOverlay.resize(this._width,this._totalControlHeight);
         if(touchMode)
         {
            this.idleTimer.delay = 8000;
         }
         else if((this.qualityBar) && (!this.qualityBlinked) || (this.multiCamBtn) && (!this.multiCamBlinked))
         {
            this.idleTimer.delay = 8000;
         }
         else if((param1) && param1.type == MouseEvent.MOUSE_MOVE)
         {
            this.idleTimer.delay = 3000;
         }
         
         
         this.resetIdleTimer();
         if(this._controlsOpened == 1)
         {
            return;
         }
         Tween.kill(this.controls);
         Tween.kill(this.basicControls);
         Tween.kill(this.statusBar);
         Tween.to(this.streamOverlay,{"alpha":1},30 * 0.3);
         if(this.seekBar)
         {
            this.seekBar.open();
            Tween.to(this.seekBar,{"y":0},30 * 0.4,"easeOutQuart");
         }
         Tween.to(this.statusBar,{"y":0},30 * 0.4,"easeOutQuart");
         Tween.to(this.controls,
            {
               "y":0,
               "alpha":1,
               "easeParams":[0,30 * 0.6 * 0.9],
               "delay":5
            },30 * 0.6,"easeOutElastic");
         Tween.to(this.basicControls,
            {
               "y":0,
               "alpha":1,
               "easeParams":[0,30 * 0.6 * 0.9],
               "delay":5
            },30 * 0.6,"easeOutElastic");
         if((this.largeChatBtn) && (contains(this.largeChatBtn)))
         {
            Tween.to(this.largeChatBtn,
               {
                  "y":this.topPanelHeight + 8 * ratio,
                  "alpha":1,
                  "delay":6
               },30 * 0.3,"easeOutQuart");
         }
         this._controlsOpened = 1;
      }
      
      private function hideControls(param1:Event = null) : void {
         var e:Event = param1;
         if(this._controlsOpened == 0 || !this._videoAdMode && (Logic.hasInstance) && !Logic.instance.playing && !this.lite)
         {
            return;
         }
         if(this.seekBar)
         {
            this.seekBar.close();
         }
         var i:int = 0;
         while(i < this.controls.numChildren)
         {
            if(this.controls.getChildAt(i) is Button)
            {
               Button(this.controls.getChildAt(i)).hideTooltip();
            }
            i++;
         }
         if(this.qualityBar)
         {
            this.qualityBar.hideTooltip();
         }
         OptionsPanel.closeAll();
         Tween.kill(this.controls);
         Tween.kill(this.basicControls);
         Tween.kill(this.statusBar);
         Tween.to(this.streamOverlay,{"alpha":0},30 * 0.7,"easeInSine");
         Tween.to(this.controls,
            {
               "y":40,
               "alpha":0
            },30 * 0.5,"easeInBack",function():void
         {
            _totalControlHeight = (seekBar) && (seekBar.visible)?3:0;
            if((_showSwitching) || !(stage.displayState == StageDisplayState.FULL_SCREEN && _width == stage.stageWidth && _height == stage.stageHeight) && !lite)
            {
               _totalControlHeight = _totalControlHeight + baseH * ratio;
            }
            setControlOffset();
            if(stage.displayState != StageDisplayState.NORMAL)
            {
               Mouse.hide();
            }
         });
         Tween.to(this.basicControls,
            {
               "y":40,
               "alpha":0
            },30 * 0.5,"easeInBack");
         if((this.largeChatBtn) && (contains(this.largeChatBtn)))
         {
            Tween.to(this.largeChatBtn,
               {
                  "y":this.topPanelHeight + (8 + 10) * ratio,
                  "alpha":0,
                  "delay":15
               },30 * 0.6,"easeOutExpo");
         }
         if(!this._showSwitching && (!(stage.displayState == StageDisplayState.NORMAL) && this._width == stage.stageWidth && this._height == stage.stageHeight || (this.lite)))
         {
            if(this.seekBar)
            {
               Tween.to(this.seekBar,
                  {
                     "y":baseH * ratio,
                     "delay":15
                  },30 * 0.5,"easeOutQuart");
            }
            Tween.to(this.statusBar,
               {
                  "y":baseH * ratio,
                  "delay":15
               },30 * 0.5,"easeOutQuart");
         }
         this._controlsOpened = 0;
      }
      
      private function resetIdleTimer(param1:Event = null) : void {
         if((!touchMode && param1) && (param1.type == Event.MOUSE_LEAVE) && (!this.qualityBar || (this.qualityBlinked)) && (!this.multiCamBtn || (this.multiCamBlinked)))
         {
            this.idleTimer.delay = 1000;
         }
         this.idleTimer.reset();
         if(!this.controlling)
         {
            this.idleTimer.start();
         }
      }
      
      private function handleMouseOnControls(param1:MouseEvent) : void {
         switch(param1.type)
         {
            case MouseEvent.MOUSE_OVER:
               if(param1.target == this.controls || param1.target == this.basicControls || param1.target == this.statusBar || param1.target == this.seekBar || param1.target == this.largeChatBtn || (this._contains(param1.target,this.controls)) || (this._contains(param1.target,this.basicControls)) || (this._contains(param1.target,this.statusBar)) || (this._contains(param1.target,this.seekBar)))
               {
                  this.controlling = true;
                  this.resetIdleTimer();
               }
               break;
            case MouseEvent.MOUSE_OUT:
               if(!(param1.relatedObject == null) && (param1.relatedObject == this.controls || param1.relatedObject == this.basicControls || param1.relatedObject == this.statusBar || param1.relatedObject == this.seekBar || param1.relatedObject == this.largeChatBtn || (this._contains(param1.relatedObject,this.controls)) || (this._contains(param1.relatedObject,this.basicControls)) || (this._contains(param1.relatedObject,this.statusBar)) || (this._contains(param1.relatedObject,this.seekBar))))
               {
                  return;
               }
               this.controlling = false;
               this.resetIdleTimer();
               break;
         }
      }
      
      private function _contains(param1:*, param2:*) : Boolean {
         if(!(param1 is DisplayObject) || !(param2 is DisplayObjectContainer))
         {
            return false;
         }
         if(param2.contains(param1))
         {
            return true;
         }
         var _loc3_:* = 0;
         while(_loc3_ < param2.numChildren)
         {
            if(param2.getChildAt(_loc3_) is DisplayObjectContainer && (this._contains(param1,param2.getChildAt(_loc3_) as DisplayObjectContainer)))
            {
               return true;
            }
            _loc3_++;
         }
         return false;
      }
      
      public function onToggleFullScreen(param1:Event = null) : void {
         var useStageVideo:Boolean = false;
         var e:Event = param1;
         this.echo("onToggleFullScreen");
         var verinfo:String = Capabilities.version;
         var mainVersion:Number = parseInt(verinfo.split(" ")[1].split(",")[0]);
         var subVersion:Number = parseInt(verinfo.split(" ")[1].split(",")[1]);
         if(stage.displayState == StageDisplayState.NORMAL)
         {
            try
            {
               stage.displayState = mainVersion >= 11 && subVersion >= 2?(stage.hasOwnProperty("allowsFullScreenInteractive")) && (stage["allowsFullScreenInteractive"])?"fullScreenInteractive":StageDisplayState.FULL_SCREEN:StageDisplayState.FULL_SCREEN;
               Viewer.gaTrack("Player","Resize","Full screen");
            }
            catch(e:Error)
            {
               echo(e.toString());
               return;
            }
            useStageVideo = false;
            if(mainVersion > 11)
            {
               useStageVideo = false;
            }
            else if(mainVersion == 11 && subVersion >= 7)
            {
               useStageVideo = false;
            }
            else
            {
               useStageVideo = true;
            }
            
            useStageVideo = Capabilities.manufacturer === "Google Pepper" && !useStageVideo?false:true;
            if(!useStageVideo)
            {
               Debug.info("stageVideo is disabled in pepperplayer");
               if((Logic.hasInstance) && (Logic.instance.media))
               {
                  Logic.instance.media.stageVideoDisabled = true;
               }
            }
            stage.addEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
         }
         else
         {
            stage.displayState = StageDisplayState.NORMAL;
            stage.removeEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
         }
         if(stage.displayState == StageDisplayState.NORMAL)
         {
            return;
         }
      }
      
      private function onMouseWheel(param1:MouseEvent) : void {
         this.volume = Math.max(0,Math.min(1,this.volume + param1.delta * 0.05));
      }
      
      private function onFullScreen(param1:FullScreenEvent) : void {
         this.showControls();
      }
      
      private function formatTime(param1:Number, param2:Number = NaN) : String {
         var _loc3_:Date = new Date();
         _loc3_.setTime(param1 * 1000);
         _loc3_.setHours(_loc3_.getUTCHours());
         return StringUtils.getFormatDateString((param2) && (param2 < 3600) || !param2 && param1 < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc3_);
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ UI ] " + param1);
      }
      
      private function onSkinUpdate(param1:DynamicEvent) : void {
         this.updateStyle(param1.data?param1.data.style:null);
      }
      
      public function updateStyle(param1:String = null) : void {
         var _loc3_:String = null;
         var _loc4_:Array = null;
         var _loc5_:String = null;
         var _loc6_:String = null;
         var _loc7_:String = null;
         var _loc8_:String = null;
         var _loc2_:Object = {};
         for(_loc3_ in defaultStyle)
         {
            _loc2_[_loc3_] = defaultStyle[_loc3_];
         }
         if(param1 == "1159")
         {
            this._ladygaga1159 = true;
            _loc2_.controlPanePlaybuttonStyle = "redsquare";
            this._alwaysShowFullScreen = true;
         }
         else if(param1)
         {
            this.echo("updateSkin: style = " + param1);
            _loc4_ = param1.split(":");
            for(_loc5_ in _loc4_)
            {
               _loc6_ = _loc4_[_loc5_].substr(0,2);
               _loc7_ = _loc4_[_loc5_].substr(2);
               if(_loc7_.length == 6)
               {
                  _loc7_ = "0x" + _loc7_;
               }
               if(_loc6_ == "ub")
               {
                  _loc2_.color = Number(_loc7_);
                  _loc2_.linkColor = Color.setBrightness(_loc2_.color,0.2);
                  break;
               }
            }
         }
         else
         {
            this.echo("updateSkin: using defaultStyle");
         }
         
         ViewerUI.style = _loc2_;
         if(this.titleLabel)
         {
            if(ViewerUI.style.color == ViewerUI.defaultStyle.color)
            {
               this.titleLabel.textGradientOverlay = [16777215,0];
               this.titleLabel.textGradientOverlayAlpha = [0.33,0.33];
            }
            else
            {
               this.titleLabel.textGradientOverlay = [Color.setBrightness(ViewerUI.style.color,0.15),Color.setBrightness(ViewerUI.style.color,-0.15)];
               this.titleLabel.textGradientOverlayAlpha = [1,1];
            }
            _loc8_ = this.titleLabel.text;
            this.titleLabel.text = " ";
            this.titleLabel.text = _loc8_;
         }
         dispatchEvent(new Event(STYLE_CHANGE));
      }
      
      private function initHighlightJs() : void {
         if(!this.highlightInterface)
         {
            this.highlightInterface = new HighlightInterface(this.seekBar);
            this.highlightInterface.addEventListener(Event.CHANGE,this.onHighlightInterfaceChange);
         }
         else
         {
            this.highlightInterface.slider = this.seekBar;
         }
      }
      
      private function destroyHighlightJs() : void {
         if(this.highlightInterface)
         {
            this.highlightInterface.removeEventListener(Event.CHANGE,this.onHighlightInterfaceChange);
            this.highlightInterface.slider = null;
            this.highlightInterface = null;
         }
      }
      
      private function onHighlightInterfaceChange(param1:Event) : void {
         if(!this.seekBar.visible)
         {
            this.seekBar.visible = true;
            this.seekBar.alpha = 0;
            Tween.to(this.seekBar,{"alpha":1},30 * 0.6,"easeOutQuart");
            this.resize();
         }
         this.showControls();
      }
      
      public function get theaterMode() : Boolean {
         return this.resizeBar?this.resizeBar.theaterMode:false;
      }
      
      public function set theaterMode(param1:Boolean) : void {
         if(this.resizeBar)
         {
            this.resizeBar.theaterMode = param1;
         }
      }
      
      private function onCustomButtonClick(param1:Event) : void {
         if(stage.displayState != StageDisplayState.NORMAL)
         {
            stage.displayState = StageDisplayState.NORMAL;
         }
         navigateToURL(new URLRequest(this.customBtnUrl),"_blank");
      }
      
      public function addCustomButton(param1:Button, param2:String) : void {
         this.removeCustomBtn();
         this.customBtnUrl = param2;
         this.statusInfoHolder.addChild(this.customBtn = param1);
         this.customBtn.addEventListener(MouseEvent.CLICK,this.onCustomButtonClick);
         this.customBtn.blendMode = BlendMode.LAYER;
         this.customBtn.alpha = 0;
         Tween.to(this.customBtn,{"alpha":1},30 * 0.8,"easeOutQuart");
         this.positionStatusBarLabels();
      }
      
      public function removeCustomBtn() : void {
         if(this.customBtn)
         {
            Tween.kill(this.customBtn);
            this.customBtn.destroy();
            this.customBtn.removeEventListener(MouseEvent.CLICK,this.onCustomButtonClick);
            this.statusInfoHolder.removeChild(this.customBtn);
            this.customBtn = null;
            this.positionStatusBarLabels();
         }
      }
      
      public function setThumbnail(param1:DisplayObject) : void {
         if(param1)
         {
            this._thumbnailHolder.visible = true;
            this._thumbnailHolder.addChildAt(param1,0);
            this.addControlPane(false);
            this.resize();
         }
         else
         {
            this._thumbnailHolder.visible = false;
         }
      }
      
      public function get hasCustomButton() : Boolean {
         return Boolean(this.customBtn);
      }
      
      public function get chatOnTop() : Boolean {
         return this._chatOnTop;
      }
      
      public function set chatOnTop(param1:Boolean) : void {
         this._chatOnTop = param1;
      }
      
      public function get showChatBtn() : Boolean {
         return this._showChatBtn;
      }
      
      public function set showChatBtn(param1:Boolean) : void {
         var _loc2_:TextFormat = null;
         var _loc3_:Shape = null;
         this.echo("showChatBtn: " + param1);
         this._showChatBtn = param1;
         this.removeChatBtn();
         if(this._showChatBtn)
         {
            if(!this._chatOnTop)
            {
               if(!this.chatBtn)
               {
                  this.chatBtn = new Button();
                  this.chatBtn.icon = new ChatIcon();
                  this.chatBtn.icon.shape.width = 20.5;
                  this.chatBtn.icon.shape.scaleY = this.chatBtn.icon.shape.scaleX;
                  this.chatBtn._hitArea = new Rectangle(0,0,22,21);
                  this.chatBtn.tooltip = "Live chat";
                  this.chatBtn.addEventListener(MouseEvent.CLICK,this.onChatBtnClick);
                  this.chatBtn.alpha = 0;
               }
               if(!this.controls.contains(this.chatBtn))
               {
                  this.controls.addChild(this.chatBtn);
               }
               Tween.kill(this.chatBtn);
               Tween.to(this.chatBtn,{"alpha":1},30 * 0.6,"easeOutQuart");
            }
            else
            {
               if(!this.largeChatBtn)
               {
                  this.largeChatBtn = new Button();
                  this.largeChatBtn.blendMode = BlendMode.LAYER;
                  this.largeChatBtn.noGlow = true;
                  this.largeChatBtn.graphics.beginFill(0,0.75);
                  this.largeChatBtn.graphics.drawRoundRect(0,0,55,45,6);
                  this.largeChatBtn.graphics.endFill();
                  this.largeChatBtn.icon = new ChatIcon();
                  this.largeChatBtn.icon.shape.overlay.visible = false;
                  this.largeChatBtn.icon.filters = [];
                  this.largeChatBtn.icon.x = 55 / 2;
                  this.largeChatBtn.icon.y = 15;
                  _loc2_ = new TextFormat("MyriadCondBoldItalic",12,16777215);
                  this.largeChatBtn.embedFonts = true;
                  this.largeChatBtn.textFormat = _loc2_;
                  this.largeChatBtn.label = "LIVE CHAT";
                  this.largeChatBtn.txtTarget.alpha = 0.8;
                  this.largeChatBtn.txtTarget.x = Math.round((55 - this.largeChatBtn.txtTarget.width) / 2) - 1;
                  this.largeChatBtn.txtTarget.y = 45 - this.largeChatBtn.txtTarget.height - 2;
                  _loc3_ = new Shape();
                  _loc3_.graphics.beginFill(ViewerUI.style.color);
                  _loc3_.graphics.drawRoundRect(0,0,55,45,6);
                  _loc3_.graphics.endFill();
                  _loc3_.alpha = 0;
                  _loc3_.name = "hover";
                  this.largeChatBtn.addChildAt(_loc3_,0);
                  this.largeChatBtn.addEventListener(MouseEvent.CLICK,this.onChatBtnClick);
                  this.largeChatBtn.addEventListener(MouseEvent.MOUSE_OVER,this.onChatBtnOver);
                  this.largeChatBtn.addEventListener(MouseEvent.MOUSE_OUT,this.onChatBtnOut);
                  this.largeChatBtn.y = this.topPanelHeight + (8 + 10) * ratio;
                  this.largeChatBtn.alpha = 0;
               }
               if(!contains(this.largeChatBtn))
               {
                  addChildAt(this.largeChatBtn,getChildIndex(this.controlsWrapper));
               }
               if(!this._startMode && !this._videoAdMode)
               {
                  Tween.kill(this.largeChatBtn);
                  Tween.to(this.largeChatBtn,
                     {
                        "y":this.topPanelHeight + 8 * ratio,
                        "alpha":1
                     },30 * 0.3,"easeOutQuart");
               }
            }
            this.resize();
            this.showControls();
         }
      }
      
      private function removeChatBtn() : void {
         if(this.largeChatBtn)
         {
            Tween.kill(this.largeChatBtn);
            Tween.to(this.largeChatBtn,{"alpha":0},30 * 0.6,"easeOutQuart",function():void
            {
               if(contains(largeChatBtn))
               {
                  removeChild(largeChatBtn);
               }
            });
         }
         if(this.chatBtn)
         {
            Tween.kill(this.chatBtn);
            Tween.to(this.chatBtn,{"alpha":0},30 * 0.6,"easeOutQuart",function():void
            {
               if(controls.contains(chatBtn))
               {
                  controls.removeChild(chatBtn);
               }
            });
         }
         this.resize();
      }
      
      private function onChatBtnOver(param1:Event = null) : void {
         Tween.to(this.largeChatBtn.getChildByName("hover"),{"alpha":1},30 * 0.2,"easeOutExpo");
         Tween.to(this.largeChatBtn.txtTarget,{"alpha":1},30 * 0.2,"easeOutExpo");
      }
      
      private function onChatBtnOut(param1:Event = null) : void {
         Tween.to(this.largeChatBtn.getChildByName("hover"),{"alpha":0},30 * 0.8,"easeOutQuart");
         Tween.to(this.largeChatBtn.txtTarget,{"alpha":0.8},30 * 0.8,"easeOutQuart");
      }
      
      private function onChatBtnClick(param1:Event = null) : void {
         if(stage.displayState != StageDisplayState.NORMAL)
         {
            stage.displayState = StageDisplayState.NORMAL;
         }
         if(param1.currentTarget == this.largeChatBtn)
         {
            Viewer.gaTrack("Player","Chat icon","Click on top - " + Logic.instance.channel.mediaId);
         }
         else
         {
            Viewer.gaTrack("Player","Chat icon","Click in controlbar - " + Logic.instance.channel.mediaId);
         }
         Logic.instance.channel.pause();
         var _loc2_:String = "http://www.ustream.tv/channel/" + Logic.instance.channel.mediaId;
         _loc2_ = _loc2_ + ("?utm_source=" + encodeURIComponent(This.pageUrl));
         _loc2_ = _loc2_ + "&utm_medium=Embed";
         _loc2_ = _loc2_ + "&utm_campaign=Chat%2Bicon";
         this.echo("chatBtn redirect url: " + _loc2_);
         try
         {
            navigateToURL(new URLRequest(_loc2_),"_blank");
         }
         catch(e:*)
         {
         }
      }
      
      public function get chromeless() : Boolean {
         return this._chromeless;
      }
      
      public function set chromeless(param1:Boolean) : void {
         this.echo("set chromeless: " + param1);
         this._chromeless = param1;
         if(this._chromeless)
         {
            this.removeControlBg();
            baseH = 0;
            this.controlsWrapper.visible = false;
            this.basicControlsWrapper.visible = false;
            this.statusBarWrapper.visible = false;
            this.seekBarWrapper.visible = false;
         }
         else
         {
            baseH = 32;
            this.controlsWrapper.visible = true;
            this.basicControlsWrapper.visible = true;
            this.statusBarWrapper.visible = true;
            this.seekBarWrapper.visible = true;
         }
      }
      
      public function set hideUIOnSeamlessSwitch(param1:Boolean) : void {
         if(param1)
         {
            this._status = null;
            this.statusLabel.type = STATUS_NONE;
         }
         var _loc2_:Array = [this.controlsWrapper,this.statusInfoHolder,this.seekBarWrapper,this.midNotificationHolder,this.ppvHolder,(parent as Viewer).brandingAnimHolder];
         var _loc3_:* = 0;
         _loc3_ = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(param1)
            {
               Tween.kill(_loc2_[_loc3_]);
               _loc2_[_loc3_].visible = false;
               _loc2_[_loc3_].alpha = 0;
            }
            else
            {
               _loc2_[_loc3_].visible = true;
               Tween.to(_loc2_[_loc3_],{"alpha":1},30 * 0.8,"easeOutQuart");
            }
            _loc3_++;
         }
      }
      
      public function set showSwitching(param1:Boolean) : void {
         var label:Label = null;
         var _height:Number = NaN;
         var loader:LoaderBar = null;
         var _width:Number = NaN;
         var value:Boolean = param1;
         this._showSwitching = value;
         if(this._showSwitching)
         {
            if(!this.switchAlert)
            {
               this.switchAlert = new Label();
               this.switchAlert.blendMode = BlendMode.LAYER;
               this.switchAlert.mouseEnabled = this.switchAlert.mouseChildren = false;
               label = new Label();
               label.textFormat = new TextFormat(baseFontName,13,16777215);
               label.embedFonts = baseFontEmbedded;
               label.overSample = !baseFontEmbedded;
               label.filters = [new DropShadowFilter(1,90,0,0.75,2,2,1,2)];
               label.textGradientOverlay = [16777215,0];
               label.textGradientOverlayAlpha = [0.33,0.33];
               label.text = Locale.instance.label(ViewerLabels.labelMultiCamSwitching);
               _height = 32;
               loader = new LoaderBar(8,2);
               loader.alpha = 1;
               loader.visible = false;
               loader.x = loader.y = _height / 2;
               loader.x = loader.x + 1;
               loader.y = loader.y - 1;
               label.x = loader.x + loader.width / 2 + 7;
               label.y = Math.round((_height - label.height) / 2) - 1;
               this.switchAlert.addChild(label);
               this.switchAlert.addChild(loader);
               _width = label.x + label.width + 8;
               this.switchAlert.graphics.beginFill(0,0);
               this.switchAlert.graphics.drawRoundRect(0,0,_width,_height,6);
               this.switchAlert.graphics.endFill();
            }
            if(!contains(this.switchAlert))
            {
               this.switchAlert.getChildAt(1).visible = true;
               addChild(this.switchAlert);
            }
            Tween.kill(this.switchAlert);
            Tween.to(this.switchAlert,{"alpha":1},30 * 0.4,"easeOutQuart");
            this.resize();
         }
         else if(this.switchAlert)
         {
            Tween.kill(this.switchAlert);
            Tween.to(this.switchAlert,{"alpha":0},30 * 0.8,"easeOutQuart",function():void
            {
               if(contains(switchAlert))
               {
                  switchAlert.getChildAt(1).visible = false;
                  removeChild(switchAlert);
               }
            });
         }
         
      }
      
      public function updateResizeBar(param1:String) : void {
         var _loc2_:ResizeBar = null;
         if(!this._ladygaga1159 || !this.resizeBar)
         {
            return;
         }
         this.echo("updateResizeBar: " + param1);
         if(param1 == "45426601" || param1 == "43186804")
         {
            _loc2_ = new ResizeBar(true,false,true);
         }
         else
         {
            _loc2_ = new ResizeBar(true,true,true);
         }
         var _loc3_:int = this.basicControls.getChildIndex(this.resizeBar);
         this.basicControls.removeChild(this.resizeBar);
         this.resizeBar.removeEventListener(ViewerUI.TOGGLE_FULL_SCREEN,this.onToggleFullScreen);
         this.resizeBar = _loc2_;
         this.resizeBar.addEventListener(ViewerUI.TOGGLE_FULL_SCREEN,this.onToggleFullScreen);
         this.basicControls.addChildAt(this.resizeBar,_loc3_);
      }
   }
}
