package tv.ustream.tween2
{
   import flash.display.DisplayObject;
   import flash.geom.ColorTransform;
   
   public class Color extends Object
   {
      
      public function Color() {
         super();
      }
      
      public static function tint(param1:DisplayObject, param2:uint, param3:Number) : void {
         var _loc4_:Number = 1 - param3;
         var _loc5_:Number = Math.round(param3 * extractRed(param2));
         var _loc6_:Number = Math.round(param3 * extractGreen(param2));
         var _loc7_:Number = Math.round(param3 * extractBlue(param2));
         var _loc8_:ColorTransform = param3 <= 0?new ColorTransform():new ColorTransform(_loc4_,_loc4_,_loc4_,1,_loc5_,_loc6_,_loc7_,0);
         param1.transform.colorTransform = _loc8_;
      }
      
      public static function extractRed(param1:uint) : uint {
         return param1 >> 16 & 255;
      }
      
      public static function extractGreen(param1:uint) : uint {
         return param1 >> 8 & 255;
      }
      
      public static function extractBlue(param1:uint) : uint {
         return param1 & 255;
      }
      
      public static function hexToRGB(param1:uint) : Object {
         var _loc2_:Object = 
            {
               "r":(param1 & 16711680) >> 16,
               "g":(param1 & 65280) >> 8,
               "b":param1 & 255
            };
         return _loc2_;
      }
      
      public static function rgbToHex(param1:int, param2:int, param3:int) : uint {
         var _loc4_:uint = param1 << 16 | param2 << 8 | param3;
         return _loc4_;
      }
      
      public static function setBrightness(param1:uint, param2:Number) : uint {
         var _loc4_:String = null;
         var _loc3_:Object = hexToRGB(param1);
         for(_loc4_ in _loc3_)
         {
            _loc3_[_loc4_] = _loc3_[_loc4_] + (param2 > 0?(255 - _loc3_[_loc4_]) * param2:_loc3_[_loc4_] * param2);
         }
         return rgbToHex(_loc3_.r,_loc3_.g,_loc3_.b);
      }
   }
}
