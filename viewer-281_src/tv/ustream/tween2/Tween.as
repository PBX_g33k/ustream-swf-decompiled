package tv.ustream.tween2
{
   import flash.utils.Dictionary;
   import flash.display.Sprite;
   import flash.events.Event;
   
   public class Tween extends Object
   {
      
      public function Tween() {
         super();
      }
      
      private static var targets:Dictionary = new Dictionary(true);
      
      private static var frame:Sprite = new Sprite();
      
      public static function to(param1:Object, param2:Object, param3:Number = 30, param4:String = "easeNone", param5:Function = null, param6:Array = null, param7:Function = null, param8:Array = null) : void {
         var _loc9_:String = null;
         if(!targets[param1])
         {
            targets[param1] = {};
         }
         targets[param1]["fn"] = 
            {
               "cb":param5,
               "cbParams":param6 || [],
               "update":param7,
               "updateParams":param8 || []
            };
         for(_loc9_ in param2)
         {
            if(!(_loc9_ == "easeParams" || _loc9_ == "delay"))
            {
               if(param1.hasOwnProperty(_loc9_))
               {
                  targets[param1][_loc9_] = 
                     {
                        "delay":int(param2.delay || 0),
                        "time":0,
                        "begin":param1[_loc9_],
                        "change":param2[_loc9_] - param1[_loc9_],
                        "duration":param3,
                        "ease":param4,
                        "easeParams":param2.easeParams || []
                     };
               }
               else
               {
                  delete param2[_loc9_];
               }
            }
         }
         if(!frame.hasEventListener(Event.ENTER_FRAME))
         {
            frame.addEventListener(Event.ENTER_FRAME,onStaticEnterFrame);
         }
      }
      
      public static function kill(param1:Object) : void {
         if(targets[param1])
         {
            delete targets[param1];
         }
      }
      
      private static function onStaticEnterFrame(... rest) : void {
         var _loc4_:* = undefined;
         var _loc5_:* = undefined;
         var _loc6_:String = null;
         var _loc7_:Array = null;
         var _loc8_:Function = null;
         var _loc9_:Object = null;
         var _loc10_:Array = null;
         var _loc2_:Number = 0;
         var _loc3_:Number = 0;
         for(_loc4_ in targets)
         {
            _loc3_ = 0;
            for(_loc6_ in targets[_loc4_])
            {
               if(_loc6_ != "fn")
               {
                  _loc3_++;
                  if(targets[_loc4_][_loc6_].delay > 0)
                  {
                     targets[_loc4_][_loc6_].delay--;
                  }
                  else
                  {
                     _loc7_ = [targets[_loc4_][_loc6_].time,targets[_loc4_][_loc6_].begin,targets[_loc4_][_loc6_].change,targets[_loc4_][_loc6_].duration];
                     if(targets[_loc4_][_loc6_].ease.indexOf("Back") != -1)
                     {
                        _loc7_.push(targets[_loc4_][_loc6_].easeParams[0]);
                     }
                     if(targets[_loc4_][_loc6_].ease.indexOf("Elastic") != -1)
                     {
                        _loc7_.push(targets[_loc4_][_loc6_].easeParams[0],targets[_loc4_][_loc6_].easeParams[1]);
                     }
                     _loc4_[_loc6_] = Equations[targets[_loc4_][_loc6_].ease].apply(Number.NaN,_loc7_);
                     targets[_loc4_][_loc6_].time++;
                     if(targets[_loc4_][_loc6_].time > targets[_loc4_][_loc6_].duration)
                     {
                        delete targets[_loc4_][_loc6_];
                     }
                  }
               }
            }
            if(targets[_loc4_].fn.update)
            {
               targets[_loc4_].fn.update.apply(targets[_loc4_],targets[_loc4_].fn.updateParams);
            }
            if(!_loc3_)
            {
               if(targets[_loc4_].fn.cb)
               {
                  _loc8_ = targets[_loc4_].fn.cb;
                  _loc9_ = targets[_loc4_];
                  _loc10_ = targets[_loc4_].fn.cbParams;
                  delete targets[_loc4_];
                  _loc8_.apply(_loc9_,_loc10_);
               }
               else
               {
                  delete targets[_loc4_];
               }
            }
         }
         for(_loc5_ in targets)
         {
            _loc2_++;
         }
         if(!_loc2_)
         {
            frame.removeEventListener(Event.ENTER_FRAME,onStaticEnterFrame);
         }
      }
   }
}
