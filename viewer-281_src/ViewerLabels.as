package 
{
   public class ViewerLabels extends Object
   {
      
      public function ViewerLabels() {
         super();
      }
      
      public static const labelAccepted:String = "labelAccepted";
      
      public static const labelAdBlockRemaining:String = "labelAdBlockRemaining";
      
      public static const labelAdBlockResuming:String = "labelAdBlockResuming";
      
      public static const labelAddToWatchlist:String = "labelAddToWatchlist";
      
      public static const labelAttending:String = "labelAttending";
      
      public static const labelAudio:String = "labelAudio";
      
      public static const labelAudioQuality:String = "labelAudioQuality";
      
      public static const labelAudioSource:String = "labelAudioSource";
      
      public static const labelBackToMenu:String = "labelBackToMenu";
      
      public static const labelBroadcast:String = "labelBroadcast";
      
      public static const labelBuyTicket:String = "labelBuyTicket";
      
      public static const labelCancel:String = "labelCancel";
      
      public static const labelCategory:String = "labelCategory";
      
      public static const labelChat:String = "labelChat";
      
      public static const labelChatUrlCopied:String = "labelChatUrlCopied";
      
      public static const labelClickHere:String = "labelClickHere";
      
      public static const labelCloseWindow:String = "labelCloseWindow";
      
      public static const labelCohost:String = "labelCohost";
      
      public static const labelCohostEnded:String = "labelCohostEnded";
      
      public static const labelCohostSettings:String = "labelCohostSettings";
      
      public static const labelCopy:String = "labelCopy";
      
      public static const labelCurrentViewers:String = "labelCurrentViewers";
      
      public static const labelCustom:String = "labelCustom";
      
      public static const labelCustomEmbed:String = "labelCustomEmbed";
      
      public static const labelDay:String = "labelDay";
      
      public static const labelDigg:String = "labelDigg";
      
      public static const labelEmbed:String = "labelEmbed";
      
      public static const labelEmbedCode:String = "labelEmbedCode";
      
      public static const labelEnter:String = "labelEnter";
      
      public static const labelFacebook:String = "labelFacebook";
      
      public static const labelFind:String = "labelFind";
      
      public static const labelFollow:String = "labelFollow";
      
      public static const labelFound:String = "labelFound";
      
      public static const labelGoToShowPage:String = "labelGoToShowPage";
      
      public static const labelGoToVideoPage:String = "labelGoToVideoPage";
      
      public static const labelHideOptions:String = "labelHideOptions";
      
      public static const labelHighlightLink:String = "labelHighlightLink";
      
      public static const labelIdLikeToCohost:String = "labelIdLikeToCohost";
      
      public static const labelInformation:String = "labelInformation";
      
      public static const labelInvalidParameter:String = "labelInvalidParameter";
      
      public static const labelJoinAsCohost:String = "labelJoinAsCohost";
      
      public static const labelKeepWatching:String = "labelKeepWatching";
      
      public static const labelLiveOnUstream:String = "labelLiveOnUstream";
      
      public static const labelLivePoll:String = "labelLivePoll";
      
      public static const labelMediaStream:String = "labelMediaStream";
      
      public static const labelMenu:String = "labelMenu";
      
      public static const labelMidNotification:String = "labelMidNotification";
      
      public static const labelMidNotificationAd:String = "labelMidNotificationAd";
      
      public static const labelMidNotificationAdLink:String = "labelMidNotificationAdLink";
      
      public static const labelMonth:String = "labelMonth";
      
      public static const labelMute:String = "labelMute";
      
      public static const labelName:String = "labelName";
      
      public static const labelNextSlide:String = "labelNextSlide";
      
      public static const labelNoThanks:String = "labelNoThanks";
      
      public static const labelOffAir:String = "labelOffAir";
      
      public static const labelOk:String = "labelOk";
      
      public static const labelOnAir:String = "labelOnAir";
      
      public static const labelPassword:String = "labelPassword";
      
      public static const labelPause:String = "labelPause";
      
      public static const labelPending:String = "labelPending";
      
      public static const labelPlay:String = "labelPlay";
      
      public static const labelPlayed:String = "labelPlayed";
      
      public static const labelPoll:String = "labelPoll";
      
      public static const labelPreview:String = "labelPreview";
      
      public static const labelPreviousSlide:String = "labelPreviousSlide";
      
      public static const labelProgressive:String = "labelProgressive";
      
      public static const labelQuit:String = "labelQuit";
      
      public static const labelQualityAuto:String = "labelQualityAuto";
      
      public static const labelQualityHD:String = "labelQualityHD";
      
      public static const labelQualityHigh:String = "labelQualityHigh";
      
      public static const labelQualityLow:String = "labelQualityLow";
      
      public static const labelQualityMedium:String = "labelQualityMedium";
      
      public static const tooltipQuality:String = "tooltipQuality";
      
      public static const labelRecommendedLive:String = "labelRecommendedLive";
      
      public static const labelRelatedVideos:String = "labelRelatedVideos";
      
      public static const labelReload:String = "labelReload";
      
      public static const labelRemaining:String = "labelRemaining";
      
      public static const labelReplayHighlight:String = "labelReplayHighlight";
      
      public static const labelReportAbuse:String = "labelReportAbuse";
      
      public static const labelRsvp:String = "labelRsvp";
      
      public static const labelSearchShows:String = "labelSearchShows";
      
      public static const labelSearchVideos:String = "labelSearchVideos";
      
      public static const labelSend:String = "labelSend";
      
      public static const labelShare:String = "labelShare";
      
      public static const labelShareStream:String = "labelShareStream";
      
      public static const labelShowOptions:String = "labelShowOptions";
      
      public static const labelSlideshow:String = "labelSlideshow";
      
      public static const labelSocialStream:String = "labelSocialStream";
      
      public static const labelStumbleupon:String = "labelStumbleupon";
      
      public static const labelTaggedBy:String = "labelTaggedBy";
      
      public static const labelToggleAnchor:String = "labelToggleAnchor";
      
      public static const labelToggleFullscreen:String = "labelToggleFullscreen";
      
      public static const labelTotalViewers:String = "labelTotalViewers";
      
      public static const labelTwitter:String = "labelTwitter";
      
      public static const labelUpcomingEvents:String = "labelUpcomingEvents";
      
      public static const labelUrl:String = "labelUrl";
      
      public static const labelUsername:String = "labelUsername";
      
      public static const labelUstreamMobileFrom:String = "labelUstreamMobileFrom";
      
      public static const labelUstreamTv:String = "labelUstreamTv";
      
      public static const labelVideo:String = "labelVideo";
      
      public static const labelVideoQuality:String = "labelVideoQuality";
      
      public static const labelVideoSource:String = "labelVideoSource";
      
      public static const labelViewers:String = "labelViewers";
      
      public static const labelViewersWithTotals:String = "labelViewersWithTotals";
      
      public static const labelViewProfile:String = "labelViewProfile";
      
      public static const labelVote:String = "labelVote";
      
      public static const labelWatchNow:String = "labelWatchNow";
      
      public static const labelWatchWithoutAds:String = "labelWatchWithoutAds";
      
      public static const labelWhen:String = "labelWhen";
      
      public static const labelXOfY:String = "labelXOfY";
      
      public static const labelYear:String = "labelYear";
      
      public static const labelYourMessage:String = "labelYourMessage";
      
      public static const labelLiveShow:String = "labelLiveShow";
      
      public static const messageBroadcasterEmbedLimitReached:String = "messageBroadcasterEmbedLimitReached";
      
      public static const messageBroadcasterLimitReached:String = "messageBroadcasterLimitReached";
      
      public static const messageCapsLockOn:String = "messageCapsLockOn";
      
      public static const messageChannelLocked:String = "messageChannelLocked";
      
      public static const messageCohostAvailable:String = "messageCohostAvailable";
      
      public static const messageCopiedToClipboard:String = "messageCopiedToClipboard";
      
      public static const messageEnterTicketNumber:String = "messageEnterTicketNumber";
      
      public static const messageEnterValidTicketNumber:String = "messageEnterValidTicketNumber";
      
      public static const messageGpsUstreamMobile:String = "messageGpsUstreamMobile";
      
      public static const messageJoinAudience:String = "messageJoinAudience";
      
      public static const messageLiveShowUrlCopied:String = "messageLiveShowUrlCopied";
      
      public static const messageMediaStreamEmbedCopied:String = "messageMediaStreamEmbedCopied";
      
      public static const messagePasswordProtected:String = "messagePasswordProtected";
      
      public static const messagePressControlC:String = "messagePressControlC";
      
      public static const messageSocialStreamUrlCopied:String = "messageSocialStreamUrlCopied";
      
      public static const messageThanksForComing:String = "messageThanksForComing";
      
      public static const messageTicketNoLongerValid:String = "messageTicketNoLongerValid";
      
      public static const messageTooYoung:String = "messageTooYoung";
      
      public static const messageVerifyAge:String = "messageVerifyAge";
      
      public static const messageVideoUrlCopied:String = "messageVideoUrlCopied";
      
      public static const messageViewOnUstream:String = "messageViewOnUstream";
      
      public static const messageViewOnUstreamJoinAudience:String = "messageViewOnUstreamJoinAudience";
      
      public static const monthApril:String = "monthApril";
      
      public static const monthAugust:String = "monthAugust";
      
      public static const monthDecember:String = "monthDecember";
      
      public static const monthFebruary:String = "monthFebruary";
      
      public static const monthJanuary:String = "monthJanuary";
      
      public static const monthJuly:String = "monthJuly";
      
      public static const monthJune:String = "monthJune";
      
      public static const monthMarch:String = "monthMarch";
      
      public static const monthMay:String = "monthMay";
      
      public static const monthNovember:String = "monthNovember";
      
      public static const monthOctober:String = "monthOctober";
      
      public static const monthSeptember:String = "monthSeptember";
      
      public static const labelSizeSmall:String = "labelSizeSmall";
      
      public static const labelSizeNormal:String = "labelSizeNormal";
      
      public static const labelSizeLarge:String = "labelSizeLarge";
      
      public static const labelCustomizeEmbedCode:String = "labelCustomizeEmbedCode";
      
      public static const labelCopyToClipboard:String = "labelCopyToClipboard";
      
      public static const labelEmbedPaneTitle:String = "labelEmbedPaneTitle";
      
      public static const labelEmbedColorDefault:String = "labelEmbedColorDefault";
      
      public static const labelEmbedColorRed:String = "labelEmbedColorRed";
      
      public static const labelEmbedColorGreen:String = "labelEmbedColorGreen";
      
      public static const labelEmbedColorBlue:String = "labelEmbedColorBlue";
      
      public static const labelEmbedColorPink:String = "labelEmbedColorPink";
      
      public static const labelEmbedOldCode:String = "labelEmbedOldCode";
      
      public static const labelEmbedEnableHwAcceleration:String = "labelEmbedEnableHwAcceleration";
      
      public static const labelShareMore:String = "labelShareMore";
      
      public static const labelShareOnTwitter:String = "labelShareOnTwitter";
      
      public static const labelShareOnFacebook:String = "labelShareOnFacebook";
      
      public static const labelClosePane:String = "labelClosePane";
      
      public static const labelPollDone:String = "labelPollDone";
      
      public static const labelPollSkipVoting:String = "labelPollSkipVoting";
      
      public static const labelPollTooltip:String = "labelPollTooltip";
      
      public static const labelCohostTheBroadcaster:String = "labelCohostTheBroadcaster";
      
      public static const labelCohostSelectCamera:String = "labelCohostSelectCamera";
      
      public static const labelCohostDisableCamera:String = "labelCohostDisableCamera";
      
      public static const labelCohostSelectMicrophone:String = "labelCohostSelectMicrophone";
      
      public static const labelCohostDisableMicrophone:String = "labelCohostDisableMicrophone";
      
      public static const labelCohostCancel:String = "labelCohostCancel";
      
      public static const labelCohostCloseSettings:String = "labelCohostCloseSettings";
      
      public static const labelCohostLookingFor:String = "labelCohostLookingFor";
      
      public static const labelCohostName:String = "labelCohostName";
      
      public static const labelCohostInfo:String = "labelCohostInfo";
      
      public static const labelCohostLikeTo:String = "labelCohostLikeTo";
      
      public static const labelCohostNoThanks:String = "labelCohostNoThanks";
      
      public static const labelCohostStatusAccepted:String = "labelCohostStatusAccepted";
      
      public static const labelCohostStatusOnAir:String = "labelCohostStatusOnAir";
      
      public static const labelCohostStatusPending:String = "labelCohostStatusPending";
      
      public static const labelCohostStatusPreview:String = "labelCohostStatusPreview";
      
      public static const labelCohostSessionEnded:String = "labelCohostSessionEnded";
      
      public static const tooltipCohostOffair:String = "tooltipCohostOffair";
      
      public static const tooltipCohostSelected:String = "tooltipCohostSelected";
      
      public static const tooltipCohostDeclined:String = "tooltipCohostDeclined";
      
      public static const tooltipCohostRequestSent:String = "tooltipCohostRequestSent";
      
      public static const tooltipCohostLookingFor:String = "tooltipCohostLookingFor";
      
      public static const labelPasswordTitle:String = "labelPasswordTitle";
      
      public static const labelPasswordHint:String = "labelPasswordHint";
      
      public static const messageEnterPassword:String = "messageEnterPassword";
      
      public static const labelPasswordSend:String = "labelPasswordSend";
      
      public static const labelAgePaneHeader:String = "labelAgePaneHeader";
      
      public static const labelAgePaneTitle:String = "labelAgePaneTitle";
      
      public static const labelAgePaneBadAge:String = "labelAgePaneBadAge";
      
      public static const labelAgePaneBadAge2:String = "labelAgePaneBadAge2";
      
      public static const labelAgePaneSubmit:String = "labelAgePaneSubmit";
      
      public static const labelMultiCamPaneTitle:String = "labelMultiCamPaneTitle";
      
      public static const labelMultiCamButtonLabel:String = "labelMultiCamButtonLabel";
      
      public static const labelMultiCamTooltip:String = "labelMultiCamTooltip";
      
      public static const labelMultiCamSwitching:String = "labelMultiCamSwitching";
      
      public static const messageTweetLive:String = "messageTweetLive";
      
      public static const messageTweetRecorded:String = "messageTweetRecorded";
      
      public static const messageJapanThankYou:String = "messageJapanThankYou";
      
      public static const labelPpvSubscribe:String = "labelPpvSubscribe";
      
      public static const labelPpvBuyTicket:String = "labelPpvBuyTicket";
      
      public static const labelPpvBuyTicketWithPrice:String = "labelPpvBuyTicketWithPrice";
      
      public static const labelPpvAlreadyHaveATicket:String = "labelPpvAlreadyHaveATicket";
      
      public static const labelPpvBuyMoreTicket:String = "labelPpvBuyMoreTicket";
      
      public static const labelPpvEnterTicket:String = "labelPpvEnterTicket";
      
      public static const labelPpvHeader:String = "labelPpvHeader";
      
      public static const labelPpvLogin:String = "labelPpvLogin";
      
      public static const labelPpvHideInfo:String = "labelPpvHideInfo";
      
      public static const labelPpvShowInfo:String = "labelPpvShowInfo";
      
      public static const labelPpvShowFaq:String = "labelPpvShowFaq";
      
      public static const labelPpvTicketHint:String = "labelPpvTicketHint";
      
      public static const labelPpvSubscribeBtnHint:String = "labelPpvSubscribeBtnHint";
      
      public static const labelPpvBuyTicketBtnHint:String = "labelPpvBuyTicketBtnHint";
      
      public static const labelPpvEnterTicketBtnHint:String = "labelPpvEnterTicketBtnHint";
      
      public static const labelPpvPreshow:String = "labelPpvPreshow";
      
      public static const labelPpvGuestOnairFree:String = "labelPpvGuestOnairFree";
      
      public static const labelPpvGuestOnairPaid:String = "labelPpvGuestOnairPaid";
      
      public static const labelPpvPayerOnairFree:String = "labelPpvPayerOnairFree";
      
      public static const labelPpvPayerOnairPaid:String = "labelPpvPayerOnairPaid";
      
      public static const labelPpvPayerOffair:String = "labelPpvPayerOffair";
      
      public static const labelPpvPayerVod:String = "labelPpvPayerVod";
      
      public static const labelPpvGuestVod:String = "labelPpvGuestVod";
      
      public static const labelSubscriptionLogin:String = "labelSubscriptionLogin";
      
      public static const labelSubscriptionGuestOnair:String = "labelSubscriptionGuestOnair";
      
      public static const labelSubscriptionGuestOffair:String = "labelSubscriptionGuestOffair";
      
      public static const labelSubscriptionPayerOnair:String = "labelSubscriptionPayerOnair";
      
      public static const labelSubscriptionPayerOffair:String = "labelSubscriptionPayerOffair";
      
      public static const labelSubscriptionGuestOnairFree:String = "labelSubscriptionGuestOnairFree";
      
      public static const labelSubscriptionGuestOffairFree:String = "labelSubscriptionGuestOffairFree";
      
      public static const labelSubscriptionPayerOnairFree:String = "labelSubscriptionPayerOnairFree";
      
      public static const labelSubscriptionPayerOffairFree:String = "labelSubscriptionPayerOffairFree";
      
      public static const labelSubscriptionRecordedVideos:String = "labelSubscriptionRecordedVideos";
      
      public static const labelSubscriptionPayerVod:String = "labelSubscriptionPayerVod";
      
      public static const labelSubscriptionGuestVod:String = "labelSubscriptionGuestVod";
      
      public static const messagePpvFreeReject:String = "messagePpvFreeReject";
      
      public static const messagePpvPaidReject:String = "messagePpvPaidReject";
      
      public static const messageContentBanned:String = "messageContentBanned";
      
      public static const labelTitlePaneUpNext:String = "labelTitlePaneUpNext";
      
      public static const labelTitlePaneUpNextPpv:String = "labelTitlePaneUpNextPpv";
      
      public static const labelTitlePanePlayingLive:String = "labelTitlePanePlayingLive";
      
      public static const labelTitlePaneTitle:String = "labelTitlePaneTitle";
      
      public static const labelTitlePaneUserInfo:String = "labelTitlePaneUserInfo";
      
      public static const labelTitlePaneRecommendations:String = "labelTitlePaneRecommendations";
      
      public static const labelEventPaneStartsAt:String = "labelEventPaneStartsAt";
      
      public static const labelEventPaneStartsIn:String = "labelEventPaneStartsIn";
      
      public static const labelEventPaneRemindMe:String = "labelEventPaneRemindMe";
      
      public static const messageMediaUrlCopied:String = "messageMediaUrlCopied";
      
      public static const messageB4fAuthenticating:String = "messageB4fAuthenticating";
      
      public static const labelB4fAddingVideo:String = "labelB4fAddingVideo";
      
      public static const labelB4fVideoAdded:String = "labelB4fVideoAdded";
      
      public static const labelB4fVideoRemoved:String = "labelB4fVideoRemoved";
      
      public static const labelToggleTheaterMode:String = "labelToggleTheaterMode";
      
      public static const labelPopout:String = "labelPopout";
      
      public static const labelCurrencyAUD:String = "labelCurrencyAUD";
      
      public static const labelCurrencyCAD:String = "labelCurrencyCAD";
      
      public static const labelCurrencyCZK:String = "labelCurrencyCZK";
      
      public static const labelCurrencyDKK:String = "labelCurrencyDKK";
      
      public static const labelCurrencyEUR:String = "labelCurrencyEUR";
      
      public static const labelCurrencyHKD:String = "labelCurrencyHKD";
      
      public static const labelCurrencyHUF:String = "labelCurrencyHUF";
      
      public static const labelCurrencyILS:String = "labelCurrencyILS";
      
      public static const labelCurrencyJPY:String = "labelCurrencyJPY";
      
      public static const labelCurrencyMXN:String = "labelCurrencyMXN";
      
      public static const labelCurrencyNOK:String = "labelCurrencyNOK";
      
      public static const labelCurrencyNZD:String = "labelCurrencyNZD";
      
      public static const labelCurrencyPHP:String = "labelCurrencyPHP";
      
      public static const labelCurrencyPLN:String = "labelCurrencyPLN";
      
      public static const labelCurrencyGBP:String = "labelCurrencyGBP";
      
      public static const labelCurrencySGD:String = "labelCurrencySGD";
      
      public static const labelCurrencySEK:String = "labelCurrencySEK";
      
      public static const labelCurrencyCHF:String = "labelCurrencyCHF";
      
      public static const labelCurrencyTWD:String = "labelCurrencyTWD";
      
      public static const labelCurrencyTHB:String = "labelCurrencyTHB";
      
      public static const labelCurrencyUSD:String = "labelCurrencyUSD";
      
      public static const labelCurrencyKRW:String = "labelCurrencyKRW";
      
      public static const labelPpvBuyWithLocalizedCurrency:String = "labelPpvBuyWithLocalizedCurrency";
      
      public static const labelDvrRewatch:String = "labelDvrRewatch";
      
      public static const labelDvrDragToRewind:String = "labelDvrDragToRewind";
      
      public static const labelDvrJumpToLive:String = "labelDvrJumpToLive";
   }
}
