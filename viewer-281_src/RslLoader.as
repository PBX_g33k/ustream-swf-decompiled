package 
{
   import flash.display.MovieClip;
   import flash.external.ExternalInterface;
   import flash.system.LoaderContext;
   import flash.display.Loader;
   import flash.utils.Timer;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import flash.system.Security;
   import flash.system.ApplicationDomain;
   import flash.system.Capabilities;
   import flash.system.SecurityDomain;
   import flash.events.IOErrorEvent;
   import flash.events.HTTPStatusEvent;
   import flash.utils.getTimer;
   import flash.net.URLRequest;
   import flash.utils.ByteArray;
   
   public class RslLoader extends MovieClip
   {
      
      public function RslLoader(param1:String) {
         super();
         this.localPath = param1;
         stop();
         if(stage)
         {
            this.onAddedToStage();
         }
         else
         {
            addEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         }
      }
      
      public static const SITE_TYPE_EMBED:uint = 1;
      
      public static const SITE_TYPE_STAGE:uint = 2;
      
      public static const SITE_TYPE_BRANCH:uint = 4;
      
      public static const SITE_TYPE_TAG:uint = 8;
      
      public static const SITE_TYPE_LOCAL:uint = 32;
      
      protected static var origin:uint;
      
      private static var _pageUrl:String;
      
      private static var _loaderUrl:String;
      
      public static function onSite(param1:String) : uint {
         var _loc2_:uint = 0;
         var _loc3_:* = false;
         if(!param1 || (param1) && (!(param1.indexOf(".swf") == -1)))
         {
            return _loc2_;
         }
         var _loc4_:String = param1.indexOf("://") == -1?param1:param1.split("://")[1];
         if(!_loc4_)
         {
            return 0;
         }
         _loc4_ = _loc4_.split("/")[0];
         if(_loc4_.substr(-10,10) == "ustream.tv" || _loc4_.substr(-13,13) == "ustream.tv.lh")
         {
            _loc2_ = _loc2_ | SITE_TYPE_STAGE;
         }
         if(_loc2_ & SITE_TYPE_STAGE)
         {
            if(!(_loc4_.indexOf(".branches.") == -1) || !(_loc4_.indexOf(".dev.") == -1) && !(_loc4_.indexOf("-branches-") == -1))
            {
               _loc2_ = _loc2_ | SITE_TYPE_BRANCH;
            }
            if(!(_loc4_.indexOf(".tags.") == -1) || !(_loc4_.indexOf(".dev.") == -1) && !(_loc4_.indexOf("-tags-") == -1))
            {
               _loc2_ = _loc2_ | SITE_TYPE_TAG;
            }
            if(_loc4_.substr(-3,3) == ".lh")
            {
               _loc2_ = _loc2_ | SITE_TYPE_LOCAL;
            }
         }
         return _loc2_;
      }
      
      public static function flashPath(param1:String) : String {
         var _loc3_:String = null;
         var _loc2_:String = secure?"https":"http";
         if((onSite(param1) & SITE_TYPE_BRANCH) || (onSite(param1) & SITE_TYPE_TAG))
         {
            _loc3_ = (param1.indexOf("://") == -1?param1:param1.split("://")[1]).split("/")[0];
            _loc3_ = _loc3_.split("-watershed").join("-www");
            return _loc2_ + "://" + _loc3_ + "/flash/";
         }
         return _loc2_ + "://www.ustream.tv/flash/";
      }
      
      public static function get pageUrl() : String {
         var js:String = null;
         if(!_pageUrl)
         {
            try
            {
               js = "getPageUrl=function(){ return window.location.href }; getPageUrl()";
               _pageUrl = ExternalInterface.call("eval",js);
            }
            catch(e:Error)
            {
            }
            if(!_pageUrl)
            {
               _pageUrl = "";
            }
         }
         return _pageUrl;
      }
      
      public static function get secure() : Boolean {
         return (_loaderUrl) && _loaderUrl.indexOf("https://") == 0;
      }
      
      protected const RETRY_DELAY:uint = 5;
      
      protected var context:LoaderContext;
      
      protected var loader:Loader;
      
      protected var document;
      
      private var loaderTimer:Timer;
      
      public var logic;
      
      protected var loaderUrl:String;
      
      protected var rslPath:String = "http://www.ustream.tv/flash/";
      
      protected var rslName:String = "viewer.rsl.swf";
      
      private var localPath:String;
      
      private var startTimer:int = 0;
      
      protected function onAddedToStage(... rest) : void {
         removeEventListener(Event.ADDED_TO_STAGE,this.onAddedToStage);
         stage.align = "TL";
         stage.scaleMode = "noScale";
         this.loaderTimer = new Timer(this.RETRY_DELAY * 1000,1);
         this.loaderTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onLoaderTimer);
         try
         {
            Security.allowDomain("*");
            Security.allowInsecureDomain("*");
         }
         catch(e:Error)
         {
         }
         if(loaderInfo)
         {
            this.echo("viewer bytesLoaded : " + loaderInfo.bytesLoaded + ", bytesTotal : " + loaderInfo.bytesTotal);
            if(loaderInfo.bytesLoaded >= loaderInfo.bytesTotal)
            {
               this.initRslLoader();
            }
            else
            {
               loaderInfo.addEventListener(Event.COMPLETE,this.initRslLoader);
            }
         }
         else
         {
            this.initRslLoader();
         }
      }
      
      private function initRslLoader(param1:Event = null) : void {
         if(param1)
         {
            param1.currentTarget.removeEventListener(Event.COMPLETE,this.initRslLoader);
         }
         this.echo("viewer bytesLoaded : " + loaderInfo.bytesLoaded + ", bytesTotal : " + loaderInfo.bytesTotal);
         this.echo("viewer complete");
         _loaderUrl = loaderInfo.url;
         this.echo("loaderUrl: " + _loaderUrl);
         if(ApplicationDomain.currentDomain.hasDefinition("ViewerRSL"))
         {
            this.echo("rsl already loaded");
            this.onRslLoaded();
         }
         else
         {
            this.echo("player : " + Capabilities.version);
            this.echo("sandbox : " + Security.sandboxType);
            this.echo("swf : " + loaderInfo.url + " " + Security.sandboxType);
            this.echo("loaderUrl : " + loaderInfo.loaderURL);
            this.echo("pageUrl : " + pageUrl);
            origin = pageUrl?onSite(pageUrl):0;
            this.rslPath = flashPath(pageUrl);
            this.context = new LoaderContext(false,ApplicationDomain.currentDomain);
            this.echo("flash player is " + (origin == 0?"":"not ") + "embedded ( origin = " + origin + " )");
            if(Security.sandboxType == "localTrusted")
            {
               if(this.localPath)
               {
                  this.rslPath = this.localPath;
               }
            }
            else
            {
               this.context.securityDomain = SecurityDomain.currentDomain;
            }
            this.loadRsl();
         }
      }
      
      protected function loadRsl() : void {
         if(this.loader)
         {
            this.loader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onRslLoaded);
            this.loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
            this.loader.contentLoaderInfo.removeEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
            try
            {
               this.loader.close();
            }
            catch(err:Error)
            {
            }
         }
         this.loader = new Loader();
         this.loader.contentLoaderInfo.addEventListener(Event.COMPLETE,this.onRslLoaded);
         this.loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
         this.loader.contentLoaderInfo.addEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         this.loaderTimer.reset();
         this.onLoaderTimer();
      }
      
      private function onLoaderTimer(param1:TimerEvent = null) : void {
         this.echo("get rsl : " + this.rslUrl);
         this.startTimer = getTimer();
         this.loader.load(new URLRequest(this.rslUrl),this.context);
      }
      
      private function onHttpStatus(param1:HTTPStatusEvent) : void {
         if(!(param1.status == 0 || param1.status == 200))
         {
            this.echo(param1.toString());
         }
      }
      
      private function onLoaderError(param1:Event) : void {
         this.echo(param1.toString());
         this.echo("retrying in " + this.RETRY_DELAY + " seconds");
         this.loaderTimer.reset();
         this.loaderTimer.start();
      }
      
      public function get rslData() : ByteArray {
         return this.loader.contentLoaderInfo.bytes;
      }
      
      protected function onRslLoaded(... rest) : void {
         this.echo("rsl loaded in " + (getTimer() - this.startTimer) + " msec");
         if(this.loader)
         {
            this.loader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.onRslLoaded);
            this.loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.onLoaderError);
            this.loader.contentLoaderInfo.removeEventListener(HTTPStatusEvent.HTTP_STATUS,this.onHttpStatus);
         }
      }
      
      protected function echo(... rest) : void {
         trace.apply(this,rest);
      }
      
      protected function get rslUrl() : String {
         return this.rslPath + this.rslName;
      }
   }
}
