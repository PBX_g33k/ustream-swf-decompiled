package 
{
   import flash.display.Sprite;
   import flash.system.Security;
   import flash.net.SharedObject;
   import flash.net.URLRequest;
   import flash.net.URLRequestMethod;
   import flash.display.Loader;
   import flash.events.IOErrorEvent;
   import flash.display.LoaderInfo;
   
   public class quantv2 extends Sprite
   {
      
      {
         Security.allowDomain("*");
      }
      
      public function quantv2() {
         var paramObj:Object = null;
         var aTrace:String = null;
         this.last_check = new Date().time;
         super();
         try
         {
            paramObj = LoaderInfo(this.root.loaderInfo).parameters;
            aTrace = paramObj["allowTrace"];
            if(aTrace == "true")
            {
               this.allowTrace = true;
            }
         }
         catch(error:Error)
         {
         }
         this.clearTPF();
      }
      
      private const RATE:Number = 4;
      
      private var cur_rate:Number = 4;
      
      private const PER:Number = 1000;
      
      private var allowance:Number = 4;
      
      private var last_check:Number;
      
      private var counter:Number = 0;
      
      private var allowTrace:Boolean = false;
      
      private function clearTPF() : void {
         var so:SharedObject = null;
         try
         {
            so = SharedObject.getLocal("com.quantserve","/");
            if(so.data._tpf != null)
            {
               this.xTrace("Deleting fpf: " + so.data._tpf);
               delete so.data._tpf;
            }
         }
         catch(err:Error)
         {
            xTrace("clear Shared Object (TPF) error: " + err);
         }
      }
      
      public function sendEvent(param1:String) : void {
         if(this.rateLimit())
         {
            return;
         }
         var _loc2_:URLRequest = new URLRequest(param1);
         _loc2_.method = URLRequestMethod.GET;
         var _loc3_:Loader = new Loader();
         _loc3_.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.loadingError);
         _loc3_.load(_loc2_);
         this.xTrace("sent event: " + param1);
      }
      
      private function loadingError(param1:IOErrorEvent) : void {
         this.xTrace("URL not found.");
      }
      
      private function rateLimit() : Boolean {
         var _loc1_:Number = new Date().time;
         var _loc2_:Number = _loc1_ - this.last_check;
         if(_loc2_ > 1000)
         {
            this.cur_rate = this.RATE;
            this.counter = 0;
         }
         this.last_check = _loc1_;
         this.allowance = this.allowance + _loc2_ * this.cur_rate / this.PER;
         if(this.allowance > this.RATE)
         {
            this.allowance = this.RATE;
         }
         if(this.allowance < 1)
         {
            this.xTrace("You are sending way to many events");
            return true;
         }
         this.allowance = this.allowance - 1;
         this.counter++;
         this.cur_rate = this.cur_rate * 1 / this.counter;
         return false;
      }
      
      public function start(param1:String) : void {
         trace("start() was invoked..." + param1);
      }
      
      private function xTrace(param1:String) : void {
         if(this.allowTrace)
         {
            trace("quantv2: " + param1);
         }
      }
   }
}
