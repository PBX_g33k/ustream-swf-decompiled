package tv.ustream.debug.interfaces
{
   import flash.utils.Timer;
   import flash.net.Socket;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import tv.ustream.debug.Debug;
   import flash.events.SecurityErrorEvent;
   import flash.events.IOErrorEvent;
   
   public class SocketInterface extends DebugInterface
   {
      
      public function SocketInterface() {
         super();
         debugSocket = new Socket("localhost",8888);
         debugSocket.addEventListener("ioError",onDebugSocketIoError);
         debugSocket.addEventListener("securityError",onSocketSecurityError);
         debugSocket.addEventListener("connect",onSocketConnect);
         debugSocket.addEventListener("close",onSocketClose);
      }
      
      protected const SOCKET_PACKAGE_SIZE_LIMIT:int = 8096;
      
      private var socketTimer:Timer;
      
      private var debugSocket:Socket;
      
      private var debugSocketBuffer:Vector.<Object>;
      
      private function onSocketTimerTimer(param1:TimerEvent) : void {
         var _loc4_:* = 0;
         var _loc2_:* = null;
         var _loc5_:* = null;
         var _loc3_:* = false;
         var _loc6_:* = null;
         if(debugSocket && debugSocket.connected)
         {
            _loc4_ = 0;
            _loc2_ = [];
            _loc5_ = "";
            _loc3_ = debugSocketBuffer.length > 0?false:true;
            while(!_loc3_)
            {
               _loc6_ = debugSocketBuffer.shift();
               if(!(_loc5_ == "") && !(_loc6_.id == _loc5_))
               {
                  debugSocketBuffer.unshift(_loc6_);
                  _loc3_ = true;
               }
               else
               {
                  _loc5_ = _loc6_.id;
                  _loc6_.id = null;
                  _loc4_ = _loc4_ + _loc6_.line.length;
                  _loc2_.push(_loc6_);
               }
               if(debugSocketBuffer.length == 0)
               {
                  socketTimer.stop();
                  socketTimer.removeEventListener("timer",onSocketTimerTimer);
                  socketTimer = null;
                  _loc3_ = true;
               }
               if(_loc4_ >= 8096)
               {
                  _loc3_ = true;
               }
            }
            writeOnSocket(
               {
                  "id":_loc5_,
                  "lines":_loc2_
               });
         }
      }
      
      private function onSocketClose(param1:Event) : void {
         removeSocketListeners();
         debugSocket = null;
         Debug.debug("SocketInterface","onSocketClose()");
      }
      
      private function onSocketConnect(param1:Event) : void {
         Debug.debug("SocketInterface","onSocketConnect()");
      }
      
      private function onSocketSecurityError(param1:SecurityErrorEvent) : void {
         destroyDebugSocketBuffer();
      }
      
      private function onDebugSocketIoError(param1:IOErrorEvent) : void {
         destroyDebugSocketBuffer();
      }
      
      private function destroyDebugSocketBuffer() : void {
         if(debugSocketBuffer)
         {
            debugSocketBuffer = null;
         }
      }
      
      private function writeOnSocket(param1:Object) : void {
         if(debugSocket && debugSocket.connected)
         {
            debugSocket.writeObject(param1);
            debugSocket.flush();
         }
      }
      
      private function removeSocketListeners() : void {
         debugSocket.removeEventListener("ioError",onDebugSocketIoError);
         debugSocket.removeEventListener("securityError",onSocketSecurityError);
         debugSocket.removeEventListener("connect",onSocketConnect);
         debugSocket.removeEventListener("close",onSocketClose);
      }
      
      override protected function echo(param1:String, param2:String, param3:uint = 0) : void {
         var _loc4_:* = null;
         if(debugSocket)
         {
            if(!debugSocketBuffer)
            {
               debugSocketBuffer = new Vector.<Object>();
            }
            _loc4_ = {};
            _loc4_.id = Debug.debugPrefix + Debug.debugId;
            _loc4_.timeStamp = param1;
            _loc4_.line = param2;
            _loc4_.level = param3 & 15;
            debugSocketBuffer.push(_loc4_);
            if(!socketTimer)
            {
               socketTimer = new Timer(100);
               socketTimer.addEventListener("timer",onSocketTimerTimer);
               socketTimer.start();
            }
         }
      }
   }
}
