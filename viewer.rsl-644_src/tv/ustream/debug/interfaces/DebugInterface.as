package tv.ustream.debug.interfaces
{
   import tv.ustream.debug.DebugItem;
   import flash.utils.ByteArray;
   
   public class DebugInterface extends Object implements IDebugInterface
   {
      
      public function DebugInterface() {
         super();
      }
      
      public function log(param1:DebugItem) : void {
         echo(param1.timeStamp,param1.source != "legacy"?"[ " + param1.source.toUpperCase() + " ] " + param1.line:param1.line,param1.level);
         if(param1.data)
         {
            explore(param1.data);
         }
      }
      
      protected function echo(param1:String, param2:String, param3:uint = 0) : void {
      }
      
      protected function explore(param1:Object, param2:uint = 0, param3:uint = 0) : void {
         var _loc4_:* = NaN;
         var _loc5_:String = "";
         _loc4_ = 0.0;
         while(_loc4_ <= param2)
         {
            _loc5_ = _loc5_ + "\t";
            _loc4_++;
         }
         var _loc8_:* = 0;
         var _loc7_:* = param1;
         for(_loc6_ in param1)
         {
            if(!param1[_loc6_])
            {
               echo("",_loc5_ + "" + _loc6_ + " : " + param1[_loc6_] + " [" + typeof param1[_loc6_] + "]",param3);
            }
            else if(param1[_loc6_] is ByteArray)
            {
               echo("",_loc5_ + "" + _loc6_ + " : " + byteArrayToString(param1[_loc6_]) + " [byteArray, " + (param1[_loc6_] as ByteArray).length + " bytes]",param3);
            }
            else if(param1[_loc6_] is Date)
            {
               echo("",_loc5_ + "" + _loc6_ + " : " + (param1[_loc6_] as Date).toString() + " [date]",param3);
            }
            else if(typeof param1[_loc6_] == "object")
            {
               echo("",_loc5_ + "" + _loc6_ + " : ",param3);
               explore(param1[_loc6_],param2 + 1,param3);
            }
            else
            {
               echo("",_loc5_ + "" + _loc6_ + " : " + param1[_loc6_] + " [" + typeof param1[_loc6_] + "]",param3);
            }
            
            
            
         }
      }
      
      protected function byteArrayToString(param1:ByteArray, param2:uint = 8) : String {
         var _loc4_:* = 0;
         var _loc6_:* = 0;
         var _loc5_:* = 0;
         var _loc3_:String = "";
         if(param1)
         {
            _loc4_ = param1.position;
            param1.position = 0;
            _loc6_ = 0;
            while(_loc6_ < Math.min(param1.length,param2))
            {
               _loc5_ = param1.readByte();
               _loc3_ = _loc3_ + (_loc5_ & 255).toString(16);
               _loc6_++;
            }
            param1.position = _loc4_;
         }
         if(param2 < param1.length)
         {
            _loc3_ = _loc3_ + "...";
         }
         return _loc3_;
      }
   }
}
