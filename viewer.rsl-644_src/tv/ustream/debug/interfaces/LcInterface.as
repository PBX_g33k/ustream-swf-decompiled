package tv.ustream.debug.interfaces
{
   import flash.net.LocalConnection;
   import flash.utils.Timer;
   import flash.events.StatusEvent;
   import tv.ustream.debug.DebugItem;
   import flash.events.TimerEvent;
   import tv.ustream.debug.Debug;
   
   public class LcInterface extends DebugInterface
   {
      
      public function LcInterface() {
         super();
         tracerLc = new LocalConnection();
         tracerLc.allowDomain("*");
         tracerLc.allowInsecureDomain("*");
         tracerLc.addEventListener("status",onTracerLcStatus);
         sendTimer = new Timer(500,1);
         sendTimer.addEventListener("timerComplete",onSendTimer);
         sendBuffer = new Vector.<String>();
      }
      
      private const TIMER_DELAY:uint = 500;
      
      private const MAX_BUFFER_SIZE:uint = 16384;
      
      private const DELIMITER:String = "\n";
      
      private var tracerLc:LocalConnection;
      
      private var sendBuffer:Vector.<String>;
      
      private var sendBufferSize:uint = 0;
      
      private var sendTimer:Timer;
      
      private function onTracerLcStatus(param1:StatusEvent) : void {
         if(param1.level == "error" && tracerLc)
         {
            tracerLc.removeEventListener("status",onTracerLcStatus);
            tracerLc = null;
            sendTimer.removeEventListener("timerComplete",onSendTimer);
            sendTimer.reset();
            sendTimer = null;
         }
      }
      
      override public function log(param1:DebugItem) : void {
         if(tracerLc)
         {
            super.log(param1);
         }
      }
      
      override protected function echo(param1:String, param2:String, param3:uint = 0) : void {
         if(sendBufferSize + param2.length >= 16384)
         {
            onSendTimer();
         }
         sendBuffer.push(param1 + " " + param2);
         sendBufferSize = sendBufferSize + param2.length;
         if(!sendTimer.running)
         {
            sendTimer.reset();
            sendTimer.start();
         }
      }
      
      private function onSendTimer(param1:TimerEvent = null) : void {
         var _loc2_:* = null;
         if(sendBuffer && sendBuffer.length)
         {
            _loc2_ = sendBuffer.join("\n");
            try
            {
               tracerLc.send("_ustreamEcho","echo",Debug.debugPrefix + Debug.debugId,_loc2_);
            }
            catch(err:*)
            {
            }
            sendBuffer = new Vector.<String>();
            sendBufferSize = 0;
         }
      }
      
      public function sendFocus() : void {
         try
         {
            tracerLc.send("_ustreamEcho","topdebug",Debug.debugPrefix + Debug.debugId);
         }
         catch(err:*)
         {
         }
      }
   }
}
