package tv.ustream.debug.interfaces
{
   import tv.ustream.debug.DebugItem;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.utils.ByteArray;
   import tv.ustream.debug.Debug;
   import flash.utils.getTimer;
   
   public class CollectorInterface extends Object implements IDebugInterface
   {
      
      public function CollectorInterface() {
         super();
         _items = new Vector.<DebugItem>();
         _cleanupTimer = new Timer(5000);
         _cleanupTimer.addEventListener("timer",onCleanupTimer);
         _cleanupTimer.start();
      }
      
      private const CLEANUP_DELAY:uint = 1800000;
      
      private const CLEANUP_FREQUENCY:uint = 5000;
      
      private const MAX_UNCOMPRESSED_PACKET_SIZE:uint = 102400;
      
      private var _items:Vector.<DebugItem>;
      
      private var _cleanupTimer:Timer;
      
      private function onCleanupTimer(param1:TimerEvent) : void {
         var _loc3_:* = 0;
         var _loc4_:* = 0;
         var _loc2_:uint = new Date().getTime();
         _loc3_ = 0;
         while(_loc3_ < _items.length)
         {
            _loc4_ = _loc2_ - _items[_loc3_].date.getTime();
            if(_loc4_ > 1800000 && !(_items[_loc3_].level & 16))
            {
               _items.splice(_loc3_--,1);
            }
            else if(_loc4_ <= 1800000)
            {
               break;
            }
            
            _loc3_++;
         }
      }
      
      public function log(param1:DebugItem) : void {
         if((param1.level & 32) == 0)
         {
            _items.push(param1);
         }
      }
      
      public function clear() : void {
         _items = new Vector.<DebugItem>();
      }
      
      public function pack() : Vector.<ByteArray> {
         var _loc2_:* = null;
         var _loc7_:* = null;
         var _loc3_:* = 0;
         var _loc1_:* = 0;
         var _loc8_:* = null;
         var _loc5_:Vector.<DebugItem> = _items;
         var _loc6_:Vector.<ByteArray> = new Vector.<ByteArray>();
         var _loc4_:uint = 0;
         _items = new Vector.<DebugItem>();
         Debug.debug("CollectorInterface","packing items: " + _loc5_.length);
         while(_loc5_.length)
         {
            _loc7_ = new ByteArray();
            _loc7_.objectEncoding = 3;
            while(_loc7_.length <= 102400 && _loc5_.length)
            {
               _loc2_ = new ByteArray();
               _loc2_.objectEncoding = 3;
               _loc2_.writeBytes(_loc5_[0].cloneToByteArray());
               _loc2_.position = 0;
               if(_loc7_.length + _loc2_.length <= 102400)
               {
                  _loc7_.writeBytes(_loc2_);
                  _loc5_.shift();
                  continue;
               }
               break;
            }
            _loc3_ = _loc7_.length;
            _loc1_ = getTimer();
            _loc7_.position = 0;
            _loc8_ = _loc7_.readObject();
            Debug.debug("CollectorInterface","test",_loc8_);
            _loc7_.deflate();
            Debug.debug("CollectorInterface","_sendBuffer compressed from " + _loc3_ + " to " + _loc7_.length + " bytes in " + (getTimer() - _loc1_) + " msec");
            _loc4_ = _loc4_ + _loc7_.length;
            _loc6_.push(_loc7_);
         }
         Debug.debug("CollectorInterface","pack complete, buffer size : " + _loc4_ + ", items : " + _loc6_.length);
         return _loc6_;
      }
   }
}
