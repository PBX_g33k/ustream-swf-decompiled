package tv.ustream.debug.interfaces
{
   public class ProxyInterface extends DebugInterface
   {
      
      public function ProxyInterface(param1:Function = null) {
         super();
         this.proxy = param1;
      }
      
      public var proxy:Function;
      
      override protected function echo(param1:String, param2:String, param3:uint = 0) : void {
         if(proxy != null)
         {
            proxy.apply(this,param1 + " " + param2);
         }
      }
   }
}
