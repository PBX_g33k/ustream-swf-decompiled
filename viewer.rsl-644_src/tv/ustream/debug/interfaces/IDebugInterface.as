package tv.ustream.debug.interfaces
{
   import tv.ustream.debug.DebugItem;
   
   public interface IDebugInterface
   {
      
      function log(param1:DebugItem) : void;
   }
}
