package tv.ustream.debug.interfaces
{
   import flash.text.TextField;
   
   public class DisplayInterface extends DebugInterface
   {
      
      public function DisplayInterface(param1:TextField = null) {
         super();
         this.textField = param1;
      }
      
      public var textField:TextField;
      
      override protected function echo(param1:String, param2:String, param3:uint = 0) : void {
         if(textField)
         {
            textField.appendText(param1 + " " + param2 + "\n");
            textField.scrollV = textField.maxScrollV;
         }
      }
   }
}
