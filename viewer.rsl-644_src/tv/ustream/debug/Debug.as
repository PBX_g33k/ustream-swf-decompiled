package tv.ustream.debug
{
   import flash.display.Stage;
   import flash.text.TextField;
   import tv.ustream.debug.interfaces.IDebugInterface;
   import flash.utils.ByteArray;
   import tv.ustream.debug.interfaces.TraceInterface;
   import tv.ustream.debug.interfaces.SocketInterface;
   import tv.ustream.debug.interfaces.LcInterface;
   import tv.ustream.debug.interfaces.CollectorInterface;
   import flash.events.KeyboardEvent;
   import flash.net.SharedObject;
   import flash.external.ExternalInterface;
   import tv.ustream.debug.interfaces.DisplayInterface;
   import tv.ustream.debug.interfaces.ProxyInterface;
   import tv.ustream.net.ViewerUms;
   import flash.events.Event;
   
   public class Debug extends Object
   {
      
      public function Debug() {
         super();
      }
      
      public static const LEVEL_DEBUG:int = 0;
      
      public static const LEVEL_INFO:int = 1;
      
      public static const LEVEL_WARN:int = 2;
      
      public static const LEVEL_ERROR:int = 3;
      
      public static const LEVEL_FATAL:int = 4;
      
      public static const FLAG_KEEPER:uint = 16;
      
      public static const FLAG_DONT_COLLECT:uint = 32;
      
      public static const DEFAULT_LEVEL:int = 1;
      
      private static var _stage:Stage;
      
      private static var _debugPrefix:String = "debug.";
      
      protected static var _textField:TextField;
      
      protected static var _proxy:Function;
      
      private static var _debugSo;
      
      public static var debugId:String;
      
      private static var interfaces:Vector.<IDebugInterface>;
      
      private static var senders:Vector.<LogSender>;
      
      private static var _enabled:Boolean = true;
      
      private static var _collectionEnabled:Boolean = true;
      
      public static function debug(param1:String, param2:String, param3:Object = null, param4:Boolean = false, param5:Boolean = false) : void {
         echo(param1,param2,clone(param3),0,param4,param5);
      }
      
      public static function info(param1:String, param2:String, param3:Object = null, param4:Boolean = false, param5:Boolean = false) : void {
         echo(param1,param2,clone(param3),1,param4,param5);
      }
      
      public static function warn(param1:String, param2:String, param3:Object = null, param4:Boolean = false, param5:Boolean = false) : void {
         echo(param1,param2,clone(param3),2,param4,param5);
      }
      
      public static function error(param1:String, param2:String, param3:Object = null, param4:Boolean = false, param5:Boolean = false) : void {
         echo(param1,param2,clone(param3),3,param4,param5);
      }
      
      public static function fatal(param1:String, param2:String, param3:Object = null, param4:Boolean = false, param5:Boolean = false) : void {
         echo(param1,param2,clone(param3),4,param4,param5);
      }
      
      public static function clone(param1:Object, param2:String = null) : Object {
         var _loc4_:ByteArray = new ByteArray();
         _loc4_.objectEncoding = 3;
         try
         {
            _loc4_.writeObject(param1);
         }
         catch(err:Error)
         {
            error("debug","failed to clone object");
            _loc6_ = null;
            return _loc6_;
         }
         _loc4_.position = 0;
         var _loc3_:Object = _loc4_.readObject();
         if(param2)
         {
            removeKeys(_loc3_,param2);
         }
         return _loc3_;
      }
      
      private static function removeKeys(param1:Object, param2:String) : void {
         var _loc5_:* = 0;
         var _loc4_:* = param1;
         for(_loc3_ in param1)
         {
            if(_loc3_ == param2)
            {
               delete param1[_loc3_];
            }
            else if(typeof param1[_loc3_] == "object")
            {
               removeKeys(param1[_loc3_],param2);
            }
            
         }
      }
      
      private static function echo(param1:String, param2:String, param3:Object, param4:int = -1, param5:Boolean = false, param6:Boolean = false) : void {
         var _loc8_:* = null;
         var _loc7_:* = null;
         var _loc9_:* = 0;
         if(!interfaces)
         {
            initialize();
         }
         if(interfaces && interfaces.length)
         {
            if(param4 < 0)
            {
               _loc8_ = param2.split(":");
               if(_loc8_.length > 1 && !isNaN(parseInt(_loc8_[0])))
               {
                  param4 = parseInt(_loc8_[0]);
                  param2 = param2.substring(param2.indexOf(":") + 1);
               }
               else
               {
                  param4 = 1;
               }
            }
            if(param5)
            {
               param4 = param4 | 16;
            }
            if(param6)
            {
               param4 = param4 | 32;
            }
            param4 = Math.max(0,Math.min(4,param4));
            if(!debugId)
            {
               debugId = (Math.random()).substr(2);
            }
            _loc7_ = new DebugItem(debugId,param1,param2,param3,param4);
            _loc9_ = 0;
            while(_loc9_ < interfaces.length)
            {
               interfaces[_loc9_].log(_loc7_);
               _loc9_++;
            }
         }
      }
      
      private static function initialize() : void {
         interfaces = new Vector.<IDebugInterface>();
         _enabled = debugSo.data.enabled;
         if(_enabled)
         {
            interfaces.push(new TraceInterface());
            interfaces.push(new SocketInterface());
            interfaces.push(new LcInterface());
         }
         if(_collectionEnabled)
         {
            interfaces.push(new CollectorInterface());
         }
      }
      
      private static function onKeyboardDownHandler(param1:KeyboardEvent) : void {
         var _loc2_:* = 0;
         if(param1.shiftKey && (param1.ctrlKey) && param1.keyCode == 32 && (_enabled))
         {
            if(interfaces && interfaces.length)
            {
               _loc2_ = 0;
               while(_loc2_ < interfaces.length)
               {
                  if(interfaces[_loc2_] is LcInterface)
                  {
                     (interfaces[_loc2_] as LcInterface).sendFocus();
                     break;
                  }
                  _loc2_++;
               }
            }
            param1.stopImmediatePropagation();
            param1.stopPropagation();
         }
      }
      
      public static function set stage(param1:Stage) : void {
         if(!_stage && param1)
         {
            _stage = param1;
            _stage.addEventListener("keyDown",onKeyboardDownHandler,false,0,true);
         }
      }
      
      public static function get debugPrefix() : String {
         return _debugPrefix;
      }
      
      public static function set debugPrefix(param1:String) : void {
         var _loc2_:* = 0;
         _debugPrefix = param1;
         if(interfaces && interfaces.length)
         {
            _loc2_ = 0;
            while(_loc2_ < interfaces.length)
            {
               if(interfaces[_loc2_] is CollectorInterface)
               {
                  (interfaces[_loc2_] as CollectorInterface).clear();
                  break;
               }
               _loc2_++;
            }
         }
         debug("Debug","set debugPrefix " + param1);
      }
      
      private static function get debugSo() : * {
         if(!_debugSo)
         {
            try
            {
               _debugSo = SharedObject.getLocal("debug","/");
            }
            catch(err:Error)
            {
               _debugSo = {};
               _debugSo.data = {"enabled":0};
               _debugSo.flush = function():void
               {
               };
            }
         }
         return _debugSo;
      }
      
      private static function get href() : String {
         var _loc1_:* = null;
         if(ExternalInterface.available)
         {
            try
            {
               _loc1_ = ExternalInterface.call("window.location.href.toString");
            }
            catch(e:Error)
            {
            }
         }
         return _loc1_?_loc1_:"";
      }
      
      public static function set enabled(param1:Boolean) : void {
         var _loc2_:* = 0;
         if(_enabled != param1)
         {
            _enabled = param1;
            if(interfaces)
            {
               if(_enabled)
               {
                  interfaces.push(new TraceInterface());
                  interfaces.push(new SocketInterface());
                  interfaces.push(new LcInterface());
               }
               else
               {
                  _loc2_ = 0;
                  while(_loc2_ < interfaces.length)
                  {
                     if(!(interfaces[_loc2_] is CollectorInterface))
                     {
                        interfaces.splice(_loc2_--,1);
                     }
                     _loc2_++;
                  }
               }
            }
         }
         debugSo.data.enabled = _enabled;
         try
         {
            debugSo.flush();
         }
         catch(err:Error)
         {
            if(_enabled)
            {
               error("Debug","unable to flush : " + err.message);
            }
         }
      }
      
      public static function get enabled() : Boolean {
         return _enabled;
      }
      
      public static function get textField() : TextField {
         return _textField;
      }
      
      public static function set textField(param1:TextField) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         _textField = param1;
         if(interfaces)
         {
            if(interfaces.length)
            {
               _loc3_ = 0;
               while(_loc3_ < interfaces.length)
               {
                  if(interfaces[_loc3_] is DisplayInterface)
                  {
                     _loc2_ = interfaces[_loc3_] as DisplayInterface;
                     break;
                  }
                  _loc3_++;
               }
            }
            if(!_loc2_)
            {
               _loc2_ = new DisplayInterface();
               interfaces.push(_loc2_);
            }
            _loc2_.textField = _textField;
         }
      }
      
      public static function get proxy() : Function {
         return _proxy;
      }
      
      public static function set proxy(param1:Function) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         _proxy = param1;
         if(interfaces)
         {
            if(interfaces.length)
            {
               _loc3_ = 0;
               while(_loc3_ < interfaces.length)
               {
                  if(interfaces[_loc3_] is ProxyInterface)
                  {
                     _loc2_ = interfaces[_loc3_] as ProxyInterface;
                     break;
                  }
                  _loc3_++;
               }
            }
            if(!_loc2_)
            {
               _loc2_ = new ProxyInterface();
               interfaces.push(_loc2_);
            }
            _loc2_.proxy = _proxy;
         }
      }
      
      public static function get collectionEnabled() : Boolean {
         return _collectionEnabled;
      }
      
      public static function set collectionEnabled(param1:Boolean) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         _collectionEnabled = param1;
         if(interfaces)
         {
            _loc3_ = 0;
            if(interfaces.length)
            {
               _loc3_ = 0;
               while(_loc3_ < interfaces.length)
               {
                  if(interfaces[_loc3_] is CollectorInterface)
                  {
                     _loc2_ = interfaces[_loc3_] as CollectorInterface;
                     break;
                  }
                  _loc3_++;
               }
            }
            if(_collectionEnabled && !_loc2_)
            {
               interfaces.push(new CollectorInterface());
            }
            else if(!_collectionEnabled && _loc2_)
            {
               _loc2_.clear();
               interfaces.splice(_loc3_,1);
            }
            
         }
      }
      
      public static function sendLogToUms(param1:ViewerUms, param2:String) : LogSender {
         var _loc4_:* = null;
         var _loc5_:* = 0;
         var _loc3_:* = null;
         if(interfaces)
         {
            if(interfaces.length)
            {
               _loc5_ = 0;
               while(_loc5_ < interfaces.length)
               {
                  if(interfaces[_loc5_] is CollectorInterface)
                  {
                     _loc4_ = interfaces[_loc5_] as CollectorInterface;
                     interfaces.splice(_loc5_,1);
                     break;
                  }
                  _loc5_++;
               }
            }
         }
         if(_loc4_)
         {
            if(!senders)
            {
               senders = new Vector.<LogSender>();
            }
            _loc3_ = new LogSender(param1,_loc4_.pack(),param2);
            _loc3_.addEventListener("destroy",onSenderDestroy);
            senders.push(_loc3_);
            return _loc3_;
         }
         return null;
      }
      
      private static function onSenderDestroy(param1:Event) : void {
         var _loc3_:* = 0;
         var _loc2_:LogSender = param1.target as LogSender;
         if(_loc2_)
         {
            _loc2_.removeEventListener("destroy",onSenderDestroy);
            if(senders && senders.length)
            {
               _loc3_ = 0;
               while(_loc3_ < senders.length)
               {
                  if(senders[_loc3_] == _loc2_)
                  {
                     senders.splice(_loc3_,1);
                     break;
                  }
                  _loc3_++;
               }
            }
         }
      }
   }
}
