package tv.ustream.debug
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.ByteArray;
   import tv.ustream.net.ViewerUms;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.GoogleAnalytics;
   import tv.ustream.tools.StringUtils;
   
   public class LogSender extends Dispatcher
   {
      
      public function LogSender(param1:ViewerUms, param2:Vector.<ByteArray>, param3:String) {
         var _loc4_:* = 0;
         super();
         GoogleAnalytics.track("Player","Error",param3);
         _hash = StringUtils.random(32);
         _sendIndex = -1;
         _sendBuffer = param2;
         _reason = param3;
         if(_sendBuffer && _sendBuffer.length)
         {
            _loc4_ = 0;
            while(_loc4_ < _sendBuffer.length)
            {
               _sendSize = _sendSize + _sendBuffer[_loc4_].length;
               _loc4_++;
            }
         }
         _ums = param1;
         if(_ums && _ums.connected)
         {
            _ums.addEventListener("uploadResponse",onUploadResponse);
            sendPacket();
         }
      }
      
      public static const START:String = "start";
      
      public static const COMPLETE:String = "complete";
      
      public static const FAILED:String = "failed";
      
      private var _hash:String;
      
      private var _sendIndex:int;
      
      private var _sendBuffer:Vector.<ByteArray>;
      
      private var _sendSize:uint = 0;
      
      private var _ums:ViewerUms;
      
      private var _reason:String;
      
      private function sendPacket() : void {
         var _loc1_:Object = {};
         _loc1_.messageId = _hash;
         _loc1_.messageIndex = _sendIndex;
         _loc1_.messageType = "viewer-tracelog";
         _loc1_.reason = _reason;
         _loc1_.messageCount = _sendBuffer.length;
         _loc1_.totalSize = _sendSize;
         if(_sendIndex >= 0)
         {
            _loc1_.rawData = _sendBuffer[_sendIndex];
         }
         Debug.debug("LogSender","sendPacket : " + _sendIndex,_loc1_);
         _ums.sendDataPacket(_loc1_);
      }
      
      private function onUploadResponse(param1:DynamicEvent) : void {
         var _loc2_:Object = param1.data.response;
         Debug.debug("LogSender","onUploadResponse",_loc2_);
         if(_loc2_ && _loc2_.messageId == _hash)
         {
            if(_loc2_.accepted)
            {
               _sendIndex = _sendIndex + 1;
               if(_sendIndex < _sendBuffer.length)
               {
                  if(!_sendIndex)
                  {
                     dispatch("start");
                  }
                  sendPacket();
               }
               else
               {
                  dispatch("complete",false,{"hash":_hash});
                  Debug.info("LogSender","finished");
                  destroy();
               }
            }
            else
            {
               Debug.info("LogSender","rejected");
               dispatch("failed");
               destroy();
            }
         }
      }
      
      override public function destroy(... rest) : * {
         if(_ums)
         {
            _ums.removeEventListener("uploadResponse",onUploadResponse);
            _ums = null;
         }
         return super.destroy();
      }
   }
}
