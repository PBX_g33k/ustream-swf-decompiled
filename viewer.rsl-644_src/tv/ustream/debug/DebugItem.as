package tv.ustream.debug
{
   import tv.ustream.tools.StringUtils;
   import flash.utils.ByteArray;
   
   public class DebugItem extends Object
   {
      
      public function DebugItem(param1:String = "", param2:String = "", param3:String = "", param4:Object = null, param5:uint = 0, param6:Boolean = false) {
         super();
         this.id = param1;
         this.data = param4;
         this.source = param2;
         this.line = param3;
         this.level = param5;
         this.level = this.level | (param6) << 4;
         date = new Date();
      }
      
      public var source:String;
      
      public var line:String;
      
      public var data:Object;
      
      public var level:uint;
      
      public var id:String;
      
      protected var _date:Date;
      
      protected var _timeStamp:String;
      
      public function get date() : Date {
         return _date;
      }
      
      public function set date(param1:Date) : void {
         _date = param1;
         _timeStamp = StringUtils.getFormatDateString("[%HOUR%:%MINUTES%:%SECONDS%.%MILLISECONDS%]",_date);
      }
      
      public function get timeStamp() : String {
         return _timeStamp;
      }
      
      public function cloneToByteArray(param1:uint = 3) : ByteArray {
         var _loc2_:ByteArray = new ByteArray();
         _loc2_.objectEncoding = param1;
         var _loc3_:Object = {};
         _loc3_.source = source;
         _loc3_.line = line;
         _loc3_.data = data;
         _loc3_.level = level;
         _loc3_.id = id;
         _loc3_.date = date;
         _loc2_.writeObject(_loc3_);
         return _loc2_;
      }
   }
}
