package tv.ustream.net
{
   import flash.utils.ByteArray;
   import tv.ustream.flv.FlvParser;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.flv.FlvTag;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   import tv.ustream.flv.FlvHeader;
   
   public class FragmentArray extends ByteArray
   {
      
      public function FragmentArray(param1:uint, param2:Boolean = true) {
         super();
         this.parseInput = param2;
         if(this.parseInput)
         {
            parser = new FlvParser(null,false);
            parser.addEventListener("malformedData",onParserMalformedData);
            parser.addEventListener("audio",onParserPacket);
            parser.addEventListener("meta",onParserPacket);
            parser.addEventListener("video",onParserPacket);
            parser.feed(new FlvHeader());
         }
         _size = param1;
      }
      
      private var parser:FlvParser;
      
      private var error:Boolean = false;
      
      private var _length:uint = 0;
      
      private var _size:uint = 0;
      
      private var parseInput:Boolean = false;
      
      public function get size() : uint {
         return _size;
      }
      
      public function get appendLength() : uint {
         return _length;
      }
      
      private function onParserPacket(param1:DynamicEvent) : void {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         var _loc4_:FlvTag = param1.data as FlvTag;
         if(_loc4_)
         {
            _loc2_ = position;
            position = length;
            _loc3_ = _loc4_.position;
            _loc4_.position = 0;
            writeBytes(_loc4_,0,_loc4_.length);
            _loc4_.position = _loc3_;
            position = _loc2_;
         }
      }
      
      private function onParserMalformedData(param1:Event) : void {
         error = true;
      }
      
      public function appendBytes(param1:ByteArray) : Boolean {
         var _loc2_:* = 0;
         _length = _length + param1.length;
         if(!parseInput)
         {
            _loc2_ = position;
            position = length;
         }
         var _loc3_:uint = param1.position;
         param1.position = 0;
         if(!parseInput)
         {
            writeBytes(param1,0,param1.length);
         }
         else
         {
            parser.feed(param1);
         }
         param1.position = _loc3_;
         if(!parseInput)
         {
            position = _loc2_;
         }
         return !error;
      }
      
      private function echo(param1:String, param2:int = 1) : void {
         Debug.echo("[ FRAGMENTARRAY ] " + param1,param2);
      }
   }
}
