package tv.ustream.net
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.Timer;
   import tv.ustream.tools.Shared;
   import tv.ustream.net.ums.ServerList;
   import tv.ustream.net.interfaces.ICommunicator;
   import flash.events.TimerEvent;
   import tv.ustream.tools.This;
   import tv.ustream.debug.Debug;
   import flash.events.Event;
   import tv.ustream.tools.StringUtils;
   
   public class Ums extends Dispatcher
   {
      
      public function Ums() {
         var _loc1_:* = NaN;
         ports = [1935,80];
         super();
         if(_hasLongPoll)
         {
            _listOfCommunicationTypes = [0,2,1];
         }
         else
         {
            _listOfCommunicationTypes = [0,1];
         }
         _id = StringUtils.random(8);
         _randomId = _id;
         shared = new Shared(null,null,"ums");
         if(shared.svAuthTimestamp)
         {
            _loc1_ = new Date().getTime();
            if(shared.svAuthTimestamp - _loc1_ > 7200000)
            {
               delete shared.svAuthTimestamp;
            }
         }
         _communicationIndex = 0;
         _communicationType = _listOfCommunicationTypes[_communicationIndex];
         createCommunication();
         addEventListener("reconnect",onForcedReconnect);
      }
      
      public static const RECONNECTING:String = "reconnection";
      
      public static const CONNECT_FAILED:String = "connectFailed";
      
      public static const REJECT:String = "reject";
      
      public static const DESTROY:String = "destroy";
      
      public static const DISCONNECTED:String = "disconnected";
      
      public static const CONNECTED:String = "connected";
      
      public static const UPLOAD_RESPONSE:String = "uploadResponse";
      
      public static const COMMUNICATION_TYPE_CONNECTION:int = 0;
      
      public static const COMMUNICATION_TYPE_WEB_SOCKET:int = 1;
      
      public static const COMMUNICATION_TYPE_LONG_POLL:int = 2;
      
      protected static const CLUSTER_LIVE:String = "live";
      
      protected static const CLUSTER_PPV:String = "ppv";
      
      private const BALANCER_FAIL_LIMIT:uint = 3;
      
      private const RETRY_MIN:uint = 1000;
      
      private const RETRY_MAX:uint = 30000;
      
      private const RETRY_STEP:uint = 2000;
      
      private const RETRY_SCATTER:uint = 5000;
      
      protected var w84Online:Boolean = true;
      
      private var plannedDisconnect:Boolean = false;
      
      protected var reconnecting:Boolean = false;
      
      private var cooldownTimer:Timer;
      
      protected var shared:Shared;
      
      private var _randomId:String;
      
      protected var _cluster:String = "live";
      
      protected var retryCount:uint = 0;
      
      protected var _hasLongPoll:Boolean = false;
      
      protected var _connected:Boolean;
      
      protected var ports:Array;
      
      protected var forcedIp:String;
      
      protected var _ip:String;
      
      protected var _ipList:ServerList;
      
      private var balancerFailed:uint = 0;
      
      protected var umsConnection:ICommunicator;
      
      private var _listOfCommunicationTypes:Array;
      
      private var _communicationIndex:int;
      
      protected var _communicationType:int;
      
      private function createCommunication() : void {
         if(umsConnection)
         {
            killCommunication();
         }
      }
      
      protected function clientSetup() : void {
         if(umsConnection)
         {
            umsConnection.client = 
               {
                  "moduleInfo":onUmsModuleInfo,
                  "goOffline":onUmsGoOffline,
                  "reject":onUmsReject,
                  "onSvHashAuth":onSvHashAuth
               };
         }
      }
      
      private function killCommunication() : void {
         if(umsConnection)
         {
            umsConnection.removeEventListener("connected",onConnected);
            umsConnection.removeEventListener("disconnected",onDisconnected);
            umsConnection.removeEventListener("failed",onFailed);
            umsConnection.kill();
            umsConnection = null;
         }
      }
      
      protected function connect(... rest) : void {
         var _loc2_:* = null;
         var _loc3_:* = null;
         var _loc5_:* = null;
         var _loc4_:* = null;
         if(umsConnection && umsConnection.connected)
         {
            plannedDisconnect = true;
            umsConnection.close();
         }
         if(rest[0] is TimerEvent)
         {
            (rest[0] as TimerEvent).target.removeEventListener("timerComplete",connect);
         }
         if(active)
         {
            _loc2_ = collectArgs();
         }
         if(active)
         {
            return;
         }
      }
      
      private function onServerListComplete(param1:Event) : void {
         Debug.debug("ums","onServerListComplete");
         _ipList.removeEventListener("complete",onServerListComplete);
         connect();
      }
      
      private function onForcedReconnect(param1:Event) : void {
         Debug.debug("UMS","onForcedReconnect");
         if(umsConnection && umsConnection.connected)
         {
            umsConnection.close();
         }
      }
      
      protected function onConnected(... rest) : void {
         if(_ip.indexOf("ums.ustream.tv") != -1)
         {
            balancerFailed = 0;
         }
         _ip = umsConnection.url;
         _connected = true;
         dispatch("connected");
         retryCount = 0;
      }
      
      protected function onDisconnected(param1:Event = null) : void {
         _connected = false;
         w84Online = true;
         if(!plannedDisconnect)
         {
            reconnecting = true;
            dispatch("disconnected");
            onFailed(param1);
         }
         _ip = "";
         plannedDisconnect = false;
      }
      
      protected function onFailed(param1:Event = null) : void {
         if(_ip.indexOf("ums.ustream.tv") != -1)
         {
            balancerFailed = balancerFailed + 1;
         }
         if(!useBalancer && !_ipList)
         {
            _ipList = new ServerList(_cluster);
         }
         retryCount = retryCount + 1;
         reportFailed(param1);
         _communicationIndex = nextCommunicationIndex();
         _communicationType = _listOfCommunicationTypes[_communicationIndex];
         createCommunication();
         startReconnect(param1);
      }
      
      private function nextCommunicationIndex() : int {
         var _loc1_:int = _communicationIndex + 1;
         _loc1_ = _loc1_ > _listOfCommunicationTypes.length - 1?0:_loc1_;
         return _loc1_;
      }
      
      protected function startReconnect(param1:Event = null) : void {
         var _loc2_:Timer = new Timer(reconnectDelay,1);
         _loc2_.addEventListener("timerComplete",connect);
         _loc2_.start();
      }
      
      protected function reportFailed(param1:Event = null) : void {
         Debug.debug("UMS","failed, retryCount : " + retryCount);
         if(param1 && param1.target is ICommunicator)
         {
            Debug.debug("UMS","ip : " + (param1.target as ICommunicator).url);
         }
      }
      
      protected function getLiveUrl() : String {
         var _loc1_:Array = [];
         _loc1_.push("r" + Math.floor(Math.random() * Math.pow(10,15)));
         _loc1_.push("ums.ustream.tv");
         return _loc1_.join(".") + "/ustream";
      }
      
      protected function collectArgs() : Object {
         var _loc1_:Object = {};
         return _loc1_;
      }
      
      protected function setCluster(param1:String) : void {
         if(_cluster != param1)
         {
            Debug.debug("UMS","cluster set to " + param1);
            _cluster = param1;
            if(_ipList)
            {
               ipListAddEventListener();
               _ipList.cluster = param1;
            }
            else
            {
               connect();
            }
         }
      }
      
      private function ipListAddEventListener() : void {
         if(_ipList && !_ipList.hasEventListener("complete"))
         {
            _ipList.addEventListener("complete",onServerListComplete);
         }
      }
      
      override public function destroy(... rest) : * {
         terminate();
      }
      
      protected function terminate(param1:Event = null) : * {
         if(_ipList)
         {
            _ipList.removeEventListener("complete",onServerListComplete);
            _ipList = _ipList.destroy();
         }
         removeEventListener("reconnect",onForcedReconnect);
         killCommunication();
         _connected = false;
         return super.destroy();
      }
      
      protected function onUmsModuleInfo(param1:Object) : void {
      }
      
      protected function onUmsGoOffline(... rest) : void {
         dispatch("goOffline");
      }
      
      protected function onUmsReject(param1:Object) : void {
         Debug.debug("UMS","onUmsReject",param1);
      }
      
      protected function onSvHashAuth(param1:Boolean) : void {
         var _loc2_:* = NaN;
         if(param1)
         {
            _loc2_ = new Date().getTime();
            shared.svAuthTimestamp = _loc2_;
         }
      }
      
      private function get useBalancer() : Boolean {
         return balancerFailed < 3 || (This.secure);
      }
      
      protected function get reconnectDelay() : Number {
         var _loc1_:int = 1000 + Math.min(30000 - 1000,2000 * retryCount) + Math.floor(Math.random() * 5000);
         echo("reconnectDelay is " + _loc1_);
         return _loc1_;
      }
      
      public function get randomId() : String {
         return _randomId;
      }
      
      public function get communicationType() : int {
         return _communicationType;
      }
      
      public function get connected() : Boolean {
         return _connected;
      }
      
      public function get ip() : String {
         return _ip;
      }
      
      public function get cluster() : String {
         return _cluster;
      }
      
      public function get communicator() : ICommunicator {
         return umsConnection;
      }
   }
}
