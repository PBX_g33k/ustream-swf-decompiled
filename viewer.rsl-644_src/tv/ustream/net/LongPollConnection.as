package tv.ustream.net
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.net.interfaces.ICommunicator;
   import flash.net.URLStream;
   import flash.utils.Timer;
   import flash.net.Responder;
   import flash.net.URLVariables;
   import flash.net.URLRequest;
   import com.brokenfunction.json.encodeJson;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import com.brokenfunction.json.decodeJson;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.events.ProgressEvent;
   import flash.events.HTTPStatusEvent;
   
   public dynamic class LongPollConnection extends Dispatcher implements ICommunicator
   {
      
      public function LongPollConnection(param1:String = null) {
         super();
         _queue = [];
         _url = param1;
      }
      
      public static var SEND_INTERVAL:uint = 5000;
      
      private var _urlStream:URLStream;
      
      private var _connected:Boolean;
      
      private var _connectParams:Object;
      
      private var _client:Object;
      
      private var _killing:Boolean = false;
      
      private var _url:String;
      
      private var _ports:Array;
      
      private var _format:String = "json";
      
      private var _queue:Array;
      
      private var _sendTimer:Timer;
      
      public function call(param1:String, param2:Responder, ... rest) : void {
         _queue.push(
            {
               "cmd":param1,
               "args":rest
            });
      }
      
      public function close() : void {
         echo("close");
      }
      
      public function connect(param1:String, ... rest) : void {
         echo("connect");
         _url = param1;
         if(_urlStream != null)
         {
            destroyURLStream();
         }
         createURLStream();
         _connected = false;
         _connectParams = rest && !(rest[0] == null)?rest[0]:{};
         var _loc3_:URLVariables = new URLVariables();
         var _loc6_:* = 0;
         var _loc5_:* = _connectParams;
         for(_loc4_ in _connectParams)
         {
            _loc3_[_loc4_] = _connectParams[_loc4_];
         }
         loadData(_loc3_);
      }
      
      private function loadData(param1:URLVariables) : void {
         var _loc2_:URLRequest = new URLRequest();
         _loc2_.method = "POST";
         _loc2_.url = getURL();
         _loc2_.data = param1;
         _urlStream.load(_loc2_);
      }
      
      private function sendMessage(param1:String = "", param2:Array = null) : void {
         echo("sendMessage");
         var _loc4_:Array = [];
         if(param1)
         {
            _loc4_.push(
               {
                  "cmd":param1,
                  "args":param2
               });
         }
         if(_queue && _queue.length)
         {
            while(_queue.length)
            {
               _loc4_.push(_queue.shift());
            }
         }
         var _loc3_:URLVariables = new URLVariables();
         _loc3_.cmds = encodeJson(_loc4_);
         loadData(_loc3_);
      }
      
      private function onSendTimerComplete(param1:TimerEvent) : void {
         destroySendTimer();
         sendMessage();
      }
      
      private function createSendTimer() : void {
         if(_sendTimer)
         {
            destroySendTimer();
         }
         _sendTimer = new Timer(SEND_INTERVAL,1);
         _sendTimer.addEventListener("timerComplete",onSendTimerComplete);
      }
      
      private function destroySendTimer() : void {
         if(_sendTimer)
         {
            _sendTimer.removeEventListener("timerComplete",onSendTimerComplete);
            _sendTimer.stop();
            _sendTimer = null;
         }
      }
      
      private function createURLStream() : void {
         _urlStream = new URLStream();
         _urlStream.addEventListener("httpStatus",onHTTPStatus);
         _urlStream.addEventListener("progress",onProgress);
         _urlStream.addEventListener("complete",onComplete);
         _urlStream.addEventListener("open",onOpen);
         _urlStream.addEventListener("close",onClose);
         _urlStream.addEventListener("securityError",onSecurityError);
         _urlStream.addEventListener("ioError",onIOError);
      }
      
      private function destroyURLStream() : void {
         echo("destroyLongPoll");
         _urlStream.removeEventListener("httpStatus",onHTTPStatus);
         _urlStream.removeEventListener("progress",onProgress);
         _urlStream.removeEventListener("complete",onComplete);
         _urlStream.removeEventListener("open",onOpen);
         _urlStream.removeEventListener("securityError",onSecurityError);
         _urlStream.removeEventListener("ioError",onIOError);
         _urlStream.close();
         _urlStream = null;
      }
      
      private function onComplete(param1:Event) : void {
         var _loc2_:* = null;
         var _loc5_:* = 0;
         var _loc3_:* = null;
         var _loc7_:* = null;
         var _loc6_:* = null;
         echo("onComplete");
         _loc2_ = _urlStream.readUTFBytes(_urlStream.bytesAvailable);
         var _loc4_:Object = decodeJson(_loc2_);
         _loc5_ = 0;
         while(_loc5_ < _loc4_.length)
         {
            _loc3_ = _loc4_[_loc5_].functionName;
            _loc7_ = _loc4_[_loc5_].parameters;
            trace("3:","funciontName:",_loc3_,"parameters:",_loc7_);
            if(client && client[_loc3_] && client[_loc3_] is Function)
            {
               _loc6_ = client[_loc3_] as Function;
               _loc6_.apply(client,_loc7_);
            }
            else if(this[_loc3_] && this[_loc3_] is Function)
            {
               _loc6_ = this[_loc3_] as Function;
               _loc6_.apply(this,_loc7_);
            }
            
            _loc5_++;
         }
         if(!_connected)
         {
            _connected = true;
            dispatch("connected");
         }
         if(!_sendTimer)
         {
            createSendTimer();
         }
         _sendTimer.start();
      }
      
      private function ping(... rest) : void {
         _queue.push(
            {
               "cmd":"pong",
               "args":rest
            });
      }
      
      private function onIOError(param1:IOErrorEvent) : void {
         echo("onIOError");
      }
      
      private function onSecurityError(param1:SecurityErrorEvent) : void {
         echo("onSecurityError  " + param1.type);
      }
      
      private function onOpen(param1:Event) : void {
         echo("onOpen");
      }
      
      private function onProgress(param1:ProgressEvent) : void {
         echo("onProgress");
      }
      
      private function onHTTPStatus(param1:HTTPStatusEvent) : void {
         echo("onHTTPStatus : " + param1.status);
      }
      
      private function onClose(param1:Event) : void {
         echo("onClose");
         if(_killing)
         {
            destroyURLStream();
         }
      }
      
      public function kill() : void {
         echo("kill");
         _active = false;
         _killing = true;
         _urlStream.close();
      }
      
      private function getURL() : String {
         var _loc1_:String = _url + "?application=" + _connectParams.application + "&media=" + _connectParams.media + "&format=" + _format;
         return _loc1_;
      }
      
      public function get connected() : Boolean {
         return _connected;
      }
      
      public function get client() : Object {
         return _client;
      }
      
      public function set client(param1:Object) : void {
         _client = param1;
      }
      
      public function get ports() : Array {
         return _ports;
      }
      
      public function set ports(param1:Array) : void {
         _ports = param1;
      }
      
      public function get uri() : String {
         return _url;
      }
      
      public function get url() : String {
         return _url;
      }
   }
}
