package tv.ustream.net
{
   import flash.net.NetStream;
   import tv.ustream.flv.FlvParser;
   import flash.display.Sprite;
   import tv.ustream.debug.Debug;
   import flash.utils.ByteArray;
   import flash.net.NetStreamPlayOptions;
   import flash.net.URLStream;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.net.URLRequest;
   import flash.events.HTTPStatusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.events.ProgressEvent;
   import flash.utils.getTimer;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.flv.FlvTag;
   import flash.events.NetStatusEvent;
   import flash.system.System;
   import flash.net.NetStreamInfo;
   import flash.net.NetConnection;
   import tv.ustream.tools.StringUtils;
   
   public class FragmentStream extends NetStream
   {
      
      public function FragmentStream(param1:NetConnection, param2:String = "connectToFMS") {
         super(param1,param2);
         parser = new FlvParser(null,false);
         parser.addEventListener("exceededMetadataParseLimit",onMetaParseExceeded);
         parser.addEventListener("malformedData",onParserMalformedData);
         parser.addEventListener("metadata",onMetaData);
         parser.addEventListener("meta",onFlvTag);
         parser.addEventListener("audio",onFlvTag);
         parser.addEventListener("video",onFlvTag);
         ticker = new Sprite();
         addEventListener("netStatus",onNetStatus,false,2147483647);
         _randomId = StringUtils.random(3);
      }
      
      public static const INIT_STARTED:String = "FragmentStream.Init.Started";
      
      public static const INIT_COMPLETE:String = "FragmentStream.Init.Complete";
      
      public static const JUMPSTART_NOTIFY:String = "FragmentStream.JumpStart.Notify";
      
      public static const PACKET_ADDED:String = "FragmentStream.Packet.Added";
      
      public static const PACKET_UPDATED:String = "FragmentStream.Packet.Updated";
      
      public static const PACKET_DROPPED:String = "FragmentStream.Packet.Dropped";
      
      public static const NO_METADATA:String = "FragmentStream.Metadata.Missing";
      
      public static const NO_KEYFRAMES_IN_METADATA:String = "FragmentStream.Metadata.NoKeyframes";
      
      public static const KEYFRAME_INTERVAL_EXCEEDS_LIMIT:String = "FragmentStream.Metadata.IntervalExceedsLimit";
      
      public static const LOAD_ERROR:String = "FragmentStream.Load.Error";
      
      private const NETSTREAM_BUFFER_EMPTY:String = "NetStream.Buffer.Empty";
      
      private const NETSTREAM_BUFFER_FULL:String = "NetStream.Buffer.Full";
      
      private const BUFFER_TIME_INIT:Number = 0.5;
      
      private const BUFFER_TIME_RUNNING:Number = 1;
      
      private const BUFFER_TIME_SEEK:Number = 0.1;
      
      public var seekParameter:String = "fs";
      
      private var parser:FlvParser;
      
      private var ticker:Sprite;
      
      private var _randomId:String;
      
      private var _bufferEmpty:Boolean;
      
      private var playing:Boolean = false;
      
      private var streamName:String;
      
      private var playOffset:Number = 0;
      
      private var delayedSeek:Number = -1;
      
      private var delayedPause:Boolean = false;
      
      override public function play(... rest) : void {
         Debug.debug("FRAGMENTSTREAM","public play");
         super.play(null);
         preBuffer = null;
         appendBytesAction("resetBegin");
         Debug.debug("FRAGMENTSTREAM","appended RESET_BEGIN");
         var _loc2_:ByteArray = new ByteArray();
         _loc2_.writeByte(70);
         _loc2_.writeByte(76);
         _loc2_.writeByte(86);
         _loc2_.writeByte(1);
         _loc2_.writeByte(5);
         _loc2_.writeByte(0);
         _loc2_.writeByte(0);
         _loc2_.writeByte(0);
         _loc2_.writeByte(9);
         _loc2_.writeByte(0);
         _loc2_.writeByte(0);
         _loc2_.writeByte(0);
         _loc2_.writeByte(0);
         appendBytes(_loc2_);
         Debug.debug("FRAGMENTSTREAM","added FLV header");
         playing = true;
         playBuffer = {};
         streamName = rest[0];
         if(streamName)
         {
            Debug.debug("FRAGMENTSTREAM","streamName : " + streamName);
            loadIndex = 0;
            keyIndex = 0;
            playOffset = 0;
            keyTimes = null;
            keyBytes = null;
            loaderCreateEnabled = true;
            load();
            initProgress = 0;
            if(!ticker.hasEventListener("enterFrame"))
            {
               Debug.warn("FRAGMENTSTREAM","added ENTER_FRAME");
               ticker.addEventListener("enterFrame",onTickerFrame);
            }
            dispatchNetStatusEvent("FragmentStream.Init.Started");
         }
      }
      
      override public function play2(param1:NetStreamPlayOptions) : void {
         if(param1.oldStreamName == streamName)
         {
            if(param1.transition != "switch")
            {
               if(param1.transition == "swap")
               {
                  streamName = param1.streamName;
                  destroyLoader();
               }
               else
               {
                  throw new ArgumentError("unsupported transition type : " + param1.transition);
               }
            }
         }
         else
         {
            Debug.debug("FRAGMENTSTREAM","param.oldStreamName : " + param1.oldStreamName);
            Debug.debug("FRAGMENTSTREAM","streamName : " + streamName);
            Debug.debug("FRAGMENTSTREAM","streamName mismatch, playing new streamName");
            play(param1.streamName);
         }
      }
      
      override public function resume() : void {
         Debug.debug("FRAGMENTSTREAM","public resume");
         if(!playing)
         {
            playing = true;
            if(!delayedPause)
            {
               super.resume();
            }
            else
            {
               delayedPause = false;
            }
         }
      }
      
      override public function pause() : void {
         Debug.debug("FRAGMENTSTREAM","public pause");
         if(playing)
         {
            playing = false;
            Debug.debug("FRAGMENTSTREAM","keyIndex : " + keyIndex + ", indexLimit : " + indexLimit);
            if(keyIndex >= indexLimit)
            {
               super.pause();
            }
            else
            {
               delayedPause = true;
            }
         }
      }
      
      override public function seek(param1:Number) : void {
         Debug.debug("FRAGMENTSTREAM","public seek " + param1);
         if(keyTimes && keyIndex >= indexLimit && initProgress > 1)
         {
            appendCount = indexLimit + 1;
            Debug.warn("FRAGMENTSTREAM","removed ENTER_FRAME");
            ticker.removeEventListener("enterFrame",onTickerFrame);
            keyIndex = getIndexForOffset(param1);
            .super.bufferTime = 0.1;
            playOffset = keyTimes[keyIndex];
            setCurrentFragment();
            if(!playBuffer[keyBytes[keyIndex]])
            {
               Debug.debug("FRAGMENTSTREAM","keyframe not present, loading");
               loaderCreateEnabled = true;
               load(keyBytes[keyIndex]);
               loadIndex = keyIndex;
            }
            super.seek(0);
            delayedSeek = -1;
         }
         else
         {
            delayedSeek = param1;
         }
      }
      
      override public function close() : void {
         Debug.debug("FRAGMENTSTREAM","public close");
         Debug.warn("FRAGMENTSTREAM","removed ENTER_FRAME");
         ticker.removeEventListener("enterFrame",onTickerFrame);
         destroyLoader();
         dropAllPackets();
         super.close();
      }
      
      override public function set bufferTime(param1:Number) : void {
         .super.bufferTime = keyIndex <= indexLimit?0.5:1.0;
      }
      
      override public function get time() : Number {
         return playOffset + super.time;
      }
      
      override public function get bytesLoaded() : uint {
         return 0;
      }
      
      override public function get bytesTotal() : uint {
         return 0;
      }
      
      override public function receiveAudio(param1:Boolean) : void {
      }
      
      override public function receiveVideo(param1:Boolean) : void {
      }
      
      private var loader:URLStream;
      
      private var loadPosition:Number = 0;
      
      private var loadStart:Number = 0;
      
      private var loadTotal:Number = 0;
      
      private var _usage:uint = 0;
      
      private var progressList:Array;
      
      private var loadIndex:uint = 0;
      
      private var metaSize:uint = 0;
      
      private var loadedTags:uint = 0;
      
      private var loaderCreateEnabled:Boolean = false;
      
      public function get memoryUsage() : Object {
         var _loc1_:Object = {};
         _loc1_.packets = _usage;
         _loc1_.buffer = parser.cacheSize;
         return _loc1_;
      }
      
      private function initLoader() : void {
         var _loc1_:* = null;
         if(!loader)
         {
            Debug.debug("FRAGMENTSTREAM","create loader");
            loader = new ThrottledURLStream();
            loader.addEventListener("complete",onLoaderComplete);
            loader.addEventListener("httpStatus",onLoaderHttpStatus);
            loader.addEventListener("ioError",onLoaderIoError);
            loader.addEventListener("securityError",onLoaderSecurityError);
            loader.addEventListener("progress",onLoaderProgress);
            parser.clearCache();
            killUnfinishedPackets();
            loadedTags = 0;
            _loc1_ = new Timer(5000,1);
            _loc1_.addEventListener("timerComplete",onLoaderDelayTimerComplete);
            _loc1_.start();
            loaderCreateEnabled = false;
         }
      }
      
      private function onLoaderDelayTimerComplete(param1:TimerEvent) : void {
         var _loc2_:Timer = param1.target as Timer;
         _loc2_.removeEventListener("timerComplete",onLoaderDelayTimerComplete);
         loaderCreateEnabled = true;
      }
      
      private function destroyLoader() : void {
         if(loader)
         {
            Debug.debug("FRAGMENTSTREAM","destroy loader");
            try
            {
               loader.close();
            }
            catch(err:Error)
            {
            }
            loader.removeEventListener("complete",onLoaderComplete);
            loader.removeEventListener("httpStatus",onLoaderHttpStatus);
            loader.removeEventListener("ioError",onLoaderIoError);
            loader.removeEventListener("securityError",onLoaderSecurityError);
            loader.removeEventListener("progress",onLoaderProgress);
            loader = null;
         }
         Debug.debug("FRAGMENTSTREAM","loaded tags : " + loadedTags);
         loadTotal = 0;
         progressList = null;
         parser.clearCache();
         killUnfinishedPackets();
      }
      
      private function load(param1:Number = 0) : void {
         Debug.debug("FRAGMENTSTREAM","load " + param1);
         destroyLoader();
         initLoader();
         loadPosition = param1;
         loadStart = param1;
         var _loc2_:String = streamName;
         if(param1)
         {
            _loc2_ = _loc2_ + ((_loc2_.indexOf("?") == -1?"?":"&") + seekParameter + "=" + param1);
         }
         Debug.debug("FRAGMENTSTREAM","load : " + _loc2_);
         loader.load(new URLRequest(_loc2_));
      }
      
      private function onLoaderHttpStatus(param1:HTTPStatusEvent) : void {
         Debug.debug("FRAGMENTSTREAM","onLoaderHttpStatus : " + param1.status);
      }
      
      private function onLoaderIoError(param1:IOErrorEvent) : void {
         Debug.debug("FRAGMENTSTREAM","onLoaderIoError",3);
         dispatchNetStatusEvent("FragmentStream.Load.Error","error");
         destroyLoader();
      }
      
      private function onLoaderSecurityError(param1:SecurityErrorEvent) : void {
         Debug.debug("FRAGMENTSTREAM","onLoaderSecurityError",3);
         dispatchNetStatusEvent("FragmentStream.Load.Error","error");
         destroyLoader();
      }
      
      private function onLoaderProgress(param1:ProgressEvent) : void {
         var _loc2_:* = null;
         if(loader.bytesAvailable)
         {
            _totalByteCount = _totalByteCount + loader.bytesAvailable;
            if(!progressList)
            {
               progressList = [];
            }
            progressList.push(
               {
                  "bytes":loader.bytesAvailable,
                  "time":getTimer()
               });
            if(!loadTotal)
            {
               loadTotal = param1.bytesTotal - (loadIndex?13:0);
            }
            _loc2_ = new ByteArray();
            loader.readBytes(_loc2_,0,loader.bytesAvailable);
            parser.feed(_loc2_);
         }
      }
      
      private function onLoaderComplete(param1:Event) : void {
         Debug.debug("FRAGMENTSTREAM","onLoaderComplete");
         destroyLoader();
      }
      
      public function get bytesPerSec() : uint {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         if(progressList)
         {
            while(progressList[0] && getTimer() - progressList[0].time > 1000)
            {
               progressList.splice(0,1);
            }
            _loc1_ = 0;
            _loc2_ = 0;
            while(_loc2_ < progressList.length)
            {
               _loc1_ = _loc1_ + progressList[_loc2_].bytes;
               _loc2_++;
            }
            if(time > 0)
            {
               return _loc1_;
            }
         }
         return 0;
      }
      
      protected function onFlvTag(param1:DynamicEvent) : void {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         var _loc5_:* = null;
         var _loc4_:FlvTag = param1.data as FlvTag;
         if(_loc4_ && keyBytes)
         {
            loadedTags = loadedTags + 1;
            _loc2_ = keyBytes[loadIndex];
            _loc3_ = loadIndex + 1 == keyBytes.length?loadPosition + loadTotal - keyBytes[loadIndex]:keyBytes[loadIndex + 1] - keyBytes[loadIndex];
            if(!preBuffer)
            {
               preBuffer = new FragmentArray(_loc2_ - 13,true);
            }
            if(preBuffer.length < preBuffer.size)
            {
               preBuffer.appendBytes(_loc4_);
               appendBytes(_loc4_);
            }
            else
            {
               _loc5_ = playBuffer[keyBytes[loadIndex]];
               if(_loc5_ && _loc5_.length == _loc5_.size)
               {
                  Debug.debug("FRAGMENTSTREAM","existing packet, stop loading");
                  destroyLoader();
               }
               else
               {
                  if(!_loc5_)
                  {
                     _loc6_ = new FragmentArray(_loc3_);
                     playBuffer[keyBytes[loadIndex]] = _loc6_;
                     _loc5_ = _loc6_;
                     dispatchEvent(new Event("FragmentStream.Packet.Added"));
                     Debug.debug("FRAGMENTSTREAM","added packet : " + loadIndex + " " + loadPosition + " " + _loc5_.size);
                  }
                  _loc5_.appendBytes(_loc4_);
                  _usage = _usage + _loc4_.length;
                  if(_loc5_.size == _loc5_.length)
                  {
                     loadIndex = loadIndex + 1;
                  }
                  if(_loc5_.length > _loc5_.size)
                  {
                     Debug.debug("FRAGMENTSTREAM","packet length exceeds packet size",3);
                     destroyLoader();
                  }
               }
               setCurrentFragment();
               if(_usage > _maxMemoryUsage)
               {
                  dropUnneededPackets();
               }
               if(_usage > _maxMemoryUsage || getBufferRemainingTime() > 60)
               {
                  Debug.debug("FRAGMENTSTREAM","remaining time exceeds MAX_PRELOAD_LIMIT");
                  destroyLoader();
               }
            }
         }
      }
      
      private function onParserMalformedData(param1:Event) : void {
         Debug.debug("FRAGMENTSTREAM","parser malformed data",2);
         destroyLoader();
      }
      
      private var indexLimit:Number = Infinity;
      
      private function onMetaData(param1:Event) : void {
         var _loc2_:* = 0;
         var _loc3_:* = NaN;
         if(param1 is DynamicEvent && (param1 as DynamicEvent).data is ByteArray)
         {
            metaSize = ((param1 as DynamicEvent).data as ByteArray).length;
            appendBytes((param1 as DynamicEvent).data as ByteArray);
         }
         Debug.debug("FRAGMENTSTREAM","onMetaData",Debug.clone(metaData,"keyframes"));
         metaData = parser.metaData;
         if(metaData.duration)
         {
            _duration = metaData.duration;
         }
         if(metaData.keyframes)
         {
            keyBytes = metaData.keyframes.filepositions;
            keyTimes = metaData.keyframes.times;
            _loc2_ = 0;
            while(_loc2_ < keyTimes.length - 1)
            {
               _loc3_ = keyTimes[_loc2_ + 1] - keyTimes[_loc2_];
               if(_loc3_ >= 60)
               {
                  Debug.debug("FRAGMENTSTREAM","keyFrame interval exceeds limit : " + keyTimes[_loc2_] + " > " + keyTimes[_loc2_ + 1]);
                  destroyLoader();
                  dispatchEvent(new Event("FragmentStream.Metadata.IntervalExceedsLimit"));
                  return;
               }
               _loc2_++;
            }
            indexLimit = 0;
            while(keyTimes[indexLimit + 1] == 0 && indexLimit < keyTimes.length - 2)
            {
               indexLimit = indexLimit + 1;
            }
            indexLimit = indexLimit + 1;
            Debug.debug("FRAGMENTSTREAM","indexLimit : " + indexLimit);
            Debug.debug("FRAGMENTSTREAM","keyTimes : " + keyTimes.length);
         }
         if(!(keyBytes && keyTimes))
         {
            Debug.debug("FRAGMENTSTREAM","no keyframes in metadata");
            destroyLoader();
            dispatchEvent(new Event("FragmentStream.Metadata.NoKeyframes"));
         }
      }
      
      private function onMetaParseExceeded(param1:Event) : void {
         Debug.debug("FRAGMENTSTREAM","no metadata in file");
         destroyLoader();
         dispatchEvent(new Event("FragmentStream.Metadata.Missing"));
      }
      
      private var _duration:Number = 0;
      
      public function get duration() : Number {
         return _duration;
      }
      
      private const MIN_BUFFER_LENGTH:uint = 3;
      
      private const MIN_PRELOAD_LIMIT:uint = 20;
      
      private const MAX_PRELOAD_LIMIT:uint = 60;
      
      private var preBuffer:FragmentArray;
      
      private var playBuffer:Object;
      
      private var metaData:Object;
      
      private var keyIndex:uint = 0;
      
      private var keyBytes:Array;
      
      private var keyTimes:Array;
      
      private var appendCount:int = 0;
      
      private var initProgress:int = 0;
      
      protected var _maxMemoryUsage:Number = 3.3554432E7;
      
      public function get maxMemoryUsage() : uint {
         return _maxMemoryUsage;
      }
      
      public function set maxMemoryUsage(param1:uint) : void {
         var _loc2_:Number = _maxMemoryUsage;
         _maxMemoryUsage = param1;
         if(_maxMemoryUsage < _loc2_)
         {
            dropUnneededPackets();
         }
      }
      
      private function onTickerFrame(param1:Event) : void {
         var _loc5_:* = null;
         var _loc6_:* = NaN;
         var _loc2_:* = NaN;
         var _loc3_:* = null;
         var _loc4_:* = 0;
         if(keyTimes && appendEnabled())
         {
            if(initProgress > 1 && keyIndex > indexLimit && !(delayedSeek == -1))
            {
               Debug.debug("FRAGMENTSTREAM","delayed seek to " + delayedSeek + " (" + getIndexForOffset(delayedSeek) + ")");
               seek(delayedSeek);
               addEventListener("netStatus",onDelayedSeekNetStatus);
               return;
            }
            if(initProgress > 1 && (delayedPause))
            {
               Debug.debug("FRAGMENTSTREAM","delayed pause");
               delayedPause = false;
               super.pause();
            }
            else if((keyIndex <= indexLimit || bufferLength < 3) && playBuffer[keyBytes[keyIndex]] && playBuffer[keyBytes[keyIndex]].bytesAvailable)
            {
               _loc5_ = playBuffer[keyBytes[keyIndex]];
               _loc6_ = bufferLength;
               _loc2_ = _loc5_.position;
               Debug.debug("FRAGMENTSTREAM","------");
               Debug.debug("FRAGMENTSTREAM","packet.bytesAvailable : " + _loc5_.bytesAvailable + ", pos : " + _loc2_ + ", length : " + _loc5_.length + ", size : " + _loc5_.size);
               _loc3_ = new ByteArray();
               _loc5_.readBytes(_loc3_);
               Debug.debug("FRAGMENTSTREAM","appending " + _loc3_.length + " bytes");
               _loc5_.position = _loc2_ + _loc3_.length;
               appendBytes(_loc3_);
               Debug.debug("FRAGMENTSTREAM","appending from packet : " + keyIndex + "/" + keyBytes.length);
               Debug.debug("FRAGMENTSTREAM","   buffer size : " + _loc6_ + " >>> " + bufferLength + " (" + Math.floor((bufferLength - _loc6_) * 1000) / 1000 + ") audioCodec : " + audioCodec + " videoCodec : " + videoCodec);
               if(!_loc5_.bytesAvailable && _loc5_.length == _loc5_.size)
               {
                  _loc5_.position = 0;
                  keyIndex = keyIndex + 1;
                  appendCount = appendCount + 1;
                  Debug.debug("FRAGMENTSTREAM","increment keyIndex, appendCount");
               }
               Debug.debug("FRAGMENTSTREAM","------");
               setCurrentFragment();
            }
            
            if(getBufferRemainingTime() < 20)
            {
               _loc4_ = getNextMissingIndex();
               if(!(_loc4_ == -1) && parser.cacheSize == 0 && (!loader || loader && loadStart > keyBytes[_loc4_] || loader && !(loadPosition == 0) && loadPosition < keyBytes[_loc4_]))
               {
                  Debug.debug("FRAGMENTSTREAM","resume loading");
                  if(_loc4_ != -1)
                  {
                     Debug.debug("FRAGMENTSTREAM","index : " + _loc4_);
                  }
                  if(!loader)
                  {
                     Debug.debug("FRAGMENTSTREAM","!loader");
                  }
                  if(loader && loadStart > keyBytes[_loc4_])
                  {
                     Debug.debug("FRAGMENTSTREAM","loadStart : " + loadStart + ", " + "index : " + _loc4_ + ", " + "keyBytes[ index ] : " + keyBytes[_loc4_]);
                  }
                  if(loader && !(loadPosition == 0) && loadPosition < keyBytes[_loc4_])
                  {
                     Debug.debug("FRAGMENTSTREAM","loadPosition : " + loadPosition + ", " + "index : " + _loc4_ + ", " + "keyBytes[ index ] : " + keyBytes[_loc4_]);
                  }
                  load(keyBytes[_loc4_]);
                  loadIndex = _loc4_;
               }
            }
            if(!bufferEmpty && bufferLength == 0)
            {
               bufferEmpty = true;
            }
            else if(bufferEmpty && bufferLength >= 1)
            {
               bufferEmpty = false;
            }
            
         }
      }
      
      private function appendEnabled() : Boolean {
         var _loc1_:* = 0;
         if(initProgress == 1)
         {
            if(super.time > 0.5)
            {
               initProgress = initProgress + 1;
               if(delayedSeek == -1)
               {
                  dispatchNetStatusEvent("FragmentStream.Init.Complete");
               }
               onTickerFrame(null);
            }
            else
            {
               Debug.warn("FRAGMENTSTREAM","appendEnabled : initProgress == 1, super.time <= BUFFER_TIME_LIMIT");
               return false;
            }
         }
         if(indexLimit == Infinity)
         {
            Debug.warn("FRAGMENTSTREAM","indexLimit == Infinity");
            return false;
         }
         if(appendCount > indexLimit)
         {
            return true;
         }
         _loc1_ = 0;
         while(_loc1_ <= indexLimit)
         {
            if(!playBuffer[keyBytes[_loc1_]])
            {
               Debug.warn("FRAGMENTSTREAM","!playBuffer[ keyBytes[ " + _loc1_ + " ] ]");
               return false;
            }
            _loc1_++;
         }
         return true;
      }
      
      private function onNetStatus(param1:NetStatusEvent) : void {
         Debug.debug("FRAGMENTSTREAM","onNetStatus : " + param1.info.code);
         var _loc2_:* = param1.info.code;
         if("NetStream.SeekStart.Notify" !== _loc2_)
         {
            if("NetStream.Seek.Notify" !== _loc2_)
            {
               if("NetStream.Buffer.Full" !== _loc2_)
               {
                  if("NetStream.Buffer.Empty" === _loc2_)
                  {
                     if(initProgress == 1)
                     {
                        param1.stopImmediatePropagation();
                        initProgress = initProgress + 1;
                        if(delayedSeek == -1)
                        {
                           dispatchNetStatusEvent("FragmentStream.Init.Complete");
                        }
                        onTickerFrame(null);
                     }
                     if(keyBytes && keyIndex >= keyBytes.length)
                     {
                        param1.stopImmediatePropagation();
                        dispatchNetStatusEvent("NetStream.Play.Complete");
                        dispatchNetStatusEvent("NetStream.Play.Stop");
                     }
                  }
               }
               else
               {
                  if(!initProgress)
                  {
                     initProgress = initProgress + 1;
                  }
                  .super.bufferTime = 1;
               }
            }
            else
            {
               appendBytesAction("resetSeek");
               if(!ticker.hasEventListener("enterFrame"))
               {
                  Debug.warn("FRAGMENTSTREAM","added ENTER_FRAME");
                  ticker.addEventListener("enterFrame",onTickerFrame);
               }
            }
         }
         else if(!playBuffer[keyBytes[keyIndex]])
         {
            param1.stopImmediatePropagation();
            dispatchNetStatusEvent("FragmentStream.JumpStart.Notify",param1.info.level);
         }
         
      }
      
      private function onDelayedSeekNetStatus(param1:NetStatusEvent) : void {
         Debug.debug("FRAGMENTSTREAM","onDelayedSeekNetStatus : " + param1.info.code);
         var _loc2_:* = param1.info.code;
         if("NetStream.Buffer.Full" === _loc2_)
         {
            removeEventListener("netStatus",onDelayedSeekNetStatus);
            dispatchNetStatusEvent("FragmentStream.Init.Complete");
         }
      }
      
      private function getIndexForOffset(param1:Number) : uint {
         var _loc5_:* = NaN;
         var _loc4_:* = 0;
         var _loc2_:* = NaN;
         var _loc3_:uint = 0;
         if(keyTimes)
         {
            _loc5_ = Infinity;
            _loc4_ = 0;
            while(_loc4_ < keyTimes.length)
            {
               _loc2_ = Math.abs(keyTimes[_loc4_] - param1);
               if(_loc2_ <= _loc5_)
               {
                  _loc3_ = _loc4_;
                  _loc5_ = _loc2_;
                  _loc4_++;
                  continue;
               }
               break;
            }
         }
         return _loc3_;
      }
      
      private function getIndexForPosition(param1:Number) : int {
         var _loc2_:* = 0;
         if(keyBytes)
         {
            _loc2_ = 0;
            while(_loc2_ < keyBytes.length)
            {
               if(keyBytes[_loc2_] == param1)
               {
                  return _loc2_;
               }
               _loc2_++;
            }
         }
         return -1;
      }
      
      public function getBufferRemainingTime() : Number {
         var _loc3_:* = 0;
         var _loc5_:* = NaN;
         var _loc1_:* = NaN;
         var _loc2_:* = NaN;
         var _loc6_:* = null;
         var _loc4_:* = 0.0;
         if(keyTimes)
         {
            _loc3_ = keyIndex;
            while(_loc3_ < keyBytes.length)
            {
               if(playBuffer[keyBytes[_loc3_]])
               {
                  _loc5_ = keyTimes[_loc3_];
                  _loc1_ = _loc3_ < keyTimes.length - 1?keyTimes[_loc3_ + 1]:_duration;
                  _loc2_ = _loc1_ - _loc5_;
                  _loc6_ = playBuffer[keyBytes[_loc3_]] as FragmentArray;
                  _loc2_ = _loc2_ * _loc6_.length / _loc6_.size;
                  _loc4_ = _loc4_ + _loc2_;
                  if(_loc6_.length == _loc6_.size)
                  {
                     _loc3_++;
                     continue;
                  }
                  break;
               }
               break;
            }
         }
         return Math.floor(1000 * _loc4_) / 1000;
      }
      
      private var fragmentStart:Number = 0;
      
      private var fragmentEnd:Number = 0;
      
      public function get currentFragment() : Object {
         return 
            {
               "start":fragmentStart,
               "end":fragmentEnd
            };
      }
      
      private function setCurrentFragment() : void {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         fragmentStart = 0;
         fragmentEnd = 0;
         if(keyTimes)
         {
            _loc1_ = keyIndex;
            _loc2_ = keyIndex;
            while(_loc1_ > 0 && playBuffer[keyBytes[_loc1_ - 1]])
            {
               _loc1_--;
            }
            while(_loc2_ < keyBytes.length - 1 && playBuffer[keyBytes[_loc2_ + 1]])
            {
               _loc2_++;
            }
            fragmentStart = keyTimes[_loc1_];
            fragmentEnd = _loc2_ < keyBytes.length - 1?keyTimes[_loc2_ + 1]:_duration;
         }
      }
      
      private function getNextMissingIndex() : int {
         var _loc2_:* = 0;
         var _loc3_:* = null;
         var _loc1_:* = -1;
         if(keyBytes)
         {
            _loc2_ = keyIndex;
            while(_loc2_ < keyBytes.length)
            {
               _loc3_ = playBuffer[keyBytes[_loc2_]] as FragmentArray;
               if(!_loc3_ || _loc3_ && !(_loc3_.length == _loc3_.size))
               {
                  _loc1_ = _loc2_;
                  break;
               }
               _loc2_++;
            }
         }
         return _loc1_;
      }
      
      public function getBufferMap() : Array {
         var _loc2_:* = 0;
         var _loc3_:* = NaN;
         var _loc1_:* = NaN;
         var _loc4_:* = null;
         var _loc5_:Array = [];
         if(keyBytes)
         {
            _loc2_ = 0;
            while(_loc2_ < keyBytes.length)
            {
               if(playBuffer[keyBytes[_loc2_]])
               {
                  _loc3_ = keyTimes[_loc2_];
                  _loc1_ = (_loc2_ == keyBytes.length - 1?_duration:keyTimes[_loc2_ + 1]) - _loc3_;
                  _loc4_ = playBuffer[keyBytes[_loc2_]] as FragmentArray;
                  if(_loc4_)
                  {
                     _loc1_ = _loc1_ * _loc4_.length / _loc4_.size;
                  }
                  _loc5_.push(
                     {
                        "time":_loc3_ / _duration,
                        "length":_loc1_ / _duration
                     });
               }
               _loc2_++;
            }
         }
         return _loc5_;
      }
      
      private function dropUnneededPackets() : void {
         var _loc3_:* = 0;
         var _loc1_:* = 0;
         var _loc4_:* = NaN;
         var _loc2_:Array = [];
         if(keyBytes)
         {
            _loc3_ = 0;
            while(_loc3_ < keyIndex)
            {
               if(playBuffer[keyBytes[_loc3_]])
               {
                  _loc2_.push(keyBytes[_loc3_]);
               }
               _loc3_++;
            }
            _loc1_ = getIndexForPosition(loadPosition);
            _loc3_ = keyBytes.length - 1;
            while(_loc3_ > _loc1_)
            {
               if(playBuffer[keyBytes[_loc3_]])
               {
                  _loc2_.push(keyBytes[_loc3_]);
               }
               _loc3_--;
            }
         }
         while(_usage > _maxMemoryUsage && _loc2_.length)
         {
            _loc4_ = _loc2_[0];
            _usage = _usage - playBuffer[_loc4_].length;
            delete playBuffer[_loc4_];
            _loc2_.splice(0,1);
            dispatchEvent(new Event("FragmentStream.Packet.Dropped"));
         }
         setCurrentFragment();
      }
      
      private function killUnfinishedPackets() : void {
         var _loc3_:* = null;
         var _loc1_:* = false;
         var _loc5_:* = 0;
         var _loc4_:* = playBuffer;
         for(_loc2_ in playBuffer)
         {
            _loc3_ = playBuffer[_loc2_] as FragmentArray;
            if(_loc3_ && !(_loc3_.length == _loc3_.size))
            {
               _usage = _usage - playBuffer[_loc2_].length;
               delete playBuffer[_loc2_];
               Debug.debug("FRAGMENTSTREAM","packet.length : " + _loc3_.length);
               Debug.debug("FRAGMENTSTREAM","packet.size : " + _loc3_.size);
               Debug.debug("FRAGMENTSTREAM","diff : " + (_loc3_.size - _loc3_.length));
               Debug.debug("FRAGMENTSTREAM","dropped partially loaded packet: " + _loc2_);
               _loc1_ = true;
            }
         }
         if(_loc1_)
         {
            dispatchEvent(new Event("FragmentStream.Packet.Dropped"));
         }
      }
      
      private function dispatchNetStatusEvent(param1:String, param2:String = "status") : void {
         var _loc3_:Object = {};
         _loc3_.code = param1;
         _loc3_.level = param2;
         dispatchEvent(new NetStatusEvent("netStatus",false,false,_loc3_));
      }
      
      public function dropAllPackets() : void {
         Debug.debug("FRAGMENTSTREAM","dropAllPackets");
         playBuffer = {};
         _usage = 0;
         destroyLoader();
         System.gc.call(this);
      }
      
      protected function get bufferEmpty() : Boolean {
         return _bufferEmpty;
      }
      
      private function suppressPauseEvents(param1:NetStatusEvent) : void {
         Debug.debug("FRAGMENTSTREAM","suppressPauseEvents");
         if(param1.info.code == "NetStream.Pause.Notify" || param1.info.code == "NetStream.Unpause.Notify")
         {
            param1.stopPropagation();
            param1.stopImmediatePropagation();
            removeEventListener("netStatus",suppressPauseEvents);
            Debug.debug("FRAGMENTSTREAM","removed");
         }
      }
      
      protected function set bufferEmpty(param1:Boolean) : void {
         if(_bufferEmpty != param1)
         {
            _bufferEmpty = param1;
            if(_bufferEmpty)
            {
               dispatchNetStatusEvent("NetStream.Buffer.Empty");
               if(playing)
               {
                  addEventListener("netStatus",suppressPauseEvents,false,2147483647);
                  super.pause();
               }
            }
            else
            {
               dispatchNetStatusEvent("NetStream.Buffer.Full");
               if(playing)
               {
                  addEventListener("netStatus",suppressPauseEvents,false,2147483647);
                  super.resume();
               }
            }
         }
      }
      
      private var _totalByteCount:Number = 0;
      
      override public function get info() : NetStreamInfo {
         var _loc1_:NetStreamInfo = super.info;
         var _loc2_:NetStreamInfo = new NetStreamInfo(_loc1_.currentBytesPerSecond,_totalByteCount,_loc1_.maxBytesPerSecond,_loc1_.audioBytesPerSecond,_loc1_.audioByteCount,_loc1_.videoBytesPerSecond,_loc1_.videoByteCount,_loc1_.dataBytesPerSecond,_loc1_.dataByteCount,_loc1_.playbackBytesPerSecond,_loc1_.droppedFrames,_loc1_.audioBufferByteLength,_loc1_.videoBufferByteLength,_loc1_.dataBufferByteLength,_loc1_.audioBufferLength,_loc1_.videoBufferLength,_loc1_.dataBufferLength,_loc1_.SRTT,_loc1_.audioLossRate,_loc1_.videoLossRate);
         return _loc2_;
      }
   }
}
