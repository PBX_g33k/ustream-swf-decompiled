package tv.ustream.net
{
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.debug.Debug;
   import flash.events.Event;
   import flash.utils.Timer;
   import tv.ustream.statistics.Statistics;
   import flash.events.TimerEvent;
   import flash.utils.getTimer;
   import tv.ustream.net.interfaces.ICommunicator;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.This;
   import flash.external.ExternalInterface;
   import flash.utils.describeType;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.tools.ObjectUtils;
   import tv.ustream.viewer.logic.modules.Logicloadtest;
   import flash.errors.IllegalOperationError;
   
   public class ViewerUms extends Ums
   {
      
      public function ViewerUms(param1:Recorded, param2:String = null, param3:String = null) {
         var _loc4_:* = null;
         var _loc6_:* = null;
         var _loc5_:* = null;
         if(validConstruct)
         {
            if(param1 && param1.hasOwnProperty("id"))
            {
               _hasLongPoll = param1.id == "17134975"?true:false;
            }
            super();
            validConstruct = false;
            this.media = param1;
            if(param1)
            {
               param1.modules.dispatchEvent(new DynamicEvent("connection",false,false,{"connection":umsConnection}));
               stats = param1.statistics;
               stats.addEventListener("change",onStatsChange);
               stats.addEventListener("destroy",onStatsDestroy);
            }
            this.userId = param2;
            this.password = param3;
            if(This.pageUrl.indexOf("#ums[") != -1)
            {
               _loc4_ = This.pageUrl.split("#ums[")[1].split("]")[0];
               _loc6_ = _loc4_.split(":")[0];
               Debug.debug("VIEWER_UMS","forced to ip : " + _loc6_);
               _loc5_ = _loc4_.split(":")[1];
               if(_loc5_)
               {
                  ports = [_loc5_];
                  Debug.debug("VIEWER_UMS","forced to port : " + _loc5_);
               }
               forcedIp = _loc6_;
            }
            statusTimer = new Timer(3000);
            statusTimer.addEventListener("timer",sendState);
            cooldownTimer = new Timer(180000,1);
            cooldownTimer.addEventListener("timerComplete",onCooldownComplete);
            connect();
            return;
         }
         throw new IllegalOperationError("Do not instantiate, use the \'requestInstance\' method");
      }
      
      public static const UPLOAD_RESPONSE:String = "uploadResponse";
      
      protected static var validConstruct:Boolean = false;
      
      private static var pool:Object;
      
      public static function requestInstance(param1:Recorded, param2:String = null, param3:String = null) : ViewerUms {
         var _loc4_:* = null;
         var _loc6_:String = (param1 is Channel?"c":"r") + param1.mediaId;
         if(!pool)
         {
            pool = {};
         }
         if(pool[_loc6_] is ViewerUms && !(pool[_loc6_] as ViewerUms).orphaned)
         {
            Debug.debug("VIEWER_UMS","return ums instance");
            return pool[_loc6_];
         }
         if(param1 is Channel)
         {
            _loc9_ = 0;
            _loc8_ = pool;
            for(_loc5_ in pool)
            {
               if(_loc5_.substr(0,1) == "c" && ((pool[_loc5_] as ViewerUms).orphaned) && (pool[_loc5_] as ViewerUms).cluster == "live")
               {
                  Debug.debug("VIEWER_UMS","found orphaned ums instance");
                  _loc7_ = pool[_loc5_];
                  pool[_loc6_] = _loc7_;
                  _loc4_ = _loc7_;
                  (pool[_loc6_] as ViewerUms).switchMedia(param1 as Channel);
                  delete pool[_loc5_];
               }
            }
         }
         if(!_loc4_)
         {
            Debug.debug("VIEWER_UMS","created new ums instance");
            validConstruct = true;
            _loc7_ = new ViewerUms(param1,param2,param3);
            pool[_loc6_] = _loc7_;
            _loc4_ = _loc7_;
         }
         _loc4_.addEventListener("destroy",onInstanceDestroy);
         return _loc4_;
      }
      
      private static function onInstanceDestroy(param1:Event) : void {
         var _loc2_:ViewerUms = param1.target as ViewerUms;
         _loc2_.removeEventListener("destroy",onInstanceDestroy);
         var _loc5_:* = 0;
         var _loc4_:* = pool;
         for(_loc3_ in pool)
         {
            if(pool[_loc3_] == _loc2_)
            {
               Debug.debug("VIEWER_UMS","removing ums " + _loc2_.randomId + " from pool");
               delete pool[_loc3_];
            }
         }
      }
      
      private const COOLDOWN_DELAY:uint = 180000;
      
      private const COOLDOWN_SCATTER:Number = 50;
      
      private const COOLDOWN_THRESHOLD:Number = 0.9;
      
      private const COOLDOWN_FADING_THRESHOLD:Number = 0.75;
      
      private const STAT_SEND_DELAY:uint = 5000;
      
      private var _pid:int = 0;
      
      private var _status:String = "";
      
      private var statusTimer:Timer;
      
      private var _cid:String;
      
      private var svSignature:String;
      
      private var svArgument:String;
      
      protected var externalArgs:Object;
      
      private var terminationTimer:Timer;
      
      private var stats:Statistics;
      
      private var statsCache:Object;
      
      private var sendEnabled:Boolean = true;
      
      private var infoCount:uint = 0;
      
      private var userId:String;
      
      private var media:Recorded;
      
      private var password:String;
      
      private var _lastPlaying:Boolean = false;
      
      private var _orphaned:Boolean = false;
      
      private var forceReport:Boolean = false;
      
      private var lastValidViewCount:Number = 0;
      
      private var cooldown:Boolean = false;
      
      private var cooldownTimer:Timer;
      
      private var cooldownStart:Number;
      
      private var cooldownFadeout:Number;
      
      override protected function clientSetup() : void {
         if(umsConnection)
         {
            umsConnection.client = 
               {
                  "moduleInfo":onUmsModuleInfo,
                  "goOffline":onUmsGoOffline,
                  "reject":onUmsReject,
                  "onSvHashAuth":onSvHashAuth,
                  "dvrSeek":onDvrSeek,
                  "dataUploadResponse":onDataUploadResponse
               };
         }
      }
      
      private function onCooldownComplete(param1:TimerEvent) : void {
         cooldown = false;
      }
      
      override protected function connect(... rest) : void {
         if(active && stats)
         {
            stats.benchmark.umsConnectStarted();
         }
         super.connect();
      }
      
      override protected function onConnected(... rest) : void {
         if(_status)
         {
            forceReport = true;
            status = _status;
         }
         if(statsCache)
         {
            Debug.debug("VIEWER_UMS","#### stats sent on connect",statsCache);
            umsConnection.call("stats",null,statsCache);
            statsCache = null;
         }
         if(stats)
         {
            stats.benchmark.umsConnectSuccess();
         }
         if(shared.playStreamList is Array && shared.playStreamList.length > 0)
         {
            callPlayStream(shared.playStreamList);
            shared.playStreamList = null;
         }
         if(svSignature && svArgument)
         {
            setSvHash(svSignature,svArgument);
            svSignature = null;
            svArgument = null;
         }
         addLogicListeners();
         if(media is Recorded && ((media as Recorded).playing))
         {
            onPlayStatus();
         }
         if(reconnecting)
         {
            dispatch("reconnection");
            cooldown = true;
            cooldownTimer.reset();
            cooldownTimer.start();
            cooldownStart = getTimer();
            cooldownFadeout = -1;
         }
         reconnecting = false;
         super.onConnected();
      }
      
      override protected function onDisconnected(param1:Event = null) : void {
         super.onDisconnected(param1);
         _lastPlaying = false;
      }
      
      override protected function reportFailed(param1:Event = null) : void {
         var _loc2_:* = null;
         super.reportFailed(param1);
         if(stats)
         {
            _loc2_ = {};
            if(param1 && param1.target is ICommunicator)
            {
               _ip = (param1.target as ICommunicator).url;
            }
            if(ip)
            {
               _loc2_.ip = ip.indexOf(".ums.ustream.tv") != -1?"balancer":ip;
            }
            else
            {
               _loc2_.ip = "unknown";
            }
            _loc2_.retryCount = retryCount;
            stats.failure.report(param1 is Event?"umsConnectionIssues":"umsDisconnected",null,_loc2_);
         }
      }
      
      override protected function startReconnect(param1:Event = null) : void {
         var _loc2_:* = null;
         if(_communicationType == 0)
         {
            _loc2_ = new Timer(reconnectDelay,1);
            _loc2_.addEventListener("timerComplete",connect);
            _loc2_.start();
            if(param1 is Event && media is Recorded && retryCount <= 5)
            {
               Debug.debug("VIEWER_UMS","reporting connection failed");
               (media as Recorded).dispatchEvent(new DynamicEvent("connectFailed",false,false,
                  {
                     "ip":_ip,
                     "reconnectDelay":reconnectDelay
                  }));
            }
         }
         else
         {
            connect();
         }
      }
      
      override protected function collectArgs() : Object {
         var _loc1_:Object = {};
         if(media is Recorded)
         {
            _loc1_.media = (media.brandId && !(media.brandId == "1")?media.brandId + "/":"") + media.mediaId;
            if(media.type == "channel")
            {
               _cid = _loc1_.media;
            }
            _loc1_.application = media.type;
            if(media.sessionId)
            {
               _loc1_.session = media.sessionId;
               if(media.sessionSource)
               {
                  _loc1_.sessionSource = media.sessionSource;
               }
            }
            if(media.hasOwnProperty("userId") && media.userId)
            {
               _loc1_.userId = media.userId;
            }
            if(media.hasOwnProperty("rpin") && media.rpin)
            {
               _loc1_.rpin = media.rpin;
            }
            if(media.hasOwnProperty("highlightId") && media.highlightId)
            {
               _loc1_.highlightId = media.highlightId.toString();
            }
            if(media.hasOwnProperty("hash") && media.hash)
            {
               _loc1_.hash = media.hash;
            }
            else if(Logic.hasInstance && Logic.instance.hash)
            {
               _loc1_.hash = Logic.instance.hash;
            }
            
         }
         _loc1_.rsid = "null";
         if(userId)
         {
            _loc1_.userId = userId;
         }
         if(password)
         {
            _loc1_.password = password;
         }
         if(This.onSite(This.pageUrl) & 2 && Logic.stage && Logic.stage.loaderInfo.parameters.rsid)
         {
            _loc1_.rsid = Logic.stage.loaderInfo.parameters.rsid;
         }
         else if(Logic.hasInstance)
         {
            _loc1_.rsid = Logic.instance.randomId;
         }
         
         if(_loc1_.rsid.indexOf(":") == -1)
         {
            _loc1_.rsid = _loc1_.rsid + (":" + randomId);
         }
         if(shared.svAuthTimestamp)
         {
            _loc1_.svHashAuth = true;
         }
         if(reconnecting || (Logic.reloading))
         {
            _loc1_.reconnecting = true;
         }
         if(This.referrer)
         {
            _loc1_.referrer = This.referrer;
         }
         if(media is Recorded && (media as Recorded).urlFailure == "multilevel_embed")
         {
            _loc1_.referrer = "multilevel_embed";
         }
         if(externalArgs)
         {
            _loc4_ = 0;
            _loc3_ = externalArgs;
            for(_loc2_ in externalArgs)
            {
               _loc1_[_loc2_] = externalArgs[_loc2_];
            }
         }
         if(Logic.loaderInfo && Logic.loaderInfo.loaderURL)
         {
            _loc1_.loaderUrl = Logic.loaderInfo.loaderURL;
         }
         _loc1_.externalInterface = ExternalInterface.available;
         return _loc1_;
      }
      
      override protected function getLiveUrl() : String {
         var _loc2_:Array = [];
         _loc2_.push("r" + Math.floor(Math.random() * Math.pow(10,15)));
         if(media is Recorded)
         {
            _loc2_.push(media.brandId);
            _loc2_.push(media.mediaId);
            _loc2_.push(media.type);
         }
         var _loc1_:String = _cluster;
         if(This.getReference(Logic.stage,"loaderInfo.parameters.hint") is String)
         {
            _loc1_ = This.getReference(Logic.stage,"loaderInfo.parameters.hint") as String;
         }
         _loc2_.push(_loc1_);
         _loc2_.push("ums.ustream.tv");
         return _loc2_.join(".") + "/ustream";
      }
      
      override public function destroy(... rest) : * {
         _orphaned = true;
         if(media is Recorded && media.modules)
         {
            media.modules.dispatchEvent(new DynamicEvent("connection",false,{"connection":null}));
         }
         removeLogicListeners();
         var _loc2_:Boolean = rest && rest[0] == "rejected";
         if(media is Channel && !_loc2_)
         {
            if(!terminationTimer)
            {
               terminationTimer = new Timer(5000,1);
               terminationTimer.addEventListener("timerComplete",terminate);
            }
            terminationTimer.reset();
            terminationTimer.start();
         }
         else
         {
            terminate();
         }
         return null;
      }
      
      override protected function terminate(param1:Event = null) : * {
         if(param1)
         {
            param1.target.removeEventListener("timerComplete",terminate);
         }
         terminationTimer = null;
         onStatsDestroy();
         if(statusTimer.running)
         {
            statusTimer.stop();
         }
         return super.terminate(param1);
      }
      
      private function addLogicListeners() : void {
         if(media is Recorded)
         {
            media.addEventListener("play",onPlayStatus);
            media.addEventListener("pause",onPlayStatus);
            media.addEventListener("offline",onOffline);
         }
      }
      
      private function removeLogicListeners() : void {
         if(media is Recorded)
         {
            media.removeEventListener("play",onPlayStatus);
            media.removeEventListener("pause",onPlayStatus);
            media.removeEventListener("offline",onOffline);
         }
      }
      
      private function onPlayStatus(param1:Event = null) : void {
         Debug.debug("VIEWER_UMS","onPlayStatus");
         var _loc3_:Boolean = media.playing && (media.hasStream);
         var _loc2_:String = "";
         if(param1 is DynamicEvent && (param1 as DynamicEvent).data.reason)
         {
            _loc2_ = (param1 as DynamicEvent).data.reason;
         }
         if(!(_loc3_ == _lastPlaying) || (w84Online))
         {
            Debug.debug("VIEWER_UMS","call playing : " + media.id + " : " + _loc3_ + ", reason : " + _loc2_);
            umsConnection.call("playing",null,media.id,_loc3_,{"reason":_loc2_});
            logicLoadTestCall("playing",null,media.id,_loc3_,{"reason":_loc2_});
            _lastPlaying = _loc3_;
         }
      }
      
      private function onOffline(param1:Event) : void {
         Debug.debug("VIEWER_UMS","onOffline");
         _lastPlaying = false;
      }
      
      public function callPlayStream(param1:Object) : void {
         var _loc2_:* = null;
         Debug.debug("VIEWER_UMS","callPlayStream",param1);
         if(param1)
         {
            if(param1 is Array)
            {
               _loc2_ = param1 as Array;
            }
            else
            {
               _loc2_ = new Array(param1);
            }
            if(_connected)
            {
               umsConnection.call("playStream",null,_loc2_);
               logicLoadTestCall("playStream",null,_loc2_);
            }
            else
            {
               if(!shared.playStreamList)
               {
                  shared.playStreamList = [];
               }
               param1.so = true;
               shared.playStreamList.push(param1);
               Debug.debug("VIEWER_UMS","not connected, storing log");
            }
         }
      }
      
      public function switchMedia(param1:Recorded) : void {
         if(param1 is Recorded)
         {
            removeLogicListeners();
            this.media = param1;
            addLogicListeners();
            onStatsDestroy();
            stats = param1.statistics;
            stats.addEventListener("change",onStatsChange);
            stats.addEventListener("destroy",onStatsDestroy);
            param1.modules.dispatchEvent(new DynamicEvent("connection",false,false,{"connection":umsConnection}));
            w84Online = true;
            _lastPlaying = false;
            Debug.debug("VIEWER_UMS","switching media from " + _cid + " to " + param1.mediaId);
            _cid = param1.mediaId;
            umsConnection.call("switch",null,_cid);
            logicLoadTestCall("switch",null,_cid);
            _orphaned = false;
            if(terminationTimer)
            {
               terminationTimer.stop();
               terminationTimer.removeEventListener("timerComplete",terminate);
               terminationTimer = null;
            }
         }
      }
      
      protected function processViewCount(param1:Number) : Number {
         var _loc2_:* = NaN;
         var _loc5_:* = NaN;
         var _loc4_:* = NaN;
         var _loc3_:* = NaN;
         if(param1 >= 0.9 * lastValidViewCount)
         {
            cooldown = false;
         }
         if(!cooldown)
         {
            lastValidViewCount = param1;
            _loc2_ = param1;
         }
         else
         {
            if(cooldownFadeout == -1 && param1 >= 0.75 * lastValidViewCount)
            {
               cooldownFadeout = getTimer();
            }
            _loc2_ = lastValidViewCount;
            if(cooldownFadeout != -1)
            {
               _loc5_ = 180000 - (cooldownFadeout - cooldownStart);
               _loc4_ = _loc5_ - (getTimer() - cooldownFadeout);
               _loc2_ = Math.floor(param1 + (lastValidViewCount - param1) * Math.max(0,Math.min(1,_loc4_ / _loc5_)));
               Debug.debug("VIEWER_UMS","viewCount fading: " + _loc5_ + " / " + _loc4_ + " )");
            }
            _loc3_ = Math.floor(Math.min(lastValidViewCount,50) / 2 - Math.min(lastValidViewCount,50) * Math.random());
            _loc2_ = _loc2_ + _loc3_;
            Debug.debug("VIEWER_UMS","viewCount " + param1 + " > " + _loc2_ + " ( scatter : " + _loc3_ + " )");
         }
         return _loc2_;
      }
      
      override protected function onUmsModuleInfo(param1:Object) : void {
         super.onUmsModuleInfo(param1);
         if(param1)
         {
            if(param1.id != undefined)
            {
               if(_pid != param1.id)
               {
                  _pid = param1.id;
                  Debug.debug("VIEWER_UMS","pid : " + _pid);
               }
               delete param1.id;
            }
            if(w84Online && param1.stream is Array && (param1.stream as Array).length)
            {
               onPlayStatus();
               w84Online = false;
            }
            if(param1.viewers != undefined)
            {
               param1.viewers = processViewCount(param1.viewers);
            }
            if(!infoCount)
            {
               if(stats)
               {
                  stats.benchmark.umsModuleInfo();
               }
            }
            infoCount = infoCount + 1;
            if(!_orphaned && media)
            {
               media.dispatchEvent(new DynamicEvent("moduleInfo",false,false,
                  {
                     "info":param1,
                     "source":"ums"
                  }));
            }
         }
      }
      
      override protected function onUmsReject(param1:Object) : void {
         var _loc3_:* = null;
         var _loc4_:* = null;
         super.onUmsReject(param1);
         if(param1 && param1.messageToken == "flash.viewer.logic2multi.recordedReject" && media && media.hasOwnProperty("type") && media.type == "recorded")
         {
            reconnecting = true;
            setCluster("ppv");
         }
         else if(param1 && param1.cluster)
         {
            Debug.debug("UMS","cluster reject");
            if(media is Recorded)
            {
               _loc3_ = {};
               _loc4_ = describeType(ViewerModuleManager).descendants("accessor");
               _loc7_ = 0;
               _loc6_ = _loc4_;
               for each(_loc2_ in _loc4_)
               {
                  if(_loc2_.@access == "readonly" && !(_loc2_.@declaredBy.indexOf("::ViewerModuleManager") == -1) && media.modules[_loc2_.@name])
                  {
                     _loc5_ = _loc2_.@name;
                     if("meta" !== _loc5_)
                     {
                        if("stream" !== _loc5_)
                        {
                           _loc3_[_loc2_.@name] = false;
                           Debug.debug("UMS","  destroy " + _loc2_.@name);
                        }
                        else
                        {
                           _loc3_["stream"] = [];
                           Debug.debug("UMS","  stream offline");
                        }
                     }
                     else
                     {
                        Debug.debug("UMS","  skip meta");
                     }
                  }
               }
               media.dispatchEvent(new DynamicEvent("moduleInfo",false,false,
                  {
                     "info":_loc3_,
                     "source":"ums"
                  }));
            }
            if(_ipList)
            {
               _ipList = _ipList.destroy();
            }
            Debug.debug("UMS","ipList cleared");
            if(param1.cluster.name)
            {
               setCluster(param1.cluster.name);
            }
         }
         else
         {
            dispatch("reject",false,param1);
            destroy("rejected");
         }
         
      }
      
      public function setSvHash(param1:String, param2:String) : void {
         if(_connected)
         {
            Debug.debug("VIEWER_UMS","setSvHash: calling \'svHash\' on ums");
            umsConnection.call("svHash",null,param1,param2);
            logicLoadTestCall("svHash",null,param1,param2);
         }
         else
         {
            Debug.debug("VIEWER_UMS","setSvHash: storing values");
            svSignature = param1;
            svArgument = param2;
         }
      }
      
      private function onDvrSeek(param1:Object) : void {
         Debug.debug("VIEWER_UMS","onDvrSeek callback:",param1);
         var _loc2_:Object = {};
         _loc2_.dvr = param1;
         _loc2_.stream = param1.stream;
         delete param1.stream;
         if(!_orphaned && media)
         {
            media.dispatchEvent(new DynamicEvent("moduleInfo",false,false,
               {
                  "info":_loc2_,
                  "source":"dvrSeek"
               }));
         }
      }
      
      public function dvrSeek(param1:Number) : Number {
         var param1:Number = Math.min(1,Math.max(0,param1));
         if(Logic.instance.media.modules.dvr)
         {
            Debug.debug("VIEWER_UMS","call dvrSeek: " + param1);
            umsConnection.call("dvrSeek",null,param1);
            return 1;
         }
         return 0;
      }
      
      private function onStatsChange(param1:DynamicEvent) : void {
         Debug.debug("ums","onStatsChange");
         if(_connected)
         {
            sendStats(param1.data);
         }
         else
         {
            if(!statsCache)
            {
               statsCache = {};
            }
            ObjectUtils.merge(param1.data,statsCache);
         }
      }
      
      private function onStatsDestroy(param1:Event = null) : void {
         if(stats)
         {
            stats.removeEventListener("change",onStatsChange);
            stats.removeEventListener("destroy",onStatsDestroy);
            stats = null;
         }
      }
      
      private function sendStats(param1:Object) : void {
         Debug.debug("VIEWER_UMS","sendStats",param1);
         umsConnection.call("stats",null,param1);
         logicLoadTestCall("stats",null,param1);
      }
      
      private function logicLoadTestCall(... rest) : void {
         if(This.getReference(media,"modules.logicloadtest") != null)
         {
            (This.getReference(media,"modules.logicloadtest") as Logicloadtest).loose.apply(this,rest);
         }
      }
      
      private function sendState(... rest) : void {
         statusTimer.reset();
      }
      
      public function sendDataPacket(param1:Object) : void {
         Debug.debug("VIEWER_UMS","sendDataPacket : " + sendEnabled);
         if(sendEnabled)
         {
            umsConnection.call("dataUpload",null,param1);
         }
      }
      
      private function onDataUploadResponse(param1:Object) : void {
         if(param1)
         {
            if(!param1.accepted)
            {
               sendEnabled = false;
               Debug.collectionEnabled = false;
            }
            dispatch("uploadResponse",false,{"response":param1});
         }
      }
      
      public function get status() : String {
         return _status;
      }
      
      public function set status(param1:String) : void {
         _status = param1;
      }
      
      public function get orphaned() : Boolean {
         return _orphaned;
      }
   }
}
