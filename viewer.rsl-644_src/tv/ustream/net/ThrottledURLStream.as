package tv.ustream.net
{
   import flash.net.URLStream;
   import flash.utils.ByteArray;
   import flash.display.Sprite;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   import flash.events.ProgressEvent;
   import flash.events.HTTPStatusEvent;
   import flash.net.URLRequest;
   
   public class ThrottledURLStream extends URLStream
   {
      
      public function ThrottledURLStream() {
         super();
         eventQueue = new Vector.<Event>();
         ticker = new Sprite();
         data = new ByteArray();
         cache = new ByteArray();
         addEventListener("open",onLoaderOpen);
         addEventListener("complete",onLoaderComplete,false,2147483647);
         addEventListener("httpStatus",onLoaderHttpStatus,false,2147483647);
      }
      
      public var maxBytesPerProgress:uint = 65536;
      
      private var data:ByteArray;
      
      private var cache:ByteArray;
      
      private var ticker:Sprite;
      
      private var eventQueue:Vector.<Event>;
      
      private var bytesLoaded:uint = 0;
      
      private var bytesTotal:uint = 0;
      
      private var open:Boolean = false;
      
      private var addedProgressListener:Boolean = false;
      
      private function addProgressListener() : void {
         if(open && !addedProgressListener)
         {
            addEventListener("progress",onLoaderProgress,false,2147483647);
            addedProgressListener = true;
         }
      }
      
      private function removeProgressListener() : void {
         if(addedProgressListener)
         {
            removeEventListener("progress",onLoaderProgress);
            addedProgressListener = false;
         }
      }
      
      private function onLoaderOpen(param1:Event) : void {
         Debug.echo("[ ThrottledURLStream ] stream open",1);
         open = true;
         addProgressListener();
      }
      
      private function onLoaderProgress(param1:ProgressEvent) : void {
         bytesLoaded = param1.bytesLoaded;
         bytesTotal = param1.bytesTotal;
         if(super.bytesAvailable)
         {
            if(super.bytesAvailable > maxBytesPerProgress)
            {
               Debug.echo("[ ThrottledURLStream ] progress exceeds data limit : " + super.bytesAvailable,2);
            }
            super.readBytes(cache,cache.length,super.bytesAvailable);
            cache.position = 0;
            moveCacheToData();
         }
      }
      
      private function moveCacheToData() : void {
         cache.readBytes(data,data.length,Math.min(maxBytesPerProgress,cache.bytesAvailable));
         data.position = 0;
         var _loc1_:ByteArray = new ByteArray();
         cache.readBytes(_loc1_,0,cache.bytesAvailable);
         cache = _loc1_;
         cache.position = 0;
         if(cache.bytesAvailable)
         {
            if(!ticker.hasEventListener("enterFrame"))
            {
               ticker.addEventListener("enterFrame",onTicker);
            }
         }
      }
      
      private function onLoaderHttpStatus(param1:HTTPStatusEvent) : void {
         if(cache.length)
         {
            eventQueue.push(param1.clone());
            param1.stopImmediatePropagation();
            param1.stopPropagation();
            Debug.echo("[ ThrottledURLStream ] captured http status event");
         }
      }
      
      private function onLoaderComplete(param1:Event) : void {
         Debug.echo("[ ThrottledURLStream ] stream complete",1);
         open = false;
         if(cache.length)
         {
            eventQueue.push(param1.clone());
            param1.stopImmediatePropagation();
            param1.stopPropagation();
            Debug.echo("[ ThrottledURLStream ] captured complete event");
         }
      }
      
      private function onTicker(param1:Event) : void {
         if(cache.length)
         {
            moveCacheToData();
            removeProgressListener();
            Debug.echo("[ ThrottledURLStream ] dispatch progress event : " + bytesAvailable + " bytes");
            dispatchEvent(new ProgressEvent("progress",false,false,bytesLoaded,bytesTotal));
            addProgressListener();
            if(!cache.bytesAvailable)
            {
               if(ticker.hasEventListener("enterFrame"))
               {
                  ticker.removeEventListener("enterFrame",onTicker);
               }
               while(eventQueue.length)
               {
                  dispatchEvent(eventQueue.shift());
               }
            }
         }
      }
      
      override public function close() : void {
         removeProgressListener();
         if(ticker)
         {
            ticker.removeEventListener("enterFrame",onTicker);
         }
         data = new ByteArray();
         cache = new ByteArray();
         super.close();
      }
      
      override public function load(param1:URLRequest) : void {
         super.load(param1);
      }
      
      override public function readBoolean() : Boolean {
         data.position = 0;
         var _loc1_:Boolean = data.readBoolean();
         updateData();
         return _loc1_;
      }
      
      override public function readByte() : int {
         data.position = 0;
         var _loc1_:int = data.readByte();
         updateData();
         return _loc1_;
      }
      
      override public function readBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void {
         data.position = 0;
         data.readBytes(param1,param2,param3);
         updateData();
      }
      
      override public function readDouble() : Number {
         data.position = 0;
         var _loc1_:Number = data.readDouble();
         updateData();
         return _loc1_;
      }
      
      override public function readFloat() : Number {
         data.position = 0;
         var _loc1_:Number = data.readFloat();
         updateData();
         return _loc1_;
      }
      
      override public function readInt() : int {
         data.position = 0;
         var _loc1_:int = data.readInt();
         updateData();
         return _loc1_;
      }
      
      override public function readMultiByte(param1:uint, param2:String) : String {
         data.position = 0;
         var _loc3_:String = data.readMultiByte(param1,param2);
         updateData();
         return _loc3_;
      }
      
      override public function readObject() : * {
         data.position = 0;
         var _loc1_:* = data.readObject();
         updateData();
         return _loc1_;
      }
      
      override public function readShort() : int {
         data.position = 0;
         var _loc1_:int = data.readShort();
         updateData();
         return _loc1_;
      }
      
      override public function readUnsignedByte() : uint {
         data.position = 0;
         var _loc1_:uint = data.readUnsignedByte();
         updateData();
         return _loc1_;
      }
      
      override public function readUnsignedInt() : uint {
         data.position = 0;
         var _loc1_:uint = data.readUnsignedInt();
         updateData();
         return _loc1_;
      }
      
      override public function readUnsignedShort() : uint {
         data.position = 0;
         var _loc1_:uint = data.readUnsignedShort();
         updateData();
         return _loc1_;
      }
      
      override public function readUTF() : String {
         data.position = 0;
         var _loc1_:String = data.readUTF();
         updateData();
         return _loc1_;
      }
      
      override public function readUTFBytes(param1:uint) : String {
         data.position = 0;
         var _loc2_:String = data.readUTFBytes(param1);
         updateData();
         return _loc2_;
      }
      
      override public function get bytesAvailable() : uint {
         return data.bytesAvailable;
      }
      
      private function updateData() : void {
         var _loc1_:ByteArray = new ByteArray();
         data.readBytes(_loc1_,0,data.bytesAvailable);
         data = _loc1_;
         data.position = 0;
      }
   }
}
