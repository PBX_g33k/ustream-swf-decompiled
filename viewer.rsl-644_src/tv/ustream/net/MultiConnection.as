package tv.ustream.net
{
   import flash.utils.Timer;
   import tv.ustream.tools.Shared;
   import tv.ustream.tools.Debug;
   import flash.events.NetStatusEvent;
   
   public dynamic class MultiConnection extends Connection
   {
      
      public function MultiConnection(param1:String = null, param2:String = null, param3:Array = null, param4:Boolean = false, param5:uint = 0) {
         urls = [];
         reConnectTimer = new Timer(5000);
         reConnectTimer.addEventListener("timer",onReConnect);
         timeOutTimer = new Timer(7100);
         timeOutTimer.addEventListener("timer",onTimeOut);
         this.ports = param3 || [1935,80,443];
         shared = new Shared(null,"/ports/");
         this.allowTunnel = param4;
         super(param2,param5);
      }
      
      private var urls:Array;
      
      private var args:Array;
      
      private var reConnectTimer:Timer;
      
      private var timeOutTimer:Timer;
      
      private var index:uint;
      
      private var _connecting:Boolean;
      
      public function get connecting() : Boolean {
         return _connecting;
      }
      
      private var allowTunnel:Boolean;
      
      private var shared:Shared;
      
      private var shortHost:String;
      
      private function getShortHostFromUrl(param1:String, param2:uint = 4) : String {
         Debug.echo("getShortHostFromUrl " + param1);
         var _loc3_:String = param1.split("://")[1].split("/")[0];
         var _loc4_:Array = _loc3_.split(".");
         while(_loc4_.length > param2)
         {
            _loc4_.splice(0,1);
         }
         return _loc4_.join(".");
      }
      
      private function getPortFromUrl(param1:String) : int {
         var _loc2_:* = -1;
         if(param1)
         {
            param1 = param1.split("://")[1];
            if(param1 && !(param1.indexOf(":") == -1))
            {
               param1 = param1.split(":")[1];
               if(param1)
               {
                  if(param1.indexOf("/") != 1)
                  {
                     param1 = param1.split("/")[0];
                  }
                  if(!isNaN(param1))
                  {
                     _loc2_ = param1;
                  }
               }
            }
         }
         return _loc2_;
      }
      
      override public function connect(param1:String, ... rest) : void {
         if(param1)
         {
            _url = param1;
            shortHost = getShortHostFromUrl(_url);
            Debug.echo("[ MULTICONNECTION ] host : " + shortHost);
            if(shortHost && !(shared[shortHost] == undefined))
            {
               Debug.echo("[ MULTICONNECTION ] ports before : " + ports);
               if(ports.indexOf(shared[shortHost]) != -1)
               {
                  ports.splice(this.ports.indexOf(shared[shortHost]),1);
                  ports.unshift(shared[shortHost]);
               }
               Debug.echo("[ MULTICONNECTION ] ports after : " + ports);
            }
            getUrls(param1);
            index = 0;
            this.args = rest;
            realConnect();
         }
         else
         {
            super.connect(param1);
         }
      }
      
      private function realConnect() : void {
         Debug.echo("[ MULTICONNECTION ] realConnect : " + urls[index]);
         if(urls[index])
         {
            timeOutTimer.start();
            _connecting = true;
            super.connect.apply(this,[urls[index]].concat(args));
         }
         else
         {
            Debug.echo("[ MULTICONNECTION ] URLs array empty");
         }
      }
      
      private function getUrls(param1:String) : void {
         var _loc5_:* = null;
         var _loc7_:* = null;
         var _loc2_:* = null;
         Debug.echo("[ MULTICONNECTION ] getUrls for " + param1);
         urls = [];
         var _loc4_:Array = param1.split("|");
         var _loc11_:* = 0;
         var _loc10_:* = ports;
         for each(_loc6_ in ports)
         {
            _loc9_ = 0;
            _loc8_ = _loc4_;
            for each(_loc3_ in _loc4_)
            {
               _loc5_ = _loc3_.split("://");
               _loc7_ = _loc5_[1].substr(0,_loc5_[1].indexOf("/"));
               _loc2_ = _loc5_[1].substr(_loc7_.length);
               if(_loc7_.indexOf(":") != -1)
               {
                  _loc7_ = _loc7_.substr(0,_loc7_.indexOf(":"));
               }
               urls.push("rtmp://" + _loc7_ + ":" + _loc6_ + _loc2_);
            }
         }
         if(allowTunnel)
         {
            _loc13_ = 0;
            _loc12_ = _loc4_;
            for each(_loc3_ in _loc4_)
            {
               _loc5_ = _loc3_.split("://");
               _loc7_ = _loc5_[1].substr(0,_loc5_[1].indexOf("/"));
               _loc2_ = _loc5_[1].substr(_loc7_.length);
               if(_loc7_.indexOf(":") != -1)
               {
                  _loc7_ = _loc7_.substr(0,_loc7_.indexOf(":"));
               }
               if(_loc7_.indexOf("ums") == -1)
               {
                  urls.push("rtmpt://" + _loc7_ + _loc2_);
               }
            }
         }
         Debug.explore(urls);
      }
      
      private function onTimeOut(... rest) : void {
         Debug.echo("[ MULTICONNECTION ] onTimeOut - call onFailed");
         onFailed(new NetStatusEvent("NetConnection.Connect.Failed"));
         _connecting = false;
      }
      
      private function onReConnect(... rest) : void {
         Debug.echo("[ MULTICONNECTION ] reconnect timer - stop timer - call realConnect()");
         reConnectTimer.stop();
         realConnect();
      }
      
      override protected function onNetStatus(param1:NetStatusEvent) : void {
         if(_connecting && param1.info.code == "NetConnection.Connect.Closed")
         {
            return;
         }
         if(!(_connecting && param1.info.code == "NetConnection.Connect.Failed"))
         {
            if(timeOutTimer)
            {
               timeOutTimer.stop();
            }
            _connecting = false;
         }
         super.onNetStatus(param1);
      }
      
      override protected function onSuccess(param1:NetStatusEvent) : void {
         var _loc2_:* = 0;
         Debug.echo("[ MULTICONNECTION ] onSuccess - kill timers");
         timeOutTimer.stop();
         reConnectTimer.stop();
         super.onSuccess(param1);
         if(shortHost)
         {
            Debug.echo("[ MULTICONNECTION ] onSuccess " + urls[index]);
            _loc2_ = getPortFromUrl(urls[index]);
            if(_loc2_ != -1)
            {
               shared[shortHost] = _loc2_;
               Debug.echo("[ MULTICONNECTION ] stored " + shared[shortHost] + " port to sharedObject for " + shortHost);
            }
         }
      }
      
      override protected function onFailed(param1:NetStatusEvent) : void {
         if(_connecting)
         {
            Debug.echo("[ MULTICONNECTION ] onFailed at " + (param1.target?param1.target.uri:" ? " + urls[index]) + " _connecting : " + _connecting);
            _connecting = false;
            timeOutTimer.stop();
            index = index + 1;
            if(index + 1 >= urls.length)
            {
               Debug.echo("[ MULTICONNECTION ] out of urls - call super");
               super.onFailed(param1);
            }
            else
            {
               Debug.echo("[ MULTICONNECTION ] try next - reconnect - start timer : " + reConnectTimer.delay);
               reConnectTimer.start();
            }
         }
      }
      
      public function get serverId() : String {
         var _loc1_:String = "";
         var _loc2_:Number = uri.indexOf("ustream.tv");
         if(_loc2_ != -1)
         {
            _loc1_ = uri.substring(uri.indexOf("://") + 3,_loc2_ - 1);
         }
         return _loc1_;
      }
      
      override public function kill() : void {
         if(reConnectTimer)
         {
            reConnectTimer.stop();
            reConnectTimer.removeEventListener("timer",onReConnect);
            reConnectTimer = null;
         }
         if(timeOutTimer)
         {
            timeOutTimer.stop();
            timeOutTimer.removeEventListener("timer",onTimeOut);
            timeOutTimer = null;
         }
         super.kill();
      }
   }
}
