package tv.ustream.net
{
   import flash.net.NetStreamPlayOptions;
   
   public class ChunkStreamPlayOptions extends NetStreamPlayOptions
   {
      
      public function ChunkStreamPlayOptions() {
         super();
      }
      
      public var index:uint;
      
      public var byteOffset:uint;
      
      public var timeOffset:uint;
      
      public var duration:uint = 5000;
      
      public var seamlessResume:Boolean = true;
      
      override public function toString() : String {
         var _loc1_:Array = [];
         _loc1_.push("index : " + index);
         _loc1_.push("streamName : " + streamName);
         _loc1_.push("byteOffset : " + byteOffset);
         _loc1_.push("timeOffset : " + timeOffset);
         _loc1_.push("duration : " + duration);
         _loc1_.push("seamlessResume : " + seamlessResume);
         return _loc1_.join(", ");
      }
   }
}
