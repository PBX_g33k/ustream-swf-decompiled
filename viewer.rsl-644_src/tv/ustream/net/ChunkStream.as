package tv.ustream.net
{
   import flash.net.NetStream;
   import flash.net.NetConnection;
   import flash.utils.Timer;
   import flash.display.Sprite;
   import flash.events.NetStatusEvent;
   import tv.ustream.debug.Debug;
   import tv.ustream.flv.FlvHeader;
   import flash.net.NetStreamPlayOptions;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.TimerEvent;
   import tv.ustream.flv.FlvTag;
   import flash.net.NetStreamInfo;
   
   public class ChunkStream extends NetStream
   {
      
      public function ChunkStream(param1:NetConnection) {
         super(param1);
         .super.bufferTime = 0.5;
         _connection = param1;
         loaders = new Vector.<ChunkLoader>();
         addEventListener("netStatus",onNetStatus);
         dataRateTimer = new Timer(250);
         dataRateTimer.addEventListener("timer",onDataRateTimer);
         dataRateTimer.start();
      }
      
      public static const CHUNK_LOAD_INIT:String = "ChunkStream.Load.Init";
      
      public static const CHUNK_LOAD_START:String = "ChunkStream.Load.Start";
      
      public static const CHUNK_LOAD_COMPLETE:String = "ChunkStream.Load.Complete";
      
      public static const CHUNK_LOAD_SLOW_START:String = "ChunkStream.Load.SlowStart";
      
      public static const CHUNK_LOAD_IO_ERROR:String = "ChunkStream.Load.IoError";
      
      public static const CHUNK_LOAD_TIMEOUT:String = "ChunkStream.Load.Timeout";
      
      public static const CHUNK_LOAD_PARSER_ERROR:String = "ChunkStream.Load.ParserError";
      
      public static const CHUNK_LOAD_UNRECOVERABLE_ERROR:String = "ChunkStream.Load.UnrecoverableError";
      
      public static const CHUNK_LOAD_FATAL_ERROR:String = "ChunkStream.Load.FatalError";
      
      public static const CHUNK_PREDICT_EXCESS_LAG:String = "ChunkStream.Predict.ExcessLag";
      
      public static const CHUNK_FEED_KEYFRAME:String = "ChunkStream.Feed.Keyframe";
      
      public static const CHUNK_FEED_SYNCINFO:String = "ChunkStream.Feed.SyncInfo";
      
      private const NETSTREAM_PLAY_TRANSITION:String = "NetStream.Play.Transition";
      
      private const NETSTREAM_PLAY_TRANSITION_COMPLETE:String = "NetStream.Play.TransitionComplete";
      
      private const NETSTREAM_PLAY_FAILED:String = "NetStream.Play.Failed";
      
      private const NETSTREAM_BUFFER_FULL:String = "NetStream.Buffer.Full";
      
      private const NETSTREAM_BUFFER_EMPTY:String = "NetStream.Buffer.Empty";
      
      private const NETSTREAM_TRANSITION_SUCCESS:String = "NetStream.Transition.Success";
      
      private const NETSTREAM_TRANSITION_FORCED:String = "NetStream.Transition.Forced";
      
      private const MIN_BUFFER_RUNNING:Number = 1;
      
      private const INIT_PRELOAD_QUEUESIZE:uint = 3000;
      
      private const TRANSITION_DELAY_WITHOUT_KEYS:uint = 2000;
      
      private const TRANSITION_DELAY_WITH_KEYS:uint = 10000;
      
      private const MAX_IO_ERROR_COUNT:uint = 2;
      
      private const MAX_PREDICT_DIFF:uint = 10;
      
      private var _connection:NetConnection;
      
      private var status:int = -1;
      
      private var chunkLength:uint;
      
      private var loaders:Vector.<ChunkLoader>;
      
      private var predictTimer:Timer;
      
      private var frameTicker:Sprite;
      
      private var dataRateTimer:Timer;
      
      private var keyPositions:Object;
      
      private var keyDataLength:uint;
      
      private var keyDataIndex:uint;
      
      private var transitionTimer:Timer;
      
      private var _switchEnabled:Boolean = false;
      
      private var _bufferEmpty:Boolean = true;
      
      public function get connection() : NetConnection {
         return _connection;
      }
      
      override public function set bufferTime(param1:Number) : void {
      }
      
      public function get loading() : Boolean {
         var _loc1_:* = 0;
         if(loaders)
         {
            _loc1_ = 0;
            while(_loc1_ < loaders.length)
            {
               if(loaders[_loc1_].loading)
               {
                  return true;
               }
               _loc1_++;
            }
         }
         return false;
      }
      
      public function get chunkIndex() : int {
         if(loaders && loaders.length && loaders[0])
         {
            return loaders[0].currentId;
         }
         return -1;
      }
      
      public function get predictedIndex() : int {
         if(loaders && loaders.length && loaders[0])
         {
            return loaders[0].targetId;
         }
         return -1;
      }
      
      public function get pattern() : String {
         if(loaders && loaders.length && loaders[0])
         {
            return loaders[0].pattern;
         }
         return "";
      }
      
      public function set pattern(param1:String) : void {
         var _loc3_:* = 0;
         var _loc2_:* = null;
         var _loc4_:* = null;
         if(loaders && loaders.length)
         {
            _loc3_ = 0;
            while(_loc3_ < loaders.length)
            {
               _loc2_ = loaders[_loc3_].pattern.split("/").pop();
               _loc4_ = param1.split("/");
               _loc4_[_loc4_.length - 1] = _loc2_;
               loaders[_loc3_].pattern = _loc4_.join("/");
               _loc3_++;
            }
         }
      }
      
      public function get playing() : Boolean {
         return status == 1;
      }
      
      protected function get currentBandwidth() : int {
         var _loc1_:* = 0;
         var _loc2_:* = -1;
         if(loaders && loaders.length)
         {
            _loc1_ = 0;
            while(_loc1_ < loaders.length)
            {
               if(loaders[_loc1_].currentBandwidth != -1)
               {
                  _loc2_ = _loc2_ + loaders[_loc1_].currentBandwidth;
                  _loc1_++;
                  continue;
               }
               break;
            }
         }
         return _loc2_;
      }
      
      protected function get averageBandwidth() : int {
         var _loc1_:* = 0;
         var _loc2_:* = -1;
         if(loaders && loaders.length)
         {
            _loc1_ = 0;
            while(_loc1_ < loaders.length)
            {
               _loc2_ = _loc2_ + loaders[_loc1_].averageBandwidth;
               _loc1_++;
            }
         }
         return _loc2_;
      }
      
      protected function get maximumBandwidth() : int {
         var _loc1_:* = 0;
         var _loc2_:* = -1;
         if(loaders && loaders.length)
         {
            _loc1_ = 0;
            while(_loc1_ < loaders.length)
            {
               if(loaders[_loc1_].maximumBandwidth != -1)
               {
                  _loc2_ = _loc2_ + loaders[_loc1_].maximumBandwidth;
                  _loc1_++;
                  continue;
               }
               break;
            }
         }
         return _loc2_;
      }
      
      public function get inTransition() : Boolean {
         return transitionTimer?transitionTimer.running:false;
      }
      
      public function get queueLength() : Number {
         var _loc1_:* = 0;
         var _loc2_:* = 0.0;
         if(loaders && loaders.length)
         {
            _loc1_ = 0;
            while(_loc1_ < loaders.length)
            {
               _loc2_ = _loc2_ + loaders[_loc1_].timeAvailableInQueue;
               _loc1_++;
            }
         }
         return _loc2_ / 1000;
      }
      
      public function get queueTime() : Number {
         if(loaders && loaders.length)
         {
            return loaders[0].maxTimeInQueue / 1000;
         }
         return 0;
      }
      
      public function set queueTime(param1:Number) : void {
         var _loc2_:* = 0;
         if(loaders && loaders.length)
         {
            _loc2_ = 0;
            while(_loc2_ < loaders.length)
            {
               loaders[_loc2_].maxTimeInQueue = param1 * 1000;
               _loc2_++;
            }
         }
      }
      
      public function get hasKeyPositions() : Boolean {
         return !(keyPositions == null);
      }
      
      public function get switchEnabled() : Boolean {
         return _switchEnabled;
      }
      
      private function onNetStatus(param1:NetStatusEvent) : void {
         var _loc2_:* = param1.info.code;
         if("NetStream.Buffer.Empty" !== _loc2_)
         {
            if("NetStream.Buffer.Full" !== _loc2_)
            {
               if("NetStream.Seek.Notify" !== _loc2_)
               {
                  echo("onNetStatus " + param1.info.code);
               }
               else
               {
                  echo("RESET_SEEK");
                  appendBytesAction("resetSeek");
                  dispatchNetStatusEvent("NetStream.Buffer.Empty");
                  if(!frameTicker.hasEventListener("enterFrame") && loaders && loaders.length && !loaders[0].hasEventListener("progress"))
                  {
                     frameTicker.addEventListener("enterFrame",onEnterFrame);
                  }
               }
            }
            else
            {
               _bufferEmpty = false;
               if(predictTimer && !predictTimer.running)
               {
                  predictTimer.start();
               }
            }
         }
         else
         {
            _bufferEmpty = true;
         }
      }
      
      private function suppressPauseEvents(param1:NetStatusEvent) : void {
         echo("suppressPauseEvents");
         if(param1.info.code == "NetStream.Pause.Notify" || param1.info.code == "NetStream.Unpause.Notify")
         {
            param1.stopPropagation();
            param1.stopImmediatePropagation();
            removeEventListener("netStatus",suppressPauseEvents);
            echo("removed");
         }
      }
      
      private function dispatchNetStatusEvent(param1:String, param2:String = "status", param3:String = null, param4:Object = null) : void {
         echo("dispatchNetStatusEvent : " + param1);
         var _loc6_:Object = {};
         if(param3)
         {
            _loc6_.reason = param3;
         }
         if(param4)
         {
            _loc8_ = 0;
            _loc7_ = param4;
            for(_loc5_ in param4)
            {
               _loc6_[_loc5_] = param4[_loc5_];
            }
         }
         _loc6_.code = param1;
         _loc6_.level = param2;
         dispatchEvent(new NetStatusEvent("netStatus",false,false,_loc6_));
      }
      
      private function echo(param1:String, param2:int = -1, param3:Object = null) : void {
      }
      
      override public function play(... rest) : void {
         var _loc2_:* = null;
         if(rest && rest[0] is ChunkStreamPlayOptions)
         {
            echo("public play");
            _loc2_ = rest[0];
            echo("opts : " + _loc2_);
            if(!frameTicker)
            {
               frameTicker = new Sprite();
            }
            if(frameTicker.hasEventListener("enterFrame"))
            {
               frameTicker.removeEventListener("enterFrame",onEnterFrame);
            }
            if(_loc2_.seamlessResume && status == 0)
            {
               frameTicker.addEventListener("enterFrame",onEnterFrameResume,false,2147483647);
            }
            else
            {
               super.resume();
               super.play(null);
               appendBytesAction("resetBegin");
               appendBytes(new FlvHeader());
            }
            while(loaders && loaders.length)
            {
               stripLoader(loaders.pop());
            }
            if(!predictTimer)
            {
               predictTimer = new Timer(_loc2_.duration);
               predictTimer.addEventListener("timer",onPredictTimer);
            }
            chunkLength = _loc2_.duration;
            predictTimer.delay = _loc2_.duration;
            predictTimer.reset();
            predictTimer.start();
            status = 1;
            _currentAudioCount = 0;
            _currentVideoCount = 0;
            _totalAudioCount = 0;
            _totalVideoCount = 0;
            _audioBitrate = 0;
            _videoBitrate = 0;
            _totalByteCount = 0;
            _bufferEmpty = true;
            keyPositions = null;
            loaders.push(createLoader(_loc2_.streamName,_loc2_.index,_loc2_.byteOffset));
            loaders[0].targetId++;
            loaders[0].addEventListener("progress",onLoaderProgress);
            dispatchNetStatusEvent("NetStream.Play.Start");
         }
      }
      
      override public function play2(param1:NetStreamPlayOptions) : void {
         if(param1 is ChunkStreamPlayOptions)
         {
            echo("public play2 > " + param1.streamName);
            if(loaders && loaders.length)
            {
               if(loaders[0].pattern == param1.streamName)
               {
                  echo("streamNames matching, ignoring switch",1);
                  if(_swapStreamName)
                  {
                     while(loaders && loaders.length > 1)
                     {
                        stripLoader(loaders.pop());
                     }
                     _swapStreamName = null;
                     _swapTimeStamp = -1;
                     _swapOffset = 0;
                     _swapIndex = -1;
                     echo("ongoing switch canceled",1);
                  }
                  return;
               }
               if(_swapStreamName == param1.streamName)
               {
                  echo("switch in progress to same stream, ignoring",1);
                  return;
               }
               if(!transitionTimer)
               {
                  echo("create transition timer",0);
                  transitionTimer = new Timer(2000,1);
                  transitionTimer.addEventListener("timerComplete",onTransitionFailed);
               }
               transitionTimer.reset();
               while(loaders && loaders.length > 1)
               {
                  stripLoader(loaders.pop());
               }
               echo("play2 transition : " + param1.transition,0);
               _loc2_ = param1.transition;
               if("switch" !== _loc2_)
               {
                  if("swap" !== _loc2_)
                  {
                     throw new Error("Transition type \'" + param1.transition + "\' is not supported.");
                  }
                  else
                  {
                     _swapTimeStamp = 0;
                     loaders.push(createLoader(param1.streamName,param1.index,param1.byteOffset,param1.byteOffset == 0));
                  }
               }
               else
               {
                  _swapTimeStamp = -1;
                  if(keyPositions)
                  {
                     _swapIndex = -1;
                     _swapOffset = 0;
                     setSwapTimeStamp(param1.streamName);
                  }
                  else
                  {
                     _swapIndex = param1.index;
                     _swapOffset = param1.offset;
                     echo("keyPositions missing");
                  }
                  transitionTimer.delay = keyPositions?10000:2000;
                  transitionTimer.start();
               }
               _swapStreamName = param1.streamName;
               dispatchNetStatusEvent("NetStream.Play.Transition","status","NetStream.Transition.Success");
            }
            else
            {
               echo("no loaders present, calling play()",2);
               play(param1);
            }
            return;
         }
         throw new ArgumentError("Invalid argument type : " + typeof param1);
      }
      
      override public function pause() : void {
         echo("public pause");
         if(status > 0)
         {
            status = 0;
            super.pause();
         }
         else
         {
            echo("  not playing");
         }
      }
      
      override public function resume() : void {
         echo("public resume");
         if(status == 0)
         {
            status = 1;
            super.resume();
         }
         else
         {
            echo("  not paused");
         }
      }
      
      override public function seek(param1:Number) : void {
      }
      
      override public function close() : void {
         echo("public close");
         while(loaders && loaders.length)
         {
            stripLoader(loaders.pop());
         }
         loaders = null;
         if(predictTimer)
         {
            predictTimer.removeEventListener("timer",onPredictTimer);
            predictTimer.reset();
            predictTimer = null;
         }
         if(frameTicker)
         {
            frameTicker.removeEventListener("enterFrame",onEnterFrame);
            frameTicker = null;
         }
         if(dataRateTimer)
         {
            dataRateTimer.removeEventListener("timer",onDataRateTimer);
            dataRateTimer.reset();
            dataRateTimer = null;
         }
         if(transitionTimer)
         {
            transitionTimer.removeEventListener("timerComplete",onTransitionFailed);
            transitionTimer.reset();
            transitionTimer = null;
         }
         resetQueue = null;
         status = 0;
         super.close();
      }
      
      private function createLoader(param1:String, param2:Number, param3:uint = 0, param4:Boolean = false) : ChunkLoader {
         echo("createLoader " + param1 + " " + param2 + " " + param3);
         var _loc6_:Object = {};
         if(_hashRanges && _hashRanges[param1])
         {
            _loc9_ = 0;
            _loc8_ = _hashRanges[param1];
            for(_loc7_ in _hashRanges[param1])
            {
               _loc6_[_loc7_] = _hashRanges[param1][_loc7_];
            }
         }
         var _loc5_:ChunkLoader = new ChunkLoader(param1,param2,_loc6_,param3,param4);
         _loc5_.addEventListener("chunkInfo",onLoaderChunkInfo);
         _loc5_.addEventListener("slowStart",onLoaderSlowStart);
         _loc5_.addEventListener("timeout",onLoaderTimeout);
         _loc5_.addEventListener("ioError",onLoaderIoError);
         _loc5_.addEventListener("parserError",onLoaderParserError);
         _loc5_.addEventListener("securityError",onLoaderSecurityError);
         _loc5_.addEventListener("init",onLoaderInit);
         _loc5_.addEventListener("start",onLoaderStart);
         _loc5_.addEventListener("complete",onLoaderComplete);
         return _loc5_;
      }
      
      private function stripLoader(param1:ChunkLoader) : void {
         echo("stripping " + param1.pattern);
         param1.removeEventListener("chunkInfo",onLoaderChunkInfo);
         param1.removeEventListener("slowStart",onLoaderSlowStart);
         param1.removeEventListener("timeout",onLoaderTimeout);
         param1.removeEventListener("ioError",onLoaderIoError);
         param1.removeEventListener("parserError",onLoaderParserError);
         param1.removeEventListener("securityError",onLoaderSecurityError);
         param1.removeEventListener("complete",onLoaderComplete);
         param1.removeEventListener("progress",onLoaderProgress);
         param1.destroy();
      }
      
      protected function getLoaderIndex(param1:ChunkLoader) : int {
         var _loc2_:* = 0;
         _loc2_ = 0;
         while(_loc2_ < loaders.length)
         {
            if(param1 == loaders[_loc2_])
            {
               return _loc2_;
            }
            _loc2_++;
         }
         return -1;
      }
      
      private function onLoaderSecurityError(param1:Event) : void {
         echo("onLoaderSecurityError",3);
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         stripLoader(_loc2_);
         dispatchNetStatusEvent("ChunkStream.Load.FatalError","error",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
      }
      
      private function onLoaderParserError(param1:Event) : void {
         echo("onLoaderParserError",3);
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         stripLoader(_loc2_);
         dispatchNetStatusEvent("ChunkStream.Load.ParserError","error",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
      }
      
      private function onLoaderTimeout(param1:Event) : void {
         dispatchNetStatusEvent("ChunkStream.Load.Timeout","error",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
      }
      
      private function onLoaderIoError(param1:Event) : void {
         echo("onLoaderIoError " + typeof param1,2,param1 is DynamicEvent?(param1 as DynamicEvent).data:null);
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         if(param1 is DynamicEvent && (param1 as DynamicEvent).data.fatal)
         {
            stripLoader(_loc2_);
            dispatchNetStatusEvent("ChunkStream.Load.FatalError","error",(param1 as DynamicEvent).data.url);
         }
         else
         {
            dispatchNetStatusEvent("ChunkStream.Load.IoError","error",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
            if(predictTimer)
            {
               predictTimer.stop();
            }
            if(_loc2_.ioErrorCount >= 2)
            {
               if(getLoaderIndex(_loc2_) > 0)
               {
                  _swapStreamName = null;
                  _swapTimeStamp = -1;
                  loaders.splice(getLoaderIndex(_loc2_),1);
                  dispatchNetStatusEvent("NetStream.Play.Failed");
               }
               stripLoader(_loc2_);
               dispatchNetStatusEvent("ChunkStream.Load.UnrecoverableError","error",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
            }
         }
      }
      
      private function onLoaderInit(param1:Event) : void {
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         if(_loc2_ == loaders[0])
         {
            dispatchNetStatusEvent("ChunkStream.Load.Init","status",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
         }
      }
      
      private function onLoaderStart(param1:Event) : void {
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         if(_loc2_ == loaders[0])
         {
            dispatchNetStatusEvent("ChunkStream.Load.Start","status",param1 is DynamicEvent?(param1 as DynamicEvent).data.url:"");
         }
      }
      
      private function onLoaderProgress(param1:Event) : void {
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         if(_loc2_ && _loc2_.timeAvailableInQueue > 3000)
         {
            echo("### onLoaderProgress");
            _loc2_.removeEventListener("progress",onLoaderProgress);
            if(frameTicker && !frameTicker.hasEventListener("enterFrame"))
            {
               echo("adding ENTER_FRAME listener",0);
               frameTicker.addEventListener("enterFrame",onEnterFrame);
            }
            else
            {
               echo("no frameTicker, or already has listener",3);
            }
         }
      }
      
      private function onLoaderComplete(param1:Event) : void {
         var _loc2_:* = null;
         var _loc4_:* = null;
         var _loc3_:ChunkLoader = param1.target as ChunkLoader;
         if(_loc3_ == loaders[0])
         {
            if(predictTimer && !predictTimer.running)
            {
               predictTimer.reset();
               predictTimer.start();
            }
            _loc2_ = null;
            _loc4_ = null;
            if(param1 is DynamicEvent)
            {
               _loc2_ = (param1 as DynamicEvent).data.url;
               _loc4_ = {};
               _loc4_.time = (param1 as DynamicEvent).data.time;
               _loc4_.size = (param1 as DynamicEvent).data.size;
            }
            dispatchNetStatusEvent("ChunkStream.Load.Complete","status",_loc2_,_loc4_);
         }
      }
      
      private function onLoaderSlowStart(param1:Event) : void {
         var _loc2_:ChunkLoader = param1.target as ChunkLoader;
         if(_loc2_ == loaders[0])
         {
            dispatchNetStatusEvent("ChunkStream.Load.SlowStart");
         }
      }
      
      private function onLoaderChunkInfo(param1:DynamicEvent) : void {
         _switchEnabled = true;
         keyPositions = param1.data;
         keyDataLength = param1.size;
         keyDataIndex = param1.index;
         if(_swapStreamName && _swapTimeStamp == -1)
         {
            setSwapTimeStamp(_swapStreamName);
         }
         dispatchNetStatusEvent("ChunkStream.Feed.SyncInfo");
         echo("onLoaderChunkInfo",0,keyPositions);
      }
      
      private function onPredictTimer(param1:TimerEvent) : void {
         var _loc2_:* = 0;
         echo("onPredictTimer");
         if(predictTimer.delay != chunkLength)
         {
            predictTimer.delay = chunkLength;
            predictTimer.reset();
            predictTimer.start();
         }
         if(status && loaders.length)
         {
            _loc2_ = 0;
            while(_loc2_ < loaders.length)
            {
               loaders[_loc2_].targetId++;
               _loc2_++;
            }
            if(loaders[0].currentId < loaders[0].targetId - 10)
            {
               dispatchNetStatusEvent("ChunkStream.Predict.ExcessLag","error");
            }
         }
      }
      
      private var _totalByteCount:uint = 0;
      
      private var _currentAudioCount:uint = 0;
      
      private var _currentVideoCount:uint = 0;
      
      private var _totalAudioCount:uint = 0;
      
      private var _totalVideoCount:uint = 0;
      
      private var _audioBitrate:uint = 0;
      
      private var _videoBitrate:uint = 0;
      
      private var _lastTimeStamp:Number = 0;
      
      private var _swapTimeStamp:Number = -1;
      
      private var _swapStreamName:String;
      
      private var _swapIndex:int = -1;
      
      private var _swapOffset:uint = 0;
      
      private var resetQueue:Vector.<FlvTag>;
      
      private function onEnterFrame(param1:Event) : void {
         var _loc2_:* = null;
         if(loaders && loaders.length && loaders[0])
         {
            while((bufferLength < 1 || bufferLength < super.bufferTime * 1.1) && loaders[0].availableTags)
            {
               _loc2_ = loaders[0].getNextTag();
               if(loaders.length > 1 && (_swapTimeStamp == 0 && loaders[1].availableTags || _swapTimeStamp > 0 && (_loc2_.isKeyFrame) && _loc2_.timeStamp >= _swapTimeStamp))
               {
                  if(_swapTimeStamp != 0)
                  {
                     echo("keyframe met, dropping loader[ 0 ]",1);
                     echo("#### _swapTimeStamp : " + _swapTimeStamp);
                     echo("#### _keyframe " + _loc2_.timeStamp);
                  }
                  else
                  {
                     echo("transition forced",2);
                     frameTicker.removeEventListener("enterFrame",onEnterFrame);
                     super.seek(0);
                  }
                  stripLoader(loaders[0]);
                  loaders.splice(0,1);
                  _swapTimeStamp = -1;
                  _swapStreamName = null;
                  transitionTimer.reset();
                  dispatchNetStatusEvent("NetStream.Play.TransitionComplete");
                  return;
               }
            }
         }
         else
         {
            echo("!!!!!!! no loader");
            close();
         }
         if(!bufferEmpty && bufferLength == 0)
         {
            bufferEmpty = true;
         }
         else if(bufferEmpty && bufferLength >= 1)
         {
            bufferEmpty = false;
         }
         
      }
      
      public function reset() : void {
         echo("public reset");
         frameTicker.removeEventListener("enterFrame",onEnterFrame);
         super.seek(0);
         addEventListener("netStatus",onResetNetStatus,false,2147483647);
      }
      
      private function onResetNetStatus(param1:NetStatusEvent) : void {
         var _loc3_:* = null;
         var _loc2_:* = 0;
         var _loc4_:* = param1.info.code;
         if("NetStream.Seek.Notify" === _loc4_)
         {
            removeEventListener("netStatus",onResetNetStatus);
            echo("RESET_SEEK");
            appendBytesAction("resetSeek");
            _loc3_ = null;
            if(loaders && loaders.length && loaders[0].availableTimeAfterKeyframe > chunkLength)
            {
               echo("onResetNetStatus appending from loader queue");
               while(!_loc3_ || _loc3_ && !_loc3_.isKeyFrame)
               {
                  _loc3_ = loaders[0].getNextTag();
               }
               resetQueue = new Vector.<FlvTag>();
               resetQueue.push(_loc3_);
               appendBytes(_loc3_);
               _lastTimeStamp = _loc3_.timeStamp;
            }
            else if(resetQueue)
            {
               echo("onResetNetStatus appending from reset queue");
               _loc2_ = 0;
               while(_loc2_ < resetQueue.length)
               {
                  _loc3_ = resetQueue[_loc2_];
                  if(_loc3_.isKeyFrame)
                  {
                     _lastTimeStamp = _loc3_.timeStamp;
                  }
                  appendBytes(resetQueue[_loc2_]);
                  _loc2_++;
               }
            }
            
         }
      }
      
      private function onEnterFrameResume(param1:Event) : void {
         if(loaders && loaders.length && loaders[0].availableTags)
         {
            super.resume();
            super.seek(0);
            appendBytesAction("resetSeek");
            frameTicker.removeEventListener("enterFrame",onEnterFrameResume);
         }
      }
      
      private function onDataRateTimer(param1:TimerEvent) : void {
         _audioBitrate = _currentAudioCount * 1000 / dataRateTimer.delay;
         _videoBitrate = _currentVideoCount * 1000 / dataRateTimer.delay;
         _totalAudioCount = _totalAudioCount + _currentAudioCount;
         _totalVideoCount = _totalVideoCount + _currentVideoCount;
         _currentAudioCount = 0;
         _currentVideoCount = 0;
      }
      
      private function setSwapTimeStamp(param1:String) : void {
         var _loc3_:* = 0;
         var _loc4_:* = 0;
         _swapTimeStamp = -1;
         var _loc2_:Object = getKeyInfo(pattern);
         var _loc5_:Object = getKeyInfo(param1);
         echo("setSwapTimeStamp");
         echo("_lastTimeStamp : " + _lastTimeStamp);
         if(_loc2_ && _loc2_.keys is Array && _loc5_ && _loc5_.keys is Array)
         {
            _loc3_ = -1;
            _loc4_ = 0;
            while(_loc4_ < _loc2_.keys.length)
            {
               if(_loc2_.keys[_loc4_].ts > _lastTimeStamp)
               {
                  _swapTimeStamp = _loc2_.keys[_loc4_].ts;
                  _loc3_ = _loc4_;
                  break;
               }
               _loc4_++;
            }
            echo("#### _swapTimeStamp : " + _swapTimeStamp);
            echo("#### keyIndex : " + _loc3_);
            if(_loc3_ != -1)
            {
               loaders.push(createLoader(param1,keyDataIndex,_loc5_.keys[_loc3_].bpos + keyDataLength));
            }
            else
            {
               echo("no suitable keyframe found, waiting for next meta object");
            }
         }
      }
      
      private function onTransitionFailed(param1:TimerEvent) : void {
         if(_swapTimeStamp == -1)
         {
            echo("onTransitionFailed",2);
            _swapTimeStamp = 0;
            if(_swapIndex == -1)
            {
               _swapIndex = loaders[0].currentId;
            }
            loaders.push(createLoader(_swapStreamName,_swapIndex,_swapOffset,_swapOffset == 0));
            dispatchNetStatusEvent("NetStream.Play.Transition","status","NetStream.Transition.Forced");
         }
      }
      
      override public function get info() : NetStreamInfo {
         var _loc1_:NetStreamInfo = super.info;
         var _loc2_:NetStreamInfo = new NetStreamInfo(currentBandwidth,_totalByteCount,maximumBandwidth,_audioBitrate,_totalAudioCount,_videoBitrate,_totalVideoCount,_loc1_.dataBytesPerSecond,_loc1_.dataByteCount,_loc1_.playbackBytesPerSecond,_loc1_.droppedFrames,_loc1_.audioBufferByteLength,_loc1_.videoBufferByteLength,_loc1_.dataBufferByteLength,_loc1_.audioBufferLength,_loc1_.videoBufferLength,_loc1_.dataBufferLength,_loc1_.SRTT,_loc1_.audioLossRate,_loc1_.videoLossRate);
         return _loc2_;
      }
      
      protected function get bufferEmpty() : Boolean {
         return _bufferEmpty;
      }
      
      protected function set bufferEmpty(param1:Boolean) : void {
         if(_bufferEmpty != param1)
         {
            _bufferEmpty = param1;
            if(_bufferEmpty)
            {
               dispatchNetStatusEvent("NetStream.Buffer.Empty");
               if(playing)
               {
                  addEventListener("netStatus",suppressPauseEvents,false,2147483647);
                  super.pause();
               }
               if(loaders && loaders.length && !loaders[0].hasEventListener("progress"))
               {
                  frameTicker.removeEventListener("enterFrame",onEnterFrame);
                  loaders[0].addEventListener("progress",onLoaderProgress);
                  echo("### added onLoaderProgress");
               }
            }
            else
            {
               dispatchNetStatusEvent("NetStream.Buffer.Full");
               if(playing)
               {
                  addEventListener("netStatus",suppressPauseEvents,false,2147483647);
                  super.resume();
               }
            }
         }
      }
      
      private var _hashRanges:Object;
      
      public function addHashRange(param1:String, param2:String, param3:String) : void {
         var _loc4_:* = 0;
         echo("addHashRange " + param1 + " " + param2 + " " + param3);
         if(!_hashRanges)
         {
            _hashRanges = {};
         }
         if(!_hashRanges[param1])
         {
            _hashRanges[param1] = {};
         }
         _hashRanges[param1][param2] = param3;
         _loc4_ = 0;
         while(_loc4_ < loaders.length)
         {
            if(loaders[_loc4_] && loaders[_loc4_].pattern == param1)
            {
               loaders[_loc4_].hashRange[param2] = param3;
            }
            _loc4_++;
         }
      }
      
      private function getKeyInfo(param1:String) : Object {
         if(keyPositions && keyPositions.streams)
         {
            _loc4_ = 0;
            _loc3_ = keyPositions.streams;
            for(_loc2_ in keyPositions.streams)
            {
               if(param1.indexOf(_loc2_) != -1)
               {
                  return keyPositions.streams[_loc2_];
               }
            }
         }
         return null;
      }
   }
}
