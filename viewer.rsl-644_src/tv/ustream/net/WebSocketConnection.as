package tv.ustream.net
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.net.interfaces.ICommunicator;
   import com.worlize.websocket.WebSocket;
   import flash.net.Responder;
   import com.worlize.websocket.WebSocketEvent;
   import com.brokenfunction.json.decodeJson;
   import flash.events.IOErrorEvent;
   import com.worlize.websocket.WebSocketErrorEvent;
   import com.brokenfunction.json.encodeJson;
   
   public dynamic class WebSocketConnection extends Dispatcher implements ICommunicator
   {
      
      public function WebSocketConnection(param1:String = null) {
         super();
         _url = param1;
      }
      
      private var _webSocket:WebSocket;
      
      private var _connected:Boolean;
      
      private var _connectParams:Object;
      
      private var _client:Object;
      
      private var _killing:Boolean = false;
      
      private var _url:String;
      
      private var _ports:Array;
      
      public function call(param1:String, param2:Responder, ... rest) : void {
         echo("call");
         sendMessage(param1,rest);
      }
      
      public function close() : void {
         echo("close");
         if(_webSocket)
         {
            _webSocket.close();
         }
      }
      
      public function connect(param1:String, ... rest) : void {
         echo("connect");
         _url = param1;
         _connected = false;
         _connectParams = rest && !(rest[0] == null)?rest[0]:{};
         if(_webSocket != null)
         {
            destroyWebSocket();
         }
         _webSocket = new WebSocket(_url,"",null,10000);
         _webSocket.debug = true;
         _webSocket.addEventListener("open",onWebSocketOpen);
         _webSocket.addEventListener("closed",onWebSocketClosed);
         _webSocket.addEventListener("message",onWebSocketMessage);
         _webSocket.addEventListener("abnormalClose",onWebSocketAbnormalClose);
         _webSocket.addEventListener("connectionFail",onWebSocketConnectionFail);
         _webSocket.addEventListener("ioError",onWebSocketIOError);
         _webSocket.connect();
      }
      
      private function onWebSocketClosed(param1:WebSocketEvent) : void {
         echo("onWebSocketClosed");
         if(_killing)
         {
            destroyWebSocket();
            destroy();
         }
         dispatch("disconnected");
      }
      
      private function destroyWebSocket() : void {
         echo("destroyWebSocket");
         _webSocket.removeEventListener("open",onWebSocketOpen);
         _webSocket.removeEventListener("closed",onWebSocketClosed);
         _webSocket.removeEventListener("message",onWebSocketMessage);
         _webSocket.removeEventListener("abnormalClose",onWebSocketAbnormalClose);
         _webSocket.removeEventListener("connectionFail",onWebSocketConnectionFail);
         _webSocket.removeEventListener("ioError",onWebSocketIOError);
         _webSocket = null;
      }
      
      private function onWebSocketMessage(param1:WebSocketEvent) : void {
         var _loc3_:* = null;
         var _loc5_:* = null;
         var _loc2_:* = undefined;
         var _loc4_:* = null;
         echo("onWebSocketMessage");
         if(param1.message.type == "utf8")
         {
            _loc3_ = decodeJson(param1.message.utf8Data);
            _loc5_ = _loc3_.type;
            _loc2_ = _loc3_.data;
            _loc6_ = _loc5_;
            if("ping" !== _loc6_)
            {
               if(!connected)
               {
                  _connected = true;
                  dispatch("connected");
               }
               if(client && client[_loc5_] && client[_loc5_] is Function)
               {
                  _loc4_ = client[_loc5_] as Function;
                  _loc4_.apply(client,_loc2_);
               }
               else if(this[_loc5_] && this[_loc5_] is Function)
               {
                  _loc4_ = this[_loc5_] as Function;
                  _loc4_.apply(this,_loc2_);
               }
               
            }
            else
            {
               sendMessage("pong");
            }
         }
      }
      
      private function onWebSocketIOError(param1:IOErrorEvent) : void {
         echo("onWebSocketIOError");
      }
      
      private function onWebSocketAbnormalClose(param1:WebSocketErrorEvent) : void {
         echo("onWebSocketAbnormalClose");
      }
      
      private function onWebSocketOpen(param1:WebSocketEvent) : void {
         echo("onWebSocketOpen");
         sendMessage("connect",_connectParams);
      }
      
      private function onWebSocketConnectionFail(param1:WebSocketErrorEvent) : void {
         echo("onWebSocketConnectionFail");
         dispatch("failed");
      }
      
      private function sendMessage(param1:String, param2:* = null) : void {
         var _loc3_:* = null;
         var _loc4_:* = null;
         echo("sendMessage");
         if(_webSocket && _webSocket.connected)
         {
            _loc3_ = 
               {
                  "type":param1,
                  "data":param2
               };
            _loc4_ = encodeJson(_loc3_);
            if(_loc4_ && _loc4_.length > 0)
            {
               _webSocket.sendUTF(_loc4_);
            }
            else
            {
               echo("message has not sent (Json encode was not successfull, or length is zero).");
            }
         }
         else
         {
            echo("message has not sent because WebSocket is not yet opened.");
         }
      }
      
      public function kill() : void {
         echo("kill");
         _active = false;
         _killing = true;
         close();
      }
      
      public function get connected() : Boolean {
         return _connected;
      }
      
      public function get client() : Object {
         return _client;
      }
      
      public function set client(param1:Object) : void {
         _client = param1;
      }
      
      public function get ports() : Array {
         return _ports;
      }
      
      public function set ports(param1:Array) : void {
         _ports = param1;
      }
      
      public function get uri() : String {
         return _url;
      }
      
      public function get url() : String {
         return _url;
      }
   }
}
