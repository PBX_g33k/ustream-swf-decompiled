package tv.ustream.net.ums
{
   public class ServerItem extends Object
   {
      
      public function ServerItem(param1:String, param2:uint) {
         super();
         _ip = param1;
         _balanceWeight = param2;
      }
      
      private var _ip:String;
      
      private var _balanceWeight:uint;
      
      public function get ip() : String {
         return _ip;
      }
      
      public function get balanceWeight() : uint {
         return _balanceWeight;
      }
   }
}
