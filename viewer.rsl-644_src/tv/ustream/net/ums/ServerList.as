package tv.ustream.net.ums
{
   import tv.ustream.tools.Dispatcher;
   import flash.net.URLLoader;
   import flash.utils.Timer;
   import flash.net.URLRequest;
   import tv.ustream.debug.Debug;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.events.HTTPStatusEvent;
   import flash.events.Event;
   import com.brokenfunction.json.decodeJson;
   import flash.events.TimerEvent;
   
   public class ServerList extends Dispatcher
   {
      
      public function ServerList(param1:String) {
         super();
         _cluster = param1;
         _loader = new URLLoader();
         _loader.addEventListener("complete",onLoaderComplete);
         _loader.addEventListener("httpStatus",onLoaderHTTPStatus);
         _loader.addEventListener("securityError",onLoaderSecurityError);
         _loader.addEventListener("ioError",onLoaderIOError);
         _retryTimer = new Timer(3000,1);
         _retryTimer.addEventListener("timerComplete",onRetryTimerComplete);
         load();
      }
      
      private const JSON_URL:String = "http://uhs-level3.ustream.tv/sjc/dns.#CLUSTER#.json";
      
      private const MAX_RANDOM_COUNT:uint = 3;
      
      private const RETRY_DELAY_MIN:uint = 3000;
      
      private const RETRY_DELAY_MAX:uint = 6000;
      
      private var _cluster:String = "live";
      
      private var _list:Vector.<ServerItem>;
      
      private var _loader:URLLoader;
      
      private var _balanceWeightSum:uint = 0;
      
      private var _loading:Boolean = false;
      
      private var _randomCount:uint = 0;
      
      private var _retryTimer:Timer;
      
      override public function destroy(... rest) : * {
         if(_loader)
         {
            _loader.removeEventListener("complete",onLoaderComplete);
            _loader.removeEventListener("httpStatus",onLoaderHTTPStatus);
            _loader.removeEventListener("securityError",onLoaderSecurityError);
            _loader.removeEventListener("ioError",onLoaderIOError);
            try
            {
               _loader.close();
            }
            catch(err:Error)
            {
            }
            _loader = null;
         }
         if(_retryTimer)
         {
            _retryTimer.reset();
            _retryTimer.removeEventListener("timerComplete",onRetryTimerComplete);
            _retryTimer = null;
         }
         _list = null;
         return super.destroy();
      }
      
      private function load() : void {
         var _loc1_:* = null;
         if(!_loading)
         {
            _loc1_ = new URLRequest("http://uhs-level3.ustream.tv/sjc/dns.#CLUSTER#.json".split("#CLUSTER#").join(_cluster));
            Debug.debug("serverList","loading " + _loc1_.url);
            _loader.load(_loc1_);
            _loading = true;
            _list = new Vector.<ServerItem>();
            _randomCount = 0;
            _balanceWeightSum = 0;
         }
      }
      
      private function onLoaderIOError(param1:IOErrorEvent) : void {
         _loading = false;
         Debug.error("serverList","onLoaderIOError " + param1);
         _retryTimer.start();
      }
      
      private function onLoaderSecurityError(param1:SecurityErrorEvent) : void {
         _loading = false;
         Debug.error("serverList","onLoaderSecurityError " + param1);
         _retryTimer.start();
      }
      
      private function onLoaderHTTPStatus(param1:HTTPStatusEvent) : void {
         Debug.debug("serverList","onLoaderHTTPStatus " + param1.status);
      }
      
      private function onLoaderComplete(param1:Event) : void {
         var _loc3_:* = 0;
         _loading = false;
         var _loc2_:Array = decodeJson(_loader.data) as Array;
         Debug.debug("serverList","onLoaderComplete");
         _loc3_ = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(_loc2_[_loc3_].loadbalance)
            {
               _list.push(new ServerItem(_loc2_[_loc3_].ip,_loc2_[_loc3_].loadbalance));
               _balanceWeightSum = _balanceWeightSum + (_loc2_[_loc3_].loadbalance);
               Debug.debug("serverList","adding " + _list[_list.length - 1].ip + " " + _list[_list.length - 1].balanceWeight);
            }
            _loc3_++;
         }
         dispatch("complete");
      }
      
      public function getRandomIp() : String {
         var _loc3_:* = 0;
         var _loc1_:* = 0;
         var _loc4_:* = 0;
         var _loc2_:String = null;
         if(_list && _list.length)
         {
            _loc3_ = Math.random() * _balanceWeightSum;
            _loc1_ = 0;
            _loc4_ = 0;
            while(_loc4_ < _list.length)
            {
               if(_loc3_ >= _loc1_ && _loc3_ < _loc1_ + _list[_loc4_].balanceWeight)
               {
                  _loc2_ = _list[_loc4_].ip;
               }
               _loc1_ = _loc1_ + _list[_loc4_].balanceWeight;
               _loc4_++;
            }
            _randomCount = _randomCount + 1;
            Debug.debug("serverList","randomCount : " + _randomCount);
            if(_randomCount >= 3)
            {
               Debug.debug("serverList","updating");
               load();
            }
         }
         else
         {
            Debug.warn("serverList","list empty");
         }
         Debug.debug("serverList","returning " + _loc2_);
         return _loc2_;
      }
      
      public function get cluster() : String {
         return _cluster;
      }
      
      public function set cluster(param1:String) : void {
         _cluster = param1;
         Debug.debug("serverList","set cluster : " + param1);
         load();
      }
      
      private function onRetryTimerComplete(param1:TimerEvent) : void {
         _retryTimer.delay = 3000 + Math.random() * (6000 - 3000);
         _retryTimer.reset();
         Debug.debug("serverList","onRetryTimerComplete delay : " + _retryTimer.delay);
         load();
      }
   }
}
