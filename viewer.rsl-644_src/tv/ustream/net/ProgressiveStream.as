package tv.ustream.net
{
   import flash.net.NetStream;
   import tv.ustream.flv.FlvParser;
   import flash.events.NetStatusEvent;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.Event;
   import flash.net.NetConnection;
   
   public class ProgressiveStream extends NetStream
   {
      
      public function ProgressiveStream(param1:NetConnection, param2:String = "connectToFMS", param3:Boolean = false) {
         this.noMeta = param3;
         super(param1,param2);
         addEventListener("netStatus",onNetStatus);
      }
      
      public var seekParameter:String = "fs";
      
      private var parser:FlvParser;
      
      private var noMeta:Boolean = false;
      
      private var duration:Number = 0;
      
      private var keyTimes:Array;
      
      private var keyBytes:Array;
      
      private var streamName:String;
      
      private var streamPosition:Number = 0;
      
      private var seekPosition:Number = -1;
      
      private var bufferBeforeJump:Number = 0;
      
      private var initComplete:Boolean = false;
      
      private var jumpPosition:Number = -1;
      
      private function onNetStatus(param1:NetStatusEvent) : void {
         var _loc2_:* = NaN;
         echo("onNetStatus " + param1.info.code,0);
         var _loc3_:* = param1.info.code;
         if("NetStream.Buffer.Full" !== _loc3_)
         {
            if("NetStream.Seek.Notify" !== _loc3_)
            {
               if("NetStream.Seek.InvalidTime" !== _loc3_)
               {
                  echo("unhandled netstatus : " + param1.info.code,0);
               }
               else
               {
                  echo("invalid time");
                  if(seekPosition != -1)
                  {
                     jump(seekPosition);
                     seekPosition = -1;
                  }
                  param1.stopImmediatePropagation();
                  param1.stopPropagation();
               }
            }
            else
            {
               seekPosition = -1;
            }
         }
         else
         {
            if(!initComplete)
            {
               initComplete = true;
               echo("seekPosition : " + seekPosition);
               if(seekPosition != -1)
               {
                  _loc2_ = seekPosition;
                  seekPosition = -1;
                  seek(_loc2_);
               }
            }
            if(bufferBeforeJump)
            {
               bufferTime = bufferBeforeJump;
               bufferBeforeJump = 0;
            }
            jumpPosition = -1;
         }
      }
      
      private function echo(param1:String, param2:int = -1) : void {
         Debug.echo("[ PROGRESSIVESTREAM ] " + param1,param2);
      }
      
      override public function play(... rest) : void {
         echo("public play");
         if(rest && rest.length)
         {
            streamName = rest[0];
         }
         keyTimes = null;
         keyBytes = null;
         if(noMeta || streamName.indexOf("http://") == -1)
         {
            echo("super.play");
            super.play.apply(this,rest);
         }
         else
         {
            echo("loading meta");
            if(!parser)
            {
               parser = new FlvParser();
               parser.addEventListener("exceededMetadataParseLimit",onParserMetaExceeded);
               parser.addEventListener("metadata",onParserMetaData);
            }
            parser.load(streamName);
         }
      }
      
      override public function seek(param1:Number) : void {
         var _loc2_:* = NaN;
         echo("public seek to " + param1);
         if(initComplete)
         {
            _loc2_ = streamPosition + (duration - streamPosition) * bytesLoaded / bytesTotal;
            echo("streamPosition : " + streamPosition);
            echo("offset : " + param1);
            echo("maxAvailable : " + _loc2_);
            if(param1 >= streamPosition && param1 < _loc2_)
            {
               seekPosition = param1;
               super.seek(param1);
            }
            else
            {
               jump(param1);
            }
         }
         else
         {
            echo("!initComplete, storing");
            seekPosition = param1;
         }
      }
      
      override public function close() : void {
         echo("public close");
         removeEventListener("netStatus",onNetStatus);
         if(parser)
         {
            parser.removeEventListener("exceededMetadataParseLimit",onParserMetaExceeded);
            parser.removeEventListener("metadata",onParserMetaData);
            parser = null;
         }
         super.close();
      }
      
      override public function get time() : Number {
         return jumpPosition == -1?super.time:jumpPosition;
      }
      
      private function onParserMetaData(param1:DynamicEvent) : void {
         echo("onParserMetaData");
         if(param1.data && param1.data.keyframes)
         {
            keyTimes = param1.data.keyframes.times;
            keyBytes = param1.data.keyframes.filepositions;
            duration = param1.data.duration;
         }
         super.play(streamName);
      }
      
      private function onParserMetaExceeded(param1:Event) : void {
         super.play(streamName);
      }
      
      private function jump(param1:Number) : void {
         echo("jump to " + param1);
         var param1:Number = Math.min(duration,Math.max(0,param1));
         streamPosition = getTimeForOffset(param1);
         echo("streamposition set to " + streamPosition);
         var _loc2_:String = streamName;
         _loc2_ = _loc2_ + (_loc2_.indexOf("?") == -1?"?":"&");
         _loc2_ = _loc2_ + (seekParameter + "=" + getByteForOffset(param1));
         jumpPosition = param1;
         bufferBeforeJump = bufferTime;
         bufferTime = 0.5;
         super.play(_loc2_);
      }
      
      private function getByteForOffset(param1:Number) : uint {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         if(keyTimes && keyBytes && keyTimes.length && keyTimes.length == keyBytes.length)
         {
            _loc2_ = 0;
            _loc3_ = 0;
            while(_loc3_ < keyTimes.length)
            {
               if(keyTimes[_loc3_] < param1)
               {
                  _loc2_ = _loc3_;
                  _loc3_++;
                  continue;
               }
               break;
            }
            return keyBytes[_loc2_];
         }
         return 0;
      }
      
      private function getTimeForOffset(param1:Number) : uint {
         var _loc2_:* = 0;
         var _loc3_:* = 0;
         if(keyTimes && keyBytes && keyTimes.length && keyTimes.length == keyBytes.length)
         {
            _loc2_ = 0;
            _loc3_ = 0;
            while(_loc3_ < keyTimes.length)
            {
               if(keyTimes[_loc3_] < param1)
               {
                  _loc2_ = _loc3_;
                  _loc3_++;
                  continue;
               }
               break;
            }
            return keyTimes[_loc2_];
         }
         return 0;
      }
   }
}
