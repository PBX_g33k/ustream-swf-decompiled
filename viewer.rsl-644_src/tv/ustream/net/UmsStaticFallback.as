package tv.ustream.net
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.Timer;
   import flash.net.URLStream;
   import flash.utils.ByteArray;
   import flash.events.HTTPStatusEvent;
   import flash.events.ErrorEvent;
   import flash.events.ProgressEvent;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   import flash.events.TimerEvent;
   import flash.net.URLRequest;
   
   public class UmsStaticFallback extends Dispatcher
   {
      
      public function UmsStaticFallback(param1:String) {
         super();
         this.mediaId = param1;
         timer = new Timer(20000,1);
         timer.addEventListener("timerComplete",onTimer);
         timer.start();
         echo("started update timer");
         loader = new URLStream();
         loader.addEventListener("httpStatus",onLoaderStatus);
         loader.addEventListener("ioError",onLoaderError);
         loader.addEventListener("securityError",onLoaderError);
         loader.addEventListener("progress",onLoaderProgress);
         loader.addEventListener("complete",onLoaderComplete);
      }
      
      public static const UPDATE:String = "update";
      
      private const UPDATE_DEFAULT_DELAY:uint = 20000;
      
      private const UPDATE_ERROR_RANDOM_DELAY:uint = 20000;
      
      private const MAXIMUM_ERROR_COUNT:uint = 4294967295;
      
      private var timer:Timer;
      
      private var loader:URLStream;
      
      private var stuff:ByteArray;
      
      private var mediaId:String;
      
      private var errorCount:uint = 0;
      
      private function onLoaderStatus(param1:HTTPStatusEvent) : void {
         echo("onLoaderStatus " + param1.status);
      }
      
      private function onLoaderError(param1:ErrorEvent) : void {
         timer.delay = 20000 + Math.floor(20000 * Math.random());
         timer.reset();
         timer.start();
         errorCount = errorCount + 1;
         echo("onLoaderError (#" + errorCount + ") " + param1.toString());
         if(errorCount >= 4294967295)
         {
            destroy();
         }
      }
      
      private function onLoaderProgress(param1:ProgressEvent) : void {
         echo("onLoaderProgress");
         if(loader.bytesAvailable)
         {
            loader.readBytes(stuff,stuff.length,loader.bytesAvailable);
         }
      }
      
      private function onLoaderComplete(param1:Event) : void {
         echo("onLoaderComplete");
         echo("stuff : " + stuff.length);
         stuff.objectEncoding = 0;
         stuff.position = 0;
         var _loc2_:Object = stuff.readObject();
         if(_loc2_)
         {
            _loc2_.meta = {};
            _loc2_.meta.adFree = true;
         }
         Debug.explore(_loc2_);
         dispatch("update",false,{"moduleInfo":_loc2_});
         timer.delay = 20000;
         timer.reset();
         timer.start();
         errorCount = 0;
      }
      
      private function onTimer(param1:TimerEvent) : void {
         var _loc2_:String = "http://uhs-akamai.ustream.tv/sjc/1/" + mediaId + "/live.amf";
         echo("loading " + _loc2_);
         loader.load(new URLRequest(_loc2_));
         stuff = new ByteArray();
      }
      
      override public function destroy(... rest) : * {
         if(loader)
         {
            try
            {
               loader.close();
            }
            catch(err:Error)
            {
            }
            loader.addEventListener("httpStatus",onLoaderStatus);
            loader.addEventListener("ioError",onLoaderError);
            loader.addEventListener("securityError",onLoaderError);
            loader.addEventListener("progress",onLoaderProgress);
            loader.addEventListener("complete",onLoaderComplete);
            loader = null;
         }
         if(timer)
         {
            timer.reset();
            timer.removeEventListener("timerComplete",onTimer);
            timer = null;
         }
         if(stuff)
         {
            stuff.clear();
            stuff = null;
         }
         return super.destroy();
      }
   }
}
