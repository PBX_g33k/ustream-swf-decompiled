package tv.ustream.net
{
   import tv.ustream.tools.Dispatcher;
   import flash.net.URLStream;
   import tv.ustream.flv.FlvParser;
   import tv.ustream.flv.FlvTag;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.Event;
   import flash.utils.ByteArray;
   import flash.utils.getTimer;
   import flash.events.ProgressEvent;
   import flash.events.HTTPStatusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.net.URLRequest;
   import tv.ustream.tools.StringUtils;
   
   public class ChunkLoader extends Dispatcher
   {
      
      public function ChunkLoader(param1:String, param2:Number, param3:Object, param4:uint = 0, param5:Boolean = false) {
         super(param1.split("/").pop() + " " + StringUtils.random(3));
         _pattern = param1;
         _currentId = param2;
         _targetId = param2;
         _hashRange = param3;
         _byteOffset = param4;
         echo("constructor");
         echo("pattern : " + param1 + ", chunkId : " + param2);
         retryTimer = new Timer(2000,1);
         retryTimer.addEventListener("timerComplete",loadChunk);
         _loader = new URLStream();
         _loader.addEventListener("complete",onLoaderComplete);
         _loader.addEventListener("httpStatus",onLoaderHttpStatus);
         _loader.addEventListener("ioError",onLoaderIoError);
         _loader.addEventListener("progress",onLoaderProgress);
         _loader.addEventListener("securityError",onLoaderSecurityError);
         _parser = new FlvParser(null,false);
         _parser.addEventListener("meta",onParserPacket);
         _parser.addEventListener("audio",onParserPacket);
         _parser.addEventListener("video",onParserPacket);
         _parser.addEventListener("malformedData",onParserError);
         _tagQueue = new Vector.<FlvTag>();
         _bandwidthList = [];
         if(_pattern && _currentId)
         {
            loadChunk();
         }
      }
      
      public static const INIT:String = "init";
      
      public static const START:String = "start";
      
      public static const PROGRESS:String = "progress";
      
      public static const COMPLETE:String = "complete";
      
      public static const SLOW_START:String = "slowStart";
      
      public static const TIMEOUT:String = "timeout";
      
      public static const IO_ERROR:String = "ioError";
      
      public static const SECURITY_ERROR:String = "securityError";
      
      public static const PARSER_ERROR:String = "parserError";
      
      public static const CHUNK_INFO:String = "chunkInfo";
      
      private const SLOW_START_THRESHOLD:Number = 500;
      
      private const TIMEOUT_THRESHOLD:uint = 3000;
      
      private var _pattern:String;
      
      private var _currentId:Number;
      
      private var _targetId:Number;
      
      private var _byteOffset:uint;
      
      private var _loader:URLStream;
      
      private var _loading:Boolean = false;
      
      private var _loaderUrl:String;
      
      private var _parser:FlvParser;
      
      private var _tagQueue:Vector.<FlvTag>;
      
      private var _hashRange:Object;
      
      private var _currentBandwidth:int = -1;
      
      private var _maximumBandwidth:int = -1;
      
      private var _bandwidthList:Array;
      
      private var _ioErrorCount:uint = 0;
      
      private var retryTimer:Timer;
      
      private var retryCount:uint = 0;
      
      public var clearPrecedingDeltas:Boolean = false;
      
      private var firstKeyFound:Boolean = false;
      
      public var maxTimeInQueue:uint = 20000;
      
      private var timeOutTimer:Timer;
      
      public function get pattern() : String {
         return _pattern;
      }
      
      public function set pattern(param1:String) : void {
         _pattern = param1;
      }
      
      public function get currentId() : Number {
         return _currentId;
      }
      
      public function get targetId() : Number {
         return _targetId;
      }
      
      public function set targetId(param1:Number) : void {
         _targetId = param1;
         echo("targetId " + _targetId);
         if(_loader && !_loading && _currentId <= _targetId && timeAvailableInQueue < maxTimeInQueue)
         {
            loadChunk();
         }
      }
      
      public function get availableTags() : uint {
         return _tagQueue?_tagQueue.length:0;
      }
      
      public function get availableKeyframes() : uint {
         var _loc2_:* = 0;
         var _loc1_:uint = 0;
         if(_tagQueue && _tagQueue.length)
         {
            _loc2_ = 0;
            while(_loc2_ < _tagQueue.length)
            {
               if(_tagQueue[_loc2_].isKeyFrame)
               {
                  _loc1_++;
               }
               _loc2_++;
            }
         }
         return _loc1_;
      }
      
      public function get availableTimeAfterKeyframe() : uint {
         var _loc2_:* = 0;
         var _loc3_:uint = 0;
         var _loc1_:FlvTag = null;
         if(_tagQueue && _tagQueue.length)
         {
            _loc2_ = 0;
            while(_loc2_ < _tagQueue.length)
            {
               if(_tagQueue[_loc2_].isKeyFrame)
               {
                  _loc1_ = _tagQueue[_loc2_];
                  break;
               }
               _loc2_++;
            }
            if(_loc1_)
            {
               _loc3_ = _tagQueue[_tagQueue.length - 1].timeStamp - _loc1_.timeStamp;
            }
         }
         return _loc3_;
      }
      
      public function get loading() : Boolean {
         return _loading;
      }
      
      public function get hashRange() : Object {
         if(!_hashRange)
         {
            _hashRange = {};
         }
         return _hashRange;
      }
      
      public function get currentBandwidth() : int {
         return _currentBandwidth;
      }
      
      public function get maximumBandwidth() : int {
         return _maximumBandwidth;
      }
      
      public function get averageBandwidth() : int {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         if(_bandwidthList && _bandwidthList.length)
         {
            _loc1_ = 0;
            while(_loc1_ < _bandwidthList.length)
            {
               _loc2_ = _loc2_ + _bandwidthList[_loc1_];
               _loc1_++;
            }
            _loc2_ = _loc2_ / _bandwidthList.length;
         }
         return _loc2_;
      }
      
      public function get bandwidthReliable() : Boolean {
         return _bandwidthList && _bandwidthList.length >= 3;
      }
      
      override public function destroy(... rest) : * {
         echo("destroy");
         if(_loader)
         {
            _loader.removeEventListener("complete",onLoaderComplete);
            _loader.removeEventListener("httpStatus",onLoaderHttpStatus);
            _loader.removeEventListener("ioError",onLoaderIoError);
            _loader.removeEventListener("progress",onLoaderProgress);
            _loader.removeEventListener("securityError",onLoaderSecurityError);
            try
            {
               _loader.close();
            }
            catch(err:Error)
            {
            }
            _loader = null;
         }
         if(timeOutTimer)
         {
            timeOutTimer.reset();
            timeOutTimer.removeEventListener("timerComplete",onTimeOutComplete);
            timeOutTimer = null;
         }
         if(_parser)
         {
            _parser.removeEventListener("meta",onParserPacket);
            _parser.removeEventListener("audio",onParserPacket);
            _parser.removeEventListener("video",onParserPacket);
            _parser.removeEventListener("malformedData",onParserError);
            _parser = null;
         }
         if(retryTimer)
         {
            retryTimer.removeEventListener("timerComplete",loadChunk);
            retryTimer.reset();
            retryTimer = null;
         }
         _tagQueue = null;
         return super.destroy();
      }
      
      private function onTimeOutComplete(param1:TimerEvent) : void {
         if(!currentTagCount)
         {
            echo("onTimeOutComplete ( threshold: 3000 msec )",2);
            _parser.clearCache();
            loadChunk(param1);
            dispatch("timeout");
         }
      }
      
      private function onParserPacket(param1:DynamicEvent) : void {
         var _loc3_:* = 0;
         var _loc4_:* = null;
         var _loc2_:* = null;
         var _loc5_:FlvTag = param1.data as FlvTag;
         if(_loc5_)
         {
            if(_loc5_.type == 8 || _loc5_.type == 9)
            {
               if(clearPrecedingDeltas && !firstKeyFound && !_loc5_.isKeyFrame)
               {
                  echo("!!!! dropping flvTag, not keyframe");
               }
               else
               {
                  firstKeyFound = true;
                  if(!chunkFirstTimeStamp)
                  {
                     chunkFirstTimeStamp = _loc5_.timeStamp;
                  }
                  else
                  {
                     chunkRollingTimeStamp = _loc5_.timeStamp;
                  }
                  _tagQueue.push(_loc5_);
                  currentTagCount = currentTagCount + 1;
                  dispatch("progress",false,null,true);
               }
            }
            else if(_loc5_.type == 18)
            {
               _loc3_ = _loc5_.position;
               _loc5_.position = 11;
               if(_loc5_.bytesAvailable)
               {
                  _loc4_ = _loc5_.readObject();
               }
               try
               {
                  _loc2_ = _loc5_.readObject();
               }
               catch(e:Error)
               {
                  echo("Chunkloader.onParserPacket cathced: " + e);
               }
               _loc5_.position = _loc3_;
               if(_loc4_ == "onChunkInfo")
               {
                  dispatch("chunkInfo",false,
                     {
                        "data":_loc2_,
                        "size":_loc5_.length,
                        "index":_currentId
                     });
               }
               else
               {
                  _tagQueue.push(_loc5_);
                  currentTagCount = currentTagCount + 1;
                  dispatch("progress",false,null,true);
               }
            }
            else
            {
               echo("unknown tag type: " + _loc5_.type);
            }
            
         }
         else
         {
            echo("onParserPacket : data mismatch");
         }
      }
      
      private function onParserError(param1:Event) : void {
         _parser.clearCache();
         if(loading)
         {
            delayedParserError = true;
         }
         else
         {
            dispatch("parserError",false,{"url":_loaderUrl});
         }
      }
      
      public function getNextTag() : FlvTag {
         var _loc1_:FlvTag = null;
         if(_tagQueue && _tagQueue.length)
         {
            _loc1_ = _tagQueue[0];
            _tagQueue.splice(0,1);
         }
         if(!_loading && _loader && _currentId <= _targetId && timeAvailableInQueue < maxTimeInQueue)
         {
            loadChunk();
         }
         return _loc1_;
      }
      
      private function checkFlvHeader(param1:ByteArray) : Boolean {
         if(param1.length < 3)
         {
            return false;
         }
         var _loc2_:uint = param1.position;
         param1.position = 0;
         var _loc3_:String = param1.readUTFBytes(3);
         param1.position = _loc2_;
         return _loc3_ == "FLV";
      }
      
      public function get timeAvailableInQueue() : uint {
         var _loc2_:* = 0;
         var _loc1_:* = 0;
         var _loc3_:uint = 0;
         if(_tagQueue && _tagQueue.length > 1)
         {
            _loc2_ = _tagQueue[0].timeStamp;
            _loc1_ = _tagQueue[_tagQueue.length - 1].timeStamp;
            _loc3_ = _loc1_ - _loc2_;
         }
         return _loc3_;
      }
      
      public function get ioErrorCount() : uint {
         return _ioErrorCount;
      }
      
      private var currentChunkSize:uint = 0;
      
      private var currentTagCount:uint = 0;
      
      private var chunkLoadStart:uint = 0;
      
      private var chunkFirstTimeStamp:uint = 0;
      
      private var chunkRollingTimeStamp:uint = 0;
      
      private var delayedParserError:Boolean = false;
      
      private function onLoaderComplete(param1:Event) : void {
         _ioErrorCount = 0;
         _byteOffset = 0;
         _currentBandwidth = currentChunkSize / (getTimer() - chunkLoadStart) * 1000;
         _bandwidthList.push(_currentBandwidth);
         while(_bandwidthList.length > 5)
         {
            _bandwidthList.splice(0,1);
         }
         _maximumBandwidth = Math.max(_currentBandwidth,_maximumBandwidth);
         _currentId = _currentId + 1;
         _loading = false;
         dispatch("complete",false,
            {
               "url":_loaderUrl,
               "time":getTimer() - chunkLoadStart,
               "size":currentChunkSize
            });
         echo("onLoaderComplete (timeAvailableInQueue : " + timeAvailableInQueue + ", tagCount : " + currentTagCount + ", currentChunkSize : " + currentChunkSize + ", contains : " + (chunkRollingTimeStamp - chunkFirstTimeStamp) + ")");
         if(_loader && _currentId <= _targetId && timeAvailableInQueue < maxTimeInQueue)
         {
            loadChunk();
         }
      }
      
      private function onLoaderProgress(param1:ProgressEvent) : void {
         var _loc2_:ByteArray = new ByteArray();
         if(_loader.bytesAvailable)
         {
            if(!currentChunkSize)
            {
               dispatch("start",false,{"url":_loaderUrl});
               if(getTimer() - chunkLoadStart > 500)
               {
                  dispatch("slowStart");
               }
            }
            currentChunkSize = currentChunkSize + _loader.bytesAvailable;
            _loader.readBytes(_loc2_,0,_loader.bytesAvailable);
            _parser.feed(_loc2_);
         }
      }
      
      private function onLoaderHttpStatus(param1:HTTPStatusEvent) : void {
         echo("onLoaderHttpStatus " + param1.status);
         if(param1.status == 200 && (delayedParserError))
         {
            dispatch("parserError",false,{"url":_loaderUrl});
            delayedParserError = false;
         }
         else if(param1.status == 0)
         {
            onLoaderIoError(null);
         }
         
      }
      
      private function onLoaderIoError(param1:IOErrorEvent) : void {
         echo("onLoaderIoError " + param1);
         _ioErrorCount = _ioErrorCount + 1;
         _parser.clearCache();
         retryTimer.reset();
         retryTimer.start();
         dispatch("ioError",false,
            {
               "url":_loaderUrl,
               "fatal":param1 == null
            });
         timeOutTimer.reset();
      }
      
      private function onLoaderSecurityError(param1:SecurityErrorEvent) : void {
         echo("onLoaderSecurityError");
         dispatch("securityError",false,{"url":_loaderUrl});
         timeOutTimer.reset();
      }
      
      private function loadChunk(param1:TimerEvent = null) : void {
         var _loc4_:* = 0;
         if(param1)
         {
            retryCount = retryCount + 1;
         }
         else
         {
            retryCount = 0;
         }
         var _loc7_:String = getHash(_currentId);
         var _loc5_:Array = _pattern.split("%");
         var _loc2_:Array = [_currentId,_loc7_];
         var _loc8_:String = "";
         _loc4_ = 0;
         while(_loc4_ < _loc5_.length - 1)
         {
            _loc8_ = _loc8_ + _loc5_[_loc4_];
            if(_loc2_[_loc4_] != undefined)
            {
               _loc8_ = _loc8_ + _loc2_[_loc4_];
            }
            _loc4_++;
         }
         _loc8_ = _loc8_ + _loc5_[_loc4_];
         var _loc3_:URLRequest = new URLRequest(_loc8_);
         var _loc6_:Array = [];
         if(_byteOffset)
         {
            _loc6_.push("start=" + _byteOffset);
         }
         if(_loc6_.length)
         {
            _loc3_.url = _loc3_.url + ("?" + _loc6_.join("&"));
         }
         echo("loading " + _loc3_.url);
         try
         {
            _loader.close();
         }
         catch(err:Error)
         {
         }
         currentChunkSize = 0;
         currentTagCount = 0;
         chunkLoadStart = getTimer();
         chunkFirstTimeStamp = 0;
         chunkRollingTimeStamp = 0;
         _loading = true;
         _loader.load(_loc3_);
         _loaderUrl = _loc3_.url;
         if(!timeOutTimer)
         {
            timeOutTimer = new Timer(3000,1);
            timeOutTimer.addEventListener("timerComplete",onTimeOutComplete);
         }
         timeOutTimer.reset();
         timeOutTimer.start();
         dispatch("init",false,{"url":_loaderUrl});
      }
      
      private function getHash(param1:uint) : String {
         var _loc5_:* = null;
         var _loc3_:* = 0;
         var _loc2_:Array = [];
         var _loc7_:* = 0;
         var _loc6_:* = _hashRange;
         for(_loc4_ in _hashRange)
         {
            _loc2_.push(_loc4_);
         }
         if(_loc2_.length)
         {
            _loc2_.sort(16);
            _loc5_ = _hashRange[_loc2_[0]];
            _loc3_ = 1;
            while(_loc3_ < _loc2_.length)
            {
               if(_loc2_[_loc3_] <= param1)
               {
                  _loc5_ = _hashRange[_loc2_[_loc3_]];
                  _loc3_++;
                  continue;
               }
               break;
            }
            return _loc5_;
         }
         return "";
      }
   }
}
