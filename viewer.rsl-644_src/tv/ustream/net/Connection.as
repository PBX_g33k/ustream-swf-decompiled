package tv.ustream.net
{
   import flash.net.NetConnection;
   import tv.ustream.net.interfaces.ICommunicator;
   import flash.utils.Timer;
   import flash.events.NetStatusEvent;
   import tv.ustream.tools.Debug;
   import flash.events.Event;
   import flash.events.IOErrorEvent;
   import tv.ustream.tools.DynamicEvent;
   import flash.net.Responder;
   
   public dynamic class Connection extends NetConnection implements ICommunicator
   {
      
      public function Connection(param1:String = null, param2:Number = 0) {
         super();
         _encoding = param2;
         addEventListener("netStatus",onNetStatus);
         addEventListener("ioError",onIoError);
         _label = param1;
      }
      
      public static const CONNECTED:String = "connected";
      
      public static const DISCONNECTED:String = "disconnected";
      
      public static const FAILED:String = "failed";
      
      public static const REJECTED:String = "rejected";
      
      public static const CALL_FAILED:String = "callFailed";
      
      public static const IDLE_TIMEOUT:String = "idleTimeout";
      
      protected var _label:String = "unknown";
      
      private var pingTimeOut:Timer;
      
      private var pingTimeOutCounter:int;
      
      public var muted:Boolean = false;
      
      protected var _url:String;
      
      protected var _ports:Array;
      
      protected var _encoding:uint = 0;
      
      override public function connect(param1:String, ... rest) : void {
         if(!connected)
         {
            objectEncoding = _encoding;
         }
         if(!_url)
         {
            _url = param1;
         }
         if(!_label || _label.indexOf("rtmp://") == 0)
         {
            _label = param1;
         }
         rest.unshift(param1);
         super.connect.apply(this,rest);
      }
      
      protected function onNetStatus(param1:NetStatusEvent) : void {
         if(!muted)
         {
            Debug.echo("onNetStatus ==> " + _label);
            Debug.explore(param1.info);
         }
         var _loc2_:* = param1.info.code;
         if("NetConnection.Connect.Success" !== _loc2_)
         {
            if("NetConnection.Connect.Closed" !== _loc2_)
            {
               if("NetConnection.Connect.Rejected" !== _loc2_)
               {
                  if("NetConnection.Connect.Failed" !== _loc2_)
                  {
                     if("NetConnection.Call.Failed" !== _loc2_)
                     {
                        if("NetConnection.Connect.IdleTimeOut" === _loc2_)
                        {
                           dispatch(new Event("idleTimeout"));
                        }
                     }
                     else
                     {
                        dispatch(new Event("callFailed"));
                     }
                  }
                  else
                  {
                     onFailed(param1);
                  }
               }
               else
               {
                  onRejected(param1);
               }
            }
            else
            {
               onClosed(param1);
            }
         }
         else
         {
            onSuccess(param1);
         }
      }
      
      private function onIoError(param1:IOErrorEvent) : void {
         dispatch("ioError");
      }
      
      protected function onSuccess(param1:NetStatusEvent) : void {
         dispatch(new Event("connected"));
      }
      
      protected function onFailed(param1:NetStatusEvent) : void {
         dispatch(new Event("failed"));
      }
      
      protected function onClosed(param1:NetStatusEvent) : void {
         if(pingTimeOut)
         {
            pingTimeOut.stop();
         }
         dispatch(new Event("disconnected"));
      }
      
      protected function onRejected(param1:NetStatusEvent) : void {
         var _loc2_:DynamicEvent = new DynamicEvent("rejected");
         if(param1.info.application)
         {
            _loc2_.reason = (param1.info.application is String?param1.info.application:param1.info.application.reason) || param1.info.application.err || param1.info.application.msg;
            _loc2_.info = param1.info.application;
            _loc3_ = _loc2_.reason;
            if("balance" !== _loc3_)
            {
               if("already online" !== _loc3_)
               {
                  if("already online fme" !== _loc3_)
                  {
                     if("already online mobile" !== _loc3_)
                     {
                        if("overflow" !== _loc3_)
                        {
                           if("Bannolva volt" !== _loc3_)
                           {
                              if("invalid password" !== _loc3_)
                              {
                                 if("NetStream.Play.StreamNotFound" !== _loc3_)
                                 {
                                    if("logicOff" !== _loc3_)
                                    {
                                       if("offline" !== _loc3_)
                                       {
                                          if("limit reached." !== _loc3_)
                                          {
                                             if("embed limit reached." !== _loc3_)
                                             {
                                                dispatch(_loc2_);
                                             }
                                             else
                                             {
                                                dispatch(new Event("embedLimitReached"));
                                             }
                                          }
                                          else
                                          {
                                             dispatch(new Event("viewerLimitReached"));
                                          }
                                       }
                                       else
                                       {
                                          dispatch(new Event("offline"));
                                       }
                                    }
                                    else
                                    {
                                       dispatch(new DynamicEvent("logicOff",false,false,
                                          {
                                             "cdn":param1.info.application.cdn,
                                             "delay":param1.info.application.delay
                                          }));
                                    }
                                 }
                                 else
                                 {
                                    dispatch(new Event("streamNotFound"));
                                 }
                              }
                              else
                              {
                                 dispatch(new Event("password"));
                              }
                           }
                           else
                           {
                              dispatch(new Event("banned"));
                           }
                        }
                        else
                        {
                           dispatch(new Event("overflow"));
                        }
                     }
                     else
                     {
                        dispatch(new Event("alreadyOnlineMobile"));
                     }
                  }
                  else
                  {
                     dispatch(new Event("alreadyOnlineFME"));
                  }
               }
               else
               {
                  dispatch(new Event("alreadyOnline"));
               }
            }
            else
            {
               dispatch(new DynamicEvent("balance",false,false,{"logicUrl":param1.info.application.logicUrl}));
            }
         }
         else
         {
            dispatch(_loc2_);
         }
      }
      
      public function ping(... rest) : void {
         if(!pingTimeOut)
         {
            pingTimeOut = new Timer(0);
            pingTimeOut.addEventListener("timer",onPingTimeOut);
         }
         pingTimeOut.stop();
         pingTimeOutCounter = 0;
         call("pong",new Responder(pongResult,pongStatus));
      }
      
      private function pongResult(param1:Object) : void {
         if(param1.delay)
         {
            pingTimeOut.delay = param1.delay;
            pingTimeOut.reset();
            pingTimeOut.start();
         }
      }
      
      private function onPingTimeOut(... rest) : void {
         pingTimeOutCounter = pingTimeOutCounter + 1;
         dispatch(new DynamicEvent("pingTimeOut",false,false,
            {
               "delay":pingTimeOut.delay,
               "counter":pingTimeOutCounter
            }));
         Debug.echo("PingTimeOut. delay : " + pingTimeOut.delay + " counter : " + pingTimeOutCounter);
         if(pingTimeOutCounter == 3)
         {
            pingTimeOut.stop();
            super.close();
         }
      }
      
      private function pongStatus(param1:Object) : void {
         trace("pongStatus @ " + _label + " : " + param1);
         Debug.explore(param1);
      }
      
      public function gwCall(param1:String, param2:Responder, ... rest) : void {
         rest.unshift(param1,param2);
         call.apply(this,rest);
      }
      
      private function dispatch(... rest) : void {
         if(rest[0] && !muted)
         {
            dispatchEvent(rest[0]);
         }
      }
      
      public function kill() : void {
         muted = true;
         if(pingTimeOut)
         {
            pingTimeOut.stop();
            pingTimeOut.removeEventListener("timer",onPingTimeOut);
            pingTimeOut = null;
         }
         super.close();
      }
      
      public function get label() : String {
         return _label;
      }
      
      public function set label(param1:String) : void {
         _label = param1;
      }
      
      public function get ports() : Array {
         return _ports;
      }
      
      public function set ports(param1:Array) : void {
         _ports = param1;
      }
      
      public function get url() : String {
         return _url;
      }
   }
}
