package tv.ustream.net.interfaces
{
   import flash.events.IEventDispatcher;
   import flash.net.Responder;
   
   public interface ICommunicator extends IEventDispatcher
   {
      
      function call(param1:String, param2:Responder, ... rest) : void;
      
      function close() : void;
      
      function connect(param1:String, ... rest) : void;
      
      function kill() : void;
      
      function get connected() : Boolean;
      
      function get client() : Object;
      
      function set client(param1:Object) : void;
      
      function get url() : String;
      
      function get ports() : Array;
      
      function set ports(param1:Array) : void;
      
      function get uri() : String;
   }
}
