package tv.ustream.data
{
   import flash.display.Sprite;
   import flash.geom.Rectangle;
   import tv.ustream.tools.DynamicEvent;
   
   public class DataListRenderer extends DataRenderer
   {
      
      public function DataListRenderer(param1:Array = null) {
         super(param1);
         children.scrollRect = new Rectangle(0,0,_width,_height);
         addEventListener("sort",onSort);
      }
      
      private static var xy:Object = 
         {
            "horizontal":"x",
            "vertical":"y"
         };
      
      private static var wh:Object = 
         {
            "horizontal":"width",
            "vertical":"height"
         };
      
      private var _width:Number = Infinity;
      
      private var _height:Number = Infinity;
      
      private var _direction:String = "vertical";
      
      private var _itemWidth:Number;
      
      private var _itemHeight:Number;
      
      protected var _margin:Number = 0;
      
      protected var _percent:Number = 0;
      
      protected var needUpdate:Boolean = true;
      
      public var autoUpdate:Boolean = true;
      
      public var itemScroll:Boolean = false;
      
      public function refresh() : void {
         var _loc4_:* = NaN;
         var _loc2_:* = null;
         var _loc3_:Number = source.length * (itemHeight + margin) - margin;
         var _loc1_:Number = Math.min(height / _loc3_,1);
         var _loc5_:Number = Math.max(_loc3_ - height,0) * _percent;
         var _loc6_:* = 0;
         _loc4_ = 0.0;
         while(_loc4_ < source.length)
         {
            source[_loc4_].index = _loc4_;
            _loc2_ = source[_loc4_].instance as Sprite;
            if(_loc2_)
            {
               _loc2_[xy[_direction]] = _loc6_ + (source[_loc4_].rightPadding?source[_loc4_].rightPadding:0);
               if(_loc2_.visible)
               {
                  _loc6_ = _loc6_ + (_loc2_[wh[direction]] + _margin + (source[_loc4_].rightPadding?source[_loc4_].rightPadding:0) + (source[_loc4_].leftPadding?source[_loc4_].leftPadding:0));
               }
            }
            _loc4_++;
         }
         children.scrollRect = new Rectangle(0,_loc5_,_width,_height);
         dispatchEvent(new DynamicEvent("scrollUpdate",true,true,
            {
               "yPercent":0,
               "grapYpercent":_loc1_
            }));
      }
      
      public function get direction() : String {
         return _direction;
      }
      
      public function set direction(param1:String) : void {
         _direction = param1 == "vertical"?"vertical":"horizontal";
         refresh();
      }
      
      private function onSort(param1:DynamicEvent) : void {
         if(autoUpdate)
         {
            refresh();
         }
      }
      
      override protected function createInstance(param1:Object, param2:Number = -1) : Object {
         var _loc3_:Object = super.createInstance(param1,param2);
         if(autoUpdate)
         {
            refresh();
         }
         return _loc3_;
      }
      
      override protected function destroyInstance(param1:Object) : Object {
         var _loc2_:Object = super.destroyInstance(param1);
         if(autoUpdate)
         {
            refresh();
         }
         return _loc2_;
      }
      
      public function get margin() : Number {
         return _margin;
      }
      
      public function set margin(param1:Number) : void {
         _margin = param1;
         refresh();
      }
      
      public function get percent() : Number {
         return _percent;
      }
      
      public function set percent(param1:Number) : void {
         _percent = param1;
         refresh();
      }
      
      public function get width() : Number {
         return _width;
      }
      
      public function set width(param1:Number) : void {
         if(param1 != _width)
         {
            _width = param1;
            refresh();
         }
      }
      
      public function get height() : Number {
         return _height;
      }
      
      public function set height(param1:Number) : void {
         if(param1 != _height)
         {
            _height = param1;
            refresh();
         }
      }
      
      public function get itemWidth() : Number {
         return _itemWidth || (children.numChildren?children.getChildAt(0).width:100.0);
      }
      
      public function set itemWidth(param1:Number) : void {
         _itemWidth = param1;
      }
      
      public function get itemHeight() : Number {
         return _itemHeight || (children.numChildren?children.getChildAt(0).height:100.0);
      }
      
      public function set itemHeight(param1:Number) : void {
         _itemHeight = param1;
      }
   }
}
