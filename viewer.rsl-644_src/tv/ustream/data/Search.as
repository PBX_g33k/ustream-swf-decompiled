package tv.ustream.data
{
   public dynamic class Search extends Array
   {
      
      public function Search(param1:Array, param2:String, param3:*, param4:Boolean = false, param5:Boolean = false) {
         super();
         this.property = param2;
         this.value = param3;
         this.hasProperty = param4;
         this.hasNoProperty = param5;
         result = param1.filter(find);
         result.unshift(0,0);
         splice.apply(this,result);
      }
      
      private var property:String;
      
      private var value;
      
      private var result:Array;
      
      private var hasProperty:Boolean;
      
      private var hasNoProperty:Boolean;
      
      protected function find(param1:*, param2:int, param3:Array) : Boolean {
         if(!hasProperty && !hasNoProperty)
         {
            return param1[property] == value;
         }
         if(hasProperty)
         {
            if(value == null)
            {
               return !(param1[property] == null);
            }
            return param1[property] == value;
         }
         if(hasNoProperty)
         {
            if(value == null)
            {
               return param1[property] == null;
            }
            return !(param1[property] == value);
         }
         return false;
      }
   }
}
