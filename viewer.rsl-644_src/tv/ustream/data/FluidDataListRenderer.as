package tv.ustream.data
{
   public class FluidDataListRenderer extends DataListRenderer
   {
      
      public function FluidDataListRenderer() {
         super();
      }
      
      override public function refresh() : void {
         var _loc10_:* = NaN;
         var _loc8_:Array = [];
         var _loc5_:Array = [];
         var _loc11_:Array = [];
         var _loc2_:Number = width;
         var _loc4_:* = 0;
         var _loc9_:* = 0;
         var _loc3_:* = 0.0;
         var _loc15_:* = 0;
         var _loc14_:* = data;
         for each(_loc12_ in data)
         {
            if(_loc12_.instance.visible == false)
            {
               _loc9_++;
            }
            else
            {
               _loc10_ = _loc12_.width?_loc12_.width:_loc12_.instance.width;
               _loc2_ = _loc2_ - _loc10_;
               _loc4_++;
               if(_loc12_.weight != undefined)
               {
                  _loc5_.push(_loc12_);
                  _loc3_ = _loc3_ + _loc12_.weight;
               }
               else if(_loc12_.instance.width != _loc10_)
               {
                  _loc12_.instance.width = _loc10_;
               }
               
            }
         }
         _loc2_ = _loc2_ - margin * _loc4_;
         _loc2_ = _loc2_ < 0?0:_loc2_;
         var _loc13_:* = _loc2_;
         var _loc6_:* = false;
         var _loc7_:Number = _loc5_.length;
         var _loc17_:* = 0;
         var _loc16_:* = _loc5_;
         for each(_loc1_ in _loc5_)
         {
            if(_loc1_.instance.visible)
            {
               _loc1_.instance.width = _loc1_.width + _loc2_ / _loc7_;
            }
         }
         super.refresh();
      }
   }
}
