package tv.ustream.data
{
   import flash.events.EventDispatcher;
   import tv.ustream.tools.DynamicEvent;
   
   public class DataProvider extends EventDispatcher
   {
      
      public function DataProvider(param1:Array = null) {
         super();
         this.source = param1 || [];
      }
      
      public static const ADD_ITEM:String = "addItem";
      
      public static const UPDATE_ITEM:String = "updateItem";
      
      public static const REMOVE_ITEM:String = "removeItem";
      
      public var source:Array;
      
      public function get data() : Array {
         return source;
      }
      
      public function clear() : void {
         while(source.length)
         {
            removeItemAt(0);
         }
      }
      
      public function addItem(param1:Object) : Object {
         source.push(param1);
         this.dispatchEvent(new DynamicEvent("addItem",true,true,
            {
               "item":param1,
               "index":source.length - 1
            }));
         return param1;
      }
      
      public function addItemAt(param1:Object, param2:Number) : Object {
         source.splice(param2,0,param1);
         dispatchEvent(new DynamicEvent("addItem",false,true,
            {
               "item":param1,
               "index":param2
            }));
         return param1;
      }
      
      public function addItemAtProperty(param1:String, param2:*, param3:Object) : Object {
         return addItemAt(param3,getItemIndex(getItemAtProperty(param1,param2)[0]));
      }
      
      public function getItemAt(param1:Number) : Object {
         return source[param1];
      }
      
      public function getItemIndex(param1:Object) : Number {
         return source.indexOf(param1);
      }
      
      public function getItemAtProperty(param1:String, param2:*, param3:Boolean = false) : Array {
         var _loc4_:Array = new Search(source,param1,param2);
         return _loc4_.slice(0,param3?_loc4_.length:1);
      }
      
      public function getItemAtHasProperty(param1:String, param2:Boolean = false) : Array {
         var _loc3_:Array = new Search(source,param1,null,true);
         return _loc3_.slice(0,param2?_loc3_.length:1);
      }
      
      public function getItemAtHasNoProperty(param1:String, param2:Boolean = false) : Array {
         var _loc3_:Array = new Search(source,param1,null,false);
         return _loc3_.slice(0,param2?_loc3_.length:1);
      }
      
      public function updateItem(param1:Object, param2:Object) : Object {
         return updateItemAt(getItemIndex(param1),param2);
      }
      
      public function updateItemAt(param1:Number, param2:Object) : Object {
         var _loc5_:* = 0;
         var _loc4_:* = param2;
         for(_loc3_ in param2)
         {
            source[param1][_loc3_] = param2[_loc3_];
         }
         dispatchEvent(new DynamicEvent("updateItem",false,true,
            {
               "item":source[param1],
               "changes":param2
            }));
         return source[param1];
      }
      
      public function updateItemAtProperty(param1:String, param2:*, param3:Object, param4:Boolean = false) : Array {
         var _loc5_:Array = getItemAtProperty(param1,param2,param4);
         var _loc8_:* = 0;
         var _loc7_:* = _loc5_;
         for each(_loc6_ in _loc5_)
         {
            updateItem(_loc6_,param3);
         }
         return _loc5_;
      }
      
      public function removeItem(param1:Object) : Object {
         return removeItemAt(getItemIndex(param1));
      }
      
      public function removeItemAt(param1:Number) : Object {
         var _loc2_:Object = source.splice(param1,1)[0];
         dispatchEvent(new DynamicEvent("removeItem",false,true,
            {
               "item":_loc2_,
               "items":_loc2_,
               "index":param1
            }));
         return _loc2_;
      }
      
      public function removeItemAtProperty(param1:String, param2:*, param3:Boolean = false) : Array {
         var _loc4_:Array = getItemAtProperty(param1,param2,param3);
         var _loc7_:* = 0;
         var _loc6_:* = _loc4_;
         for each(_loc5_ in _loc4_)
         {
            removeItem(_loc5_);
         }
         return _loc4_;
      }
      
      public function sortOn(param1:Object, param2:Object = null) : void {
         source.sortOn(param1,param2);
         dispatchEvent(new DynamicEvent("sort"));
      }
      
      public function swap(param1:Number, param2:Number) : void {
         var _loc3_:Object = source[param1];
         var _loc4_:Object = source[param2];
         source[param1] = _loc4_;
         source[param2] = _loc3_;
      }
      
      public function setItemIndex(param1:Object, param2:int, param3:String = null) : void {
         var _loc4_:* = NaN;
         var _loc5_:Number = getItemIndex(param1);
         if(!(_loc5_ == -1) && !(_loc5_ == param2))
         {
            source.splice(_loc5_,1);
            source.splice(param2,0,param1);
            if(param3)
            {
               _loc4_ = 0.0;
               while(_loc4_ < source.length)
               {
                  source[_loc4_][param3] = _loc4_;
                  _loc4_++;
               }
            }
            dispatchEvent(new DynamicEvent("sort",false,true,
               {
                  "item":param1,
                  "index":param2
               }));
         }
      }
      
      public function compare(param1:Array, param2:String, param3:Boolean = true) : void {
         var _loc6_:* = undefined;
         var _loc4_:* = null;
         var _loc5_:* = 0;
         if(param3)
         {
            _loc5_ = 0;
            while(_loc5_ < param1.length)
            {
               _loc6_ = param1[_loc5_][param2];
               if(_loc6_ != undefined)
               {
                  if(param1[_loc5_].remove)
                  {
                     removeItemAtProperty(param2,_loc6_,true);
                     _loc5_--;
                     param1.splice(_loc5_,1);
                  }
                  else if(param1[_loc5_].index != undefined)
                  {
                     setItemIndex(getItemAtProperty(param2,param1[_loc5_][param2])[0],param1[_loc5_].index);
                  }
                  
               }
               _loc5_++;
            }
         }
         _loc5_ = 0;
         while(_loc5_ < source.length)
         {
            _loc6_ = source[_loc5_][param2];
            if(_loc6_ != undefined)
            {
               _loc4_ = new Search(param1,param2,_loc6_);
               if(new Search(param1,param2,_loc6_).length)
               {
                  updateItemAt(_loc5_,_loc4_[0]);
               }
               else if(!param3)
               {
                  _loc5_--;
                  removeItemAt(_loc5_);
               }
               
            }
            _loc5_++;
         }
         _loc5_ = 0;
         while(_loc5_ < param1.length)
         {
            _loc6_ = param1[_loc5_][param2];
            if(!(_loc6_ == undefined) && !new Search(source,param2,_loc6_).length)
            {
               addItem(param1[_loc5_]);
            }
            _loc5_++;
         }
      }
   }
}
