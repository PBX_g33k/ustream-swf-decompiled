package tv.ustream.data
{
   import flash.display.Sprite;
   
   public class DataRenderer extends DataProvider
   {
      
      public function DataRenderer(param1:Array = null) {
         _children = new Sprite();
         super(param1);
      }
      
      private var _itemRenderer:Object;
      
      private var _children:Sprite;
      
      public function get children() : Sprite {
         return _children;
      }
      
      public function set itemRenderer(param1:Object) : void {
         _itemRenderer = param1;
         var _loc4_:* = 0;
         var _loc3_:* = source;
         for each(_loc2_ in source)
         {
            if(!_loc2_.renderer)
            {
               destroyInstance(_loc2_);
               createInstance(_loc2_);
            }
         }
      }
      
      public function get itemRenderer() : Object {
         return _itemRenderer;
      }
      
      protected function createInstance(param1:Object, param2:Number = -1) : Object {
         var _loc3_:Object = param1.renderer || _itemRenderer;
         if(_loc3_)
         {
            param1.instance = new _loc3_();
            if(param2 == -1)
            {
               _children.addChild(param1.instance);
            }
            else
            {
               _children.addChildAt(param1.instance,param2);
            }
            updateInstance(param1);
         }
         return param1;
      }
      
      protected function destroyInstance(param1:Object) : Object {
         if(param1.instance)
         {
            try
            {
               param1.instance.destroy();
            }
            catch(e:Error)
            {
            }
            _children.removeChild(param1.instance);
            param1.instance = null;
         }
         return param1;
      }
      
      private function updateInstance(param1:Object, param2:Object = null) : Object {
         var _loc4_:* = null;
         if(param1.instance)
         {
            _loc4_ = param2 || param1;
            try
            {
               param1.instance.update(_loc4_);
            }
            catch(e:Error)
            {
               _loc7_ = 0;
               _loc6_ = _loc4_;
               for(_loc3_ in _loc4_)
               {
                  if(param1.instance.hasOwnProperty(_loc3_))
                  {
                     param1.instance[_loc3_] = _loc4_[_loc3_];
                  }
               }
            }
            if(param1.index != undefined)
            {
               _children.setChildIndex(param1.instance,param1.index);
            }
         }
         return param1;
      }
      
      override public function addItem(param1:Object) : Object {
         return createInstance(super.addItem(param1));
      }
      
      override public function addItemAt(param1:Object, param2:Number) : Object {
         return createInstance(super.addItemAt(param1,param2),param2);
      }
      
      override public function removeItemAt(param1:Number) : Object {
         var _loc2_:Object = super.removeItemAt(param1);
         destroyInstance(_loc2_);
         return _loc2_;
      }
      
      override public function updateItemAt(param1:Number, param2:Object) : Object {
         return updateInstance(super.updateItemAt(param1,param2),param2);
      }
   }
}
