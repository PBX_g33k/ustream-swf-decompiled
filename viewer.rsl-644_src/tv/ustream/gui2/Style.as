package tv.ustream.gui2
{
   import flash.events.EventDispatcher;
   import flash.utils.describeType;
   import flash.events.Event;
   
   public dynamic class Style extends EventDispatcher
   {
      
      public function Style(param1:Object = null) {
         setBool = {};
         super();
         if(param1)
         {
            this.config(param1);
         }
      }
      
      protected var _underline:Boolean;
      
      protected var _background:Boolean;
      
      protected var _backgroundColor:Number;
      
      protected var _backgroundColor2:Number;
      
      protected var _backgroundAlpha:Number;
      
      protected var _cornerRadius:Number;
      
      protected var _corner:Array;
      
      protected var _border:Number;
      
      protected var _borderColor:Number;
      
      protected var _borderAlpha:Number;
      
      protected var _bold:Boolean;
      
      protected var _color:Number;
      
      protected var _size:Number;
      
      protected var _font:String;
      
      protected var _tint:Number;
      
      protected var setBool:Object;
      
      public function clone() : Style {
         var _loc1_:* = null;
         var _loc2_:Object = {};
         var _loc3_:XML = describeType(this);
         var _loc5_:* = 0;
         var _loc4_:* = _loc3_.accessor;
         for each(_loc1_ in _loc3_.accessor)
         {
            if(_loc1_.@access == "readwrite" && _loc1_.@declaredBy == "tv.ustream.gui2::Style" && (!(_loc1_.@type == "Boolean") || (_loc1_.@type == "Boolean" && (getBool(_loc1_.@name)))))
            {
               _loc2_[_loc1_.@name] = this[_loc1_.@name];
            }
         }
         return new Style(_loc2_);
      }
      
      public function config(param1:Object) : Style {
         var _loc6_:* = 0;
         var _loc5_:* = param1;
         for(_loc2_ in param1)
         {
            try
            {
               this["_" + _loc2_] = param1[_loc2_];
               if(this["_" + _loc2_] is Boolean)
               {
                  setBool[_loc2_] = true;
               }
            }
            catch(e:*)
            {
               continue;
            }
         }
         dispatch();
         return this;
      }
      
      private function dispatch() : void {
         dispatchEvent(new Event("update"));
      }
      
      public function get background() : Boolean {
         return _background;
      }
      
      public function set background(param1:Boolean) : void {
         setBool["background"] = true;
         _background = param1;
         dispatch();
      }
      
      public function get backgroundColor() : Number {
         return _backgroundColor;
      }
      
      public function set backgroundColor(param1:Number) : void {
         if(_backgroundColor != param1)
         {
            _backgroundColor = param1;
            if(_background)
            {
               dispatch();
            }
         }
      }
      
      public function get backgroundColor2() : Number {
         return _backgroundColor2;
      }
      
      public function set backgroundColor2(param1:Number) : void {
         if(_backgroundColor2 != param1)
         {
            _backgroundColor2 = param1;
            if(_background)
            {
               dispatch();
            }
         }
      }
      
      public function get backgroundAlpha() : Number {
         return _backgroundAlpha;
      }
      
      public function set backgroundAlpha(param1:Number) : void {
         if(_backgroundAlpha != param1)
         {
            _backgroundAlpha = param1;
            if(_background)
            {
               dispatch();
            }
         }
      }
      
      public function get border() : Number {
         return _border;
      }
      
      public function set border(param1:Number) : void {
         if(_border != param1)
         {
            _border = param1;
            dispatch();
         }
      }
      
      public function get borderColor() : Number {
         return _borderColor;
      }
      
      public function set borderColor(param1:Number) : void {
         if(_borderColor != param1)
         {
            _borderColor = param1;
            if(!isNaN(_border))
            {
               dispatch();
            }
         }
      }
      
      public function get borderAlpha() : Number {
         return _borderAlpha;
      }
      
      public function set borderAlpha(param1:Number) : void {
         if(_borderAlpha != param1)
         {
            _borderAlpha = param1;
            if(!isNaN(_border))
            {
               dispatch();
            }
         }
      }
      
      public function get cornerRadius() : Number {
         return _cornerRadius;
      }
      
      public function set cornerRadius(param1:Number) : void {
         if(param1 != _cornerRadius)
         {
            _cornerRadius = param1;
            _corner = [_cornerRadius,_cornerRadius,_cornerRadius,_cornerRadius];
            dispatch();
         }
      }
      
      public function get corner() : Array {
         return _corner;
      }
      
      public function set corner(param1:Array) : void {
         _corner = param1;
         dispatch();
      }
      
      public function get bold() : Boolean {
         return _bold;
      }
      
      public function set bold(param1:Boolean) : void {
         setBool["bold"] = true;
         _bold = param1;
         dispatch();
      }
      
      public function get underline() : Boolean {
         return _underline;
      }
      
      public function set underline(param1:Boolean) : void {
         setBool["underline"] = true;
         _underline = param1;
         dispatch();
      }
      
      public function get color() : Number {
         return _color;
      }
      
      public function set color(param1:Number) : void {
         _color = param1;
         dispatch();
      }
      
      public function get size() : Number {
         return _size;
      }
      
      public function set size(param1:Number) : void {
         _size = param1;
         dispatch();
      }
      
      public function get font() : String {
         return _font;
      }
      
      public function set font(param1:String) : void {
         _font = param1;
         dispatch();
      }
      
      public function get tint() : Number {
         return _tint;
      }
      
      public function set tint(param1:Number) : void {
         _tint = param1;
         dispatch();
      }
      
      public function getBool(param1:String) : Boolean {
         return setBool[param1] == true;
      }
   }
}
