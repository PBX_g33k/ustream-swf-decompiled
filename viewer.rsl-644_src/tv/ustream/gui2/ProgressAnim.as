package tv.ustream.gui2
{
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.display.Sprite;
   
   public class ProgressAnim extends MovieClip
   {
      
      public function ProgressAnim() {
         super();
         create();
         mouseChildren = false;
         mouseEnabled = false;
         addEventListener("enterFrame",onEnterFrame);
      }
      
      private var _count:uint = 21;
      
      private var _radius:uint = 12;
      
      private var _color:uint = 16777215;
      
      private var _gap:Number = 1.5;
      
      private var spotList:Array;
      
      public var step:Number = 0.33;
      
      private function onEnterFrame(param1:Event) : void {
         var _loc3_:* = 0;
         var _loc2_:* = null;
         _loc3_ = 0;
         while(_loc3_ < spotList.length)
         {
            _loc2_ = spotList[_loc3_];
            _loc2_.alpha = (1 + (_loc2_.alpha - 1 / _count * step) % 1) % 1;
            _loc3_++;
         }
      }
      
      private function create(... rest) : void {
         var _loc4_:* = 0;
         var _loc3_:* = null;
         var _loc2_:uint = Math.ceil(_radius * 2 * 3.141592653589793 - _count * _gap) / _count / 2;
         if(spotList && spotList.length)
         {
            _loc4_ = 0;
            while(_loc4_ < spotList.length)
            {
               removeChild(spotList[_loc4_]);
               _loc4_++;
            }
         }
         spotList = [];
         _loc4_ = 0;
         while(_loc4_ < _count)
         {
            _loc3_ = new Sprite();
            _loc3_.graphics.beginFill(_color,1);
            _loc3_.graphics.drawCircle(0,0,_loc2_);
            _loc3_.graphics.endFill();
            _loc3_.x = _radius * Math.cos(_loc4_ / _count * 2 * 3.141592653589793);
            _loc3_.y = _radius * Math.sin(_loc4_ / _count * 2 * 3.141592653589793);
            _loc3_.alpha = _loc4_ / _count;
            addChild(_loc3_);
            spotList.push(_loc3_);
            _loc4_++;
         }
      }
      
      public function get count() : uint {
         return _count;
      }
      
      public function set count(param1:uint) : void {
         _count = param1;
         create();
      }
      
      public function get radius() : uint {
         return _radius;
      }
      
      public function set radius(param1:uint) : void {
         _radius = param1;
         create();
      }
      
      public function get color() : uint {
         return _color;
      }
      
      public function set color(param1:uint) : void {
         _color = param1;
         create();
      }
      
      public function get gap() : Number {
         return _gap;
      }
      
      public function set gap(param1:Number) : void {
         _gap = param1;
         create();
      }
      
      override public function set visible(param1:Boolean) : void {
         .super.visible = param1;
         if(visible && !hasEventListener("enterFrame"))
         {
            addEventListener("enterFrame",onEnterFrame);
         }
         else if(!visible && (hasEventListener("enterFrame")))
         {
            removeEventListener("enterFrame",onEnterFrame);
         }
         
      }
   }
}
