package tv.ustream.gui2
{
   import flash.display.Sprite;
   import tv.ustream.data.DataProvider;
   import flash.display.Stage;
   import flash.display.DisplayObjectContainer;
   import tv.ustream.tools.Debug;
   import flash.filters.ColorMatrixFilter;
   import flash.filters.BlurFilter;
   import tv.ustream.tween.Tween;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.errors.IllegalOperationError;
   
   public class Alert extends Sprite
   {
      
      public function Alert() {
         super();
         if(!alertInstance)
         {
            addEventListener("addedToStage",onAddedToStage);
            ap = new AlertPanel();
            addChild(new AlertPanel());
            ap.okBtn.addEventListener("click",onClick);
            ap.cancelBtn.addEventListener("click",onCancel);
            alertInstance = this;
            return;
         }
         throw new IllegalOperationError("Use the \'instance\' property to access the singleton");
      }
      
      private static var alertInstance:Alert;
      
      private static var alertQueue:DataProvider = new DataProvider();
      
      private static var _percent:Number = 1;
      
      private static var _callback:Function;
      
      private static var _modalAlpha:Number = 0.5;
      
      private static var _modal:Sprite;
      
      private static var modalOriginalAlpha:Number = 1;
      
      private static var ap:AlertPanel;
      
      private static var blurAmount:Number = 4;
      
      private static var shownId:uint;
      
      private static var index:uint = 1;
      
      private static var temp:Boolean = false;
      
      private static var _stage:Stage;
      
      private static var _parent:DisplayObjectContainer;
      
      private static var _interaction:Boolean = true;
      
      private static var _customButtonLabels:Array;
      
      public static var okLabel:String = "OK";
      
      public static var cancelLabel:String = "Cancel";
      
      public static function get instance() : Alert {
         if(!alertInstance)
         {
            alertInstance = new Alert();
         }
         return alertInstance;
      }
      
      public static function get hasInstance() : Boolean {
         return alertInstance is Alert;
      }
      
      public static function get customButtonLabels() : Array {
         return _customButtonLabels || [];
      }
      
      public static function set customButtonLabels(param1:Array) : void {
         Debug.echo("ALERT.customBUttonLabels " + param1);
         if(param1)
         {
            _customButtonLabels = param1;
         }
      }
      
      public static function set interaction(param1:Boolean) : void {
         var _loc2_:* = null;
         _interaction = param1;
         if(_interaction)
         {
            Debug.echo("set interaction -- " + customButtonLabels);
            ap.updateLabels();
         }
         if(ap.okBtn)
         {
            ap.okBtn.visible = _interaction;
         }
         if(ap.cancelBtn)
         {
            _loc2_ = alertQueue.getItemAtProperty("id",shownId)[0];
            ap.cancelBtn.visible = param1 && (alertQueue && shownId && _loc2_ && !(_loc2_.ccb == null)?true:false);
         }
         if(_stage && alertInstance)
         {
            alertInstance.onStageResize();
         }
      }
      
      public static function set modalAlpha(param1:Number) : void {
         _modalAlpha = param1;
      }
      
      public static function set modal(param1:Sprite) : void {
         _modal = param1;
         modalOriginalAlpha = _modal.alpha;
      }
      
      public static function show(param1:String, param2:Function = null, param3:Function = null) : uint {
         index = index + 1;
         var _loc5_:uint = index;
         Debug.echo("Alert.show - msg: " + param1);
         var _loc4_:Object = 
            {
               "id":_loc5_,
               "cb":param2,
               "msg":param1
            };
         if(param3 != null)
         {
            _loc4_.ccb = param3;
         }
         alertQueue.addItem(_loc4_);
         if(!shownId)
         {
            showPanel(_loc5_);
         }
         if(_modal)
         {
            startTransition(true);
         }
         return _loc5_;
      }
      
      private static function showPanel(param1:uint) : void {
         shownId = param1;
         var _loc2_:Object = alertQueue.getItemAtProperty("id",param1)[0];
         if(_loc2_.ccb && _interaction)
         {
            ap.cancelBtn.visible = true;
         }
         ap.lab.label = _loc2_.msg;
         alertInstance.onStageResize();
         ap.visible = true;
      }
      
      public static function hide(param1:uint = 0, param2:Function = null) : void {
         if(param1 > 0)
         {
            alertQueue.removeItemAtProperty("id",param1);
            if(alertQueue.data.length > 0)
            {
               showPanel(alertQueue.getItemAt(0).id);
            }
         }
         else if(ap)
         {
            if(ap.visible && shownId)
            {
               alertQueue.removeItemAtProperty("id",shownId);
            }
            if(alertQueue.data.length > 0)
            {
               showPanel(alertQueue.getItemAt(0).id);
            }
            else
            {
               ap.visible = false;
               if(param2 != null)
               {
                  _callback = param2;
               }
               if(_modal)
               {
                  startTransition(false,_callback);
               }
               else if(_callback != null)
               {
                  _callback();
               }
               
               shownId = 0;
            }
         }
         
      }
      
      public static function clear() : void {
         var _loc1_:* = 0;
         Debug.echo("clear alerts:  ");
         if(alertQueue.data.length > 0)
         {
            Debug.explore(alertQueue.data);
         }
         _loc1_ = alertQueue.data.length;
         while(_loc1_ > 0)
         {
            hide();
            _loc1_--;
         }
      }
      
      public static function get percent() : Number {
         return _percent;
      }
      
      public static function set percent(param1:Number) : void {
         _percent = param1;
         var _loc3_:Number = 0.33 + 0.6699999999999999 * _percent;
         var _loc4_:Number = 0.33 - 0.33 * _percent;
         var _loc2_:Array = [_loc3_,_loc4_,_loc4_,0,0,_loc4_,_loc3_,_loc4_,0,0,_loc4_,_loc4_,_loc3_,0,0,_loc4_,_loc4_,_loc4_,1,0];
         _modal.filters = [new ColorMatrixFilter(_loc2_),new BlurFilter(blurAmount - blurAmount * _percent,blurAmount - blurAmount * _percent,2)];
         _modal.alpha = 0.5 + 0.5 * _percent;
         if(_percent == 1 && !(_callback == null))
         {
            _callback();
         }
      }
      
      private static function startTransition(param1:Boolean, param2:Function = null) : void {
         if(param1)
         {
            Tween.to(Alert,{"percent":0});
            _loc3_ = false;
            _modal.mouseChildren = _loc3_;
            _modal.mouseEnabled = _loc3_;
         }
         else
         {
            Tween.to(Alert,{"percent":1});
            _loc3_ = true;
            _modal.mouseChildren = _loc3_;
            _modal.mouseEnabled = _loc3_;
         }
      }
      
      private function onAddedToStage(param1:Event) : void {
         Debug.echo("[ ALERT ] onAddedToStage");
         removeEventListener("addedToStage",onAddedToStage);
         addEventListener("removedFromStage",onRemovedFromStage);
         _stage = stage;
         _parent = this.parent;
         if(stage)
         {
            stage.addEventListener("resize",onStageResize);
            stage.addEventListener("added",onStageAdded);
         }
         onStageResize();
         onStageAdded();
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         Debug.echo("[ ALERT ] onRemovedFromStage");
         removeEventListener("removedFromStage",onRemovedFromStage);
         addEventListener("addedToStage",onAddedToStage);
         if(_stage)
         {
            _stage.removeEventListener("resize",onStageResize);
            _stage.removeEventListener("added",onStageAdded);
         }
         _stage = null;
         _parent = null;
      }
      
      private function onStageAdded(param1:Event = null) : void {
         if(_parent && !(_parent.getChildIndex(this) == _parent.numChildren - 1) && !hasEventListener("enterFrame"))
         {
            addEventListener("enterFrame",onEnterFrame);
         }
      }
      
      private function onEnterFrame(param1:Event) : void {
         removeEventListener("enterFrame",onEnterFrame);
         this.parent.setChildIndex(this,this.parent.numChildren - 1);
      }
      
      private function onStageResize(param1:Event = null) : void {
         if(_parent)
         {
            _loc2_ = 0;
            ap.y = _loc2_;
            ap.x = _loc2_;
            ap.lab.maxWidth = Math.round(_parent.width * 0.8);
            ap.width = Math.round(ap.lab.width + 2 * ap.lab.x);
            if(ap.okBtn.visible)
            {
               ap.okBtn.y = Math.round(ap.lab.y + ap.lab.height + 10);
               if(ap.cancelBtn.visible)
               {
                  ap.okBtn.x = Math.round((ap.width - (ap.okBtn.width + 10 + ap.cancelBtn.width)) / 2);
                  ap.cancelBtn.x = Math.round(ap.okBtn.x + ap.okBtn.width + 10);
                  ap.cancelBtn.y = ap.okBtn.y;
               }
               else
               {
                  ap.okBtn.x = Math.round((ap.width - ap.okBtn.width) / 2);
               }
               ap.height = Math.round(ap.okBtn.y + ap.okBtn.height + 10);
            }
            else
            {
               ap.height = Math.round(ap.lab.y + ap.lab.height + 10);
            }
            ap.x = Math.round((_parent.width - ap.width) / 2);
            ap.y = Math.round((_parent.height - ap.height) / 2);
         }
      }
      
      private function onClick(param1:MouseEvent) : void {
         var _loc2_:Object = alertQueue.getItemAtProperty("id",shownId)[0];
         if(!_loc2_)
         {
            return;
         }
         if(_loc2_.cb != null)
         {
            _loc2_.cb();
         }
         alertQueue.removeItemAtProperty("id",shownId);
         shownId = 0;
      }
      
      private function onCancel(param1:MouseEvent) : void {
         var _loc2_:Object = alertQueue.getItemAtProperty("id",shownId)[0];
         if(!_loc2_)
         {
            return;
         }
         if(_loc2_.ccb != null)
         {
            _loc2_.ccb();
         }
         alertQueue.removeItemAtProperty("id",shownId);
         shownId = 0;
      }
      
      override public function set width(param1:Number) : void {
         onStageResize();
      }
      
      override public function set height(param1:Number) : void {
         onStageResize();
      }
   }
}
import tv.ustream.gui2.Gui;
import tv.ustream.gui2.Style;
import tv.ustream.gui2.Text;
import tv.ustream.gui2.Button;
import tv.ustream.gui2.Alert;
import tv.ustream.tools.Debug;

class AlertPanel extends Gui
{
   
   function AlertPanel() {
      visible = false;
      super();
      lab = new Text("");
      addChild(new Text(""));
      lab.autoSizeHorizontal = true;
      lab.autoSizeVertical = true;
      lab.wordWrap = true;
      lab.margin = 0;
      lab.minWidth = 100;
      lab.style = AlertPanel.style.clone();
      lab.style.background = false;
      lab.style.border = 0;
      lab.align = "center";
      var _loc1_:* = 10;
      lab.y = _loc1_;
      lab.x = _loc1_;
      okBtn = new Button();
      addChild(new Button());
      okBtn.label = Alert.okLabel;
      okBtn.align = "center";
      cancelBtn = new Button();
      addChild(new Button()).visible = false;
      cancelBtn.label = Alert.cancelLabel;
      cancelBtn.align = "center";
   }
   
   protected static var _style:Object = {};
   
   public static function get style() : Style {
      if(!Gui.style["alertpanel"])
      {
         Gui.style["alertpanel"] = new Style(_style);
      }
      return Gui.style.alertpanel;
   }
   
   public var lab:Text;
   
   public var okBtn:Button;
   
   public var cancelBtn:Button;
   
   public function updateLabels() : void {
      var _loc1_:Array = Alert.customButtonLabels;
      Debug.echo("Alert.ap.updateLabels() - cl: " + _loc1_);
      if(_loc1_.length)
      {
         okBtn.label = _loc1_[0];
         if(_loc1_[1])
         {
            cancelBtn.label = _loc1_[1];
         }
      }
      else
      {
         okBtn.label = Alert.okLabel;
         cancelBtn.label = Alert.cancelLabel;
      }
   }
   
   override protected function init() : void {
      super.init();
   }
}
