package tv.ustream.gui2
{
   import tv.ustream.tools.Container;
   import flash.events.Event;
   import flash.display.DisplayObject;
   
   public class ViewStack extends Container
   {
      
      public function ViewStack() {
         super();
         addEventListener("added",onAdded);
      }
      
      private static var selectedEvent:Event = new Event("selected",false,false);
      
      private var _defaultItem:DisplayObject;
      
      private var _selected:DisplayObject;
      
      private function onAdded(param1:Event) : void {
         if(param1.target.parent == this)
         {
            if(numChildren == 1)
            {
               selected = param1.target as DisplayObject;
            }
            else
            {
               param1.target.visible = false;
            }
         }
      }
      
      override protected function render(... rest) : void {
         updateSelected();
      }
      
      private function updateSelected() : void {
         if(_selected)
         {
            if(_selected.width != _width)
            {
               _selected.width = _width;
            }
            if(_selected.height != _height)
            {
               _selected.height = _height;
            }
         }
      }
      
      public function get selected() : DisplayObject {
         return _selected;
      }
      
      public function set selected(param1:DisplayObject) : void {
         var _loc2_:* = null;
         if(_selected != param1)
         {
            _loc2_ = _selected;
            _selected = param1;
            if(_loc2_)
            {
               _loc2_.visible = false;
            }
            if(param1 && numChildren && contains(param1))
            {
               _selected.visible = true;
               updateSelected();
            }
            else
            {
               _selected = null;
            }
            dispatchEvent(selectedEvent);
         }
      }
      
      override public function removeChild(param1:DisplayObject) : DisplayObject {
         if(_selected == param1)
         {
            selected = null;
            if(_defaultItem)
            {
               selected = _defaultItem;
            }
         }
         return super.removeChild(param1);
      }
      
      public function get defaultItem() : DisplayObject {
         return _defaultItem;
      }
      
      public function set defaultItem(param1:DisplayObject) : void {
         _defaultItem = param1;
      }
   }
}
