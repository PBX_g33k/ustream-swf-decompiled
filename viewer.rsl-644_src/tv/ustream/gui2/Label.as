package tv.ustream.gui2
{
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.events.KeyboardEvent;
   import flash.events.Event;
   import flash.geom.Rectangle;
   import flash.text.TextLineMetrics;
   import tv.ustream.tools.Debug;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   import tv.ustream.tools.DynamicEvent;
   
   public class Label extends Gui
   {
      
      public function Label(param1:String = null) {
         content = new Sprite();
         addChild(new Sprite()).cacheAsBitmap = true;
         labelTf = new TextField();
         content.addChild(new TextField());
         labelTf.selectable = false;
         labelTf.multiline = true;
         labelTf.wordWrap = false;
         labelTf.blendMode = "layer";
         labelTf.autoSize = "left";
         labelTf.text = param1 || "";
         labelTf.addEventListener("change",onLabelTfChange);
         labelTf.addEventListener("keyDown",onKeyDown);
         labelTf.addEventListener("focusIn",onFocusIn);
         labelTf.addEventListener("focusOut",onFocusOut);
         super();
      }
      
      public static var overSampling:int = 8;
      
      protected static var _style:Object = 
         {
            "background":false,
            "border":0
         };
      
      public static function get style() : Style {
         if(!Gui.style["label"])
         {
            Gui.style["label"] = new Style(_style);
         }
         return Gui.style.label;
      }
      
      protected var _overSample:Boolean = false;
      
      protected var _textCopy:Bitmap;
      
      protected var content:Sprite;
      
      protected var cMask:Sprite;
      
      protected var _contentMask:Boolean = true;
      
      protected var _customContent:Sprite;
      
      protected var labelTf:TextField;
      
      protected var textFormat:TextFormat;
      
      protected var _icon:Sprite;
      
      protected var _iconAlign:String = "left";
      
      protected var _align:String = "center";
      
      protected var _margin:Number = 8;
      
      protected var _password:Boolean = false;
      
      protected var _hint:String = "";
      
      protected var _hintAlpha:Number = 0.3;
      
      protected var _autoSize:Boolean = true;
      
      protected var _editable:Boolean = false;
      
      protected var originStyle:Style;
      
      private function onKeyDown(param1:KeyboardEvent) : void {
         if(param1.keyCode == 13)
         {
            dispatchEvent(new Event("enter",false,false));
         }
      }
      
      private function onFocusOut(... rest) : void {
         if(_editable)
         {
            _style = originStyle;
            render();
            if(labelTf.text == "" && _hint)
            {
               labelTf.alpha = _hintAlpha;
               labelTf.text = _hint;
               labelTf.displayAsPassword = false;
            }
         }
      }
      
      override public function set style(param1:Style) : void {
         if(_style)
         {
            _style.removeEventListener("update",render);
         }
         _style = param1;
         originStyle = param1;
         if(_style)
         {
            _style.addEventListener("update",render);
         }
         render();
      }
      
      private function onFocusIn(... rest) : void {
         if(_editable)
         {
            this._style = (this._style?this._style.clone():Label.style.clone()).config(
               {
                  "borderColor":getStyle("color"),
                  "border":1
               });
            render();
            addEventListener("enterFrame",selectFrame);
         }
      }
      
      public function setFocus() : void {
         if(_editable && stage)
         {
            stage.focus = labelTf;
         }
      }
      
      private function selectFrame(... rest) : void {
         removeEventListener("enterFrame",selectFrame);
         if(labelTf.text == _hint && !(_hint == ""))
         {
            labelTf.alpha = 1;
            labelTf.text = "";
            if(_password)
            {
               labelTf.displayAsPassword = true;
            }
         }
         labelTf.setSelection(0,labelTf.text.length);
      }
      
      override protected function init() : void {
         super.init();
         originStyle = Label.style;
         textFormat = new TextFormat(getStyle("font"),getStyle("size"),getStyle("color"),getStyle("bold"),null,getStyle("underline"));
         render();
      }
      
      override protected function render(... rest) : void {
         updateContent();
         if(_autoSize)
         {
            _width = content.width + _margin * 2;
            _height = content.height + _margin * 2;
         }
         arrange();
         super.render();
      }
      
      public function get normalWidth() : Number {
         return content.width + _margin * 2;
      }
      
      protected function updateContent(... rest) : void {
         var _loc3_:* = null;
         var _loc8_:* = null;
         var _loc4_:* = 0;
         var _loc5_:* = 0;
         var _loc6_:* = 0;
         var _loc2_:* = null;
         if(_icon && !(_icon["color"] == getStyle("color")))
         {
            _icon["color"] = getStyle("color");
         }
         textFormat.font = getStyle("font");
         textFormat.bold = getStyle("bold");
         textFormat.size = getStyle("size");
         textFormat.underline = getStyle("underline");
         textFormat.color = getStyle("color");
         textFormat.align = _align;
         var _loc9_:* = textFormat;
         labelTf.defaultTextFormat = _loc9_;
         labelTf.setTextFormat(_loc9_);
         var _loc7_:uint = 0;
         if(_icon)
         {
            _loc3_ = _icon.getBounds(_icon);
            _icon.x = _iconAlign == "left"?-_loc3_.x:_width - _icon.width - _loc3_.x;
            _icon.y = Math.floor(-_loc3_.y - icon.height / 2);
            _loc7_ = _icon.width + 5;
         }
         if(_customContent && _customContent.width)
         {
            _loc3_ = _customContent.getBounds(_customContent);
            _customContent.x = _loc7_;
            _customContent.y = Math.floor(-_loc3_.y - _customContent.height / 2);
            _loc7_ = _loc7_ + (_loc3_.width + 5 + _loc3_.x);
         }
         if(_autoSize)
         {
            labelTf.height = labelTf.textHeight + 4;
         }
         else if(_editable && labelTf.text == "")
         {
            _loc8_ = labelTf.getLineMetrics(0);
            labelTf.height = _loc8_.height + 4;
         }
         
         labelTf.x = _loc7_;
         labelTf.y = Math.floor(-labelTf.height / 2);
         if(_textCopy)
         {
            _textCopy.mask = labelTf.mask;
            _textCopy.x = labelTf.x;
            _textCopy.y = labelTf.y;
            _loc4_ = 0;
            _loc5_ = 0;
            _loc6_ = Label.overSampling;
            if(labelTf.width && (labelTf.height))
            {
               _loc6_ = Math.min(2880 / Math.round(labelTf.width),_loc6_);
               _loc6_ = Math.min(2880 / Math.round(labelTf.height),_loc6_);
               _loc6_ = Math.max(1,Math.pow(2,Math.floor(Math.log(_loc6_) / Math.log(2))));
               if(_loc6_ != Label.overSampling)
               {
                  Debug.echo("[ LABEL ] overSampling changed from " + overSampling + " to " + _loc6_);
               }
               _loc9_ = _loc6_;
               labelTf.scaleY = _loc9_;
               labelTf.scaleX = _loc9_;
               _loc4_ = Math.round(labelTf.textWidth) * _loc6_;
               _loc5_ = Math.round(labelTf.height / _loc6_) * _loc6_;
            }
            if(_loc4_ && (_loc5_))
            {
               _textCopy.bitmapData = new BitmapData(Math.round(labelTf.textWidth) * _loc6_,Math.round(labelTf.height / _loc6_) * _loc6_,true,0);
               _loc9_ = 1;
               labelTf.scaleY = _loc9_;
               labelTf.scaleX = _loc9_;
               _loc2_ = new Matrix();
               _loc2_.scale(_loc6_,_loc6_);
               _loc2_.translate(-_loc6_ * 2,1);
               _textCopy.bitmapData.draw(labelTf,_loc2_);
               _loc9_ = 1 / _loc6_;
               _textCopy.scaleY = _loc9_;
               _textCopy.scaleX = _loc9_;
            }
            else
            {
               if(_textCopy.bitmapData)
               {
                  _textCopy.bitmapData.dispose();
               }
               _textCopy.bitmapData = null;
            }
         }
         if(!_editable)
         {
            if(!label && (content.contains(labelTf)))
            {
               content.removeChild(labelTf);
            }
            else if(label && !overSample && !content.contains(labelTf))
            {
               content.addChild(labelTf);
            }
            
         }
         else if(!_autoSize)
         {
            labelTf.autoSize = "none";
            labelTf.width = _width - _loc7_ - 2 * _margin;
         }
         
      }
      
      protected function arrange(... rest) : void {
         var _loc5_:* = null;
         var _loc2_:* = null;
         var _loc4_:* = NaN;
         var _loc3_:* = NaN;
         _loc5_ = content.getBounds(content);
         if(_contentMask)
         {
            if(!autoSize && content.height > _height)
            {
               if(!cMask)
               {
                  cMask = new Sprite();
                  addChild(new Sprite()).cacheAsBitmap = true;
               }
               content.mask = cMask;
               cMask.visible = true;
               cMask.graphics.clear();
               _loc4_ = _height - _margin * 2;
               _loc2_ = new Matrix();
               _loc2_.createGradientBox(_width,_loc4_,3.141592653589793 / 2);
               cMask.graphics.beginGradientFill("linear",[0,0],[1,0],[255 - 3825 / _loc4_,255],_loc2_);
               cMask.graphics.drawRect(0,0,_width,_loc4_);
               cMask.graphics.endFill();
            }
            else if(!autoSize && content.width > _width - 2 * _margin)
            {
               if(!cMask)
               {
                  cMask = new Sprite();
                  addChild(new Sprite()).cacheAsBitmap = true;
               }
               content.mask = cMask;
               cMask.visible = true;
               cMask.graphics.clear();
               _loc3_ = _width - _margin * 2;
               _loc2_ = new Matrix();
               _loc2_.createGradientBox(_loc3_,_height);
               cMask.graphics.beginGradientFill("linear",[0,0],[1,0],[255 - 3825 / _loc3_,255],_loc2_);
               cMask.graphics.drawRect(0,0,_loc3_,_height);
               cMask.graphics.endFill();
            }
            else if(content.mask)
            {
               cMask.visible = false;
               content.mask = null;
            }
            
            
         }
         content.y = Math.max(0,-_loc5_.y + ((_height - _loc5_.height) / 2));
         if(_align == "left")
         {
            content.x = -_loc5_.x + _margin;
         }
         else if(_align == "right")
         {
            content.x = -_loc5_.x + (_width - (content.width > width?width:content.width)) - _margin;
         }
         else
         {
            _loc3_ = content.mask?cMask.width:content.width;
            content.x = -_loc5_.x + ((_width - (_loc3_ > width?width:_loc3_)) / 2);
         }
         
      }
      
      private function onLabelTfChange(... rest) : void {
         render();
         dispatchEvent(new DynamicEvent("change"));
      }
      
      override public function set width(param1:Number) : void {
         if(_autoSize)
         {
            autoSize = false;
         }
         .super.width = param1;
         render();
      }
      
      override public function set height(param1:Number) : void {
         if(_autoSize)
         {
            autoSize = false;
         }
         .super.height = param1;
         render();
      }
      
      public function get label() : String {
         return labelTf.text == _hint?"":labelTf.text;
      }
      
      public function set label(param1:String) : void {
         if(labelTf.text != param1)
         {
            labelTf.text = param1;
         }
         onLabelTfChange();
      }
      
      public function get autoSize() : Boolean {
         return _autoSize;
      }
      
      public function set autoSize(param1:Boolean) : void {
         if(content.scrollRect)
         {
            content.scrollRect = null;
         }
         if(_autoSize != param1)
         {
            _autoSize = param1;
            if(_autoSize)
            {
               render();
            }
         }
      }
      
      public function get icon() : Sprite {
         return _icon;
      }
      
      public function set icon(param1:Sprite) : void {
         if(_icon != param1)
         {
            if(_icon)
            {
               content.removeChild(_icon);
            }
            _icon = param1;
            if(_icon)
            {
               icon["color"] = getStyle("color");
               content.addChild(_icon);
               render();
            }
         }
      }
      
      public function get iconAlign() : String {
         return _iconAlign;
      }
      
      public function set iconAlign(param1:String) : void {
         if(param1 == "left" || param1 == "right")
         {
            _iconAlign = param1;
            render();
         }
      }
      
      public function get customContent() : Sprite {
         return _customContent;
      }
      
      public function set customContent(param1:Sprite) : void {
         if(_customContent != param1)
         {
            if(_customContent)
            {
               content.removeChild(_customContent);
            }
            _customContent = param1;
            if(_customContent)
            {
               content.addChild(_customContent);
            }
            render();
         }
      }
      
      public function get align() : String {
         return _align;
      }
      
      public function set align(param1:String) : void {
         if(param1 != _align)
         {
            _align = param1;
            render();
         }
      }
      
      public function get margin() : Number {
         return _margin;
      }
      
      public function set margin(param1:Number) : void {
         if(param1 != _margin)
         {
            _margin = param1;
            render();
         }
      }
      
      public function get editable() : Boolean {
         return _editable;
      }
      
      public function set editable(param1:Boolean) : void {
         if(_editable != param1)
         {
            _editable = param1;
            _loc2_ = param1;
            labelTf.selectable = _loc2_;
            labelTf.type = _loc2_?"input":"dynamic";
            labelTf.multiline = !param1;
            if(param1)
            {
               labelTf.addEventListener("change",onLabelChange);
            }
            else if(labelTf.hasEventListener("change"))
            {
               labelTf.removeEventListener("change",onLabelChange);
            }
            
            if(_editable && (overSample))
            {
               overSample = false;
            }
            else if(!content.contains(labelTf))
            {
               content.addChild(labelTf);
            }
            
         }
      }
      
      private function onLabelChange(param1:Event) : void {
         dispatchEvent(new DynamicEvent("changeLabel",true,true,{"label":labelTf.text}));
      }
      
      public function get maxChars() : Number {
         return labelTf.maxChars;
      }
      
      public function set maxChars(param1:Number) : void {
         labelTf.maxChars = param1;
      }
      
      public function config(param1:Object) : void {
         var _loc2_:* = false;
         var _loc7_:* = 0;
         var _loc6_:* = param1;
         for(_loc3_ in param1)
         {
            try
            {
               this["_" + _loc3_] = param1[_loc3_];
            }
            catch(e:Error)
            {
               trace(id,"Cannot create property : _" + _loc3_);
               continue;
            }
         }
         render();
      }
      
      override public function get mouseEnabled() : Boolean {
         return super.mouseEnabled;
      }
      
      override public function set mouseEnabled(param1:Boolean) : void {
         if(super.mouseEnabled != param1)
         {
            .super.mouseEnabled = param1;
            alpha = param1?1:0.5;
         }
      }
      
      override public function set alpha(param1:Number) : void {
         .super.alpha = param1;
         labelTf.alpha = param1;
      }
      
      public function set contentMask(param1:Boolean) : void {
         _contentMask = param1;
         if(content.mask)
         {
            content.mask = null;
         }
         if(cMask)
         {
            if(contains(cMask))
            {
               removeChild(cMask);
            }
            cMask = null;
         }
      }
      
      override public function resize(param1:Number, param2:Number) : void {
         if(_autoSize)
         {
            autoSize = false;
         }
         super.resize(param1,param2);
         render();
      }
      
      public function get hint() : String {
         return _hint;
      }
      
      public function set hint(param1:String) : void {
         if(param1 != "")
         {
            _hint = param1;
            if(label == "")
            {
               labelTf.text = _hint;
               labelTf.alpha = _hintAlpha;
            }
         }
      }
      
      public function get password() : Boolean {
         return _password;
      }
      
      public function set password(param1:Boolean) : void {
         _password = param1;
         if(_password)
         {
            if(!(_hint == "") && label == "")
            {
               labelTf.text = _hint;
               labelTf.displayAsPassword = false;
               labelTf.alpha = _hintAlpha;
            }
            else
            {
               labelTf.alpha = 1;
               labelTf.displayAsPassword = true;
            }
         }
         else
         {
            labelTf.displayAsPassword = false;
         }
      }
      
      public function get labelWidth() : Number {
         return labelTf.width;
      }
      
      public function get overSample() : Boolean {
         return _overSample;
      }
      
      public function set overSample(param1:Boolean) : void {
         if(_overSample != param1)
         {
            _overSample = param1;
            if(_overSample && !_textCopy)
            {
               _textCopy = new Bitmap();
               content.addChild(new Bitmap());
               _textCopy.smoothing = true;
               _textCopy.cacheAsBitmap = true;
            }
            else if(!_overSample && _textCopy)
            {
               if(content.contains(_textCopy))
               {
                  content.removeChild(_textCopy);
               }
               _textCopy.bitmapData.dispose();
               _textCopy = null;
            }
            
            if(overSample && (content.contains(labelTf)))
            {
               content.removeChild(labelTf);
            }
            else if(!overSample && !content.contains(labelTf))
            {
               content.addChild(labelTf);
            }
            
            render();
         }
      }
      
      public function set hintAlpha(param1:Number) : void {
         _hintAlpha = param1;
         if(label == "")
         {
            labelTf.alpha = _hintAlpha;
         }
      }
   }
}
