package tv.ustream.gui2
{
   import tv.ustream.data2.DataManager;
   import tv.ustream.tools.Container;
   import tv.ustream.tools.ScrollBar;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.display.DisplayObject;
   import tv.ustream.tools.DynamicEvent;
   import flash.geom.Rectangle;
   import tv.ustream.data2.renderers.DataListRenderer;
   
   public class ComboBox extends Button
   {
      
      public function ComboBox(param1:String) {
         super(param1);
         addEventListener("mouseDown",onMouseDown);
         autoSize = false;
         dmBg = new Container();
         dmBg.visible = false;
         dm = new DataManager(null,new DataListRenderer(),Button);
         dm.visible = false;
         dm.y = height + 2;
         _listWidth = width;
         dm.width = width;
         dm.height = height;
         dm.dataRenderer.rowHeight = 20;
         dm.dataRenderer.direction = "vertical";
         dm.addEventListener("scrollUpdate",onScrollUpdate);
         dm.addEventListener("click",onItemClick);
         scrollContainer = new Sprite();
         scrollContainer.visible = false;
         scroll = new ScrollBar();
         scrollContainer.addChild(new ScrollBar());
         scroll.addEventListener("yPercent",onScroll);
         scroll.width = 15;
         scroll.height = dm.height;
         scroll.grabYpercent = 1;
         dm.dataProvider.addEventListener("addItem",onAddItem);
      }
      
      protected static var _style:Object = 
         {
            "size":13,
            "bold":true,
            "border":1,
            "borderColor":12303291,
            "background":true,
            "backgroundColor2":15330545,
            "cornerRadius":8
         };
      
      public static function get style() : Style {
         if(!Gui.style["combobox"])
         {
            Gui.style["combobox"] = new Style(_style);
         }
         return Gui.style.combobox;
      }
      
      private var _open:Boolean = false;
      
      private var dm:DataManager;
      
      private var dmBg:Container;
      
      private var _listWidth:Number = 0;
      
      private var _listHeight:Number = -1;
      
      private var _listBackground:Boolean = false;
      
      private var listMinHeight:Number = 100;
      
      private var scroll:ScrollBar;
      
      private var scrollContainer:Sprite;
      
      private var _listOffsetX:Number = 0;
      
      private var _listOffsetY:Number = 0;
      
      private var isListOffsetX:Boolean = false;
      
      private var isListOffsetY:Boolean = false;
      
      protected var _selectedProperty:String = "mouseEnabled";
      
      protected var _selectedValue = false;
      
      public function get selectedProperty() : String {
         return _selectedProperty;
      }
      
      public function set selectedProperty(param1:String) : void {
         _selectedProperty = param1;
      }
      
      public function get selectedValue() : * {
         return _selectedValue;
      }
      
      public function set selectedValue(param1:*) : void {
         _selectedValue = param1;
      }
      
      public var innerScroll:Boolean = false;
      
      public function get hasSelected() : Boolean {
         return dm.dataProvider.getItemAtProperty(_selectedProperty,_selectedValue).length > 0;
      }
      
      private function onAddItem(param1:Event) : void {
         dm.dataProvider.updateItemAt(dm.dataProvider.data.length - 1,{"width":dm.width});
      }
      
      private function onMouseDown(param1:MouseEvent) : void {
         open = !open;
      }
      
      private function onStageMouseDown(param1:MouseEvent) : void {
         if(open && (!contains(param1.target as DisplayObject) && !scroll.contains(param1.target as DisplayObject) && !dm.contains(param1.target as DisplayObject)))
         {
            open = false;
         }
      }
      
      private function onItemClick(param1:MouseEvent) : void {
         var _loc3_:* = null;
         var _loc2_:* = null;
         if(param1.target is dm.itemRenderer && !(param1.target == this))
         {
            _loc3_ = {};
            _loc3_[_selectedProperty] = !_selectedValue;
            dm.dataProvider.updateItemAtProperty(_selectedProperty,_selectedValue,_loc3_);
            _loc2_ = dm.getDataOfInstance(param1.target as DisplayObject);
            label = _loc2_.label;
            _loc3_[_selectedProperty] = _selectedValue;
            dm.dataProvider.updateItem(_loc2_,_loc3_);
            dispatchEvent(new Event("select"));
         }
         param1.stopImmediatePropagation();
         param1.stopPropagation();
         open = false;
      }
      
      private function onMouseWheel(param1:MouseEvent) : void {
         var _loc2_:* = NaN;
         if(scrollContainer.visible)
         {
            _loc2_ = dm.percent - param1.delta / 3 * dm.itemPercent;
            _loc3_ = _loc2_ < 0?0:_loc2_ > 1?1:_loc2_;
            scroll.yPercent = _loc3_;
            dm.percent = _loc3_;
            param1.stopImmediatePropagation();
            param1.stopPropagation();
         }
      }
      
      private function onScroll(param1:DynamicEvent) : void {
         dm.percent = param1.percent;
      }
      
      private function onScrollUpdate(param1:DynamicEvent) : void {
         if(!isNaN(param1.grab))
         {
            scroll.grabYpercent = param1.grab;
         }
      }
      
      override public function set mouseEnabled(param1:Boolean) : void {
         mouseChildren = param1;
         .super.mouseEnabled = param1;
      }
      
      override public function set width(param1:Number) : void {
         .super.width = param1;
         _listWidth = param1 - (innerScroll) * scrollContainer.width;
         dm.width = param1 - (innerScroll) * scrollContainer.width;
      }
      
      public function get listWidth() : Number {
         return _listWidth;
      }
      
      public function set listWidth(param1:Number) : void {
         var _loc2_:* = param1;
         dm.width = _loc2_;
         _listWidth = _loc2_;
         dm.dataProvider.updateItemAtProperty("semmi",null,{"width":param1},true);
      }
      
      public function get listHeight() : Number {
         return _listHeight;
      }
      
      public function set listHeight(param1:Number) : void {
         var _loc2_:* = param1;
         dm.height = _loc2_;
         _listHeight = _loc2_;
      }
      
      public function get listOffsetX() : Number {
         return _listOffsetX;
      }
      
      public function set listOffsetX(param1:Number) : void {
         isListOffsetX = true;
         _listOffsetX = param1;
      }
      
      public function get listOffsetY() : Number {
         return _listOffsetY;
      }
      
      public function set listOffsetY(param1:Number) : void {
         isListOffsetY = true;
         _listOffsetY = param1;
      }
      
      public function set listBackground(param1:Object) : void {
         if(param1 != null)
         {
            _listBackground = true;
            _loc4_ = 0;
            _loc3_ = param1;
            for(_loc2_ in param1)
            {
               if(dmBg[_loc2_])
               {
                  dmBg[_loc2_] = param1[_loc2_];
               }
            }
         }
         else
         {
            _listBackground = false;
         }
      }
      
      public function get dataManager() : DataManager {
         return dm;
      }
      
      public function get open() : Boolean {
         return _open;
      }
      
      public function set open(param1:Boolean) : void {
         var _loc2_:* = null;
         var _loc3_:* = false;
         _open = param1;
         dm.visible = _open;
         if(_open)
         {
            if(!dm.dataProvider.data.length)
            {
               return;
            }
            if(!dm.stage)
            {
               stage.addChild(dm);
            }
            if(!dmBg.stage)
            {
               stage.addChild(dmBg);
            }
            if(!scrollContainer.stage)
            {
               stage.addChild(scrollContainer);
            }
            _loc2_ = bg.getBounds(stage);
            _loc2_.height = _loc2_.height + (dm.dataRenderer.rowHeight - 1);
            _loc3_ = stage.stageHeight < _loc2_.y + _loc2_.height + (listHeight > 0?listHeight:listMinHeight);
            dm.height = Math.floor((listHeight > 0?listHeight:_loc3_?_loc2_.y:stage.stageHeight - (_loc2_.y + _loc2_.height)) / dm.dataRenderer.rowHeight) * dm.dataRenderer.rowHeight;
            if(dm.height > dm.visibleItems * dm.dataRenderer.rowHeight)
            {
               dm.height = dm.visibleItems * dm.dataRenderer.rowHeight;
            }
            dm.y = _loc2_.y + (isListOffsetY?_listOffsetY:_loc3_?-dm.height:height);
            if(!_listBackground)
            {
               dm.x = _loc2_.x + (isListOffsetX?_listOffsetX:stage.stageWidth < _loc2_.x + dm.width?_loc2_.width - dm.width:0.0);
            }
            else
            {
               dm.x = _loc2_.x + (isListOffsetX?_listOffsetX:(stage.stageWidth < _loc2_.x + dm.width?_loc2_.width - dm.width:dm.x) + 1);
               dmBg.width = dm.width + 2;
               dmBg.height = dm.height + 2;
               dmBg.x = dm.x - 1;
               dmBg.y = dm.y - 2;
               dm.y--;
            }
            dm.addEventListener("mouseWheel",onMouseWheel);
            stage.addEventListener("mouseUp",onStageMouseDown);
            scroll.height = dm.height;
            scrollContainer.graphics.clear();
            scrollContainer.graphics.beginFill(this.style.color,this.style.backgroundAlpha);
            scrollContainer.graphics.drawRect(0,0,scroll.width,scroll.height);
            scrollContainer.x = dm.x + dm.width;
            scrollContainer.y = dm.y;
            scrollContainer.visible = scroll.grabYpercent < 1?true:false;
         }
         else
         {
            stage.removeEventListener("click",onStageMouseDown);
            if(dm.hasEventListener("mouseWheel"))
            {
               dm.removeEventListener("mouseWheel",onMouseWheel);
            }
            scrollContainer.visible = false;
            if(dm.stage)
            {
               stage.removeChild(dm);
            }
            if(dmBg.stage)
            {
               stage.removeChild(dmBg);
            }
            if(scrollContainer.stage)
            {
               stage.removeChild(scrollContainer);
            }
         }
         if(_listBackground)
         {
            dmBg.visible = _open;
         }
      }
      
      override protected function init() : void {
         super.init();
      }
   }
}
