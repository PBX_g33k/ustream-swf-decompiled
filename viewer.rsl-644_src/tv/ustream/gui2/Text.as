package tv.ustream.gui2
{
   import flash.text.TextField;
   import flash.display.Sprite;
   import flash.text.TextFormat;
   import tv.ustream.tools.ScrollBar;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.geom.Matrix;
   import tv.ustream.tools.Debug;
   import flash.display.BitmapData;
   import flash.events.FocusEvent;
   
   public class Text extends Gui
   {
      
      public function Text(param1:String = null) {
         textField = new TextField();
         textField.cacheAsBitmap = true;
         textField.autoSize = "none";
         textField.wordWrap = false;
         textField.multiline = true;
         textField.mouseWheelEnabled = false;
         textField.selectable = false;
         textField.addEventListener("scroll",onTextScroll);
         textMask = new Sprite();
         textMask.cacheAsBitmap = true;
         _scrollBar = new ScrollBar();
         _scrollBar.visible = _scroll;
         _scrollBar.addEventListener("drag",onScrollDrag);
         super();
         if(param1)
         {
            this.text = param1;
         }
         addChild(textField);
         addChild(textMask);
         addChild(_scrollBar);
      }
      
      public static var overSampling:int = 4;
      
      protected static var _style:Object = {};
      
      public static function get style() : Style {
         if(!Gui.style["text"])
         {
            Gui.style["text"] = new Style(_style);
         }
         return Gui.style.text;
      }
      
      protected var _autoSizeHorizontal:Boolean = false;
      
      protected var _autoSizeVertical:Boolean = false;
      
      protected var textField:TextField;
      
      protected var textContainer:Sprite;
      
      protected var textFormat:TextFormat;
      
      protected var textMask:Sprite;
      
      protected var _text:String;
      
      protected var _minWidth:Number = 0;
      
      protected var _minHeight:Number = 0;
      
      protected var _maxWidth:Number = 2880;
      
      protected var _maxHeight:Number = 2880;
      
      protected var _align:String = "left";
      
      protected var _horizontalMargin:Number = 0;
      
      protected var _verticalMargin:Number = 0;
      
      protected var originStyle:Style;
      
      protected var _scroll:Boolean = false;
      
      protected var _scrollBar:ScrollBar;
      
      protected var _overSample:Boolean = false;
      
      protected var _textCopy:Bitmap;
      
      protected var _contentMask:Boolean = true;
      
      protected var _editable:Boolean = false;
      
      protected var _hint:String = "";
      
      public function get selectable() : Boolean {
         return textField.selectable;
      }
      
      public function set selectable(param1:Boolean) : void {
         textField.selectable = param1;
      }
      
      override protected function init() : void {
         super.init();
         originStyle = Text.style;
         textFormat = new TextFormat(getStyle("font"),getStyle("size"),getStyle("color"),getStyle("bold"));
         updateTextFormat();
      }
      
      private function updateTextFormat() : void {
         textField.setTextFormat(textFormat);
         realign();
      }
      
      public function get wordWrap() : Boolean {
         return textField.wordWrap;
      }
      
      public function set wordWrap(param1:Boolean) : void {
         textField.wordWrap = param1;
         realign();
      }
      
      public function get autoSizeHorizontal() : Boolean {
         return _autoSizeHorizontal;
      }
      
      public function set autoSizeHorizontal(param1:Boolean) : void {
         _autoSizeHorizontal = param1;
         realign();
      }
      
      public function get autoSizeVertical() : Boolean {
         return _autoSizeVertical;
      }
      
      public function set autoSizeVertical(param1:Boolean) : void {
         _autoSizeVertical = param1;
         realign();
      }
      
      override public function set width(param1:Number) : void {
         if(super.width != Math.floor(param1))
         {
            .super.width = Math.floor(param1);
            realign();
         }
      }
      
      override public function set height(param1:Number) : void {
         if(super.height != Math.floor(param1))
         {
            .super.height = Math.floor(param1);
            realign();
         }
      }
      
      public function get label() : String {
         return text;
      }
      
      public function set label(param1:String) : void {
         text = param1;
      }
      
      public function get text() : String {
         return _text?_text:"";
      }
      
      public function set text(param1:String) : void {
         if(_text != param1)
         {
            _text = param1;
            textField.htmlText = param1;
            updateTextFormat();
         }
      }
      
      public function get minWidth() : Number {
         return _minWidth;
      }
      
      public function set minWidth(param1:Number) : void {
         _minWidth = Math.min(_maxWidth,Math.max(0,param1));
         if(_width != Math.max(_minWidth,_width))
         {
            _width = Math.max(_minWidth,_width);
            dispatchEvent(new Event("resize"));
         }
         realign();
      }
      
      public function get minHeight() : Number {
         return _minHeight;
      }
      
      public function set minHeight(param1:Number) : void {
         _minHeight = Math.min(_maxHeight,Math.max(0,param1));
         if(_height != Math.max(_height,_minHeight))
         {
            _height = Math.max(_height,_minHeight);
            dispatchEvent(new Event("resize"));
         }
         realign();
      }
      
      public function get maxWidth() : Number {
         return _maxWidth;
      }
      
      public function set maxWidth(param1:Number) : void {
         _maxWidth = Math.min(2880,Math.max(_minWidth,param1));
         realign();
      }
      
      public function get maxHeight() : Number {
         return _maxHeight;
      }
      
      public function set maxHeight(param1:Number) : void {
         _maxHeight = Math.min(2880,Math.max(_minHeight,param1));
         realign();
      }
      
      public function get horizontalMargin() : Number {
         return _horizontalMargin;
      }
      
      public function set horizontalMargin(param1:Number) : void {
         _horizontalMargin = Math.max(0,param1);
         realign();
      }
      
      public function get verticalMargin() : Number {
         return _verticalMargin;
      }
      
      public function set verticalMargin(param1:Number) : void {
         _verticalMargin = Math.max(0,param1);
         realign();
      }
      
      public function set margin(param1:Number) : void {
         verticalMargin = param1;
         horizontalMargin = param1;
      }
      
      public function get scrollBar() : ScrollBar {
         return _scrollBar;
      }
      
      public function get scroll() : Boolean {
         return _scroll;
      }
      
      public function set scroll(param1:Boolean) : void {
         _scroll = param1;
         realign();
      }
      
      protected function realign() : void {
         var _loc2_:* = null;
         var _loc6_:* = NaN;
         var _loc4_:* = NaN;
         var _loc5_:* = 0;
         var _loc1_:* = null;
         var _loc7_:* = false;
         var _loc9_:* = horizontalMargin;
         textField.x = _loc9_;
         textMask.x = _loc9_;
         _loc9_ = verticalMargin;
         textField.y = _loc9_;
         textMask.y = _loc9_;
         var _loc3_:Number = horizontalMargin * 2 + (_scrollBar.visible) * _scrollBar.width;
         var _loc8_:Number = verticalMargin * 2;
         textField.width = _width - _loc3_;
         textField.height = _height - _loc8_;
         if(autoSizeHorizontal)
         {
            textField.width = maxWidth - _loc3_;
            textField.width = Math.min(maxWidth - _loc3_,Math.max(minWidth - _loc3_,textField.textWidth + 5));
            if(_width != textField.width + _loc3_)
            {
               _width = textField.width + _loc3_;
               _loc7_ = true;
            }
         }
         if(autoSizeVertical)
         {
            textField.height = Math.min(maxHeight - _loc8_,Math.max(minHeight - _loc8_,textField.textHeight + textField.getLineMetrics(0).descent + textField.getLineMetrics(0).leading + 2));
            if(_height != textField.height + _loc8_)
            {
               _height = textField.height + _loc8_;
               _loc7_ = true;
            }
         }
         if(_height < textField.textHeight && !_scroll)
         {
            textField.mask = textMask;
            textMask.graphics.clear();
            _loc6_ = _height - (_contentMask?verticalMargin * 2:0.0);
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(_width - horizontalMargin * 2,_loc6_,3.141592653589793 / 2);
            textMask.graphics.beginGradientFill("linear",[0,0],[1,0],[255 - 3825 / _loc6_,255],_loc2_);
            textMask.graphics.drawRect(0,0,_width - horizontalMargin * 2,_loc6_);
            textMask.graphics.endFill();
         }
         else if(_width < textField.textWidth)
         {
            textField.mask = textMask;
            textMask.graphics.clear();
            _loc4_ = _width - (_contentMask?horizontalMargin * 2:0.0);
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(_loc4_,_height - verticalMargin * 2);
            textMask.graphics.beginGradientFill("linear",[0,0],[1,0],[255 - 3825 / _loc4_,255],_loc2_);
            textMask.graphics.drawRect(0,0,_loc4_,_height - verticalMargin * 2);
            textMask.graphics.endFill();
         }
         else
         {
            textField.mask = null;
            textMask.graphics.clear();
         }
         
         if(_scroll)
         {
            onTextScroll();
            if(1 + textField.bottomScrollV - textField.scrollV < textField.numLines)
            {
               if(!_scrollBar.visible)
               {
                  _scrollBar.visible = true;
                  realign();
               }
            }
            else if(_scrollBar.visible)
            {
               _scrollBar.visible = false;
               realign();
            }
            
         }
         _scrollBar.height = _height - verticalMargin * 2;
         _scrollBar.x = _width - horizontalMargin - _scrollBar.width;
         _scrollBar.y = verticalMargin;
         if(_textCopy && textField.width && textField.height)
         {
            _textCopy.mask = textField.mask;
            _textCopy.x = textField.x;
            _textCopy.y = textField.y;
            _loc5_ = Math.min(2880 / Math.round(textField.width),overSampling);
            _loc5_ = Math.min(2880 / Math.round(textField.height),_loc5_);
            _loc5_ = Math.max(1,Math.floor(_loc5_ / 2) * 2);
            if(_loc5_ != overSampling)
            {
               Debug.echo("[ TEXT ] overSampling lowered from " + overSampling + " to " + _loc5_);
            }
            _textCopy.bitmapData = new BitmapData(Math.round(textField.width) * _loc5_,Math.round(textField.height) * _loc5_,true,0);
            _loc1_ = new Matrix();
            _loc1_.scale(_loc5_,_loc5_);
            _textCopy.bitmapData.draw(textField,_loc1_);
            _loc9_ = 1 / _loc5_;
            _textCopy.scaleY = _loc9_;
            _textCopy.scaleX = _loc9_;
         }
         if(_loc7_)
         {
            dispatchEvent(new Event("resize"));
         }
         render();
      }
      
      private function onTextScroll(param1:Event = null) : void {
         _scrollBar.yPercent = (textField.scrollV - 1) / Math.max(textField.maxScrollV - 1,1);
         _scrollBar.grabYpercent = (1 + textField.bottomScrollV - textField.scrollV) / textField.numLines;
      }
      
      private function onScrollDrag(param1:Event) : void {
         textField.removeEventListener("scroll",onTextScroll);
         textField.scrollV = textField.maxScrollV * _scrollBar.yPercent;
         textField.addEventListener("scroll",onTextScroll);
      }
      
      override public function set style(param1:Style) : void {
         .super.style = param1;
         textFormat = new TextFormat(getStyle("font"),getStyle("size"),getStyle("color"),getStyle("bold"));
         textFormat.align = _align;
         updateTextFormat();
      }
      
      public function get align() : String {
         return _align;
      }
      
      public function set align(param1:String) : void {
         _align = param1;
         textFormat.align = _align;
         updateTextFormat();
      }
      
      public function get overSample() : Boolean {
         return _overSample;
      }
      
      public function set overSample(param1:Boolean) : void {
         if(_overSample != param1)
         {
            _overSample = param1;
            textField.visible = !param1;
            if(_overSample && !_textCopy)
            {
               _textCopy = new Bitmap();
               addChild(new Bitmap());
               _textCopy.smoothing = true;
               _textCopy.cacheAsBitmap = true;
            }
            else if(!_overSample && _textCopy)
            {
               removeChild(_textCopy);
               _textCopy.bitmapData.dispose();
               _textCopy = null;
            }
            
            realign();
         }
      }
      
      public function get contentMask() : Boolean {
         return _contentMask;
      }
      
      public function set contentMask(param1:Boolean) : void {
         if(_contentMask != param1)
         {
            _contentMask = param1;
            realign();
         }
      }
      
      public function get editable() : Boolean {
         return _editable;
      }
      
      public function set editable(param1:Boolean) : void {
         if(_editable != param1)
         {
            _editable = param1;
            textField.selectable = param1;
            textField.type = _editable?"input":"dynamic";
            if(_editable && (overSample))
            {
               overSample = false;
            }
            if(_editable)
            {
               if(!textField.hasEventListener("change"))
               {
                  textField.addEventListener("change",onFieldChange);
               }
               if(!textField.hasEventListener("focusIn"))
               {
                  textField.addEventListener("focusIn",onFieldFocusIn);
               }
               if(!textField.hasEventListener("focusOut"))
               {
                  textField.addEventListener("focusOut",onFieldFocusOut);
               }
               if((!_text || _text && !_text.length) && _hint)
               {
                  textField.htmlText = hint;
                  textField.alpha = 0.5;
                  updateTextFormat();
               }
            }
            else
            {
               if(textField.hasEventListener("change"))
               {
                  textField.removeEventListener("change",onFieldChange);
               }
               if(textField.hasEventListener("focusIn"))
               {
                  textField.removeEventListener("focusIn",onFieldFocusIn);
               }
               if(textField.hasEventListener("focusOut"))
               {
                  textField.removeEventListener("focusOut",onFieldFocusOut);
               }
               textField.htmlText = _text;
               textField.alpha = 1;
               updateTextFormat();
            }
         }
      }
      
      private function onFieldFocusIn(param1:FocusEvent) : void {
         if(!_text || _text && !_text.length)
         {
            _text = "";
            textField.htmlText = "";
            updateTextFormat();
         }
         textField.alpha = 1;
         dispatchEvent(new FocusEvent("focusIn"));
      }
      
      private function onFieldFocusOut(param1:FocusEvent) : void {
         if((!_text || _text && !_text.length) && _hint)
         {
            textField.htmlText = hint;
            textField.alpha = 0.5;
            updateTextFormat();
         }
         dispatchEvent(new FocusEvent("focusOut"));
      }
      
      public function get hint() : String {
         return _hint;
      }
      
      public function set hint(param1:String) : void {
         if(_hint != param1)
         {
            _hint = param1;
            if(_editable && (!_text || !_text.length) && _hint)
            {
               textField.htmlText = hint;
               textField.alpha = 0.5;
               updateTextFormat();
            }
         }
      }
      
      private function onFieldChange(param1:Event) : void {
         _text = textField.text;
         dispatchEvent(new Event("change"));
         updateTextFormat();
         realign();
      }
      
      override public function set mouseEnabled(param1:Boolean) : void {
         .super.mouseChildren = param1;
         .super.mouseEnabled = param1;
      }
   }
}
