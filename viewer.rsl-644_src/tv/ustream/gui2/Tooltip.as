package tv.ustream.gui2
{
   import flash.display.Sprite;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   
   public class Tooltip extends Sprite
   {
      
      public function Tooltip(param1:String = "") {
         super();
         body = new Text(param1);
         var _loc2_:* = false;
         body.mouseChildren = _loc2_;
         body.mouseEnabled = _loc2_;
         body.height = 16;
         body.horizontalMargin = 3;
         body.verticalMargin = 1;
         body.wordWrap = true;
         body.autoSizeVertical = true;
         body.autoSizeHorizontal = true;
         if(stage)
         {
            onAddedToStage();
         }
         else
         {
            addEventListener("addedToStage",onAddedToStage);
         }
         pin = new Sprite();
         body.addEventListener("resize",align);
         addEventListener("enterFrame",onEnterFrame);
         bitmap = new Bitmap();
         addChild(new Bitmap());
         mouseEnabled = false;
         mouseChildren = false;
         render();
         visible = false;
      }
      
      private static const FONT_SIZE:Number = 12;
      
      private static const MARGIN_SIZE:Number = 2;
      
      public static const LEFT:String = "left";
      
      public static const RIGHT:String = "right";
      
      public static const CENTER:String = "center";
      
      public static const TOP:String = "top";
      
      public static const BOTTOM:String = "bottom";
      
      private var body:Text;
      
      private var pin:Sprite;
      
      private var bitmap:Bitmap;
      
      private var _pinSize:uint = 6;
      
      private var _pinAlignHorizontal:String = "center";
      
      private var _pinAlignVertical:String = "bottom";
      
      public var sustain:uint = 5;
      
      public var decay:uint = 1;
      
      private var counter:int = -1;
      
      private function onStyleUpdate(param1:Event) : void {
         render();
      }
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener("addedToStage",onAddedToStage);
         stage.addEventListener("resize",onStageResize);
         stage.addEventListener("removedFromStage",onRemovedFromStage);
         onStageResize();
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         stage.removeEventListener("resize",onStageResize);
         removeEventListener("removedFromStage",onRemovedFromStage);
         addEventListener("addedToStage",onAddedToStage);
      }
      
      private function onStageResize(param1:Event = null) : void {
         if(body && stage)
         {
            body.maxWidth = stage.stageWidth * 0.5;
         }
      }
      
      public function get pinSize() : uint {
         return _pinSize;
      }
      
      public function set pinSize(param1:uint) : void {
         _pinSize = param1;
         render();
      }
      
      public function get pinAlignHorizontal() : String {
         return _pinAlignHorizontal;
      }
      
      public function set pinAlignHorizontal(param1:String) : void {
         if(param1 == "left" || param1 == "right")
         {
            _pinAlignHorizontal = param1;
         }
         else
         {
            _pinAlignHorizontal = "center";
         }
         render();
      }
      
      public function get pinAlignVertical() : String {
         return _pinAlignVertical;
      }
      
      public function set pinAlignVertical(param1:String) : void {
         _pinAlignVertical = param1 == "top"?"top":"bottom";
         render();
      }
      
      private function initStyle() : void {
         if(Gui.style["tooltip"])
         {
            Gui.style["tooltip"].removeEventListener("update",onStyleUpdate);
         }
         Gui.style["tooltip"] = new Style(
            {
               "cornerRadius":0,
               "background":true,
               "backgroundColor":Gui.style.color,
               "size":12,
               "margin":2,
               "color":Gui.style.backgroundColor,
               "backgroundColor2":-1,
               "border":Gui.style.border,
               "borderColor":Gui.style.borderColor
            });
         body.style = Gui.style["tooltip"];
         Gui.style["tooltip"].addEventListener("update",onStyleUpdate);
      }
      
      private function drawPin() : void {
         pin.graphics.clear();
         pin.graphics.beginFill(Gui.style["tooltip"].backgroundColor);
         pin.graphics.lineStyle(Gui.style["tooltip"].border,Gui.style["tooltip"].borderColor,1);
         pin.graphics.moveTo((_pinAlignHorizontal == "center") * -_pinSize,(_pinAlignVertical == "top") * _pinSize);
         pin.graphics.lineTo(0,(_pinAlignVertical == "bottom") * _pinSize);
         pin.graphics.lineTo((1 - (_pinAlignHorizontal == "right") * 2) * _pinSize,(_pinAlignVertical == "top") * _pinSize);
         pin.graphics.lineStyle(0,0,0);
         pin.graphics.lineTo((_pinAlignHorizontal == "center") * -_pinSize,(_pinAlignVertical == "top") * _pinSize);
         pin.graphics.endFill();
      }
      
      public function get label() : String {
         return body.label;
      }
      
      public function set label(param1:String) : void {
         body.label = param1;
         align();
         .super.visible = visible && body.label.length > 0;
      }
      
      private function align(... rest) : void {
         if(bitmap.bitmapData)
         {
            bitmap.bitmapData.dispose();
         }
         bitmap.bitmapData = new BitmapData(body.width,body.height + _pinSize,true,0);
         var _loc2_:Matrix = new Matrix();
         _loc2_.translate(0,_pinAlignVertical == "top"?_pinSize - 1:0.0);
         bitmap.bitmapData.draw(body,_loc2_);
         _loc2_ = new Matrix();
         var _loc3_:* = 0.0;
         var _loc4_:Number = _pinAlignVertical == "top"?0:body.height - 1;
         if(_pinAlignHorizontal == "center")
         {
            _loc3_ = body.width / 2;
         }
         else if(_pinAlignHorizontal == "right")
         {
            _loc3_ = body.width - 1;
         }
         
         _loc2_.translate(_loc3_,_loc4_);
         bitmap.bitmapData.draw(pin,_loc2_);
         bitmap.x = _pinAlignHorizontal == "center"?-bitmap.width / 2:_pinAlignHorizontal == "right"?-bitmap.width:0.0;
         bitmap.y = _pinAlignVertical == "bottom"?-bitmap.height:0.0;
      }
      
      private function onEnterFrame(param1:Event) : void {
         var _loc2_:uint = stage?stage.frameRate:30.0;
         if(alpha > 0 && (visible) && counter >= 0)
         {
            counter = counter + 1;
            if(counter > sustain * _loc2_)
            {
               alpha = alpha - 1 / (decay * _loc2_);
               if(alpha <= 0)
               {
                  alpha = 1;
                  .super.visible = false;
                  counter = -1;
               }
            }
         }
      }
      
      protected function render() : void {
         initStyle();
         drawPin();
         align();
      }
      
      public function blink() : void {
         alpha = 1;
         .super.visible = true;
         counter = 0;
      }
      
      override public function set visible(param1:Boolean) : void {
         alpha = 1;
         counter = -1;
         .super.visible = param1 && body && body.label && body.label.length;
      }
      
      public function get textAlign() : String {
         return body.align;
      }
      
      public function set textAlign(param1:String) : void {
         body.align = param1;
      }
   }
}
