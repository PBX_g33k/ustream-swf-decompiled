package tv.ustream.gui2
{
   import flash.display.Sprite;
   import flash.geom.Rectangle;
   import flash.events.Event;
   
   public class CheckBox extends Label
   {
      
      public function CheckBox(param1:String = null, param2:Boolean = true) {
         buttonMode = true;
         mouseChildren = false;
         _checked = param2;
         super(param1);
         addEventListener("mouseDown",onMouseDown);
      }
      
      protected static var _style:Object = 
         {
            "backgroundAlpha":1,
            "backgroundColor":16777215,
            "bold":false,
            "background":false,
            "border":0
         };
      
      public static function get style() : Style {
         if(!Gui.style["checkbox"])
         {
            Gui.style["checkbox"] = new Style(_style);
         }
         return Gui.style.checkbox;
      }
      
      private var _checked:Boolean;
      
      private var checker:Sprite;
      
      protected var _checkerStyle:Style;
      
      override protected function init() : void {
         super.init();
         customContent = new Sprite();
         checker = new Sprite();
         renderCustomContent(new Sprite());
         checker.visible = _checked;
         customContent.addChild(checker);
         updateContent();
      }
      
      override protected function render(... rest) : void {
         super.render();
         if(checker)
         {
            renderCustomContent(checker);
         }
         if(customContent && customContent.width > 0 && customContent.height > 0)
         {
            renderCustomContentBackground(customContent.getBounds(this));
         }
      }
      
      protected function renderCustomContentBackground(param1:Rectangle) : void {
         customContent.graphics.clear();
         customContent.graphics.beginFill(_checkerStyle?_checkerStyle.borderColor:getStyle("color"));
         customContent.graphics.drawRect(0,0,param1.width,param1.height);
         customContent.graphics.beginFill(_checkerStyle?_checkerStyle.backgroundColor:getStyle("backgroundColor"));
         customContent.graphics.drawRect(1,1,param1.width - 2,param1.height - 2);
      }
      
      protected function renderCustomContent(param1:Sprite) : void {
         target = param1;
         with(target.graphics)
         {
         }
         clear();
         beginFill(getStyle("borderColor"),0);
         drawRect(0,0,13,13);
         beginFill(_checkerStyle?_checkerStyle.color:getStyle("color"));
         moveTo(3,5);
         lineTo(5,7);
         lineTo(10,2);
         lineTo(11,4);
         lineTo(5,10);
         lineTo(2,7);
         }
      }
      
      public function get checked() : Boolean {
         return checker.visible;
      }
      
      public function set checked(param1:Boolean) : void {
         checker.visible = param1;
      }
      
      protected function onMouseDown(... rest) : void {
         checked = !checked;
         dispatchEvent(new Event("select"));
      }
      
      public function get checkerStyle() : Style {
         return _checkerStyle;
      }
      
      public function set checkerStyle(param1:Style) : void {
         if(_checkerStyle)
         {
            _checkerStyle.removeEventListener("update",render);
         }
         _checkerStyle = param1;
         if(_checkerStyle)
         {
            _checkerStyle.addEventListener("update",render);
         }
         render();
      }
   }
}
