package tv.ustream.gui2
{
   import tv.ustream.tools.Container;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.Event;
   import flash.geom.*;
   import tv.ustream.tween.Tween;
   import flash.utils.getQualifiedClassName;
   
   public class Gui extends Container
   {
      
      public function Gui() {
         _bg = new Sprite();
         addChildAt(new Sprite(),0);
         _type = getQualifiedClassName(this).toLowerCase();
         if(_type.indexOf("::") != -1)
         {
            _type = _type.split("::")[1];
         }
         id = _type + "." + Math.random();
         super();
      }
      
      public static var style:Style = new Style(
         {
            "background":true,
            "backgroundColor":16777215,
            "backgroundColor2":15330545,
            "backgroundAlpha":1,
            "cornerRadius":8,
            "border":1,
            "borderColor":12303291,
            "borderAlpha":1,
            "bold":false,
            "color":1925311,
            "size":13,
            "bold":true,
            "font":"Arial",
            "tint":4563455
         });
      
      protected static function isValue(param1:Style, param2:String) : Boolean {
         if(param1 && !(param1[param2] == undefined))
         {
            if(param1[param2] is Boolean && !param1.getBool(param2))
            {
               return false;
            }
            if(param1[param2] is Number && (isNaN(param1[param2])))
            {
               return false;
            }
            if(param1[param2] is String && !param1[param2])
            {
               return false;
            }
            return true;
         }
         return false;
      }
      
      protected var id:String;
      
      private var _type:String;
      
      protected var _bg:Sprite;
      
      protected var _style:Style;
      
      private var blinkObj:Sprite;
      
      protected var blinkTo:Number = 1;
      
      protected var blinkFinish:Number;
      
      protected var blinkCounter:uint;
      
      protected var targetPercent:Number;
      
      protected var _percent:Number;
      
      protected var _tooltip:Tooltip;
      
      public var tooltipOffsetX:Number = 0;
      
      public var tooltipOffsetY:Number = -8;
      
      override protected function init() : void {
         _height = 30;
         super.init();
         Gui.style.addEventListener("update",render);
         if(Gui.style[_type])
         {
            Gui.style[_type].addEventListener("update",render);
         }
      }
      
      protected function getStyle(param1:String) : * {
         return isValue(_style,param1)?_style[param1]:isValue(Gui.style[_type],param1)?Gui.style[_type][param1]:Gui.style[param1];
      }
      
      override protected function render(... rest) : void {
         var _loc6_:* = null;
         super.render();
         var _loc2_:Number = getStyle("border");
         var _loc3_:Number = getStyle("cornerRadius");
         var _loc7_:Array = getStyle("corner");
         var _loc4_:Number = _loc2_?_width - _loc2_ * 2:_width;
         var _loc5_:Number = _loc2_?_height - _loc2_ * 2:_height;
         bg.graphics.clear();
         if(_loc2_ && getStyle("borderAlpha"))
         {
            bg.graphics.beginFill(getStyle("borderColor"),getStyle("borderAlpha"));
            if(_loc7_ && _loc7_.length == 4)
            {
               bg.graphics.drawRoundRectComplex(0,0,_width,_height,_loc7_[0],_loc7_[1],_loc7_[2],_loc7_[3]);
            }
            else if(_loc3_)
            {
               bg.graphics.drawRoundRect(0,0,_width,_height,_loc3_,_loc3_);
            }
            else
            {
               bg.graphics.drawRect(0,0,_width,_height);
            }
            
         }
         if(getStyle("background"))
         {
            if(getStyle("backgroundAlpha") != 1)
            {
               draw(_loc2_,_loc3_,_loc7_,_loc4_,_loc5_);
            }
            if(getStyle("backgroundColor2") == -1)
            {
               bg.graphics.beginFill(getStyle("backgroundColor"),getStyle("backgroundAlpha"));
            }
            else
            {
               _loc6_ = new Matrix();
               _loc6_.createGradientBox(_loc4_,_loc5_,3.141592653589793 / 2);
               bg.graphics.beginGradientFill("linear",[getStyle("backgroundColor"),getStyle("backgroundColor2")],[getStyle("backgroundAlpha"),getStyle("backgroundAlpha")],[0,255],_loc6_,"pad","rgb");
            }
         }
         draw(_loc2_,_loc3_,_loc7_,_loc4_,_loc5_);
      }
      
      private function draw(param1:Number, param2:Number, param3:Array, param4:Number, param5:Number) : void {
         if(param3 && param3.length == 4)
         {
            bg.graphics.drawRoundRectComplex(param1,param1,param4,param5,param3[0],param3[1],param3[2],param3[3]);
         }
         else if(param2)
         {
            bg.graphics.drawRoundRect(param1,param1,param4,param5,param2,param2);
         }
         else
         {
            bg.graphics.drawRect(param1,param1,param4,param5);
         }
         
      }
      
      public function get style() : Style {
         return _style;
      }
      
      public function set style(param1:Style) : void {
         if(_style)
         {
            _style.removeEventListener("update",render);
         }
         _style = param1;
         if(_style)
         {
            _style.addEventListener("update",render);
         }
         render();
      }
      
      public function get bg() : Sprite {
         return _bg;
      }
      
      public function get type() : String {
         return _type;
      }
      
      public function get tooltip() : String {
         return _tooltip?_tooltip.label:"";
      }
      
      public function set tooltip(param1:String) : void {
         if(!_tooltip)
         {
            _tooltip = new Tooltip();
         }
         _tooltip.label = param1;
         addEventListener("mouseOver",onMouseOver);
         addEventListener("mouseOut",onMouseOut);
         if(stage)
         {
            onAddedToStage();
         }
         else
         {
            addEventListener("addedToStage",onAddedToStage);
         }
      }
      
      private function onAddedToStage(... rest) : void {
         if(_tooltip)
         {
            stage.addChild(_tooltip);
         }
      }
      
      protected function onMouseOver(param1:MouseEvent) : void {
         onMouseMove(param1);
         _tooltip.visible = true;
         addEventListener("mouseMove",onMouseMove);
      }
      
      public function blinkTooltip(param1:uint = 3, param2:uint = 1) : void {
         var _loc3_:* = null;
         if(_tooltip)
         {
            _loc3_ = localToGlobal(new Point(_width / 2,_height / 2));
            setTooltipPosition(_loc3_.x,_loc3_.y);
            _tooltip.sustain = param1;
            _tooltip.decay = param2;
            _tooltip.blink();
            addEventListener("enterFrame",onBlinkEnterFrame);
         }
      }
      
      private function onBlinkEnterFrame(param1:Event) : void {
         onMouseMove();
         if(!_tooltip.visible)
         {
            removeEventListener("enterFrame",onBlinkEnterFrame);
         }
      }
      
      protected function onMouseMove(param1:MouseEvent = null) : void {
         var _loc2_:Point = localToGlobal(new Point(_width / 2,_height / 2));
         setTooltipPosition(_loc2_.x,_loc2_.y);
         if(param1)
         {
            param1.updateAfterEvent();
         }
      }
      
      protected function setTooltipPosition(param1:Number, param2:Number) : void {
         if(!stage)
         {
            return;
         }
         _tooltip.x = param1 + tooltipOffsetX;
         _tooltip.y = param2 + tooltipOffsetY;
         _tooltip.pinAlignVertical = "bottom";
         var _loc3_:Rectangle = _tooltip.getBounds(stage);
         if(!_loc3_)
         {
            return;
         }
         while(_loc3_.x + _loc3_.width > stage.stageWidth && !(_tooltip.pinAlignHorizontal == "right"))
         {
            _tooltip.pinAlignHorizontal = _tooltip.pinAlignHorizontal == "left"?"center":"right";
            _loc3_ = _tooltip.getBounds(stage);
         }
         while(_loc3_.x < 0 && !(_tooltip.pinAlignHorizontal == "left"))
         {
            _tooltip.pinAlignHorizontal = _tooltip.pinAlignHorizontal == "right"?"center":"left";
            _loc3_ = _tooltip.getBounds(stage);
         }
         if(_loc3_.y < 0)
         {
            _tooltip.pinAlignVertical = "top";
            _tooltip.y = param2 - tooltipOffsetY;
         }
      }
      
      protected function onMouseOut(... rest) : void {
         removeEventListener("mouseMove",onMouseMove);
         _tooltip.visible = false;
      }
      
      public function blink(param1:Number = 1, param2:int = -1, param3:int = -1) : void {
         repeatCount = param1;
         color = param2;
         color2 = param3;
         var b:Number = getStyle("border");
         var c:Number = getStyle("cornerRadius");
         var ca:Array = getStyle("corner");
         var w:Number = b?_width - b * 2:_width;
         var h:Number = b?_height - b * 2:_height;
         if(blinkObj)
         {
            if(contains(blinkObj))
            {
               removeChild(blinkObj);
            }
            blinkObj = null;
         }
         blinkObj = new Sprite();
         with(new Sprite())
         {
         }
         if(color2 > -1)
         {
            matrix = new Matrix();
            matrix.createGradientBox(w,h,Math.PI / 2);
            blinkObj.graphics.beginGradientFill("linear",[color,color2],[1,1],[0,255],matrix,"pad","rgb");
         }
         else
         {
            blinkObj.graphics.beginFill(color > -1?color:getStyle("borderColor"),1);
         }
         if(ca && ca.length == 4)
         {
            blinkObj.graphics.drawRoundRectComplex(b,b,w,h,ca[0],ca[1],ca[2],ca[3]);
         }
         else if(c)
         {
            blinkObj.graphics.drawRoundRect(b,b,w,h,c,c);
         }
         else
         {
            blinkObj.graphics.drawRect(b,b,w,h);
         }
         
         graphics.endFill();
         alpha = 0;
         }
         addChild(blinkObj);
         setChildIndex(blinkObj,1);
         _percent = 0;
         blinkCounter = 0;
         blinkFinish = repeatCount;
         targetPercent = blinkTo;
         Tween.to(this,{"percent":blinkTo},15,"easeInOutCubic");
      }
      
      public function get percent() : Number {
         return _percent;
      }
      
      public function set percent(param1:Number) : void {
         _percent = param1;
         blinkObj.alpha = param1;
         if(param1 == targetPercent)
         {
            if(param1 == 0)
            {
               blinkCounter = blinkCounter + 1;
            }
            if(blinkCounter != blinkFinish)
            {
               targetPercent = param1 == 0?blinkTo:0.0;
               Tween.to(this,{"percent":(param1 == 0?blinkTo:0.0)},15,"easeInOutCubic");
            }
            else
            {
               removeChild(blinkObj);
               blinkObj = null;
            }
         }
      }
      
      override public function set x(param1:Number) : void {
         if(x != param1)
         {
            .super.x = param1;
            if(_tooltip && stage)
            {
               onMouseMove();
            }
         }
      }
      
      override public function set y(param1:Number) : void {
         if(y != param1)
         {
            .super.y = param1;
            if(_tooltip && stage)
            {
               onMouseMove();
            }
         }
      }
      
      override public function set width(param1:Number) : void {
         if(width != param1)
         {
            .super.width = param1;
            if(_tooltip && stage)
            {
               onMouseMove();
            }
         }
      }
      
      override public function set height(param1:Number) : void {
         if(height != param1)
         {
            .super.height = param1;
            if(_tooltip && stage)
            {
               onMouseMove();
            }
         }
      }
   }
}
