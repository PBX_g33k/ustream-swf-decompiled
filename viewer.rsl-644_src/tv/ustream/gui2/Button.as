package tv.ustream.gui2
{
   public class Button extends Label
   {
      
      public function Button(param1:String = null, param2:Boolean = true) {
         buttonMode = true;
         mouseChildren = false;
         tabEnabled = false;
         tabChildren = false;
         super(param1);
         if(param2)
         {
            addEventListener("rollOver",onRollOver);
            addEventListener("rollOut",onRollOut);
         }
      }
      
      protected static var _style:Object = {};
      
      public static function get style() : Style {
         if(!Gui.style["button"])
         {
            Gui.style["button"] = new Style(_style);
         }
         return Gui.style.button;
      }
      
      override protected function init() : void {
         super.init();
         originStyle = Button.style;
      }
      
      protected function onRollOver(... rest) : void {
         this._style = (this._style?this._style.clone():Button.style.clone()).config(
            {
               "borderColor":getStyle("color"),
               "borderAlpha":1
            });
         render();
      }
      
      protected function onRollOut(... rest) : void {
         _style = originStyle;
         render();
      }
      
      override public function get mouseEnabled() : Boolean {
         return super.mouseEnabled;
      }
      
      override public function set mouseEnabled(param1:Boolean) : void {
         if(super.mouseEnabled != param1)
         {
            .super.mouseEnabled = param1;
            useHandCursor = param1;
         }
      }
      
      public function set toggle(param1:Boolean) : void {
      }
   }
}
