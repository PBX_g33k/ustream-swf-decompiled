package tv.ustream.gui2
{
   import tv.ustream.tools.ScrollBar;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.Event;
   import flash.geom.Rectangle;
   import flash.geom.Point;
   import tv.ustream.tools.StringUtils;
   import tv.ustream.tools.DynamicEvent;
   import flash.filters.DropShadowFilter;
   import flash.filters.GlowFilter;
   
   public class Slider extends CheckBox
   {
      
      public function Slider(param1:String = null, param2:Sprite = null) {
         if(param2)
         {
            thumb = param2;
            _customThumb = true;
         }
         super(param1);
         buttonMode = false;
         mouseChildren = true;
         removeEventListener("mouseDown",onMouseDown);
         if(!_customThumb)
         {
            thumb = new Sprite();
            thumb.alpha = 1;
            drawScrollThumb();
         }
         thumb.name = "thumb";
         var _loc3_:* = true;
         thumb.useHandCursor = _loc3_;
         thumb.buttonMode = _loc3_;
         customContentBg = new Sprite();
         _customContent.addChild(new Sprite()).name = "customContentBg";
         bufferBg = new Sprite();
         _customContent.addChild(new Sprite());
         scrollBarBg = new Sprite();
         _customContent.addChild(new Sprite()).name = "scrollBarBg";
         _scrollBar = new ScrollBar(thumb);
         _customContent.addChild(new ScrollBar(thumb));
         _customContent.addChild(thumb);
         thumb.addEventListener("mouseOver",onThumbOver);
         thumb.addEventListener("mouseOut",onThumbOut);
         _scrollBar.addEventListener("yPercent",updateScrollBarBg);
         _scrollBar.addEventListener("xPercent",updateScrollBarBg);
         _scrollBar.lockDrag = true;
         _scrollBar.width = _vertical?1:_width;
         _scrollBar.height = _vertical?_height:1.0;
         _scrollBar.x = thumb.getBounds(thumb).x;
         _scrollBar.y = thumb.getBounds(thumb).y;
         _customContent.name = "customContent";
         _customContent.addEventListener("mouseDown",onScrollClick);
         customContentBg.filters = [new DropShadowFilter(1,90,0,0.35,3,3,1,2,true),new GlowFilter(16777215,1,1.1,1.1,10,2)];
         scrollBarBg.filters = [new DropShadowFilter(3,270,0,0.3,4,4,1,2,true),new DropShadowFilter(0,0,0,0.4,2,2,1,2,true)];
      }
      
      protected static var _style:Object = 
         {
            "backgroundAlpha":1,
            "backgroundColor":16777215,
            "bold":false,
            "background":false,
            "border":0,
            "borderColor":0,
            "cornerRadius":0
         };
      
      public static function get style() : Style {
         if(!Gui.style["slider"])
         {
            Gui.style["slider"] = new Style(_style);
         }
         return Gui.style.slider;
      }
      
      private var _scrollAutoSize:Boolean = true;
      
      private var _scrollBar:ScrollBar;
      
      private var scrollBarBg:Sprite;
      
      private var bufferBg:Sprite;
      
      protected var customContentBg:Sprite;
      
      protected var thumb:Sprite;
      
      private var barHeight:Number = 12;
      
      private var defaultScrollWidth:uint = 200;
      
      private var gfx:Sprite;
      
      private var _vertical:Boolean = false;
      
      protected var _customThumb:Boolean = false;
      
      protected var _tooltipText:String;
      
      protected var _isTooltipTimeFormat:Boolean = true;
      
      private var _duration:Number = 0;
      
      public var drawBuffer:Boolean = false;
      
      private var _bufferStart:Number = 0;
      
      private var _bufferPercent:Number = 0;
      
      protected var isOverThumb:Boolean = false;
      
      protected var isOverComment:CommentMarker;
      
      public var liveThreshold:Number = 10;
      
      private var _intervalStart:Number = 0;
      
      private var _intervalEnd:Number = 1;
      
      private var _intervalEditable:Boolean = false;
      
      private var _intervalLock:Boolean = false;
      
      private var _intervalMinimum:Number = 0;
      
      private var _intervalMinimumSeconds:Number = 0;
      
      private var _comments:Array;
      
      private var startMarker:Tooltip;
      
      private var endMarker:Tooltip;
      
      private var draggedMarker:Tooltip;
      
      private var w84duration:Boolean = false;
      
      private function onThumbOut(param1:MouseEvent) : void {
         isOverThumb = false;
      }
      
      private function onThumbOver(param1:MouseEvent) : void {
         isOverThumb = true;
      }
      
      override protected function onMouseOver(param1:MouseEvent) : void {
         if(!_intervalEditable && startMarker && endMarker && (_intervalStart > 0 || _intervalEnd < 1))
         {
            startMarker.visible = true;
            endMarker.visible = true;
         }
         if(!(param1.target is Tooltip))
         {
            super.onMouseOver(param1);
         }
      }
      
      override protected function onMouseOut(... rest) : void {
         if(!_intervalEditable && startMarker && endMarker && (_intervalStart > 0 || _intervalEnd < 1))
         {
            startMarker.blink();
            endMarker.blink();
         }
         super.onMouseOut(rest);
      }
      
      protected function renderBackground() : void {
         if(thumb && !_customThumb)
         {
            drawScrollThumb();
         }
         if(_scrollBar)
         {
            renderCustomContentBackground(_scrollBar.getBounds(_scrollBar));
         }
      }
      
      private function drawScrollThumb() : void {
         thumb.graphics.clear();
         thumb.graphics.beginFill(getStyle("borderColor"));
         thumb.graphics.drawCircle(0,0,6);
         thumb.graphics.beginFill(getStyle("backgroundColor"));
         thumb.graphics.drawCircle(0,0,5);
      }
      
      override protected function renderCustomContent(param1:Sprite) : void {
      }
      
      private function onScrollClick(param1:MouseEvent) : void {
         var _loc2_:* = NaN;
         if(_vertical)
         {
            _loc2_ = customContentBg.mouseY / (customContentBg.height - thumb.height + 5);
         }
         else
         {
            _loc2_ = customContentBg.mouseX / (customContentBg.width - thumb.width + 5);
         }
         _loc2_ = Math.max(0,Math.min(1,_loc2_));
         if(_vertical)
         {
            _scrollBar.yPercent = _loc2_;
         }
         else
         {
            _scrollBar.xPercent = _loc2_;
         }
         _scrollBar.drag.dispatchEvent(new Event("set"));
      }
      
      override protected function onMouseMove(param1:MouseEvent = null) : void {
         var _loc2_:* = null;
         refreshTooltip();
         if(isOverThumb)
         {
            _loc2_ = thumb.getBounds(stage);
            setTooltipPosition(_loc2_.x + _loc2_.width / 2,_loc2_.y + _loc2_.height / 2);
         }
         else if(isOverComment && isOverComment is CommentMarker)
         {
            _loc2_ = isOverComment.getBounds(stage);
            setTooltipPosition(_loc2_.x + _loc2_.width / 2,_loc2_.y + _loc2_.height / 2);
         }
         else
         {
            setTooltipPosition(stage.mouseX,localToGlobal(new Point(0,_height / 2)).y);
         }
         
         if(param1)
         {
            param1.updateAfterEvent();
         }
      }
      
      override public function set tooltip(param1:String) : void {
         _tooltipText = param1;
         onMouseMove(new MouseEvent("mouseMove"));
      }
      
      protected function refreshTooltip() : void {
         var _loc5_:* = NaN;
         var _loc2_:* = null;
         var _loc3_:* = NaN;
         var _loc1_:* = null;
         var _loc4_:* = null;
         if(customContentBg.mouseX <= customContentBg.width && (_isTooltipTimeFormat) && !isOverComment)
         {
            if(isOverThumb)
            {
               _loc5_ = Math.abs(_duration) * progress;
            }
            else
            {
               _loc1_ = scrollBar.getBounds(customContentBg);
               _loc1_.x = thumb.width / 2;
               _loc1_.width = _loc1_.width - thumb.width;
               _loc3_ = customContentBg.mouseX / _loc1_.width;
               _loc3_ = Math.max(0,Math.min(1,_loc3_));
               if(_intervalLock)
               {
                  _loc3_ = _loc3_ * (_intervalEnd - _intervalStart) + _intervalStart;
               }
               _loc5_ = Math.abs(_duration) * _loc3_;
            }
            if(_duration < 0)
            {
               _loc5_ = Math.abs(_duration) - _loc5_;
            }
            if(_duration < 0 && _loc5_ < liveThreshold)
            {
               .super.tooltip = "Live";
            }
            else
            {
               _loc2_ = new Date();
               _loc2_.setTime(_loc5_ * 1000);
               _loc2_.setHours(_loc2_.getUTCHours());
               .super.tooltip = (_duration < 0?"-":"") + StringUtils.getFormatDateString(_loc5_ < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc2_);
            }
         }
         else if(isOverComment && isOverComment is CommentMarker)
         {
            _loc4_ = isOverComment as CommentMarker;
            _loc5_ = Math.abs(_duration) * _loc4_.to;
            _loc2_ = new Date();
            _loc2_.setHours(_loc2_.getUTCHours());
            _loc2_.setTime(_loc5_ * 1000);
            .super.tooltip = _loc4_.text + "\nBy: " + _loc4_.from + "\n" + StringUtils.getFormatDateString(_loc5_ < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc2_);
         }
         else
         {
            .super.tooltip = _tooltipText;
         }
         
         _tooltip.pinAlignHorizontal = "center";
         _tooltip.textAlign = "center";
      }
      
      override protected function renderCustomContentBackground(param1:Rectangle) : void {
         customContentBg.graphics.clear();
         customContentBg.graphics.beginFill(getStyle("backgroundColor2"),1);
         if(_vertical)
         {
            customContentBg.graphics.drawRoundRect(2 - barHeight / 2,1 - thumb.height / 2,barHeight - 4,param1.height - 2,getStyle("cornerRadius"),getStyle("cornerRadius"));
         }
         else
         {
            customContentBg.graphics.drawRoundRect(1 - barHeight / 2,2 - barHeight / 2,param1.width - 2 - 3,barHeight - 4,getStyle("cornerRadius"),getStyle("cornerRadius"));
         }
         updateScrollBarBg(param1);
      }
      
      private function updateScrollBarBg(... rest) : void {
         var _loc2_:* = null;
         var _loc3_:Number = _bufferStart;
         var _loc4_:Number = _bufferStart + _bufferPercent * (1 - _bufferStart);
         if(_intervalLock)
         {
            _loc3_ = Math.max(0,Math.min(1,(_loc3_ - _intervalStart) / (_intervalEnd - _intervalStart)));
            _loc4_ = Math.max(0,Math.min(1,(_loc4_ - _intervalStart) / (_intervalEnd - _intervalStart)));
         }
         if(scrollBarBg)
         {
            _loc2_ = customContentBg.getBounds(scrollBarBg);
            scrollBarBg.graphics.clear();
            scrollBarBg.graphics.beginFill(getStyle("tint"));
            if(_vertical)
            {
               _loc2_.y = _loc2_.y + _loc2_.height * _scrollBar.yPercent;
               _loc2_.height = _loc2_.height - _loc2_.height * _scrollBar.yPercent;
            }
            else if(drawBuffer)
            {
               _loc2_.x = _loc2_.x + _loc2_.width * _loc3_;
               _loc2_.width = _loc2_.width * (scrollBar.xPercent - _loc3_);
            }
            else
            {
               _loc2_.width = _loc2_.width * _scrollBar.xPercent;
            }
            
            scrollBarBg.graphics.drawRoundRect(_loc2_.x,_loc2_.y,_loc2_.width,_loc2_.height,getStyle("cornerRadius"),getStyle("cornerRadius"));
            scrollBarBg.graphics.endFill();
         }
         if(bufferBg)
         {
            _loc2_ = customContentBg.getBounds(bufferBg);
            bufferBg.graphics.clear();
            if(drawBuffer)
            {
               bufferBg.graphics.beginFill(getStyle("tint"),0.5);
               if(!_vertical)
               {
                  _loc2_.x = _loc2_.x + _loc2_.width * _loc3_;
                  _loc2_.width = _loc2_.width * (_loc4_ - _loc3_);
               }
               bufferBg.graphics.drawRoundRect(_loc2_.x,_loc2_.y,_loc2_.width,_loc2_.height,getStyle("cornerRadius"),getStyle("cornerRadius"));
               bufferBg.graphics.endFill();
            }
         }
         positionMarkers();
      }
      
      private function checkInterval(... rest) : void {
         if(_scrollBar.xPercent < _intervalStart)
         {
            _scrollBar.xPercent = _intervalStart;
         }
         if(_scrollBar.xPercent > _intervalEnd)
         {
            _scrollBar.xPercent = _intervalEnd;
         }
      }
      
      public function get scrollBar() : ScrollBar {
         return _scrollBar;
      }
      
      public function get scrollWidth() : Number {
         return scrollBar.width;
      }
      
      public function set scrollWidth(param1:Number) : void {
         if(!autoSize && param1 + (content.width - customContent.width) > _width)
         {
            param1 = _width - (content.width - customContent.width + 2 * _margin);
         }
         _scrollBar.width = param1;
         renderCustomContentBackground(scrollBar.getBounds(scrollBar));
         updateContent();
         render();
      }
      
      public function get scrollAutoSize() : Boolean {
         return _scrollAutoSize;
      }
      
      public function set scrollAutoSize(param1:Boolean) : void {
         _scrollAutoSize = param1;
         if(_scrollAutoSize && !autoSize)
         {
            _scrollBar.width = _width - (labelTf.width + 15 + 2 * _margin);
         }
         else
         {
            _scrollBar.width = defaultScrollWidth;
         }
         renderBackground();
         updateContent();
         render();
      }
      
      override public function set label(param1:String) : void {
         var _loc2_:* = !(labelTf.text.length == param1.length);
         .super.label = param1;
         if(_loc2_)
         {
            width = width;
         }
      }
      
      override public function set width(param1:Number) : void {
         if(autoSize)
         {
            autoSize = false;
         }
         .super.width = param1;
         var _loc2_:Number = param1 - ((label != ""?labelTf.width + 15:0.0) + _margin * 2);
         if(icon)
         {
            _loc2_ = _loc2_ - (icon.width + 5);
         }
         if(!scrollAutoSize && param1 - (labelTf.width + 5 + (_margin) * 2) > defaultScrollWidth)
         {
            _loc2_ = defaultScrollWidth;
         }
         _scrollBar.width = _vertical?1:_loc2_;
         renderBackground();
         updateContent();
         render();
      }
      
      override public function set height(param1:Number) : void {
         .super.height = param1;
         _scrollBar.height = _vertical?param1 - 10 - (thumb.height - barHeight):1.0;
         renderBackground();
         updateContent();
         render();
      }
      
      override public function set mouseEnabled(param1:Boolean) : void {
         mouseChildren = param1;
         .super.mouseEnabled = param1;
      }
      
      public function get bufferPercent() : Number {
         return _bufferPercent;
      }
      
      public function set bufferPercent(param1:Number) : void {
         _bufferPercent = param1;
         updateScrollBarBg();
      }
      
      public function get bufferStart() : Number {
         return _bufferStart;
      }
      
      public function set bufferStart(param1:Number) : void {
         _bufferStart = param1;
         updateScrollBarBg();
      }
      
      public function get progress() : Number {
         var _loc1_:Number = _vertical?_scrollBar.yPercent:_scrollBar.xPercent;
         if(_intervalLock)
         {
            return intervalStart + _loc1_ * (intervalEnd - intervalStart);
         }
         return _loc1_;
      }
      
      public function set progress(param1:Number) : void {
         var _loc2_:* = param1;
         if(_intervalLock)
         {
            _loc2_ = (_loc2_ - _intervalStart) / (_intervalEnd - _intervalStart);
         }
         if(_vertical)
         {
            _scrollBar.yPercent = _loc2_;
         }
         else
         {
            _scrollBar.xPercent = _loc2_;
         }
      }
      
      public function set vertical(param1:Boolean) : void {
         _vertical = param1;
         if(param1)
         {
            _margin = 0;
            autoSize = false;
            scrollAutoSize = false;
         }
      }
      
      override protected function init() : void {
         super.init();
      }
      
      public function get isTooltipTimeFormat() : Boolean {
         return _isTooltipTimeFormat;
      }
      
      public function set isTooltipTimeFormat(param1:Boolean) : void {
         _isTooltipTimeFormat = param1;
      }
      
      public function get intervalStart() : Number {
         return _intervalStart;
      }
      
      public function set intervalStart(param1:Number) : void {
         _intervalStart = Math.max(0,Math.min(_intervalEnd,param1));
         if(_intervalStart)
         {
            createMarkers();
         }
         if((_intervalStart > 0 || _intervalEnd < 1) && !intervalEditable)
         {
            if(_duration)
            {
               startMarker.blink();
               endMarker.blink();
            }
            else
            {
               _loc2_ = false;
               endMarker.visible = _loc2_;
               startMarker.visible = _loc2_;
               w84duration = true;
            }
         }
         else
         {
            if(startMarker)
            {
               startMarker.visible = intervalEditable;
            }
            if(endMarker)
            {
               endMarker.visible = intervalEditable;
            }
         }
         updateScrollBarBg();
      }
      
      public function get intervalEnd() : Number {
         return _intervalEnd;
      }
      
      public function set intervalEnd(param1:Number) : void {
         _intervalEnd = Math.max(intervalStart,Math.min(1,param1));
         if(_intervalEnd < 1)
         {
            createMarkers();
         }
         if((_intervalStart > 0 || _intervalEnd < 1) && !intervalEditable)
         {
            if(_duration)
            {
               startMarker.blink();
               endMarker.blink();
            }
            else
            {
               _loc2_ = false;
               endMarker.visible = _loc2_;
               startMarker.visible = _loc2_;
               w84duration = true;
            }
         }
         else
         {
            if(startMarker)
            {
               startMarker.visible = intervalEditable;
            }
            if(endMarker)
            {
               endMarker.visible = intervalEditable;
            }
         }
         updateScrollBarBg();
      }
      
      public function get intervalEditable() : Boolean {
         return _intervalEditable;
      }
      
      public function set intervalEditable(param1:Boolean) : void {
         _intervalEditable = param1;
         if(_intervalEditable)
         {
            createMarkers();
         }
         if(startMarker)
         {
            _loc2_ = _intervalEditable;
            startMarker.buttonMode = _loc2_;
            startMarker.visible = _loc2_;
         }
         if(endMarker)
         {
            _loc2_ = _intervalEditable;
            endMarker.buttonMode = _loc2_;
            endMarker.visible = _loc2_;
         }
         if(draggedMarker)
         {
            draggedMarker.stopDrag();
            draggedMarker = null;
         }
         positionMarkers();
         stage.removeEventListener("mouseUp",onMarkerUp);
         stage.removeEventListener("mouseMove",onMarkerMove);
      }
      
      public function get intervalLock() : Boolean {
         return _intervalLock;
      }
      
      public function set intervalLock(param1:Boolean) : void {
         _intervalLock = param1;
         positionMarkers();
      }
      
      public function get intervalMinimum() : Number {
         return _intervalMinimum;
      }
      
      public function set intervalMinimum(param1:Number) : void {
         _intervalMinimum = param1;
         if(_duration)
         {
            _intervalMinimumSeconds = _intervalMinimum * _duration;
         }
      }
      
      public function get intervalMinimumSeconds() : Number {
         return _intervalMinimumSeconds;
      }
      
      public function set intervalMinimumSeconds(param1:Number) : void {
         _intervalMinimumSeconds = param1;
         if(_duration)
         {
            intervalMinimum = param1 / _duration;
         }
      }
      
      public function get duration() : Number {
         return _duration;
      }
      
      public function set duration(param1:Number) : void {
         _duration = param1;
         if(_intervalMinimumSeconds)
         {
            intervalMinimum = _intervalMinimumSeconds / _duration;
         }
         if((_intervalStart > 0 || _intervalEnd < 1) && (w84duration) && _duration)
         {
            if(!intervalEditable)
            {
               startMarker.blink();
               endMarker.blink();
            }
            w84duration = false;
         }
      }
      
      public function get comments() : Array {
         return _comments;
      }
      
      public function set comments(param1:Array) : void {
         var _loc2_:* = 0;
         var _loc3_:* = null;
         if(_comments)
         {
            while(_comments.length)
            {
               _loc3_ = _comments.pop() as CommentMarker;
               if(_loc3_.parent)
               {
                  _loc3_.parent.removeChild(_loc3_);
               }
               _loc3_.removeEventListener("click",onMarkerClick);
            }
            removeEventListener("mouseOver",onMarkerOver);
         }
         if(param1 && param1.length)
         {
            _comments = [];
            addEventListener("mouseOver",onMarkerOver);
            _loc2_ = 0;
            while(_loc2_ < param1.length)
            {
               _loc3_ = new CommentMarker(param1[_loc2_]);
               _loc3_.addEventListener("click",onMarkerClick);
               _customContent.addChild(_loc3_);
               _comments.push(_loc3_);
               _loc2_++;
            }
            _customContent.setChildIndex(thumb,_customContent.numChildren - 1);
         }
         positionMarkers();
      }
      
      private function onMarkerClick(param1:MouseEvent) : void {
      }
      
      private function onMarkerOver(param1:MouseEvent) : void {
         if(param1.target is CommentMarker)
         {
            isOverComment = param1.target as CommentMarker;
            isOverComment.addEventListener("mouseOut",onMarkerOut);
         }
      }
      
      private function onMarkerOut(param1:MouseEvent) : void {
         if(param1.target is CommentMarker && isOverComment == param1.target)
         {
            isOverComment.removeEventListener("mouseOut",onMarkerOut);
            isOverComment = null;
         }
      }
      
      override public function resize(param1:Number, param2:Number) : void {
         if(autoSize)
         {
            autoSize = false;
         }
         var _loc3_:Number = param1 - ((label != ""?labelTf.width + 15:4.0) + _margin * 2);
         if(icon)
         {
            _loc3_ = _loc3_ - (icon.width + 5);
         }
         if(!scrollAutoSize && param1 - (labelTf.width + 5 + _margin * 2) > defaultScrollWidth)
         {
            _loc3_ = defaultScrollWidth;
         }
         _scrollBar.width = _vertical?1:_loc3_;
         _scrollBar.height = _vertical?param2 - 10 - (thumb.height - barHeight):1.0;
         renderBackground();
         updateContent();
         super.resize(param1,param2);
      }
      
      private function positionMarkers() : void {
         var _loc2_:* = 0;
         var _loc3_:* = null;
         var _loc1_:Rectangle = customContentBg.getBounds(this);
         _loc1_.x = _loc1_.x + thumb.width / 2;
         _loc1_.width = _loc1_.width - thumb.width;
         if(startMarker)
         {
            if(startMarker != draggedMarker)
            {
               startMarker.x = _loc1_.x + (_intervalLock?0:_intervalStart) * _loc1_.width;
            }
            startMarker.y = 0;
         }
         if(endMarker)
         {
            if(endMarker != draggedMarker)
            {
               endMarker.x = _loc1_.x + (_intervalLock?1:_intervalEnd) * _loc1_.width;
            }
            endMarker.y = 0;
         }
         if(_comments && _comments.length)
         {
            _loc1_ = customContentBg.getBounds(_customContent);
            _loc1_.x = scrollBar.x + thumb.width / 2;
            _loc1_.width = scrollBar.width - thumb.width;
            _loc2_ = 0;
            while(_loc2_ < _comments.length)
            {
               _loc3_ = _comments[_loc2_];
               _loc3_.x = Math.floor(_loc1_.x + _loc3_.to * _loc1_.width);
               _loc3_.y = Math.floor(_loc1_.y + (_loc1_.height - _loc3_.height) / 2);
               _loc3_.visible = !_intervalLock;
               _loc2_++;
            }
         }
         updateMarkerLabels();
      }
      
      private function updateMarkerLabels() : void {
         var _loc2_:* = NaN;
         var _loc1_:* = null;
         if(startMarker)
         {
            _loc2_ = Math.round(Math.abs(_duration) * _intervalStart);
            _loc2_ = Math.min(Math.abs(_duration),Math.max(0,_loc2_));
            _loc1_ = new Date();
            _loc1_.setTime(_loc2_ * 1000);
            _loc1_.setHours(_loc1_.getUTCHours());
            startMarker.label = StringUtils.getFormatDateString(_loc2_ < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc1_);
         }
         if(endMarker)
         {
            _loc2_ = Math.round(Math.abs(_duration) * _intervalEnd);
            _loc2_ = Math.min(Math.abs(_duration),Math.max(0,_loc2_));
            _loc1_ = new Date();
            _loc1_.setTime(_loc2_ * 1000);
            _loc1_.setHours(_loc1_.getUTCHours());
            endMarker.label = StringUtils.getFormatDateString(_loc2_ < 3600?"%MINUTES%:%SECONDS%":"%HOUR%:%MINUTES%:%SECONDS%",_loc1_);
         }
      }
      
      private function createMarkers() : void {
         if(!startMarker)
         {
            startMarker = new Tooltip();
            addChild(new Tooltip());
            startMarker.pinAlignHorizontal = "right";
            startMarker.mouseEnabled = true;
            startMarker.buttonMode = _intervalEditable;
            startMarker.addEventListener("mouseDown",onMarkerDown);
         }
         if(!endMarker)
         {
            endMarker = new Tooltip();
            addChild(new Tooltip());
            endMarker.pinAlignHorizontal = "left";
            endMarker.mouseEnabled = true;
            endMarker.buttonMode = _intervalEditable;
            endMarker.addEventListener("mouseDown",onMarkerDown);
         }
      }
      
      private function onMarkerDown(param1:MouseEvent) : void {
         var _loc2_:* = null;
         if(_intervalEditable)
         {
            draggedMarker = param1.currentTarget as Tooltip;
            _loc2_ = customContentBg.getBounds(this);
            _loc2_.x = _loc2_.x + thumb.width / 2;
            _loc2_.width = _loc2_.width - thumb.width;
            _loc2_.y = 0;
            _loc2_.height = 0;
            _loc3_ = draggedMarker;
            if(startMarker !== _loc3_)
            {
               if(endMarker === _loc3_)
               {
                  _loc2_.x = _loc2_.x + _loc2_.width * (_intervalStart + _intervalMinimum);
                  _loc2_.width = _loc2_.width - _loc2_.width * (_intervalStart + _intervalMinimum);
               }
            }
            else
            {
               _loc2_.width = _loc2_.width * (_intervalEnd - _intervalMinimum);
            }
            draggedMarker.startDrag(false,_loc2_);
            draggedMarker.removeEventListener("mouseDown",onMarkerDown);
            stage.addEventListener("mouseUp",onMarkerUp);
            stage.addEventListener("mouseMove",onMarkerMove);
         }
      }
      
      private function onMarkerUp(param1:MouseEvent) : void {
         draggedMarker.stopDrag();
         draggedMarker.addEventListener("mouseDown",onMarkerDown);
         stage.removeEventListener("mouseUp",onMarkerUp);
         stage.removeEventListener("mouseMove",onMarkerMove);
         draggedMarker = null;
      }
      
      private function onMarkerMove(param1:MouseEvent) : void {
         var _loc3_:* = null;
         var _loc2_:* = NaN;
         if(draggedMarker)
         {
            _loc3_ = customContentBg.getBounds(this);
            _loc3_.x = _loc3_.x + thumb.width / 2;
            _loc3_.width = _loc3_.width - thumb.width;
            _loc2_ = (draggedMarker.x - _loc3_.x) / _loc3_.width;
            _loc2_ = Math.max(0,Math.min(1,_loc2_));
            _loc4_ = draggedMarker;
            if(startMarker !== _loc4_)
            {
               if(endMarker === _loc4_)
               {
                  _intervalEnd = Math.max(_intervalStart,Math.min(1,_loc2_));
                  dispatchEvent(new DynamicEvent("change",false,false,{"end":_intervalEnd}));
               }
            }
            else
            {
               _intervalStart = Math.max(0,Math.min(_intervalEnd,_loc2_));
               dispatchEvent(new DynamicEvent("change",false,false,{"start":_intervalStart}));
            }
         }
      }
   }
}
import flash.display.Sprite;
import tv.ustream.gui2.Gui;

class CommentMarker extends Sprite
{
   
   function CommentMarker(param1:Object) {
      super();
      if(Gui.style.button)
      {
         Gui.style.button.addEventListener("update",redraw);
      }
      data = param1;
      redraw();
   }
   
   private var data:Object;
   
   private var _width:Number = 1;
   
   private var _height:Number = 20;
   
   public function resize(param1:Number, param2:Number) : void {
      _width = param1;
      _height = param2;
      redraw();
   }
   
   private function redraw(... rest) : void {
      graphics.clear();
      graphics.beginFill(0,0);
      graphics.drawRect(-1,-1,_width + 2,_height + 2);
      graphics.endFill();
      graphics.beginFill(Gui.style.tint,0.5);
      graphics.drawRect(0,0,_width,_height);
      graphics.endFill();
   }
   
   public function get text() : String {
      return data && !(data.text == undefined)?data.text:"";
   }
   
   public function get to() : Number {
      return data && !(data.to == undefined)?data.to:undefined;
   }
   
   public function get from() : String {
      return data && !(data.from == undefined)?data.from:"";
   }
   
   public function get commentId() : String {
      return data && !(data.id == undefined)?data.id:"";
   }
   
   public function get start() : Number {
      return data && !(data.start == undefined)?data.start:undefined;
   }
   
   public function get length() : Number {
      return data && !(data.length == undefined)?data.length:undefined;
   }
   
   override public function get width() : Number {
      return _width;
   }
   
   override public function set width(param1:Number) : void {
      _width = param1;
      redraw();
   }
   
   override public function get height() : Number {
      return _height;
   }
   
   override public function set height(param1:Number) : void {
      _height = param1;
      redraw();
   }
}
