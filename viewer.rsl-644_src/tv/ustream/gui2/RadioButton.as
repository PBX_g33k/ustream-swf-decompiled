package tv.ustream.gui2
{
   import flash.display.Sprite;
   import flash.geom.Rectangle;
   
   public class RadioButton extends CheckBox
   {
      
      public function RadioButton(param1:String = null, param2:Boolean = false) {
         super(param1,param2);
      }
      
      protected static var _style:Object = 
         {
            "backgroundAlpha":1,
            "backgroundColor":16777215,
            "bold":false,
            "background":false,
            "border":0
         };
      
      private static var groups:Object = {};
      
      public static function get style() : Style {
         if(!Gui.style["radiobutton"])
         {
            Gui.style["radiobutton"] = new Style(_style);
         }
         return Gui.style.radiobutton;
      }
      
      private var checker:Sprite;
      
      private var _group:String;
      
      override protected function renderCustomContent(param1:Sprite) : void {
         target = param1;
         with(target.graphics)
         {
         }
         beginFill(16711680,0);
         drawRect(0,0,12,12);
         beginFill(_checkerStyle?_checkerStyle.color:getStyle("color"));
         drawCircle(6,4,2);
         }
      }
      
      override protected function renderCustomContentBackground(param1:Rectangle) : void {
         customContent.graphics.beginFill(_checkerStyle?_checkerStyle.borderColor:getStyle("color"));
         customContent.graphics.drawCircle(6,4,6);
         customContent.graphics.beginFill(_checkerStyle?_checkerStyle.backgroundColor:getStyle("backgroundColor"));
         customContent.graphics.drawCircle(6,4,5);
      }
      
      override protected function onMouseDown(... rest) : void {
         if(!checked)
         {
            checked = !checked;
         }
      }
      
      public function get group() : String {
         return _group;
      }
      
      public function set group(param1:String) : void {
         if(_group)
         {
            groups[_group][id] = null;
         }
         _group = param1;
         if(_group)
         {
            if(!groups[_group])
            {
               groups[_group] = {};
            }
            groups[_group][id] = this;
         }
      }
      
      override public function set checked(param1:Boolean) : void {
         .super.checked = param1;
         if(param1 && _group)
         {
            _loc4_ = 0;
            _loc3_ = groups[_group];
            for each(_loc2_ in groups[_group])
            {
               if(_loc2_ != this)
               {
                  _loc2_.checked = false;
               }
            }
         }
      }
      
      override protected function init() : void {
         super.init();
      }
   }
}
