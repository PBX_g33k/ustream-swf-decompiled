package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class MicrophoneIcon extends Icon
   {
      
      public function MicrophoneIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRoundRect(-3,-7,6,10,6,6);
         graphics.drawRect(-3,-4,6,1);
         graphics.drawRect(-3,-2,6,1);
         graphics.drawRect(-1,5,2,2);
         graphics.drawRect(-5,7,10,1);
         graphics.endFill();
         }
         var m:Sprite = new Sprite();
         with(addChild(m))
         {
         }
         graphics.beginFill(0,0.5);
         graphics.drawRect(-7,-2,14,8);
         graphics.endFill();
         }
         var h:Sprite = new Sprite();
         with(addChild(h))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRoundRect(-5,-9,10,14,11,11);
         graphics.drawRoundRect(-4,-8,8,12,9,9);
         graphics.endFill();
         }
         h.mask = m;
      }
   }
}
