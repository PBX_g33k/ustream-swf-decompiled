package tv.ustream.gui2.icons
{
   import flash.display.Sprite;
   
   public class OptionsIcon extends Icon
   {
      
      public function OptionsIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         var _loc2_:* = 0;
         var _loc3_:Sprite = new Sprite();
         addChild(_loc3_);
         var _loc4_:Sprite = new Sprite();
         _loc3_.addChild(_loc4_);
         _loc4_.graphics.beginFill(_color,1);
         _loc4_.graphics.drawCircle(0,0,6);
         _loc4_.graphics.drawCircle(0,0,3);
         _loc4_.graphics.endFill();
         var _loc1_:uint = 8;
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc3_.addChild(createTeeth(_loc2_)).rotation = _loc2_ * 360 / _loc1_;
            _loc2_++;
         }
      }
      
      private function createTeeth(param1:uint) : Sprite {
         var _loc2_:Sprite = new Sprite();
         _loc2_.graphics.beginFill(_color);
         _loc2_.graphics.drawRoundRect(-2,5,3,3,0,0);
         _loc2_.graphics.endFill();
         return _loc2_;
      }
   }
}
