package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class PollIcon extends Icon
   {
      
      public function PollIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRect(-8,-7,4,4);
         graphics.drawRect(-7,-6,2,2);
         graphics.drawRect(-8,-2,4,4);
         graphics.drawRect(-7,-1,2,2);
         graphics.drawRect(-8,3,4,4);
         graphics.drawRect(-7,4,2,2);
         graphics.drawRect(-2,-7,10,3);
         graphics.drawRect(-2,-2,4,3);
         graphics.drawRect(-2,3,7,3);
         graphics.endFill();
         }
      }
   }
}
