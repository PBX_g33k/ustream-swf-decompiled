package tv.ustream.gui2.icons
{
   public class MuteIcon extends Icon
   {
      
      public function MuteIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(graphics)
         {
         }
         beginFill(_color);
         moveTo(-10,-4);
         lineTo(-5,-4);
         lineTo(0,-8);
         lineTo(0,8);
         lineTo(-5,4);
         lineTo(-10,4);
         }
      }
   }
}
