package tv.ustream.gui2.icons
{
   public class VolumeIcon extends MuteIcon
   {
      
      public function VolumeIcon(param1:Number = -1) {
         super(param1);
      }
      
      private var _lines:int = 3;
      
      override protected function renderIcon() : void {
         super.renderIcon();
         with(graphics)
         {
         }
         beginFill(_color);
         if(lines)
         {
            drawRect(1,-2,1,4);
         }
         if(lines > 1)
         {
            drawRect(3,-3,1,6);
         }
         if(lines > 2)
         {
            drawRect(5,-4,1,8);
         }
         }
      }
      
      public function get lines() : int {
         return _lines;
      }
      
      public function set lines(param1:int) : void {
         _lines = param1;
         graphics.clear();
         renderIcon();
      }
   }
}
