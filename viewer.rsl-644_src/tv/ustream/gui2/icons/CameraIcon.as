package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class CameraIcon extends Icon
   {
      
      public function CameraIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRoundRect(-8,-4,10,8,3,3);
         graphics.drawRoundRect(-5,-2,4,2,2,2);
         graphics.drawRect(2,-2,2,4);
         graphics.moveTo(4,-2);
         graphics.lineTo(7,-4);
         graphics.lineTo(7,4);
         graphics.lineTo(4,2);
         graphics.drawRect(7,-4,1,8);
         graphics.endFill();
         }
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRect(-6,-6,5,2);
         graphics.endFill();
         }
      }
   }
}
