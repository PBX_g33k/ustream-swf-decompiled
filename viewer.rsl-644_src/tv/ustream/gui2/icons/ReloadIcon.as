package tv.ustream.gui2.icons
{
   import flash.display.Sprite;
   import flash.display.Graphics;
   
   public class ReloadIcon extends Icon
   {
      
      public function ReloadIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         var _loc2_:Sprite = new Sprite();
         var _loc1_:Sprite = new Sprite();
         _loc1_.graphics.beginFill(_color,1);
         _loc1_.graphics.moveTo(1,0);
         _loc1_.graphics.lineTo(10,0);
         _loc1_.graphics.lineTo(5.5,4.5);
         _loc1_.graphics.lineTo(1,0);
         _loc1_.graphics.endFill();
         _loc1_.graphics.lineStyle(2.5,_color,1);
         _loc1_.graphics.moveTo(5.5,0);
         drawCircleSegment(_loc1_.graphics,0,0,0,1.6111111111111112 * 3.141592653589793,5.5,-1);
         _loc2_.addChild(_loc1_);
         _loc1_.rotation = -40;
         addChild(_loc2_);
      }
      
      protected function drawCircleSegment(param1:Graphics, param2:Number, param3:Number, param4:Number, param5:Number, param6:Number, param7:Number) : void {
         var _loc9_:* = 0;
         var _loc10_:Number = Math.abs(param5 - param4);
         var _loc11_:Number = Math.floor(_loc10_ / (3.141592653589793 / 4)) + 1;
         var _loc12_:Number = param7 * _loc10_ / (2 * _loc11_);
         var _loc8_:Number = param6 / Math.cos(_loc12_);
         param1.moveTo(param2 + Math.cos(param4) * param6,param3 + Math.sin(param4) * param6);
         _loc9_ = 0;
         while(_loc9_ < _loc11_)
         {
            param5 = param4 + _loc12_;
            param4 = param5 + _loc12_;
            param1.curveTo(param2 + Math.cos(param5) * _loc8_,param3 + Math.sin(param5) * _loc8_,param2 + Math.cos(param4) * param6,param3 + Math.sin(param4) * param6);
            _loc9_++;
         }
      }
   }
}
