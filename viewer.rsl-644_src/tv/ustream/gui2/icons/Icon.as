package tv.ustream.gui2.icons
{
   import flash.display.Sprite;
   
   public class Icon extends Sprite
   {
      
      public function Icon(param1:Number = -1) {
         super();
         if(param1 != -1)
         {
            _color = param1;
         }
         graphics.beginFill(0,0);
         graphics.drawRect(-9,-9,18,18);
         renderIcon();
      }
      
      protected var _color:Number = 16777215;
      
      protected function renderIcon() : void {
      }
      
      public function get color() : Number {
         return _color;
      }
      
      public function set color(param1:Number) : void {
         _color = param1;
         graphics.clear();
         renderIcon();
      }
   }
}
