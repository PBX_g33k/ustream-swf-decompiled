package tv.ustream.gui2.icons
{
   public class PlayIcon extends Icon
   {
      
      public function PlayIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(graphics)
         {
         }
         beginFill(_color);
         moveTo(-6,-7);
         lineTo(6,0);
         lineTo(-6,7);
         lineTo(-6,-7);
         endFill();
         }
      }
   }
}
