package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class ShareIcon extends Icon
   {
      
      public function ShareIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawCircle(5,-5,3);
         graphics.drawCircle(-5,0,3);
         graphics.drawCircle(5,5,3);
         graphics.endFill();
         graphics.lineStyle(0,_color);
         graphics.moveTo(5,-5);
         graphics.lineTo(-5,0);
         graphics.lineTo(5,5);
         }
      }
   }
}
