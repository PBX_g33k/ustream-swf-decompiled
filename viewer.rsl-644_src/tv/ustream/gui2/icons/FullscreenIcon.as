package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class FullscreenIcon extends Icon
   {
      
      public function FullscreenIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRect(-11,-8,4,1);
         graphics.drawRect(-11,-7,1,3);
         graphics.drawRect(7,-8,4,1);
         graphics.drawRect(10,-7,1,3);
         graphics.drawRect(-11,4,1,3);
         graphics.drawRect(-11,7,4,1);
         graphics.drawRect(10,4,1,3);
         graphics.drawRect(7,7,4,1);
         graphics.drawRect(-9,-6,18,12);
         graphics.endFill();
         }
      }
   }
}
