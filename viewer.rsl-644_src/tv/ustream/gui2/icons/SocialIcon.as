package tv.ustream.gui2.icons
{
   import flash.display.*;
   
   public class SocialIcon extends Icon
   {
      
      public function SocialIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         var m:Sprite = new Sprite();
         with(addChild(m))
         {
         }
         graphics.beginFill(0,1);
         graphics.drawRect(-9,-9,18,18);
         graphics.drawRoundRect(-4,-3,8,12,6,6);
         graphics.endFill();
         }
         var b:Sprite = new Sprite();
         with(addChild(b))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawRoundRect(-8,-2,4,8,4,4);
         graphics.drawRoundRect(4,-2,4,8,4,4);
         graphics.drawCircle(-6,-4,2);
         graphics.drawCircle(6,-4,2);
         graphics.endFill();
         }
         b.mask = m;
         with(addChild(new Sprite()))
         {
         }
         graphics.beginFill(_color,1);
         graphics.drawCircle(0,-5,3);
         graphics.drawRoundRect(-3,-2,6,10,5,5);
         graphics.endFill();
         }
      }
   }
}
