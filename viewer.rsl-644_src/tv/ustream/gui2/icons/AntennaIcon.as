package tv.ustream.gui2.icons
{
   import flash.display.Sprite;
   
   public class AntennaIcon extends Icon
   {
      
      public function AntennaIcon(param1:Number = -1) {
         clr = param1;
         circles = new Sprite();
         addChild(new Sprite());
         msk = new Sprite();
         addChild(new Sprite());
         with(msk.graphics)
         {
         }
         beginFill(16711680);
         moveTo(-10,-10);
         lineTo(10,-10);
         lineTo(10,10);
         lineTo(0,0);
         lineTo(-10,10);
         lineTo(-10,-10);
         }
         circles.mask = msk;
         super(clr);
      }
      
      private var msk:Sprite;
      
      private var circles:Sprite;
      
      override protected function renderIcon() : void {
         with(circles.graphics)
         {
         }
         clear();
         lineStyle(1,_color);
         drawCircle(0,0,4);
         drawCircle(0,0,8);
         }
         with(graphics)
         {
         }
         clear();
         beginFill(_color);
         moveTo(-3,9);
         lineTo(0,0);
         lineTo(3,9);
         endFill();
         }
      }
   }
}
