package tv.ustream.gui2.icons
{
   public class PauseIcon extends Icon
   {
      
      public function PauseIcon(param1:Number = -1) {
         super(param1);
      }
      
      override protected function renderIcon() : void {
         with(graphics)
         {
         }
         beginFill(_color);
         drawRect(-4,-5,3,10);
         drawRect(1,-5,3,10);
         }
      }
   }
}
