package tv.ustream.data2.renderers
{
   import flash.display.DisplayObject;
   
   public class DataListRenderer extends DataRenderer
   {
      
      public function DataListRenderer() {
         super();
      }
      
      protected static var xy:Array = ["x","y"];
      
      protected static var wh:Array = ["width","height"];
      
      protected static var spc:Array = ["rowHeight","colWidth"];
      
      override protected function getMaxItems() : Number {
         return horizontal?Math.floor(manager.width / colWidth):Math.floor(manager.height / rowHeight);
      }
      
      protected var horizontal:Boolean = true;
      
      protected var _autoSizeElements:Boolean = false;
      
      override public function arrange() : void {
         var _loc4_:* = null;
         var _loc2_:* = NaN;
         var _loc1_:* = NaN;
         var _loc3_:* = NaN;
         if(manager)
         {
            _loc2_ = horizontal;
            _loc1_ = !horizontal;
            _loc3_ = 0.0;
            while(_loc3_ < manager.numChildren)
            {
               _loc4_ = manager.getChildAt(_loc3_);
               if(_autoSizeElements)
               {
                  _loc4_.width = _colWidth;
                  _loc4_.height = _rowHeight;
               }
               _loc4_[xy[_loc2_]] = 0;
               _loc4_[xy[_loc1_]] = _loc3_ * this[spc[_loc2_]];
               _loc3_++;
            }
         }
      }
      
      override public function get direction() : String {
         return horizontal?"horizontal":"vertical";
      }
      
      override public function set direction(param1:String) : void {
         var _loc2_:String = param1 == "vertical"?"vertical":"horizontal";
         if(direction != _loc2_)
         {
            horizontal = _loc2_ == "horizontal";
            if(manager)
            {
               manager.chkItemsCount();
            }
         }
      }
      
      override public function set rowHeight(param1:Number) : void {
         var _loc2_:* = NaN;
         .super.rowHeight = param1;
         if(manager)
         {
            _loc2_ = 0.0;
            while(_loc2_ < manager.numChildren)
            {
               manager.getChildAt(_loc2_).height = _rowHeight;
               _loc2_++;
            }
            if(!horizontal)
            {
               manager.chkItemsCount();
               colWidth = colWidth;
            }
         }
      }
      
      override public function set colWidth(param1:Number) : void {
         var _loc2_:* = NaN;
         .super.colWidth = param1;
         if(manager)
         {
            _loc2_ = 0.0;
            while(_loc2_ < manager.numChildren)
            {
               manager.getChildAt(_loc2_).width = _colWidth;
               _loc2_++;
            }
            if(horizontal)
            {
               manager.chkItemsCount();
               rowHeight = rowHeight;
            }
         }
      }
      
      public function get autoSizeElements() : Boolean {
         return _autoSizeElements;
      }
      
      public function set autoSizeElements(param1:Boolean) : void {
         if(_autoSizeElements != param1)
         {
            _autoSizeElements = param1;
            arrange();
         }
      }
   }
}
