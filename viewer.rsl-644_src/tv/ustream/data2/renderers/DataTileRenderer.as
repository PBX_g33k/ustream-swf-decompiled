package tv.ustream.data2.renderers
{
   import flash.display.DisplayObject;
   
   public class DataTileRenderer extends DataListRenderer
   {
      
      public function DataTileRenderer() {
         super();
      }
      
      override protected function getMaxItems() : Number {
         return colCount * rowCount;
      }
      
      override public function arrange() : void {
         var _loc4_:* = null;
         var _loc1_:* = NaN;
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         if(manager)
         {
            _loc1_ = 0.0;
            _loc2_ = 0.0;
            _loc3_ = 0.0;
            while(_loc3_ < manager.numChildren)
            {
               _loc4_ = manager.getChildAt(_loc3_);
               if(horizontal)
               {
                  _loc2_ = _loc3_ % colCount;
                  _loc1_ = Math.floor(_loc3_ / colCount);
               }
               else
               {
                  _loc2_ = Math.floor(_loc3_ / rowCount);
                  _loc1_ = _loc3_ % rowCount;
               }
               _loc4_.x = _loc2_ * colWidth;
               _loc4_.y = _loc1_ * rowHeight;
               _loc3_++;
            }
         }
      }
   }
}
