package tv.ustream.data2.renderers
{
   import tv.ustream.data2.DataManager;
   
   public class DataRenderer extends Object
   {
      
      public function DataRenderer() {
         super();
      }
      
      public var manager:DataManager;
      
      public var skipDisplayProperties:Boolean = false;
      
      public final function get maxItems() : Number {
         return manager?getMaxItems():Infinity;
      }
      
      protected function getMaxItems() : Number {
         return Infinity;
      }
      
      public function arrange() : void {
      }
      
      public function get direction() : String {
         return "";
      }
      
      public function set direction(param1:String) : void {
      }
      
      protected var _rowHeight:Number = 100;
      
      public function get rowHeight() : Number {
         return _rowHeight;
      }
      
      public function set rowHeight(param1:Number) : void {
         _rowHeight = param1;
      }
      
      protected var _colWidth:Number = 100;
      
      public function get colWidth() : Number {
         return _colWidth;
      }
      
      public function set colWidth(param1:Number) : void {
         _colWidth = param1;
      }
      
      public function get colCount() : Number {
         return Math.floor(manager.width / colWidth);
      }
      
      public function get rowCount() : Number {
         return Math.floor(manager.height / rowHeight);
      }
   }
}
