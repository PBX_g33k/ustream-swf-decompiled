package tv.ustream.data2
{
   import flash.display.Sprite;
   import tv.ustream.data.DataProvider;
   import tv.ustream.data2.renderers.DataRenderer;
   import flash.utils.Dictionary;
   import tv.ustream.tools.DynamicEvent;
   import flash.display.DisplayObject;
   import tv.ustream.data2.renderers.DataListRenderer;
   
   public class DataManager extends Sprite
   {
      
      public function DataManager(param1:DataProvider = null, param2:DataRenderer = null, param3:Class = null) {
         super();
         lookup = new Dictionary();
         this.dataProvider = param1 || new DataProvider();
         if(param2)
         {
            this.dataRenderer = param2;
         }
         if(param3)
         {
            this.itemRenderer = param3;
         }
         initMode = false;
         chkItemsCount();
      }
      
      private var _dataProvider:DataProvider;
      
      private var _dataRenderer:DataRenderer;
      
      private var _itemRenderer:Class;
      
      private var lookup:Dictionary;
      
      private var _width:Number = 100;
      
      private var _height:Number = 100;
      
      private var maxItems:Number = 0;
      
      private var _percent:Number = 0;
      
      private var initMode:Boolean = true;
      
      private var autoUpdate:Boolean = true;
      
      private var first:Number = 0;
      
      private var _skipHidden:Boolean = false;
      
      private var _visibleItems:Number = 0;
      
      private var _itemPercent:Number = 0;
      
      override public function set visible(param1:Boolean) : void {
         .super.visible = param1;
         if(param1)
         {
            chkItemsCount();
         }
      }
      
      public function chkItemsCount(... rest) : void {
         var _loc4_:* = NaN;
         var _loc2_:* = NaN;
         var _loc5_:* = undefined;
         var _loc3_:* = NaN;
         if(_dataProvider && _itemRenderer && visible)
         {
            maxItems = _dataRenderer?_dataRenderer.maxItems:Infinity;
            _visibleItems = Math.max(Math.min(maxItems,source.length),0);
            if(!(_visibleItems == numChildren) || rest.length && (rest[0] === true || rest[0] && rest[0].type == "removeItem"))
            {
               _loc4_ = _visibleItems - numChildren;
               if(_loc4_ > 0)
               {
                  _loc2_ = 0.0;
                  while(_loc2_ < _loc4_)
                  {
                     _loc5_ = addChild(new _itemRenderer());
                     try
                     {
                        _loc5_.dataProvider = _dataProvider;
                     }
                     catch(e:*)
                     {
                     }
                     _loc2_++;
                  }
               }
               else
               {
                  _loc3_ = 0.0;
                  while(_loc3_ > _loc4_)
                  {
                     freeInstance(removeChildAt(numChildren - 1));
                     _loc3_--;
                  }
               }
               getFirst(true);
            }
            else
            {
               arrange();
            }
            _itemPercent = 1 / (source.length - _visibleItems);
            dispatchEvent(new DynamicEvent("scrollUpdate",false,false,
               {
                  "first":first,
                  "visibleItems":_visibleItems,
                  "grab":_visibleItems / source.length,
                  "itemPercent":_itemPercent
               }));
         }
      }
      
      public function getFirst(... rest) : void {
         var _loc2_:* = NaN;
         var _loc10_:* = NaN;
         var _loc4_:* = false;
         var _loc7_:* = null;
         var _loc3_:* = null;
         var _loc8_:* = NaN;
         var _loc12_:* = null;
         var _loc9_:* = NaN;
         var _loc5_:* = NaN;
         var _loc6_:* = NaN;
         var _loc11_:* = NaN;
         if(_dataProvider)
         {
            _loc2_ = Math.max(Math.floor((source.length - numChildren) * _percent),0);
            if(!(first == _loc2_) || rest && rest.length && rest[0] == true)
            {
               _loc10_ = _loc2_ - first;
               _loc4_ = _loc10_ < 0;
               first = _loc2_;
               _loc7_ = [];
               _loc12_ = new Array(numChildren);
               _loc9_ = 0.0;
               while(_loc9_ < numChildren)
               {
                  _loc3_ = getChildAt(_loc9_);
                  _loc8_ = lookup[getChildAt(_loc9_)]?source.indexOf(lookup[_loc3_]):-1;
                  if(_loc8_ < first || _loc8_ >= first + numChildren)
                  {
                     _loc7_.push(freeInstance(_loc3_));
                  }
                  else
                  {
                     _loc12_[_loc8_ - first] = true;
                  }
                  _loc9_++;
               }
               _loc5_ = 0.0;
               while(_loc5_ < _loc7_.length)
               {
                  _loc8_ = _loc12_.indexOf(undefined);
                  if(_loc12_.indexOf(undefined) != -1)
                  {
                     attachInstance(_loc7_[_loc5_],source[first + _loc8_]);
                     _loc12_[_loc8_] = true;
                  }
                  _loc5_++;
               }
               _loc6_ = 0.0;
               _loc6_ = 0.0;
               while(_loc6_ < numChildren)
               {
                  _loc11_ = source.indexOf(lookup[getChildAt(_loc6_)]) - first;
                  if(!(_loc6_ == _loc11_) && !(_loc11_ == -1))
                  {
                     _loc6_--;
                     swapChildrenAt(_loc6_,_loc11_);
                  }
                  _loc6_++;
               }
               arrange();
            }
         }
      }
      
      private function arrange(... rest) : void {
         if(_dataRenderer)
         {
            _dataRenderer.arrange();
         }
      }
      
      private function freeInstance(param1:DisplayObject) : DisplayObject {
         var _loc2_:Object = lookup[param1];
         if(_loc2_)
         {
            lookup[_loc2_] = null;
         }
         lookup[param1] = null;
         return param1;
      }
      
      private function attachInstance(param1:DisplayObject, param2:Object) : DisplayObject {
         lookup[freeInstance(param1)] = param2;
         lookup[param2] = param1;
         updateInstance(param2);
         if(param1.hasOwnProperty("item"))
         {
            try
            {
               param1["item"] = param2;
            }
            catch(e:*)
            {
            }
         }
         return param1;
      }
      
      private function updateInstance(param1:Object, param2:Object = null) : void {
         var _loc4_:* = null;
         if(lookup[param1])
         {
            _loc4_ = param2 || param1;
            if(lookup[param1].hasOwnProperty("update"))
            {
               lookup[param1].update(_loc4_);
            }
            else
            {
               _loc7_ = 0;
               _loc6_ = _loc4_;
               for(_loc3_ in _loc4_)
               {
                  if(lookup[param1].hasOwnProperty(_loc3_))
                  {
                     _loc5_ = _loc3_;
                     if("x" !== _loc5_)
                     {
                        if("y" !== _loc5_)
                        {
                           if("width" !== _loc5_)
                           {
                              if("height" !== _loc5_)
                              {
                              }
                              lookup[param1][_loc3_] = _loc4_[_loc3_];
                              continue;
                           }
                           if(!_dataRenderer.skipDisplayProperties)
                           {
                              lookup[param1][_loc3_] = _loc4_[_loc3_];
                           }
                           continue;
                        }
                        if(!_dataRenderer.skipDisplayProperties)
                        {
                           lookup[param1][_loc3_] = _loc4_[_loc3_];
                        }
                        continue;
                     }
                     if(!_dataRenderer.skipDisplayProperties)
                     {
                        lookup[param1][_loc3_] = _loc4_[_loc3_];
                     }
                  }
               }
            }
         }
      }
      
      public function getDataOfInstance(param1:DisplayObject) : Object {
         return lookup[param1]?lookup[param1]:null;
      }
      
      public function getInstanceOfData(param1:Object) : DisplayObject {
         return lookup[param1]?lookup[param1]:null;
      }
      
      public function destroyAllInstance() : void {
         while(numChildren)
         {
            freeInstance(removeChildAt(0));
         }
      }
      
      private function onUpdateItem(param1:DynamicEvent) : void {
         updateInstance(param1.item,param1.changes);
         if(_skipHidden && !(param1.changes.visible == undefined))
         {
            chkItemsCount(lookup[param1.item]?true:false);
         }
      }
      
      private function onSortItem(... rest) : void {
         getFirst(true);
      }
      
      public function get dataProvider() : DataProvider {
         return _dataProvider;
      }
      
      public function set dataProvider(param1:DataProvider) : void {
         var _loc2_:* = null;
         if(_dataProvider)
         {
            _dataProvider.removeEventListener("addItem",chkItemsCount);
            _dataProvider.removeEventListener("updateItem",onUpdateItem);
            _dataProvider.removeEventListener("removeItem",chkItemsCount);
            _dataProvider.removeEventListener("sort",onSortItem);
            destroyAllInstance();
         }
         _dataProvider = param1;
         if(_dataProvider)
         {
            _dataProvider.addEventListener("addItem",chkItemsCount);
            _dataProvider.addEventListener("updateItem",onUpdateItem);
            _dataProvider.addEventListener("removeItem",chkItemsCount);
            _dataProvider.addEventListener("sort",onSortItem);
            if(!initMode)
            {
               chkItemsCount();
            }
         }
      }
      
      public function get dataRenderer() : DataRenderer {
         return _dataRenderer;
      }
      
      public function set dataRenderer(param1:DataRenderer) : void {
         if(_dataRenderer != param1)
         {
            _dataRenderer = param1;
            if(_dataRenderer)
            {
               _dataRenderer.manager = this;
               chkItemsCount();
            }
         }
      }
      
      public function get itemRenderer() : Class {
         return _itemRenderer;
      }
      
      public function set itemRenderer(param1:Class) : void {
         if(_itemRenderer && numChildren)
         {
            destroyAllInstance();
         }
         _itemRenderer = param1;
         if(_itemRenderer && dataProvider)
         {
            chkItemsCount();
         }
      }
      
      public function get percent() : Number {
         return _percent;
      }
      
      public function set percent(param1:Number) : void {
         if(_percent != param1)
         {
            _percent = Math.min(Math.max(param1,0),1);
            getFirst();
         }
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         if(_width != param1)
         {
            _width = param1;
            if(dataRenderer is DataListRenderer && (dataRenderer as DataListRenderer).direction == "vertical" && ((dataRenderer as DataListRenderer).autoSizeElements))
            {
               (dataRenderer as DataListRenderer).colWidth = _width;
            }
            chkItemsCount();
         }
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         if(_height != param1)
         {
            _height = param1;
            if(dataRenderer is DataListRenderer && (dataRenderer as DataListRenderer).direction == "horizontal" && ((dataRenderer as DataListRenderer).autoSizeElements))
            {
               (dataRenderer as DataListRenderer).rowHeight = _height;
            }
            chkItemsCount();
         }
      }
      
      public function get skipHidden() : Boolean {
         return _skipHidden;
      }
      
      public function set skipHidden(param1:Boolean) : void {
         _skipHidden = param1;
         getFirst();
      }
      
      private function get source() : Array {
         return _skipHidden?_dataProvider.getItemAtProperty("visible",true,true):_dataProvider.source;
      }
      
      public function get visibleItems() : Number {
         return _visibleItems;
      }
      
      public function get itemPercent() : Number {
         return _itemPercent;
      }
      
      public function get firstVisibleItem() : Number {
         return first;
      }
   }
}
