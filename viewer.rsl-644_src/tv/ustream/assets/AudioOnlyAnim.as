package tv.ustream.assets
{
   import tv.ustream.tools.Container;
   import flash.media.Microphone;
   import flash.text.TextField;
   import flash.events.Event;
   import flash.text.TextFormat;
   import flash.filters.DropShadowFilter;
   
   public class AudioOnlyAnim extends Container
   {
      
      public function AudioOnlyAnim() {
         super();
         visible = false;
         if(stage)
         {
            onAddedToStage();
         }
         else
         {
            addEventListener("addedToStage",onAddedToStage);
         }
      }
      
      public static var MyriadCondBoldItalic:Class = MyriadPro-BoldCondIt_ttf$c78cac581937b8d0f430a53e04997f551005414776;
      
      private var eqAnim:EqAnim;
      
      private var mic:Microphone;
      
      private var tf:TextField;
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener("addedToStage",onAddedToStage);
         eqAnim = new EqAnim(400,300);
         addChild(new EqAnim(400,300));
         tf = new TextField();
         addChild(new TextField());
         tf.autoSize = "left";
         var _loc2_:TextFormat = new TextFormat("MyriadCondBoldItalic",18,16777215,true,true);
         tf.embedFonts = true;
         tf.text = " AUDIO ONLY ";
         tf.setTextFormat(_loc2_);
         tf.selectable = false;
         tf.filters = [new DropShadowFilter(0,0,0,1,5,5,2,3)];
      }
      
      public function set microphone(param1:Microphone) : void {
         if(eqAnim && param1)
         {
            eqAnim.mic = param1;
         }
      }
      
      public function set enable(param1:Boolean) : void {
         if(eqAnim)
         {
            eqAnim.enabled = param1;
         }
      }
      
      public function destroy() : void {
         eqAnim.enabled = false;
         removeChild(eqAnim);
         eqAnim = null;
         removeChild(tf);
         tf = null;
      }
      
      override protected function render(... rest) : void {
         if(eqAnim)
         {
            eqAnim.x = Math.round((_width - eqAnim.width) / 2);
            eqAnim.y = Math.round((_height - eqAnim.height) / 2);
         }
         if(tf)
         {
            tf.x = Math.round((_width - tf.width) / 2);
            tf.y = Math.round((_height - tf.height) / 2);
         }
      }
      
      override public function set visible(param1:Boolean) : void {
         .super.visible = param1;
         if(eqAnim)
         {
            eqAnim.enabled = param1;
         }
      }
   }
}
