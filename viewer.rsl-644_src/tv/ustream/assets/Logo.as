package tv.ustream.assets
{
   import flash.display.Sprite;
   import flash.display.Loader;
   import flash.filters.GlowFilter;
   import flash.system.Security;
   import flash.net.URLRequest;
   import flash.system.LoaderContext;
   import flash.system.ApplicationDomain;
   import flash.system.SecurityDomain;
   import flash.events.Event;
   import flash.display.Bitmap;
   import flash.events.IOErrorEvent;
   
   public class Logo extends Sprite
   {
      
      public function Logo(param1:String = "ustream", param2:Boolean = true) {
         super();
         name = "logo";
         this.watermark = param2;
         this.brand = param1 || "ustream";
         tabChildren = false;
         tabEnabled = false;
      }
      
      private var ustreamLogoAdd:Sprite;
      
      private var ustreamLogoOverlay:Sprite;
      
      private var ustreamLogoShadow:Sprite;
      
      private var loader:Loader;
      
      private var _live:Boolean = false;
      
      private var _recorded:Boolean = false;
      
      private var _mobile:Boolean = false;
      
      private var watermark:Boolean;
      
      private var _brand:String;
      
      private var _url:String;
      
      private var logo:Sprite;
      
      public function get brand() : String {
         return _brand;
      }
      
      public function set brand(param1:String) : void {
         value = param1;
         if(!(value == _brand) && value)
         {
            _brand = value;
            if(logo && contains(logo))
            {
               removeChild(logo);
               logo = null;
            }
            if(!logo)
            {
               logo = new Sprite();
               logo.mouseChildren = false;
               logo.addEventListener("added",updateLogoHitArea);
               logo.addEventListener("removed",updateLogoHitArea);
               updateLogoHitArea();
               addChild(logo);
            }
            _loc2_ = _brand;
            if("ustream" !== _loc2_)
            {
               if("youtube" !== _loc2_)
               {
                  loader = new Loader();
                  loader.contentLoaderInfo.addEventListener("complete",onLoadComplete);
                  loader.contentLoaderInfo.addEventListener("ioError",onLoadError);
                  if(Security.sandboxType == "localTrusted")
                  {
                     loader.load(new URLRequest(value));
                  }
                  else
                  {
                     loader.load(new URLRequest(value),new LoaderContext(false,ApplicationDomain.currentDomain,SecurityDomain.currentDomain));
                  }
               }
               else
               {
                  if(YouTubeLogo)
                  {
                  }
                  logo.addChild(new YouTubeLogo());
               }
            }
            else
            {
               if(watermark)
               {
                  ustreamLogoAdd = new UstreamLogo();
                  with(logo.addChild(new UstreamLogo()))
                  {
                  }
                  alpha = 0.4;
                  blendMode = "add";
                  }
                  ustreamLogoOverlay = new UstreamLogo();
                  with(logo.addChild(new UstreamLogo()))
                  {
                  }
                  ustreamLogoOverlay.ustream.alpha = 0.3;
                  ustreamLogoOverlay.ustream.blendMode = "overlay";
                  ustreamLogoOverlay.recorded.alpha = 0.3;
                  ustreamLogoOverlay.recorded.blendMode = "overlay";
                  ustreamLogoOverlay.live.alpha = 0.3;
                  ustreamLogoOverlay.live.blendMode = "overlay";
                  }
               }
               ustreamLogoShadow = new UstreamLogo();
               logo.addChild(new UstreamLogo());
               if(watermark)
               {
                  ustreamLogoShadow.filters = [new GlowFilter(0,1,4,4,0.5,3,false,true)];
               }
               live = _live;
               recorded = _recorded;
               mobile = _mobile;
            }
         }
      }
      
      private function updateLogoHitArea(... rest) : void {
         logo.graphics.clear();
         logo.graphics.beginFill(16711680,0);
         logo.graphics.drawRect(0,0,logo.width,_brand == "ustream" && !_live && !_recorded?16:logo.height);
         logo.graphics.endFill();
      }
      
      private function onLoadComplete(param1:Event) : void {
         if(loader.content is Bitmap)
         {
            Bitmap(loader.content).smoothing = true;
         }
         logo.addChild(loader);
         dispatchEvent(new Event("complete"));
      }
      
      private function onLoadError(param1:IOErrorEvent) : void {
      }
      
      public function get live() : Boolean {
         return _live;
      }
      
      public function set live(param1:Boolean) : void {
         _live = param1;
         if(_live && (_recorded))
         {
            recorded = false;
         }
         if(ustreamLogoAdd)
         {
            (ustreamLogoAdd as UstreamLogo).live.visible = param1;
         }
         if(ustreamLogoOverlay)
         {
            (ustreamLogoOverlay as UstreamLogo).live.visible = param1;
         }
         if(ustreamLogoShadow)
         {
            (ustreamLogoShadow as UstreamLogo).live.visible = param1;
         }
         updateLogoHitArea();
         posMobile();
      }
      
      public function get recorded() : Boolean {
         return _recorded;
      }
      
      public function set recorded(param1:Boolean) : void {
         _recorded = param1;
         if(_recorded && (_live))
         {
            live = false;
         }
         if(ustreamLogoAdd)
         {
            (ustreamLogoAdd as UstreamLogo).recorded.visible = param1;
         }
         if(ustreamLogoOverlay)
         {
            (ustreamLogoOverlay as UstreamLogo).recorded.visible = param1;
         }
         if(ustreamLogoShadow)
         {
            (ustreamLogoShadow as UstreamLogo).recorded.visible = param1;
         }
         updateLogoHitArea();
         posMobile();
      }
      
      public function posMobile() : void {
         var _loc2_:* = 0.0;
         var _loc1_:* = 18.0;
         if(_live)
         {
            _loc2_ = 31.0;
         }
         else if(_recorded)
         {
            _loc2_ = 48.0;
            _loc1_ = 30.0;
         }
         
         if(ustreamLogoAdd)
         {
            (ustreamLogoAdd as UstreamLogo).mobile.x = _loc2_;
            (ustreamLogoAdd as UstreamLogo).mobile.y = _loc1_;
         }
         if(ustreamLogoOverlay)
         {
            (ustreamLogoOverlay as UstreamLogo).mobile.x = _loc2_;
            (ustreamLogoOverlay as UstreamLogo).mobile.y = _loc1_;
         }
         if(ustreamLogoShadow)
         {
            (ustreamLogoShadow as UstreamLogo).mobile.x = _loc2_;
            (ustreamLogoShadow as UstreamLogo).mobile.y = _loc1_;
         }
      }
      
      public function get mobile() : Boolean {
         return _mobile;
      }
      
      public function set mobile(param1:Boolean) : void {
         if(ustreamLogoAdd)
         {
            (ustreamLogoAdd as UstreamLogo).mobile.visible = param1;
         }
         if(ustreamLogoOverlay)
         {
            (ustreamLogoOverlay as UstreamLogo).mobile.visible = param1;
            (ustreamLogoOverlay as UstreamLogo).live.alpha = param1?1:0.3;
            (ustreamLogoOverlay as UstreamLogo).live.blendMode = param1?"normal":"overlay";
         }
         if(ustreamLogoShadow)
         {
            (ustreamLogoShadow as UstreamLogo).mobile.visible = param1;
         }
         updateLogoHitArea();
      }
      
      override public function set width(param1:Number) : void {
      }
      
      override public function get width() : Number {
         return logo.width;
      }
      
      override public function set height(param1:Number) : void {
      }
      
      override public function get height() : Number {
         return logo.height;
      }
   }
}
