package tv.ustream.assets
{
   import flash.display.Sprite;
   import flash.display.BitmapData;
   import flash.geom.Point;
   import flash.media.Microphone;
   import flash.geom.Matrix;
   import flash.filters.BlurFilter;
   import flash.filters.DisplacementMapFilter;
   import flash.events.Event;
   import flash.display.Bitmap;
   
   public class EqAnim extends Sprite
   {
      
      public function EqAnim(param1:Number = 400, param2:Number = 225) {
         point = new Point();
         matrix = new Matrix();
         blur = new BlurFilter(2,2,1);
         rules = [
            {
               "r":255,
               "g":0,
               "b":0,
               "dr":0,
               "dg":1,
               "db":0
            },
            {
               "r":255,
               "g":255,
               "b":0,
               "dr":-1,
               "dg":0,
               "db":0
            },
            {
               "r":0,
               "g":255,
               "b":0,
               "dr":0,
               "dg":0,
               "db":1
            },
            {
               "r":0,
               "g":255,
               "b":255,
               "dr":0,
               "dg":-1,
               "db":0
            },
            {
               "r":0,
               "g":0,
               "b":255,
               "dr":1,
               "dg":0,
               "db":0
            },
            {
               "r":255,
               "g":0,
               "b":255,
               "dr":0,
               "dg":0,
               "db":-1
            }];
         super();
         beat = new Sprite();
         resize(param1,param2);
      }
      
      private var bmd:BitmapData;
      
      private var noise:BitmapData;
      
      private var point:Point;
      
      private var microphone:Microphone;
      
      private var matrix:Matrix;
      
      private var _enabled:Boolean = false;
      
      private var blur:BlurFilter;
      
      private var displace:DisplacementMapFilter;
      
      private var counter:Number = 0;
      
      private var beat:Sprite;
      
      private var rules:Array;
      
      public function set mic(param1:Microphone) : void {
         microphone = param1;
         if(_enabled)
         {
            enabled = true;
         }
      }
      
      public function get enabled() : Boolean {
         return _enabled;
      }
      
      public function set enabled(param1:Boolean) : void {
         removeEventListener("enterFrame",update);
         bmd.fillRect(bmd.rect,0);
         _enabled = param1;
         if(_enabled && microphone)
         {
            addEventListener("enterFrame",update);
         }
      }
      
      private function getColor(param1:Number) : Number {
         var param1:Number = param1 % 1530;
         var _loc3_:Number = param1 % 1530 % 255;
         var _loc2_:Object = rules[param1 / 255];
         return (_loc2_.r + _loc2_.dr * _loc3_ << 16) + (_loc2_.g + _loc2_.dg * _loc3_ << 8) + (_loc2_.b + _loc2_.db * _loc3_);
      }
      
      private function update(param1:Event) : void {
         var _loc3_:* = counter + 3;
         counter = _loc3_;
         var _loc2_:Number = getColor(_loc3_);
         beat.graphics.clear();
         beat.graphics.lineStyle(microphone.activityLevel / 10,_loc2_);
         beat.graphics.drawCircle(0,0,microphone.activityLevel);
         if(!(counter % 2))
         {
            bmd.applyFilter(bmd,bmd.rect,point,displace);
         }
         bmd.applyFilter(bmd,bmd.rect,point,blur);
         bmd.draw(beat,matrix);
      }
      
      public function resize(param1:Number, param2:Number) : void {
         while(numChildren)
         {
            removeChildAt(0);
         }
         bmd = new BitmapData(param1,param2,true,0);
         addChild(new Bitmap(new BitmapData(param1,param2,true,0)));
         beat.x = param1 / 2;
         beat.y = param2 / 2;
         matrix.identity();
         matrix.translate(param1 / 2,param2 / 2);
         noise = new BitmapData(param1,param2);
         noise.perlinNoise(100,100,25,1,false,true,6);
         displace = new DisplacementMapFilter(noise,point,2,4,5,5,"wrap");
      }
   }
}
