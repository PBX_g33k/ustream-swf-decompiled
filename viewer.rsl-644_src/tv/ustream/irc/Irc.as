package tv.ustream.irc
{
   import tv.ustream.tools.Dispatcher;
   import flash.display.LoaderInfo;
   import tv.ustream.tools.Debug;
   import flash.net.Socket;
   import flash.system.Security;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.events.ProgressEvent;
   import flash.system.Capabilities;
   import flash.external.ExternalInterface;
   import flash.events.Event;
   
   public class Irc extends Dispatcher
   {
      
      public function Irc(param1:String, param2:String = null, param3:String = "chat1.ustream.tv", param4:int = 6667) {
         chats = {};
         channels = {};
         serverCapabilities = {};
         hasModeParams = 
            {
               "-":"k",
               "+":"kl"
            };
         super();
         _nick = param1;
         _password = param2;
         this.host = param3;
         this.port = param4;
         createSocket();
      }
      
      private static var _loaderInfo:LoaderInfo;
      
      private static function removeFirst(param1:String, param2:String) : String {
         return param1.charAt(0) != param2?param1:param1.substr(1);
      }
      
      static function fixChannelName(param1:String) : String {
         Debug.echo("fixChannelName " + param1);
         var param1:String = param1.replace(new RegExp("[:,#]","g"),"").toLowerCase();
         param1 = param1.replace(new RegExp("^\\+*"),"").toLowerCase();
         Debug.echo("RESULT : " + param1);
         return param1;
      }
      
      static function fixColon(param1:String) : String {
         return removeFirst(param1,":");
      }
      
      static function getUser(param1:String) : Array {
         return fixColon(param1).split("!");
      }
      
      public static function get loaderInfo() : LoaderInfo {
         return _loaderInfo;
      }
      
      public static function set loaderInfo(param1:LoaderInfo) : void {
         if(!_loaderInfo)
         {
            _loaderInfo = param1;
         }
      }
      
      private var host:String;
      
      private var port:int;
      
      private var socket:Socket;
      
      private var _nick:String;
      
      private var _password:String;
      
      private var _motd:String = "";
      
      private var ready:Boolean = false;
      
      private var wasInjured:Boolean = false;
      
      private var nowInjured:Boolean = false;
      
      private var injured:String;
      
      private var chats:Object;
      
      private var channels:Object;
      
      private var list:Array;
      
      private var serverCapabilities:Object;
      
      private var hasModeParams:Object;
      
      private function createSocket() : void {
         dispatch("connect");
         Security.loadPolicyFile("xmlsocket://" + host + ":843");
         socket = new Socket();
         socket.addEventListener("ioError",onSocketIoError);
         socket.addEventListener("securityError",onSocketSecurityError);
         socket.addEventListener("connect",onSocketConnect);
         socket.addEventListener("socketData",onSocketData);
         socket.addEventListener("close",onSocketClose);
         Debug.echo("connecting to " + host + " on " + port);
         socket.connect(host,port);
      }
      
      private function onSocketIoError(param1:IOErrorEvent) : void {
         dispatch("ioError",false,{"error":param1});
         Debug.echo("onSocketIoError " + param1);
         destroy();
      }
      
      private function onSocketSecurityError(param1:SecurityErrorEvent) : void {
         dispatch("securityError",false,{"error":param1});
         Debug.echo("onSocketIoError " + param1);
      }
      
      private function onSocketConnect(... rest) : void {
         dispatch("connected");
         send("USER " + nick + " " + nick + " " + nick + " :ustream.tv");
         send("NICK " + nick);
      }
      
      private function onSocketData(param1:ProgressEvent) : void {
         var _loc2_:* = 0;
         var _loc4_:* = 0;
         var _loc3_:Array = socket.readUTFBytes(param1.bytesLoaded).split("\r\n");
         nowInjured = _loc3_[_loc3_.length - 1];
         _loc2_ = 0;
         while(_loc2_ < _loc3_.length)
         {
            if(!_loc3_[_loc2_])
            {
               _loc3_.splice(_loc2_,1);
               _loc2_--;
            }
            _loc2_++;
         }
         if(wasInjured)
         {
            parseLine(injured + _loc3_.shift());
         }
         if(nowInjured)
         {
            injured = _loc3_.pop();
         }
         _loc4_ = 0;
         while(_loc4_ < _loc3_.length)
         {
            parseLine(_loc3_[_loc4_]);
            _loc4_++;
         }
         wasInjured = nowInjured;
      }
      
      private function onSocketClose(... rest) : void {
         dispatch("disconnected");
         destroy();
      }
      
      override public function destroy(... rest) : * {
         var _loc5_:* = 0;
         var _loc4_:* = chats;
         for each(_loc2_ in chats)
         {
            _loc2_.destroy();
         }
         var _loc7_:* = 0;
         var _loc6_:* = channels;
         for each(_loc3_ in channels)
         {
            _loc3_.destroy();
         }
         if(socket.connected)
         {
            socket.close();
         }
         return super.destroy();
      }
      
      public function parseLine(param1:String) : void {
         var _loc6_:* = null;
         var _loc15_:* = null;
         var _loc21_:* = null;
         var _loc9_:* = null;
         var _loc2_:* = null;
         var _loc12_:* = false;
         var _loc16_:* = null;
         var _loc4_:* = null;
         var _loc13_:* = null;
         var _loc22_:* = null;
         var _loc5_:* = null;
         var _loc20_:* = null;
         var _loc17_:* = null;
         var _loc7_:* = null;
         var _loc11_:* = null;
         var _loc19_:* = null;
         var _loc14_:* = null;
         Debug.echo("&lt;&lt; " + param1);
         var _loc8_:Array = param1.split(" ");
         var _loc3_:uint = 0;
         var _loc18_:String = _loc8_[0];
         var _loc10_:String = _loc8_.length > 3?fixColon(_loc8_.slice(3).join(" ")):"";
         if(_loc18_ == "PING")
         {
            send("PONG " + _loc8_[1]);
         }
         else if(_loc18_ == "ERROR")
         {
            dispatch("error",false,{"error":_loc8_.join(" ")});
         }
         else
         {
            _loc18_ = _loc8_[1];
            _loc27_ = _loc8_[1];
            if("001" !== _loc27_)
            {
               if("005" !== _loc27_)
               {
                  if("311" !== _loc27_)
                  {
                     if("321" !== _loc27_)
                     {
                        if("322" !== _loc27_)
                        {
                           if("323" !== _loc27_)
                           {
                              if("324" !== _loc27_)
                              {
                                 if("332" !== _loc27_)
                                 {
                                    if("333" !== _loc27_)
                                    {
                                       if("353" !== _loc27_)
                                       {
                                          if("366" !== _loc27_)
                                          {
                                             if("367" !== _loc27_)
                                             {
                                                if("368" !== _loc27_)
                                                {
                                                   if("372" !== _loc27_)
                                                   {
                                                      if("376" !== _loc27_)
                                                      {
                                                         if("404" !== _loc27_)
                                                         {
                                                            if("433" !== _loc27_)
                                                            {
                                                               if("471" !== _loc27_)
                                                               {
                                                                  if("473" !== _loc27_)
                                                                  {
                                                                     if("474" !== _loc27_)
                                                                     {
                                                                        if("475" !== _loc27_)
                                                                        {
                                                                           if("477" !== _loc27_)
                                                                           {
                                                                              if("851" !== _loc27_)
                                                                              {
                                                                                 if("JOIN" !== _loc27_)
                                                                                 {
                                                                                    if("MODE" !== _loc27_)
                                                                                    {
                                                                                       if("PRIVMSG" !== _loc27_)
                                                                                       {
                                                                                          if("TOPIC" !== _loc27_)
                                                                                          {
                                                                                             if("NICK" !== _loc27_)
                                                                                             {
                                                                                                if("PART" !== _loc27_)
                                                                                                {
                                                                                                   if("QUIT" !== _loc27_)
                                                                                                   {
                                                                                                      if("KICK" !== _loc27_)
                                                                                                      {
                                                                                                         if("NOTICE" === _loc27_)
                                                                                                         {
                                                                                                            trace("NOTICE");
                                                                                                            if(fixColon(_loc8_[3]) == "REMOVE")
                                                                                                            {
                                                                                                               _loc15_ = fixChannelName(_loc8_[2]);
                                                                                                               if(channels[_loc15_])
                                                                                                               {
                                                                                                                  channels[_loc15_].removeMessage(_loc8_.slice(5).join(" "),_loc8_[4],getUser(_loc8_[0])[0]);
                                                                                                               }
                                                                                                            }
                                                                                                            else if(fixColon(_loc8_[3]) == "CHANGE")
                                                                                                            {
                                                                                                               _loc15_ = fixChannelName(_loc8_[2]);
                                                                                                               _loc14_ = new RegExp("~-~+","g");
                                                                                                               if(channels[_loc15_])
                                                                                                               {
                                                                                                                  channels[_loc15_].replaceMessage(_loc8_[5].replace(_loc14_," "),_loc8_[6].replace(_loc14_," "),getUser(_loc8_[4])[0]);
                                                                                                               }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                               _loc23_ = fixColon(_loc8_.slice(3).join(" "));
                                                                                                               if("This is a registered nick, either choose another nick or enter the password by doing: /PASS <password>" !== _loc23_)
                                                                                                               {
                                                                                                                  if("Incorrect password for this account" !== _loc23_)
                                                                                                                  {
                                                                                                                     if("This is a registered nickname, to use this nick use: /nick <nick>:password" !== _loc23_)
                                                                                                                     {
                                                                                                                        dispatch("notice",false,
                                                                                                                           {
                                                                                                                              "to":_loc8_[2],
                                                                                                                              "message":fixColon(_loc8_.slice(3).join(" "))
                                                                                                                           });
                                                                                                                     }
                                                                                                                     else
                                                                                                                     {
                                                                                                                        dispatch("needPasswordToNick");
                                                                                                                     }
                                                                                                                  }
                                                                                                                  else
                                                                                                                  {
                                                                                                                     dispatch("invalidPassword");
                                                                                                                  }
                                                                                                               }
                                                                                                               else if(_password)
                                                                                                               {
                                                                                                                  send("PASS " + _password);
                                                                                                                  _password = null;
                                                                                                               }
                                                                                                               else
                                                                                                               {
                                                                                                                  dispatch("needPassword");
                                                                                                               }
                                                                                                               
                                                                                                            }
                                                                                                            
                                                                                                         }
                                                                                                      }
                                                                                                      else
                                                                                                      {
                                                                                                         _loc15_ = fixChannelName(_loc8_[2]);
                                                                                                         if(channels[_loc15_])
                                                                                                         {
                                                                                                            channels[_loc15_].kick(_loc8_[3],getUser(_loc8_[0])[0],_loc8_.slice(4).join(" "));
                                                                                                         }
                                                                                                         if(_loc8_[3] == _nick)
                                                                                                         {
                                                                                                            channels[_loc15_].destroy();
                                                                                                         }
                                                                                                      }
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                                      _loc29_ = 0;
                                                                                                      _loc28_ = channels;
                                                                                                      for each(_loc6_ in channels)
                                                                                                      {
                                                                                                         _loc6_.quit(getUser(_loc8_[0]));
                                                                                                      }
                                                                                                   }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                   _loc21_ = getUser(_loc8_[0]);
                                                                                                   _loc15_ = fixChannelName(_loc8_[2]);
                                                                                                   if(channels[_loc15_])
                                                                                                   {
                                                                                                      channels[_loc15_].part(_loc21_);
                                                                                                      if(_loc21_[0] == _nick)
                                                                                                      {
                                                                                                         channels[_loc15_].destroy();
                                                                                                      }
                                                                                                   }
                                                                                                }
                                                                                             }
                                                                                             else
                                                                                             {
                                                                                                _loc21_ = getUser(_loc8_[0]);
                                                                                                _loc11_ = _loc21_[0];
                                                                                                _loc19_ = fixColon(_loc8_[2]);
                                                                                                if(nick == _loc11_)
                                                                                                {
                                                                                                   nick = _loc19_;
                                                                                                }
                                                                                                _loc27_ = 0;
                                                                                                _loc26_ = channels;
                                                                                                for each(_loc6_ in channels)
                                                                                                {
                                                                                                   _loc6_.nick(_loc11_,_loc19_);
                                                                                                }
                                                                                             }
                                                                                          }
                                                                                          else
                                                                                          {
                                                                                             _loc15_ = fixChannelName(_loc8_[2]);
                                                                                             if(channels[_loc15_])
                                                                                             {
                                                                                                channels[_loc15_].setTopic(fixColon(_loc8_.slice(3).join(" ")));
                                                                                             }
                                                                                          }
                                                                                       }
                                                                                       else
                                                                                       {
                                                                                          _loc15_ = fixChannelName(_loc8_[2]);
                                                                                          _loc9_ = fixColon(_loc8_.slice(3).join(" "));
                                                                                          _loc13_ = "message";
                                                                                          _loc21_ = getUser(_loc8_[0]);
                                                                                          if(_loc9_.charCodeAt(0) == 1 && _loc9_.charCodeAt(_loc9_.length - 1) == 1)
                                                                                          {
                                                                                             _loc22_ = String.fromCharCode(1);
                                                                                             _loc5_ = _loc9_.substring(1,_loc9_.length - 1).split(" ");
                                                                                             _loc23_ = _loc5_[0].toLowerCase();
                                                                                             if("version" !== _loc23_)
                                                                                             {
                                                                                                if("time" !== _loc23_)
                                                                                                {
                                                                                                   if("ping" !== _loc23_)
                                                                                                   {
                                                                                                      if("action" !== _loc23_)
                                                                                                      {
                                                                                                         return;
                                                                                                      }
                                                                                                      _loc9_ = _loc5_.slice(1).join(" ");
                                                                                                      _loc13_ = "me";
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                                      send("NOTICE " + _loc21_[0] + " :" + _loc22_ + "PING " + _loc5_[1] + _loc22_);
                                                                                                      return;
                                                                                                   }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                   send("NOTICE " + _loc21_[0] + " :" + _loc22_ + "TIME " + new Date() + "" + _loc22_);
                                                                                                   return;
                                                                                                }
                                                                                             }
                                                                                             else
                                                                                             {
                                                                                                _loc20_ = "NOTICE " + _loc21_[0] + " :" + _loc22_ + "VERSION ustream.tv IRC | flashPlayer : " + Capabilities.version + " | sandBox : " + Security.sandboxType;
                                                                                                if(_loaderInfo)
                                                                                                {
                                                                                                   _loc20_ = _loc20_ + (" | rsl : " + _loaderInfo.url);
                                                                                                   if(_loaderInfo.loaderURL)
                                                                                                   {
                                                                                                      _loc20_ = _loc20_ + (" | document : " + _loaderInfo.loaderURL);
                                                                                                   }
                                                                                                }
                                                                                                try
                                                                                                {
                                                                                                   _loc7_ = "getPageUrl=function(){ return window.location.href }; getPageUrl()";
                                                                                                   _loc17_ = ExternalInterface.call("eval",_loc7_);
                                                                                                }
                                                                                                catch(e:Error)
                                                                                                {
                                                                                                }
                                                                                                if(_loc17_)
                                                                                                {
                                                                                                   _loc20_ = _loc20_ + (" | pageUrl : " + _loc17_);
                                                                                                }
                                                                                                send(_loc20_ + _loc22_);
                                                                                                return;
                                                                                             }
                                                                                          }
                                                                                          Chat(_loc15_ == _nick.toLowerCase() && !((_loc8_[2]).charAt(0) == "#")?createChat(getUser(_loc8_[0])[0]):createChannel(_loc15_)).getMessage(_loc9_.replace(new RegExp("<","g"),"&lt;").replace(new RegExp(">","g"),"&gt;"),fixColon(_loc8_[0]),_loc13_);
                                                                                       }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                       if(_loc8_[2].indexOf("#") == -1)
                                                                                       {
                                                                                          return;
                                                                                       }
                                                                                       _loc15_ = fixChannelName(_loc8_[2]);
                                                                                       if(channels[fixChannelName(_loc8_[2])])
                                                                                       {
                                                                                          parseMode(_loc15_,_loc8_[3],_loc8_.slice(4));
                                                                                       }
                                                                                    }
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                    _loc21_ = getUser(_loc8_[0]);
                                                                                    _loc15_ = fixChannelName(_loc8_[2]);
                                                                                    if(_loc21_[0] == nick)
                                                                                    {
                                                                                       createChannel(_loc15_);
                                                                                    }
                                                                                    else if(channels[_loc15_])
                                                                                    {
                                                                                       channels[_loc15_].join(_loc21_);
                                                                                    }
                                                                                    
                                                                                 }
                                                                              }
                                                                              else
                                                                              {
                                                                                 _loc15_ = fixChannelName(_loc8_[3]);
                                                                                 if(channels[fixChannelName(_loc8_[3])])
                                                                                 {
                                                                                    _loc12_ = _loc8_[4].substr(0,2) == ":[";
                                                                                    _loc16_ = _loc8_[_loc12_?5:4.0].replace(new RegExp("[<:>]","g"),"");
                                                                                    if(_loc8_[6] == "REMOVE")
                                                                                    {
                                                                                       createChannel(_loc15_).removeMessage(_loc8_.slice(8).join(" "),_loc8_[7],_loc8_[5].split("-").join(""),true);
                                                                                    }
                                                                                    else if(_loc8_[6] == "CHANGE")
                                                                                    {
                                                                                       _loc4_ = new RegExp("~-~+","g");
                                                                                       createChannel(_loc15_).replaceMessage(_loc8_[8].replace(_loc4_," "),_loc8_[9].replace(_loc4_," "),getUser(_loc8_[7])[0]);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                       _loc9_ = _loc8_.slice(_loc12_?6:5.0).join(" ").replace(new RegExp("<","g"),"&lt;").replace(new RegExp(">","g"),"&gt;");
                                                                                       createChannel(_loc15_).getMessage(_loc9_,_loc16_,"log",_loc12_?_loc8_[4].substr(1):null);
                                                                                    }
                                                                                    
                                                                                 }
                                                                              }
                                                                           }
                                                                           else
                                                                           {
                                                                              dispatch("cantJoin",false,
                                                                                 {
                                                                                    "reason":"regOnly",
                                                                                    "channelName":fixChannelName(_loc8_[3])
                                                                                 });
                                                                           }
                                                                        }
                                                                     }
                                                                     _loc23_ = _loc8_[7];
                                                                     if("(+l)" !== _loc23_)
                                                                     {
                                                                        if("(+i)" !== _loc23_)
                                                                        {
                                                                           if("(+b)" !== _loc23_)
                                                                           {
                                                                              if("(+k)" !== _loc23_)
                                                                              {
                                                                                 _loc2_ = "unknown";
                                                                              }
                                                                              else
                                                                              {
                                                                                 _loc2_ = "key";
                                                                              }
                                                                           }
                                                                           else
                                                                           {
                                                                              _loc2_ = "ban";
                                                                           }
                                                                        }
                                                                        else
                                                                        {
                                                                           _loc2_ = "inviteOnly";
                                                                        }
                                                                     }
                                                                     else
                                                                     {
                                                                        _loc2_ = "limit";
                                                                     }
                                                                     dispatch("cantJoin",false,
                                                                        {
                                                                           "reason":_loc2_,
                                                                           "channelName":fixChannelName(_loc8_[3])
                                                                        });
                                                                  }
                                                                  _loc23_ = _loc8_[7];
                                                                  if("(+l)" !== _loc23_)
                                                                  {
                                                                     if("(+i)" !== _loc23_)
                                                                     {
                                                                        if("(+b)" !== _loc23_)
                                                                        {
                                                                           if("(+k)" !== _loc23_)
                                                                           {
                                                                              _loc2_ = "unknown";
                                                                           }
                                                                           else
                                                                           {
                                                                              _loc2_ = "key";
                                                                           }
                                                                        }
                                                                        else
                                                                        {
                                                                           _loc2_ = "ban";
                                                                        }
                                                                     }
                                                                     else
                                                                     {
                                                                        _loc2_ = "inviteOnly";
                                                                     }
                                                                  }
                                                                  else
                                                                  {
                                                                     _loc2_ = "limit";
                                                                  }
                                                                  dispatch("cantJoin",false,
                                                                     {
                                                                        "reason":_loc2_,
                                                                        "channelName":fixChannelName(_loc8_[3])
                                                                     });
                                                               }
                                                               _loc23_ = _loc8_[7];
                                                               if("(+l)" !== _loc23_)
                                                               {
                                                                  if("(+i)" !== _loc23_)
                                                                  {
                                                                     if("(+b)" !== _loc23_)
                                                                     {
                                                                        if("(+k)" !== _loc23_)
                                                                        {
                                                                           _loc2_ = "unknown";
                                                                        }
                                                                        else
                                                                        {
                                                                           _loc2_ = "key";
                                                                        }
                                                                     }
                                                                     else
                                                                     {
                                                                        _loc2_ = "ban";
                                                                     }
                                                                  }
                                                                  else
                                                                  {
                                                                     _loc2_ = "inviteOnly";
                                                                  }
                                                               }
                                                               else
                                                               {
                                                                  _loc2_ = "limit";
                                                               }
                                                               dispatch("cantJoin",false,
                                                                  {
                                                                     "reason":_loc2_,
                                                                     "channelName":fixChannelName(_loc8_[3])
                                                                  });
                                                            }
                                                            else if(fixColon(_loc8_.slice(4).join(" ")) == "Nickname is already in use.")
                                                            {
                                                               dispatch("nickAlreadyInUse",false,{"nick":_loc8_[3]});
                                                            }
                                                            
                                                         }
                                                         else
                                                         {
                                                            _loc15_ = _loc8_[_loc8_.length - 1];
                                                            _loc15_ = fixChannelName(_loc15_.substring(1,_loc15_.length - 1));
                                                            if(channels[_loc15_])
                                                            {
                                                               _loc2_ = fixColon(_loc8_.slice(4).join(" "));
                                                               if(_loc2_.indexOf("You need voice") != -1)
                                                               {
                                                                  _loc2_ = "voice";
                                                               }
                                                               else if(_loc2_.indexOf("Pause Mode") != -1)
                                                               {
                                                                  _loc2_ = "pause";
                                                               }
                                                               else if(_loc2_.indexOf("slow mode") != -1)
                                                               {
                                                                  _loc2_ = "slow";
                                                               }
                                                               else if(_loc2_.indexOf("time-out") != -1)
                                                               {
                                                                  _loc2_ = "timeOut";
                                                               }
                                                               
                                                               
                                                               
                                                               channels[_loc15_].cantSend(_loc2_);
                                                            }
                                                         }
                                                      }
                                                      else
                                                      {
                                                         dispatch("motd",false,{"motd":_motd});
                                                      }
                                                   }
                                                   else
                                                   {
                                                      _motd = _motd + (_loc10_ + "\n");
                                                   }
                                                }
                                                else
                                                {
                                                   _loc15_ = fixChannelName(_loc8_[3]);
                                                   if(channels[fixChannelName(_loc8_[3])])
                                                   {
                                                      channels[_loc15_].getBan();
                                                   }
                                                }
                                             }
                                             else
                                             {
                                                _loc15_ = fixChannelName(_loc8_[3]);
                                                if(channels[fixChannelName(_loc8_[3])])
                                                {
                                                   channels[_loc15_].addBan(_loc8_.slice(4));
                                                }
                                             }
                                          }
                                          else
                                          {
                                             _loc15_ = fixChannelName(_loc8_[3]);
                                             if(channels[fixChannelName(_loc8_[3])])
                                             {
                                                channels[_loc15_].initUserListDone();
                                             }
                                          }
                                       }
                                       else
                                       {
                                          _loc15_ = fixChannelName(_loc8_[4]);
                                          if(channels[fixChannelName(_loc8_[4])])
                                          {
                                             channels[_loc15_].initUserList(_loc8_.slice(5));
                                          }
                                       }
                                    }
                                 }
                                 else
                                 {
                                    _loc15_ = fixChannelName(_loc8_[3]);
                                    if(channels[_loc15_])
                                    {
                                       channels[_loc15_].setTopic(fixColon(_loc8_.slice(4).join(" ")));
                                    }
                                 }
                              }
                              else
                              {
                                 if(_loc8_[3].indexOf("#") == -1)
                                 {
                                    return;
                                 }
                                 _loc15_ = fixChannelName(_loc8_[3]);
                                 if(channels[fixChannelName(_loc8_[3])])
                                 {
                                    parseMode(_loc15_,_loc8_[4],_loc8_.slice(5));
                                 }
                              }
                           }
                           else
                           {
                              dispatch("channelList",false,{"channels":list});
                           }
                        }
                        else
                        {
                           list.push(
                              {
                                 "id":_loc8_[3],
                                 "users":_loc8_[4]
                              });
                        }
                     }
                     else
                     {
                        list = [];
                     }
                  }
                  else
                  {
                     _loc25_ = 0;
                     _loc24_ = channels;
                     for each(_loc6_ in channels)
                     {
                        _loc6_.userHost(_loc8_[3],_loc8_[4] + "@" + _loc8_[5]);
                     }
                  }
               }
               else
               {
                  _loc8_.splice(0,3);
                  _loc8_.splice(_loc8_.indexOf(":are"));
                  _loc3_ = 0;
                  while(_loc3_ < _loc8_.length)
                  {
                     _loc8_[_loc3_] = _loc8_[_loc3_].split("=");
                     serverCapabilities[_loc8_[_loc3_][0]] = _loc8_[_loc3_][1] || true;
                     _loc23_ = _loc8_[_loc3_][0];
                     if("PREFIX" !== _loc23_)
                     {
                        if("CHANMODES" === _loc23_)
                        {
                           Channel.MODES = _loc8_[_loc3_][1];
                        }
                     }
                     else
                     {
                        User.setModes(_loc8_[_loc3_][1]);
                     }
                     _loc3_++;
                  }
               }
            }
            else
            {
               _nick = _loc8_[2];
               dispatch("ready");
               ready = true;
            }
         }
         
      }
      
      public function send(param1:String) : void {
         if(socket.connected)
         {
            Debug.echo("&gt;&gt; " + param1);
            socket.writeUTFBytes(param1 + "\r\n");
            socket.flush();
         }
      }
      
      private function parseMode(param1:String, param2:String, param3:Array) : void {
         var _loc6_:* = null;
         var _loc4_:* = null;
         var _loc5_:* = NaN;
         _loc5_ = 0.0;
         while(_loc5_ < param2.length)
         {
            _loc4_ = param2.charAt(_loc5_);
            if(_loc4_ == "+")
            {
               _loc6_ = "+";
            }
            else if(_loc4_ == "-")
            {
               _loc6_ = "-";
            }
            else if(User.MODES.indexOf(_loc4_) != -1)
            {
               channels[param1].userMode(param3.shift(),_loc4_,_loc6_ == "+");
            }
            else if(Channel.MODES.indexOf(_loc4_) != -1)
            {
               channels[param1].channelMode(_loc4_,_loc6_ == "+",hasModeParams[_loc6_] + User.MODES.indexOf(_loc4_) != -1?param3.shift():"");
            }
            
            
            
            _loc5_++;
         }
      }
      
      public function createChat(param1:String) : Chat {
         if(!chats[param1])
         {
            _loc2_ = new Chat(param1,this);
            chats[param1] = _loc2_;
            _loc2_.addEventListener("destroy",onDestroyChat);
            dispatch("createChat",false,{"chat":chats[param1]});
         }
         return chats[param1];
      }
      
      private function onDestroyChat(param1:Event) : void {
         delete chats[param1.target.id];
      }
      
      private function createChannel(param1:String) : Channel {
         if(!channels[param1])
         {
            _loc2_ = new Channel(param1,this);
            channels[param1] = _loc2_;
            _loc2_.addEventListener("destroy",onDestroyChannel);
            dispatch("createChannel",false,{"channel":channels[param1]});
            send("MODE #" + param1);
         }
         return channels[param1];
      }
      
      private function onDestroyChannel(param1:Event) : void {
         delete channels[param1.target.id];
      }
      
      public function join(param1:String) : void {
         if(!channels[param1])
         {
            send("JOIN #" + param1);
         }
      }
      
      public function get nick() : String {
         return _nick;
      }
      
      public function set nick(param1:String) : void {
         _nick = param1;
         dispatch("nick");
      }
      
      public function get password() : String {
         return _password;
      }
      
      public function set password(param1:String) : void {
         _password = param1;
      }
      
      public function get motd() : String {
         return _motd;
      }
   }
}
