package tv.ustream.irc
{
   public class User extends Object
   {
      
      public function User(param1:String) {
         var _loc2_:* = NaN;
         super();
         this.nick = param1;
         _loc2_ = 0.0;
         while(_loc2_ < prefixs.length)
         {
            if(param1.charAt(0) == prefixs.charAt(_loc2_))
            {
               mode = mode + (1 << MODES.length - _loc2_ - 1);
               prefix = prefixs.charAt(_loc2_);
               this.nick = param1.substr(1);
               break;
            }
            _loc2_++;
         }
      }
      
      static var MODES:String = "qaohv";
      
      static var prefixs:String = "~&@%+";
      
      static function setModes(param1:String) : void {
         var _loc2_:uint = param1.indexOf(")");
         if(_loc2_ != -1)
         {
            MODES = param1.substring(1,_loc2_);
            prefixs = param1.substr(_loc2_ + 1);
         }
      }
      
      public var nick:String;
      
      public var host:String;
      
      public var mode:Number = 0;
      
      public var prefix:String;
   }
}
