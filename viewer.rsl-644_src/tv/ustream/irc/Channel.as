package tv.ustream.irc
{
   import tv.ustream.data.DataProvider;
   
   public class Channel extends Chat
   {
      
      public function Channel(param1:String, param2:Irc) {
         users = {};
         bans = [];
         w84Whois2Ban = {};
         _modes = {};
         guestName = {};
         super(param1,param2);
      }
      
      static var MODES:String = "bIe,k,l.";
      
      public var users:Object;
      
      private var _usersDp:DataProvider;
      
      public var bans:Array;
      
      private var _bansDp:DataProvider;
      
      private var w84BanList:Boolean = false;
      
      private var w84Whois2Ban:Object;
      
      private var _hideGuests:Boolean = false;
      
      private var _guestCounter:int = 0;
      
      private var _topic:String;
      
      private var _modes:Object;
      
      private var guestName:Object;
      
      function initUserList(param1:Array) : void {
         var _loc4_:* = null;
         var _loc3_:Array = [];
         var _loc6_:* = 0;
         var _loc5_:* = param1;
         for each(_loc2_ in param1)
         {
            _loc2_ = Irc.fixColon(_loc2_);
            if(_loc2_)
            {
               _loc4_ = new User(_loc2_);
               if(!this.users[_loc4_.nick] && (_hideGuests && _loc4_.mode || !_hideGuests))
               {
                  this.users[_loc4_.nick] = _loc4_;
                  if(_usersDp)
                  {
                     _usersDp.addItem(userToObject(_loc4_));
                  }
                  else
                  {
                     _loc3_.push(userToObject(_loc4_));
                  }
               }
               else if(_hideGuests && !_loc4_.mode)
               {
                  guestName[_loc4_.nick] = _loc4_;
                  _guestCounter = _guestCounter + 1;
               }
               
            }
         }
         if(!_usersDp)
         {
            _usersDp = new DataProvider(_loc3_);
         }
      }
      
      function initUserListDone() : void {
         dispatch("users",false,{"users":users});
      }
      
      function setTopic(param1:String) : void {
         _topic = param1;
         dispatch("topic",false,{"topic":param1});
      }
      
      override function nick(param1:String, param2:String) : void {
         if(users[param1])
         {
            users[param1].nick = param2;
            users[param2] = users[param1];
            users[param1] = null;
            _usersDp.updateItemAtProperty("nick",param1,{"nick":param2});
         }
         if(guestName[param1])
         {
            guestName[param2] = guestName[param1];
            guestName[param1] = null;
         }
         super.nick(param1,param2);
      }
      
      private function addUser(param1:String) : User {
         if(!users[param1])
         {
            _loc2_ = new User(param1);
            users[param1] = _loc2_;
            _usersDp.addItem(userToObject(_loc2_));
         }
         return users[param1];
      }
      
      private function removeUser(param1:String) : void {
         if(users[param1])
         {
            if(_hideGuests && guestName[param1])
            {
               removeGuest(param1);
            }
            _usersDp.removeItemAtProperty("nick",param1,true);
            users[param1] = null;
         }
      }
      
      function join(param1:Array) : void {
         if(!users[param1[0]] && !_hideGuests)
         {
            addUser(param1[0]).host = param1[1];
            dispatch("join",false,{"user":users[param1[0]]});
         }
         else if(_hideGuests)
         {
            guestName[param1[0]] = true;
            _guestCounter = _guestCounter + 1;
         }
         
      }
      
      function part(param1:Array) : void {
         if(users[param1[0]] && (!hideGuests || _hideGuests && users[param1[0]].mode))
         {
            dispatch("part",false,{"user":users[param1[0]]});
            removeUser(param1[0]);
         }
         else
         {
            removeGuest(users[0]);
         }
      }
      
      function quit(param1:Array) : void {
         if(users[param1[0]] && (!hideGuests || _hideGuests && users[param1[0]].mode))
         {
            dispatch("quit",false,{"user":users[param1[0]]});
            removeUser(param1[0]);
         }
         else
         {
            removeGuest(param1[0]);
         }
      }
      
      private function removeGuest(param1:String) : void {
         if(_hideGuests && guestName[param1])
         {
            guestName[param1] = null;
            _guestCounter = _guestCounter - 1;
         }
      }
      
      function kick(param1:String, param2:String, param3:String) : void {
         if(users[param1] && (!_hideGuests || _hideGuests && users[param1].mode || users[param1] == irc.nick) || _hideGuests && param1 == irc.nick)
         {
            dispatch("kick",false,
               {
                  "user":param1,
                  "by":param2,
                  "reason":param3
               });
            removeUser(param1);
         }
         else
         {
            removeGuest(param1);
         }
      }
      
      override function getMessage(param1:String, param2:String, param3:String = "message", param4:String = null) : void {
         var _loc5_:Array = param2.split("!");
         if(_loc5_[1] && users[_loc5_[0]])
         {
            users[_loc5_[0]].host = _loc5_[1];
         }
         super.getMessage(param1,param2,param3,param4);
      }
      
      function removeMessage(param1:String, param2:String, param3:String, param4:Boolean = false) : void {
         if(users[param2] && users[param3] && users[param3].mode >= 4 || param4)
         {
            dispatch("removeMessage",false,
               {
                  "message":param1,
                  "from":param2,
                  "by":param3
               });
         }
      }
      
      function replaceMessage(param1:String, param2:String, param3:String) : void {
         if(users[param3])
         {
            dispatch("replaceMessage",false,
               {
                  "oldMessage":param1,
                  "newMessage":param2,
                  "from":param3
               });
         }
      }
      
      function cantSend(param1:String) : void {
         dispatch("cantSend",false,{"reason":param1});
      }
      
      function channelMode(param1:String, param2:Boolean, param3:* = true) : void {
         if(param2)
         {
            _modes[param1] = param3 || "+";
         }
         else
         {
            _modes[param1] = null;
            delete _modes[param1];
         }
         if(param1 == "b")
         {
            getBanList();
         }
         dispatch("mode",false,
            {
               "sign":param2,
               "mode":param1,
               "param":param3
            });
      }
      
      function userHost(param1:String, param2:String) : void {
         var _loc3_:* = false;
         if(users[param1])
         {
            users[param1].host = param2;
            usersDp.updateItemAtProperty("nick",param1,{"host":param2});
            dispatch("userHost",false,
               {
                  "nick":param1,
                  "host":param2
               });
         }
         if(w84Whois2Ban[param1])
         {
            _loc3_ = w84Whois2Ban[param1] == "kick";
            if(users[param1])
            {
               ban(param1,_loc3_);
            }
            else
            {
               irc.send("mode #" + id + " +b *!*@" + param2.split("@")[1]);
               if(_loc3_)
               {
                  irc.send("kick #" + id + " " + param1);
               }
            }
            w84Whois2Ban[param1] = null;
         }
      }
      
      function userMode(param1:String, param2:String, param3:Boolean) : void {
         var _loc6_:* = 0;
         var _loc5_:* = null;
         var _loc4_:* = NaN;
         if(users[param1] || _hideGuests)
         {
            if(!users[param1] && (_hideGuests))
            {
               addUser(param1);
            }
            _loc6_ = User.MODES.indexOf(param2);
            if(_loc6_ != -1)
            {
               removeGuest(param1);
               users[param1].mode = users[param1].mode + (1 << User.MODES.length - _loc6_ - 1) * (param3?1:-1.0);
               _loc5_ = "";
               _loc4_ = 0.0;
               while(_loc4_ < User.MODES.length)
               {
                  if(users[param1].mode >= 1 << _loc4_)
                  {
                     _loc5_ = User.prefixs.charAt(User.MODES.length - _loc4_ - 1);
                     _loc4_++;
                     continue;
                  }
                  break;
               }
               if(_hideGuests && (!users[param1] || users[param1] && !users[param1].mode) && !guestName[param1])
               {
                  _guestCounter = _guestCounter + 1;
                  guestName[param1] = users[param1];
               }
               users[param1].prefix = _loc5_;
            }
            if(_hideGuests && !users[param1].mode)
            {
               removeUser(param1);
            }
            else
            {
               _usersDp.updateItemAtProperty("nick",param1,
                  {
                     "mode":users[param1].mode,
                     "prefix":users[param1].prefix
                  });
            }
         }
      }
      
      function addBan(param1:Array) : void {
         bans.push(param1);
         var _loc2_:Object = {};
         if(param1.length)
         {
            _loc2_.host = param1.shift();
         }
         if(param1.length)
         {
            _loc2_.by = param1.shift();
         }
         if(param1.length)
         {
            _loc2_.posix = param1.shift();
         }
         _bansDp.addItem(_loc2_);
      }
      
      public function ban(param1:String, param2:Boolean = true) : void {
         if(param1 != irc.nick)
         {
            if(users[param1] && users[param1].host)
            {
               irc.send("mode #" + id + " +b *!*@" + users[param1].host.split("@")[1]);
               if(param2)
               {
                  irc.send("kick #" + id + " " + param1);
               }
            }
            else
            {
               w84Whois2Ban[param1] = param2?"kick":true;
               irc.send("whois " + param1);
            }
         }
      }
      
      function getBan() : void {
         w84BanList = false;
         dispatch("banList");
      }
      
      public function getBanList() : void {
         bans = [];
         if(_bansDp)
         {
            bansDp.clear();
         }
         else
         {
            _bansDp = new DataProvider();
         }
         if(!w84BanList)
         {
            w84BanList = true;
            irc.send("mode #" + id + " b");
         }
      }
      
      override public function destroy(... rest) : * {
         irc.send("part #" + id);
         return super.destroy();
      }
      
      public function get topic() : String {
         return _topic;
      }
      
      public function get usersDp() : DataProvider {
         return _usersDp;
      }
      
      public function get modes() : Object {
         return _modes;
      }
      
      public function get bansDp() : DataProvider {
         return _bansDp;
      }
      
      public function get hideGuests() : Boolean {
         return _hideGuests;
      }
      
      public function set hideGuests(param1:Boolean) : void {
         _hideGuests = param1;
      }
      
      public function get guestCounter() : int {
         return _guestCounter;
      }
      
      private function userToObject(param1:User) : Object {
         return 
            {
               "nick":param1.nick,
               "mode":param1.mode,
               "prefix":param1.prefix,
               "host":param1.host
            };
      }
   }
}
