package tv.ustream.irc
{
   import tv.ustream.tools.Dispatcher;
   
   public class Chat extends Dispatcher
   {
      
      public function Chat(param1:String, param2:Irc) {
         urlRx = new RegExp("\\[url=([^\\]]+)\\]([^\\[]+)\\[\\/url\\]","gi");
         urlReg = new RegExp("((((http|https|ftp):\\/\\/)|(www.))([-a-z0-9]*\\.+)?[-a-zA-Z0-9]*\\.[-a-zA-Z]+)([\\/&\\?=\\.;%\\+\\$@~][\\S]*)?","gi");
         links = [];
         _irc = param2;
         super(param1);
      }
      
      private static var MAX_LINKS:uint = 1000;
      
      protected var _irc:Irc;
      
      private var urlRx:RegExp;
      
      private var urlReg:RegExp;
      
      private var links:Array;
      
      private var actualLinkPos:uint = 0;
      
      public function send(param1:String) : void {
         if(param1)
         {
            _irc.send("privmsg " + (_type == "chat"?"":"#") + id + " :" + param1);
            getMessage(param1.replace(new RegExp("<","g"),"&lt;").replace(new RegExp(">","g"),"&gt;"),_irc.nick);
         }
      }
      
      public function getURLFormat(... rest) : String {
         if(actualLinkPos > MAX_LINKS)
         {
            actualLinkPos = 0;
         }
         links[actualLinkPos] = (rest[0].indexOf("http://") != 0?"http://":"") + rest[0];
         rest[0] = rest[0].replace(new RegExp("\\\'","g"),"&#39;").replace(new RegExp("\\\"","g"),"&#34;");
         actualLinkPos = actualLinkPos + 1;
         return "<a href=\'Event:link_" + (actualLinkPos) + "\'>" + rest[0] + "</a>";
      }
      
      public function getURL(param1:uint) : String {
         if(links.length < param1)
         {
            return "";
         }
         return links[param1];
      }
      
      function getMessage(param1:String, param2:String, param3:String = "message", param4:String = null) : void {
         var _loc5_:Array = param2.split("!");
         var param1:String = param1.replace(urlRx,"<a href=\"$1\" target=\"_blank\">$2</a>");
         if(param1.indexOf("<a href=") == -1)
         {
            param1 = param1.replace(urlReg,getURLFormat);
         }
         dispatch("message",false,
            {
               "from":_loc5_[0],
               "message":param1,
               "messageType":param3,
               "when":param4
            });
      }
      
      function nick(param1:String, param2:String) : void {
         dispatch("nick",false,
            {
               "oldNick":param1,
               "newNick":param2
            });
      }
      
      public function get irc() : Irc {
         return _irc;
      }
   }
}
