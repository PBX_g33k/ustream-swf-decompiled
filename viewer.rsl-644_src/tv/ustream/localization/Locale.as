package tv.ustream.localization
{
   import flash.events.EventDispatcher;
   import flash.errors.IllegalOperationError;
   import tv.ustream.tools.Shared;
   import flash.utils.Timer;
   import tv.ustream.tools.This;
   import flash.net.Responder;
   import flash.net.NetConnection;
   import tv.ustream.debug.Debug;
   import flash.events.Event;
   import flash.events.NetStatusEvent;
   
   public class Locale extends EventDispatcher
   {
      
      public function Locale(param1:String = null) {
         super();
         if(param1)
         {
            Locale.nameSpace = param1;
         }
         if(Locale._instance)
         {
            throw new IllegalOperationError("Use the \'instance\' property to access the singleton");
         }
         else
         {
            if(_nameSpace)
            {
               gwRecallTimer = new Timer(2 * 1000,1);
               gwRecallTimer.addEventListener("timer",callGw);
               gwTimeoutTimer = new Timer(15 * 1000,1);
               gwTimeoutTimer.addEventListener("timer",callGw);
               shared = new Shared({"defaultLanguage":"en_US"},"/locale/",_nameSpace);
               _language = desiredLanguage || defaultLanguage;
               checkStrings();
               return;
            }
            throw new ArgumentError("Set the nameSpace property before instantiation");
         }
      }
      
      public static const DEFAULT_LANGUAGE:String = "en_US";
      
      private static var desiredLanguage:String;
      
      private static var _instance:Locale;
      
      private static var _nameSpace:String = "";
      
      public static function get hasInstance() : Boolean {
         if(_instance)
         {
            return true;
         }
         return false;
      }
      
      public static function init(param1:String, param2:String = "") : void {
         if(!_instance)
         {
            if(param1)
            {
               _nameSpace = param1;
            }
            if(param2)
            {
               desiredLanguage = param2;
            }
            return;
         }
         throw new IllegalOperationError("Setting the nameSpace after instantiation is forbidden");
      }
      
      public static function get instance() : Locale {
         if(!_instance)
         {
            _instance = new Locale();
         }
         return _instance;
      }
      
      public static function get nameSpace() : String {
         return _nameSpace;
      }
      
      public static function set nameSpace(param1:String) : void {
         if(!_instance)
         {
            _nameSpace = param1;
            return;
         }
         throw new IllegalOperationError("Setting the nameSpace after instantiation is forbidden");
      }
      
      protected const GW_TIMEOUT:uint = 15;
      
      protected const GW_RECALL:uint = 2;
      
      protected var gwActive:Boolean = true;
      
      protected var shared:Shared;
      
      protected var strings:Object;
      
      protected var gwUrl:String;
      
      protected var _language:String;
      
      protected var gwRecallTimer:Timer;
      
      protected var gwTimeoutTimer:Timer;
      
      public function get defaultLanguage() : String {
         return shared.defaultLanguage;
      }
      
      public function set defaultLanguage(param1:String) : void {
         _language = param1;
         shared.defaultLanguage = param1;
         checkStrings();
      }
      
      public function get language() : String {
         return _language;
      }
      
      public function set language(param1:String) : void {
         var _loc2_:* = null;
         if(_language != param1)
         {
            _loc2_ = param1.split("_");
            if(_loc2_[1])
            {
               _loc2_[1] = (_loc2_[1] as String).toUpperCase();
            }
            _language = _loc2_.join("_");
            checkStrings();
         }
      }
      
      protected function checkStrings() : void {
         if(!shared.data)
         {
            shared.data = {};
         }
         if(!shared.data[_language])
         {
            shared.data[_language] = {};
         }
         var _loc3_:* = 0;
         var _loc2_:* = shared.data;
         for(_loc1_ in shared.data)
         {
            if(_loc1_ != _language)
            {
               delete shared.data[_loc1_];
            }
         }
         callGw();
      }
      
      protected function callGw(... rest) : void {
         var _loc2_:Object = {};
         _loc2_.namespace = _nameSpace;
         _loc2_.language = _language;
         if(shared.data[_language].hash)
         {
            _loc2_.hash = shared.data[_language].hash;
         }
         else
         {
            _loc2_.hash = "";
         }
         gwUrl = This.secure?"https://rgw.ustream.tv/gateway.php?Localization.getTranslations":"http://cdngw.ustream.tv/rgw/Localization/getTranslations?namespace=" + _loc2_.namespace + "&language=" + _loc2_.language + "&hash=" + _loc2_.hash;
         var _loc4_:Responder = new Responder(onAmfResult,onAmfStatus);
         var _loc3_:NetConnection = new NetConnection();
         _loc3_.objectEncoding = 0;
         _loc3_.addEventListener("netStatus",onGwNetStatus);
         _loc3_.addEventListener("ioError",onGwError);
         _loc3_.addEventListener("securityError",onGwError);
         _loc3_.connect(gwUrl);
         Debug.debug("locale","gwUrl : " + gwUrl);
         _loc3_.call("Localization.getTranslations",_loc4_,_loc2_);
         gwActive = true;
         gwRecallTimer.reset();
         gwTimeoutTimer.reset();
         gwTimeoutTimer.start();
      }
      
      private function onGwError(param1:Event) : void {
         Debug.debug("locale",param1.toString());
         gwTimeoutTimer.reset();
         if(shared.data[_language] && shared.data[_language].strings)
         {
            strings = shared.data[_language].strings;
            dispatchEvent(new Event("complete"));
         }
         else
         {
            gwRecallTimer.reset();
            gwRecallTimer.start();
         }
      }
      
      private function onGwNetStatus(param1:NetStatusEvent) : void {
         if(param1.info.code == "NetConnection.Call.Failed")
         {
            gwTimeoutTimer.reset();
            gwRecallTimer.reset();
            gwRecallTimer.start();
         }
      }
      
      private function onAmfStatus(param1:Event) : void {
         gwTimeoutTimer.reset();
         gwRecallTimer.reset();
         gwRecallTimer.start();
      }
      
      private function onAmfResult(param1:Object) : void {
         gwTimeoutTimer.reset();
         gwRecallTimer.reset();
         if(gwActive)
         {
            if(param1.success)
            {
               gwActive = false;
               if(shared.data[param1.language].hash != param1.hash)
               {
                  if(param1.strings)
                  {
                     Debug.debug("locale","Localization changed");
                     shared.data[param1.language].hash = param1.hash;
                     shared.data[param1.language].strings = param1.strings;
                     dispatchEvent(new Event("change"));
                  }
                  else
                  {
                     Debug.debug("locale","Localization changed, data missing.");
                     handleFailure();
                  }
               }
               else
               {
                  Debug.debug("locale","Localization up to date");
               }
               strings = shared.data[_language].strings;
               if(_language == param1.language)
               {
                  dispatchEvent(new Event("complete"));
               }
            }
            else
            {
               Debug.debug("locale","Call unsuccessful.",param1);
               handleFailure();
            }
         }
      }
      
      private function handleFailure() : void {
         if(shared.data[_language].strings)
         {
            Debug.debug("locale","Using locally stored keys");
            strings = shared.data[_language].strings;
            dispatchEvent(new Event("complete"));
         }
         else
         {
            Debug.debug("locale","Recalling gw in 2 seconds");
            gwRecallTimer.start();
         }
      }
      
      public function hasLabel(param1:String) : Boolean {
         var _loc2_:Boolean = strings && !(strings[param1] == null);
         if(!_loc2_)
         {
            Debug.debug("locale","hasLabel error: " + param1);
         }
         return _loc2_;
      }
      
      public function label(param1:String, param2:Object = null) : String {
         var _loc3_:* = null;
         if(strings && strings[param1])
         {
            _loc3_ = strings[param1];
            if(param2)
            {
               _loc6_ = 0;
               _loc5_ = param2;
               for(_loc4_ in param2)
               {
                  _loc3_ = _loc3_.split("#" + _loc4_ + "#").join(param2[_loc4_]);
               }
            }
            return _loc3_;
         }
         return "!" + param1;
      }
      
      public function showLang() : void {
         Debug.debug("locale","---- LANG -----",strings);
         Debug.debug("locale","-------------");
      }
      
      public function get hash() : String {
         return shared.data[_language].hash;
      }
   }
}
