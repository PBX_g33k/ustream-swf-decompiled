package tv.ustream.cc
{
   import flash.display.DisplayObject;
   import flash.geom.Rectangle;
   
   public interface ICCDecoder
   {
      
      function get display() : DisplayObject;
      
      function get enabled() : Boolean;
      
      function set enabled(param1:Boolean) : void;
      
      function captionInfo(param1:Object = null) : void;
      
      function setVideoBounds(param1:Rectangle) : void;
   }
}
