package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.ModuleManager;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.viewer.logic.media.Recorded;
   
   public class ViewerModuleManager extends ModuleManager
   {
      
      public function ViewerModuleManager(param1:* = null, param2:String = null, param3:String = null) {
         super(param1);
         parseOrder = ["broadcaster","meta","ppv","stream","tcdn","poll"];
      }
      
      private var skipFmsPoll:Boolean = false;
      
      override protected function info(param1:Object, param2:String) : void {
         if(param1 is Number)
         {
            return;
         }
         if(param1.poll != undefined)
         {
            if(param2 == "fms" && (skipFmsPoll))
            {
               delete param1.poll;
            }
            else if(param2 == "ums")
            {
               skipFmsPoll = true;
            }
            
         }
         if(param1.stream === true)
         {
            delete param1.stream;
         }
         if(param1.ppv === true)
         {
            delete param1.ppv;
         }
         if(Logic.instance.channel && Logic.instance.channel.id == "8914332")
         {
            delete param1.allTimeTotal;
         }
         if(!(param2 == "ums") && logic is Recorded && logic.ums && logic.ums.connected)
         {
            delete param1.viewers;
            delete param1.allViewers;
            delete param1.allTimeTotal;
            delete param1.multiCam;
         }
         super.info(param1,param2);
      }
      
      public function get smooth() : Smooth {
         return _modules["smooth"];
      }
      
      public function get viewers() : Viewers {
         return _modules["viewers"];
      }
      
      public function get multiCam() : MultiCam {
         return _modules["multicam"];
      }
      
      public function get allViewers() : AllViewers {
         return _modules["allviewers"];
      }
      
      public function get allTimeTotal() : AllTimeTotal {
         return _modules["alltimetotal"];
      }
      
      public function get poll() : Poll {
         return _modules["poll"];
      }
      
      public function get layers() : Layers {
         return _modules["layers"];
      }
      
      public function get cohost() : Cohost {
         return _modules["cohost"];
      }
      
      public function get logo() : Logo {
         return _modules["logo"];
      }
      
      public function get skin() : Skin {
         return _modules["skin"];
      }
      
      public function get meta() : Meta {
         return _modules["meta"];
      }
      
      public function get share() : Share {
         return _modules["share"];
      }
      
      public function get buffer() : Buffer {
         return _modules["buffer"];
      }
      
      public function get comments() : Comments {
         return _modules["comments"];
      }
      
      public function get admin() : Admin {
         return _modules["admin"];
      }
      
      public function get gps() : Gps {
         return _modules["gps"];
      }
      
      public function get transform() : Transform {
         return _modules["transform"];
      }
      
      public function get advert() : Advert {
         return _modules["advert"];
      }
      
      public function get streamModuleInfo() : StreamModuleInfo {
         return _modules["streammoduleinfo"];
      }
      
      public function get ppv() : Ppv {
         return _modules["ppv"];
      }
      
      public function get ageVerification() : AgeVerification {
         return _modules["ageverification"];
      }
      
      public function get ageLock() : AgeLock {
         return _modules["agelock"];
      }
      
      public function get started() : Started {
         return _modules["started"];
      }
      
      public function get rss() : Rss {
         return _modules["rss"];
      }
      
      public function get stream() : Stream {
         return _modules["stream"];
      }
      
      public function get qos() : Qos {
         return _modules["qos"];
      }
      
      public function get quality() : Quality {
         return _modules["quality"];
      }
      
      public function get tcdn() : Tcdn {
         return _modules["tcdn"];
      }
      
      public function get rpinLock() : RpinLock {
         return _modules["rpinlock"];
      }
      
      public function get utcp() : Utcp {
         return _modules["utcp"];
      }
      
      public function get logicloadtest() : Logicloadtest {
         return _modules["logicloadtest"];
      }
      
      public function get closedCaption() : ClosedCaption {
         return _modules["closedcaption"];
      }
      
      public function get dvr() : Dvr {
         return _modules["dvr"];
      }
      
      public function get broadcaster() : Broadcaster {
         return _modules["broadcaster"];
      }
      
      public function get thumbnail() : Thumbnail {
         return _modules["thumbnail"];
      }
      
      public function get logCollector() : LogCollector {
         return _modules["logcollector"];
      }
      
      public function get clientRateLimit() : ClientRateLimit {
         return _modules["clientratelimit"];
      }
   }
}
