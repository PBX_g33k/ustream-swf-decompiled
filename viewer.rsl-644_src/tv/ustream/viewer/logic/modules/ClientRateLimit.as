package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.debug.Debug;
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.modules.ModuleManager;
   
   public class ClientRateLimit extends Module
   {
      
      public function ClientRateLimit(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      private const IMMEDIATE_DEFAULT_VALUE:uint = 1;
      
      private const DEFERRED_DEFAULT_VALUE:uint = 5;
      
      private var _immediate:uint = 1;
      
      private var _deferred:uint = 5;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1)
         {
            if(param1.immediate != undefined)
            {
               immediate = param1.immediate;
            }
            if(param1.deferred != undefined)
            {
               deferred = param1.deferred;
            }
         }
      }
      
      protected function get immediate() : uint {
         return _immediate;
      }
      
      protected function set immediate(param1:uint) : void {
         Debug.debug("CLIENTRATELIMIT","set immediate : " + param1);
         _immediate = param1;
         if(logic is Recorded && (logic as Recorded).statistics)
         {
            (logic as Recorded).statistics.aggregatorImmediateDelay = _immediate;
         }
      }
      
      protected function get deferred() : uint {
         return _deferred;
      }
      
      protected function set deferred(param1:uint) : void {
         Debug.debug("CLIENTRATELIMIT","set deferred : " + param1);
         _deferred = param1;
         if(logic is Recorded && (logic as Recorded).statistics)
         {
            (logic as Recorded).statistics.aggregatorDeferredDelay = _deferred;
         }
      }
      
      override public function destroy(... rest) : * {
         immediate = 1;
         deferred = 5;
         return super.destroy();
      }
   }
}
