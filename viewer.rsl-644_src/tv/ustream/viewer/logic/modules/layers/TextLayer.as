package tv.ustream.viewer.logic.modules.layers
{
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.text.StyleSheet;
   import flash.filters.DropShadowFilter;
   import flash.geom.Rectangle;
   import tv.ustream.viewer.logic.modules.Layers;
   
   public dynamic class TextLayer extends Layer
   {
      
      public function TextLayer(param1:Object = null, param2:Layers = null) {
         tf = new TextField();
         tf.selectable = false;
         scroll = _scroll;
         format = new TextFormat("Verdana",10);
         format.target = "_blank";
         tf.defaultTextFormat = format;
         style = new StyleSheet();
         style.parseCSS("a:hover{text-decoration:underline}");
         super(param1,param2);
         addChild(tf);
      }
      
      public static const publicTextProperties:Object = 
         {
            "text":1,
            "htmlText":1
         };
      
      private var tf:TextField;
      
      private var format:TextFormat;
      
      private var _scroll:Number = 0;
      
      private var offset:Number = 0;
      
      private var bgAlpha:Number = 0;
      
      private var bgColor:Number = 0;
      
      private var style:StyleSheet;
      
      private var _url:String;
      
      private var isLink:Boolean = false;
      
      private var isUnderline:Boolean = false;
      
      private var allowLinks:Boolean = false;
      
      override public function update(param1:Object) : void {
         super.update(param1);
         var _loc8_:* = 0;
         var _loc7_:* = param1;
         for(_loc2_ in param1)
         {
            if(_loc2_ == "url" && (format.hasOwnProperty(_loc2_)))
            {
               format[_loc2_] = param1[_loc2_];
               if(param1[_loc2_])
               {
                  isLink = true;
                  format["underline"] = true;
                  continue;
               }
               isLink = false;
               if(!isUnderline)
               {
                  format["underline"] = false;
                  continue;
               }
            }
            if(_loc2_ == "allowLinks")
            {
               allowLinks = param1[_loc2_];
               if(!allowLinks)
               {
                  isLink = false;
               }
            }
            else
            {
               if(_loc2_ == "underline")
               {
                  isUnderline = param1[_loc2_];
                  if(!param1[_loc2_] && (isLink))
                  {
                     format[_loc2_] = true;
                     continue;
                  }
               }
               if(format.hasOwnProperty(_loc2_))
               {
                  try
                  {
                     format[_loc2_] = param1[_loc2_];
                  }
                  catch(e:Error)
                  {
                  }
               }
               else if(publicTextProperties[_loc2_])
               {
                  try
                  {
                     tf[_loc2_] = param1[_loc2_];
                  }
                  catch(e:Error)
                  {
                     continue;
                  }
               }
               
            }
         }
         tf.styleSheet = null;
         _loc8_ = format;
         tf.defaultTextFormat = _loc8_;
         tf.setTextFormat(_loc8_);
         if(param1.shadow != null)
         {
            tf.filters = param1.shadow?[new DropShadowFilter(0,0,param1.shadowColor || 0,1,4,4,1,3)]:[];
         }
         if(!(param1.background == null) || !(param1.backgroundColor == null))
         {
            bgAlpha = param1.background != null?param1.background?1:0.0:bgAlpha;
            bgColor = param1.backgroundColor != null?param1.backgroundColor:bgColor;
            renderBg();
         }
      }
      
      override public function render() : void {
         renderBg();
         if(!_scroll)
         {
            tf.width = _width;
            tf.height = _height;
         }
         scrollRect = new Rectangle(0,0,_width,_height);
      }
      
      public function set scroll(param1:Number) : void {
         _scroll = param1;
         if(_scroll)
         {
            tf.autoSize = "left";
            if(!hasEventListener("enterFrame"))
            {
               addEventListener("enterFrame",onEnterFrame);
            }
         }
         else
         {
            tf.autoSize = "none";
            tf.width = _width;
            tf.height = _height;
            if(hasEventListener("enterFrame"))
            {
               removeEventListener("enterFrame",onEnterFrame);
            }
            tf.x = 0;
         }
         var _loc2_:* = !(_scroll);
         tf.wordWrap = _loc2_;
         tf.multiline = _loc2_;
      }
      
      private function onEnterFrame(... rest) : void {
         tf.x = tf.x - _scroll;
         if(tf.x < -tf.width)
         {
            tf.x = _width;
         }
         else if(tf.x > _width)
         {
            tf.x = -tf.width;
         }
         
      }
      
      override protected function renderBg() : void {
         bg.graphics.clear();
         bg.graphics.beginFill(bgColor,bgAlpha);
         bg.graphics.drawRect(0,0,_width * 2,_height);
         bg.graphics.endFill();
      }
      
      public function destroy() : void {
         scroll = 0;
      }
   }
}
