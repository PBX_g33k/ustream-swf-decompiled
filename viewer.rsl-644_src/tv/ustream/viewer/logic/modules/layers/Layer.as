package tv.ustream.viewer.logic.modules.layers
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import tv.ustream.tools.Drag;
   import tv.ustream.viewer.logic.modules.Layers;
   import flash.events.Event;
   import flash.display.DisplayObject;
   import tv.ustream.viewer.logic.media.Recorded;
   
   public dynamic class Layer extends Sprite
   {
      
      public function Layer(param1:Object = null, param2:Layers = null) {
         super();
         bg = new Shape();
         addChild(new Shape());
         var _loc3_:* = param1?param1:{};
         this.data = _loc3_;
         update(_loc3_);
         render();
         addEventListener("added",onAdded);
         addEventListener("update",onAdded);
         this.manager = param2;
         if(this.manager)
         {
            param2.addEventListener("size",updateDrag);
         }
      }
      
      protected var _width:Number = 320;
      
      protected var _height:Number = 240;
      
      protected var _logic;
      
      protected var data:Object;
      
      protected var bg:Shape;
      
      public var type:String;
      
      private var _container:Sprite;
      
      private var drag:Drag;
      
      private var manager:Layers;
      
      private function onAdded(param1:Event) : void {
         if(param1.target.parent == this)
         {
            param1.target.width = _width;
            param1.target.height = _height;
         }
         param1.stopImmediatePropagation();
      }
      
      public function update(param1:Object) : void {
         if(param1)
         {
            _loc8_ = 0;
            _loc7_ = param1;
            for(_loc2_ in param1)
            {
               this.data[_loc2_] = param1[_loc2_];
               if(!((_loc2_ == "x" || _loc2_ == "y") && _container))
               {
                  try
                  {
                     this[_loc2_] = param1[_loc2_];
                  }
                  catch(e:Error)
                  {
                  }
               }
               else
               {
                  _container[_loc2_] = param1[_loc2_];
               }
               if(logic && param1[_loc2_])
               {
                  try
                  {
                     logic[_loc2_] = param1[_loc2_];
                     trace(logic[_loc2_]);
                  }
                  catch(e:Error)
                  {
                     trace("SET " + _loc2_ + " error : " + e);
                     continue;
                  }
               }
            }
            dispatchEvent(new Event("update"));
         }
      }
      
      public function render() : void {
         renderBg();
         var c:Number = 0.0;
         while(c < numChildren)
         {
            child = getChildAt(c);
            with(getChildAt(c))
            {
            }
            width = _width;
            height = _height;
            }
            c = c + 1;
         }
      }
      
      protected function renderBg() : void {
         bg.graphics.clear();
         bg.graphics.beginFill(0,0);
         bg.graphics.drawRect(0,0,_width,_height);
         bg.graphics.endFill();
      }
      
      private function onLogicVideo(... rest) : void {
         this.visible = _logic.video;
      }
      
      private function dispatchResize() : void {
         dispatchEvent(new Event("resize",false,false));
         updateDrag();
      }
      
      private function updateDrag(... rest) : void {
         if(manager && drag)
         {
            drag.right = manager.width - _width;
            drag.bottom = manager.height - _height;
         }
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         _width = param1;
         render();
         dispatchResize();
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         _height = param1;
         render();
         dispatchResize();
      }
      
      public function set logic(param1:*) : void {
         _logic = param1;
         if(param1 is Recorded)
         {
            param1.addEventListener("video",onLogicVideo);
         }
         update(data);
      }
      
      public function get logic() : * {
         return _logic;
      }
      
      public function get container() : Sprite {
         return _container;
      }
      
      public function set container(param1:Sprite) : void {
         var _loc2_:* = null;
         _container = param1;
         if(_container)
         {
            _container.x = x;
            _container.y = y;
            this.x = 0;
            this.y = 0;
            _loc2_ = _container;
            if(_container.hasOwnProperty("drag") && _container["drag"] && _container["drag"] is DisplayObject)
            {
               _loc2_ = _container["drag"];
            }
            drag = new Drag(_container,_loc2_);
            _loc3_ = 0;
            drag.top = _loc3_;
            drag.left = _loc3_;
            dispatchResize();
            drag.addEventListener("startDrag",eventProxy);
            drag.addEventListener("drag",eventProxy);
            updateDrag();
         }
      }
      
      public final function get connected() : Boolean {
         return manager?manager.connected:false;
      }
      
      private function eventProxy(... rest) : void {
         if(rest.length && rest[0].hasOwnProperty("type"))
         {
            dispatchEvent(new Event(rest[0].type));
         }
      }
      
      public function get layer() : Boolean {
         return false;
      }
   }
}
