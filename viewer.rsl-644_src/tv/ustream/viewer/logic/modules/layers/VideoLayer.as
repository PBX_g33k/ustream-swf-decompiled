package tv.ustream.viewer.logic.modules.layers
{
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.viewer.logic.modules.Layers;
   
   public dynamic class VideoLayer extends Layer
   {
      
      public function VideoLayer(param1:Object = null, param2:Layers = null) {
         super(param1,param2);
      }
      
      public var needProgress:Boolean = false;
      
      override public function set logic(param1:*) : void {
         .super.logic = param1;
         if(_logic && (_logic.type == "youtube" || _logic.type == "recorded"))
         {
            _logic.addEventListener("destroy",onDestroy);
            _logic.addEventListener("createStream",onCreateStream);
         }
      }
      
      private function onCreateStream(... rest) : void {
         addEventListener("enterFrame",onEnterFrame);
      }
      
      private function onDestroy(... rest) : void {
         if(hasEventListener("enterFrame"))
         {
            removeEventListener("enterFrame",onEnterFrame);
         }
      }
      
      private function onEnterFrame(... rest) : void {
         if(_logic.duration && logic.time && logic.playing && needProgress)
         {
            dispatchEvent(new DynamicEvent("progress",false,true,{"progress":logic.time / logic.duration}));
         }
      }
   }
}
