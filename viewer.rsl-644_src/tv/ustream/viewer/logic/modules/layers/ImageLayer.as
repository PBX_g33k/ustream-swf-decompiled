package tv.ustream.viewer.logic.modules.layers
{
   import flash.display.Loader;
   import tv.ustream.tools.Display;
   import flash.system.LoaderContext;
   import flash.events.Event;
   import flash.display.Bitmap;
   import flash.net.URLRequest;
   import tv.ustream.viewer.logic.modules.Layers;
   
   public dynamic class ImageLayer extends Layer
   {
      
      public function ImageLayer(param1:Object = null, param2:Layers = null) {
         context = new LoaderContext(true);
         super(param1,param2);
         display = new Display();
         addChild(new Display());
         loader = new Loader();
         loader.contentLoaderInfo.addEventListener("complete",onLoadComplete);
         src = _src;
      }
      
      private var loader:Loader;
      
      private var display:Display;
      
      private var _src:String;
      
      private var context:LoaderContext;
      
      private function onLoadComplete(param1:Event) : void {
         (param1.target.loader.content as Bitmap).smoothing = true;
         display.addChild(loader);
         display.update();
      }
      
      override public function render() : void {
         graphics.clear();
         graphics.beginFill(16711680,0);
         graphics.drawRect(0,0,_width,_height);
         if(display)
         {
            display.width = _width;
            display.height = _height;
         }
      }
      
      override public function update(param1:Object) : void {
         super.update(param1);
         if(param1.src)
         {
            src = param1.src;
         }
      }
      
      public function get src() : String {
         return _src;
      }
      
      public function set src(param1:String) : void {
         _src = param1;
         if(loader)
         {
            loader.load(new URLRequest(param1),context);
         }
      }
   }
}
