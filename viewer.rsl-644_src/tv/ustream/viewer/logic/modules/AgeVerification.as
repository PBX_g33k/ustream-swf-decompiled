package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.Shared;
   import flash.events.Event;
   import tv.ustream.modules.ModuleManager;
   
   public class AgeVerification extends Module
   {
      
      public function AgeVerification(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         shared = new Shared();
         super(param1,param2,param3);
         mustBeOnline = false;
         _lock = true;
         param3.addEventListener("createAgeVerification",onSelfCreate);
      }
      
      private var _minage:Number;
      
      private var _servertime:Number;
      
      private var shared:Shared;
      
      private function onSelfCreate(param1:Event) : void {
         manager.removeEventListener("createAgeVerification",onSelfCreate);
         info(initValue);
      }
      
      override protected function info(param1:*) : void {
         if(typeof param1 == "object")
         {
            super.info(param1);
            if(param1.minAge)
            {
               _minage = param1.minAge;
            }
            if(param1.serverTime)
            {
               _servertime = param1.serverTime < 1.0E10?param1.serverTime * 1000:param1.serverTime;
            }
            dispatch("update");
            dispatch("lock");
            if(shared.verifiedAge)
            {
               echo("verifying age from previous entry");
               setAge(shared.verifiedAge);
            }
         }
      }
      
      public function setAge(param1:Number) : void {
         if(verifyAge(param1))
         {
            dispatch(_lock?"badAge":"ageOk");
            dispatch("lock");
         }
      }
      
      private function verifyAge(param1:Number) : Boolean {
         if(isNaN(param1) || (isNaN(_minage)) || (isNaN(_servertime)))
         {
            return false;
         }
         var _loc2_:Date = new Date();
         _loc2_.time = _servertime;
         _loc2_.fullYear = _loc2_.getFullYear() - _minage;
         _lock = _loc2_.time < param1;
         if(!_lock)
         {
            shared.verifiedAge = param1;
         }
         return true;
      }
      
      public function get minage() : Number {
         return _minage;
      }
      
      public function get servertime() : Number {
         return _servertime;
      }
   }
}
