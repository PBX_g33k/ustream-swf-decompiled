package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.net.URLStream;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.tools.StringUtils;
   import flash.net.URLRequest;
   import flash.utils.getTimer;
   import flash.events.ProgressEvent;
   import flash.events.ErrorEvent;
   import flash.events.HTTPStatusEvent;
   import flash.events.Event;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.system.Capabilities;
   import flash.external.ExternalInterface;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.modules.ModuleManager;
   
   public class Utcp extends Module
   {
      
      public function Utcp(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         if(param1 is Channel)
         {
            (param1 as Channel).addEventListener("online",destroy);
         }
      }
      
      private var testId:String;
      
      private var list:Array;
      
      private var loader:URLStream;
      
      private var loading:Boolean = false;
      
      private var index:uint = 0;
      
      private var loadStamp:uint = 0;
      
      private var progressStamp:uint = 0;
      
      private var completeStamp:uint = 0;
      
      private var results:Array;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1 is Object)
         {
            if(param1.probability && Math.random() < param1.probability)
            {
               testId = param1.testId;
               list = param1.urls as Array;
               startMeasurement();
            }
            else
            {
               echo("improbable");
            }
         }
      }
      
      override public function destroy(... rest) : * {
         if(logic is Channel)
         {
            (logic as Channel).removeEventListener("online",destroy);
         }
         stopMeasurement();
         return super.destroy();
      }
      
      private function startMeasurement() : void {
         echo("startMeasurement");
         if(loading || index)
         {
            stopMeasurement();
         }
         if(list && list.length)
         {
            index = 0;
            results = [];
            load(list[index]);
         }
         else
         {
            echo("url list has no items");
         }
      }
      
      private function load(param1:String) : void {
         var param1:String = param1 + (param1.indexOf("?") == -1?"?":"&") + "foo=" + StringUtils.random(16);
         echo("load " + param1);
         createLoader().load(new URLRequest(param1));
         loading = true;
         loadStamp = getTimer();
      }
      
      private function createLoader() : URLStream {
         if(loader)
         {
            destroyLoader();
         }
         loader = new URLStream();
         loader.addEventListener("complete",onLoaderComplete);
         loader.addEventListener("httpStatus",onLoaderStatus);
         loader.addEventListener("ioError",onLoaderError);
         loader.addEventListener("progress",onLoaderProgress);
         loader.addEventListener("securityError",onLoaderError);
         return loader;
      }
      
      private function destroyLoader() : void {
         if(loader)
         {
            try
            {
               loader.close();
            }
            catch(err:Error)
            {
            }
            loader.removeEventListener("complete",onLoaderComplete);
            loader.removeEventListener("httpStatus",onLoaderStatus);
            loader.removeEventListener("ioError",onLoaderError);
            loader.removeEventListener("progress",onLoaderProgress);
            loader.removeEventListener("securityError",onLoaderError);
            loader = null;
         }
         loading = false;
      }
      
      private function onLoaderProgress(param1:ProgressEvent) : void {
         loader.removeEventListener("progress",onLoaderProgress);
         progressStamp = getTimer();
         echo("onLoaderProgress " + (progressStamp - loadStamp) + " ms");
      }
      
      private function onLoaderError(param1:ErrorEvent) : void {
         echo("onLoaderError : " + param1.toString());
         loadStamp = 0;
         progressStamp = 0;
         completeStamp = 0;
         process();
      }
      
      private function onLoaderStatus(param1:HTTPStatusEvent) : void {
         echo("onLoaderStatus : " + param1.status);
      }
      
      private function onLoaderComplete(param1:Event) : void {
         completeStamp = getTimer();
         echo("onLoaderComplete " + (completeStamp - loadStamp) + " ms");
         process();
      }
      
      private function stopMeasurement() : void {
         echo("stopMeasurement");
         destroyLoader();
         index = 0;
      }
      
      private function process() : void {
         var _loc4_:* = 0;
         var _loc3_:* = 0;
         var _loc2_:* = null;
         var _loc1_:Object = {};
         if(loadStamp)
         {
            _loc1_.firstByte = progressStamp - loadStamp;
            _loc1_.complete = completeStamp - loadStamp;
         }
         else
         {
            _loc1_.firstByte = -1;
            _loc1_.complete = -1;
         }
         _loc1_.url = list[index];
         results.push(_loc1_);
         index = index + 1;
         if(index < list.length)
         {
            load(list[index]);
         }
         else
         {
            _loc4_ = 0;
            _loc3_ = 0;
            while(_loc3_ < results.length)
            {
               _loc4_ = _loc4_ + results[_loc3_].complete;
               _loc3_++;
            }
            if(_loc4_ > 0 && logic is Recorded)
            {
               _loc2_ = {};
               _loc2_.results = results;
               _loc2_.testId = testId;
               _loc2_.os = Capabilities.os;
               _loc2_.player = Capabilities.version;
               if(ExternalInterface.available)
               {
                  try
                  {
                     _loc2_.browser = ExternalInterface.call("window.navigator.userAgent.toString");
                  }
                  catch(err:Error)
                  {
                  }
               }
               (logic as Recorded).statistics.benchmark.dispatchEvent(new DynamicEvent("change",false,false,{"benchmark":{"utcp":_loc2_}}));
            }
            destroy();
         }
      }
   }
}
