package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.display.Sprite;
   import flash.events.Event;
   import tv.ustream.viewer.logic.modules.layers.*;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.net.Connection;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.display.DisplayObject;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.modules.ModuleManager;
   
   public dynamic class Layers extends Module
   {
      
      public function Layers(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         items = [];
         classes = 
            {
               "text":TextLayer,
               "youtube":VideoLayer,
               "recorded":VideoLayer,
               "image":ImageLayer
            };
         streamLayers = 
            {
               "channel":1,
               "youtube":1,
               "viewer":1,
               "recorded":1
            };
         super(param1,param2,param3);
         mustBeOnline = false;
         var _loc5_:* = false;
         if(param2)
         {
            _loc5_ = true;
            _loc7_ = 0;
            _loc6_ = param2;
            for(_loc4_ in param2)
            {
               if(_loc4_ != "remove")
               {
                  _loc5_ = false;
                  break;
               }
            }
         }
         _display = new Sprite();
         setDisplaySize(_width,_height);
         if(_loc5_)
         {
            param3.addEventListener("createLayers",onSelfCreate);
         }
      }
      
      protected var _display:Sprite;
      
      protected var items:Array;
      
      private var _volume:Number = 1;
      
      private var _muted:Boolean = false;
      
      private var classes:Object;
      
      private var streamLayers:Object;
      
      private var _playing:Boolean = true;
      
      protected var destroyIfEmpty:Boolean = true;
      
      private var _destroying:Boolean = false;
      
      protected var _width:Number = 320;
      
      protected var _height:Number = 240;
      
      private function onSelfCreate(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         _loc2_.removeEventListener("createLayers",onSelfCreate);
         dispatch("cleanup");
         destroy();
      }
      
      override protected function info(param1:*) : void {
         var _loc5_:* = NaN;
         super.info(param1);
         if(param1.size)
         {
            setDisplaySize(param1.size.width,param1.size.height);
         }
         if(param1.add)
         {
            _loc7_ = 0;
            _loc6_ = param1.add;
            for(_loc2_ in param1.add)
            {
               addItem(_loc2_,param1.add[_loc2_]);
            }
         }
         if(param1.update)
         {
            _loc9_ = 0;
            _loc8_ = param1.update;
            for(_loc4_ in param1.update)
            {
               updateItem(_loc4_,param1.update[_loc4_]);
            }
         }
         if(param1.remove)
         {
            _loc11_ = 0;
            _loc10_ = param1.remove;
            for each(_loc3_ in param1.remove)
            {
               removeItem(_loc3_);
            }
         }
         if(param1.order)
         {
            dispatch("order");
            if(param1.order.length <= _display.numChildren)
            {
               _loc5_ = 0.0;
               while(_loc5_ < param1.order.length)
               {
                  if(items[param1.order[_loc5_]])
                  {
                     _display.setChildIndex(items[param1.order[_loc5_]],_loc5_);
                  }
                  _loc5_++;
               }
            }
         }
      }
      
      public function infoGate(param1:*) : void {
         info(param1);
      }
      
      protected function addItem(param1:String, param2:Object) : void {
         var _loc7_:* = null;
         var _loc4_:* = null;
         var _loc5_:* = null;
         var _loc6_:* = null;
         var _loc3_:* = undefined;
         if(param2 && param2.type == "channel" && Channel && (!(Channel.channels.indexOf(param2.cid) == -1) || !(Channel.channels.indexOf(param2.id) == -1)))
         {
            removeItem(param2.id);
         }
         if(!items[param1])
         {
            _loc8_ = classes[param2.type]?new classes[param2.type](param2,this):new Layer(param2,this);
            items[param1] = _loc8_;
            _loc7_ = _loc8_;
            if(param2.type == "text")
            {
               try
               {
                  items[param1].update({"playing":true});
               }
               catch(e:Error)
               {
               }
            }
            _loc7_.name = param1;
            if(streamLayers[param2.type])
            {
               _loc7_.originalVolume = param2.volume != null?param2.volume:1;
               param2.volume = _loc7_.originalVolume * _volume;
               _loc7_.originalPlaying = param2.playing != null?param2.playing:true;
               param2.playing = _loc7_.originalPlaying && _playing;
               _loc7_.audioEnabled = param2.audio;
               _loc7_.videoEnabled = param2.video;
               _loc10_ = param2.type;
               if("channel" !== _loc10_)
               {
                  if("viewer" !== _loc10_)
                  {
                     if("recorded" === _loc10_)
                     {
                        _loc6_ = new Recorded(param2.id,param2.playing);
                        _loc6_.layer = _loc7_;
                        _loc7_.logic = _loc6_;
                        _loc7_.addChild(_loc6_.media);
                        _loc6_.audio = param2.audio;
                        _loc6_.video = param2.video;
                     }
                  }
                  else
                  {
                     _loc5_ = new Channel(null);
                     _loc5_.setEchoPrefix("COHOST_VIEWER");
                     _loc5_.layer = _loc7_;
                     _loc7_.logic = _loc5_;
                     _loc7_.addChild(_loc5_.media);
                     if(_connection is Connection)
                     {
                        _loc5_.puppet(_connection as Connection,"cohost/" + param1,false);
                     }
                     _loc5_.audio = param2.audio;
                     _loc5_.video = param2.video;
                  }
               }
               else
               {
                  _loc4_ = new Channel(param2.id || param2.cid,param2.playing);
                  _loc4_.setEchoPrefix("COHOST_CHANNEL");
                  _loc4_.layer = _loc7_;
                  _loc7_.logic = _loc4_;
                  _loc4_.addEventListener("audio",onChannelAudio);
                  _loc4_.addEventListener("offline",onChannelOffline);
                  _loc7_.addChild(_loc4_.media);
                  _loc4_.audio = param2.audio;
                  _loc4_.video = param2.video;
               }
            }
            if(param2.container)
            {
               _loc3_ = new param2.container();
               if(!(_loc3_ is DisplayObject))
               {
                  _loc3_ = null;
               }
            }
            if(_loc3_)
            {
               _loc7_.container = _loc3_;
               _loc7_.container.name = param1;
               if(_loc7_.container.hasOwnProperty("drag") && _loc7_.container["drag"] && _loc7_.container["drag"] is DisplayObject)
               {
                  _loc7_.container["drag"].name = param1;
               }
               _display.addChild(_loc3_);
               if(_loc3_.hasOwnProperty("layer"))
               {
                  _loc3_.layer = _loc7_;
               }
               _loc7_.addEventListener("drag",onLayerDrag);
            }
            else
            {
               _display.addChild(_loc7_);
            }
            dispatch("add",false,
               {
                  "id":param1,
                  "layer":param2
               });
         }
      }
      
      protected function onLayerDrag(param1:Event) : void {
      }
      
      protected function updateItem(param1:String, param2:Object) : void {
         if(items[param1])
         {
            if(param2.hasOwnProperty("volume"))
            {
               items[param1].originalVolume = param2.volume;
               param2.volume = items[param1].originalVolume * _volume;
            }
            if(param2.hasOwnProperty("playing"))
            {
               items[param1].originalPlaying = param2.playing;
               param2.playing = items[param1].originalPlaying && _playing;
            }
            if(param2.hasOwnProperty("video"))
            {
               if(items[param1].type == "viewer")
               {
                  items[param1].logic.video = param2.video;
               }
               items[param1].videoEnabled = param2.video;
            }
            if(param2.hasOwnProperty("audio"))
            {
               if(items[param1].type == "viewer")
               {
                  items[param1].logic.audio = param2.audio;
               }
               items[param1].audioEnabled = param2.audio;
            }
            items[param1].update(param2);
            dispatch("update",false,
               {
                  "id":param1,
                  "layer":param2
               });
         }
      }
      
      protected function removeItem(param1:String) : void {
         var _loc2_:* = NaN;
         if(items[param1])
         {
            _loc2_ = Channel.channels.indexOf(items[param1].id);
            if(_loc2_ != -1)
            {
               Channel.channels.splice(_loc2_,1);
            }
            dispatch("remove",false,
               {
                  "id":param1,
                  "layer":items[param1]
               });
            if(_display.contains(items[param1]))
            {
               if(items[param1].container && _display.contains(items[param1].container))
               {
                  _display.removeChild(items[param1].container);
               }
               else
               {
                  _display.removeChild(items[param1]);
               }
            }
            if(items[param1].logic)
            {
               items[param1].logic.destroy();
            }
            else
            {
               try
               {
                  items[param1].destroy();
               }
               catch(e:Error)
               {
               }
            }
            items[param1] = null;
            if(!_display.numChildren && (destroyIfEmpty))
            {
               destroy();
            }
         }
      }
      
      protected function setDisplaySize(param1:Number, param2:Number) : void {
         var _loc3_:Number = display.width;
         var _loc4_:Number = display.height;
         _display.graphics.clear();
         _display.graphics.beginFill(16711680,0);
         _width = param1;
         _height = param2;
         _display.graphics.drawRect(0,0,param1,param2);
         _display.graphics.endFill();
         if(_loc3_ && (_loc4_))
         {
            _display.width = _loc3_;
            _display.height = _loc4_;
         }
         dispatchEvent(new Event("size"));
         _display.dispatchEvent(new DynamicEvent("setSize",true,false,
            {
               "width":param1,
               "height":param2
            }));
      }
      
      private function onChannelAudio(param1:Event) : void {
         if(param1.target.audio && (_muted || !(param1.target.layer && param1.target.layer.audioEnabled)))
         {
            param1.target.audio = false;
         }
      }
      
      protected function onChannelOffline(param1:Event) : void {
         removeItem("channel_" + param1.target.layer.logic.id);
      }
      
      override public function destroy(... rest) : * {
         _destroying = true;
         var _loc4_:* = 0;
         var _loc3_:* = items;
         for(_loc2_ in items)
         {
            removeItem(_loc2_);
         }
         if(_display.parent && _display.parent.contains(_display))
         {
            _display.parent.removeChild(_display);
         }
         return super.destroy();
      }
      
      public final function get connected() : Boolean {
         return _connection && _connection.uri?_connection.connected:false;
      }
      
      public function get display() : Sprite {
         return _display;
      }
      
      public function get volume() : Number {
         return _volume;
      }
      
      public function set volume(param1:Number) : void {
         _volume = param1;
         var _loc6_:* = 0;
         var _loc5_:* = items;
         for(_loc2_ in items)
         {
            try
            {
               items[_loc2_].update({"volume":items[_loc2_].originalVolume * _volume});
            }
            catch(e:Error)
            {
               continue;
            }
         }
      }
      
      public function get playing() : Boolean {
         return _playing;
      }
      
      public function set playing(param1:Boolean) : void {
         _playing = param1;
         var _loc6_:* = 0;
         var _loc5_:* = items;
         for(_loc2_ in items)
         {
            try
            {
               items[_loc2_].update({"playing":items[_loc2_].originalPlaying && _playing});
            }
            catch(e:Error)
            {
               continue;
            }
         }
      }
      
      public function get muted() : Boolean {
         return _muted;
      }
      
      public function set muted(param1:Boolean) : void {
         _muted = param1;
         try
         {
            _loc5_ = 0;
            _loc4_ = items;
            for(_loc2_ in items)
            {
               if(items[_loc2_].hasOwnProperty("audio"))
               {
                  items[_loc2_].update({"audio":!_muted && items[_loc2_].audioEnabled && items[_loc2_].logic.hasAudio});
               }
            }
         }
         catch(e:*)
         {
         }
      }
      
      public function get destroying() : Boolean {
         return _destroying;
      }
   }
}
