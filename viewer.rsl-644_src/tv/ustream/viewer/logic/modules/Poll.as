package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Poll extends Module
   {
      
      public function Poll(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         _answers = [];
         _votes = [];
         super(param1,param2,param3);
         _needUms = true;
      }
      
      protected var _question:String;
      
      protected var _answers:Array;
      
      protected var _votes:Array;
      
      protected var _canVote:Boolean = true;
      
      protected var _pollId:String = "";
      
      public function vote(param1:uint) : void {
         echo("vote : " + _canVote);
         if(_canVote && _connection && _connection.connected)
         {
            echo("calling \'poll.vote\'");
            echo("index : " + param1);
            echo("_pollId : " + _pollId);
            _connection.call("poll.vote",null,param1,_pollId);
            _canVote = false;
         }
      }
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1.alreadyvoted)
         {
            _canVote = false;
         }
         if(param1.data)
         {
            _question = param1.data.question;
            _answers = param1.data.answers;
         }
         if(param1.votes)
         {
            _votes = param1.votes;
         }
         if(param1.event == "stop" || param1.event == "remove")
         {
            _canVote = false;
         }
         if(param1.id != undefined)
         {
            _pollId = param1.id;
         }
         dispatch(param1.event);
         if(param1.event == "remove")
         {
            destroy();
         }
      }
      
      public function get question() : String {
         return _question;
      }
      
      public function get answers() : Array {
         return _answers;
      }
      
      public function get votes() : Array {
         return _votes;
      }
      
      public function get canVote() : Boolean {
         return _canVote;
      }
   }
}
