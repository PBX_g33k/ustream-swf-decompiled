package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.utils.Timer;
   import tv.ustream.viewer.logic.media.Channel;
   import flash.events.TimerEvent;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.Event;
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.tools.This;
   import tv.ustream.modules.ModuleManager;
   
   public class Dvr extends Module
   {
      
      public function Dvr(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         if(param2)
         {
            info(param2);
         }
         if(param1 is Channel)
         {
            param1.addEventListener("play",onStreamTogglePlaying);
            param1.addEventListener("pause",onStreamTogglePlaying);
         }
         addEventListener("offair",onStreamOffline);
         addEventListener("live",onStreamOnline);
         updateTimer = new Timer(1000);
         updateTimer.addEventListener("timer",onUpdate);
         updateTimer.start();
      }
      
      public static const DVR_UPDATE:String = "dvr.update";
      
      public static const DVR_OUT:String = "dvr.out";
      
      private var _contentLength:Number = 0;
      
      private var _maximumContentLength:Number = 0;
      
      private var _timeShift:Number = 0;
      
      private var offlineTime:Number = 0;
      
      private var contentLengthWhenGoesOffline:Number = 0;
      
      private var _finished:Boolean = false;
      
      private var _pauseTime:Number = 0;
      
      public var lastChunk:int = 2147483647;
      
      private var updateTimer:Timer;
      
      override public function destroy(... rest) : * {
         if(logic is Channel)
         {
            logic.removeEventListener("play",onStreamTogglePlaying);
            logic.removeEventListener("pause",onStreamTogglePlaying);
         }
         removeEventListener("offair",onStreamOffline);
         removeEventListener("live",onStreamOnline);
         updateTimer.stop();
         updateTimer.removeEventListener("timer",onUpdate);
         updateTimer = null;
         return super.destroy();
      }
      
      private function onUpdate(param1:TimerEvent) : void {
         var _loc2_:Object = 
            {
               "timeShift":timeShift,
               "streamOnline":streamOnline,
               "playing":playing,
               "contentLength":contentLength,
               "maximumContentLength":maximumContentLength,
               "dvr":this
            };
         trace("4:contentLengthWhenGoesOffline = " + contentLengthWhenGoesOffline);
         dispatchEvent(new DynamicEvent("dvr.update",false,false,_loc2_));
         if(timeShift <= 0 && !streamOnline)
         {
            _finished = true;
            dispatchEvent(new DynamicEvent("dvr.out",false,false,_loc2_));
         }
      }
      
      private function onStreamTogglePlaying(param1:Event) : void {
         if(param1.type == "play" && _pauseTime > 0)
         {
            _pauseTime = 0;
         }
         else if(param1.type == "pause")
         {
            _pauseTime = new Date().getTime();
         }
         
      }
      
      private function onStreamOnline(param1:Event) : void {
         offlineTime = 0;
         contentLengthWhenGoesOffline = 0;
         _finished = false;
      }
      
      private function onStreamOffline(param1:Event) : void {
         if(timeShift == 0)
         {
            _finished = true;
         }
         contentLengthWhenGoesOffline = contentLength;
         offlineTime = new Date().getTime();
      }
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(typeof param1 == "object")
         {
            _contentLength = param1.hasOwnProperty("contentLength")?param1.contentLength:0;
            _maximumContentLength = param1.hasOwnProperty("maximumContentLength")?param1.maximumContentLength:0;
         }
      }
      
      public function get playing() : Boolean {
         return _pauseTime == 0;
      }
      
      public function seek(param1:Number) : void {
         var _loc2_:* = NaN;
         echo("seek");
         if(logic is Recorded)
         {
            logic.ums.dvrSeek(param1);
            _loc2_ = Math.min(maximumContentLength,contentLength);
            _timeShift = Math.min(_loc2_,Math.max(0,_loc2_ * (1 - param1)));
            if(!streamOnline)
            {
               contentLengthWhenGoesOffline = contentLength;
            }
            else
            {
               contentLengthWhenGoesOffline = 0;
            }
         }
      }
      
      public function get contentLength() : Number {
         return _contentLength;
      }
      
      public function set contentLength(param1:Number) : void {
         if(_contentLength != param1)
         {
            _contentLength = param1;
            dispatch("update");
         }
      }
      
      public function get maximumContentLength() : Number {
         return _maximumContentLength;
      }
      
      public function set maximumContentLength(param1:Number) : void {
         if(_maximumContentLength != param1)
         {
            _maximumContentLength = param1;
            dispatch("update");
         }
      }
      
      public function get timeShift() : Number {
         var _loc2_:Number = _timeShift + (_pauseTime > 0?(new Date().getTime() - _pauseTime) / 1000:0.0);
         var _loc1_:Number = contentLengthWhenGoesOffline - _contentLength;
         trace("4: timeshift diff = " + _loc1_,"ts = " + _loc2_,"clwgo = " + contentLengthWhenGoesOffline,"cl = " + _contentLength);
         if(_loc1_ > 0)
         {
            return Math.max(0,_loc2_ - _loc1_);
         }
         return Math.max(0,Math.min(_contentLength,_loc2_));
      }
      
      public function get seekPercent() : Number {
         return Math.min(1,Math.max(0,1 - timeShift / contentLength));
      }
      
      public function get streamOnline() : Boolean {
         if(This.getReference(logic,"modules.stream"))
         {
            return logic.modules.stream.hasLiveContent;
         }
         return false;
      }
      
      public function get finished() : Boolean {
         return !streamOnline && (_finished) && timeShift <= 0;
      }
   }
}
