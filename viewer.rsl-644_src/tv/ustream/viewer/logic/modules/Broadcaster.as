package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Broadcaster extends Module
   {
      
      public function Broadcaster(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
      }
      
      override protected function info(param1:*) : void {
         if(param1)
         {
            if(!_data)
            {
               _data = param1;
            }
            else
            {
               _loc4_ = 0;
               _loc3_ = param1;
               for(_loc2_ in param1)
               {
                  _data[_loc2_] = param1[_loc2_];
               }
            }
         }
         dispatch("update");
      }
   }
}
