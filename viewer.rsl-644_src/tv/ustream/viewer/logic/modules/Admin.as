package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Admin extends Module
   {
      
      public function Admin(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      override protected function info(param1:*) : void {
         if(typeof param1 == "object")
         {
            super.info(param1);
            dispatch("update");
         }
      }
      
      public function callAdapter(param1:String, ... rest) : void {
         call.apply(this,["run",param1].concat(rest));
      }
   }
}
