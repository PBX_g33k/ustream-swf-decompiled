package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.debug.Debug;
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.tools.UncaughtErrorHandler;
   import tv.ustream.modules.ModuleManager;
   
   public class LogCollector extends Module
   {
      
      public function LogCollector(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         UncaughtErrorHandler.instance.addEventListener("UncaughtError",onUncaughtError);
         UncaughtErrorHandler.instance.addEventListener("UncaughtErrorEvent",onUncaughtError);
         UncaughtErrorHandler.instance.addEventListener("UncaughtThrownObject",onUncaughtError);
         if(param1 is Recorded && (param1 as Recorded).statistics)
         {
            (param1 as Recorded).statistics.failure.addEventListener("change",onStatsFailure);
         }
      }
      
      private var autoSendFlashErrors:Array;
      
      private var autoSendStatsErrors:Array;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1)
         {
            if(param1.autoSendFlashErrors is Array)
            {
               autoSendFlashErrors = param1.autoSendFlashErrors;
            }
            if(param1.autoSendStatsErrors is Array)
            {
               autoSendStatsErrors = param1.autoSendStatsErrors;
            }
         }
      }
      
      private function onUncaughtError(param1:DynamicEvent) : void {
         Debug.debug("LogCollector","onUncaughtError",param1.data);
         if(param1.data && param1.data.code && !(autoSendFlashErrors.indexOf(param1.data.code) == -1) && logic is Recorded)
         {
            Debug.sendLogToUms(logic.ums,param1.data.message);
            destroy();
         }
      }
      
      private function onStatsFailure(param1:DynamicEvent) : void {
         if(param1.data && param1.data.error is Array)
         {
            if(autoSendStatsErrors.indexOf(param1.data.error[0].type) != -1)
            {
               Debug.sendLogToUms(logic.ums,"StreamStats Error : " + param1.data.error[0].type);
               destroy();
            }
         }
      }
      
      override public function destroy(... rest) : * {
         UncaughtErrorHandler.instance.removeEventListener("UncaughtError",onUncaughtError);
         UncaughtErrorHandler.instance.removeEventListener("UncaughtErrorEvent",onUncaughtError);
         UncaughtErrorHandler.instance.removeEventListener("UncaughtThrownObject",onUncaughtError);
         if(logic is Recorded && (logic as Recorded).statistics)
         {
            (logic as Recorded).statistics.failure.removeEventListener("change",onStatsFailure);
         }
         return super.destroy();
      }
   }
}
