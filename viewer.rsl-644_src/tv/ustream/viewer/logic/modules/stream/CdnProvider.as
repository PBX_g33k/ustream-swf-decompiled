package tv.ustream.viewer.logic.modules.stream
{
   public class CdnProvider extends Provider
   {
      
      public function CdnProvider() {
         super();
      }
      
      protected var _needsSubscription:Boolean = true;
      
      public function get needsSubscription() : Boolean {
         return _needsSubscription;
      }
      
      public function set needsSubscription(param1:Boolean) : void {
         _needsSubscription = param1;
      }
   }
}
