package tv.ustream.viewer.logic.modules.stream
{
   public class UhsProvider extends Provider
   {
      
      public function UhsProvider() {
         super();
      }
      
      public static const CHUNK_ID_CHANGE:String = "chunkIdChange";
      
      public static const CHUNK_RANGE_CHANGE:String = "chunkRangeChange";
      
      protected var _varnishUrl:String;
      
      protected var _chunkId:uint;
      
      protected var _chunkRange:Object;
      
      protected var _offset:uint;
      
      protected var _offsetInMs:uint;
      
      protected var _chunkTime:uint;
      
      public function get chunkId() : uint {
         return _chunkId;
      }
      
      public function set chunkId(param1:uint) : void {
         if(_chunkId != param1)
         {
            _chunkId = param1;
            dispatch("chunkIdChange");
         }
      }
      
      public function get chunkRange() : Object {
         return _chunkRange;
      }
      
      public function set chunkRange(param1:Object) : void {
         _chunkRange = param1;
         dispatch("chunkRangeChange");
      }
      
      public function get varnishUrl() : String {
         return _varnishUrl;
      }
      
      public function set varnishUrl(param1:String) : void {
         _varnishUrl = param1;
      }
      
      public function get offset() : uint {
         return _offset;
      }
      
      public function set offset(param1:uint) : void {
         _offset = param1;
      }
      
      public function get chunkTime() : uint {
         return _chunkTime;
      }
      
      public function set chunkTime(param1:uint) : void {
         _chunkTime = param1;
      }
      
      public function get offsetInMs() : uint {
         return _offsetInMs;
      }
      
      public function set offsetInMs(param1:uint) : void {
         _offsetInMs = param1;
      }
   }
}
