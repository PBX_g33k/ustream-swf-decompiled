package tv.ustream.viewer.logic.modules.stream
{
   public class ProgressiveProvider extends Provider
   {
      
      public function ProgressiveProvider() {
         super();
         _seek = {};
      }
      
      protected var _streamNameService:String;
      
      protected var _seek:Object;
      
      public function get streamNameService() : String {
         return _streamNameService;
      }
      
      public function set streamNameService(param1:String) : void {
         _streamNameService = param1;
      }
      
      public function get seek() : Object {
         return _seek;
      }
      
      public function set seek(param1:Object) : void {
         _seek = param1;
      }
   }
}
