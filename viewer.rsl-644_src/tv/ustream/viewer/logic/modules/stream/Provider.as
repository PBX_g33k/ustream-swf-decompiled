package tv.ustream.viewer.logic.modules.stream
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   
   public class Provider extends Dispatcher
   {
      
      public function Provider() {
         super();
         _dispatchList = [];
         _dispatchTimer = new Timer(20,1);
         _dispatchTimer.addEventListener("timerComplete",onDispatchTimerComplete);
      }
      
      public static const URL_CHANGE:String = "urlChange";
      
      public static const STREAM_CHANGE:String = "streamChange";
      
      protected var _url:String;
      
      protected var _streamName:String;
      
      protected var _streamId:Number;
      
      protected var _name:String;
      
      protected var _isTranscoded:Boolean = false;
      
      protected var _streams:Array;
      
      protected var _dispatchTimer:Timer;
      
      protected var _dispatchList:Array;
      
      private function onDispatchTimerComplete(param1:TimerEvent) : void {
         var _loc2_:* = null;
         echo("onDispatchTimerComplete");
         while(_dispatchList && _dispatchList.length)
         {
            _loc2_ = _dispatchList.shift();
            super.dispatch(_loc2_.eventName,_loc2_.bubbles,_loc2_.data,_loc2_.skipEcho);
         }
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function set url(param1:String) : void {
         if(_url != param1)
         {
            echo("set url");
            echo("_url : " + _url);
            echo("value : " + param1);
            _url = param1;
            dispatch("urlChange");
         }
      }
      
      public function get streamName() : String {
         return _streamName;
      }
      
      public function set streamName(param1:String) : void {
         if(_streamName != param1)
         {
            echo("set streamName");
            echo("_streamName : " + _streamName);
            echo("value : " + param1);
            _streamName = param1;
            dispatch("streamChange");
         }
      }
      
      public function get name() : String {
         return _name;
      }
      
      public function set name(param1:String) : void {
         echo("set name");
         echo("_name : " + _name);
         echo("value : " + param1);
         _name = param1;
      }
      
      public function get isTranscoded() : Boolean {
         return _isTranscoded;
      }
      
      public function set isTranscoded(param1:Boolean) : void {
         _isTranscoded = param1;
      }
      
      public function get streams() : Array {
         return _streams;
      }
      
      public function set streams(param1:Array) : void {
         _streams = param1;
      }
      
      public function get streamId() : Number {
         return _streamId;
      }
      
      public function set streamId(param1:Number) : void {
         _streamId = param1;
      }
      
      override protected function dispatch(param1:String, param2:Boolean = false, param3:* = null, param4:Boolean = false) : void {
         if(hasEventListener(param1))
         {
            if(_dispatchList)
            {
               _dispatchList.push(
                  {
                     "eventName":param1,
                     "bubbles":param2,
                     "data":param3,
                     "skipEcho":param4
                  });
               _dispatchTimer.start();
            }
            else
            {
               super.dispatch(param1,param2,param3,param4);
            }
         }
      }
      
      override public function destroy(... rest) : * {
         _dispatchList = null;
         if(_dispatchTimer)
         {
            _dispatchTimer.reset();
            _dispatchTimer.removeEventListener("timerComplete",onDispatchTimerComplete);
            _dispatchTimer = null;
         }
         return super.destroy();
      }
   }
}
