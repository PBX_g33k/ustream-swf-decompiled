package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.net.Ums;
   import flash.events.Event;
   import tv.ustream.tools.This;
   import tv.ustream.tools.Shell;
   import tv.ustream.viewer.logic.Logic;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.modules.ModuleManager;
   
   public class Ppv extends Module
   {
      
      public function Ppv(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         _auth = param1.sessionId?"wait":"failed";
         updateLock();
         if(param1["ums"])
         {
            ums = param1["ums"];
            ums.addEventListener("connected",onUmsConnected);
         }
      }
      
      public static const SUBSCRIBE:String = "subscribe";
      
      public static const BUY_TICKET:String = "buyTicket";
      
      public static const ENTER_TICKET:String = "enterTicket";
      
      public static const INITIATE_LOGON:String = "initiateLogon";
      
      private var _title:String;
      
      private var _description:String;
      
      private var _thumbUrl:String;
      
      private var _url:String;
      
      private var _eventId:String;
      
      private var _bgUrl:String;
      
      private var _faqUrl:String;
      
      private var _enterTicket:String;
      
      private var _buyTicket:String;
      
      private var _subscribe:String;
      
      private var _guest:Boolean;
      
      private var _price:String;
      
      private var _currency:String;
      
      private var ums:Ums;
      
      private var _status:String;
      
      private var _auth:String;
      
      private var _user:String = "wait";
      
      private var dpFreeReject:Boolean = false;
      
      private var _hasTicket:Boolean = false;
      
      private var _alreadySubscribed:Boolean = false;
      
      private var _hasVideo:Boolean = false;
      
      private function onUmsConnected(param1:Event) : void {
         dispatch("lock");
      }
      
      override public function get lock() : Boolean {
         return !_dying && (_lock);
      }
      
      override protected function info(param1:*) : void {
         if(lastInfoSource == "ums" && infoSource == "gw")
         {
            echo("skip gw ppv data");
            return;
         }
         lastInfoSource = infoSource;
         echo("infoSource: " + infoSource);
         if(param1.meta)
         {
            if(param1.meta.title)
            {
               _title = param1.meta.title;
            }
            if(param1.meta.description)
            {
               _description = param1.meta.description;
            }
            if(param1.meta.thumbUrl)
            {
               _thumbUrl = param1.meta.thumbUrl;
            }
            if(param1.meta.url)
            {
               _url = param1.meta.url;
            }
            if(param1.meta.hasVideo)
            {
               _hasVideo = param1.meta.hasVideo;
            }
         }
         _buyTicket = param1.buyTicket;
         _subscribe = param1.subscribe;
         if(param1.enterTicket)
         {
            _enterTicket = param1.enterTicket;
         }
         if(param1.eventId)
         {
            _eventId = param1.eventId;
         }
         if(param1.faqUrl)
         {
            _faqUrl = param1.faqUrl;
         }
         if(param1.status)
         {
            _status = param1.status.toLowerCase();
         }
         if(param1.auth)
         {
            _auth = param1.auth;
         }
         if(param1.cost)
         {
            _price = param1.cost;
         }
         if(param1.costCurrency)
         {
            _currency = param1.costCurrency;
         }
         var _loc2_:Meta = manager is ViewerModuleManager?(manager as ViewerModuleManager).meta:null;
         var _loc3_:Object = param1.multiCurrencyInfo;
         if(_loc3_ && _loc2_ && _loc3_[_loc2_.countryCode])
         {
            _price = _loc3_[_loc2_.countryCode].amount;
            _currency = _loc3_[_loc2_.countryCode].currency;
         }
         if(param1.bgUrl)
         {
            _bgUrl = param1.bgUrl;
         }
         if(_status == "paid")
         {
            if(_auth == "success" && !(_user == "success"))
            {
               _user = "success";
               dispatch("goPaid",false,{"user":_user});
            }
            else if(_auth == "failed" && !(_user == "failed"))
            {
               _user = "failed";
               dispatch("goPaid",false,{"user":_user});
            }
            else if(_auth == "wait" && !(user == "wait"))
            {
               _user = "wait";
               dispatch("goPaid",false,{"user":_user});
            }
            else if(_auth == "reject" && !(user == "reject"))
            {
               _user = "reject";
               dispatch("paidReject");
               logic.destroy();
            }
            
            
            
         }
         else
         {
            if(_auth == "reject")
            {
               if(!dpFreeReject)
               {
                  dpFreeReject = true;
                  dispatch("freeReject");
               }
               _auth = "failed";
            }
            if(user != _status)
            {
               _user = _status;
               dispatch("goFree");
            }
         }
         dispatch("update");
         updateLock();
      }
      
      private function updateLock() : void {
         var _loc1_:Boolean = _lock;
         _lock = !(ums && ums.connected && (!(_status == "paid") || (status == "paid" && _user == "success")));
         if(_lock != _loc1_)
         {
            dispatch("lock");
         }
      }
      
      public function get title() : String {
         return _title;
      }
      
      public function get description() : String {
         return _description;
      }
      
      public function get thumbUrl() : String {
         return _thumbUrl;
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function get status() : String {
         return _status;
      }
      
      public function get buyTicket() : String {
         return _buyTicket;
      }
      
      public function get enterTicket() : String {
         return _enterTicket;
      }
      
      public function get guest() : Boolean {
         var _loc1_:* = true;
         if(This.pageUrl && (This.onSite(This.pageUrl) & 2 || This.onSite(This.pageUrl) & 1) && logic.sessionId && !(logic.sessionId == "adamCarollaDummySessionId") && !(logic.sessionId == "UstreamGuestSession"))
         {
            _loc1_ = false;
         }
         return _loc1_;
      }
      
      public function get auth() : String {
         return _auth;
      }
      
      public function get user() : String {
         var _loc1_:String = _user;
         if(_user == "successW84Data")
         {
            _loc1_ = "success";
         }
         return _user;
      }
      
      public function get bgUrl() : String {
         return _bgUrl;
      }
      
      public function get price() : String {
         return _price;
      }
      
      public function get currency() : String {
         return _currency;
      }
      
      public function get subscribe() : String {
         return _subscribe;
      }
      
      public function get faqUrl() : String {
         return _faqUrl;
      }
      
      public function get hasVideo() : Boolean {
         return _hasVideo;
      }
      
      public function get hasTicket() : Boolean {
         return _hasTicket;
      }
      
      public function set hasTicket(param1:Boolean) : void {
         if(_hasTicket != param1)
         {
            _hasTicket = param1;
            dispatch("update");
         }
      }
      
      public function get alreadySubscribed() : Boolean {
         return _alreadySubscribed;
      }
      
      public function set alreadySubscribed(param1:Boolean) : void {
         if(_alreadySubscribed != param1)
         {
            _alreadySubscribed = param1;
            dispatch("update");
         }
      }
      
      public function get eventId() : String {
         return _eventId;
      }
      
      public function ticketAction(param1:Object) : void {
         var _loc2_:* = param1.type;
         if("subscribe" !== _loc2_)
         {
            if("buyTicket" !== _loc2_)
            {
               if("enterTicket" !== _loc2_)
               {
                  if("initiateLogon" === _loc2_)
                  {
                     echo("initiateLogon");
                     if(Shell.hasInstance && (Shell.instance.hasEventListener("ppv","initiateLogon")))
                     {
                        dispatch("initiateLogon");
                     }
                     else
                     {
                        navigateToURL(new URLRequest(Logic.instance.media.url));
                     }
                  }
               }
               else
               {
                  echo("enterTicket");
                  if(Shell.hasInstance && (Shell.instance.hasEventListener("ppv","enterTicket")))
                  {
                     dispatch("enterTicket",false,
                        {
                           "eventId":_eventId,
                           "mediaId":Logic.instance.media.id
                        });
                  }
                  else if(_enterTicket)
                  {
                     navigateToURL(new URLRequest(_enterTicket));
                  }
                  
               }
            }
            else
            {
               echo("buyTicket");
               if(Shell.hasInstance && (Shell.instance.hasEventListener("ppv","buyTicket")))
               {
                  dispatch("buyTicket",false,
                     {
                        "eventId":_eventId,
                        "mediaId":Logic.instance.media.id
                     });
               }
               else if(_buyTicket)
               {
                  navigateToURL(new URLRequest(_buyTicket));
               }
               
            }
         }
         else
         {
            echo("subscribe");
            if(Shell.hasInstance && (Shell.instance.hasEventListener("ppv","subscribe")))
            {
               dispatch("subscribe",false,
                  {
                     "eventId":_eventId,
                     "mediaId":Logic.instance.media.id
                  });
            }
            else if(_subscribe)
            {
               navigateToURL(new URLRequest(_subscribe));
            }
            
         }
      }
      
      override public function destroy(... rest) : * {
         if(ums)
         {
            ums.removeEventListener("connected",onUmsConnected);
            ums = null;
         }
         return super.destroy();
      }
   }
}
