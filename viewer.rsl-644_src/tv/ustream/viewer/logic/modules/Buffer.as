package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Buffer extends Module
   {
      
      public function Buffer(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         info(true);
      }
      
      override protected function info(param1:*) : void {
         if(param1 != this.data)
         {
            if(param1 is Boolean)
            {
               param1 = param1?5:0.0;
            }
            else if(param1.hasOwnProperty("buffer"))
            {
               if(!isNaN(param1.buffer))
               {
                  param1 = param1.buffer;
               }
               else
               {
                  return;
               }
            }
            
            super.info(param1);
            dispatch("update");
         }
      }
   }
}
