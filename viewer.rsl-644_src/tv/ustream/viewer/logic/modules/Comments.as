package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.Debug;
   import tv.ustream.modules.ModuleManager;
   
   public class Comments extends Module
   {
      
      public function Comments(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param2)
         {
            info(param2);
         }
      }
      
      private var _processedData:Array;
      
      override protected function info(param1:*) : void {
         var _loc2_:* = null;
         super.info(param1);
         if(param1 is Array)
         {
            _loc2_ = [];
            _loc5_ = 0;
            _loc4_ = param1;
            for each(_loc3_ in param1)
            {
               if(_loc3_ && _loc3_.comment && _loc3_.moment && _loc3_.author && _loc3_.id && _loc3_.start)
               {
                  _loc2_.push(
                     {
                        "text":_loc3_.comment,
                        "to":_loc3_.moment,
                        "from":_loc3_.author,
                        "id":_loc3_.id,
                        "start":_loc3_.start,
                        "length":_loc3_.length
                     });
               }
            }
            _data = _loc2_;
            processCut();
         }
      }
      
      override public function get data() : * {
         return _processedData;
      }
      
      public function processCut(param1:Number = 0, param2:Number = 1) : void {
         var _loc3_:* = 0;
         Debug.echo("[ COMMENTS ] processCut");
         _processedData = [];
         _loc3_ = 0;
         while(_loc3_ < _data.length)
         {
            if(_data[_loc3_].start >= param1 && _data[_loc3_].start <= param2)
            {
               _data[_loc3_].start = (_data[_loc3_].start - param1) / (param2 - param1);
               _data[_loc3_].to = (_data[_loc3_].to - param1) / (param2 - param1);
               _processedData.push(_data[_loc3_]);
            }
            _loc3_++;
         }
         Debug.explore(_processedData);
         dispatch("update");
      }
   }
}
