package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Started extends Module
   {
      
      public function Started(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         _data = {};
      }
      
      override protected function info(param1:*) : void {
         var _loc2_:* = null;
         if(typeof param1 != "object")
         {
            _loc2_ = 
               {
                  "timestamp":param1,
                  "recordId":"-1"
               };
            param1 = _loc2_;
         }
         super.info(param1);
      }
      
      override public function toString() : String {
         return _data.timestamp;
      }
      
      public function get at() : Number {
         return _data.timestamp;
      }
      
      public function get timestamp() : String {
         return _data.timestamp;
      }
      
      public function get recordId() : String {
         return _data.recordId;
      }
   }
}
