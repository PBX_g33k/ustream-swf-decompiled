package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Meta extends Module
   {
      
      public function Meta(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
      }
      
      protected var _username:String;
      
      protected var _title:String;
      
      protected var _description:String;
      
      protected var _url:String;
      
      protected var _tags:Array;
      
      protected var _embeds:Object;
      
      protected var _alternative:Object;
      
      protected var _ppvEvents:Object;
      
      protected var _pageUrl:String;
      
      protected var _countryCode:String;
      
      override protected function info(param1:*) : void {
         if(param1)
         {
            if(!_data)
            {
               _data = param1;
            }
            else
            {
               _loc4_ = 0;
               _loc3_ = param1;
               for(_loc2_ in param1)
               {
                  _data[_loc2_] = param1[_loc2_];
               }
            }
         }
         if(param1.userName)
         {
            _username = param1.userName;
         }
         if(param1.title)
         {
            _title = param1.title;
         }
         if(param1.url)
         {
            _url = param1.url;
         }
         if(param1.tags)
         {
            _tags = param1.tags;
         }
         if(param1.description)
         {
            _description = param1.description;
         }
         if(param1.embeds)
         {
            _embeds = param1.embeds;
         }
         if(param1.alternative)
         {
            _alternative = param1.alternative;
         }
         if(param1.ppvEvents)
         {
            _ppvEvents = param1.ppvEvents;
         }
         if(param1.pageUrl)
         {
            _pageUrl = param1.pageUrl;
         }
         if(param1.countryCode)
         {
            _countryCode = param1.countryCode;
         }
         dispatch("update");
      }
      
      public function get username() : String {
         return _username;
      }
      
      public function get title() : String {
         return _title;
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function get tags() : Array {
         return _tags;
      }
      
      public function get description() : String {
         return _description;
      }
      
      public function get embeds() : Object {
         return _embeds;
      }
      
      public function get alternative() : Object {
         return _alternative;
      }
      
      public function get ppvEvents() : Object {
         return _ppvEvents;
      }
      
      public function get countryCode() : String {
         return _countryCode;
      }
   }
}
