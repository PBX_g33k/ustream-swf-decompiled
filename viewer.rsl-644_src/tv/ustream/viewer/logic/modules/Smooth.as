package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Smooth extends Module
   {
      
      public function Smooth(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      private var _value:Boolean;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         _value = param1;
         dispatch("update");
      }
      
      public function get value() : Boolean {
         return _value;
      }
   }
}
