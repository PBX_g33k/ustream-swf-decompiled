package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.This;
   import tv.ustream.modules.ModuleManager;
   
   public class Viewers extends Module
   {
      
      public function Viewers(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         _silent = true;
      }
      
      protected var count:String;
      
      override protected function info(param1:*) : void {
         var _loc2_:* = null;
         if(infoSource == "stream")
         {
            _loc2_ = This.getReference(manager,"stream") as Stream;
            if(_loc2_ && _loc2_.logicUrl)
            {
               return;
            }
         }
         echo("lastInfoSource : " + lastInfoSource + ", infoSource : " + infoSource);
         if(lastInfoSource == "ums" && !(infoSource == "ums"))
         {
            return;
         }
         if(!param1)
         {
            destroy();
         }
         else
         {
            super.info(param1);
            count = param1;
            dispatch("update");
            lastInfoSource = infoSource;
         }
      }
      
      override public function toString() : String {
         return count;
      }
      
      public function get viewerCount() : Number {
         return count;
      }
   }
}
