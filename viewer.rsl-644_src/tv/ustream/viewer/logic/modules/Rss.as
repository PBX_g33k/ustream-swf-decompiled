package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.net.URLLoader;
   import tv.ustream.tools.DynamicEvent;
   import flash.net.URLRequest;
   import flash.events.Event;
   import tv.ustream.modules.ModuleManager;
   
   public class Rss extends Module
   {
      
      public function Rss(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         rssLoader = new URLLoader();
         rssLoader.addEventListener("complete",onRssLoaded);
         viewerModuleManager = param3 as ViewerModuleManager;
      }
      
      private var url:String;
      
      private var rssLoader:URLLoader;
      
      private var entries:XMLList;
      
      private var viewerModuleManager:ViewerModuleManager;
      
      private function configLayers(... rest) : void {
         logic.dispatchEvent(new DynamicEvent("moduleInfo",true,true,{"info":{"layers":{"add":{"rss":
            {
               "type":"text",
               "htmlText":" ",
               "x":0,
               "y":322,
               "background":true,
               "backgroundColor":0,
               "alpha":0.8,
               "width":610,
               "height":20,
               "font":"Verdana",
               "shadowColor":16776960,
               "size":12,
               "scroll":1,
               "color":16777215
            }}}}}));
      }
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1.url && !url)
         {
            url = param1.url;
            if(viewerModuleManager.layers)
            {
               configLayers();
            }
            else
            {
               viewerModuleManager.addEventListener("createLayers",configLayers);
               dispatchEvent(new DynamicEvent("request",false,false,{"requestModule":"layers"}));
            }
            updateRss();
         }
      }
      
      private function updateRss(... rest) : void {
         if(url)
         {
            rssLoader.load(new URLRequest(url));
         }
      }
      
      private function onRssLoaded(param1:Event) : void {
         var _loc5_:* = 0;
         var _loc3_:XML = new XML(rssLoader.data as String);
         entries = _loc3_.channel.item;
         var _loc4_:Namespace = entries[0].namespace();
         var _loc2_:String = "";
         _loc5_ = 0;
         while(_loc5_ < entries.length())
         {
            _loc2_ = _loc2_ + ("<a href=\"" + entries[_loc5_]._loc4_::link + "\" target=\"_blank\">" + entries[_loc5_]._loc4_::title + "</a> <b>|</b> ");
            _loc5_++;
         }
         logic.dispatchEvent(new DynamicEvent("moduleInfo",true,true,{"info":{"layers":{"update":{"rss":{"htmlText":_loc2_}}}}}));
      }
   }
}
