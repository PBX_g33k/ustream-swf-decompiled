package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.utils.Timer;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import tv.ustream.viewer.logic.Logic;
   import flash.net.URLLoader;
   import flash.net.URLRequest;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.Json;
   import flash.events.HTTPStatusEvent;
   import tv.ustream.modules.ModuleManager;
   
   public class RpinLock extends Module
   {
      
      public function RpinLock(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param2)
         {
            info(param2);
         }
         reportTimer = new Timer(10000,1);
         reportTimer.addEventListener("timerComplete",onReportTimer);
      }
      
      public static const PUSH_REQUESTED:String = "pushRequested";
      
      public static const PUSH_SUCCESSFUL:String = "pushSuccesful";
      
      public static const PUSH_FAILED:String = "pushFailed";
      
      public static const REMOVE_REQUESTED:String = "removeRequested";
      
      public static const REMOVE_SUCCESSFUL:String = "removeSuccesful";
      
      public static const REMOVE_FAILED:String = "removeFailed";
      
      private const REPORT_DELAY:uint = 10000;
      
      private var _userName:String;
      
      private var _facebookAuthToken:String;
      
      private var _success:int = -1;
      
      private var _url:String;
      
      private var reportTimer:Timer;
      
      private var fbActionId:String = "";
      
      private var offairContent:Recorded;
      
      override public function get data() : * {
         return null;
      }
      
      public function get userName() : String {
         return _userName;
      }
      
      public function get facebookAuthToken() : String {
         return _facebookAuthToken;
      }
      
      public function get success() : Boolean {
         return _success == 1;
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function get rpin() : String {
         return logic is Recorded?(logic as Recorded).rpin:"";
      }
      
      override protected function info(param1:*) : void {
         if(param1.userName != undefined)
         {
            _userName = param1.userName;
         }
         if(param1.facebookAuthToken != undefined)
         {
            _facebookAuthToken = param1.facebookAuthToken;
         }
         if(param1.url != undefined)
         {
            _url = param1.url;
         }
         dispatch("update");
         if(_lock != !_success)
         {
            _lock = !_success;
            dispatch("lock");
         }
      }
      
      private function onMediaPlay(param1:Event) : void {
         echo("onMediaPlay");
         reportTimer.reset();
         reportTimer.start();
      }
      
      private function onMediaPause(param1:Event) : void {
         echo("onMediaPlay");
         reportTimer.reset();
      }
      
      private function onReportTimer(param1:TimerEvent) : void {
         echo("onReportTimer");
         if(Logic.hasInstance)
         {
            Logic.instance.removeEventListener("play",onMediaPlay);
            Logic.instance.removeEventListener("pause",onMediaPause);
         }
         else if(logic is Recorded)
         {
            (logic as Recorded).removeEventListener("play",onMediaPlay);
            (logic as Recorded).removeEventListener("pause",onMediaPause);
         }
         
         pushToTimeline();
      }
      
      public function pushToTimeline() : void {
         var _loc2_:URLLoader = new URLLoader();
         _loc2_.dataFormat = "text";
         _loc2_.addEventListener("complete",onPushComplete);
         _loc2_.addEventListener("ioError",onPushFailed);
         _loc2_.addEventListener("httpStatus",onHttpStatus);
         _loc2_.addEventListener("securityError",onPushFailed);
         var _loc3_:Object = 
            {
               "video":"http://www.broadcastforfriends.com/video/" + (logic as Recorded).mediaId,
               "access_token":_facebookAuthToken,
               "method":"post"
            };
         var _loc1_:URLRequest = new URLRequest();
         _loc1_.method = "POST";
         _loc1_.url = "https://graph.facebook.com/me/video.watches";
         _loc1_.url = _loc1_.url + ("?video=" + _loc3_.video);
         _loc1_.url = _loc1_.url + ("&access_token=" + _loc3_.access_token);
         _loc1_.url = _loc1_.url + ("&method=" + _loc3_.method);
         echo("pushToTimeline request url : " + _loc1_.url);
         Debug.explore(_loc1_.data);
         _loc2_.load(_loc1_);
         dispatch("pushRequested");
      }
      
      private function onPushFailed(param1:Event) : void {
         echo("onPushFailed : " + param1.toString());
         dispatch("pushFailed");
      }
      
      private function onPushComplete(param1:Event) : void {
         var _loc2_:URLLoader = param1.target as URLLoader;
         echo("onPushComplete\n" + _loc2_.data);
         if(Json.getTokenValue(_loc2_.data,"id"))
         {
            fbActionId = Json.getTokenValue(_loc2_.data,"id");
            fbActionId = fbActionId.split("\"").join("");
            dispatch("pushSuccesful");
         }
         else
         {
            fbActionId = "";
            dispatch("pushFailed");
         }
      }
      
      public function removeFromTimeline() : void {
         var _loc2_:URLLoader = new URLLoader();
         _loc2_.dataFormat = "text";
         _loc2_.addEventListener("complete",onRemoveComplete);
         _loc2_.addEventListener("ioError",onRemoveFailed);
         _loc2_.addEventListener("httpStatus",onHttpStatus);
         _loc2_.addEventListener("securityError",onRemoveFailed);
         var _loc3_:Object = 
            {
               "method":"delete",
               "access_token":_facebookAuthToken
            };
         var _loc1_:URLRequest = new URLRequest();
         _loc1_.method = "POST";
         _loc1_.url = "https://graph.facebook.com/" + fbActionId;
         _loc1_.url = _loc1_.url + ("?method=" + _loc3_.method);
         _loc1_.url = _loc1_.url + ("&access_token=" + _loc3_.access_token);
         echo("removeFromTimeline request url : " + _loc1_.url);
         Debug.explore(_loc1_.data);
         _loc2_.load(_loc1_);
         dispatch("removeRequested");
      }
      
      private function onHttpStatus(param1:HTTPStatusEvent) : void {
         echo("onHttpStatus : " + param1.toString());
      }
      
      private function onRemoveFailed(param1:Event) : void {
         echo("onRemoveFailed : " + param1.toString());
         dispatch("removeFailed");
      }
      
      private function onRemoveComplete(param1:Event) : void {
         var _loc2_:URLLoader = param1.target as URLLoader;
         echo("onRemoveComplete\n" + _loc2_.data);
         if(!Json.getTokenValue(_loc2_.data,"error"))
         {
            dispatch("removeSuccesful");
         }
         else
         {
            echo(Json.getTokenValue(Json.getTokenValue(_loc2_.data,"error"),"message"));
            dispatch("removeFailed");
         }
      }
   }
}
