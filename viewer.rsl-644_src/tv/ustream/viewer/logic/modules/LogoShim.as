package tv.ustream.viewer.logic.modules
{
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.This;
   
   public class LogoShim extends Object
   {
      
      public function LogoShim() {
         super();
      }
      
      public static var skipDefault:Boolean = false;
      
      public static function decorateModuleConfig(param1:Object, param2:Recorded) : void {
         var _loc3_:* = null;
         var _loc4_:Array = [];
         if(param2)
         {
            Debug.echo("[ LOGOSHIM ] brandId : " + param2.brandId);
         }
         if(param1.moduleConfig && param2)
         {
            Debug.echo("[ LOGOSHIM ] parsing moduleConfig");
            if(param1.moduleConfig.logo is Array)
            {
               Debug.echo("[ LOGOSHIM ] logo moduleConfig is Array, skipping");
            }
            else
            {
               if(param1.moduleConfig.meta && param1.moduleConfig.meta.logo)
               {
                  _loc3_ = param1.moduleConfig.meta.logo;
                  if(_loc3_.align == undefined)
                  {
                     _loc3_.align = 0;
                  }
                  if(_loc3_.margin == undefined)
                  {
                     _loc3_.margin = 5;
                  }
                  _loc4_.push(_loc3_);
                  delete param1.moduleConfig.meta.logo;
                  Debug.echo("[ LOGOSHIM ] shifted logo from meta");
               }
               if(param1.moduleConfig.logo)
               {
                  if(param1.moduleConfig.logo.show != false)
                  {
                     _loc4_.push(param1.moduleConfig.logo);
                     Debug.echo("[ LOGOSHIM ] added logo from moduleConfig");
                  }
                  else if(param1.moduleConfig.logo.show === false)
                  {
                     delete param1.moduleConfig.logo;
                  }
                  
               }
               else if(!skipDefault && param2.brandId == "1" && !(param1.moduleConfig.logo == false) && !This.getReference(param2,"modules.logo"))
               {
                  _loc3_ = {};
                  _loc3_.align = 2;
                  _loc3_.margin = 5;
                  _loc4_.push(_loc3_);
                  Debug.echo("[ LOGOSHIM ] added default ustream logo");
               }
               
               if(_loc4_.length)
               {
                  param1.moduleConfig.logo = _loc4_;
               }
            }
         }
      }
      
      public static function logoFromObject(param1:Object) : Array {
         var _loc2_:Object = {};
         _loc2_.margin = param1 && !(param1.margin == undefined)?param1.margin:5;
         _loc2_.align = param1 && !(param1.align == undefined)?param1.align:2;
         if(param1 && param1.url)
         {
            _loc2_.url = param1.url;
         }
         if(param1 && param1.clickurl)
         {
            _loc2_.clickurl = param1.clickurl;
         }
         return [_loc2_];
      }
   }
}
