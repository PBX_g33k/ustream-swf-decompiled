package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.localization.Locale;
   import tv.ustream.modules.ModuleManager;
   
   public class Share extends Module
   {
      
      public function Share(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
      }
      
      override protected function info(param1:*) : void {
         var _loc3_:* = 0;
         var _loc2_:Array = ["facebook","twitter","mail","embed"];
         if(Locale.hasInstance && Locale.instance.language == "ja_JP")
         {
            _loc2_ = ["tyjapan","embed","mail","twitter","mixi","facebook"];
         }
         _loc3_ = 0;
         while(_loc3_ < param1.length)
         {
            if(_loc2_.indexOf(param1[_loc3_].name) != -1)
            {
               param1[_loc3_].cb = false;
               param1[_loc3_].priority = _loc2_.indexOf(param1[_loc3_].name);
            }
            else
            {
               param1.splice(_loc3_--,1);
            }
            _loc3_++;
         }
         (param1 as Array).sortOn("priority",16);
         super.info(param1);
         dispatch("update");
      }
      
      public function get items() : Array {
         return data as Array;
      }
   }
}
