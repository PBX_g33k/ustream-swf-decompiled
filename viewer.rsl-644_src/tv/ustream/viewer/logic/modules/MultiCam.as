package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.events.Event;
   import tv.ustream.modules.ModuleManager;
   
   public class MultiCam extends Module
   {
      
      public function MultiCam(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         param3.addEventListener("createMultiCam",onSelfCreate);
      }
      
      private var _channels;
      
      private function onSelfCreate(param1:Event) : void {
         manager.removeEventListener("createMultiCam",onSelfCreate);
         info(initValue);
      }
      
      override protected function info(param1:*) : void {
         _channels = param1;
         if(param1.hasOwnProperty("channels"))
         {
            _channels = param1.channels;
         }
         super.info(param1);
         dispatch("update");
      }
      
      public function get channels() : Array {
         return _channels as Array;
      }
   }
}
