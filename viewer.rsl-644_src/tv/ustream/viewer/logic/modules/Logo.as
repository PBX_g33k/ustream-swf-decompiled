package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.display.Sprite;
   import tv.ustream.tools.Locker;
   import flash.utils.ByteArray;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.This;
   import tv.ustream.assets.Logo;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.events.MouseEvent;
   import tv.ustream.viewer.logic.media.SlideShow;
   import tv.ustream.tools.Debug;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.viewer.logic.media.Monitor;
   import flash.events.Event;
   import flash.geom.Rectangle;
   import tv.ustream.modules.ModuleManager;
   
   public class Logo extends Module
   {
      
      public function Logo(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         _padding = 
            {
               "top":0,
               "right":0,
               "bottom":0,
               "left":0
            };
         _logoCache = {};
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param2)
         {
            info(param2);
         }
      }
      
      public static const CLICK:String = "Logo.Click";
      
      private var _canvas:Sprite;
      
      private var locker:Locker;
      
      private var _logoData:Array;
      
      private var _logoObjects:Array;
      
      private var _logoCache:Object;
      
      protected var logoScale:Number = 1;
      
      private var logoOffsetX:Number = 0;
      
      private var logoOffsetY:Number = 0;
      
      private var _mobile:Boolean = false;
      
      private var _recorded:Boolean = false;
      
      private var _live:Boolean = false;
      
      private var _padding:Object;
      
      override public function get data() : * {
         return null;
      }
      
      private var verbose:Boolean = true;
      
      public var onStatusBar:Boolean = false;
      
      override protected function info(param1:*) : void {
         var _loc2_:* = null;
         var _loc9_:* = null;
         var _loc4_:* = 0;
         var _loc3_:* = null;
         var _loc6_:* = null;
         var _loc5_:* = null;
         var _loc7_:* = false;
         _loc2_ = _loc2_ as ViewerModuleManager;
         if(lastInfoSource == "ums" && !(infoSource == "ums") && logic && logic.ums && logic.ums.connected)
         {
            echo("ums module connected, skipping data");
            return;
         }
         echo("parse data");
         _logoData = [];
         if(param1 is Array)
         {
            _loc4_ = 0;
            while(_loc4_ < param1.length)
            {
               _loc9_ = new ByteArray();
               _loc9_.writeObject(param1[_loc4_]);
               _loc9_.position = 0;
               _logoData.push(_loc9_.readObject());
               _loc4_++;
            }
         }
         else if(typeof param1 == "object")
         {
            if(param1.hasOwnProperty("url") && !param1.url)
            {
               destroy();
               return;
            }
            _loc9_ = new ByteArray();
            _loc9_.writeObject(param1);
            _loc9_.position = 0;
            _logoData.push(_loc9_.readObject());
         }
         
         onStatusBar = false;
         if(Logic.isNewViewer && !Logic.keepLogoOverlay)
         {
            _loc4_ = 0;
            while(_loc4_ < _logoData.length)
            {
               if(!_logoData[_loc4_].url)
               {
                  _logoData.splice(_loc4_,1);
                  onStatusBar = true;
               }
               _loc4_++;
            }
         }
         echo("create logos");
         _logoObjects = [];
         var _loc8_:String = This.secure?"https":"http";
         echo("protocolPrefix: " + _loc8_);
         _loc4_ = 0;
         while(_loc4_ < _logoData.length)
         {
            if(_logoData[_loc4_].margin != undefined)
            {
               _logoData[_loc4_].margin = _logoData[_loc4_].margin;
            }
            if(_logoData[_loc4_].align != undefined)
            {
               _logoData[_loc4_].align = _logoData[_loc4_].align;
            }
            _loc6_ = _logoData[_loc4_].url?This.secure?_logoData[_loc4_].secureUrl:_logoData[_loc4_].url:"ustream";
            _loc5_ = _logoCache[_loc6_];
            if(!_loc5_)
            {
               echo("created " + _loc6_);
               _loc5_ = new tv.ustream.assets.Logo(_loc6_);
               _logoCache[_loc6_] = new tv.ustream.assets.Logo(_loc6_);
               if(_loc6_ == "ustream")
               {
                  _loc5_.addEventListener("exitFrame",onLogoExitFrame);
               }
               else
               {
                  _loc5_.addEventListener("complete",onLogoLoadComplete);
               }
            }
            else
            {
               echo("from cache: " + _loc6_);
            }
            if(_loc6_ == "ustream")
            {
               _loc5_.recorded = _recorded;
               _loc5_.live = _live;
               _loc5_.mobile = _mobile;
            }
            if(_logoData[_loc4_].mobile)
            {
               _mobile = _logoData[_loc4_].mobile;
               _loc5_.mobile = _logoData[_loc4_].mobile;
            }
            _loc7_ = false;
            if(logic is Recorded && (logic as Recorded).hash || Logic.hasInstance && (Logic.instance.hash || Logic.instance.isLimitedChannel))
            {
               _loc7_ = true;
            }
            if(!_loc7_ && (!(_logoData[_loc4_].clickurl == undefined) && _logoData[_loc4_].clickurl.length > 0 || (!(_logoData[_loc4_].clickUrl == undefined) && _logoData[_loc4_].clickUrl.length > 0) || _loc6_ == "ustream"))
            {
               if(!_loc5_.hasEventListener("click"))
               {
                  _loc5_.addEventListener("click",onLogoClick);
               }
            }
            else if(_loc5_.hasEventListener("click"))
            {
               _loc5_.removeEventListener("click",onLogoClick);
            }
            
            _loc10_ = _loc5_.hasEventListener("click");
            _loc5_.buttonMode = _loc10_;
            _loc5_.mouseEnabled = _loc10_;
            _logoObjects.push(_loc5_);
            _loc4_++;
         }
         echo("update canvas");
         if(_canvas)
         {
            while(_canvas.numChildren)
            {
               _canvas.removeChildAt(0);
            }
            _loc4_ = 0;
            while(_loc4_ < _logoObjects.length)
            {
               _canvas.addChild(_logoObjects[_loc4_]);
               _loc4_++;
            }
            updateLogoPos();
         }
         lastInfoSource = infoSource;
         dispatch("update");
      }
      
      private function onLogoClick(param1:MouseEvent) : void {
         var _loc5_:* = null;
         var _loc3_:* = 0;
         var _loc2_:Logo = param1.currentTarget as tv.ustream.assets.Logo;
         var _loc4_:Recorded = logic is Recorded?logic as Recorded:logic is SlideShow && (Logic.hasInstance) && Logic.instance.media?Logic.instance.media:null;
         if(_loc2_)
         {
            echo("logo.brand : " + _loc2_.brand);
            if(_loc2_.brand == "ustream" && _loc4_)
            {
               _loc5_ = _loc4_.url;
            }
            else
            {
               _loc3_ = 0;
               while(_loc3_ < _logoData.length)
               {
                  if(_loc2_.brand == _logoData[_loc3_].url || _loc2_.brand == _logoData[_loc3_].secureUrl)
                  {
                     Debug.explore(_logoData[_loc3_]);
                     if(_logoData[_loc3_].clickurl != undefined)
                     {
                        _loc5_ = _logoData[_loc3_].clickurl;
                     }
                     else if(_logoData[_loc3_].clickUrl != undefined)
                     {
                        _loc5_ = _logoData[_loc3_].clickUrl;
                     }
                     
                     break;
                  }
                  _loc3_++;
               }
            }
            if(_loc5_ && _loc4_ && !(_loc4_.pageUrl == _loc5_))
            {
               navigateToURL(new URLRequest(_loc5_),"_blank");
               if(_loc4_ && !(_loc4_ is Monitor))
               {
                  _loc4_.pause();
                  _loc4_.dispatchEvent(new Event("Logo.Click"));
               }
            }
         }
      }
      
      private function onLogoLoadComplete(param1:Event) : void {
         echo("onLogoLoadComplete");
         var _loc2_:Logo = param1.target as tv.ustream.assets.Logo;
         if(_loc2_.parent)
         {
            updateLogoPos();
         }
         _loc2_.removeEventListener("complete",onLogoLoadComplete);
      }
      
      private function onLogoExitFrame(param1:Event) : void {
         var _loc2_:Logo = param1.target as tv.ustream.assets.Logo;
         _loc2_.live = _live;
         _loc2_.recorded = _recorded;
         _loc2_.mobile = _mobile;
         _loc2_.removeEventListener("exitFrame",onLogoExitFrame);
      }
      
      protected function onCanvasResize(param1:Event = null) : void {
         if(locker.width < 320)
         {
            logoScale = locker.width / 320;
         }
         else
         {
            logoScale = 1;
         }
         updateLogoPos();
      }
      
      protected function updateLogoPos(... rest) : void {
         var _loc4_:* = 0;
         var _loc5_:* = null;
         var _loc8_:* = NaN;
         var _loc6_:* = NaN;
         var _loc2_:* = NaN;
         var _loc7_:* = NaN;
         var _loc3_:* = null;
         if(!_logoObjects || !_canvas || !locker)
         {
            return;
         }
         _loc4_ = 0;
         while(_loc4_ < _logoObjects.length)
         {
            _loc5_ = _logoObjects[_loc4_] as tv.ustream.assets.Logo;
            if(_loc5_)
            {
               _loc9_ = 0;
               _loc5_.y = _loc9_;
               _loc5_.x = _loc9_;
               _loc8_ = locker.width / locker.scaleX;
               _loc6_ = locker.height / locker.scaleX;
               _loc2_ = _logoData[_loc4_].margin?_logoData[_loc4_].margin:0;
               if(rest[0] != false)
               {
                  _loc2_ = _loc2_ / locker.scaleX;
               }
               if(_loc5_.brand != "ustream")
               {
                  _loc7_ = Math.min(1,Math.min(_loc6_ / 7 * 2 / _loc5_.height,_loc8_ / 7 * 2 / _loc5_.width));
                  _loc9_ = _loc7_;
                  _loc5_.scaleY = _loc9_;
                  _loc5_.scaleX = _loc9_;
                  _loc2_ = Math.round(_loc2_ * _loc7_);
               }
               else
               {
                  _loc9_ = logoScale;
                  _loc5_.scaleY = _loc9_;
                  _loc5_.scaleX = _loc9_;
               }
               _loc3_ = _loc5_.getBounds(locker);
            }
            if(_loc5_)
            {
               _loc4_++;
            }
            else
            {
               _loc4_++;
            }
         }
      }
      
      public function set canvas(param1:Sprite) : void {
         var _loc2_:* = 0;
         if(param1 && param1.parent is Locker)
         {
            if(_canvas != param1)
            {
               if(_canvas)
               {
                  while(_canvas.numChildren)
                  {
                     _canvas.removeChildAt(0);
                  }
                  _canvas.removeEventListener("resize",onCanvasResize);
               }
               _canvas = param1;
               locker = _canvas.parent as Locker;
               _canvas.addEventListener("resize",onCanvasResize);
               echo("canvas set");
               if(_logoObjects && _logoObjects.length)
               {
                  _loc2_ = 0;
                  while(_loc2_ < _logoObjects.length)
                  {
                     _canvas.addChild(_logoObjects[_loc2_]);
                     _loc2_++;
                  }
               }
               updateLogoPos();
            }
         }
         else
         {
            if(_canvas)
            {
               while(_canvas.numChildren)
               {
                  _canvas.removeChildAt(0);
               }
               _canvas.removeEventListener("resize",onCanvasResize);
            }
            _canvas = null;
            locker = null;
         }
      }
      
      public function get mobile() : Boolean {
         return _mobile;
      }
      
      public function set mobile(param1:Boolean) : void {
         if(_mobile != param1)
         {
            _mobile = param1;
            updateLogoFlags();
         }
      }
      
      public function get recorded() : Boolean {
         return _recorded;
      }
      
      public function set recorded(param1:Boolean) : void {
         if(_recorded != param1)
         {
            _recorded = param1;
            updateLogoFlags();
         }
      }
      
      public function get live() : Boolean {
         return _live;
      }
      
      public function set live(param1:Boolean) : void {
         if(_live != param1)
         {
            _live = param1;
            updateLogoFlags();
         }
      }
      
      private function updateLogoFlags() : void {
         var _loc1_:* = 0;
         var _loc2_:* = null;
         if(_logoObjects && _logoObjects.length)
         {
            _loc1_ = 0;
            while(_loc1_ < _logoObjects.length)
            {
               _loc2_ = _logoObjects[_loc1_] as tv.ustream.assets.Logo;
               if(_loc2_.brand == "ustream")
               {
                  _loc2_.live = _live;
                  _loc2_.recorded = _recorded;
                  _loc2_.mobile = _mobile;
               }
               _loc1_++;
            }
         }
      }
      
      public function get logos() : Array {
         var _loc2_:* = 0;
         var _loc1_:Array = [];
         _loc2_ = 0;
         while(_loc2_ < _logoData.length)
         {
            _loc1_.push(_logoData[_loc2_]);
            _loc2_++;
         }
         return _loc1_;
      }
      
      public function get padding() : Object {
         return _padding;
      }
      
      public function set padding(param1:Object) : void {
         var _loc3_:* = 0;
         _padding = param1;
         var _loc2_:Array = ["top","left","bottom","right"];
         _loc3_ = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(_padding[_loc2_[_loc3_]] == undefined || (isNaN(_padding[_loc2_[_loc3_]])))
            {
               _padding[_loc2_[_loc3_]] = 0;
            }
            else
            {
               _padding[_loc2_[_loc3_]] = Math.max(0,Math.min(_padding[_loc2_[_loc3_]],78));
            }
            _loc3_++;
         }
         updateLogoPos();
      }
      
      public function logoOffset(param1:Number, param2:Number) : void {
         logoOffsetX = param1;
         logoOffsetY = param2;
         updateLogoPos();
      }
      
      override public function destroy(... rest) : * {
         echo("destroy");
         if(_canvas)
         {
            canvas = null;
         }
         if(_logoCache)
         {
            _loc4_ = 0;
            _loc3_ = _logoCache;
            for(_loc2_ in _logoCache)
            {
               if(_logoCache[_loc2_].hasEventListener("exitFrame"))
               {
                  _logoCache[_loc2_].removeEventListener("exitFrame",onLogoExitFrame);
               }
               if(_logoCache[_loc2_].hasEventListener("complete"))
               {
                  _logoCache[_loc2_].removeEventListener("complete",onLogoLoadComplete);
               }
            }
         }
         return super.destroy();
      }
      
      override protected function echo(param1:String, param2:int = -1) : void {
         if(verbose)
         {
            super.echo(param1,param2);
         }
      }
   }
}
