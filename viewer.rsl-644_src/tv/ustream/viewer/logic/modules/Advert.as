package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Advert extends Module
   {
      
      public function Advert(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      private var _revShare:Object;
      
      private var _disableAds:Boolean = false;
      
      private var _init:Object;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1.revShare)
         {
            _revShare = param1.revShare;
         }
         if(param1.disableAds)
         {
            _disableAds = param1.disableAds;
            _init = null;
         }
         if(param1.init)
         {
            _disableAds = false;
            _init = param1.init;
         }
         dispatch("update");
      }
      
      public function get revShare() : Object {
         return _revShare;
      }
      
      public function get disableAds() : Boolean {
         return _disableAds;
      }
      
      public function get init() : Object {
         return _init;
      }
   }
}
