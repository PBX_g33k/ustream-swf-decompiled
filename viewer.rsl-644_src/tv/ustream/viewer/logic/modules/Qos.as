package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.net.NetStream;
   import flash.utils.Timer;
   import flash.events.NetStatusEvent;
   import tv.ustream.debug.Debug;
   import flash.utils.getTimer;
   import tv.ustream.net.ChunkStream;
   import flash.events.TimerEvent;
   import tv.ustream.modules.ModuleManager;
   
   public class Qos extends Module
   {
      
      public function Qos(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         processTimer = new Timer(1000);
         processTimer.addEventListener("timer",process);
         eventList = [];
         super(param1,param2,param3);
         if(param2)
         {
            info(param2);
         }
      }
      
      public static const STREAM_STALLED:String = "streamStalled";
      
      public static const STREAM_UNSTABLE:String = "streamUnstable";
      
      public static const USED_BANDWIDTH:String = "usedBandwidth";
      
      private const STALL_THRESHOLD:uint = 10000;
      
      private const NETSTREAM_BUFFER_EMPTY:String = "NetStream.Buffer.Empty";
      
      private const NETSTREAM_BUFFER_FULL:String = "NetStream.Buffer.Full";
      
      private var _netStream:NetStream;
      
      private var updateTimer:Timer;
      
      private var bufferTimer:Timer;
      
      private var lastBufferTimer:Number = -1;
      
      private var processTimer:Timer;
      
      private var _measureInterval:uint = 28000;
      
      private var _emptyCount:uint = 0;
      
      private var _emptyRate:Number = 0;
      
      private var eventList:Array;
      
      private var _stable:Boolean = false;
      
      private var _currentBandwidth:Number = -1;
      
      private var _averageBandwidth:Number = -1;
      
      private var _bandwidthList:Array;
      
      private var _maximumBandwidth:Number = -1;
      
      private var _currentBitrate:Number = -1;
      
      override protected function info(param1:*) : void {
         if(param1.stream is NetStream && param1.stream.hasOwnProperty("info"))
         {
            if(_netStream)
            {
               nsCleanup();
            }
            _netStream = param1.stream;
            nsMount(param1.stream);
         }
      }
      
      private function nsMount(param1:NetStream) : void {
         _netStream.addEventListener("netStatus",onStreamNetStatus,false,9999);
         eventList = [];
         processTimer.delay = _measureInterval;
         processTimer.reset();
         processTimer.start();
      }
      
      private function nsCleanup() : void {
         _netStream.removeEventListener("netStatus",onStreamNetStatus);
         processTimer.reset();
         _currentBandwidth = -1;
         _currentBitrate = -1;
         _maximumBandwidth = -1;
      }
      
      private function onStreamNetStatus(param1:NetStatusEvent) : void {
         Debug.debug("qos","onStreamNetStatus",param1.info);
         var _loc2_:* = param1.info.code;
         if("NetStream.Buffer.Empty" !== _loc2_)
         {
            if("NetStream.Buffer.Full" !== _loc2_)
            {
               if("NetStream.Pause.Notify" !== _loc2_)
               {
                  if("NetStream.Play.Stop" !== _loc2_)
                  {
                     if("NetStream.Unpause.Notify" !== _loc2_)
                     {
                        if("NetStream.Play.Reset" !== _loc2_)
                        {
                           if("NetStream.Play.Start" !== _loc2_)
                           {
                              if("NetStream.Play.Transition" !== _loc2_)
                              {
                                 if("ChunkStream.Load.Complete" !== _loc2_)
                                 {
                                    if("ChunkStream.Load.Timeout" === _loc2_)
                                    {
                                       if(_netStream.bufferLength < 0.2)
                                       {
                                          _emptyRate = 1;
                                          _emptyCount = 1;
                                          _stable = false;
                                          echo("Chunk load timeout");
                                          dispatch("update");
                                       }
                                    }
                                 }
                                 else
                                 {
                                    updateChunkStreamInfo();
                                    dispatch("usedBandwidth",false,
                                       {
                                          "url":param1.info.reason,
                                          "bytesLoaded":param1.info.size,
                                          "time":param1.info.time
                                       },true);
                                 }
                              }
                              else
                              {
                                 reset();
                                 _loc2_ = param1.info.reason;
                                 if("NetStream.Transition.Success" !== _loc2_)
                                 {
                                    if("NetStream.Transition.Forced" !== _loc2_)
                                    {
                                    }
                                 }
                                 eventList.push(
                                    {
                                       "code":"NetStream.Buffer.Full",
                                       "timer":getTimer()
                                    });
                                 processBufferEvents();
                              }
                           }
                           else
                           {
                              reset();
                              eventList.push(
                                 {
                                    "code":"NetStream.Buffer.Empty",
                                    "timer":getTimer(),
                                    "noCount":true
                                 });
                              processBufferEvents();
                           }
                        }
                     }
                     reset();
                  }
                  reset();
               }
               reset();
            }
            return;
         }
         eventList.push(
            {
               "code":param1.info.code,
               "timer":getTimer()
            });
         processBufferEvents();
      }
      
      private function updateChunkStreamInfo() : void {
         var _loc1_:* = 0;
         echo("updateChunkStreamInfo");
         if(_netStream is ChunkStream)
         {
            _currentBandwidth = _netStream.info.currentBytesPerSecond;
            _maximumBandwidth = _netStream.info.maxBytesPerSecond;
            if(!_bandwidthList)
            {
               _bandwidthList = [];
            }
            _bandwidthList.push(_currentBandwidth);
            _averageBandwidth = -1;
            _loc1_ = 0;
            while(_loc1_ < _bandwidthList.length)
            {
               _averageBandwidth = _averageBandwidth + _bandwidthList[_loc1_];
               _loc1_++;
            }
            _averageBandwidth = _averageBandwidth / _bandwidthList.length;
            while(_bandwidthList.length > 5)
            {
               _bandwidthList.splice(0,1);
            }
            _currentBitrate = _netStream.info.playbackBytesPerSecond;
            dispatch("update");
         }
      }
      
      private function process(param1:TimerEvent) : void {
         processTimer.delay = 1000;
         processBufferEvents();
         if(!(_netStream is ChunkStream))
         {
            processBandwidth();
         }
      }
      
      private var _lastBytesLoaded:uint = 0;
      
      private var _lastBufferTime:uint = 0;
      
      private function processBandwidth() : void {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         if(_lastBufferTime)
         {
            _loc1_ = _netStream.info.byteCount - _lastBytesLoaded;
            _loc2_ = getTimer() - _lastBufferTime;
            if(_loc1_)
            {
               dispatch("usedBandwidth",false,
                  {
                     "bytesLoaded":_loc1_,
                     "time":_loc2_
                  },true);
            }
            _lastBytesLoaded = _netStream.info.byteCount;
         }
         _lastBufferTime = getTimer();
      }
      
      private function processBufferEvents(param1:Boolean = false) : void {
         var _loc3_:* = 0;
         var _loc4_:* = 0;
         var _loc8_:* = 0;
         var _loc5_:* = 0;
         var _loc7_:* = 0;
         var _loc6_:uint = _emptyCount;
         var _loc9_:Number = _emptyRate;
         var _loc2_:Boolean = _stable;
         _emptyCount = 0;
         _emptyRate = 0;
         _stable = false;
         if(eventList.length)
         {
            _loc3_ = getTimer();
            _loc4_ = getTimer();
            _loc8_ = 0;
            _loc5_ = 0;
            _loc7_ = eventList.length - 1;
            while(_loc7_ >= 0)
            {
               if(_loc3_ - eventList[_loc7_].timer > _measureInterval)
               {
                  Debug.warn("qos","entry time exceeds measureinterval, cutting point : " + (_loc3_ - _measureInterval) + ", entry : " + eventList[_loc7_].timer);
                  eventList[_loc7_].timer = _loc3_ - _measureInterval;
                  eventList.splice(0,_loc7_);
                  _loc7_ = 0;
               }
               _loc10_ = eventList[_loc7_].code;
               if("NetStream.Buffer.Empty" !== _loc10_)
               {
                  if("NetStream.Buffer.Full" === _loc10_)
                  {
                     _loc8_ = _loc8_ + (_loc4_ - eventList[_loc7_].timer);
                  }
               }
               else
               {
                  _loc5_ = _loc5_ + (_loc4_ - eventList[_loc7_].timer);
                  if(!eventList[_loc7_].noCount)
                  {
                     _emptyCount = _emptyCount + 1;
                  }
               }
               _loc4_ = eventList[_loc7_].timer;
               _loc7_--;
            }
            _emptyRate = Math.floor(_loc5_ / _measureInterval * 1000) / 1000;
            _stable = _loc8_ == _measureInterval && _netStream.bufferLength > 1;
            Debug.debug("qos","emptyTime : " + _loc5_ + ", fullTime : " + _loc8_ + ", emptyRate : " + _emptyRate,eventList);
         }
         else if(param1)
         {
            dispatch("streamStalled");
         }
         
         if(!(_stable == _loc2_) || !(_emptyCount == _loc6_) || !(_emptyRate == _loc9_))
         {
            dispatch("update",false,null,true);
         }
      }
      
      override public function destroy(... rest) : * {
         nsCleanup();
         processTimer.stop();
         processTimer.removeEventListener("timer",process);
         processTimer = null;
         return super.destroy();
      }
      
      public function get measureInterval() : uint {
         return _measureInterval;
      }
      
      public function set measureInterval(param1:uint) : void {
         _measureInterval = param1;
         processBufferEvents();
      }
      
      public function get emptyCount() : uint {
         return _emptyCount;
      }
      
      public function get emptyRate() : Number {
         return _emptyRate;
      }
      
      public function get stable() : Boolean {
         return _stable;
      }
      
      public function get currentBandwidth() : Number {
         return Math.floor(_currentBandwidth);
      }
      
      public function get averageBandwidth() : Number {
         return Math.floor(_averageBandwidth);
      }
      
      public function get maximumBandwidth() : Number {
         return Math.floor(_maximumBandwidth);
      }
      
      public function get currentBitrate() : Number {
         return Math.floor(_currentBitrate);
      }
      
      public function get bandwidthAvailable() : Boolean {
         return _netStream is ChunkStream;
      }
      
      public function get bandwidthReliable() : Boolean {
         return _bandwidthList && _bandwidthList.length > 3;
      }
      
      public function reset(param1:Boolean = false) : void {
         Debug.warn("qos","reset");
         var _loc2_:Object = eventList.pop();
         eventList = [];
         if(param1 && _loc2_)
         {
            _loc2_.timer = getTimer();
            eventList.push(_loc2_);
         }
         Debug.debug("qos","eventList",eventList);
         _bandwidthList = null;
         _lastBytesLoaded = _netStream.info.byteCount;
         _lastBufferTime = getTimer();
         processTimer.delay = _measureInterval;
         processTimer.reset();
         processTimer.start();
         processBufferEvents();
         updateChunkStreamInfo();
      }
   }
}
