package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Tcdn extends Module
   {
      
      public function Tcdn(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      public static const SWITCH_TO:int = 1;
      
      public static const IGNORE:int = 0;
      
      public static const LEAVE:int = -1;
      
      protected var _status:int = 0;
      
      override protected function info(param1:*) : void {
         var _loc2_:* = NaN;
         var _loc4_:* = 0;
         var _loc3_:* = 0;
         var _loc5_:* = 0.0;
         if(param1.hasOwnProperty("percent"))
         {
            _loc5_ = param1["percent"];
         }
         else if(param1.hasOwnProperty("threshold"))
         {
            _loc2_ = 0.0;
            if(param1.viewers != undefined)
            {
               _loc2_ = param1.viewers;
            }
            else if((manager as ViewerModuleManager).viewers is Viewers)
            {
               _loc2_ = (manager as ViewerModuleManager).viewers.viewerCount;
            }
            
            _loc5_ = _loc2_ > 0?(param1["threshold"]) / _loc2_:0.0;
         }
         
         if(_loc5_ != 0)
         {
            _loc4_ = Math.abs(_loc5_) / _loc5_;
            _loc5_ = Math.abs(_loc5_);
            _loc3_ = _status;
            _status = _loc4_ * (Math.random() < _loc5_?1:0.0);
            echo("_status : " + _status);
            if(_loc3_ != _status)
            {
               dispatch("update");
            }
         }
      }
      
      public function get cdnStatus() : int {
         return _status;
      }
   }
}
