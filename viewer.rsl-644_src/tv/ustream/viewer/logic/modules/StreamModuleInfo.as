package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class StreamModuleInfo extends Module
   {
      
      public function StreamModuleInfo(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      public function send(param1:Object) : void {
         if(_connection && active)
         {
            call("send",param1);
         }
      }
   }
}
