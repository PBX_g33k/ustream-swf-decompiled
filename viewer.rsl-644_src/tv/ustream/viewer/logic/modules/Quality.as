package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import tv.ustream.net.interfaces.ICommunicator;
   import flash.utils.getTimer;
   import tv.ustream.modules.ModuleManager;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.Logic;
   
   public class Quality extends Module
   {
      
      public function Quality(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         _availableQualities = [];
         cooldownTimer = new Timer(15000,1);
         cooldownTimer.addEventListener("timerComplete",onCooldownComplete);
         if(param2)
         {
            info(param2);
         }
         param1.addEventListener("transitionFailed",onTransitionFailed);
         if(This.getReference(Logic.stage,"loaderInfo.parameters.lowbitrate") == "true")
         {
            _desiredQuality = 0;
            previousDesiredQuality = 0;
         }
         var _loc4_:* = This.getReference(Logic.stage,"loaderInfo.parameters.quality") as String;
         if("hd" !== _loc4_)
         {
            if("best" !== _loc4_)
            {
               if("high" !== _loc4_)
               {
                  if("med" !== _loc4_)
                  {
                     if("medium" !== _loc4_)
                     {
                        if("low" === _loc4_)
                        {
                           previousDesiredQuality = 0;
                           _desiredQuality = 0;
                        }
                     }
                  }
                  previousDesiredQuality = 1;
                  _desiredQuality = 1;
               }
               else
               {
                  previousDesiredQuality = 2;
                  _desiredQuality = 2;
               }
            }
            return;
         }
         previousDesiredQuality = 16;
         _desiredQuality = 16;
      }
      
      public static const ADAPTIVE:String = "adaptive";
      
      public static const LOW:uint = 0;
      
      public static const MEDIUM:uint = 1;
      
      public static const HIGH:uint = 2;
      
      public static const HD:uint = 16;
      
      private const MAX_EMPTY_COUNT:uint = 1;
      
      private const MAX_EMPTY_RATE:Number = 0.5;
      
      private const TOLERABLE_FAILURE_COUNT:uint = 2;
      
      private const FAILURE_RELEASE_DELAY:uint = 600000;
      
      private const BANDWIDTH_THRESHOLD:Number = 1.2;
      
      private const COOLDOWN_TIME:uint = 15000;
      
      protected var _availableQualities:Array;
      
      protected var _desiredQuality:uint = 16;
      
      protected var previousDesiredQuality:uint = 16;
      
      private var _adaptiveMbr:Boolean = false;
      
      private var _adaptiveAvailable:Boolean = true;
      
      private var qos:Qos;
      
      private var bitrateFails:Array;
      
      private var failCount:Array;
      
      private var qualityCap:uint = 0;
      
      private var cooldownTimer:Timer;
      
      private var cooldown:Boolean = false;
      
      private var _screenWidth:Number = Infinity;
      
      private var _screenHeight:Number = Infinity;
      
      private function onCooldownComplete(param1:TimerEvent) : void {
         cooldown = false;
      }
      
      private function onTransitionFailed(param1:Event) : void {
         desiredQuality = previousDesiredQuality;
      }
      
      override protected function info(param1:*) : void {
         /*
          * Decompilation error
          * Code may be obfuscated
          * Error type: TranslateException
          */
         throw new flash.errors.IllegalOperationError("Not decompiled due to error");
      }
      
      override public function get data() : * {
         return null;
      }
      
      public function get desiredQuality() : uint {
         return _desiredQuality;
      }
      
      public function set desiredQuality(param1:uint) : void {
         if(_desiredQuality != param1)
         {
            previousDesiredQuality = _desiredQuality;
            _desiredQuality = param1;
            dispatch("change");
            if(qos)
            {
               qos.reset();
            }
         }
      }
      
      public function get availableQualities() : Array {
         return _availableQualities;
      }
      
      public function set availableQualities(param1:Array) : void {
         _availableQualities = param1;
      }
      
      override public function set connection(param1:ICommunicator) : void {
         _connection = param1;
      }
      
      public function get adaptiveAvailable() : Boolean {
         return _adaptiveAvailable;
      }
      
      protected function setAdaptiveAvailable(param1:Boolean) : void {
         if(_adaptiveAvailable != param1)
         {
            _adaptiveAvailable = param1;
            manageAdaptive();
         }
      }
      
      public function get adaptiveMbr() : Boolean {
         return _adaptiveMbr;
      }
      
      public function set adaptiveMbr(param1:Boolean) : void {
         echo("### adaptiveMbr : " + param1);
         var _loc2_:Boolean = _adaptiveMbr && (_adaptiveAvailable);
         if(_adaptiveMbr != param1)
         {
            _adaptiveMbr = param1;
            manageAdaptive();
         }
      }
      
      override public function destroy(... rest) : * {
         logic.removeEventListener("transitionFailed",onTransitionFailed);
         adaptiveMbr = false;
         if(cooldownTimer)
         {
            cooldownTimer.reset();
            cooldown = false;
         }
         return super.destroy.apply(this,rest);
      }
      
      public function getQualityProperties(param1:uint) : Object {
         var _loc2_:Array = _data as Array;
         if(_loc2_ && _loc2_[param1])
         {
            return _loc2_[param1];
         }
         return null;
      }
      
      private function onCreateQos(param1:Event) : void {
         echo("onCreateQos");
         qos = (manager as ViewerModuleManager).qos;
         if(qos)
         {
            qos.addEventListener("destroy",onDestrosQos);
            qos.addEventListener("update",onQosUpdate);
         }
      }
      
      private function onDestrosQos(param1:Event) : void {
         qos.removeEventListener("destroy",onDestrosQos);
         qos.removeEventListener("update",onQosUpdate);
         qos = null;
      }
      
      private function onQosUpdate(param1:Event) : void {
         var _loc7_:* = 0;
         var _loc2_:* = null;
         var _loc4_:* = 0;
         var _loc3_:* = 0;
         var _loc8_:* = 0;
         var _loc5_:Qos = param1.target as Qos;
         var _loc6_:uint = _availableQualities.indexOf(desiredQuality);
         echo("module.bandwidthAvailable : " + _loc5_.bandwidthAvailable);
         if(_loc5_.bandwidthAvailable && (hasBitrate()))
         {
            echo("module.averageBandwidth : " + _loc5_.averageBandwidth);
            echo("module.currentBandwidth : " + _loc5_.currentBandwidth);
            echo("module.maximumBandwidth : " + _loc5_.maximumBandwidth);
            echo("module.currentBitrate : " + _loc5_.currentBitrate);
            echo("module.stable : " + _loc5_.stable);
            _loc2_ = [];
            _loc7_ = 0;
            while(_loc7_ < _data.length)
            {
               _loc2_.push(_loc7_ == _loc6_?"[" + _data[_loc7_].bitrate + "]":_data[_loc7_].bitrate);
               _loc7_++;
            }
            echo("stream bitrates : " + _loc2_.join(" "));
            _loc4_ = _loc5_.stable?_loc5_.averageBandwidth:_loc5_.currentBandwidth;
            _loc3_ = _loc6_;
            if(_loc4_ > _data[_loc6_].bitrate * 1.2 && (compareSizes(_loc6_)))
            {
               echo("#### BW+");
               if(_loc5_.bandwidthReliable)
               {
                  _loc4_ = _loc5_.averageBandwidth;
                  _loc7_ = _loc6_;
                  while(_loc7_ >= 0)
                  {
                     echo("--- " + _loc7_ + " " + _loc4_ + " " + _data[_loc7_].bitrate * 1.2);
                     if(_loc4_ >= _data[_loc7_].bitrate * 1.2 && (compareSizes(_loc7_)))
                     {
                        _loc3_ = _loc7_;
                     }
                     _loc7_--;
                  }
               }
            }
            else if(!(_loc4_ == -1) && _loc4_ < _data[_loc6_].bitrate || !compareSizes(_loc6_))
            {
               echo("#### BW-");
               _loc3_ = _data.length - 1;
               _loc7_ = _loc6_;
               while(_loc7_ < _data.length)
               {
                  echo("--- " + _loc7_ + " " + _loc4_ + " " + _data[_loc7_].bitrate * 1.2);
                  if(_loc4_ >= _data[_loc7_].bitrate * 1.2 && (compareSizes(_loc7_)))
                  {
                     _loc3_ = _loc7_;
                     break;
                  }
                  _loc7_++;
               }
            }
            
            echo("swapIndex : " + _loc3_);
            echo("index : " + _loc6_);
            if(_loc3_ != _loc6_)
            {
               if(!cooldown)
               {
                  echo("### switching " + (_loc3_ > _loc6_?"down":"up") + " to " + _loc3_ + " (" + _data[_loc3_].bitrate + ")");
                  desiredQuality = availableQualities[_loc3_];
                  if(cooldownTimer)
                  {
                     cooldown = true;
                     cooldownTimer.reset();
                     cooldownTimer.start();
                  }
               }
               else
               {
                  echo("### cooldown prevents switching");
               }
            }
         }
         else
         {
            _loc8_ = qualityCap;
            qualityCap = 0;
            failCount = [];
            _loc7_ = 0;
            while(_loc7_ < bitrateFails.length)
            {
               if(getTimer() - bitrateFails[_loc7_].timer > 600000)
               {
                  _loc7_--;
                  bitrateFails.splice(_loc7_,1);
               }
               else if(failCount[bitrateFails[_loc7_].index] == undefined)
               {
                  failCount[bitrateFails[_loc7_].index] = 1;
               }
               else
               {
                  _loc9_ = failCount;
                  _loc10_ = bitrateFails[_loc7_].index;
                  _loc11_ = _loc9_[_loc10_] + 1;
                  _loc9_[_loc10_] = _loc11_;
               }
               
               _loc7_++;
            }
            _loc7_ = 0;
            while(_loc7_ < _availableQualities.length)
            {
               if(failCount[_loc7_] >= 2)
               {
                  qualityCap = _loc7_ + 1;
               }
               _loc7_++;
            }
            if(qualityCap != _loc8_)
            {
               echo("quality cap changed: " + _loc8_ + " > " + qualityCap);
            }
            else if(qualityCap > 0)
            {
               echo("quality cap in place: " + qualityCap);
            }
            
            if(qos.stable && _loc6_ > qualityCap)
            {
               echo("\n\nswitching up from " + _loc6_ + " to " + (_loc6_ - 1) + "\n\n");
               desiredQuality = _availableQualities[--_loc6_];
            }
            else if((qos.emptyCount >= 1 || qos.emptyRate >= 0.5) && _loc6_ < _availableQualities.length - 1)
            {
               echo("\n\nswitching down from " + _loc6_ + " to " + (_loc6_ + 1) + "\n\n");
               bitrateFails.push(
                  {
                     "index":_loc6_,
                     "timer":getTimer()
                  });
               desiredQuality = _availableQualities[++_loc6_];
            }
            
         }
      }
      
      private function hasBitrate() : Boolean {
         var _loc1_:* = 0;
         var _loc2_:Array = _data as Array;
         _loc1_ = 0;
         while(_loc1_ < _loc2_.length)
         {
            if(!_loc2_[_loc1_].bitrate)
            {
               return false;
            }
            _loc1_++;
         }
         return true;
      }
      
      private function manageAdaptive() : void {
         var _loc1_:Boolean = _adaptiveMbr && (_adaptiveAvailable);
         var _loc2_:* = bitrateFails is Array;
         if(_loc1_ != _loc2_)
         {
            if(_loc1_)
            {
               if(!bitrateFails)
               {
                  bitrateFails = [];
               }
               if(manager is ViewerModuleManager)
               {
                  if((manager as ViewerModuleManager).qos)
                  {
                     onCreateQos(null);
                  }
                  (manager as ViewerModuleManager).addEventListener("createQos",onCreateQos);
               }
            }
            else
            {
               bitrateFails = null;
               if(qos)
               {
                  onDestrosQos(null);
               }
               if(manager is ViewerModuleManager)
               {
                  (manager as ViewerModuleManager).removeEventListener("createQos",onCreateQos);
               }
            }
            dispatch("adaptive");
         }
      }
      
      public function setDisplaySize(param1:Number, param2:Number) : void {
         echo("### setDisplaySize " + param1 + " " + param2);
         _screenHeight = param2;
         _screenWidth = param1;
      }
      
      private function compareSizes(param1:uint) : Boolean {
         if(_data is Array && _data[param1])
         {
            echo("compareSizes ( " + _screenWidth + " " + _screenHeight + " ) <> ( " + _data[param1].width + " " + _data[param1].height + " )");
            return _data[param1].width < _screenWidth * 1.5 && _data[param1].height < _screenHeight * 1.5 && _data[param1].height <= 720;
         }
         echo("compareSizes : data is not array");
         return false;
      }
      
      private function selectVersionBySize() : uint {
         var _loc1_:* = 0;
         if(_data is Array)
         {
            _loc1_ = _data.length - 1;
            while(compareSizes(_loc1_) && _loc1_ > 0)
            {
               _loc1_--;
            }
            return _loc1_;
         }
         return 0;
      }
   }
}
