package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.display.Loader;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.events.Event;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.media.Channel;
   import flash.system.LoaderContext;
   import flash.net.URLRequest;
   import tv.ustream.modules.ModuleManager;
   
   public class Thumbnail extends Module
   {
      
      public function Thumbnail(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         _media = param1 as Recorded;
         if(_media)
         {
            _media.addEventListener("play",onMediaPlay);
            if(_media.id == "43186804" || _media.id == "45426601")
            {
               _media.addEventListener("seekStart",onMediaPlay);
            }
            _media.addEventListener("online",checkThumbnailLoading);
         }
         _display = new Loader();
         _display.contentLoaderInfo.addEventListener("ioError",onSnapError);
         _display.contentLoaderInfo.addEventListener("securityError",onSnapError);
         _display.contentLoaderInfo.addEventListener("complete",onSnapComplete);
      }
      
      public static const LOADING:String = "loading";
      
      public static const COMPLETE:String = "complete";
      
      public static const HIDE:String = "hide";
      
      public static const SHOW:String = "show";
      
      private const THUMBNAIL_SITE_640x360:String = "640x360";
      
      private const THUMBNAIL_SITE_320x180:String = "320x180";
      
      private const THUMBNAIL_SITE_256x144:String = "256x144";
      
      private const THUMBNAIL_SITE_192x108:String = "192x108";
      
      private const THUMBNAIL_SITE_128x72:String = "128x72";
      
      private var _urlList:Vector.<String>;
      
      private var _display:Loader;
      
      private var _media:Recorded;
      
      private var _loading:Boolean;
      
      private function onMediaPlay(param1:Event) : void {
         if(_media.id == "43186804" || _media.id == "45426601")
         {
            hide = true;
            _media.addEventListener("finish",onMediaStop);
         }
         else
         {
            destroy();
         }
      }
      
      private function onMediaStop(param1:Event) : void {
         hide = false;
         _media.removeEventListener("finish",onMediaStop);
      }
      
      private function get hide() : Boolean {
         return !_display.visible;
      }
      
      private function set hide(param1:Boolean) : void {
         _display.visible = !param1;
         dispatch(param1?"hide":"show",false);
      }
      
      override protected function info(param1:*) : void {
         var _loc3_:* = null;
         var _loc2_:* = 0;
         var _loc4_:* = null;
         if(param1 is Object)
         {
            _loc3_ = ["liveThumbnail","thumbnail","channelPicture"];
            _urlList = new Vector.<String>();
            _loc2_ = 0;
            while(_loc2_ < _loc3_.length)
            {
               _loc4_ = _loc3_[_loc2_] + (This.secure?"SecureUrl":"Url");
               if(param1[_loc4_])
               {
                  _urlList.push(param1[_loc4_].split("#SIZE#").join("640x360"));
                  break;
               }
               _loc2_++;
            }
            checkThumbnailLoading();
         }
         _data = param1;
      }
      
      private function checkThumbnailLoading(param1:Event = null) : void {
         if(!_loading && _urlList.length && _media && !_media.autoPlay && !_media.playing && !(_media.type == "channel" && !(_media as Channel).online))
         {
            getSnap(_urlList[0]);
         }
         else if(_media.id == "43186804" || _media.id == "45426601")
         {
            getSnap(_urlList[0]);
            hide = true;
         }
         
      }
      
      override public function destroy(... rest) : * {
         _media.removeEventListener("play",onMediaPlay);
         _media.removeEventListener("online",checkThumbnailLoading);
         _media.removeEventListener("finish",onMediaStop);
         _media.removeEventListener("seekStart",onMediaPlay);
         _media = null;
         _display.contentLoaderInfo.removeEventListener("ioError",onSnapError);
         _display.contentLoaderInfo.removeEventListener("securityError",onSnapError);
         _display.contentLoaderInfo.removeEventListener("complete",onSnapComplete);
         _display.unload();
         if(_display.parent)
         {
            _display.parent.removeChild(_display);
         }
         _display = null;
         _urlList = null;
         return super.destroy();
      }
      
      private function getSnap(param1:String) : void {
         var _loc2_:* = null;
         if(param1)
         {
            _loc2_ = new LoaderContext(true);
            dispatch("loading");
            _loading = true;
            _display.load(new URLRequest(param1),_loc2_);
         }
      }
      
      private function onSnapComplete(... rest) : void {
         dispatch("complete");
         echo("snap loaded " + _display.content.width + " x " + _display.content.height);
      }
      
      private function onSnapError(... rest) : void {
         echo("error " + rest,2);
         _urlList.splice(0,1);
         if(_urlList.length)
         {
            getSnap(_urlList[0]);
         }
         else
         {
            destroy();
         }
      }
      
      public function get display() : Loader {
         return _display;
      }
   }
}
