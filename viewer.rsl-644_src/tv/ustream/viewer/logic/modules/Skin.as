package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Skin extends Module
   {
      
      public function Skin(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param2)
         {
            info(param2);
         }
      }
      
      override protected function info(param1:*) : void {
         if(typeof param1 == "object")
         {
            super.info(param1);
            dispatch("update");
         }
      }
      
      public function update(param1:*) : void {
         info(param1);
      }
   }
}
