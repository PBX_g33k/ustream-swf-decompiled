package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import flash.media.Camera;
   import flash.media.Microphone;
   import flash.net.NetStream;
   import tv.ustream.net.MultiConnection;
   import tv.ustream.net.interfaces.ICommunicator;
   import tv.ustream.tools.Debug;
   import flash.events.NetStatusEvent;
   import flash.system.Security;
   import tv.ustream.modules.ModuleManager;
   
   public class Cohost extends Module
   {
      
      public function Cohost(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         audioQualityValues = [5,8,11,22];
         resourceRequester = [];
         super(param1,param2,param3);
      }
      
      private var _camera:Camera;
      
      private var _microphone:Microphone;
      
      private var _hasCam:Boolean;
      
      private var _hasMic:Boolean;
      
      private var _video:Boolean = true;
      
      private var _audio:Boolean = true;
      
      private var _allowVideo:Boolean = true;
      
      private var _allowAudio:Boolean = true;
      
      private var _audioQuality:Number = 0;
      
      private var audioQualityIndex:Number = 0;
      
      private var audioQualityValues:Array;
      
      private var _videoQuality:Number = 0.3;
      
      private var stream:NetStream;
      
      private var streamHasVideo:Boolean = false;
      
      private var streamHasAudio:Boolean = false;
      
      private var _name:String;
      
      private var _info:String;
      
      private var _psid:String;
      
      private var resourceRequester:Array;
      
      private var netConnection:MultiConnection;
      
      private var streamConnection:MultiConnection;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(param1 == "start")
         {
            dispatch("available");
         }
         if(param1 == "stop")
         {
            dispatch("unavailable");
         }
         dispatch("update");
      }
      
      override public function set connection(param1:ICommunicator) : void {
         if(param1 is MultiConnection)
         {
            if(netConnection)
            {
               _loc2_ = null;
               netConnection.cohostAudioBroadcast = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostRedirect = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostVideoBroadcast = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostStopStream = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostStartStream = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostTerminated = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostDecline = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostAccept = _loc2_;
               _loc2_ = _loc2_;
               netConnection.cohostOffAir = _loc2_;
               netConnection.cohostOnAir = _loc2_;
            }
            netConnection = param1 as MultiConnection;
            if(netConnection)
            {
               netConnection.cohostOnAir = onAir;
               netConnection.cohostOffAir = offAir;
               netConnection.cohostAccept = onAccept;
               netConnection.cohostDecline = onDecline;
               netConnection.cohostTerminate = onTerminate;
               netConnection.cohostStartStream = onStartStream;
               netConnection.cohostStopStream = onStopStream;
               netConnection.cohostVideoBroadcast = onEnableVideoBroadcast;
               netConnection.cohostAudioBroadcast = onEnableAudioBroadcast;
               netConnection.cohostRedirect = onCohostRedirect;
            }
         }
         .super.connection = param1;
      }
      
      private function onAir() : void {
         dispatch("onAir");
      }
      
      private function offAir() : void {
         dispatch("offAir");
      }
      
      private function onAccept() : void {
         dispatch("accept");
      }
      
      private function onDecline() : void {
         dispatch("decline");
         onTerminate();
      }
      
      private function onTerminate() : void {
         dispatch("terminate");
         destroyStream();
         if(streamConnection)
         {
            streamConnection.kill();
            streamConnection = null;
         }
         _id = null;
      }
      
      private function onStartStream(param1:String) : void {
         _psid = param1.substr(param1.lastIndexOf("/") + 1);
         dispatch("startStream");
         if(!stream)
         {
            createStream();
         }
         chkStream();
         stream.publish(param1);
      }
      
      private function onStopStream(... rest) : void {
         dispatch("stopStream");
         destroyStream();
      }
      
      private function onEnableVideoBroadcast(param1:Boolean) : void {
         _allowVideo = param1;
         dispatch("allowVideoUpdate");
         chkStream();
      }
      
      private function onEnableAudioBroadcast(param1:Boolean) : void {
         _allowAudio = param1;
         dispatch("allowAudioUpdate");
         chkStream();
      }
      
      private function onCohostRedirect(param1:Object, ... rest) : void {
         if(param1.fmsUrl && param1.key)
         {
            if(streamConnection)
            {
               streamConnection.kill();
            }
            Debug.echo("get streamConnection to " + param1.fmsUrl);
            streamConnection = new MultiConnection(null,"streamConnection",[1935,80,443],true);
            streamConnection.addEventListener("connected",onStreamConnected);
            streamConnection.addEventListener("disconnected",onStreamConnectionFailed);
            streamConnection.addEventListener("failed",onStreamConnectionFailed);
            streamConnection.connect(param1.fmsUrl,{"cohostKey":param1.key});
         }
      }
      
      private function onStreamConnectionFailed(... rest) : void {
         dispatch("streamError");
         terminate();
      }
      
      private function onStreamConnected(... rest) : void {
         createStream();
      }
      
      public function sendRequest(param1:String = null, param2:String = null) : void {
         if(hasCam || (hasMic))
         {
            if(hasCam && !_camera)
            {
               camera = Camera.getCamera();
            }
            if(hasMic && !_microphone)
            {
               microphone = Microphone.getMicrophone();
            }
            if(param1)
            {
               _name = param1;
            }
            if(param2)
            {
               _info = param2;
            }
            if(chkSources())
            {
               call("sendRequest",
                  {
                     "name":_name,
                     "info":_info,
                     "hasCam":_hasCam,
                     "hasMic":_hasMic
                  });
               dispatch("sentRequest");
            }
            else
            {
               dispatch("noAccessibleSource");
               resourceRequester.push(sendRequest);
            }
         }
         else
         {
            dispatch("noAvailableSource");
         }
      }
      
      public function terminate() : void {
         call("terminate");
         destroyStream();
         onTerminate();
      }
      
      private function createStream() : void {
         destroyStream();
         trace("createStream " + (streamConnection || netConnection).uri);
         stream = new NetStream(streamConnection || netConnection);
         stream.addEventListener("netStatus",onCohostStreamStatus);
      }
      
      private function destroyStream(... rest) : void {
         streamHasAudio = false;
         streamHasVideo = false;
         if(stream)
         {
            stream.close();
            stream = null;
         }
      }
      
      private function onCohostStreamStatus(param1:NetStatusEvent) : void {
         trace("onCohostStreamStatus");
         Debug.explore(param1.info);
      }
      
      private function chkStream() : void {
         if(stream)
         {
            if(!streamHasVideo && (hasAccessibleCam) && (_video) && (_allowVideo))
            {
               streamHasVideo = true;
               stream.attachCamera(_camera);
            }
            else if(streamHasVideo && (!hasAccessibleCam || !_video || !_allowVideo))
            {
               streamHasVideo = false;
               stream.attachCamera(null);
            }
            
            if(!streamHasAudio && (hasAccessibleMic) && (_audio) && (_allowAudio))
            {
               streamHasAudio = true;
               stream.attachAudio(_microphone);
            }
            else if(streamHasAudio && (!hasAccessibleMic || !_audio || !_allowAudio))
            {
               streamHasAudio = false;
               stream.attachAudio(null);
            }
            
         }
      }
      
      private function chkSources() : Boolean {
         if(!hasCam && !hasMic)
         {
            dispatch("noAvailableSource");
         }
         else
         {
            if(hasAccessibleCam || (hasAccessibleMic))
            {
               resourceRequester = [];
               return true;
            }
            Security.showSettings("privacy");
            dispatch("noAccessibleSource");
         }
         return false;
      }
      
      private function chkRequests() : void {
         var _loc3_:* = 0;
         var _loc2_:* = resourceRequester;
         for each(_loc1_ in resourceRequester)
         {
            _loc1_();
         }
         resourceRequester = [];
      }
      
      private function onCamStatus(... rest) : void {
         if(hasAccessibleCam)
         {
            chkRequests();
         }
         chkStream();
      }
      
      private function onMicStatus(... rest) : void {
         if(hasAccessibleMic)
         {
            chkRequests();
         }
         chkStream();
      }
      
      override public function destroy(... rest) : * {
         onTerminate();
         return super.destroy();
      }
      
      public function get camera() : Camera {
         return _camera;
      }
      
      public function set camera(param1:Camera) : void {
         if(_camera)
         {
            _camera.removeEventListener("status",onCamStatus);
         }
         _camera = param1;
         if(_camera)
         {
            _camera.addEventListener("status",onCamStatus);
            _camera.setMode(160,120,11);
            applyVideoQuality();
            streamHasVideo = false;
         }
         onCamStatus();
      }
      
      public function get microphone() : Microphone {
         return _microphone;
      }
      
      public function set microphone(param1:Microphone) : void {
         if(_microphone)
         {
            _microphone.removeEventListener("status",onMicStatus);
         }
         _microphone = param1;
         if(_microphone)
         {
            _microphone.addEventListener("status",onMicStatus);
            streamHasAudio = false;
         }
         onMicStatus();
      }
      
      public function get hasCam() : Boolean {
         _hasCam = new Boolean(Camera.names.length);
         return new Boolean(Camera.names.length);
      }
      
      public function get hasMic() : Boolean {
         _hasMic = new Boolean(Microphone.names.length);
         return new Boolean(Microphone.names.length);
      }
      
      public function get hasAccessibleCam() : Boolean {
         return _camera?!_camera.muted:false;
      }
      
      public function get hasAccessibleMic() : Boolean {
         return _microphone?!_microphone.muted:false;
      }
      
      public function set video(param1:Boolean) : void {
         _video = param1;
         chkStream();
         call("enableVideoBroadcast",{"value":param1});
      }
      
      public function get video() : Boolean {
         return _video;
      }
      
      public function set audio(param1:Boolean) : void {
         _audio = param1;
         chkStream();
         call("enableAudioBroadcast",{"value":param1});
      }
      
      public function get audio() : Boolean {
         return _audio;
      }
      
      public function get allowVideo() : Boolean {
         return _allowVideo;
      }
      
      public function get allowAudio() : Boolean {
         return _allowAudio;
      }
      
      public function get psid() : String {
         return _psid;
      }
      
      public function get audioQuality() : Number {
         return _audioQuality;
      }
      
      public function set audioQuality(param1:Number) : void {
         _audioQuality = Math.min(Math.max(param1,0),1);
         var _loc2_:Number = Math.round(_audioQuality / 0.3333333333333333);
         if(_loc2_ != audioQualityIndex)
         {
            audioQualityIndex = _loc2_;
            applyAudioQuality();
         }
      }
      
      private function applyAudioQuality() : void {
         if(_microphone)
         {
            _microphone.rate = audioQualityValues[audioQualityIndex];
         }
      }
      
      public function get videoQuality() : Number {
         return _videoQuality;
      }
      
      public function set videoQuality(param1:Number) : void {
         _videoQuality = Math.min(Math.max(param1,0),1);
         applyVideoQuality();
      }
      
      private function applyVideoQuality() : void {
         if(_camera)
         {
            _camera.setQuality(0,10 + _videoQuality * 80);
         }
      }
   }
}
