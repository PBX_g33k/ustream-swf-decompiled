package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class Gps extends Module
   {
      
      public function Gps(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param2)
         {
            info(param2);
         }
      }
      
      private var _city:String;
      
      private var _country:String;
      
      private var _longitude:Number;
      
      private var _latitude:Number;
      
      private var _altitude:Number;
      
      override protected function info(param1:*) : void {
         if(typeof param1 == "object")
         {
            super.info(param1);
            if(param1.city)
            {
               _city = param1.city;
            }
            if(param1.country)
            {
               _country = param1.country;
            }
            if(param1.longitude)
            {
               _longitude = param1.longitude;
            }
            if(param1.latitude)
            {
               _latitude = param1.latitude;
            }
            if(param1.altitude)
            {
               _altitude = param1.altitude;
            }
            dispatch("update");
         }
      }
      
      public function get city() : String {
         return _city;
      }
      
      public function get country() : String {
         return _country;
      }
      
      public function get longitude() : Number {
         return _longitude;
      }
      
      public function get latitude() : Number {
         return _latitude;
      }
      
      public function get altitude() : Number {
         return _altitude;
      }
   }
}
