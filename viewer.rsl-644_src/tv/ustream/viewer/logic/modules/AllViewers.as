package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.ModuleManager;
   
   public class AllViewers extends Viewers
   {
      
      public function AllViewers(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      override protected function info(param1:*) : void {
         if(infoSource == "ums")
         {
            super.info(param1);
         }
      }
   }
}
