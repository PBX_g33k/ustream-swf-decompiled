package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.modules.ModuleManager;
   
   public class ClosedCaption extends Module
   {
      
      public function ClosedCaption(param1:*, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         if(param2)
         {
            info(param2);
         }
      }
      
      private var _enabled:Boolean = false;
      
      override protected function info(param1:*) : void {
         super.info(param1);
         if(typeof param1 == "object")
         {
            _enabled = param1.enabled;
         }
      }
      
      public function get enabled() : Boolean {
         return _enabled;
      }
      
      public function set enabled(param1:Boolean) : void {
         if(_enabled != param1)
         {
            _enabled = param1;
            dispatch("update");
         }
      }
   }
}
