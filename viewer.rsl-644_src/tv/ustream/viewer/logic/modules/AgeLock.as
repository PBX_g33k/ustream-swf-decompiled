package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.Shared;
   import flash.events.Event;
   import tv.ustream.tools.Debug;
   import tv.ustream.modules.ModuleManager;
   
   public class AgeLock extends Module
   {
      
      public function AgeLock(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         shared = new Shared();
         super(param1,param2,param3);
         mustBeOnline = false;
         _lock = true;
         param3.addEventListener("createAgeLock",onSelfCreate);
      }
      
      private var _minage:Number;
      
      private var _maxdate:Number;
      
      private var shared:Shared;
      
      private var _age:int;
      
      private function onSelfCreate(param1:Event) : void {
         manager.removeEventListener("createAgeLock",onSelfCreate);
         info(initValue);
      }
      
      override protected function info(param1:*) : void {
         var _loc2_:* = null;
         super.info(param1);
         if(!isNaN(param1))
         {
            _minage = param1;
            echo("numeric data, _minage : " + _minage);
         }
         else if(param1 is Object)
         {
            echo("data is Object");
            Debug.explore(param1);
            if(param1.minimumAge)
            {
               _minage = param1.minimumAge;
               echo("_minage : " + _minage);
            }
            else if(param1.maximumBirthDate is String)
            {
               _loc2_ = param1.maximumBirthDate.split("-");
               _maxdate = new Date(_loc2_[0],_loc2_[1],_loc2_[2]).getTime();
               _minage = calculateAge(_maxdate);
               echo("maximumBirthDate : " + _maxdate);
               echo("_minage : " + _minage);
            }
            
         }
         else
         {
            echo("data is " + typeof param1);
         }
         
         dispatch("update");
         dispatch("lock");
         if(shared.verifiedAge)
         {
            echo("verifying age from previous entry");
            setAge(shared.verifiedAge);
         }
      }
      
      public function setAge(param1:Number) : void {
         Debug.echo("setAge time in AgeLock:  " + param1);
         if(verifyAge(param1))
         {
            dispatch(_lock?"badAge":"ageOk");
            dispatch("lock");
         }
      }
      
      private function verifyAge(param1:Number) : Boolean {
         if(isNaN(param1) || (isNaN(_minage)))
         {
            return false;
         }
         if(_maxdate)
         {
            echo("verifyAge _maxdate : " + _maxdate + ", time : " + param1);
            _lock = _maxdate < param1;
            if(!_lock)
            {
               shared.verifiedDate = param1;
            }
         }
         else if(_minage)
         {
            _age = calculateAge(param1);
            _lock = _age < _minage;
            if(!_lock)
            {
               shared.verifiedAge = param1;
            }
         }
         
         return true;
      }
      
      private function calculateAge(param1:Number) : int {
         var _loc4_:uint = 0;
         var _loc3_:Date = new Date();
         _loc3_.setTime(param1);
         echo("birth date : " + _loc3_.toDateString());
         var _loc2_:Date = new Date();
         echo("current date : " + _loc2_.toDateString());
         _loc4_ = _loc2_.getFullYear() - _loc3_.getFullYear();
         if(_loc2_.getMonth() < _loc3_.getMonth() || (_loc2_.getMonth() == _loc3_.getMonth() && _loc2_.getDate() < _loc3_.getDate()))
         {
            _loc4_--;
         }
         echo("age : " + _loc4_);
         return _loc4_;
      }
      
      public function get minage() : Number {
         return _minage;
      }
      
      public function get age() : int {
         return _age;
      }
   }
}
