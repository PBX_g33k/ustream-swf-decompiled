package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.viewer.logic.modules.stream.Provider;
   import flash.utils.Timer;
   import flash.events.Event;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.viewer.logic.modules.stream.UhsProvider;
   import tv.ustream.viewer.logic.modules.stream.ProgressiveProvider;
   import tv.ustream.viewer.logic.modules.stream.TcdnProvider;
   import tv.ustream.viewer.logic.modules.stream.CdnProvider;
   import flash.utils.describeType;
   import flash.events.EventDispatcher;
   import tv.ustream.tools.DynamicEvent;
   import flash.events.TimerEvent;
   import tv.ustream.viewer.logic.media.Recorded;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.modules.ModuleManager;
   
   public class Stream extends Module
   {
      
      public function Stream(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
         mustBeOnline = false;
         if(param1 is Recorded)
         {
            (param1 as Recorded).addEventListener("providerFailed",onProviderFailed);
            (param1 as Recorded).addEventListener("providerSwitch",onProviderSwitch);
         }
         if(param3 is ViewerModuleManager)
         {
            (param3 as ViewerModuleManager).addEventListener("createQuality",onCreateQuality);
            (param3 as ViewerModuleManager).addEventListener("createTcdn",onCreateTcdn);
         }
         switchTimer = new Timer(600000,1);
         switchTimer.addEventListener("timerComplete",onSwitchTimer);
      }
      
      public static const PROVIDER_CREATED:String = "providerCreated";
      
      public static const PROVIDER_DESTROYED:String = "providerDestroyed";
      
      public static const STATUS_CHANGE:String = "statusChange";
      
      public static const STATUS_UNKNOWN:int = -1;
      
      public static const STATUS_OFFLINE:int = 0;
      
      public static const STATUS_ONLINE:int = 1;
      
      private const FMS:String = "fms";
      
      private const CDN:String = "cdn";
      
      private const UHS:String = "uhs";
      
      private const AHD:String = "ahd";
      
      private const IM360:String = "im360";
      
      private const PROG:String = "progressive";
      
      private var _logicUrl:String;
      
      private var _streamProvider:Provider;
      
      private var _currentProvider:Object;
      
      private var _providerList:Array;
      
      private var providerIndex:int;
      
      private var switchEnabled:Boolean = true;
      
      private var switchTimer:Timer;
      
      private const SWITCH_DELAY:uint = 600000;
      
      private var _status:int = -1;
      
      private var _isLive:Boolean = false;
      
      private var uhsSafeTimer:Timer;
      
      private var _dvrData:Object;
      
      override protected function info(param1:*) : void {
         var _loc2_:* = false;
         echo("update");
         echo("infoSource : " + infoSource + ", lastInfoSource : " + lastInfoSource);
         var param1:* = hackData(param1);
         if(param1)
         {
            if(infoSource == "dvrSeek")
            {
               echo("data stored to dvr");
               if(_streamProvider)
               {
                  _streamProvider.destroy();
               }
               _dvrData = param1;
            }
            else
            {
               echo("data stored to live");
               _data = param1;
               _loc2_ = _data is Array && (_data as Array).length;
               if(_loc2_ != _isLive)
               {
                  _isLive = _loc2_;
                  if(manager is ViewerModuleManager && (manager as ViewerModuleManager).dvr is Dvr)
                  {
                     ((manager as ViewerModuleManager).dvr as Dvr).dispatchEvent(new Event(_isLive?"live":"offair"));
                  }
               }
            }
            parseData();
            parseStreams();
            setStatus(_streamProvider is Provider?1:0);
            lastInfoSource = infoSource;
         }
      }
      
      private function hackData(param1:Object) : Object {
         var _loc3_:* = 0;
         var _loc2_:String = null;
         if(logic.mediaId == "13663180")
         {
            param1 = [
               {
                  "name":"akamai",
                  "url":"360vod",
                  "streams":[{"streamName":"360vod"}]
               }];
         }
         if(_providerList)
         {
            _loc3_ = 0;
            while(_loc3_ < _providerList.length)
            {
               if(_providerList[_loc3_].name == "tcdn")
               {
                  _loc2_ = _providerList[_loc3_].url;
               }
               _loc3_++;
            }
         }
         if(param1 is String)
         {
            echo("data is string: \'" + param1 + "\', faking override object");
            return [];
         }
         if(param1 is Array)
         {
            _loc3_ = 0;
            while(_loc3_ < param1.length)
            {
               if(param1[_loc3_].name == "tcdn" && (logic.mediaId == "13663186" || logic.mediaId == "13663180"))
               {
                  param1.splice(_loc3_--,1);
               }
               else if(_loc2_ && param1[_loc3_].name == "tcdn")
               {
                  param1[_loc3_].url = _loc2_;
               }
               
               _loc3_++;
            }
            return param1;
         }
         if(typeof param1 == "object" && infoSource == "ums" && param1.lock && param1.ustream)
         {
            setStatus(1);
            return null;
         }
         echo("invalid data");
         return null;
      }
      
      private function parseStreams() : void {
         if(_currentProvider && !(_currentProvider.type == "ahd") && _currentProvider.streams is Array && _currentProvider.streams.length > 1)
         {
            updateQualityModule(_currentProvider.streams);
         }
         else if((manager as ViewerModuleManager).quality)
         {
            updateQualityModule(null);
         }
         else
         {
            selectStream(0);
         }
         
      }
      
      private function parseData() : void {
         var _loc5_:* = 0;
         var _loc7_:* = 0;
         var _loc8_:* = 0;
         var _loc3_:* = 0;
         var _loc1_:* = null;
         var _loc2_:* = 0;
         var _loc4_:* = null;
         echo("parseData");
         var _loc6_:Object = _data;
         if(logic is Channel && !((logic as Channel).progress == 1) && _dvrData)
         {
            _loc6_ = _dvrData;
         }
         _logicUrl = "";
         _providerList = [];
         if(_loc6_ is Array)
         {
            _loc5_ = 0;
            while(_loc5_ < _loc6_.length)
            {
               if(_loc6_[_loc5_].streams is Array && (_loc6_[_loc5_].streams as Array).length)
               {
                  providerList.push(_loc6_[_loc5_]);
               }
               _loc5_++;
            }
         }
         if(_providerList.length)
         {
            providerIndex = -1;
            _loc5_ = 0;
            while(_loc5_ < _providerList.length)
            {
               if(logic.mediaId == "13663186" || logic.mediaId == "13663180" || logic.mediaId == "11798606" || logic.mediaId == "11537288" || logic.mediaId == "12830737")
               {
                  _providerList[_loc5_].type = "im360";
               }
               else if(_providerList[_loc5_].name)
               {
                  _loc9_ = _providerList[_loc5_].name.split("_")[0];
                  if("ustream" !== _loc9_)
                  {
                     if("uhs" !== _loc9_)
                     {
                        if("akamaihd" !== _loc9_)
                        {
                           if("tcdn" === _loc9_)
                           {
                              if(logic.type == "channel" && (logic.mediaId == "3064708" || !logic.modules.viewers || logic.modules.viewers && logic.modules.viewers.viewerCount > 1000))
                              {
                                 _loc7_ = 1;
                                 _loc8_ = 2;
                                 _loc3_ = _loc7_;
                                 while(_loc3_ <= _loc8_)
                                 {
                                    _providerList[_loc5_].url = _providerList[_loc5_].url.split("ustreamCdn" + _loc3_).join("ustreamCdn");
                                    _loc3_++;
                                 }
                                 _providerList[_loc5_].url = _providerList[_loc5_].url.split("ustreamCdn").join("ustreamCdn" + Math.round(_loc7_ + (_loc8_ - _loc7_) * Math.random()));
                              }
                           }
                           _providerList[_loc5_].type = "cdn";
                           if(logic && logic.type == "recorded" && _providerList[_loc5_].url && _providerList[_loc5_].url.substr(0,7) == "http://")
                           {
                              _providerList[_loc5_].type = "progressive";
                           }
                        }
                        else
                        {
                           _providerList[_loc5_].type = "ahd";
                        }
                     }
                     else
                     {
                        _providerList[_loc5_].type = "uhs";
                     }
                  }
                  else
                  {
                     _providerList[_loc5_].type = "fms";
                     if(logic.mediaId == "13663186" || logic.mediaId == "13663180" || logic.mediaId == "11382968" || logic.mediaId == "11396284" || logic.mediaId == "8678223")
                     {
                        _providerList[_loc5_].type = "im360";
                     }
                     else if(_providerList[_loc5_].url)
                     {
                        if(!_providerList[_loc5_].url)
                        {
                           delete _providerList.type;
                        }
                        if(_providerList[_loc5_].url.substr(0,7) == "http://")
                        {
                           _providerList[_loc5_].type = "uhs";
                        }
                        else if(logic.type == "channel")
                        {
                           _logicUrl = _providerList[_loc5_].url;
                        }
                        
                     }
                     
                  }
               }
               
               _loc5_++;
            }
            if(_currentProvider && providerIndex == -1)
            {
               _loc5_ = 0;
               while(_loc5_ < _providerList.length && providerIndex == -1)
               {
                  if(_providerList[_loc5_].streams && _providerList[_loc5_].streams.length)
                  {
                     if(_currentProvider.type == "progressive")
                     {
                        _loc1_ = _currentProvider.streamName.split("?")[0];
                        _loc2_ = 0;
                        while(_loc2_ < _providerList[_loc5_].streams.length)
                        {
                           _loc4_ = _providerList[_loc5_].streams[_loc2_].streamName.split("?")[0];
                           if(_loc1_ == _loc4_)
                           {
                              providerIndex = _loc5_;
                              _currentProvider = _providerList[_loc5_];
                              break;
                           }
                           _loc2_++;
                        }
                     }
                     else if(_providerList[_loc5_].url == _currentProvider.url)
                     {
                        echo("url matches, provider set");
                        providerIndex = _loc5_;
                        _currentProvider = _providerList[_loc5_];
                     }
                     
                  }
                  _loc5_++;
               }
            }
            if(providerIndex == -1)
            {
               echo("provider set to index");
               providerIndex = 0;
               _currentProvider = _providerList[0];
            }
         }
         else
         {
            _currentProvider = null;
         }
      }
      
      private function updateProvider() : void {
         var _loc1_:* = false;
         var _loc2_:* = false;
         var _loc3_:* = false;
         var _loc5_:* = false;
         var _loc4_:* = null;
         var _loc8_:* = null;
         var _loc7_:* = 0;
         echo("updateProvider");
         if(_streamProvider)
         {
            _loc1_ = _streamProvider is UhsProvider && _currentProvider && _currentProvider.type == "uhs";
            _loc2_ = _streamProvider is ProgressiveProvider && _currentProvider && _currentProvider.type == "progressive";
            _loc3_ = !_currentProvider || _currentProvider && !(_streamProvider.url == _currentProvider.url);
            if(!_currentProvider || (!(_loc2_ || (_loc1_)) && (_loc3_)))
            {
               _streamProvider.destroy();
            }
            if(_loc2_)
            {
               (_streamProvider as ProgressiveProvider).streamNameService = null;
            }
         }
         if(_currentProvider)
         {
            _loc5_ = false;
            if(!_streamProvider)
            {
               _loc9_ = _currentProvider.type;
               if("cdn" !== _loc9_)
               {
                  if("uhs" !== _loc9_)
                  {
                     if("progressive" !== _loc9_)
                     {
                        _streamProvider = new Provider();
                     }
                     else
                     {
                        _streamProvider = new ProgressiveProvider();
                     }
                  }
                  else
                  {
                     _streamProvider = new UhsProvider();
                  }
               }
               else
               {
                  _streamProvider = _currentProvider.name == "tcdn"?new TcdnProvider():new CdnProvider();
               }
               _streamProvider.addEventListener("destroy",onProviderDestroy);
               _loc5_ = true;
            }
            _loc4_ = ["url"];
            _loc8_ = [];
            _loc11_ = 0;
            _loc10_ = describeType(_streamProvider).descendants("accessor");
            for each(_loc6_ in describeType(_streamProvider).descendants("accessor"))
            {
               if(_loc6_.@access == "readwrite" && !(_loc6_.@declaredBy.indexOf("stream::") == -1))
               {
                  _loc8_.push(
                     {
                        "name":_loc6_.@name,
                        "priority":_loc4_.indexOf(_loc6_.@name)
                     });
               }
            }
            _loc8_.sortOn("priority");
            _loc7_ = 0;
            while(_loc7_ < _loc8_.length)
            {
               _streamProvider[_loc8_[_loc7_].name] = _currentProvider[_loc8_[_loc7_].name];
               _loc7_++;
            }
            if(_loc5_)
            {
               dispatch("providerCreated");
            }
         }
         else
         {
            echo("no currentProvider");
         }
      }
      
      private function onProviderDestroy(param1:Event) : void {
         var _loc2_:* = null;
         if(param1.target is Provider)
         {
            _loc2_ = param1.target as Provider;
            _loc2_.removeEventListener("destroy",onProviderDestroy);
         }
         _streamProvider = null;
         dispatch("providerDestroyed");
      }
      
      private function updateQualityModule(param1:Array = null) : void {
         var _loc3_:* = 0;
         var _loc6_:* = null;
         var _loc4_:* = null;
         var _loc7_:* = null;
         var _loc2_:Object = {};
         if(param1)
         {
            _loc3_ = 0;
            while(_loc3_ < param1.length)
            {
               if(param1[_loc3_].bitrate)
               {
                  param1[_loc3_].bitrate = param1[_loc3_].bitrate;
               }
               _loc3_++;
            }
            _loc2_.quality = [];
            _loc3_ = 0;
            while(_loc3_ < param1.length)
            {
               _loc6_ = {};
               if(param1[_loc3_].bitrate)
               {
                  _loc6_.bitrate = param1[_loc3_].bitrate;
               }
               if(param1[_loc3_].description)
               {
                  _loc6_.description = param1[_loc3_].description;
               }
               if(param1[_loc3_].isTranscoded)
               {
                  _loc6_.isTranscoded = param1[_loc3_].isTranscoded;
               }
               if(param1[_loc3_].width)
               {
                  _loc6_.width = param1[_loc3_].width;
               }
               if(param1[_loc3_].height)
               {
                  _loc6_.height = param1[_loc3_].height;
               }
               if(param1[_loc3_].size)
               {
                  _loc4_ = param1[_loc3_].size as String;
                  if(_loc4_)
                  {
                     _loc7_ = _loc4_.toLowerCase().split("x");
                     if(_loc7_ && _loc7_.length > 1)
                     {
                        _loc6_.width = !isNaN(parseInt(_loc7_[0]))?parseInt(_loc7_[0]):0.0;
                        _loc6_.height = !isNaN(parseInt(_loc7_[1]))?parseInt(_loc7_[1]):0.0;
                     }
                     else
                     {
                        echo("streamSize was given in wrong format ([width:int]x[height:int])",2);
                     }
                  }
                  else
                  {
                     echo("streamSize is not a String",2);
                  }
               }
               _loc2_.quality.push(_loc6_);
               _loc3_++;
            }
            if(!_loc2_.quality.length)
            {
               _loc2_.quality = false;
            }
         }
         else
         {
            _loc2_.quality = false;
         }
         var _loc5_:Quality = (manager as ViewerModuleManager).quality as Quality;
         (logic as EventDispatcher).dispatchEvent(new DynamicEvent("moduleInfo",false,false,
            {
               "info":_loc2_,
               "source":"streamModule"
            }));
         if(_loc5_ && _loc2_.quality)
         {
            selectStream(_loc5_.availableQualities.indexOf(_loc5_.desiredQuality));
         }
      }
      
      public function get online() : Boolean {
         return _status == 1;
      }
      
      public function get hasLiveContent() : Boolean {
         return _data is Array && (_data as Array).length;
      }
      
      public function get logicUrl() : String {
         return _logicUrl;
      }
      
      public function get streamProvider() : Provider {
         return _streamProvider;
      }
      
      public function get providerList() : Array {
         return _providerList;
      }
      
      override public function get data() : * {
         return null;
      }
      
      private function switchProvider(param1:Boolean = false, param2:String = null) : void {
         var _loc3_:* = false;
         var _loc4_:* = 0;
         echo("switchEnabled : " + switchEnabled);
         echo("ignoreFlag : " + param1);
         if(switchEnabled || (param1))
         {
            _loc3_ = false;
            if(param2)
            {
               _loc4_ = 0;
               while(_loc4_ < _providerList.length)
               {
                  if(_providerList[_loc4_].name == param2)
                  {
                     providerIndex = _loc4_;
                     _loc3_ = true;
                     break;
                  }
                  _loc4_++;
               }
            }
            if(!param2 || param2 && !_loc3_)
            {
               providerIndex = providerIndex + 1;
               if(providerIndex >= _providerList.length)
               {
                  providerIndex = 0;
                  if(!param1)
                  {
                     echo("ran out of providers, disabling switch for 600 seconds");
                     switchEnabled = false;
                     switchTimer.reset();
                     switchTimer.start();
                  }
               }
            }
            echo("providerIndex : " + providerIndex);
            _currentProvider = _providerList[providerIndex];
            parseStreams();
         }
      }
      
      private function onProviderSwitch(param1:Event) : void {
         var _loc2_:String = null;
         if(param1 is DynamicEvent)
         {
            _loc2_ = (param1 as DynamicEvent).data.preferred;
         }
         switchProvider(false,_loc2_);
      }
      
      private function onProviderFailed(param1:Event) : void {
         var _loc2_:String = null;
         if(param1 is DynamicEvent)
         {
            _loc2_ = (param1 as DynamicEvent).data.preferred;
         }
         switchProvider(true,_loc2_);
      }
      
      private function onSwitchTimer(param1:TimerEvent) : void {
         echo("provider switch enabled");
         switchEnabled = true;
      }
      
      override public function destroy(... rest) : * {
         if(_streamProvider)
         {
            _streamProvider.destroy();
            _streamProvider = null;
         }
         if(logic is Recorded)
         {
            (logic as Recorded).removeEventListener("providerFailed",onProviderFailed);
            (logic as Recorded).removeEventListener("providerSwitch",onProviderSwitch);
         }
         (manager as ViewerModuleManager).removeEventListener("createQuality",onCreateQuality);
         (manager as ViewerModuleManager).removeEventListener("createTcdn",onCreateTcdn);
         var _loc2_:Quality = (manager as ViewerModuleManager).quality as Quality;
         if(_loc2_)
         {
            (manager as ViewerModuleManager).removeEventListener("destroyQuality",onDestroyQuality);
            (logic as EventDispatcher).dispatchEvent(new DynamicEvent("moduleInfo",false,false,
               {
                  "info":{"quality":false},
                  "source":"streamModule"
               }));
         }
         return super.destroy();
      }
      
      private function onDestroyQuality(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_ && _loc2_.quality)
         {
            _loc2_.quality.removeEventListener("change",onQualityChange);
            _loc2_.removeEventListener("destroyQuality",onDestroyQuality);
         }
         selectStream(0);
      }
      
      private function onCreateQuality(param1:Event) : void {
         var _loc4_:* = 0;
         var _loc2_:* = 0;
         var _loc3_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc3_ && _loc3_.quality)
         {
            if(This.getReference(Logic.stage,"loaderInfo.parameters.quality") && !isNaN(This.getReference(Logic.stage,"loaderInfo.parameters.quality")))
            {
               _loc4_ = This.getReference(Logic.stage,"loaderInfo.parameters.quality");
               _loc2_ = Math.min(_loc3_.quality.availableQualities.length - 1,Math.max(0,_loc3_.quality.availableQualities.length - 1 - _loc4_));
               _loc3_.quality.desiredQuality = _loc3_.quality.availableQualities[_loc2_];
               echo("flashvars param: " + _loc4_ + ", module index: " + _loc2_);
               echo("desiredQuality: " + _loc3_.quality.desiredQuality);
            }
            _loc3_.addEventListener("destroyQuality",onDestroyQuality);
            _loc3_.quality.addEventListener("change",onQualityChange);
            selectStream(_loc3_.quality.availableQualities.indexOf(_loc3_.quality.desiredQuality));
         }
      }
      
      private function onQualityChange(param1:Event) : void {
         var _loc2_:Quality = param1.target as Quality;
         selectStream(_loc2_.availableQualities.indexOf(_loc2_.desiredQuality));
      }
      
      private function selectStream(param1:int) : void {
         var _loc5_:* = 0;
         var _loc3_:* = null;
         var _loc2_:* = 0;
         var _loc4_:* = 0;
         echo("selectStream");
         if(param1 < 0)
         {
            echo("selectStream index mismatch : " + param1);
            param1 = 0;
         }
         _loc5_ = 0;
         while(_loc5_ < _providerList.length)
         {
            _loc3_ = ["streamName","streamNameService","chunkId","offset","offsetInMs","chunkTime","chunkRange"];
            if(_providerList[_loc5_].streams is Array && (_providerList[_loc5_].streams as Array).length)
            {
               delete _providerList[_loc5_].isTranscoded;
               _loc4_ = 0;
               while(_loc4_ < _providerList[_loc5_].streams.length)
               {
                  if(_providerList[_loc5_].streams[_loc4_].isTranscoded)
                  {
                     _providerList[_loc5_].isTranscoded = true;
                     break;
                  }
                  _loc4_++;
               }
               if((_providerList[_loc5_].streams as Array).length > param1)
               {
                  _loc2_ = 0;
                  while(_loc2_ < _loc3_.length)
                  {
                     if(_providerList[_loc5_].streams[param1][_loc3_[_loc2_]])
                     {
                        _providerList[_loc5_][_loc3_[_loc2_]] = _providerList[_loc5_].streams[param1][_loc3_[_loc2_]];
                     }
                     else
                     {
                        delete _providerList[_loc5_][_loc3_[_loc2_]];
                     }
                     _loc2_++;
                  }
                  echo("" + _providerList[_loc5_].streamName + " " + (_providerList[_loc5_].streamName as String).indexOf("http://"));
                  if(!(providerList[_loc5_].streamNameService is String) || (providerList[_loc5_].streamNameService is String && !((providerList[_loc5_].streamNameService as String).indexOf("http://") == 0)))
                  {
                     delete providerList[_loc5_].streamNameService;
                  }
                  if(logic.type == "recorded" && !((_providerList[_loc5_].streamName as String).indexOf("http://") == -1))
                  {
                     _providerList[_loc5_].type = "progressive";
                  }
                  echo("" + _providerList[_loc5_].name + " " + _loc5_ + ": " + _providerList[_loc5_].streams[param1].streamName);
                  echo(_providerList[_loc5_].name + " streamName: " + _providerList[_loc5_].streamName);
               }
               else
               {
                  echo(_providerList[_loc5_].name + " quality index is out of bounds ( index : " + param1 + ", count: " + (_providerList[_loc5_].streams as Array).length + " )");
               }
            }
            else
            {
               _loc2_ = 0;
               while(_loc2_ < _loc3_.length)
               {
                  delete _providerList[_loc5_][_loc3_[_loc2_]];
                  _loc2_++;
               }
            }
            _loc5_++;
         }
         if(_currentProvider && _currentProvider.type == "uhs")
         {
            if(!uhsSafeTimer)
            {
               echo("creating UHS safe timer");
               uhsSafeTimer = new Timer(60000,1);
               uhsSafeTimer.addEventListener("timerComplete",onUhsTimerComplete);
            }
            uhsSafeTimer.reset();
            uhsSafeTimer.start();
         }
         else if(uhsSafeTimer)
         {
            uhsSafeTimer.reset();
            uhsSafeTimer.removeEventListener("timerComplete",onUhsTimerComplete);
            uhsSafeTimer = null;
         }
         
         updateProvider();
      }
      
      private function onUhsTimerComplete(param1:TimerEvent) : void {
         if(logic is Recorded && (logic as Recorded).ums)
         {
            (logic as Recorded).ums.dispatchEvent(new Event("reconnect"));
         }
      }
      
      private function onCreateTcdn(param1:Event) : void {
         var _loc2_:Tcdn = (param1.target as ViewerModuleManager).tcdn;
         _loc2_.addEventListener("update",onTcdnUpdate);
         _loc2_.addEventListener("destroy",onTcdnDestroy);
      }
      
      private function onTcdnUpdate(param1:Event) : void {
         var _loc3_:* = 0;
         var _loc2_:Tcdn = param1.target as Tcdn;
         if(_loc2_.cdnStatus != 0)
         {
            if(_currentProvider)
            {
               echo("_currentProvider.name : " + _currentProvider.name);
               if(_currentProvider.name == "tcdn" && _loc2_.cdnStatus == -1)
               {
                  echo("leaving tcdn");
                  switchProvider(true);
               }
               else if(!(_currentProvider.name == "tcdn") && _loc2_.cdnStatus == 1)
               {
                  echo("switching to tcdn");
                  _loc3_ = 0;
                  while(_loc3_ < _providerList.length)
                  {
                     if(_providerList[_loc3_].name == "tcdn")
                     {
                        providerIndex = _loc3_ - 1;
                        if(providerIndex < 0)
                        {
                           providerIndex = _providerList.length - 1;
                        }
                        switchProvider(true);
                        break;
                     }
                     _loc3_++;
                  }
               }
               
            }
            else
            {
               echo("no streamProvider, skipping");
            }
         }
         else
         {
            echo("tcnd update, ignored");
         }
      }
      
      private function onTcdnDestroy(param1:Event) : void {
         var _loc2_:Tcdn = param1.target as Tcdn;
         _loc2_.removeEventListener("update",onTcdnUpdate);
         _loc2_.removeEventListener("destroy",onTcdnDestroy);
      }
      
      private function setStatus(param1:int) : void {
         var _loc2_:int = _status;
         _status = param1;
         if(_status != _loc2_)
         {
            dispatch("statusChange");
         }
      }
      
      public function get source() : String {
         return infoSource;
      }
      
      public function get status() : int {
         return _status;
      }
   }
}
