package tv.ustream.viewer.logic.modules
{
   import tv.ustream.modules.Module;
   import tv.ustream.tools.Debug;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.This;
   import tv.ustream.modules.ModuleManager;
   
   public class Logicloadtest extends Module
   {
      
      public function Logicloadtest(param1:* = null, param2:* = null, param3:ModuleManager = null) {
         super(param1,param2,param3);
      }
      
      private var connectionList:Array;
      
      override protected function info(param1:*) : void {
         var _loc4_:* = null;
         var _loc2_:* = null;
         var _loc8_:* = 0;
         var _loc3_:* = null;
         var _loc5_:* = 0;
         var _loc6_:* = 0;
         var _loc7_:* = 0;
         var _loc9_:* = 0;
         echo("info()");
         Debug.explore(param1);
         super.info(param1);
         cleanupConnections();
         if(_data.topMedias && _data.connections)
         {
            _loc4_ = {};
            if(_data.port)
            {
               _loc4_.port = _data.port;
            }
            if(_data.ipList)
            {
               _loc4_.ipList = _data.ipList;
            }
            _loc2_ = {};
            _loc2_.media = "";
            _loc2_.application = _data.application;
            _loc2_.rsid = (Logic.hasInstance?Logic.instance.randomId:"null") + ":";
            if(This.referrer)
            {
               _loc2_.referrer = This.referrer;
            }
            if(logic.sessionId)
            {
               _loc2_.session = logic.sessionId;
            }
            if(logic.hasOwnProperty("userId") && logic.userId)
            {
               _loc2_.userId = logic.userId;
            }
            if(logic.hasOwnProperty("rpin") && logic.rpin)
            {
               _loc2_.rpin = logic.rpin;
            }
            _loc8_ = 0;
            _loc5_ = _data.randomConnectDelay || 0;
            _loc6_ = 0;
            while(_loc6_ < _data.topMedias.length)
            {
               _loc7_ = 0;
               while(_loc7_ < Math.round(_data.connections * _data.topMedias[_loc6_].percent / 100))
               {
                  _loc2_.counter = _loc8_++;
                  _loc2_.media = _data.topMedias[_loc6_].media;
                  _loc3_ = new TestConnection(_loc2_,_loc4_);
                  _loc3_.connect(_loc5_);
                  connectionList.push(_loc3_);
                  _loc7_++;
               }
               _loc6_++;
            }
            echo(" >> " + [_loc8_,param1.connections]);
            if(_loc8_ < param1.connections && _data.mediaRange && _data.mediaRange.length == 2)
            {
               _loc2_.media = Math.round(Math.random() * (_data.mediaRange[1] - _data.mediaRange[0])) + _data.mediaRange[0];
               echo("pick random media >> " + [_loc2_.media,_data.mediaRange[0],_data.mediaRange[1]]);
               _loc9_ = param1.connections - _loc8_;
               _loc6_ = 0;
               while(_loc6_ < _loc9_)
               {
                  _loc2_.counter = _loc8_++;
                  _loc3_ = new TestConnection(_loc2_,_loc4_);
                  _loc3_.connect(_loc5_);
                  connectionList.push(_loc3_);
                  _loc6_++;
               }
            }
         }
      }
      
      private function cleanupConnections() : void {
         var _loc1_:* = null;
         if(connectionList && connectionList.length)
         {
            while(connectionList.length)
            {
               _loc1_ = connectionList.shift();
               _loc1_.destroy();
            }
         }
         connectionList = [];
      }
      
      override public function destroy(... rest) : * {
         cleanupConnections();
         return super.destroy();
      }
      
      public function traceConnections() : void {
         var _loc1_:* = 0;
         if(connectionList)
         {
            _loc1_ = 0;
            while(_loc1_ < connectionList.length)
            {
               echo("traceConnections - " + _loc1_ + " - " + (connectionList[_loc1_] as TestConnection).info);
               _loc1_++;
            }
         }
      }
      
      public function set playing(param1:Boolean) : void {
         var _loc2_:* = 0;
         echo("set playing: " + param1);
         if(connectionList)
         {
            _loc2_ = 0;
            while(_loc2_ < connectionList.length)
            {
               (connectionList[_loc2_] as TestConnection).playing(logic.id,param1);
               _loc2_++;
            }
         }
      }
      
      public function set status(param1:String) : void {
         var _loc2_:* = 0;
         echo("set status: " + param1);
         if(connectionList)
         {
            _loc2_ = 0;
            while(_loc2_ < connectionList.length)
            {
               (connectionList[_loc2_] as TestConnection).status(logic.id,param1);
               _loc2_++;
            }
         }
      }
      
      public function loose(param1:String, ... rest) : void {
         var _loc3_:* = 0;
         echo("call " + param1 + ": " + rest);
         rest.unshift(param1);
         if(connectionList)
         {
            _loc3_ = 0;
            while(_loc3_ < connectionList.length)
            {
               (connectionList[_loc3_] as TestConnection).call.apply(this,rest);
               _loc3_++;
            }
         }
      }
   }
}
import tv.ustream.net.MultiConnection;
import flash.utils.Timer;
import tv.ustream.tools.Debug;
import flash.events.Event;
import tv.ustream.viewer.logic.Logic;
import flash.events.TimerEvent;
import tv.ustream.tools.StringUtils;

class TestConnection extends Object
{
   
   function TestConnection(param1:Object, param2:Object) {
      args = {};
      super();
      var _loc6_:* = 0;
      var _loc5_:* = param1;
      for(_loc3_ in param1)
      {
         this.args[_loc3_] = param1[_loc3_];
      }
      this.args.rsid = this.args.rsid + StringUtils.random(8);
      this.data = param2;
      reconnectTimer = new Timer(1000,1);
      reconnectTimer.addEventListener("timer",connect);
      var _loc4_:Array = param2.ipList.concat();
      ipList = [];
      while(_loc4_.length)
      {
         ipList.push(_loc4_.splice(Math.random() * _loc4_.length,1)[0]);
      }
      connection = new MultiConnection(null,"UMS",param2.port is Array?param2.port:[param2.port]);
      connection.client = 
         {
            "moduleInfo":onUmsModuleInfo,
            "goOffline":onUmsGoOffline,
            "reject":onUmsReject
         };
      connection.addEventListener("connected",onConnected);
      connection.addEventListener("disconnected",onDisconnected);
      connection.addEventListener("failed",onFailed);
      echo("creation... ipList: " + ipList + " - port: " + param2.port);
   }
   
   private var connection:MultiConnection;
   
   private var args:Object;
   
   private var data:Object;
   
   private var ipList:Array;
   
   private var reconnectTimer:Timer;
   
   private var connectTimer:Timer;
   
   private var ip:String;
   
   private function onUmsReject(param1:Object) : void {
      echo("onUmsReject()");
      Debug.explore(param1);
      destroy();
   }
   
   private function onUmsGoOffline(... rest) : void {
      echo("onUmsGoOffline()");
   }
   
   private function onUmsModuleInfo(param1:Object) : void {
      echo("onUmsModuleInfo()");
      Debug.explore(param1);
   }
   
   private function onFailed(param1:Event) : void {
      echo("onFailed()");
   }
   
   private function onDisconnected(param1:Event) : void {
      echo("onDisconnected()");
   }
   
   private function onConnected(param1:Event) : void {
      echo("onConnected()");
      if(Logic.hasInstance)
      {
         playing(args.media,Logic.instance.media.playing);
         status(args.media,Logic.instance.media.ums.status);
      }
   }
   
   public function connect(param1:uint = 0) : void {
      ip = data.ipList[Math.floor(data.ipList.length * Math.random())];
      echo("connect() - randomConnectDelay: " + param1);
      if(param1 > 0)
      {
         connectTimer = new Timer(Math.round(param1 * Math.random() * 1000),1);
         connectTimer.addEventListener("timer",onConnectTimer);
         connectTimer.start();
         echo("connect() - smooth: " + connectTimer.delay);
      }
      else
      {
         onConnectTimer();
      }
   }
   
   private function onConnectTimer(param1:TimerEvent = null) : void {
      echo("onConnectTimer() - ip: " + ip + " - port: " + data.port);
      Debug.explore(args);
      if(ip && args)
      {
         connection.connect(ip,args);
      }
      cleanUpConnectTimer();
      if(reconnectTimer && reconnectTimer.running)
      {
         reconnectTimer.stop();
      }
   }
   
   public function playing(param1:String, param2:Boolean) : void {
      if(connection)
      {
         echo("call playing: " + [param1,param2]);
         connection.call("playing",null,param1,param2);
      }
   }
   
   public function status(param1:String, param2:String) : void {
      if(connection && param2)
      {
         echo("call status: " + [param1,param2]);
         connection.call("status",null,param1,param2);
      }
   }
   
   public function call(param1:String, ... rest) : void {
      if(connection)
      {
         echo("call " + param1 + ": " + rest);
         rest.unshift(param1);
         connection.call.apply(this,rest);
      }
   }
   
   private function reconnect() : void {
   }
   
   private function cleanUpConnectTimer() : void {
      if(connectTimer)
      {
         connectTimer.stop();
         connectTimer.removeEventListener("timer",onConnectTimer);
         connectTimer = null;
      }
   }
   
   public function destroy() : void {
      cleanUpConnectTimer();
      if(reconnectTimer && reconnectTimer.running)
      {
         reconnectTimer.stop();
         reconnectTimer.removeEventListener("timer",connect);
         reconnectTimer = null;
      }
      if(connection)
      {
         connection.removeEventListener("connected",onConnected);
         connection.removeEventListener("disconnected",onDisconnected);
         connection.removeEventListener("failed",onFailed);
         connection.kill();
         connection = null;
      }
   }
   
   public function get info() : String {
      return "connected: " + (connection.connected?ip + ":" + data.port + " - media: " + args.media:ip + ":" + data.port);
   }
   
   private function echo(param1:String) : void {
      Debug.echo("[ TESTCONNECTION." + args.counter + "] " + param1);
   }
}
