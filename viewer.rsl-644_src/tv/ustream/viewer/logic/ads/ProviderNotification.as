package tv.ustream.viewer.logic.ads
{
   import flash.display.Sprite;
   import flash.display.Shape;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.display.Loader;
   import flash.net.URLRequest;
   import flash.system.LoaderContext;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   import flash.system.SecurityDomain;
   import flash.filters.DropShadowFilter;
   import flash.geom.Matrix;
   import flash.events.MouseEvent;
   import flash.net.navigateToURL;
   import flash.events.Event;
   import flash.display.Bitmap;
   import flash.events.HTTPStatusEvent;
   import flash.events.TimerEvent;
   
   public class ProviderNotification extends ProviderBase
   {
      
      public function ProviderNotification(param1:Ads, param2:Object) {
         super();
         adType = "notification";
         this.ads = param1;
         this.data = param2;
         if(param1.adHolder)
         {
            width = param1.adHolder.width;
            height = param1.adHolder.height;
         }
         createUI();
      }
      
      private const HEIGHT:Number = 62;
      
      private const MIN_WIDTH:Number = 350;
      
      private const MAX_WIDTH:Number = 470;
      
      private const THUMB_HEIGHT:Number = 44;
      
      private var container:Sprite;
      
      private var holder:Sprite;
      
      private var bg:Shape;
      
      private var img:Sprite;
      
      private var textField:TextField;
      
      private var textField2:TextField;
      
      private var textFieldHolder:Sprite;
      
      private var closeButton:Sprite;
      
      private var openButton:Sprite;
      
      private var textFormat:TextFormat;
      
      private var thumbLoader:Loader;
      
      private var _scrollText:Boolean = false;
      
      private var mask:Shape;
      
      private function createUI() : void {
         var _loc7_:* = null;
         var _loc3_:* = null;
         container = new Sprite();
         container.visible = false;
         textFormat = new TextFormat("Arial",12,0);
         textFormat.leading = 1;
         holder = new Sprite();
         holder.buttonMode = true;
         container.addChild(holder);
         bg = new Shape();
         bg.x = 1;
         bg.y = 1;
         bg.alpha = 0.85;
         holder.addChild(bg);
         img = new Sprite();
         img.x = 9;
         img.y = 9;
         holder.addChild(img);
         if(data.thumb)
         {
            thumbLoader = new Loader();
            thumbLoader.contentLoaderInfo.addEventListener("complete",onThumbLoaded);
            thumbLoader.contentLoaderInfo.addEventListener("ioError",onLoaderError);
            thumbLoader.contentLoaderInfo.addEventListener("httpStatus",onHttpStatus);
            _loc7_ = new URLRequest(data.thumb);
            _loc3_ = new LoaderContext(false,ApplicationDomain.currentDomain);
            if(Security.sandboxType != "localTrusted")
            {
               _loc3_.securityDomain = SecurityDomain.currentDomain;
            }
            thumbLoader.load(_loc7_,_loc3_);
         }
         closeButton = new Sprite();
         closeButton.mouseChildren = false;
         closeButton.buttonMode = true;
         closeButton.graphics.beginFill(16737792,0);
         closeButton.graphics.drawRect(0,0,26,26);
         closeButton.graphics.endFill();
         var _loc5_:Shape = new Shape();
         _loc5_.graphics.beginFill(6579300,1);
         _loc5_.graphics.drawCircle(0,0,8);
         _loc5_.graphics.endFill();
         _loc5_.x = 13;
         _loc5_.y = 13;
         _loc5_.alpha = 0.33;
         _loc5_.filters = [new DropShadowFilter(1,90,0,0.8,1,1,1,2,true),new DropShadowFilter(1,90,16777215,1,2,2,1,2)];
         closeButton.addChild(_loc5_);
         var _loc4_:Shape = new Shape();
         _loc4_.graphics.lineStyle(2,16777215,1);
         _loc4_.graphics.moveTo(-2.5,-2.5);
         _loc4_.graphics.lineTo(2.5,2.5);
         _loc4_.graphics.moveTo(2.5,-2.5);
         _loc4_.graphics.lineTo(-2.5,2.5);
         _loc4_.graphics.endFill();
         _loc4_.x = 13;
         _loc4_.y = 13;
         _loc4_.filters = [new DropShadowFilter(1,90,0,0.18,1,1,1,2)];
         closeButton.addChild(_loc4_);
         closeButton.addEventListener("click",onCloseButtonClick);
         holder.addChild(closeButton);
         textFieldHolder = new Sprite();
         textFieldHolder.cacheAsBitmap = true;
         holder.addChild(textFieldHolder);
         mask = new Shape();
         mask.cacheAsBitmap = true;
         mask.graphics.beginFill(0);
         mask.graphics.drawRect(10,2,10,62 - 4);
         mask.graphics.endFill();
         textFieldHolder.mask = mask;
         holder.addChild(mask);
         textField = new TextField();
         textField.defaultTextFormat = textFormat;
         textField.cacheAsBitmap = true;
         textField.autoSize = "left";
         textField.multiline = true;
         textField.wordWrap = true;
         textField.selectable = false;
         textField.mouseEnabled = false;
         textField.filters = [new DropShadowFilter(1,90,16777215,0.8,1,1,1,2)];
         textFieldHolder.addChild(textField);
         openButton = new Sprite();
         openButton.mouseChildren = false;
         openButton.buttonMode = true;
         openButton.graphics.beginFill(0,0.11);
         openButton.graphics.drawRoundRect(0,0,28,13,6);
         openButton.graphics.endFill();
         var _loc6_:Shape = new Shape();
         var _loc1_:Matrix = new Matrix();
         _loc1_.createGradientBox(26,11,1.5707963267948966);
         _loc6_.graphics.beginGradientFill("linear",[16777215,14737632],[1,1],[0,255],_loc1_);
         _loc6_.graphics.drawRoundRect(0,0,26,11,4);
         _loc6_.graphics.endFill();
         _loc6_.x = 1;
         _loc6_.y = 1;
         _loc6_.alpha = 0.5;
         openButton.addChild(_loc6_);
         var _loc2_:Shape = new Shape();
         _loc2_.graphics.lineStyle(2,0,1);
         _loc2_.graphics.moveTo(11,5);
         _loc2_.graphics.lineTo(14,8);
         _loc2_.graphics.lineTo(17,5);
         _loc2_.graphics.endFill();
         _loc2_.alpha = 0.5;
         openButton.addChild(_loc2_);
         openButton.addEventListener("mouseOver",onOpenButtonOver);
         openButton.addEventListener("mouseOut",onOpenButtonOut);
         openButton.addEventListener("click",onOpenButtonClick);
         openButton.visible = false;
         container.addChild(openButton);
         if(data.message.length > 120)
         {
            data.message = data.message.substr(0,120);
         }
         textField.text = data.message;
         holder.addEventListener("mouseOver",onMouseOver);
         holder.addEventListener("mouseOut",onMouseOut);
         holder.addEventListener("click",onMouseClick);
         holder.name = "holder";
         closeButton.name = "close";
      }
      
      private function onMouseClick(param1:MouseEvent) : void {
         var _loc2_:* = false;
         if(data.mediaId && !(param1.target.name == "close"))
         {
            _loc2_ = true;
            echo("onMouseClick() - mediaId: " + data.mediaId);
            try
            {
               navigateToURL(new URLRequest("http://www.ustream.tv/channel/" + data.mediaId),"_blank");
            }
            catch(e:Error)
            {
               echo("ERROR: " + e.message);
               _loc2_ = false;
            }
         }
      }
      
      private function onThumbLoaded(param1:Event) : void {
         if(thumbLoader.content is Bitmap)
         {
            Bitmap(thumbLoader.content).smoothing = true;
         }
         thumbLoader.height = 44;
         thumbLoader.scaleX = thumbLoader.scaleY;
         thumbLoader.width = Math.round(thumbLoader.width);
         img.addChild(thumbLoader);
         render(ads.adHolder.width - 10);
      }
      
      private function onOpenButtonClick(param1:MouseEvent) : void {
         openButton.visible = false;
         holder.visible = true;
      }
      
      private function onCloseButtonClick(param1:MouseEvent) : void {
         holder.visible = false;
         openButton.visible = true;
      }
      
      private function onHttpStatus(param1:HTTPStatusEvent) : void {
         echo("onHttpStatus() - " + param1.toString());
      }
      
      private function onLoaderError(param1:Event) : void {
         echo("onLoaderError() - " + param1.toString());
      }
      
      private function render(param1:Number = 470) : void {
         var _loc4_:Number = Math.max(350,Math.min(param1,470));
         var _loc3_:* = 0.0;
         var _loc5_:* = 10.0;
         echo("render() - _width: " + _loc4_);
         if(holder.contains(img) && thumbLoader)
         {
            _loc3_ = thumbLoader.width;
            _loc5_ = _loc5_ + (9 + thumbLoader.width);
         }
         holder.graphics.clear();
         holder.graphics.beginFill(0,0.11);
         holder.graphics.drawRoundRect(0,0,_loc4_,62,10);
         holder.graphics.endFill();
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(_loc4_ - 2,62 - 2,1.5707963267948966,0,0);
         bg.graphics.clear();
         bg.graphics.beginGradientFill("linear",[16777215,14737632],[1,1],[0,255],_loc2_);
         bg.graphics.drawRoundRect(0,0,_loc4_ - 2,62 - 2,8);
         bg.graphics.endFill();
         img.graphics.clear();
         img.graphics.beginFill(0,0.08);
         img.graphics.drawRect(-1,-1,_loc3_ + 2,44 + 2);
         img.graphics.endFill();
         closeButton.x = _loc4_ - 26 - 1;
         closeButton.y = 1;
         textField.x = _loc5_;
         textField.width = closeButton.x - _loc5_;
         textField.y = img.y + Math.round((44 - textField.height) / 2);
         if(textField2)
         {
            textField2.width = closeButton.x - _loc5_;
            textField2.y = textField.y + textField.height;
            textField2.x = textField.x;
         }
         mask.graphics.clear();
         mask.graphics.beginFill(0);
         mask.graphics.drawRect(textField.x,2,closeButton.x - _loc5_,62 - 4);
         mask.graphics.endFill();
         if(textField.numLines > 3)
         {
            if(!scrollText)
            {
               scrollText = true;
            }
         }
      }
      
      private function get scrollText() : Boolean {
         return _scrollText;
      }
      
      private function set scrollText(param1:Boolean) : void {
         if(_scrollText != param1)
         {
            _scrollText = param1;
            if(_scrollText)
            {
               if(!textField2)
               {
                  textField2 = new TextField();
                  textField2.defaultTextFormat = textFormat;
                  textField2.cacheAsBitmap = true;
                  textField2.autoSize = "left";
                  textField2.multiline = true;
                  textField2.wordWrap = true;
                  textField2.selectable = false;
                  textField2.mouseEnabled = false;
                  textField2.filters = [new DropShadowFilter(1,90,16777215,0.8,1,1,1,2)];
                  textField2.y = textField.y + textField.height;
                  textField2.y = textField.x;
                  textField2.text = textField.text;
                  textFieldHolder.addChild(textField2);
               }
               render(ads.adHolder.width - 10);
               textField2.visible = true;
               holder.addEventListener("enterFrame",onScroll);
            }
            else
            {
               if(textField2 && holder.contains(textField2))
               {
                  textField2.visible = false;
                  render(ads.adHolder.width - 10);
               }
               holder.removeEventListener("enterFrame",onScroll);
            }
         }
      }
      
      private function onScroll(param1:Event) : void {
         textField.y = textField.y - 0.1;
         textField2.y = textField2.y - 0.1;
         if(textField.y + textField.height < 0)
         {
            textField.y = textField2.y + textField2.height;
         }
         if(textField2.y + textField2.height < 0)
         {
            textField2.y = textField.y + textField.height;
         }
      }
      
      private function onMouseOver(param1:Event) : void {
         bg.alpha = 1;
         if(param1.target == closeButton)
         {
            closeButton.getChildAt(0).alpha = 1;
         }
      }
      
      private function onMouseOut(param1:Event) : void {
         bg.alpha = 0.85;
         if(param1.target == closeButton)
         {
            closeButton.getChildAt(0).alpha = 0.33;
         }
      }
      
      private function onOpenButtonOver(param1:Event) : void {
         openButton.getChildAt(0).alpha = 1;
         openButton.getChildAt(1).alpha = 0.8;
      }
      
      private function onOpenButtonOut(param1:Event) : void {
         openButton.getChildAt(0).alpha = 0.5;
         openButton.getChildAt(1).alpha = 0.5;
      }
      
      override public function requestAd(param1:uint) : void {
         echo("requestAd()");
         ads.adHolder.addChild(container);
         render(ads.adHolder.width - 10);
         setPlayerSize(ads.adHolder.width,ads.adHolder.height);
         container.visible = ads.adHolder.width >= 350;
         if(data.duration && (data.duration) > 0 && adTimer == null)
         {
            startCountdown(data.duration);
         }
      }
      
      override public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
         var _loc4_:* = 0;
         echo("setPlayerSize() " + [param1,350]);
         if(param1 < 350)
         {
            if(container.visible)
            {
               container.visible = false;
            }
         }
         else
         {
            render(param1 - 10);
            _loc4_ = Math.round((param1 - holder.width) / 2);
            if(_loc4_ < 0)
            {
               _loc4_ = 0;
            }
            holder.x = _loc4_;
            holder.y = 5;
            openButton.x = Math.round((param1 - openButton.width) / 2);
            openButton.y = 5;
            if(!container.visible)
            {
               container.visible = true;
            }
         }
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         echo("onTimer()");
         if(!enabled)
         {
            return;
         }
         super.onTimer(param1);
         ads.onAdComplete();
      }
      
      override public function destroyAd() : void {
         echo("destroyAd");
         enabled = false;
         adStatus = "completed";
         if(ads.adHolder.contains(container))
         {
            ads.adHolder.removeChild(container);
         }
         if(holder.hasEventListener("enterFrame"))
         {
            holder.removeEventListener("enterFrame",onScroll);
         }
         holder.removeChild(bg);
         bg = null;
         holder.removeChild(img);
         img = null;
         holder.removeChild(closeButton);
         closeButton.removeEventListener("click",onCloseButtonClick);
         closeButton = null;
         textFieldHolder.removeChild(textField);
         if(textField2 && textFieldHolder.contains(textField2))
         {
            textFieldHolder.removeChild(textField2);
            textField2 = null;
         }
         textField = null;
         textFieldHolder.mask = null;
         holder.removeChild(mask);
         mask = null;
         holder.removeChild(textFieldHolder);
         textFieldHolder = null;
         container.removeChild(openButton);
         openButton.removeEventListener("mouseOver",onOpenButtonOver);
         openButton.removeEventListener("mouseOut",onOpenButtonOut);
         openButton.removeEventListener("click",onOpenButtonClick);
         openButton = null;
         thumbLoader.contentLoaderInfo.removeEventListener("complete",onThumbLoaded);
         thumbLoader.contentLoaderInfo.removeEventListener("ioError",onLoaderError);
         thumbLoader.contentLoaderInfo.removeEventListener("httpStatus",onHttpStatus);
         thumbLoader = null;
         container.addChild(holder);
         container = null;
         textFormat = null;
         holder.removeEventListener("mouseOver",onMouseOver);
         holder.removeEventListener("mouseOut",onMouseOut);
         holder = null;
      }
   }
}
