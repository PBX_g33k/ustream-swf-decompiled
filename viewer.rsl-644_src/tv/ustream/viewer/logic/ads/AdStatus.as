package tv.ustream.viewer.logic.ads
{
   public class AdStatus extends Object
   {
      
      public function AdStatus() {
         super();
      }
      
      public static const CREATED:uint = 0;
      
      public static const INITED:uint = 1;
      
      public static const REQUESTED:uint = 2;
      
      public static const RESPONSED:uint = 3;
      
      public static const STARTED:uint = 4;
      
      public static const COMPLETED:uint = 5;
      
      public static const DESTRUCTED:uint = 6;
      
      public static const ERROR:uint = 7;
      
      public static const DELAYED:uint = 8;
      
      public static const WAIT_FOR_MID:uint = 9;
      
      public static const PLAYING:uint = 10;
   }
}
