package tv.ustream.viewer.logic.ads
{
   import flash.net.URLLoader;
   import flash.display.Sprite;
   import flash.utils.Timer;
   import flash.display.MovieClip;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.geom.Matrix;
   import flash.external.ExternalInterface;
   import flash.net.URLVariables;
   import flash.net.URLRequest;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import tv.ustream.tween.Tween;
   import flash.display.DisplayObject;
   import flash.events.MouseEvent;
   import flash.net.navigateToURL;
   import flash.geom.Rectangle;
   import flash.system.Security;
   
   public class ProviderYahooIM extends ProviderBase
   {
      
      public function ProviderYahooIM(param1:Ads, param2:Object) {
         adItems = [];
         adList = [];
         adLabel = {};
         super();
         this.ads = param1;
         try
         {
            Security.allowDomain("*");
         }
         catch(e:SecurityError)
         {
         }
         try
         {
            Security.allowInsecureDomain("*");
         }
         catch(e:SecurityError)
         {
         }
         echo("constructor - timers - start: " + param2.adStartTime + " - overlayDuration: " + param2.overlayDuration + " - adDelay: " + param2.adDelay);
         adType = "yahooim";
         this.data = param2;
         if(param1.adHolder)
         {
            width = param1.adHolder.width;
            height = param1.adHolder.height;
         }
         if(param2.adStartTime)
         {
            startTimer = new Timer((param2.adStartTime) * 1000,1);
            startTimer.addEventListener("timer",onAdStart);
         }
         if(param2.overlayDuration)
         {
            durationTimer = new Timer((param2.overlayDuration) * 1000,1);
            durationTimer.addEventListener("timer",onAdDuration);
         }
         if(param2.adDelay)
         {
            delayTimer = new Timer((param2.adDelay) * 1000,1);
            delayTimer.addEventListener("timer",onAdDelay);
         }
         width = 320;
         height = 240;
      }
      
      public static const TWEENEQ:String = "easeOutCubic";
      
      public static const FIXLINK:String = "fixLink";
      
      private var dataLoader:URLLoader;
      
      private var adItems:Array;
      
      private var adList:Array;
      
      private var adLabel:Object;
      
      private var adIndex:uint = 0;
      
      private var adHeight:uint = 70;
      
      private var bg:Sprite;
      
      private var ad:Sprite;
      
      private var adContainer:Sprite;
      
      private var adPager:Sprite;
      
      private var closeButton:Sprite;
      
      private var openButton:Sprite;
      
      private var fixLink:Sprite;
      
      private var fixContainer:Sprite;
      
      private var isOpen:Boolean = true;
      
      private var isDimensionSmall:Boolean = false;
      
      private var startTimer:Timer;
      
      private var durationTimer:Timer;
      
      private var delayTimer:Timer;
      
      private var bgTimer:Timer;
      
      private var hideTimer:Timer;
      
      private var pos:String = "";
      
      override public function init(param1:MovieClip) : void {
         var _loc2_:* = null;
         var _loc5_:* = null;
         width = ads.adHolder.width;
         height = ads.adHolder.height;
         adList[0] = {};
         ad = new Sprite();
         bg = new Sprite();
         ad.addChild(new Sprite()).alpha = 0;
         drawBg();
         adContainer = new Sprite();
         ad.addChild(new Sprite());
         var _loc6_:* = false;
         adContainer.mouseEnabled = _loc6_;
         adContainer.mouseChildren = _loc6_;
         fixContainer = new Sprite();
         ad.addChild(new Sprite()).alpha = 0;
         fixLink = new Sprite();
         fixContainer.addChild(new Sprite());
         closeButton = new Sprite();
         fixContainer.addChild(new Sprite());
         closeButton.graphics.beginFill(16711680,0);
         closeButton.graphics.drawRect(0,0,20,20);
         closeButton.graphics.endFill();
         closeButton.buttonMode = true;
         var _loc3_:Sprite = new Sprite();
         _loc3_.graphics.beginFill(16777215);
         _loc3_.graphics.drawCircle(0,0,6);
         _loc3_.graphics.drawRect(-4,-1,8,2);
         _loc3_.graphics.drawRect(-1,-4,2,3);
         _loc3_.graphics.drawRect(-1,1,2,3);
         _loc3_.graphics.endFill();
         _loc6_ = 10;
         _loc3_.y = _loc6_;
         _loc3_.x = _loc6_;
         closeButton.x = width - closeButton.width - 1;
         closeButton.y = 1;
         closeButton.addChild(_loc3_).rotation = 45;
         closeButton.addEventListener("click",onCloseClick);
         bg.addEventListener("click",onAdClick);
         bg.buttonMode = true;
         openButton = new Sprite();
         openButton.graphics.beginFill(16777215,0.5);
         openButton.graphics.drawRoundRect(0,0,25,15,5,5);
         openButton.graphics.drawRoundRect(1,1,23,13,4,4);
         openButton.graphics.beginFill(0,0.65);
         openButton.graphics.drawRoundRect(1,1,23,13,4,4);
         openButton.graphics.endFill();
         var _loc4_:TextField = new TextField();
         _loc4_.antiAliasType = "advanced";
         _loc4_.text = "Ad";
         _loc4_.width = 20;
         _loc4_.height = 15;
         _loc4_.y = -1;
         _loc4_.setTextFormat(new TextFormat("Arial",10,16777215));
         _loc6_ = false;
         _loc4_.mouseEnabled = _loc6_;
         _loc6_ = _loc6_;
         _loc4_.selectable = _loc6_;
         _loc6_ = _loc6_;
         _loc4_.tabEnabled = _loc6_;
         openButton.tabEnabled = _loc6_;
         openButton.addChild(_loc4_).x = 4;
         openButton.visible = false;
         openButton.buttonMode = true;
         openButton.addEventListener("click",onOpenClick);
         ads.adHolder.addChild(openButton);
         if(data.adNum > 1)
         {
            adPager = new Sprite();
            adPager.visible = false;
            _loc2_ = createPagerButton(-45);
            _loc2_.addEventListener("click",onPrevClick);
            adPager.addChild(_loc2_);
            _loc5_ = createPagerButton(135);
            _loc5_.addEventListener("click",onNextClick);
            adPager.addChild(_loc5_).x = 17;
            fixContainer.addChild(adPager).y = 0;
         }
      }
      
      private function createPagerButton(param1:Number) : Sprite {
         var _loc2_:uint = 6;
         var _loc4_:Sprite = new Sprite();
         _loc4_.graphics.beginFill(65280,0);
         _loc4_.graphics.drawRect(0,0,_loc2_ * 2,_loc2_ * 2);
         _loc4_.graphics.endFill();
         var _loc3_:Sprite = new Sprite();
         _loc3_.graphics.beginFill(16777215);
         _loc3_.graphics.drawCircle(0,0,_loc2_);
         _loc3_.graphics.drawRect(-3,-3,6,2);
         _loc3_.graphics.drawRect(-3,-1,2,4);
         _loc3_.graphics.endFill();
         var _loc5_:* = _loc2_;
         _loc3_.y = _loc5_;
         _loc3_.x = _loc5_;
         _loc3_.rotation = param1;
         _loc4_.addChild(_loc3_);
         return _loc4_;
      }
      
      private function drawBg() : void {
         bg.graphics.clear();
         var _loc1_:uint = 5;
         var _loc2_:Matrix = new Matrix();
         _loc2_.createGradientBox(width,5,3.141592653589793 / 2);
         bg.graphics.beginGradientFill("linear",[6710886,0],[0.65,0.65],[0,255],_loc2_,"pad","rgb");
         bg.graphics.drawRect(0,0,width,_loc1_);
         bg.graphics.beginFill(0,0.65);
         bg.graphics.drawRect(0,_loc1_,width,adHeight - _loc1_);
         bg.graphics.endFill();
      }
      
      override public function requestAd(param1:uint) : void {
         var _loc4_:String = "http://im.yahooapis.jp/v2/xml/";
         _loc4_ = _loc4_ + ("?source=" + data.source);
         _loc4_ = _loc4_ + ("&type=" + data.typeId);
         _loc4_ = _loc4_ + ("&ctxtId=" + data.ctxtId);
         _loc4_ = _loc4_ + ("&maxCount=" + data.adNum);
         _loc4_ = _loc4_ + "&keywordCharEnc=utf8";
         _loc4_ = _loc4_ + "&outputCharEnc=utf8";
         var _loc5_:String = "";
         try
         {
            _loc5_ = ExternalInterface.call("function(){return navigator.userAgent}");
         }
         catch(e:Error)
         {
         }
         var _loc2_:URLVariables = new URLVariables();
         _loc2_.ctxtUrl = ads.mediaInfo.url;
         _loc2_.affilData = "";
         echo("clientIp: " + ads.clientIp);
         if(ads.clientIp)
         {
            _loc2_.affilData = _loc2_.affilData + ("ip=" + ads.clientIp);
         }
         if(_loc5_)
         {
            _loc2_.affilData = _loc2_.affilData + ((_loc2_.affilData == ""?"":"&") + "ua=" + _loc5_);
         }
         _loc4_ = _loc4_ + ("&" + _loc2_.toString());
         var _loc3_:URLRequest = new URLRequest(_loc4_);
         _loc3_.method = "POST";
         echo("requestAd: " + _loc3_.url);
         dataLoader = new URLLoader();
         dataLoader.addEventListener("complete",onDataLoadingComplete);
         dataLoader.addEventListener("ioError",onLoadingError);
         dataLoader.addEventListener("securityError",onLoadingError);
         dataLoader.addEventListener("asyncError",onLoadingError);
         dataLoader.load(_loc3_);
      }
      
      private function removeLoaderEvents() : void {
         dataLoader.removeEventListener("complete",onDataLoadingComplete);
         dataLoader.removeEventListener("ioError",onLoadingError);
         dataLoader.removeEventListener("securityError",onLoadingError);
         dataLoader.removeEventListener("asyncError",onLoadingError);
      }
      
      private function onLoadingError(param1:Event = null) : void {
         echo("onLoadingError: " + (param1?param1.toString():""));
         removeLoaderEvents();
      }
      
      private function onDataLoadingComplete(param1:Event) : void {
         var _loc5_:* = null;
         var _loc2_:* = null;
         removeLoaderEvents();
         var _loc4_:XML = new XML(dataLoader.data);
         echo("onDataLoadingComplete: " + _loc4_.ResultSet.Listing.length());
         echo("onDataLoadingComplete: " + _loc4_.ResultSet);
         adItems = [];
         if(_loc4_.ResultSet.Label)
         {
            adLabel.text = _loc4_.ResultSet.Label.LabelText;
            adLabel.url = _loc4_.ResultSet.Label.InquiryUrl;
            _loc5_ = new TextField();
            _loc5_.antiAliasType = "advanced";
            _loc5_.text = adLabel.text;
            _loc5_.autoSize = "left";
            _loc5_.height = 20;
            _loc5_.setTextFormat(new TextFormat("Arial",10,16777215));
            _loc5_.borderColor = 16711680;
            _loc5_.border = false;
            _loc6_ = false;
            _loc5_.tabEnabled = _loc6_;
            fixLink.tabEnabled = _loc6_;
            _loc6_ = false;
            _loc5_.mouseEnabled = _loc6_;
            _loc5_.selectable = _loc6_;
            fixLink.addChild(_loc5_);
            fixLink.buttonMode = true;
            fixLink.x = Math.round(width - fixLink.width - 7);
            fixLink.addEventListener("click",onFixLinkClick);
         }
         var _loc8_:* = 0;
         var _loc7_:* = _loc4_.ResultSet.Listing;
         for each(_loc3_ in _loc4_.ResultSet.Listing)
         {
            _loc2_ = 
               {
                  "title":_loc3_.attribute("title"),
                  "description":_loc3_.attribute("description"),
                  "siteHost":_loc3_.attribute("siteHost"),
                  "rank":_loc3_.attribute("rank"),
                  "clickUrl":_loc3_.ClickUrl
               };
            adItems.push(_loc2_);
         }
         adItems.sortOn("rank");
         if(adItems.length)
         {
            if(adItems.length > 1 && adPager && !adPager.visible)
            {
               adPager.visible = true;
            }
            if(startTimer)
            {
               startTimer.start();
            }
            else
            {
               onAdStart();
            }
            if(!(data.duration == null) && (data.duration) > 0 && adTimer == null)
            {
               startCountdown(data.duration);
            }
         }
         else if(ads.errorWait && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
         {
            startCountdown(data.duration);
         }
         else
         {
            ads.onAdError();
         }
         
      }
      
      private function onAdStart(param1:TimerEvent = null) : void {
         echo("onAdStart - ads.adHolder: " + ads.adHolder);
         if(startTimer)
         {
            startTimer.stop();
            startTimer = null;
         }
         if(ads.adHolder && !ads.adHolder.contains(ad))
         {
            ads.adHolder.addChild(ad).y = height - adHeight;
         }
         if(!ad.visible && !isDimensionSmall)
         {
            echo("show ad onAdStart");
            ad.visible = true;
         }
         var _loc2_:* = true;
         ad.mouseEnabled = _loc2_;
         ad.mouseChildren = _loc2_;
         adList[1] = new YimItem(adItems[adIndex],width,height,echo);
         adList[1].init();
         adContainer.addChild(adList[1]);
         setPlayerSize(width,height,true);
         if(!bgTimer)
         {
            bgTimer = new Timer(500,1);
            bgTimer.addEventListener("timer",onBgTimer);
         }
         if(!fixContainer.visible)
         {
            fixContainer.visible = true;
         }
         if(bg.alpha == 0)
         {
            bgTimer.start();
            Tween.to(bg,{"alpha":1},15,"easeOutCubic");
         }
         else
         {
            onBgTimer();
         }
         ads.onAdShown();
         ads.logAdEvent(
            {
               "eventName":"adShow",
               "host":"yahooim",
               "adType":"mid"
            });
      }
      
      private function onBgTimer(... rest) : void {
         echo("onBgTimer");
         if(rest && rest[0] is TimerEvent)
         {
            Tween.to(fixContainer,{"alpha":1},15,"easeOutCubic");
         }
         bgTimer.stop();
         hideCurrentAd();
         showNextAd();
         if(durationTimer)
         {
            durationTimer.start();
         }
      }
      
      private function onAdDuration(param1:TimerEvent) : void {
         echo("onAdDuration");
         durationTimer.stop();
         durationTimer.reset();
         hideCurrentAd(true);
         if(delayTimer)
         {
            delayTimer.start();
         }
      }
      
      private function onAdDelay(param1:TimerEvent) : void {
         echo("onAdDelay");
         delayTimer.stop();
         delayTimer.reset();
         requestAd(adPosition);
      }
      
      private function hideCurrentAd(param1:Boolean = false) : void {
         echo("hideCurrentAd - hideBg: " + param1);
         if(adList[0])
         {
            if(param1)
            {
               ads.onAdRemoved();
               Tween.to(bg,{"alpha":0},15,"easeOutCubic");
               Tween.to(fixContainer,{"alpha":0},15,"easeOutCubic");
               if(!isOpen && openButton)
               {
                  openButton.visible = false;
                  isOpen = true;
               }
            }
            if(adList[0].hide)
            {
               adList[0].hide();
            }
            if(!hideTimer)
            {
               hideTimer = new Timer(500,1);
               hideTimer.addEventListener("timer",onHideFinished);
            }
            hideTimer.start();
         }
         else
         {
            adList[0] = adList[1];
            adList.shift();
         }
      }
      
      private function onHideFinished(param1:TimerEvent) : void {
         echo("onHideFinished");
         hideTimer.stop();
         hideTimer.reset();
         var _loc2_:* = adList.shift();
         if(fixContainer.alpha == 0)
         {
            ad.visible = false;
         }
         if(_loc2_ is DisplayObject && (adContainer.contains(_loc2_)))
         {
            adContainer.removeChild(_loc2_);
         }
      }
      
      private function showNextAd() : void {
         if(adList[1])
         {
            adList[1].show();
         }
         else
         {
            adList[0].show();
         }
         if(ad && !ad.visible && !isDimensionSmall)
         {
            echo("show ad showNextAd");
            ad.visible = true;
         }
         if(ad.alpha == 0)
         {
            ad.alpha = 1;
         }
      }
      
      private function onFixLinkClick(param1:MouseEvent) : void {
         echo("onFixLinkClick");
      }
      
      private function onAdClick(param1:MouseEvent) : void {
         echo("onAdClick");
      }
      
      private function onCloseClick(param1:MouseEvent) : void {
         echo("onCloseClick");
         isOpen = false;
         var _loc2_:* = false;
         ad.mouseChildren = _loc2_;
         ad.mouseEnabled = _loc2_;
         openButton.visible = true;
         ads.onAdHidden();
         Tween.to(ad,{"alpha":0},15,"easeOutCubic");
      }
      
      private function onOpenClick(param1:MouseEvent) : void {
         echo("onOpenClick");
         isOpen = true;
         var _loc2_:* = true;
         ad.mouseChildren = _loc2_;
         ad.mouseEnabled = _loc2_;
         openButton.visible = false;
         ads.onAdShown();
         Tween.to(ad,{"alpha":1},15,"easeOutCubic");
      }
      
      private function isAnimatedItem() : Boolean {
         var _loc2_:* = 0;
         var _loc1_:* = false;
         _loc2_ = 0;
         while(_loc2_ < adList.length)
         {
            if(adList[_loc2_] && adList[_loc2_] is YimItem && adList[_loc2_].isAnimating == true)
            {
               _loc1_ = true;
            }
            _loc2_++;
         }
         return _loc1_;
      }
      
      private function onPrevClick(param1:MouseEvent) : void {
         if(isAnimatedItem())
         {
            return;
         }
         echo("onPrevClick");
         adIndex = adIndex - 1;
         if(adIndex < 0)
         {
            adIndex = adItems.length - 1;
         }
         createCurrentAd();
      }
      
      private function onNextClick(param1:MouseEvent) : void {
         if(isAnimatedItem())
         {
            return;
         }
         echo("onNextClick");
         adIndex = adIndex + 1;
         if(adIndex >= adItems.length)
         {
            adIndex = 0;
         }
         createCurrentAd();
      }
      
      private function createCurrentAd() : void {
         if(durationTimer && durationTimer.running)
         {
            durationTimer.reset();
            durationTimer.start();
         }
         adList[1] = new YimItem(adItems[adIndex],width,height,echo);
         adList[1].init();
         adContainer.addChild(adList[1]);
         setPlayerSize(width,height,true);
         hideCurrentAd();
         showNextAd();
      }
      
      private function render() : void {
         if(!enabled)
         {
            return;
         }
         echo("render: " + width);
         drawBg();
         if(adList[1])
         {
            adList[1].resize(width);
         }
         closeButton.x = Math.round(width - closeButton.width - 5);
         if(fixLink)
         {
            fixLink.x = Math.round(width - fixLink.width - 7);
            fixLink.y = adHeight - fixLink.height - 5;
         }
         if(width < 300)
         {
            isDimensionSmall = true;
            echo("hide ad render");
            ad.visible = false;
         }
         else if(isDimensionSmall)
         {
            isDimensionSmall = false;
            echo("show ad render");
            ad.visible = true;
         }
         
         ad.y = Math.round(height - adHeight) + 5;
         ad.scrollRect = new Rectangle(0,0,width,height);
         if(openButton)
         {
            openButton.y = Math.round(height - openButton.height - 1);
            openButton.x = Math.round(width - openButton.width - 5);
         }
         if(adPager)
         {
            adPager.x = Math.round(closeButton.x + closeButton.width - adPager.width);
         }
      }
      
      override public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
         if((!(width == param1) || !(height == param2) || (param3)) && (enabled))
         {
            width = param1;
            height = param2;
            render();
         }
      }
      
      override public function destroyAd() : void {
         var _loc2_:* = 0;
         var _loc1_:* = undefined;
         echo("destroyAd");
         if(bgTimer)
         {
            bgTimer.stop();
         }
         if(delayTimer)
         {
            delayTimer.stop();
         }
         if(durationTimer)
         {
            durationTimer.stop();
         }
         if(hideTimer)
         {
            hideTimer.stop();
         }
         if(startTimer)
         {
            startTimer.stop();
         }
         if(adTimer)
         {
            adTimer.stop();
         }
         _loc2_ = 0;
         while(_loc2_ < adList.length)
         {
            _loc1_ = adList[_loc2_];
            if(!(_loc1_ == null) && _loc1_ is Sprite && (adContainer.contains(_loc1_)))
            {
               adContainer.removeChild(_loc1_);
            }
            _loc2_++;
         }
         if(ads.adHolder.contains(ad))
         {
            ads.adHolder.removeChild(ad);
         }
         if(ads.adHolder.contains(openButton))
         {
            ads.adHolder.removeChild(openButton);
         }
         adList = [];
         adItems = [];
         openButton = null;
         ad = null;
         enabled = false;
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         echo("onTimer");
         if(!enabled)
         {
            return;
         }
         super.onTimer();
         ads.onAdComplete();
      }
   }
}
import flash.display.Sprite;
import flash.text.TextField;
import flash.utils.Timer;
import flash.events.Event;
import flash.text.TextFormat;
import tv.ustream.tween.Tween;
import flash.events.TimerEvent;

class YimItem extends Sprite
{
   
   function YimItem(param1:Object, param2:uint, param3:uint, param4:Function) {
      super();
      this.source = param1;
      this.w = param2;
      this.h = param3;
      this.tracer = param4;
      title = new TextField();
      addChild(new TextField());
      description = new TextField();
      addChild(new TextField());
      site = new TextField();
      addChild(new TextField());
      addEventListener("addedToStage",onAddedToStage);
   }
   
   private var w:uint = 0;
   
   private var h:uint = 0;
   
   private var title:TextField;
   
   private var description:TextField;
   
   private var site:TextField;
   
   private var source:Object;
   
   private var textAlign:String = "left";
   
   private var margin:uint = 10;
   
   private var tracer:Function;
   
   private var tweenTimer:Timer;
   
   private var tweenDuration:uint = 15;
   
   private var _index:uint;
   
   private var _isAnimating:Boolean = false;
   
   private function onAddedToStage(param1:Event) : void {
      removeEventListener("addedToStage",onAddedToStage);
      var _loc2_:Number = 1000 / stage.frameRate;
      tweenTimer = new Timer(Math.round(_loc2_ * (tweenDuration + 5)),1);
      tweenTimer.addEventListener("timer",onTweenComlete);
   }
   
   public function init() : void {
      tracer("[YimItem] init");
      title.antiAliasType = "advanced";
      title.text = source.title;
      title.width = w;
      title.height = 24;
      title.y = 5;
      title.setTextFormat(new TextFormat("Arial",15,16777215,true,null,null,null,null,textAlign,margin,margin));
      title.cacheAsBitmap = true;
      description.antiAliasType = "advanced";
      description.text = source.description;
      description.width = w;
      description.height = 20;
      description.multiline = true;
      description.wordWrap = false;
      description.y = Math.ceil(title.y + title.height - 3);
      description.setTextFormat(new TextFormat("Arial",12,16777215,false,null,null,null,null,textAlign,margin,margin));
      description.cacheAsBitmap = true;
      site.antiAliasType = "advanced";
      site.text = source.siteHost;
      site.width = w;
      site.height = 20;
      site.y = Math.ceil(description.y + description.height - 3);
      site.setTextFormat(new TextFormat("Arial",10,16762956,false,null,null,null,null,textAlign,margin,margin));
      site.cacheAsBitmap = true;
      var _loc1_:* = false;
      site.tabEnabled = _loc1_;
      _loc1_ = _loc1_;
      description.tabEnabled = _loc1_;
      _loc1_ = _loc1_;
      title.tabEnabled = _loc1_;
      tabEnabled = _loc1_;
      _loc1_ = false;
      site.mouseEnabled = _loc1_;
      _loc1_ = _loc1_;
      description.mouseEnabled = _loc1_;
      title.mouseEnabled = _loc1_;
      _loc1_ = false;
      site.selectable = _loc1_;
      _loc1_ = _loc1_;
      description.selectable = _loc1_;
      title.selectable = _loc1_;
      _loc1_ = -w;
      site.x = _loc1_;
      _loc1_ = _loc1_;
      description.x = _loc1_;
      title.x = _loc1_;
   }
   
   public function show() : void {
      Tween.to(title,{"x":0},15,"easeOutCubic");
      Tween.to(description,{"x":0},15,"easeOutCubic");
      Tween.to(site,{"x":0},15,"easeOutCubic");
      _isAnimating = true;
      tweenTimer.start();
   }
   
   public function hide() : void {
      Tween.to(title,{"x":-w},15,"easeOutCubic");
      Tween.to(description,{"x":-w},15,"easeOutCubic");
      Tween.to(site,{"x":-w},15,"easeOutCubic");
      _isAnimating = true;
      tweenTimer.start();
   }
   
   private function onTweenComlete(param1:TimerEvent) : void {
      tweenTimer.stop();
      tweenTimer.reset();
      _isAnimating = false;
   }
   
   public function resize(param1:Number) : void {
      var _loc2_:* = param1;
      this.w = _loc2_;
      _loc2_ = _loc2_;
      site.width = _loc2_;
      _loc2_ = _loc2_;
      description.width = _loc2_;
      title.width = _loc2_;
   }
   
   public function get isAnimating() : Boolean {
      return _isAnimating;
   }
}
