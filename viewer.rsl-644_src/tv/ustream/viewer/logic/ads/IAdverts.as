package tv.ustream.viewer.logic.ads
{
   import tv.ustream.interfaces.IDispatcher;
   import flash.display.Sprite;
   
   public interface IAdverts extends IDispatcher
   {
      
      function set data(param1:Array) : void;
      
      function set media(param1:Sprite) : void;
      
      function get started() : Boolean;
   }
}
