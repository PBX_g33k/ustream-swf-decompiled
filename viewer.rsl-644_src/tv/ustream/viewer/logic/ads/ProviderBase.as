package tv.ustream.viewer.logic.ads
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.Timer;
   import flash.display.MovieClip;
   import flash.events.TimerEvent;
   
   public class ProviderBase extends Dispatcher
   {
      
      public function ProviderBase() {
         super("ProviderBase");
      }
      
      protected var data:Object;
      
      protected var ads:Ads;
      
      var adStatus:String;
      
      var adType:String;
      
      var adPosition:uint = 0;
      
      var enabled:Boolean = true;
      
      var adTimer:Timer;
      
      var hasRespond:uint = 0;
      
      var hasTimeout:Boolean = false;
      
      var isDestroyed:Boolean = false;
      
      var width:Number;
      
      var height:Number;
      
      public function init(param1:MovieClip) : void {
      }
      
      public function requestAd(param1:uint) : void {
      }
      
      public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
      }
      
      public function setAdPosition(param1:uint) : void {
         adPosition = param1;
      }
      
      public function play() : Boolean {
         return true;
      }
      
      public function destroyAd() : void {
      }
      
      public function finish() : Boolean {
         return true;
      }
      
      public function startCountdown(param1:uint = 30) : void {
         var param1:uint = param1 * 1000;
         echo("startTimer - delay: " + param1);
         adTimer = new Timer(param1,1);
         adTimer.addEventListener("timer",onTimer);
         adTimer.start();
      }
      
      public function onTimer(param1:TimerEvent = null) : void {
         if(adTimer)
         {
            if(adTimer.running)
            {
               adTimer.stop();
            }
            if(adTimer.hasEventListener("timer"))
            {
               adTimer.removeEventListener("timer",onTimer);
            }
            adTimer = null;
         }
      }
      
      public function postNeeded() : void {
      }
      
      public function setData(param1:Object) : void {
         this.data = param1;
      }
      
      public function setLocalConnections(param1:String, param2:String) : void {
      }
      
      public function pauseVideo() : void {
      }
      
      public function playVideo() : void {
      }
      
      public function adAvailable(param1:Boolean) : void {
      }
      
      public function viewCovered() : void {
      }
      
      public function handleAdEvent(param1:Object) : void {
      }
      
      public function handleVpaidEvent(param1:Object) : void {
      }
      
      public function setStatusCallback() : void {
      }
      
      public function setPlayTime(param1:Number) : void {
      }
      
      public function finished(param1:Boolean = false) : void {
      }
      
      public function onVideoFinished() : void {
      }
      
      public function setDivId(param1:String) : void {
      }
      
      public function needPlay() : Boolean {
         return true;
      }
      
      public function forceControl(param1:String) : void {
      }
      
      public function set contentStatus(param1:uint) : void {
      }
      
      public function set contentDuration(param1:*) : void {
      }
   }
}
