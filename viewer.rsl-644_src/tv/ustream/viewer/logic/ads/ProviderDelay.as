package tv.ustream.viewer.logic.ads
{
   import flash.events.TimerEvent;
   
   public class ProviderDelay extends ProviderBase
   {
      
      public function ProviderDelay(param1:Ads, param2:Object) {
         super();
         this.ads = param1;
         if(param2 && param2.duration && (param2.duration) > 0)
         {
            startCountdown(param2.duration);
         }
         else
         {
            param1.onAdError();
         }
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         echo("onTimer()");
         super.onTimer();
         ads.onAdComplete();
      }
      
      override public function destroyAd() : void {
         echo("destroyAd()");
         super.onTimer();
      }
   }
}
