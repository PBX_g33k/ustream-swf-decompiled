package tv.ustream.viewer.logic.ads
{
   import flash.display.MovieClip;
   
   public class AdHolder extends MovieClip
   {
      
      public function AdHolder() {
         super();
         name = "proxyholder";
         mouseEnabled = false;
      }
      
      private var _width:Number = 320;
      
      private var _height:Number = 240;
      
      private var _partnerProgramHeight:uint = 0;
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         _width = param1;
      }
      
      override public function get height() : Number {
         return Math.floor(_height - _partnerProgramHeight);
      }
      
      override public function set height(param1:Number) : void {
         _height = param1;
      }
      
      function set partnerProgramHeight(param1:uint) : void {
         _partnerProgramHeight = param1;
      }
   }
}
