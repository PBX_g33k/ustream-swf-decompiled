package tv.ustream.viewer.logic.ads
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import flash.filters.DropShadowFilter;
   import flash.text.TextFormat;
   
   public class VideoAdTimer extends Sprite
   {
      
      public function VideoAdTimer(param1:String, param2:String = "right", param3:Boolean = true, param4:Number = 0) {
         super();
         tf = new TextField();
         _count = param4;
         this.str = param1;
         this.align = param2;
         if(param3)
         {
            shadowFilter = new DropShadowFilter(0,45,0,1,3,3,2);
         }
      }
      
      private var tf:TextField;
      
      private var str:String;
      
      private var align:String = "right";
      
      private var shadowFilter:DropShadowFilter;
      
      private var _count:Number = 0;
      
      public function init() : void {
         var _loc1_:TextFormat = new TextFormat();
         _loc1_.font = "Arial";
         _loc1_.size = 11;
         _loc1_.bold = true;
         _loc1_.color = 16777215;
         tf.selectable = false;
         tf.autoSize = align == "right"?"right":"left";
         tf.sharpness = 100;
         tf.defaultTextFormat = _loc1_;
         tf.text = _count > 0?str.split("###").join(_count.toString()):str;
         if(shadowFilter)
         {
            tf.filters = [shadowFilter];
         }
         addChild(tf);
      }
      
      public function get count() : Number {
         return _count;
      }
      
      public function set count(param1:Number) : void {
         if(_count != param1)
         {
            _count = param1;
            updateText();
         }
      }
      
      public function set text(param1:String) : void {
         this.str = param1;
         if(!(param1.indexOf("###") == -1) && _count > 0)
         {
            updateText();
         }
      }
      
      private function updateText() : void {
         tf.text = str.split("###").join(_count.toString());
      }
      
      public function destruct() : void {
         if(tf)
         {
            if(contains(tf))
            {
               removeChild(tf);
            }
            tf = null;
         }
         if(shadowFilter)
         {
            shadowFilter = null;
         }
      }
   }
}
