package tv.ustream.viewer.logic.ads
{
   import flash.utils.Timer;
   import tv.ustream.tools.This;
   import flash.net.URLVariables;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.Debug;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import flash.external.ExternalInterface;
   
   public class ProviderAdaptv extends ProviderBase
   {
      
      public function ProviderAdaptv(param1:Ads, param2:Object, param3:* = null) {
         eventList = 
            {
               "scriptFetched":onScriptFetched,
               "breakStarted":onBreakStarted,
               "breakEnded":onBreakEnded,
               "custom":onCustom,
               "clickThru":onClickThru,
               "linearChanged":onLinearChanged,
               "companion":onCompanion,
               "noCompanion":onNoCompanion,
               "paused":onPaused,
               "resumed":onResumed,
               "error":onError,
               "volumeChanged":onVolumeChanged,
               "overlayStarted":onOverlayStarted,
               "overlayStopped":onOverlayStopped,
               "overlayExpanded":onOverlayExpanded,
               "overlayCollapsed":onOverlayCollapsed
            };
         super();
         index = index + 1;
         currentIndex = index;
         this.ads = param1;
         adType = "adaptv";
         echo("constructor");
         this.data = param2;
         scriptFetchTimer = new Timer(10000,1);
         scriptFetchTimer.addEventListener("timer",scriptFetchTimeout);
         playheadTimer = new Timer(1000);
         playheadTimer.addEventListener("timer",onPlayheadChanged);
         setSizeTimer = new Timer(500,1);
         setSizeTimer.addEventListener("timer",onSetSize);
         adStartTimer = new Timer(30000);
         adStartTimer.addEventListener("timer",onAdStartTimeout);
         if(!adPlayer)
         {
            adPlayer = param3;
            adPlayer.apiVersion("2.1");
         }
         if(!browserLanguage)
         {
            try
            {
               browserLanguage = ExternalInterface.call("navigator.language.toString");
            }
            catch(e:Error)
            {
            }
         }
         if(browserLanguage == "undefined")
         {
            try
            {
               browserLanguage = ExternalInterface.call("navigator.userLanguage.toString");
            }
            catch(e:Error)
            {
            }
         }
      }
      
      private static const PRECACHE_DELAY:Number = 15;
      
      private static var index:uint = 0;
      
      private static var browserLanguage:String = "";
      
      private var eventList:Object;
      
      private var adPlayer;
      
      private var currentIndex:uint = 0;
      
      private var config:Object;
      
      private var playTime:uint = 0;
      
      private var setSizeTimer:Timer;
      
      private var playheadTimer:Timer;
      
      private var scriptFetchTimer:Timer;
      
      private var adStartTimer:Timer;
      
      private var overlayTimer:Timer;
      
      private var preCacheTimer:Timer;
      
      var isCompleted:Boolean = false;
      
      var isAdShown:Boolean = false;
      
      private var isStartBreakCalled:Boolean = false;
      
      override public function requestAd(param1:uint) : void {
         var _loc5_:* = 0;
         var _loc10_:* = null;
         var _loc3_:* = null;
         echo("requestAd() - adPlayer: " + adPlayer + " - adPosition: " + adPosition + " - new pos: " + param1 + " - externalinterface: " + This.externalInterface);
         if(!This.externalInterface)
         {
            onAdFinished({"error":"ExternalInterfaceFalse"});
            return;
         }
         if(!enabled || (scriptFetchTimer.running))
         {
            echo(!enabled && !scriptFetchTimer.running?"error: adaptvas3 is not enabled":"error: request is in progress");
            return;
         }
         var _loc18_:* = 0;
         var _loc17_:* = eventList;
         for(_loc14_ in eventList)
         {
            if(!adPlayer.hasEventListener(_loc14_))
            {
               adPlayer.addEventListener(_loc14_,eventList[_loc14_]);
            }
         }
         if(playheadTimer.running)
         {
            playheadTimer.stop();
         }
         var _loc4_:String = ads.mediaInfo.keywords?ads.mediaInfo.keywords.join(","):"";
         var _loc16_:String = (ads.mediaInfo.description).replace(new RegExp("<[^<]+?>","gi"),"");
         var _loc12_:Array = [",",".",";","!","?","  "];
         _loc5_ = 0;
         while(_loc5_ < _loc12_.length)
         {
            _loc16_ = _loc16_.split(_loc12_[_loc5_]).join(" ");
            _loc5_++;
         }
         _loc16_ = _loc16_.replace(String.fromCharCode(13),"");
         _loc16_ = _loc16_.replace("\r","");
         _loc16_ = _loc16_.replace("\n","");
         var _loc7_:Array = new String(_loc16_).split(" ");
         _loc5_ = 0;
         while(_loc5_ < _loc7_.length)
         {
            if((_loc4_ + _loc7_[_loc5_]).length < 256 && _loc7_[_loc5_].length > 0)
            {
               _loc4_ = _loc4_ + ("," + _loc7_[_loc5_]);
               _loc5_++;
               continue;
            }
            break;
         }
         _loc16_ = _loc7_.join(",");
         var _loc15_:String = data.adaptag?data.adaptag.join(","):"";
         var _loc11_:String = ads.mediaInfo.categories?ads.mediaInfo.categories.join(","):"";
         var _loc13_:String = ads.media.pageUrl;
         if(ads.media.urlFailure != "")
         {
            _loc13_ = ads.media.urlFailure;
         }
         var _loc2_:URLVariables = new URLVariables();
         _loc2_["title"] = ads.mediaInfo.title;
         var _loc6_:String = _loc2_.toString().split("=")[1];
         config = 
            {
               "key":data.key,
               "zid":data.zid,
               "id":(ads.media.type == "recorded" && ads.media.channelId?ads.media.channelId:ads.mediaInfo.id),
               "title":_loc6_,
               "description":_loc16_,
               "adaptag":_loc15_,
               "keywords":_loc4_,
               "categories":(ads.mediaInfo.categoryIds?ads.mediaInfo.categoryIds.main + "-" + ads.mediaInfo.categoryIds.sub:_loc11_),
               "duration":ads.duration as String || "555000",
               "pageUrlOv":ads.mediaInfo.url
            };
         if(_loc13_)
         {
            if(This.onSite(_loc13_))
            {
               config.url = ads.mediaInfo.url;
            }
            else
            {
               config.url = _loc13_;
            }
         }
         else
         {
            config.url = "ustream.com";
         }
         if(!data.context)
         {
            data.context = {};
         }
         if(data.context)
         {
            config.context = data.context;
         }
         if(data.uname)
         {
            config.context.uname = data.uname;
         }
         config.context.prg = "";
         var _loc9_:Object = ads.adaptvParams;
         if(_loc9_)
         {
            if(_loc9_.locale)
            {
               config.context.locale = _loc9_.locale + (_loc9_.location?"|geo-" + _loc9_.location:"|geo-null");
            }
            if(_loc9_.userId)
            {
               config.context.uname = _loc9_.userId;
            }
            if(_loc9_.vid)
            {
               config.context.vid = _loc9_.vid;
            }
            if(_loc9_.autoplay && (This.onSite(_loc13_) == 1 || This.onSite(_loc13_) == 0))
            {
               config.context.prg = config.context.prg + "a";
            }
            if(_loc9_.featured)
            {
               config.context.prg = config.context.prg + "f";
            }
            if(_loc9_.ps4)
            {
               config.context.prg = config.context.prg + "4";
            }
            if(_loc9_.verizon)
            {
               config.context.prg = config.context.prg + "v";
            }
         }
         if(Logic.instance.viewMode == "explore" || Logic.instance.viewMode == "discovery")
         {
            config.context.prg = config.context.prg + "e";
            _loc10_ = _loc13_.substr(_loc13_.indexOf("://")?_loc13_.indexOf("://") + 3:0.0).split("/");
            if(This.onSite(_loc13_) & 2 && (_loc10_.length == 1 || _loc10_[1].length == 0 || (_loc10_[1] as String).charAt(0) == "?") || (_loc10_[1] as String).charAt(0) == "#")
            {
               config.context.prg = config.context.prg + "h";
            }
         }
         if(Logic.hasInstance && (Logic.instance.secondPreroll))
         {
            config.context.prg = config.context.prg + "s";
         }
         if(browserLanguage)
         {
            config.context.language = browserLanguage;
         }
         config.companionId = config.context.prg.indexOf("e") > -1?"DiscoveryRect":"PageRectangle";
         config.videoPlayerId = ads.mediaInfo.divId || "channelFlashContent";
         var _loc22_:* = 0;
         var _loc21_:* = config;
         for(_loc14_ in config)
         {
            Debug.echo(" >> " + _loc14_ + ": " + config[_loc14_]);
            if(_loc14_ == "context")
            {
               _loc20_ = 0;
               _loc19_ = config[_loc14_];
               for(_loc8_ in config[_loc14_])
               {
                  Debug.echo("    >> " + _loc8_ + ": " + config[_loc14_][_loc8_]);
               }
            }
         }
         isCompleted = false;
         playTime = 0;
         if(data.titlecard)
         {
            if(This.onSite(ads.media.pageUrl) & 2 && param1 == 0)
            {
               echo("titlecard");
               _loc3_ = {};
               _loc24_ = 0;
               _loc23_ = config;
               for(_loc14_ in config)
               {
                  _loc3_[_loc14_] = config[_loc14_];
               }
               _loc3_.forceTimer = data.forceTimer?data.forceTimer:false;
               ads.titlecardStart(_loc3_);
               onScriptFetched();
            }
            else
            {
               onAdFinished({"error":"adTitlecardNotSupported"});
            }
         }
         else
         {
            if(!ads.adHolder.contains(adPlayer))
            {
               ads.adHolder.addChild(adPlayer);
            }
            echo("adHolder dimension: " + [ads.adHolder.width,ads.adHolder.height]);
            try
            {
               adPlayer.setContentSize(ads.adHolder.width,ads.adHolder.height,false);
            }
            catch(e:Error)
            {
               echo("setPlayerSize() - " + e);
            }
            scriptFetchTimer.start();
            adPlayer.setMetadata(config);
            adPlayer.fetchScript();
            adPosition = param1;
            if(data.preCache)
            {
               echo("start precache timer");
               preCacheTimer = new Timer((data.preCache == 1?15:data.preCache) * 1000,1);
               preCacheTimer.addEventListener("timerComplete",startAd);
               preCacheTimer.start();
            }
         }
      }
      
      private function scriptFetchTimeout(param1:TimerEvent) : void {
         echo("scriptFetchTimeout");
         scriptFetchTimer.stop();
         if(!ads.errorWait || (ads.errorWait && data.type == "video"))
         {
            onAdFinished({"error":"scriptFetchTimeout"});
         }
         else if(adPosition == 1 && !(data.type == "video") && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
         {
            startCountdown(data.duration);
         }
         
      }
      
      function startRequestedAd() : void {
         echo("startRequestedAd()");
         startAd();
      }
      
      private function startAd(param1:Event = null) : void {
         echo("startAd()");
         if(preCacheTimer)
         {
            echo("destroy precache timer");
            preCacheTimer.stop();
            preCacheTimer.removeEventListener("timerComplete",startAd);
            preCacheTimer = null;
         }
         if(!isStartBreakCalled)
         {
            isStartBreakCalled = true;
            if(!adStartTimer)
            {
               adStartTimer = new Timer(data.type == "video"?30000:10000);
               adStartTimer.addEventListener("timer",onAdStartTimeout);
            }
            if(adStartTimer.running)
            {
               adStartTimer.stop();
            }
            adStartTimer.reset();
            adStartTimer.start();
            if(adPlayer && !data.titlecard)
            {
               echo("adHolder dimension: " + [ads.adHolder.width,ads.adHolder.height]);
               try
               {
                  adPlayer.setContentSize(ads.adHolder.width,ads.adHolder.height,false);
               }
               catch(e:Error)
               {
                  echo("setPlayerSize() - " + e);
               }
               echo("adPlayer.startBreak()");
               adPlayer.startBreak();
            }
         }
      }
      
      private function onScriptFetched(param1:Object = null) : void {
         echo("onScriptFetched() :: linearAdDuration: " + adPlayer.linearAdDuration + " - linearAdRemainingTime: " + adPlayer.linearAdRemainingTime + " - linearAdPlaying: " + adPlayer.linearAdPlaying);
         if(scriptFetchTimer && scriptFetchTimer.running)
         {
            scriptFetchTimer.stop();
         }
         if(!data.preCache)
         {
            startAd();
         }
      }
      
      private function onAdStartTimeout(param1:TimerEvent) : void {
         echo("onAdStartTimeout");
         if(adStartTimer)
         {
            adStartTimer.stop();
         }
         if(!ads.errorWait || (ads.errorWait && data.type == "video"))
         {
            onAdFinished({"error":"adStartTimeout"});
         }
         else if(adPosition == 1 && !(data.type == "video") && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
         {
            startCountdown(data.duration);
         }
         
      }
      
      public function onBreakStarted(param1:Object = null) : void {
         if(adStartTimer)
         {
            adStartTimer.stop();
         }
         echo("onBreakStarted()");
         if(!data.titlecard)
         {
            echo(" :: linearAdDuration: " + adPlayer.linearAdDuration + " - linearAdRemainingTime: " + adPlayer.linearAdRemainingTime + " - linearAdPlaying: " + adPlayer.linearAdPlaying);
            Debug.explore(param1);
            if((adPosition == 0 || (adPosition == 1 && data.type == "video")) && data.forceTimer && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
            {
               startCountdown(data.duration);
            }
         }
         adStatus = "playing";
         isAdShown = true;
         if(!data.withoutContentPlayStatusChange)
         {
            ads.pauseContent();
         }
         if(data.disableContentAudio)
         {
            ads.contentAudio = false;
         }
         ads.onVideoAdStarted(true,adPlayer.linearAdRemainingTime);
         if((adPosition == 0 || (adPosition == 1 && data.type == "video")) && data.forceTimer && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
         {
            startCountdown(data.duration);
         }
         ads.logAdEvent(
            {
               "eventName":"adShow",
               "host":"adaptv",
               "adType":(adPosition == 0?"preroll":"midroll")
            });
      }
      
      public function onBreakEnded(param1:Object = null) : void {
         if(adStartTimer)
         {
            adStartTimer.stop();
         }
         echo("onBreakEnded() - adPosition: " + adPosition + " - adType: " + data.type);
         Debug.explore(param1);
         if(adPosition == 0 || data.revShare || !data.revShare && adPosition == 1 && data.type == "video")
         {
            if(isAdShown)
            {
               isAdShown = false;
            }
            ads.onVideoAdFinished();
            if(!data.withoutContentPlayStatusChange)
            {
               ads.playContent();
            }
            if(data.disableContentAudio)
            {
               ads.contentAudio = true;
            }
            onAdFinished();
         }
         if(adPosition == 1 && !(data.type == "video") && !data.revShare)
         {
            adStatus = "playing";
            adPlayer.contentStarted();
            playheadTimer.start();
            overlayTimer = new Timer(35000,1);
            overlayTimer.addEventListener("timer",onOverlayTimeout);
            overlayTimer.start();
            if((adPosition == 1 && !(data.type == "video") || adPosition == 0 && data.forceTimer) && !(data.duration == null) && (data.duration) > 0 && adTimer == null)
            {
               startCountdown(data.duration);
            }
         }
      }
      
      private function onOverlayTimeout(param1:TimerEvent = null) : void {
         echo("onOverlayTimeout() :: e: " + param1);
         if(overlayTimer)
         {
            overlayTimer.stop();
            overlayTimer.removeEventListener("timer",onOverlayTimeout);
            overlayTimer = null;
         }
         if(param1 && !ads.errorWait)
         {
            onAdFinished({"error":"adStartTimeout"});
         }
      }
      
      public function onCustom(param1:Object) : void {
         echo("onCustom() :: e: " + param1);
         Debug.explore(param1);
      }
      
      public function onClickThru(param1:Object) : void {
         echo("onClickThru() - pause: " + param1.data.pause + " - url: " + param1.data.url);
         Debug.explore(param1);
         if(param1.data.pause)
         {
            ads.pauseContent();
         }
      }
      
      public function onLinearChanged(param1:Object) : void {
         echo("onLinearChanged() :: e: " + param1);
         Debug.explore(param1);
         if(param1 && param1.data && !(param1.data.linear == null))
         {
            if(param1.data.linear && adTimer && adTimer.running)
            {
               ads.pauseContent();
               ads.onVideoAdStarted();
               adTimer.stop();
            }
            if(!param1.data.linear && adTimer && !adTimer.running)
            {
               ads.onVideoAdFinished();
               ads.playContent();
               adTimer.start();
            }
         }
      }
      
      private function onCompanion(param1:Object) : void {
         echo("onCompanion() :: e: " + param1);
         Debug.explore(param1);
         ads.onCompanionTimer();
      }
      
      private function onNoCompanion(param1:Object) : void {
         echo("onNoCompanion() :: e: " + param1);
         Debug.explore(param1);
         ads.onCompanionTimer(new TimerEvent("timerComplete"));
      }
      
      private function onPaused(param1:Object) : void {
         echo("onPaused() :: e: " + param1);
         Debug.explore(param1);
      }
      
      private function onResumed(param1:Object) : void {
         echo("onResumed() :: e: " + param1);
         Debug.explore(param1);
      }
      
      private function onError(param1:Object) : void {
         echo("onError() :: e: " + param1);
         Debug.explore(param1);
      }
      
      private function onVolumeChanged(param1:Object) : void {
         echo("onVolumeChanged() :: e: " + param1);
         Debug.explore(param1);
      }
      
      private function onOverlayStarted(param1:Object) : void {
         echo("onOverlayStarted() :: e: " + param1);
         Debug.explore(param1);
         onOverlayTimeout();
         ads.onAdShown();
         ads.logAdEvent(
            {
               "eventName":"adShow",
               "host":"adaptv",
               "adType":"mid"
            });
      }
      
      private function onOverlayStopped(param1:Object) : void {
         echo("onOverlayStopped() :: e: " + param1);
         Debug.explore(param1);
         ads.onAdHidden();
      }
      
      private function onOverlayCollapsed(param1:Object) : void {
         echo("onOverlayCollapsed() :: e: " + param1);
         Debug.explore(param1);
      }
      
      private function onOverlayExpanded(param1:Object) : void {
         echo("onOverlayExpanded() :: e: " + param1);
         Debug.explore(param1);
      }
      
      function setVolume(param1:Number) : void {
         if(adPlayer)
         {
            adPlayer.setVolume(param1);
         }
      }
      
      function setHTMLSize(param1:Number, param2:Number, param3:Number, param4:Number) : void {
         if(adPlayer)
         {
            adPlayer.setHTMLSize(param1,param2,param3,param4);
         }
      }
      
      override public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
         if(enabled)
         {
            width = param1;
            height = param2;
            if(setSizeTimer)
            {
               if(setSizeTimer.running)
               {
                  setSizeTimer.reset();
               }
               setSizeTimer.start();
            }
         }
      }
      
      private function onSetSize(param1:TimerEvent) : void {
         setSizeTimer.stop();
         echo("onSetSize: " + [width,height]);
         if(width > 0 && height > 0 && adPlayer && isStartBreakCalled)
         {
            try
            {
               adPlayer.setContentSize(width,height,ads.adHolder?ads.adHolder.stage.displayState == "fullScreen":false);
            }
            catch(e:Error)
            {
               echo("setPlayerSize() - " + e);
            }
            echo("onSetSize() - w: " + width + " - h: " + height + " - " + [ads.adHolder.width,ads.adHolder.height]);
            width = 0;
            height = 0;
         }
         if(width > 0 && height > 0 && adPlayer && isStartBreakCalled)
         {
            return;
         }
      }
      
      private function onPlayheadChanged(param1:TimerEvent) : void {
         if(adPlayer)
         {
            playTime = playTime + 1;
            adPlayer.contentPlayheadChanged(playTime);
         }
      }
      
      private function onAdFinished(param1:Object = null) : void {
         if(!enabled)
         {
            return;
         }
         if(setSizeTimer)
         {
            setSizeTimer.stop();
            setSizeTimer.removeEventListener("timer",onSetSize);
            setSizeTimer = null;
         }
         var _loc2_:Boolean = param1 && param1.error?true:false;
         if(_loc2_)
         {
            ads.onCompanionTimer(new TimerEvent("timerComplete"));
         }
         echo("onAdFinished()" + (_loc2_?" - error: " + param1.error:""));
         adStatus = "completed";
         super.onTimer();
         if(_loc2_)
         {
            ads.onAdError();
         }
         else
         {
            ads.onAdComplete();
         }
      }
      
      function titlecardStatus(param1:String) : void {
         echo("titlecardStatus: " + param1);
         if(param1 == "end")
         {
            onBreakEnded();
         }
         else if(param1 == "error" || param1 == "timeout")
         {
            onAdFinished({"error":"adTitlecardError"});
         }
         else if(param1 == "start")
         {
            onBreakStarted();
         }
         
         
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         echo("onTimer()");
         if(!enabled)
         {
            return;
         }
         super.onTimer(param1);
         if((adPosition == 0 || (adPosition == 1 && data.type == "video")) && data.adDuration)
         {
            if(isAdShown)
            {
               isAdShown = false;
            }
            ads.onVideoAdFinished();
            ads.playContent();
         }
         if(!isCompleted)
         {
            isCompleted = true;
            ads.onAdComplete();
         }
      }
      
      override public function finished(param1:Boolean = false) : void {
         echo("finished() - isSlideShow: " + param1);
         destroyAd();
      }
      
      override public function destroyAd() : void {
         echo("destroyAd");
         playheadTimer.stop();
         scriptFetchTimer.stop();
         if(preCacheTimer)
         {
            preCacheTimer.stop();
            if(preCacheTimer.hasEventListener("timerComplete"))
            {
               preCacheTimer.removeEventListener("timerComplete",startAd);
            }
            preCacheTimer = null;
         }
         super.onTimer();
         if(overlayTimer)
         {
            onOverlayTimeout();
         }
         isAdShown = false;
         enabled = false;
         adStatus = "completed";
         if(adPlayer)
         {
            echo("remove eventlisteners");
            _loc3_ = 0;
            _loc2_ = eventList;
            for(_loc1_ in eventList)
            {
               if(adPlayer.hasEventListener(_loc1_))
               {
                  adPlayer.removeEventListener(_loc1_,eventList[_loc1_]);
               }
            }
            try
            {
               adPlayer.endBreak();
               adPlayer.contentEnded();
            }
            catch(e:Error)
            {
               echo("error on endBreak");
            }
            if(!(adPlayer == null) && (ads.adHolder.contains(adPlayer)))
            {
               ads.adHolder.removeChild(adPlayer);
            }
         }
         if(adPosition == 1 && !data.revShare)
         {
            ads.onAdRemoved();
         }
         if(adPlayer)
         {
            adPlayer.destroy();
            adPlayer = null;
         }
      }
      
      override protected function echo(param1:String, param2:int = -1) : void {
         super.echo("[" + currentIndex + "] " + param1,param2);
      }
   }
}
