package tv.ustream.viewer.logic.ads
{
   import flash.utils.Timer;
   import flash.display.Loader;
   import flash.events.EventDispatcher;
   import flash.display.MovieClip;
   import flash.events.Event;
   import com.adotube.events.AdotubeEvent;
   import flash.events.TimerEvent;
   import tv.ustream.tools.Debug;
   import flash.net.URLRequest;
   
   public class ProviderAdotube extends ProviderBase
   {
      
      public function ProviderAdotube(param1:Ads, param2:Object) {
         super();
         this.ads = param1;
         echo("constructor");
         adType = "adotube";
         this.data = param2;
         timeoutTimer = new Timer(25000,1);
         timeoutTimer.addEventListener("timer",onTimeout);
         if(!nopLoader)
         {
            nopLoader = new Loader();
            nopLoader.contentLoaderInfo.addEventListener("complete",onNopLoaded);
            nopLoader.contentLoaderInfo.addEventListener("ioError",onNopError);
            nopLoader.load(new URLRequest("http://chibis.adotube.com/overstreamPlatform/NOP.swf"));
         }
         else
         {
            onNopLoaded();
         }
      }
      
      protected static function serialize(param1:*) : String {
         var _loc2_:* = null;
         var _loc5_:* = typeof param1;
         if("object" !== _loc5_)
         {
            return param1;
         }
         _loc2_ = [];
         _loc5_ = 0;
         var _loc4_:* = param1;
         for(_loc3_ in param1)
         {
            _loc2_.push(_loc3_ + " = " + serialize(param1[_loc3_]));
         }
         return "[" + _loc2_.join("; ") + "]";
      }
      
      private var needRequest:Boolean = false;
      
      private var hasContent:uint = 2;
      
      private var timeoutTimer:Timer;
      
      private var nopLoader:Loader;
      
      private var nopDispatcher:EventDispatcher;
      
      private var nopMC:MovieClip;
      
      private var requestIsNeeded:Boolean = false;
      
      private var isControlsDisabled:Boolean = false;
      
      private var isVideoAdStarted:Boolean = false;
      
      private function onNopError(param1:Event) : void {
         echo("onNopError() :: " + param1.toString());
         nopLoader.contentLoaderInfo.removeEventListener("complete",onNopLoaded);
         nopLoader.contentLoaderInfo.removeEventListener("ioError",onNopError);
      }
      
      private function onNopLoaded(param1:Event = null) : void {
         echo("onNopLoaded()");
         nopLoader.contentLoaderInfo.removeEventListener("complete",onNopLoaded);
         nopLoader.contentLoaderInfo.removeEventListener("ioError",onNopError);
         nopDispatcher = nopLoader.contentLoaderInfo.sharedEvents;
         nopDispatcher.addEventListener("requestPlayerPause",onContentPause);
         nopDispatcher.addEventListener("requestPlayerPlay",onContentPlay);
         nopDispatcher.addEventListener("requestPlayerStream",onContentStream);
         nopDispatcher.addEventListener("requestPlayerDisableControls",onDisableControls);
         nopDispatcher.addEventListener("requestVideoResize",onVideoResize);
         nopDispatcher.addEventListener("registerContainerEvent",onContainerEvent);
         nopDispatcher.addEventListener("interactivePluginCompleted",onInteractivePluginCompleted);
         nopDispatcher.addEventListener("onOMLLoaded",onOmlLoaded);
         nopMC = MovieClip(nopLoader.content);
         if(nopMC && !ads.adHolder.contains(nopMC))
         {
            ads.adHolder.addChild(nopMC);
         }
         if(requestIsNeeded)
         {
            requestAd(1);
         }
      }
      
      private function onInteractivePluginCompleted(param1:*) : void {
         echo("onInteractivePluginCompleted()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
      }
      
      private function onContainerEvent(param1:*) : void {
         echo("onContainerEvent()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
         if(param1 && param1["type"] == "registerContainerEvent" && enabled)
         {
            if(param1["param"]["eventName"] == "Impression")
            {
               if(data.duration && (data.duration) > 0 && adTimer == null)
               {
                  startCountdown(data.duration);
               }
               ads.onAdShown();
               ads.logAdEvent(
                  {
                     "eventName":"adShow",
                     "host":"adotube",
                     "adType":"mid"
                  });
            }
            if(param1["param"]["eventName"] == "adVideoStart")
            {
               isVideoAdStarted = true;
               ads.onVideoAdStarted();
               if(adTimer && adTimer.running)
               {
                  adTimer.stop();
               }
            }
            if(param1["param"]["eventName"] == "Exit" || param1["param"]["eventName"] == "adVideoComplete")
            {
               isVideoAdStarted = false;
               ads.onAdHidden();
               ads.onVideoAdFinished();
               ads.playContent();
               ads.onAdComplete();
            }
         }
      }
      
      private function onVideoResize(param1:*) : void {
         echo("onVideoResize()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
      }
      
      private function onDisableControls(param1:*) : void {
         echo("onDisableControls()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
      }
      
      private function onContentStream(param1:*) : void {
         echo("onContentStream()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
      }
      
      private function onContentPlay(param1:*) : void {
         echo("onContentPlay()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
         ads.playContent(true);
         if(isVideoAdStarted)
         {
            isVideoAdStarted = false;
            ads.onVideoAdFinished();
         }
         if(adTimer && !adTimer.running)
         {
            adTimer.start();
         }
      }
      
      private function onContentPause(param1:*) : void {
         echo("onContentPause()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
         ads.pauseContent();
      }
      
      private function onOmlLoaded(param1:*) : void {
         echo("onOmlLoaded()");
         echo("Got event from NOP: " + param1["type"] + " -- " + serialize(param1["param"]));
         adStatus = "responsed";
         if(param1["param"] && !(param1["param"]["hasContent"] == null) && param1["param"]["hasContent"] == false)
         {
            onEvent("Error");
         }
      }
      
      override public function setData(param1:Object) : void {
         echo("setData()");
         this.data = param1;
      }
      
      override public function requestAd(param1:uint) : void {
         if(nopDispatcher && data && data.oml)
         {
            echo("requestAd() - oml: " + data.oml);
            nopDispatcher.dispatchEvent(new AdotubeEvent("onLayoutSizeChanged",
               {
                  "width":ads.adHolder.width,
                  "height":ads.adHolder.height
               }));
            nopDispatcher.dispatchEvent(new AdotubeEvent("playerDuration",{"duration":ads.duration}));
            nopDispatcher.dispatchEvent(new AdotubeEvent("loadOML",
               {
                  "omlSource":data.oml,
                  "pdv":{}
               }));
         }
         else
         {
            echo("requestAd() - nop is not loaded!");
            requestIsNeeded = true;
         }
      }
      
      private function onTimeout(param1:TimerEvent) : void {
         timeoutTimer.stop();
         super.onTimer();
         if(enabled && !ads.errorWait)
         {
            ads.onAdError();
         }
         else if(ads.errorWait && !adTimer && !(data.adDuration == null) && (data.adDuration) > 25)
         {
            startCountdown((data.adDuration) - 25);
         }
         
      }
      
      public function onEvent(param1:String) : void {
         Debug.echo("onEvent: " + param1);
         var _loc2_:* = param1;
         if("Available" !== _loc2_)
         {
            if("Impression" !== _loc2_)
            {
               if("Conversion" !== _loc2_)
               {
                  if("Exit" !== _loc2_)
                  {
                     if("contentPause" !== _loc2_)
                     {
                        if("contentPlay" !== _loc2_)
                        {
                           if("Error" === _loc2_)
                           {
                              echo("ERROR!!!");
                              adStatus = "completed";
                              super.onTimer();
                              ads.onAdError();
                           }
                        }
                        else
                        {
                           ads.onVideoAdFinished();
                           ads.playContent();
                           if(adTimer && !adTimer.running)
                           {
                              adTimer.reset();
                              adTimer.start();
                           }
                        }
                     }
                     else
                     {
                        ads.pauseContent();
                        ads.onVideoAdStarted();
                        if(adTimer && adTimer.running)
                        {
                           adTimer.stop();
                        }
                     }
                  }
                  else
                  {
                     ads.onAdHidden();
                     if(enabled)
                     {
                        ads.onAdComplete();
                     }
                  }
               }
            }
            else
            {
               timeoutTimer.stop();
               if(!adTimer && !(data.duration == null) && (data.duration) > 0)
               {
                  startCountdown(data.duration);
               }
               if(hasContent == 2)
               {
                  hasContent = 1;
               }
               ads.onAdShown();
               ads.logAdEvent(
                  {
                     "eventName":"adShow",
                     "host":"adotube",
                     "adType":"mid"
                  });
            }
         }
         else if(hasRespond == 2 && (needRequest))
         {
            echo("onEvent() :: NEEDS NEW REQUEST");
            needRequest = false;
            requestAd(adPosition);
            return;
         }
         
      }
      
      override public function destroyAd() : void {
         echo("destroyAd()");
         enabled = false;
         if(adTimer && adTimer.running)
         {
            adTimer.stop();
         }
         if(nopMC && ads.adHolder.contains(nopMC))
         {
            ads.adHolder.removeChild(nopMC);
         }
         if(nopDispatcher)
         {
            nopDispatcher.dispatchEvent(new AdotubeEvent("destroy"));
            nopDispatcher.removeEventListener("requestPlayerPause",onContentPause);
            nopDispatcher.removeEventListener("requestPlayerPlay",onContentPlay);
            nopDispatcher.removeEventListener("requestPlayerStream",onContentStream);
            nopDispatcher.removeEventListener("requestPlayerDisableControls",onDisableControls);
            nopDispatcher.removeEventListener("requestVideoResize",onVideoResize);
            nopDispatcher.removeEventListener("registerContainerEvent",onContainerEvent);
            nopDispatcher.removeEventListener("onOMLLoaded",onOmlLoaded);
         }
         nopDispatcher = null;
         nopMC = null;
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         echo("onTimer()");
         adTimer.stop();
         if(enabled)
         {
            ads.onAdComplete();
         }
      }
      
      override public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
         if(nopDispatcher)
         {
            nopDispatcher.dispatchEvent(new AdotubeEvent("onLayoutSizeChanged",
               {
                  "width":param1,
                  "height":param2
               }));
         }
      }
      
      override public function set contentStatus(param1:uint) : void {
         echo("contentStatus: " + param1);
         if(nopDispatcher)
         {
            nopDispatcher.dispatchEvent(new AdotubeEvent("playerStateChanged",{"playerState":(param1 == 1?"playing":param1 == 0?"paused":"stopped")}));
         }
      }
      
      override public function setPlayTime(param1:Number) : void {
         if(nopDispatcher)
         {
            nopDispatcher.dispatchEvent(new AdotubeEvent("PlaybackTimeChanged",{"playbackTime":param1}));
         }
      }
   }
}
