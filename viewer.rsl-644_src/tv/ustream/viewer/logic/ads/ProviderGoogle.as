package tv.ustream.viewer.logic.ads
{
   import com.google.ads.ima.api.AdsLoader;
   import com.google.ads.ima.api.AdsManager;
   import com.google.ads.ima.api.AdsRequest;
   import flash.utils.Timer;
   import flash.display.MovieClip;
   import tv.ustream.tools.This;
   import flash.net.URLVariables;
   import tv.ustream.tools.Debug;
   import com.google.ads.ima.api.AdErrorEvent;
   import com.google.ads.ima.api.AdsManagerLoadedEvent;
   import com.google.ads.ima.api.AdsRenderingSettings;
   import flash.events.TimerEvent;
   import com.google.ads.ima.api.AdEvent;
   import flash.display.DisplayObjectContainer;
   
   public class ProviderGoogle extends ProviderBase
   {
      
      public function ProviderGoogle(param1:Ads, param2:Object) {
         super();
         echo("creating...basic intialization");
         adType = "google";
         this.ads = param1;
         this.data = param2;
         index = index + 1;
         currentIndex = index;
         if(param1.adHolder)
         {
            width = Math.round(param1.adHolder.width);
            height = Math.round(param1.adHolder.height);
         }
         enabled = true;
      }
      
      private static var index:uint = 0;
      
      private var currentIndex:uint = 0;
      
      private var adsLoader:AdsLoader;
      
      private var adsManager:AdsManager;
      
      private var adsRequest:AdsRequest;
      
      private var isSiteViewer:Boolean = false;
      
      private var adProvider:String = "AdSense";
      
      private var setSizeTimer:Timer;
      
      private var timeoutTimer:Timer;
      
      private var startTimeoutTimer:Timer;
      
      private var oldWidth:uint = 0;
      
      private var oldHeight:uint = 0;
      
      private var contentPlayheadTime:uint = 0;
      
      override public function setData(param1:Object) : void {
         echo("setData()");
         this.data = param1;
      }
      
      override public function init(param1:MovieClip) : void {
         echo("init()");
         if(enabled)
         {
            if(This.onSite(ads.media.pageUrl) & 2)
            {
               isSiteViewer = true;
            }
            startTimeoutTimer = new Timer(10000,1);
            startTimeoutTimer.addEventListener("timer",onStartTimeout);
            setSizeTimer = new Timer(200,1);
            setSizeTimer.addEventListener("timer",onSetSize);
            if(!adsLoader)
            {
               echo("creating adsLoader");
               adsLoader = new AdsLoader();
               adsLoader.addEventListener("adsManagerLoaded",adsManagerLoadedHandler);
               adsLoader.addEventListener("adError",adsLoadErrorHandler);
            }
         }
      }
      
      override public function requestAd(param1:uint) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         if(enabled)
         {
            echo("requestAd()");
            if(adsRequest)
            {
               adsRequest = null;
            }
            _loc2_ = new URLVariables();
            _loc2_.ad_type = "image_text_flash";
            _loc2_.adsafe = "high";
            _loc2_.client = data.key;
            _loc2_.videoad_start_delay = "0";
            _loc2_.description_url = ads.media.pageUrl;
            _loc2_.overlay = "1";
            if(data.channels && data.channels.length)
            {
               _loc3_ = 0;
               while(_loc3_ < data.channels.length)
               {
                  if(!isSiteViewer && data.channels[_loc3_] == "5655041003")
                  {
                     data.channels[_loc3_] = "3447246221";
                     break;
                  }
                  _loc3_++;
               }
               _loc2_.channel = data.channels.join("+");
            }
            if(ads.media.pageUrl)
            {
               _loc2_.description_url = isSiteViewer?ads.mediaInfo.url:ads.media.pageUrl;
            }
            else
            {
               _loc2_.description_url = "";
            }
            Debug.explore(_loc2_);
            adsRequest = new AdsRequest();
            _loc4_ = Math.floor(ads.adHolder.width);
            adsRequest.nonLinearAdSlotWidth = _loc4_;
            adsRequest.linearAdSlotWidth = _loc4_;
            _loc4_ = Math.floor(ads.adHolder.height);
            adsRequest.nonLinearAdSlotHeight = _loc4_;
            adsRequest.linearAdSlotHeight = _loc4_;
            adsRequest.adTagUrl = "http://googleads.g.doubleclick.net/pagead/ads?" + _loc2_.toString();
            echo("adsRequest.adTagUrl: " + adsRequest.adTagUrl);
            adsLoader.requestAds(adsRequest);
         }
      }
      
      override public function setPlayerSize(param1:Number, param2:Number, param3:Boolean = false) : void {
         if(enabled && (!(oldWidth == param1) || !(oldHeight == param2)))
         {
            echo("setPlayerSize() :: " + [param1,param2]);
            oldWidth = param1;
            this.width = param1;
            oldHeight = param2;
            this.height = param2;
            if(setSizeTimer && setSizeTimer.running)
            {
               setSizeTimer.reset();
            }
            setSizeTimer.start();
         }
      }
      
      override public function forceControl(param1:String) : void {
         if(param1 == "play" && adsManager)
         {
            echo("forceControl()");
            adsManager.resume();
            adTimer.start();
         }
      }
      
      private function adsLoadErrorHandler(param1:AdErrorEvent) : void {
         destroyStartTimeoutTimer();
         if(enabled)
         {
            adFinished({"type":"adsLoader.error"});
         }
      }
      
      private function adsManagerLoadedHandler(param1:AdsManagerLoadedEvent) : void {
         e = param1;
         destroyStartTimeoutTimer();
         if(enabled)
         {
            adsRenderingSettings = new AdsRenderingSettings();
            contentPlayhead = {};
            contentPlayhead.time = function():Number
            {
               return contentPlayheadTime * 1000;
            };
            adsManager = e.getAdsManager(contentPlayhead,adsRenderingSettings);
            if(adsManager)
            {
               adsManager.addEventListener("allAdsCompleted",allAdsCompletedHandler);
               adsManager.addEventListener("contentPauseRequested",contentPauseRequestedHandler);
               adsManager.addEventListener("contentResumeRequested",contentResumeRequestedHandler);
               adsManager.addEventListener("started",startedHandler);
               adsManager.addEventListener("adError",adsManagerPlayErrorHandler);
               adsManager.handshakeVersion("1.0");
               adsManager.init(ads.adHolder.width,ads.adHolder.height,"normal");
               ads.adHolder.addChild(adsManager.adsContainer);
               adsManager.start();
            }
         }
      }
      
      private function onStartTimeout(param1:TimerEvent) : void {
         destroyStartTimeoutTimer();
         if(enabled)
         {
            echo("onStartTimeout()");
            hasRespond = 0;
            adStatus = "timeout";
            adFinished({"type":"adStart.timeout.error"});
         }
      }
      
      override public function onTimer(param1:TimerEvent = null) : void {
         if(enabled)
         {
            echo("onTimer()");
            super.onTimer(param1);
            adStatus = "completed";
            ads.onAdRemoved();
            ads.onAdComplete();
         }
      }
      
      private function allAdsCompletedHandler(param1:AdEvent) : void {
         if(enabled)
         {
            echo("allAdsCompletedHandler()");
            adFinished();
         }
      }
      
      private function contentPauseRequestedHandler(param1:AdEvent) : void {
         if(enabled)
         {
            echo("contentPauseRequestedHandler()");
            ads.media.pause();
         }
      }
      
      private function contentResumeRequestedHandler(param1:AdEvent) : void {
         if(enabled)
         {
            echo("contentResumeRequestedHandler()");
            ads.media.play();
         }
      }
      
      private function startedHandler(param1:AdEvent) : void {
         if(enabled)
         {
            echo("startedHandler()");
            destroyStartTimeoutTimer();
            if(data.duration && (data.duration) > 0 && adTimer == null)
            {
               startCountdown(data.duration);
            }
         }
      }
      
      private function adsManagerPlayErrorHandler(param1:AdErrorEvent) : void {
         if(enabled)
         {
            echo("adsManagerPlayErrorHandler()");
            adFinished({"type":"adsManager.play.error"});
         }
      }
      
      override public function destroyAd() : void {
         echo("destroyAd() - enabled: " + enabled);
         if(enabled)
         {
            enabled = false;
            destroyStartTimeoutTimer();
            super.onTimer();
            adsRequest = null;
            if(adsManager)
            {
               adsManager.removeEventListener("allAdsCompleted",allAdsCompletedHandler);
               adsManager.removeEventListener("contentPauseRequested",contentPauseRequestedHandler);
               adsManager.removeEventListener("contentResumeRequested",contentResumeRequestedHandler);
               adsManager.removeEventListener("started",startedHandler);
               adsManager.removeEventListener("adError",adsManagerPlayErrorHandler);
               if(adsManager.adsContainer && adsManager.adsContainer.parent)
               {
                  echo(" .. remove adsContainer");
                  adsManager.adsContainer.parent.removeChild(adsManager.adsContainer);
               }
               adsManager.destroy();
            }
            if(adsLoader)
            {
               echo(" .. adsLoader cleanup");
               adsLoader.removeEventListener("adsManagerLoaded",adsManagerLoadedHandler);
               adsLoader.removeEventListener("adError",adsLoadErrorHandler);
               adsLoader = null;
            }
         }
      }
      
      private function destroyStartTimeoutTimer() : void {
         if(startTimeoutTimer)
         {
            echo("destroyStartTimeoutTimer()");
            startTimeoutTimer.stop();
            startTimeoutTimer.removeEventListener("timer",onStartTimeout);
            startTimeoutTimer = null;
         }
      }
      
      private function adFinished(param1:Object = null) : void {
         if(enabled)
         {
            super.onTimer();
            adStatus = "completed";
            echo("onAdFinished()");
            if(param1)
            {
               echo("ERROR: " + param1.type);
               ads.onAdError();
            }
            else
            {
               ads.onAdComplete();
            }
         }
      }
      
      private function onSetSize(param1:TimerEvent) : void {
         echo("onSetSize() - width: " + width + " - height: " + height);
         if(enabled)
         {
            if(setSizeTimer && setSizeTimer.running)
            {
               setSizeTimer.stop();
            }
            if(width > 0 && height > 0 && adsManager)
            {
               echo("resize adsManager");
               adsManager.resize(width,height,"normal");
               width = 0;
               height = 0;
            }
         }
      }
      
      override protected function echo(param1:String, param2:int = -1) : void {
         super.echo("[" + currentIndex + "] " + param1,param2);
      }
   }
}
