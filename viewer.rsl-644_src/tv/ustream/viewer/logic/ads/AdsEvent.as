package tv.ustream.viewer.logic.ads
{
   public class AdsEvent extends Object
   {
      
      public function AdsEvent() {
         super();
      }
      
      public static const ADS_INIT:String = "adsInit";
      
      public static const ADS_DATA:String = "adsData";
      
      public static const ADS_MEDIA_DURATION:String = "adsMediaDuration";
      
      public static const ADS_MEDIA_PARAM:String = "adsMediaParam";
      
      public static const ADS_MEDIA_STATUS:String = "adsMediaStatus";
      
      public static const ADS_ADHOLDER:String = "adsAdHolder";
      
      public static const ADS_DIVID:String = "adsDivId";
      
      public static const ADS_START:String = "adsStart";
      
      public static const ADS_REVSHARE:String = "adsRevshare";
      
      public static const ADS_PLAY_MIDROLL:String = "adsPlayMidroll";
      
      public static const ADS_RESUME:String = "adsResume";
      
      public static const ADS_LINEAR_FEED_AD_BREAK:String = "adsLinearFeedAdBreak";
      
      public static const TITLECARD_STATUS:String = "titlecardStatus";
      
      public static const ADS_PAUSE_CONTENT:String = "adsPauseContent";
      
      public static const ADS_PLAY_CONTENT:String = "adsPlayContent";
      
      public static const ADS_VIDEO_AD_STARTED:String = "adsVideoAdStarted";
      
      public static const ADS_VIDEO_AD_FINISHED:String = "adsVideoAdFinished";
      
      public static const ADS_FINISHED:String = "adsFinished";
      
      public static const ADS_BLOCK_STATUS:String = "adsBlockStatus";
      
      public static const ADS_LOG_AD_EVENT:String = "adsLogAdEvent";
      
      public static const ADS_MIDROLL_NOTIFICATION:String = "adsMidrollNotification";
      
      public static const ADS_AD_HEIGHT:String = "adsAdHeight";
      
      public static const ADS_CONTROLBAR_STATUS:String = "adsControlbarStatus";
      
      public static const ADS_PAUSE:String = "adsPause";
      
      public static const ADS_PREROLL_DONE:String = "adsPrerollDone";
      
      public static const ADS_CONTENT_AUDIO:String = "adsContentAudio";
      
      public static const ADS_COMPANION:String = "adsCompanion";
      
      public static const TITLECARD_START:String = "titlecardStart";
   }
}
