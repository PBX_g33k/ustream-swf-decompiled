package tv.ustream.viewer.logic.ads
{
   import flash.display.Sprite;
   import flash.events.Event;
   import tv.ustream.localization.Locale;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.Logic;
   import flash.text.TextFormat;
   import flash.text.TextField;
   import flash.geom.Matrix;
   import tv.ustream.tools.Debug;
   
   public class AdSupportMessage extends Sprite
   {
      
      public function AdSupportMessage(param1:uint, param2:uint) {
         super();
         _width = param1;
         _height = param2;
         if(stage)
         {
            onAddedToStage();
         }
         else
         {
            addEventListener("addedToStage",onAddedToStage);
         }
      }
      
      static const HEIGHT:uint = 32;
      
      private var tfList:Array;
      
      private var tfHolder:Sprite;
      
      private var tfMask:Sprite;
      
      private var _message:String;
      
      private var _width:uint = 0;
      
      private var _height:uint = 0;
      
      private function onAddedToStage(param1:Event = null) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         removeEventListener("addedToStage",onAddedToStage);
         var _loc4_:String = Locale.hasInstance?This.getReference(Logic.instance,"media.modules.meta.data.userName") as String:"";
         if(!_loc4_)
         {
            _loc4_ = "broadcaster user";
         }
         _message = Locale.hasInstance?Locale.instance.label("labelAdSupportMessage",{"user":_loc4_}):"This advertisement directly supports " + _loc4_ + ".";
         if(_message == "!labelAdSupportMessage")
         {
            _message = "This advertisement directly supports " + _loc4_ + ".";
         }
         tfList = [];
         var _loc5_:TextFormat = new TextFormat("Arial",13,3377407);
         tfHolder = new Sprite();
         _loc3_ = 0;
         while(_loc3_ < 2)
         {
            _loc2_ = new TextField();
            _loc2_.autoSize = "left";
            _loc2_.cacheAsBitmap = true;
            _loc2_.selectable = false;
            _loc2_.text = _message;
            _loc2_.setTextFormat(_loc5_);
            tfList.push(_loc2_);
            tfHolder.addChild(_loc2_);
            _loc3_++;
         }
         tfList[1].visible = false;
         tfMask = new Sprite();
         addChild(tfHolder);
         addChild(tfMask);
         tfHolder.mask = tfMask;
         drawMask();
         drawBackground();
         checkScroll();
         tfHolder.x = _width - tfHolder.width >> 1;
         tfHolder.y = _height - tfHolder.height >> 1;
      }
      
      private function drawMask() : void {
         tfMask.graphics.clear();
         var _loc1_:Matrix = new Matrix();
         _loc1_.createGradientBox(16,_height,0,-16);
         tfMask.graphics.beginGradientFill("linear",[0,0],[0,1],[0,255],_loc1_);
         tfMask.graphics.drawRect(-16,0,16,_height);
         tfMask.graphics.endFill();
         tfMask.graphics.beginFill(16737792,1);
         tfMask.graphics.drawRect(0,0,_width - 16,_height);
         tfMask.graphics.endFill();
         _loc1_ = new Matrix();
         _loc1_.createGradientBox(16,_height,0,_width - 16);
         tfMask.graphics.beginGradientFill("linear",[0,0],[1,0],[0,255],_loc1_);
         tfMask.graphics.drawRect(_width - 16,0,16,_height);
         tfMask.graphics.endFill();
      }
      
      private function drawBackground() : void {
         graphics.clear();
         graphics.beginFill(1315860);
         graphics.drawRect(0,0,_width,_height);
         graphics.endFill();
      }
      
      private function checkScroll() : void {
         if(tfList[0].width > _width)
         {
            if(!hasEventListener("enterFrame"))
            {
               tfList[1].visible = true;
               addEventListener("enterFrame",scrollText);
               _loc1_ = 0;
               tfMask.x = _loc1_;
               tfHolder.x = _loc1_;
            }
         }
         else if(hasEventListener("enterFrame"))
         {
            removeEventListener("enterFrame",scrollText);
            tfList[1].visible = false;
            tfList[0].x = 0;
            _loc1_ = _width - tfList[0].width >> 1;
            tfMask.x = _loc1_;
            tfHolder.x = _loc1_;
            _loc1_ = _height - tfList[0].height >> 1;
            tfMask.x = _loc1_;
            tfHolder.y = _loc1_;
         }
         
      }
      
      private function scrollText(param1:Event = null) : void {
         var _loc2_:* = tfList[0].x - 1;
         tfList[0].x--;
         tfList[0].x = _loc2_ % (tfList[0].width + 16);
         tfList[1].x = tfList[0].x + tfList[0].width + 16;
      }
      
      function resize(param1:uint, param2:uint) : void {
         if(!(param1 == _width) || !(param2 == _height))
         {
            _width = param1;
            _height = param2;
            drawBackground();
            drawMask();
            checkScroll();
            if(tfList[0].width < _width)
            {
               _loc3_ = _width - tfList[0].width >> 1;
               tfMask.x = _loc3_;
               tfHolder.x = _loc3_;
               _loc3_ = _height - tfList[0].height >> 1;
               tfMask.x = _loc3_;
               tfHolder.y = _loc3_;
            }
            if(!hasEventListener("enterFrame"))
            {
               tfList[0].x = 0;
            }
         }
      }
      
      function destroy() : void {
         var _loc1_:* = 0;
         if(hasEventListener("enterFrame"))
         {
            removeEventListener("enterFrame",scrollText);
         }
         if(tfHolder)
         {
            if(tfList)
            {
               _loc1_ = 0;
               while(_loc1_ < 2)
               {
                  if(tfHolder.contains(tfList[_loc1_]))
                  {
                     tfHolder.removeChild(tfList[_loc1_]);
                     tfList[_loc1_] = null;
                  }
                  _loc1_++;
               }
            }
            if(contains(tfHolder))
            {
               removeChild(tfHolder);
            }
            tfHolder.mask = null;
            tfHolder = null;
         }
         if(tfMask)
         {
            if(contains(tfMask))
            {
               removeChild(tfMask);
            }
            tfMask = null;
         }
         tfList = null;
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ AdsSupportMessage ] " + param1);
      }
   }
}
