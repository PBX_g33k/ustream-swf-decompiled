package tv.ustream.viewer.logic.ads
{
   import tv.ustream.tools.Dispatcher;
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.utils.Timer;
   import flash.display.Loader;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.Debug;
   import flash.events.Event;
   import tv.ustream.viewer.logic.Logic;
   import flash.system.LoaderContext;
   import tv.ustream.tools.UncaughtErrorHandler;
   import flash.events.IEventDispatcher;
   import flash.net.URLRequest;
   import flash.utils.getQualifiedClassName;
   import flash.events.ErrorEvent;
   import flash.events.TimerEvent;
   import tv.ustream.localization.Locale;
   import flash.system.Security;
   
   public class Ads extends Dispatcher
   {
      
      public function Ads() {
         mediaEventList = 
            {
               "adsInit":onAdsInit,
               "adsData":onAdsData,
               "adsMediaDuration":onAdsMediaDuration,
               "adsMediaParam":onAdsMediaParams,
               "adsMediaStatus":onAdsMediaStatus,
               "adsAdHolder":onAdsHolder,
               "adsDivId":onAdsDivId,
               "adsStart":onAdsStart,
               "adsRevshare":onAdsRevShare,
               "adsPlayMidroll":onAdsPlayMidroll,
               "adsResume":onAdsResume,
               "adsLinearFeedAdBreak":onLinearFeedAdBreak,
               "titlecardStatus":onTitlecardStatus,
               "viewMode":onViewModeChange
            };
         availableProviderList = ["yahooim","adotube","delay","googleads","adaptv","notification"];
         customApiLoadingList = ["yahooim","adotube","delay","googleads"];
         _mediaInfo = {};
         Security.allowDomain("*");
         Security.allowInsecureDomain("*");
         super("ads");
         echo("created");
      }
      
      public static const PRE:uint = 0;
      
      public static const MID:uint = 1;
      
      public static const POST:uint = 2;
      
      public static const PARTNER:uint = 3;
      
      public static const LINEARFEED:uint = 4;
      
      public static const DEFAULT_PARTNER_DELAY:uint = 900;
      
      private var mediaEventList:Object;
      
      private var availableProviderList:Array;
      
      private var customApiLoadingList:Array;
      
      private var _data:Array;
      
      private var adIndex:int;
      
      private var adPosition:uint;
      
      private var videoAdBg:Sprite;
      
      private var currentAdSource:Object;
      
      private var currentAdObj:ProviderBase;
      
      private var currentBlockArguments:Object;
      
      private var libList:Array;
      
      private var _media:Recorded;
      
      private var _mediaInfo:Object;
      
      private var _adHolder:AdHolder;
      
      private var _started:Boolean = false;
      
      private var _revShare:Object;
      
      private var _duration:Number;
      
      private var _divId:String;
      
      private var _status:uint;
      
      private var _mediaStatus:uint = 1;
      
      private var isAdBlockPlaying:Boolean = false;
      
      private var midrollNotification:Object;
      
      private var midrollNotificationTimer:Timer;
      
      private var dying:Boolean = false;
      
      private var embedSize:Object;
      
      private var loader:Loader;
      
      private var isPaused:Boolean = false;
      
      private var isForcePlay:Boolean = false;
      
      private var _contentStatus:String = "";
      
      private var _contentDuration:String = "";
      
      private var _clientIp:String;
      
      private var _adaptvParams:Object;
      
      private var disabledOverlays:Boolean = false;
      
      private var disabledOverlayIndex:uint = 0;
      
      private var isVideoAdInMid:Boolean = false;
      
      private var adSupportMessage:AdSupportMessage;
      
      private var _contentAudio:Boolean = true;
      
      private var companionTimer:Timer;
      
      var errorWait:Boolean = false;
      
      public function get media() : Recorded {
         return _media;
      }
      
      public function set media(param1:Recorded) : void {
         echo("set media()");
         _media = param1;
         var _loc4_:* = 0;
         var _loc3_:* = mediaEventList;
         for(_loc2_ in mediaEventList)
         {
            _media.addEventListener(_loc2_,mediaEventList[_loc2_]);
         }
      }
      
      function get adHolder() : AdHolder {
         return _adHolder;
      }
      
      private function onAdsHolder(param1:DynamicEvent) : void {
         echo("onAdsHolder()");
         _adHolder = param1.data.adHolder as AdHolder;
         videoAdBg = new Sprite();
         _adHolder.addChild(new Sprite());
         videoAdBg.visible = false;
         drawVideoAdBg();
      }
      
      function get duration() : Number {
         return _duration;
      }
      
      private function onAdsMediaDuration(param1:DynamicEvent) : void {
         echo("onAdsMediaDuration()");
         _duration = param1.data.duration;
      }
      
      private function onAdsMediaParams(param1:DynamicEvent) : void {
         echo("onAdsMediaParams()");
         Debug.explore(param1.data);
      }
      
      function get mediaStatus() : Number {
         return _mediaStatus;
      }
      
      private function onAdsMediaStatus(param1:DynamicEvent) : void {
         var _loc2_:* = NaN;
         echo("onAdsMediaStatus()");
         if(_mediaStatus != param1.data.playStatus)
         {
            _mediaStatus = param1.data.playStatus;
            if(currentAdObj && currentAdObj.adType == "adotube")
            {
               currentAdObj.contentStatus = _mediaStatus;
            }
            if(_mediaStatus == 1 && (isPaused))
            {
               onAdsResume();
            }
            if(midrollNotification && midrollNotificationTimer)
            {
               if(_mediaStatus && !midrollNotificationTimer.running)
               {
                  midrollNotificationTimer.start();
               }
               if(!_mediaStatus && (midrollNotificationTimer.running))
               {
                  _loc2_ = midrollNotificationTimer.repeatCount - midrollNotificationTimer.currentCount;
                  midrollNotificationTimer.reset();
                  midrollNotificationTimer.repeatCount = _loc2_;
               }
            }
         }
      }
      
      private function onAdsDivId(param1:DynamicEvent) : void {
         echo("onAdsDivId()");
         _mediaInfo.divId = param1.data.divId;
      }
      
      private function onAdsData(param1:DynamicEvent) : void {
         var _loc4_:* = 0;
         var _loc5_:* = null;
         var _loc6_:* = 0;
         var _loc3_:* = null;
         echo("onAdsData()");
         var _loc2_:Object = param1.data.adData;
         if(_active)
         {
            if(_loc2_.revshare)
            {
               onAdsRevShare(new DynamicEvent("adsRevshare",false,false,_loc2_));
            }
            else
            {
               _data = [];
               if(_loc2_.info)
               {
                  _loc9_ = 0;
                  _loc8_ = _loc2_.info;
                  for(_loc7_ in _loc2_.info)
                  {
                     _mediaInfo[_loc7_] = _loc2_.info[_loc7_];
                  }
               }
               if(_loc2_.pre && _loc2_.pre.length)
               {
                  _data[0] = [];
                  _loc4_ = 0;
                  while(_loc4_ < _loc2_.pre.length)
                  {
                     if(!(availableProviderList.indexOf(_loc2_.pre[_loc4_].provider) == -1) && !(_loc2_.pre[_loc4_].provider == "googleads"))
                     {
                        _data[0].push(_loc2_.pre[_loc4_]);
                     }
                     else
                     {
                        echo("onAdsData() - ERROR - invalid provider: " + _loc2_.pre[_loc4_].provider + " - skipped");
                     }
                     _loc4_++;
                  }
                  if(_data[0].length > 1)
                  {
                     createBlockOnList(_data[0]);
                  }
               }
               if(_loc2_.mid && _loc2_.mid.length)
               {
                  _data[1] = [];
                  _loc6_ = -1;
                  _loc4_ = 0;
                  while(_loc4_ < _loc2_.mid.length)
                  {
                     if(availableProviderList.indexOf(_loc2_.mid[_loc4_].provider) != -1)
                     {
                        _loc5_ = _loc2_.mid[_loc4_];
                        if(_loc5_.type == "video" && _loc5_.provider == "googleads")
                        {
                           _loc5_.type = "overlay";
                        }
                        if(_loc5_.type == "overlay")
                        {
                           _loc5_.errors = 0;
                        }
                        if(_loc5_.errorWait == true)
                        {
                           errorWait = true;
                        }
                        if(_loc5_.block)
                        {
                           if(_loc5_.block != _loc6_)
                           {
                              if(_loc6_ != -1)
                              {
                                 _data[1][_data[1].length - 1].blockEnd = true;
                              }
                              _loc6_ = _loc5_.block;
                              _loc5_.blockStart = true;
                           }
                           else if(_loc4_ == _loc2_.mid.length - 1)
                           {
                              _loc5_.blockEnd = true;
                           }
                           
                        }
                        else if(_loc6_ != -1)
                        {
                           _data[1][_data[1].length - 1].blockEnd = true;
                           _loc6_ = -1;
                        }
                        
                        if(_loc5_.lfeed)
                        {
                           if(!_data[4])
                           {
                              _data[4] = [];
                           }
                           _data[4].push(_loc5_);
                        }
                        else
                        {
                           _data[1].push(_loc5_);
                        }
                     }
                     else
                     {
                        echo("onAdsData() - ERROR - invalid provider: " + _loc2_.mid[_loc4_].provider + " - skipped");
                     }
                     _loc4_++;
                  }
               }
               if(_loc2_.post && _loc2_.post.length)
               {
                  _data[2] = [];
                  _loc4_ = 0;
                  while(_loc4_ < _loc2_.post.length)
                  {
                     if(availableProviderList.indexOf(_loc2_.post[_loc4_].provider) != -1)
                     {
                        _data[2].push(_loc2_.post[_loc4_]);
                     }
                     else
                     {
                        echo("onAdsData() - ERROR - invalid provider: " + _loc2_.post[_loc4_].provider + " - skipped");
                     }
                     _loc4_++;
                  }
                  if(_data[2].length > 1)
                  {
                     createBlockOnList(_data[2]);
                  }
               }
               if(_loc2_.partner && _loc2_.partner.length)
               {
                  _data[3] = [];
                  _loc4_ = 0;
                  while(_loc4_ < _loc2_.partner.length)
                  {
                     if(availableProviderList.indexOf(_loc2_.partner[_loc4_].provider) != -1)
                     {
                        _data[3].push(_loc2_.partner[_loc4_]);
                     }
                     else
                     {
                        echo("onAdsData() - ERROR - invalid provider: " + _loc2_.partner[_loc4_].provider + " - skipped");
                     }
                     _loc4_++;
                  }
                  if(_data[3].length > 1)
                  {
                     createBlockOnList(_data[3]);
                  }
                  if(_media && _media.type == "recorded")
                  {
                     _loc3_ = 
                        {
                           "duration":900,
                           "provider":"delay",
                           "rule":1
                        };
                     _data[1] = [];
                     _data[1].push(_loc3_);
                     _data[1].push(_data[3][0]);
                  }
               }
               echo("advert object -----------------------------------------------------");
               Debug.explore(_data);
               echo("-------------------------------------------------------------------");
            }
         }
      }
      
      private function createBlockOnList(param1:Array) : void {
         param1[0].blockStart = true;
         param1[param1.length - 1].blockEnd = true;
      }
      
      private function onAdsInit(param1:DynamicEvent) : void {
         echo("onAdsInit()");
         libList = [];
         _duration = 0;
         adPosition = 0;
         adIndex = -1;
         _started = false;
         _active = true;
         _revShare = null;
         Debug.explore(param1.data);
         if(param1.data.adaptvParams)
         {
            _adaptvParams = param1.data.adaptvParams;
         }
         if(param1.data.clientIp)
         {
            _clientIp = param1.data.clientIp;
            echo("clientIp: " + _clientIp);
         }
      }
      
      public function get started() : Boolean {
         return _started;
      }
      
      private function onAdsStart(param1:Event) : void {
         if(_active && !_revShare)
         {
            echo("onAdsStart()");
            _started = true;
            adPosition = _data[0] && _data[0].length?0:1;
            if(adPosition == 1)
            {
               _media.onPrerollDone();
            }
            prepairNextAd();
         }
      }
      
      function get mediaInfo() : Object {
         return _mediaInfo;
      }
      
      private function prepairNextAd() : void {
         var _loc1_:* = 0;
         if(_active)
         {
            if(!_data)
            {
               if(currentAdObj)
               {
                  destroyCurrentAd();
               }
               return;
            }
            if(currentAdSource && (currentAdSource.type == "video" || currentAdSource.type == "fullscreen"))
            {
               if(isAdBlockPlaying)
               {
                  currentBlockArguments.index++;
               }
               if(!_media.playing)
               {
                  playContent();
               }
               if(videoAdBg.visible)
               {
                  onVideoAdFinished();
               }
            }
            if(currentAdObj)
            {
               destroyCurrentAd();
            }
            _loc1_ = 0;
            if(_data[adPosition])
            {
               _loc1_ = _data[adPosition].length;
            }
            echo("prepairNextAd() - adPosition: " + adPosition + " - adIndex: " + adIndex + " - length of ads in current pos: " + _loc1_);
            if(Logic.hasInstance && (Logic.instance.overlay))
            {
               isPaused = true;
               echo("skip");
               dispatch("adsPause",false,{"reason":1});
               return;
            }
            adIndex = adIndex + 1;
            if(_data[adPosition] && adIndex >= _data[adPosition].length)
            {
               if(_revShare && _revShare.type == "adaptv")
               {
                  _revShare = null;
                  return;
               }
               adIndex = 0;
               if(adPosition == 0)
               {
                  _media.onPrerollDone();
                  adPosition = 1;
               }
               if(adPosition == 2)
               {
                  echo("prepairNextAd() - postrolls have ended");
                  return;
               }
            }
            if(_data[adPosition] && _data[adPosition].length && _data[adPosition][adIndex])
            {
               if(disabledOverlays && adPosition == 1)
               {
                  if(disabledOverlayIndex == adIndex && disabledOverlayIndex > -1 && !isVideoAdInMid)
                  {
                     echo("jaDE stops playing ads");
                     return;
                  }
                  if(disabledOverlayIndex == -1)
                  {
                     echo("jaDE disabledOverlayIndex was -1, reset it to 0");
                     disabledOverlayIndex = 0;
                  }
                  if(_data[1][adIndex].type == "video" || _data[1][adIndex].provider == "delay")
                  {
                     echo("jaDE found video or delay: " + [_data[1][adIndex].type,_data[1][adIndex].provider]);
                     isVideoAdInMid = true;
                  }
                  else if(!_data[1][adIndex].type || _data[1][adIndex].type == "overlay")
                  {
                     echo("jaDE skip this overlay");
                     prepairNextAd();
                     return;
                  }
                  
               }
               if(adPosition == 1 && _data[adPosition][adIndex].type == "video" && _mediaStatus == 0 && !_revShare && !isAdBlockPlaying)
               {
                  adIndex = adIndex - 1;
                  isPaused = true;
               }
               else
               {
                  echo("prepairNextAd() - adPosition: " + adPosition + " - adIndex: " + adIndex);
                  createAdvert(_data[adPosition][adIndex]);
               }
            }
            else
            {
               echo("prepairNextAd() - there is no ad in this position: " + adPosition);
            }
         }
      }
      
      private function hasAvailableMid() : Boolean {
         var _loc2_:* = 0;
         var _loc1_:* = false;
         if(_data[1])
         {
            _loc2_ = 0;
            while(_loc2_ < _data[1].length)
            {
               if(_data[1][_loc2_].errors && _data[1][_loc2_].errors < 5 && !(_data[1][_loc2_].type == "delay"))
               {
                  _loc1_ = true;
               }
               _loc2_++;
            }
         }
         echo("hasAvailableMid() - result: " + _loc1_);
         return _loc1_;
      }
      
      private function createAdvert(param1:Object, param2:Boolean = false) : void {
         var _loc4_:* = null;
         var _loc3_:* = 0;
         var _loc6_:* = 0;
         var _loc5_:* = null;
         var _loc7_:* = null;
         var _loc8_:* = null;
         echo("createAdvert() - provider: " + param1.provider + " - type: " + param1.type);
         currentAdSource = param1;
         if(!embedSize)
         {
            _loc4_ = [300,240,180,100,30,10,1];
            embedSize = {};
            embedSize.available = true;
            echo("checking embed size... " + [_adHolder,_adHolder.stage,_adHolder.stage.stageWidth,_adHolder.stage.stageHeight]);
            if(!_adHolder || !_adHolder.stage)
            {
               echo("there is no adHolder or stage of adHolder");
               embedSize.available = false;
               embedSize.error = true;
            }
            else if(_adHolder.stage.stageWidth < 300 || _adHolder.stage.stageHeight < 180)
            {
               echo("dimension of flash app is smaller than 300px wide or 180px high");
               embedSize.available = false;
            }
            
            if(!embedSize.available && !embedSize.error)
            {
               _loc3_ = 0;
               embedSize.width = "OK";
               embedSize.height = "OK";
               _loc6_ = 0;
               while(_loc6_ < _loc4_.length)
               {
                  if(_adHolder.stage.stageWidth <= _loc4_[_loc6_])
                  {
                     embedSize.width = _loc4_[_loc6_];
                  }
                  if(_loc6_ > 1 && _adHolder.stage.stageHeight <= _loc4_[_loc6_])
                  {
                     embedSize.height = _loc4_[_loc6_];
                  }
                  _loc6_++;
               }
               echo("the result: " + [embedSize.width,embedSize.height,embedSize.width + "x" + embedSize.height]);
            }
            Debug.explore(embedSize);
         }
         if(embedSize.available || param1.provider == "yahooim" || param1.provider == "delay")
         {
            if(!_revShare && adPosition == 1 && _data && _data[adPosition][adIndex] && _data[adPosition][adIndex].errors > 4)
            {
               if(hasAvailableMid())
               {
                  prepairNextAd();
               }
               else
               {
                  echo("ERROR: there is no available ad in mid position!");
               }
               return;
            }
            if(adPosition == 1 && currentAdSource.type == "video" && currentAdSource.delay && (currentAdSource.delay) > 5 && !param2)
            {
               _status = 9;
               midrollNotification = 
                  {
                     "duration":currentAdSource.delay,
                     "time":0,
                     "start":true
                  };
               if(currentAdSource.offeringText)
               {
                  midrollNotification.customOffering = {"ad":currentAdSource.offeringText};
                  if(currentAdSource.offeringLink)
                  {
                     midrollNotification.customOffering.link = currentAdSource.offeringLink;
                  }
                  if(currentAdSource.offeringUrl)
                  {
                     midrollNotification.customOffering.url = currentAdSource.offeringUrl;
                  }
               }
               midrollNotificationTimer = new Timer(1000,midrollNotification.duration);
               midrollNotificationTimer.addEventListener("timer",onMidrollNotificationTimer);
               midrollNotificationTimer.start();
               onAdShown();
               dispatch("adsMidrollNotification",false,midrollNotification);
               return;
            }
            media.adOffsetLock = currentAdSource.type == "video" || adPosition == 0 || adPosition == 2;
            if(customApiLoadingList.indexOf(currentAdSource.provider) != -1)
            {
               startAdvert(currentAdSource);
            }
            else if(libList && !(libList[currentAdSource.provider] == null) && !(currentAdSource.provider == "adaptv") || currentAdSource.provider == "notification")
            {
               echo("createAdvert - provider\'s API is loaded");
               startAdvert(currentAdSource);
            }
            else
            {
               echo("createAdvert - we have to load provider\'s API");
               _loc5_ = new LoaderContext();
               _loc5_.checkPolicyFile = true;
               loader = new Loader();
               loader.contentLoaderInfo.addEventListener("init",onAdLoadingComplete);
               loader.contentLoaderInfo.addEventListener("ioError",onAdLoadingError);
               loader.contentLoaderInfo.addEventListener("securityError",onAdLoadingError);
               loader.contentLoaderInfo.addEventListener("error",onAdLoadingError);
               loader.contentLoaderInfo.addEventListener("asyncError",onAdLoadingError);
               _loc7_ = UncaughtErrorHandler.instance.add(loader,this.toString());
               if(_loc7_)
               {
                  _loc7_.addEventListener("uncaughtError",onUncaughtAdError);
               }
               _loc8_ = getAdLibraryUrl(currentAdSource.provider);
               if(_loc8_)
               {
                  loader.load(new URLRequest(_loc8_),_loc5_);
               }
            }
            
         }
         else
         {
            echo("ERROR ( " + param1.provider + ", " + param1.ruleId + " ), because embed is small ( " + embedSize.width + ", " + embedSize.height + " )");
            if(adPosition == 1 && _data[adPosition][adIndex].errors > 4 && !_revShare && !hasAvailableMid() || adPosition == 1 && (_data[adPosition][adIndex].lfeed || _data[adPosition][adIndex].adCueTone))
            {
               return;
            }
            onAdError();
         }
      }
      
      private function onUncaughtAdError(param1:Event) : void {
         var _loc3_:* = null;
         var _loc5_:* = null;
         var _loc4_:* = null;
         var _loc2_:* = false;
         if(param1["error"])
         {
            _loc3_ = getQualifiedClassName(param1["error"]);
            if(param1["error"] is Error)
            {
               _loc5_ = param1["error"] as Error;
               if(_loc5_.message.indexOf("#2067") >= 0 || _loc5_.message.indexOf("#2060") >= 0 || _loc5_.message.indexOf("#1009") >= 0)
               {
                  _loc2_ = true;
               }
            }
            else if(param1["error"] is ErrorEvent)
            {
               _loc4_ = param1["error"] as ErrorEvent;
            }
            
         }
         else
         {
            trace(" error property undefined!");
         }
         if(_loc2_)
         {
            if(currentAdObj)
            {
               destroyCurrentAd();
            }
            onAdError();
         }
      }
      
      private function destructLoader() : void {
         if(loader && loader.contentLoaderInfo)
         {
            loader.contentLoaderInfo.removeEventListener("init",onAdLoadingComplete);
            loader.contentLoaderInfo.removeEventListener("ioError",onAdLoadingError);
            loader.contentLoaderInfo.removeEventListener("securityError",onAdLoadingError);
            loader.contentLoaderInfo.removeEventListener("error",onAdLoadingError);
            loader.contentLoaderInfo.removeEventListener("asyncError",onAdLoadingError);
         }
         loader = null;
      }
      
      private function onAdLoadingError(param1:Event) : void {
         echo("onAdLoadingError - error: " + param1);
         destructLoader();
         if(adPosition == 1)
         {
            _data[adPosition][adIndex].errors++;
         }
         prepairNextAd();
      }
      
      private function onAdLoadingComplete(param1:Event) : void {
         destructLoader();
         if(currentAdSource)
         {
            echo("onAdLoadingComplete - host: " + currentAdSource.provider + " - type: " + currentAdSource.type);
            libList[currentAdSource.provider] = param1.target.loader;
            startAdvert(currentAdSource);
         }
      }
      
      private function startAdvert(param1:Object) : void {
         var _loc2_:* = null;
         if(currentAdObj)
         {
            destroyCurrentAd();
         }
         if(_revShare && _revShare.type == "adaptv")
         {
            param1.revShare = true;
            if(_revShare.partner)
            {
               _adHolder.partnerProgramHeight = 32;
            }
         }
         var _loc3_:* = param1.provider;
         if("googleads" !== _loc3_)
         {
            if("yahooim" !== _loc3_)
            {
               if("adotube" !== _loc3_)
               {
                  if("adaptv" !== _loc3_)
                  {
                     if("delay" !== _loc3_)
                     {
                        if("notification" === _loc3_)
                        {
                           currentAdObj = new ProviderNotification(this,param1);
                        }
                     }
                     else
                     {
                        currentAdObj = new ProviderDelay(this,param1);
                     }
                  }
                  else
                  {
                     currentAdObj = new ProviderAdaptv(this,param1,libList[param1.provider].content);
                  }
               }
               else
               {
                  currentAdObj = new ProviderAdotube(this,param1);
               }
            }
            else
            {
               currentAdObj = new ProviderYahooIM(this,param1);
            }
         }
         else
         {
            currentAdObj = new ProviderGoogle(this,param1);
         }
         currentAdObj.setAdPosition(adPosition);
         currentAdObj.init(_adHolder);
         if(currentAdObj.enabled)
         {
            if(customApiLoadingList.indexOf(param1.provider) == -1 && (!(param1.provider == "googleads") && !(param1.provider == "adaptv") && !(param1.provider == "notification")))
            {
               _adHolder.addChild(libList[param1.provider]);
            }
            _status = 2;
            currentAdObj.requestAd(adPosition);
         }
         else
         {
            echo("[AdManager] startAdvert - ERROR: currentAd is not enabled");
            if(adPosition == 1)
            {
               _data[adPosition][adIndex].errors++;
            }
            _status = 7;
            prepairNextAd();
         }
      }
      
      function onForcePlay() : void {
         isForcePlay = true;
         dispatch("adsPlayContent");
      }
      
      function onAdComplete() : void {
         echo("onAdComplete() - adPosition: " + adPosition + " - status: " + _status + " - revShare: " + _revShare);
         if(_revShare && _revShare.state == "started" && (_revShare.type == "google" || _revShare.type == "notification"))
         {
            _revShare = null;
         }
         _status = 5;
         if(_revShare && _revShare.state == "init")
         {
            if(adPosition == 0)
            {
               _media.onPrerollDone();
            }
            startRevShare();
         }
         else
         {
            if(adPosition == 1 && _data && _data[adPosition] && _data[adPosition][adIndex])
            {
               _data[adPosition][adIndex].errors = 0;
            }
            prepairNextAd();
         }
      }
      
      function onAdError() : void {
         echo("onAdError - adPosition: " + adPosition + " - status: " + _status);
         _status = 7;
         if(_revShare && _revShare.type == "google")
         {
            if(_revShare.state == "started" && !(_revShare.data.type == "fullslot"))
            {
               _revShare.state = "video_failed";
               _revShare.data.type = "fullslot";
               _revShare.data.duration = 30;
               createAdvert(_revShare.data);
               return;
            }
            _revShare = null;
         }
         if(adPosition == 1)
         {
            _data[adPosition][adIndex].errors++;
         }
         prepairNextAd();
      }
      
      private function getAdLibraryUrl(param1:String) : String {
         var _loc2_:String = "";
         var _loc3_:* = param1;
         if("adaptv" === _loc3_)
         {
            _loc2_ = "http://redir.adap.tv/redir/client/static/AS3AdPlayer.swf";
         }
         return _loc2_;
      }
      
      public function play() : Boolean {
         var _loc1_:* = true;
         if(dying || !currentAdObj)
         {
            return _loc1_;
         }
         if(_status == 10 && currentAdSource.type == "video")
         {
            _loc1_ = false;
         }
         if(adPosition == 0 && currentAdObj.type == "adaptv" && (currentAdObj.needPlay()) || _status == 5 || (isForcePlay))
         {
            _loc1_ = true;
            isForcePlay = false;
         }
         echo("play - adPosition: " + adPosition + " - status: " + _status + " - canPlay: " + _loc1_ + (currentAdObj?" - hastimeout: " + currentAdObj.hasTimeout:" - CHALLANGE ACCEPTED 1") + (currentAdSource?" - currentAdSource.type: " + currentAdSource.type:" - CHALLANGE ACCEPTED 2"));
         return _loc1_;
      }
      
      public function hasPostData() : Boolean {
         return _data[2] && _data[2].length;
      }
      
      public function startPost() : void {
         echo("startPost() currentAdObj.adStatus: " + currentAdObj.adStatus);
      }
      
      public function destroyCurrentAd() : void {
         if(currentAdObj)
         {
            echo("destroyCurrentAd()");
            currentAdObj.destroyAd();
            currentAdObj = null;
            dispatch("adRemoved");
         }
         destructCompanionTimer();
         if(currentAdSource)
         {
            if(adIndex >= 0 && _data && _data[adPosition] && _data[adPosition][adIndex] && _data[adPosition][adIndex] == currentAdSource && currentAdSource.remove)
            {
               _data[adPosition].splice(adIndex,1);
               adIndex = adIndex - 1;
            }
            if(libList[currentAdSource.provider] && _adHolder.contains(libList[currentAdSource.provider]))
            {
               _adHolder.removeChild(libList[currentAdSource.provider]);
            }
            currentAdSource = null;
         }
      }
      
      public function destruct() : void {
         dying = true;
         if(_media)
         {
            _loc3_ = 0;
            _loc2_ = mediaEventList;
            for(_loc1_ in mediaEventList)
            {
               _media.removeEventListener(_loc1_,mediaEventList[_loc1_]);
            }
         }
         echo("destroy() - numChildren: " + _adHolder.numChildren);
         if(isAdBlockPlaying)
         {
            dispatch("adsBlockStatus",false,{"remove":true});
         }
         if(midrollNotificationTimer)
         {
            destructMidrollNotificationTimer();
         }
         destructCompanionTimer();
         if(midrollNotification)
         {
            dispatch("adsMidrollNotification",false,{"remove":true});
         }
         if(currentAdSource && currentAdSource.type == "video" && (_status == 10 || _status == 2))
         {
            if(_status == 10)
            {
               dispatch("adsVideoAdFinished",false,{"enableControlBar":true});
            }
            dispatch("adsPlayContent");
         }
         destroyCurrentAd();
         if(adSupportMessage)
         {
            echo("remove ad support message");
            if(_adHolder.contains(adSupportMessage))
            {
               _adHolder.removeChild(adSupportMessage);
            }
            adSupportMessage.destroy();
            adSupportMessage = null;
         }
         if(_adHolder)
         {
            while(_adHolder.numChildren)
            {
               _adHolder.removeChildAt(0);
            }
         }
         var _loc5_:* = 0;
         var _loc4_:* = libList;
         for(_loc1_ in libList)
         {
            libList[_loc1_] = null;
         }
         libList = [];
         _active = false;
      }
      
      override public function destroy(... rest) : * {
         if(_active)
         {
            destruct();
         }
         return super.destroy();
      }
      
      public function setPlayTime(param1:Number) : void {
         if(currentAdObj && (currentAdObj.adType == "adotube" || currentAdObj.adType == "googleads"))
         {
            currentAdObj.setPlayTime(param1);
         }
      }
      
      public function finished(param1:Boolean = false) : void {
         echo("finished()");
         destroyCurrentAd();
         while(_adHolder.numChildren)
         {
            _adHolder.removeChildAt(0);
         }
      }
      
      public function setSize(param1:Number, param2:Number) : void {
         if(_adHolder)
         {
            drawVideoAdBg();
            if(adSupportMessage)
            {
               adSupportMessage.y = _adHolder.height;
               adSupportMessage.resize(_adHolder.width,32);
            }
            if(currentAdObj)
            {
               currentAdObj.setPlayerSize(_adHolder.width,_adHolder.height);
            }
         }
      }
      
      private function onAdsRevShare(param1:DynamicEvent) : void {
         var _loc2_:* = null;
         var _loc3_:* = 0;
         var _loc5_:* = null;
         echo("onAdsRevShare()");
         Debug.explore(param1.data);
         if(currentAdObj)
         {
            echo("onAdsRevShare() adPosition: " + currentAdObj.adPosition?currentAdObj.adPosition as String:" - adStatus: " + currentAdObj.adStatus?currentAdObj.adStatus:" - revShare.type: " + _loc2_.mid[0].provider);
            if(_revShare && _revShare.type == "adaptv")
            {
               echo("setRevShare() - previous revshare mid is still running");
               return;
            }
         }
         else
         {
            echo("onAdsRevShare()");
         }
         if(!_data)
         {
            _data = [];
         }
         _loc2_ = param1.data;
         if(_loc2_ && _loc2_.mid && _loc2_.mid[0])
         {
            _loc2_.revShare = true;
            _revShare = 
               {
                  "state":"init",
                  "data":_loc2_
               };
            if(_loc2_.mid[0].repeatCount && _data[3] && _data[3].length)
            {
               _revShare.partner = true;
               _data[1] = [];
               _loc3_ = 0;
               while(_loc3_ < _loc2_.mid[0].repeatCount)
               {
                  _data[1][_loc3_] = {};
                  _loc7_ = 0;
                  _loc6_ = _data[3][0];
                  for(_loc4_ in _data[3][0])
                  {
                     _data[1][_loc3_][_loc4_] = _data[3][0][_loc4_];
                  }
                  _data[1][_loc3_].errorCount = 0;
                  if(_loc3_ > 0)
                  {
                     _data[1][_loc3_].zid = _data[1][_loc3_].zid + (_loc3_ + 1);
                  }
                  _loc3_++;
               }
               _data[1][0].preCache = 1;
               _data[1][0].blockStart = true;
               _data[1][_loc2_.mid[0].repeatCount - 1].blockEnd = true;
               _revShare.type = "adaptv";
            }
            else if(_loc2_.mid[0].provider == "googleads")
            {
               _revShare.type = "google";
               _revShare.data = _loc2_.mid[0];
               _revShare.data.revshare = true;
               if(currentAdObj && currentAdObj.adType == "google" && currentAdObj.hasRespond == 2)
               {
                  _revShare.state = "started";
                  _status = 2;
                  currentAdSource = _revShare.data;
                  currentAdObj.setData(_revShare.data);
                  currentAdObj.requestAd(adPosition);
                  return;
               }
            }
            else if(_loc2_.mid[0].provider == "adaptv")
            {
               _data[1] = _loc2_.mid;
               _loc3_ = 0;
               while(_loc3_ < _data[1].length)
               {
                  _data[1][_loc3_].errorCount = 0;
                  _loc3_++;
               }
               _data[1][0].blockStart = true;
               _data[1][_loc2_.mid.length - 1].blockEnd = true;
               _revShare.type = "adaptv";
            }
            else if(_loc2_.mid[0].provider == "notification")
            {
               _loc5_ = _loc2_.mid[0];
               _loc5_.remove = true;
               if(!_data[1])
               {
                  _data[1] = [];
               }
               _data[1].splice(adIndex + 1,0,_loc5_);
            }
            
            
            
            echo("-------------------------------------------------");
            Debug.explore(_data);
            echo("-------------------------------------------------");
            if(!_revShare.type)
            {
               _revShare = null;
            }
            else if(!currentAdObj || !(currentAdSource && currentAdSource.type == "video" && currentAdObj.adStatus == "playing"))
            {
               startRevShare();
            }
            
         }
      }
      
      private function startRevShare() : void {
         echo("startRevShare() - type: " + _revShare.type);
         if(currentAdObj)
         {
            destroyCurrentAd();
         }
         _revShare.state = "started";
         _started = true;
         if(_revShare.type == "google" || _revShare.type == "notification")
         {
            if(_revShare.type == "notification")
            {
               adPosition = 1;
               adIndex = 0;
            }
            createAdvert(_revShare.data);
         }
         else if(_revShare.type == "adaptv")
         {
            adPosition = 1;
            adIndex = 0;
            createAdvert(_data[adPosition][adIndex]);
         }
         
      }
      
      function onAdHidden() : void {
         dispatch("adHidden");
      }
      
      function onAdShown() : void {
         dispatch("adShown");
      }
      
      function onAdRemoved() : void {
         dispatch("adRemoved");
      }
      
      function logAdEvent(param1:Object) : void {
         echo("logAdEvent - event type: " + param1.eventName + " - host: " + param1.host);
         dispatch("adsLogAdEvent",false,param1);
      }
      
      public function get isRevshare() : Boolean {
         return !(_revShare == null);
      }
      
      public function get status() : uint {
         return _status;
      }
      
      public function get adHostName() : String {
         return currentAdSource.provider;
      }
      
      private function onMidrollNotificationTimer(param1:TimerEvent) : void {
         if(midrollNotification)
         {
            midrollNotification.time = midrollNotification.duration - (midrollNotificationTimer.repeatCount - midrollNotificationTimer.currentCount);
            if(midrollNotification.start)
            {
               delete midrollNotification.start;
            }
            if(midrollNotification.customOffering)
            {
               delete midrollNotification.customOffering;
            }
            if(midrollNotification.duration == midrollNotification.time)
            {
               midrollNotification.remove = true;
            }
            dispatch("adsMidrollNotification",false,midrollNotification);
            if(midrollNotification.remove)
            {
               onAdsPlayMidroll();
            }
            if(Logic.hasInstance && (Logic.instance.overlay))
            {
               midrollNotificationTimer.stop();
               isPaused = true;
               echo("skip midroll notification");
               dispatch("adsPause",false,{"reason":1});
            }
         }
      }
      
      private function onAdsPlayMidroll(param1:Event = null) : void {
         echo("onAdsPlayMidroll()");
         midrollNotification = null;
         if(midrollNotificationTimer)
         {
            destructMidrollNotificationTimer();
         }
         createAdvert(currentAdSource,true);
      }
      
      private function onAdsResume(param1:Event = null) : void {
         if(isPaused && (Logic.hasInstance) && !Logic.instance.overlay)
         {
            isPaused = false;
            dispatch("adsPause",false,{"reason":0});
            if(!midrollNotification && !midrollNotificationTimer)
            {
               prepairNextAd();
            }
         }
      }
      
      private function onTitlecardStatus(param1:DynamicEvent = null) : void {
         echo("onTitlecardStatus() - " + param1.data.status);
         if(currentAdObj)
         {
            echo("onTitlecardStatus() - " + currentAdObj);
         }
         if(currentAdObj && currentAdObj.type)
         {
            echo("onTitlecardStatus() - " + currentAdObj.type);
         }
         if(currentAdObj && currentAdObj.type == "provideradaptv" && param1.data.status)
         {
            echo("NA MOST");
            (currentAdObj as ProviderAdaptv).titlecardStatus(param1.data.status);
         }
      }
      
      function titlecardStart(param1:Object) : void {
         echo("titlecardStart()");
         dispatch("titlecardStart",false,param1);
      }
      
      private function destructMidrollNotificationTimer() : void {
         midrollNotificationTimer.stop();
         midrollNotificationTimer.removeEventListener("timer",onMidrollNotificationTimer);
         midrollNotificationTimer = null;
      }
      
      private function destructCompanionTimer() : void {
         if(companionTimer)
         {
            companionTimer.stop();
            companionTimer.removeEventListener("timerComplete",onCompanionTimer);
            companionTimer = null;
         }
      }
      
      public function get hasMidrollNotification() : Boolean {
         return !(midrollNotification == null);
      }
      
      public function get hasMidrolls() : Boolean {
         return _data && _data[1] && _data[1].length > 0;
      }
      
      public function get hasPreroll() : Boolean {
         return _data && _data[0] && _data[0].length > 0;
      }
      
      public function get clientIp() : String {
         return _clientIp;
      }
      
      public function set clientIp(param1:String) : void {
         _clientIp = param1;
      }
      
      function get adaptvParams() : Object {
         return _adaptvParams;
      }
      
      private function drawVideoAdBg() : void {
         videoAdBg.graphics.clear();
         videoAdBg.graphics.beginFill(0);
         videoAdBg.graphics.drawRect(0,0,_adHolder.width,_adHolder.height);
         videoAdBg.graphics.endFill();
      }
      
      function pauseContent(param1:Boolean = false) : void {
         echo("pauseContent() - isAdBlockPlaying: " + isAdBlockPlaying + " - disableControlBar: " + param1);
         if(!(adPosition == 2) && !isAdBlockPlaying)
         {
            dispatch("adsPauseContent");
         }
         if(param1)
         {
            dispatch("adsControlbarStatus",false,{"enable":false});
         }
      }
      
      function playContent(param1:Boolean = false) : void {
         echo("playContent() - isAdBlockPlaying: " + isAdBlockPlaying + " - enableControlBar: " + param1);
         if(!isAdBlockPlaying)
         {
            dispatch("adsPlayContent");
         }
         if(param1)
         {
            dispatch("adsControlbarStatus",false,{"enable":true});
         }
      }
      
      function onVideoAdStarted(param1:Boolean = true, param2:Number = 0) : void {
         echo("onVideoAdStarted() - disableControlBar: " + param1 + " - currentAdSource.blockStart: " + currentAdSource.blockStart + " - media.playing: " + media.playing);
         _status = 10;
         drawVideoAdBg();
         videoAdBg.visible = true;
         var _loc3_:* = false;
         if(currentAdSource.blockStart)
         {
            isAdBlockPlaying = true;
            echo("isAdBlockPlaying: " + isAdBlockPlaying);
            setCurrentBlockArguments(param2);
         }
         if(_revShare && _revShare.partner)
         {
            echo("adSupportMessage: " + !(adSupportMessage == null));
            if(!adSupportMessage)
            {
               echo("creating adSupportMessage");
               adSupportMessage = new AdSupportMessage(_adHolder.width,32);
               adSupportMessage.y = _adHolder.height;
               adHolder.partnerProgramHeight = 32;
               adHolder.addChild(adSupportMessage);
            }
            else
            {
               echo("adSupportMessage: show");
               adSupportMessage.visible = true;
            }
            _loc3_ = true;
         }
         if(currentAdSource.blockStart || !isAdBlockPlaying || _loc3_)
         {
            dispatch("adsVideoAdStarted",false,{"disableControlBar":param1});
            if(currentAdObj.adType == "adaptv")
            {
               if(!companionTimer)
               {
                  companionTimer = new Timer(1000,1);
                  companionTimer.addEventListener("timerComplete",onCompanionTimer);
               }
               companionTimer.start();
            }
            else
            {
               onCompanionTimer(new TimerEvent("timerComplete"));
            }
         }
         if(isAdBlockPlaying && !_revShare)
         {
            dispatch("adsBlockStatus",false,
               {
                  "blockIndex":currentBlockArguments.index,
                  "blockTotal":currentBlockArguments.total,
                  "blockDuration":currentBlockArguments.duration,
                  "currentAdDuration":currentBlockArguments.currentAdDuration
               });
         }
      }
      
      function onCompanionTimer(param1:TimerEvent = null) : void {
         destructCompanionTimer();
         dispatch("adsCompanion",false,{"companion":(param1?false:true)});
      }
      
      private function setCurrentBlockArguments(param1:uint = 0) : void {
         var _loc2_:* = 0;
         echo("setCurrentBlockArguments()");
         currentBlockArguments = 
            {
               "index":0,
               "total":_data[adPosition].length,
               "duration":currentAdSource.blockDuration || "15"
            };
         if(param1 > 0)
         {
            currentBlockArguments.currentAdDuration = param1;
         }
         if(adPosition == 1)
         {
            _loc2_ = adIndex;
            while(_loc2_ < _data[adPosition].length)
            {
               if(_data[adPosition][_loc2_].blockEnd)
               {
                  currentBlockArguments.total = _loc2_ + 1 - adIndex;
                  break;
               }
               _loc2_++;
            }
         }
      }
      
      function onVideoAdFinished(param1:Boolean = true) : void {
         if(currentAdSource.blockStart && !isAdBlockPlaying)
         {
            pauseContent();
            isAdBlockPlaying = true;
            setCurrentBlockArguments();
         }
         echo("onVideoAdFinished() - enableControlBar: " + param1 + " - isAdBlockPlaying: " + isAdBlockPlaying);
         _status = 5;
         if(currentAdSource.blockEnd)
         {
            isAdBlockPlaying = false;
            echo("isAdBlockPlaying: " + isAdBlockPlaying);
            currentBlockArguments = null;
            if(!_revShare)
            {
               dispatch("adsBlockStatus",false,{"remove":true});
            }
            else
            {
               echo("_revShare.partner: " + _revShare.partner);
               if(_revShare.partner)
               {
                  if(adSupportMessage)
                  {
                     echo("adSupportMessage: hide");
                     adSupportMessage.visible = false;
                  }
               }
            }
         }
         if(currentAdSource.blockEnd || !isAdBlockPlaying)
         {
            videoAdBg.visible = false;
            dispatch("adsVideoAdFinished",false,{"enableControlBar":param1});
         }
      }
      
      private function onViewModeChange(param1:Event) : void {
         echo("onViewModeChange() - " + [Logic.instance.viewMode,Locale.instance.language,disabledOverlays]);
         if(Logic.instance.viewMode == "explore" || Logic.instance.viewMode == "discovery")
         {
            echo("jaDE - conditions are done, remove overlays");
            disabledOverlays = true;
            echo(" >> " + [adPosition,currentAdSource?currentAdSource.type:"",adIndex]);
            if(adPosition == 1 && currentAdSource && currentAdSource.type == "overlay")
            {
               disabledOverlayIndex = adIndex;
               prepairNextAd();
            }
         }
         else if(disabledOverlays)
         {
            echo("jaDE continue " + ["isVideoAdInMid",isVideoAdInMid," - isPaused",isPaused," - adIndex",adIndex," - disabledOverlayIndex",disabledOverlayIndex]);
            disabledOverlays = false;
            disabledOverlayIndex = 0;
            if(!isVideoAdInMid && !(_status == 10 && currentAdSource && currentAdSource.type == "video"))
            {
               prepairNextAd();
            }
            isVideoAdInMid = false;
         }
         
      }
      
      private function onLinearFeedAdBreak(param1:DynamicEvent) : void {
         var _loc2_:* = null;
         echo("onLinearFeedAdBreak");
         if(_data[4] && _data[4].length)
         {
            if(param1.videoId)
            {
               _loc2_ = _data[4][param1.videoId]?_data[4][param1.videoId]:_data[4][0]?_data[4][0]:null;
               if(_loc2_)
               {
                  _loc2_.remove = true;
                  _loc2_.preCache = param1.remainingTime?param1.remainingTime / 1000:15.0;
                  _loc2_.withoutContentPlayStatusChange = true;
                  _loc2_.disableContentAudio = true;
                  _loc2_.adCueTone = true;
                  adIndex = -1;
                  adPosition = 1;
                  _data[1] = [_loc2_];
                  prepairNextAd();
               }
            }
            else if(param1.start && currentAdObj && currentAdObj["startRequestedAd"])
            {
               currentAdObj["startRequestedAd"]();
            }
            
         }
      }
      
      function get contentAudio() : Boolean {
         return _contentAudio;
      }
      
      function set contentAudio(param1:Boolean) : void {
         _contentAudio = param1;
         dispatch("adsContentAudio",false,{"enable":param1});
      }
   }
}
