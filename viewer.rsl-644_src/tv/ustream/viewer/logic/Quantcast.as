package tv.ustream.viewer.logic
{
   import flash.display.Sprite;
   import flash.display.Loader;
   import flash.events.Event;
   import flash.system.ApplicationDomain;
   import flash.external.ExternalInterface;
   import flash.system.Capabilities;
   import tv.ustream.tools.This;
   import flash.system.Security;
   import flash.net.URLRequest;
   import flash.system.LoaderContext;
   
   public class Quantcast extends Sprite
   {
      
      public function Quantcast(param1:Object, param2:Sprite) {
         parameters = [];
         queue = [];
         super();
         if(This.secure)
         {
            Quantcast.pixelUrl = Quantcast.pixelUrl.split("http:").join("https:");
            Quantcast.quantUrl = Quantcast.quantUrl.split("http:").join("https:");
         }
         if(Security.sandboxType == "localWithFile")
         {
            return;
         }
         rootMC = param2;
         updateParameters(buildDefaults());
         updateParameters(param1);
         var _loc3_:URLRequest = new URLRequest(createPixelUrl(Quantcast.quantUrl,parameters));
         _loc3_.method = "GET";
         loader = new Loader();
         loader.contentLoaderInfo.addEventListener("init",initListener);
         loader.load(_loc3_,new LoaderContext(false,ApplicationDomain.currentDomain));
         embedded();
      }
      
      private static var quantv2:Object;
      
      private static var pixelUrl:String = "http://flash.quantserve.com/pixel.swf?";
      
      private static var quantUrl:String = "http://flash.quantserve.com/quantv2.swf?";
      
      private static var dotMacro:String = "{DOT_MACRO}";
      
      private var loader:Loader;
      
      private var parameters:Array;
      
      private var rootMC:Sprite;
      
      private var queue:Array;
      
      private function initListener(param1:Event) : void {
         var _loc2_:ApplicationDomain = param1.target.applicationDomain;
         var _loc3_:Class = _loc2_.getDefinition("quantv2") as Class;
         Quantcast.quantv2 = _loc3_(param1.target.content);
         dequeue(queue);
      }
      
      private function dequeue(param1:Array) : void {
         if(param1.length == 0)
         {
            return;
         }
         var _loc2_:String = param1.pop() as String;
         dequeue(param1);
         quantv2.sendEvent(_loc2_);
      }
      
      public function embedded(param1:Object = null) : void {
         sendEvent("embedded",param1);
      }
      
      public function clicked(param1:Object = null) : void {
         sendEvent("clicked",param1);
      }
      
      public function resumed(param1:Object = null) : void {
         sendEvent("resumed",param1);
      }
      
      public function paused(param1:Object = null) : void {
         sendEvent("paused",param1);
      }
      
      public function seeked(param1:Object = null) : void {
         sendEvent("seeked",param1);
      }
      
      public function played(param1:Object = null) : void {
         sendEvent("played",param1);
      }
      
      public function progress(param1:Object = null) : void {
         sendEvent("progress",param1);
      }
      
      public function finished(param1:Object = null) : void {
         sendEvent("finished",param1);
      }
      
      public function refresh(param1:Object = null) : void {
         sendEvent("refresh",param1);
      }
      
      private function getLocation() : String {
         var _loc1_:* = null;
         var _loc2_:* = null;
         try
         {
            if(ExternalInterface.available)
            {
               _loc1_ = "function getLoc() { return window.location.toString(); }";
               _loc2_ = ExternalInterface.call(_loc1_);
               _loc4_ = _loc2_;
               return _loc4_;
            }
         }
         catch(error:Error)
         {
         }
         return "";
      }
      
      protected function sendEvent(param1:String, param2:Object = null) : void {
         var _loc3_:Number = Math.floor(10000 * Math.random());
         if(param2 != null)
         {
            updateParameters(param2);
         }
         parameters["event"] = param1;
         parameters["r"] = _loc3_;
         if(Quantcast.quantv2 == null)
         {
            queue.push(createPixelUrl(Quantcast.pixelUrl,parameters));
            return;
         }
         quantv2.sendEvent(createPixelUrl(Quantcast.pixelUrl,parameters));
      }
      
      private function buildDefaults() : Object {
         var _loc1_:Object = {};
         _loc1_.qcv = "5.1.3";
         _loc1_.pageURL = getLocation();
         _loc1_.media = "default";
         _loc1_.url = rootMC.loaderInfo.loaderURL;
         _loc1_.flashPlayer = Capabilities.version;
         return _loc1_;
      }
      
      private function createPixelUrl(param1:String, param2:Array) : String {
         var _loc5_:* = null;
         var _loc4_:String = new String(param1);
         var _loc7_:* = 0;
         var _loc6_:* = param2;
         for(_loc5_ in param2)
         {
            _loc5_ = _loc5_.replace(dotMacro,".");
            _loc4_ = _loc4_ + (_loc5_ + "=" + encodeURIComponent(param2[_loc3_]) + "&");
         }
         return _loc4_;
      }
      
      public function updateParameters(param1:Object) : void {
         var _loc2_:* = null;
         var _loc5_:* = NaN;
         if(param1.length != undefined)
         {
            _loc2_ = {};
            _loc5_ = 0.0;
            while(_loc5_ < param1.length)
            {
               _loc7_ = 0;
               _loc6_ = param1[_loc5_];
               for(_loc4_ in param1[_loc5_])
               {
                  _loc2_[_loc4_ + dotMacro + (_loc5_ + 1)] = param1[_loc5_][_loc4_];
               }
               _loc5_++;
            }
            param1 = _loc2_;
         }
         var _loc9_:* = 0;
         var _loc8_:* = param1;
         for(_loc3_ in param1)
         {
            parameters[_loc3_] = param1[_loc3_];
         }
      }
   }
}
