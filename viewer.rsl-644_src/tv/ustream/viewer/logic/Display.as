package tv.ustream.viewer.logic
{
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.display.DisplayObject;
   import tv.ustream.tools.Locker;
   
   public class Display extends Sprite
   {
      
      public function Display() {
         super();
         addEventListener("added",onAdded);
      }
      
      private var _width:Number = 320;
      
      private var _height:Number = 240;
      
      private function onAdded(param1:Event) : void {
         try
         {
            if(!(param1.target == this) && (param1.target as DisplayObject).parent == this)
            {
               update(param1.target as DisplayObject);
            }
         }
         catch(e:Error)
         {
         }
      }
      
      private function update(param1:DisplayObject) : void {
         if(param1 is Locker)
         {
            (param1 as Locker).resize(_width,_height);
         }
         else
         {
            param1.width = _width;
            param1.height = _height;
         }
      }
      
      private function updateAll() : void {
         var _loc1_:* = NaN;
         _loc1_ = 0.0;
         while(_loc1_ < numChildren)
         {
            update(getChildAt(_loc1_));
            _loc1_++;
         }
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         _width = param1;
         updateAll();
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         _height = param1;
         updateAll();
      }
   }
}
