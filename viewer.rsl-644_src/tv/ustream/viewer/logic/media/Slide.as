package tv.ustream.viewer.logic.media
{
   import flash.display.Sprite;
   import flash.display.Loader;
   import flash.display.BitmapData;
   import flash.geom.Point;
   import flash.display.Bitmap;
   import flash.filters.DisplacementMapFilter;
   import flash.filters.BlurFilter;
   import flash.system.LoaderContext;
   import flash.events.IOErrorEvent;
   import flash.events.Event;
   import flash.net.URLRequest;
   
   public class Slide extends Sprite
   {
      
      public function Slide(param1:String) {
         point = new Point();
         context = new LoaderContext(true);
         super();
         _loader = new Loader();
         addChild(new Loader());
         _loader.contentLoaderInfo.addEventListener("complete",onLoadcomplete);
         _loader.contentLoaderInfo.addEventListener("ioError",onLoadIoError);
         _loader.load(new URLRequest(param1),context);
         copy = new Bitmap();
         addChild(new Bitmap()).cacheAsBitmap = true;
         bm = new Bitmap();
         addChild(new Bitmap()).cacheAsBitmap = true;
      }
      
      private var _alpha:Number = 0;
      
      private var _loader:Loader;
      
      private var perlin:BitmapData;
      
      private var point:Point;
      
      private var bm:Bitmap;
      
      private var bmd:BitmapData;
      
      private var copy:Bitmap;
      
      private var displace:DisplacementMapFilter;
      
      private var blur:BlurFilter;
      
      private var context:LoaderContext;
      
      private function onLoadIoError(param1:IOErrorEvent) : void {
         dispatchEvent(new Event("error"));
      }
      
      private function onLoadcomplete(param1:Event) : void {
         (param1.target.loader.content as Bitmap).smoothing = true;
         perlin = new BitmapData(_loader.width,_loader.height,true,0);
         perlin.perlinNoise(_loader.width / 3,_loader.height / 3,1,Math.random() * 10000,false,true,1,false);
         bmd = new BitmapData(_loader.width,_loader.height,true,0);
         bm.bitmapData = bmd;
         bm.smoothing = true;
         copy.bitmapData = new BitmapData(_loader.width,_loader.height,false);
         copy.bitmapData.draw(_loader);
         copy.mask = bm;
         copy.smoothing = true;
         blur = new BlurFilter(10,10,3);
         displace = new DisplacementMapFilter(copy.bitmapData,point,1,1,1,1,"clamp");
         _loader.blendMode = "add";
         setBitmapData();
         dispatchEvent(new Event("complete"));
      }
      
      override public function get alpha() : Number {
         return _alpha;
      }
      
      override public function set alpha(param1:Number) : void {
         _alpha = param1;
         _loader.alpha = _alpha * 1.5;
         bm.alpha = _alpha;
         if(bmd)
         {
            setBitmapData();
         }
      }
      
      public function get loader() : Loader {
         return _loader;
      }
      
      private function setBitmapData() : void {
         bmd.threshold(perlin,bmd.rect,point,">",16777215 * _alpha,0,16711680,true);
         var _loc1_:* = (1 - _alpha) * 200;
         displace.scaleX = _loc1_;
         displace.scaleY = _loc1_;
         bm.filters = [displace];
         _loc1_ = (1 - _loader.alpha) * 30;
         blur.blurY = _loc1_;
         blur.blurX = _loc1_;
         _loader.filters = [blur];
      }
   }
}
