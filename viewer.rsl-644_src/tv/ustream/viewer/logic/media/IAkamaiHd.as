package tv.ustream.viewer.logic.media
{
   import tv.ustream.interfaces.IDispatcher;
   import flash.net.NetStream;
   import tv.ustream.net.Connection;
   import flash.display.DisplayObject;
   
   interface IAkamaiHd extends IDispatcher
   {
      
      function get stream() : NetStream;
      
      function get connection() : Connection;
      
      function get currentBitrate() : Number;
      
      function get streamProvider() : Object;
      
      function setDisplayObject(param1:DisplayObject) : void;
      
      function play(param1:Object = null) : void;
      
      function compareTokens(param1:Array) : void;
   }
}
