package tv.ustream.viewer.logic.media
{
   import tv.ustream.net.MultiConnection;
   import flash.utils.Timer;
   import tv.ustream.net.Connection;
   import flash.display.Sprite;
   import flash.events.Event;
   import tv.ustream.viewer.logic.modules.stream.UhsProvider;
   import tv.ustream.tools.This;
   import tv.ustream.viewer.logic.modules.stream.CdnProvider;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   import tv.ustream.tools.LibLoader;
   import tv.ustream.tools.DynamicEvent;
   import flash.net.URLLoader;
   import flash.events.SecurityErrorEvent;
   import flash.events.IOErrorEvent;
   import tv.ustream.net.ChunkStream;
   import tv.ustream.tools.Debug;
   import flash.events.AsyncErrorEvent;
   import flash.events.NetStatusEvent;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.viewer.logic.modules.RpinLock;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.viewer.logic.modules.Logo;
   import tv.ustream.viewer.logic.modules.AgeVerification;
   import tv.ustream.viewer.logic.modules.AgeLock;
   import tv.ustream.modules.Module;
   import tv.ustream.viewer.logic.modules.Ppv;
   import tv.ustream.viewer.logic.modules.Layers;
   import flash.events.TimerEvent;
   import tv.ustream.viewer.logic.modules.Quality;
   import flash.net.URLRequest;
   import tv.ustream.viewer.logic.modules.Dvr;
   import tv.ustream.viewer.logic.modules.Stream;
   import tv.ustream.viewer.logic.modules.stream.Provider;
   import tv.ustream.net.ChunkStreamPlayOptions;
   
   public class Channel extends Recorded
   {
      
      public function Channel(param1:String, param2:Boolean = true, param3:String = null, param4:String = null, param5:String = null, param6:Object = null) {
         addEventListener("play",callCdnPlay);
         addEventListener("pause",callCdnPlay);
         super(param1,param2,param3,param4,param5,param6);
         createUmsFallback();
         echo("pageUrl: " + This.pageUrl);
         if(shared.forcedBuffer >= 0)
         {
            forcedBuffer = shared.forcedBuffer;
         }
         stats.ums = ums;
         varnish = true;
         _type = "channel";
         cdnTimer = new Timer(3000);
         cdnTimer.addEventListener("timer",connectToCdn);
         addEventListener("getConnection",onGetFmsConnection);
         if(param1)
         {
            channels.push(param1);
         }
      }
      
      public static const STATUS_WAITING:int = -1;
      
      public static const STATUS_OFFAIR:int = 0;
      
      public static const STATUS_DVR:int = 1;
      
      public static const STATUS_LIVE:int = 2;
      
      public static const TRANSITION_FAILED:String = "transitionFailed";
      
      public static const TRANSITION_STARTED:String = "transitionStarted";
      
      public static const TRANSITION_COMPLETED:String = "transitionCompleted";
      
      public static const OFFAIR_CONTENT_INFO:String = "offairContentInfo";
      
      private static var _channels:Array = [];
      
      public static var preferredCdn:String;
      
      public static function get channels() : Array {
         return _channels;
      }
      
      private const CHUNKS_REQUIRED_FLASHVERSION:String = "10.3";
      
      private const MIN_TRANSITION_LENGTH:Number = 2;
      
      private var cdn:MultiConnection;
      
      private var cdnTimer:Timer;
      
      private var cdnSubscribeTimer:Timer;
      
      protected var p2pUrl:String;
      
      protected var p2pHash:String;
      
      protected var p2pForce:Boolean;
      
      protected var p2pFallBack:Boolean = false;
      
      private var hasP2p:Boolean;
      
      private var p2p:Connection;
      
      private var moduleInitRef:Function;
      
      private var moduleInfoRef:Function;
      
      private var _offairContent:Object;
      
      private var bufferOutCounter:int = 0;
      
      private var _extendedBuffer:Boolean = false;
      
      private var _status:int = -1;
      
      private var fmsReconnectCounter:uint = 0;
      
      private var netGroup;
      
      private var reportRpin:Boolean = false;
      
      private var backupCid:String = "";
      
      private var cdnIdleTimeout:Boolean = false;
      
      private var offairContentDisabled:Boolean = false;
      
      private var forcedBuffer:int = -1;
      
      private var cdnSubscribes:Object;
      
      private var newStreamName:String;
      
      private var im360:Sprite;
      
      private var ignoreNextLowBandwidth:Boolean = false;
      
      protected var chunks:Connection;
      
      protected var chunksVarnishFailed:Boolean = false;
      
      protected var uhsTcdnUrl:String;
      
      protected var uhsTcdnResolvedUrl:String;
      
      private function callCdnPlay(param1:Event) : void {
         if(provider && provider.name == "tcdn" && cdn && cdn.connected)
         {
            cdn.call("playing",null,_playing);
            echo("cdn playing( " + _playing + " )");
         }
      }
      
      override public function destroy(... rest) : * {
         removeEventListener("play",callCdnPlay);
         removeEventListener("pause",callCdnPlay);
         if(id && _channels && !(_channels.indexOf(id) == -1))
         {
            _channels.splice(_channels.indexOf(id),1);
         }
         if(_modules.stream)
         {
            _modules.stream.eliminateEventListeners();
         }
         return super.destroy();
      }
      
      override protected function corePause(param1:String = "") : void {
         super.corePause(param1);
         if(im360)
         {
            im360["playing"] = false;
         }
      }
      
      override protected function corePlay() : void {
         super.corePlay();
         if(im360)
         {
            im360["playing"] = true;
         }
      }
      
      override public function play(... rest) : void {
         extendedBuffer = false;
         if(online)
         {
            super.play.apply(this,rest);
         }
      }
      
      override protected function connect() : void {
         destroyStream();
         if(_playing && provider && _modules)
         {
            if(!_modules.lock)
            {
               if(geoAds)
               {
                  createAdverts(geoAds);
               }
               w84ModulesLock = false;
               streamName = "";
               if(provider && !reConnect)
               {
                  streamName = provider.streamName;
                  if(provider is UhsProvider)
                  {
                     echo("### provider is uhs");
                     if(This.compareVersion("10.3"))
                     {
                        streamOwner = setupChunks();
                     }
                     else
                     {
                        echo("### uhs not available on client");
                        if(!hasNonUhsProvider() || (hasNonUhsProvider() && !providerSwitch(true)))
                        {
                           echo("### uhs only channel");
                           provider.eliminateEventListeners();
                           onRejected({"unavailable":"The current format is unavailable in your player version!"});
                        }
                     }
                  }
                  else if(provider is CdnProvider)
                  {
                     connectToCdn();
                  }
                  else
                  {
                     _loc1_ = provider.name;
                     if("im360" !== _loc1_)
                     {
                        if("akamaiHd" !== _loc1_)
                        {
                           fmsStreamOwner = true;
                           connectToFms();
                        }
                        else
                        {
                           setupAkamaiHd();
                        }
                     }
                     else
                     {
                        setupIm360();
                     }
                  }
                  
               }
            }
            else
            {
               w84ModulesLock = true;
               dispatch("moduleLock");
            }
         }
      }
      
      private function setupIm360() : void {
         var _loc2_:* = null;
         var _loc1_:* = null;
         echo("setupIm360");
         if(ApplicationDomain.currentDomain.hasDefinition("tv.ustream.viewer.logic.media::Im360"))
         {
            echo("setupIm360 hasDefinition");
            initIm360();
         }
         else
         {
            echo("setupIm360 load...");
            _loc2_ = "viewer.rsl3d.swf";
            if(Security.sandboxType == "localTrusted")
            {
               _loc2_ = "../../stage/api/bin/" + _loc2_;
            }
            _loc1_ = LibLoader.instance.load(_loc2_,3,true);
            LibLoader.instance.addEventListener("complete:" + _loc1_,initIm360);
         }
      }
      
      private function initIm360(param1:Event = null) : void {
         var _loc3_:* = null;
         var _loc5_:* = null;
         var _loc2_:* = null;
         echo("im360 loaded");
         var _loc4_:Class = ApplicationDomain.currentDomain.getDefinition("tv.ustream.viewer.logic.media::Im360") as Class;
         if(_loc4_)
         {
            echo("new im360");
            _loc3_ = provider.url.substr(provider.url.length - 1,1);
            _loc5_ = provider.streamName.substr(0,1);
            _loc2_ = provider.url + provider.streamName;
            if(!(_loc3_ == "/") && !(_loc5_ == "/"))
            {
               _loc2_ = provider.url + "/" + provider.streamName;
            }
            im360 = new _loc4_(_loc2_,playing,_volume,mediaId) as Sprite;
            custom.addChild(im360).addEventListener("unpublish",onIm360Unpublish);
            custom.visible = true;
            onLockerResize();
            audio = true;
            video = true;
            dispatch("getStreamSize",false,
               {
                  "width":400,
                  "height":225
               });
         }
      }
      
      private function onIm360Unpublish(param1:Event) : void {
         goOffline();
         if(reConnect)
         {
            reConnect = false;
         }
      }
      
      private function destroyIm360() : void {
         if(im360)
         {
            echo("destroyIm360");
            if(custom.contains(im360))
            {
               custom.removeChild(im360);
               custom.visible = false;
            }
            im360["destroy"]();
            im360 = null;
         }
      }
      
      private function setupAkamaiHd() : void {
         var _loc2_:* = null;
         var _loc1_:* = null;
         if(ApplicationDomain.currentDomain.hasDefinition("tv.ustream.viewer.logic.media::AkamaiHd"))
         {
            initAkamaiHd();
         }
         else
         {
            _loc2_ = "viewer.rslhd.swf";
            if(Security.sandboxType == "localTrusted")
            {
               _loc2_ = "../../../api/bin/" + _loc2_;
            }
            _loc1_ = LibLoader.instance.load(_loc2_,3,true);
            LibLoader.instance.addEventListener("complete:" + _loc1_,initAkamaiHd);
         }
      }
      
      private function initAkamaiHd(param1:Event = null) : void {
         echo("akamaihd loaded");
         var _loc2_:Class = ApplicationDomain.currentDomain.getDefinition("tv.ustream.viewer.logic.media::AkamaiHd") as Class;
         if(_loc2_)
         {
            echo("new akamaihd");
            akamaiHd = new _loc2_();
            akamaiHd.addEventListener("switch",onAkamaiHdSwitch);
            _streamOwner = akamaiHd.connection;
            createStream();
         }
      }
      
      private function onAkamaiHdSwitch(param1:DynamicEvent) : void {
         streamName = param1.streamName;
         updateUMS();
      }
      
      private function destroyAkamaiHd() : void {
         if(akamaiHd)
         {
            destroyStream();
            if(_streamOwner == akamaiHd.connection)
            {
               _streamOwner = null;
            }
            akamaiHd.removeEventListener("switch",onAkamaiHdSwitch);
            akamaiHd = akamaiHd.destroy();
         }
      }
      
      override protected function onFmsDisconnected(... rest) : void {
         if(active && !rejected && !plannedDisconnected)
         {
            if(_modules.stream && provider is CdnProvider)
            {
               fmsReconnectCounter = fmsReconnectCounter + 1;
               if(fmsReconnectCounter > 3)
               {
                  fmsReconnectCounter = 0;
                  fms.kill();
                  fms = null;
                  if(fmsTimer && fmsTimer.running)
                  {
                     fmsTimer.stop();
                  }
                  logicOff();
                  reConnect = true;
               }
               else
               {
                  connectToFms();
               }
            }
            else
            {
               super.onFmsDisconnected();
            }
         }
         plannedDisconnected = false;
      }
      
      private function goOnline() : void {
         updateStatus();
      }
      
      private function goOffline(... rest) : void {
         dispatch("goOffline");
         var _loc2_:* = true;
         if(ads)
         {
            if(ads.hasPostData())
            {
               _loc2_ = false;
               ads.startPost();
               offline(true);
            }
         }
         if(_loc2_ || !ads)
         {
            offline();
         }
      }
      
      override protected function onPostRollDone(... rest) : void {
         offline();
      }
      
      private function offline(... rest) : void {
         reConnect = false;
         if(rest[0] == null)
         {
            destroyAdverts();
            updateStatus();
            onPrerollDone("offline");
         }
         updateUMS();
      }
      
      private function onFmsOffline(... rest) : void {
         echo("onFmsOffline");
         if(!reConnect)
         {
            if(online)
            {
               goOffline();
            }
            else
            {
               offline();
            }
         }
         else
         {
            logicOff();
            reConnect = true;
         }
      }
      
      private function init(... rest) : void {
         info(rest[1]);
      }
      
      private function info(param1:Object) : void {
         if(param1.streamInfo)
         {
            if(!(param1.streamInfo.audio == null) && !(param1.streamInfo.audio == _audio))
            {
               audio = param1.streamInfo.audio;
            }
            if(!(param1.streamInfo.video == null) && !(param1.streamInfo.video == _video))
            {
               video = param1.streamInfo.video;
            }
         }
         if(moduleInfoRef != null)
         {
            moduleInfoRef(param1,"fms");
         }
      }
      
      private function onGetFmsConnection(... rest) : void {
         fms.addEventListener("password",onPassword);
         fms.addEventListener("viewerLimitReached",onViewerLimitReached);
         fms.addEventListener("embedLimitReached",onEmbedLimitReached);
         fms.addEventListener("streamNotFound",onFmsStreamNotFound);
         fms.goOffline = onFoo;
         fms.cdnOn = onFoo;
         fms.cdnOff = onFoo;
         fms.logicOff = onFoo;
         fms.ban = ban;
         if(_modules)
         {
            _modules.connection = fms;
         }
         moduleInitRef = fms.moduleInit;
         moduleInfoRef = fms.moduleInfo;
         fms.moduleInit = init;
         fms.moduleInfo = info;
         updateUMS();
      }
      
      private function onFoo(... rest) : void {
      }
      
      private function onFmsStreamNotFound(... rest) : void {
         if(!providerSwitch() && provider)
         {
            provider.destroy();
         }
      }
      
      override protected function fmsDestroy() : void {
         if(fms)
         {
            if(_modules)
            {
               _modules.connection = null;
            }
            fms.removeEventListener("password",onPassword);
            fms.removeEventListener("viewerLimitReached",onViewerLimitReached);
            fms.removeEventListener("embedLimitReached",onEmbedLimitReached);
            fms.removeEventListener("streamNotFound",onFmsStreamNotFound);
            fms.goOffline = null;
            fms.cdnOn = null;
            fms.cdnOff = null;
            fms.logicOff = null;
            fms.ban = null;
         }
         super.fmsDestroy();
         updateUMS();
      }
      
      protected function connectToCdn(... rest) : Connection {
         echo("connectToCdn");
         fmsStreamOwner = false;
         if(cdn)
         {
            cdnDestroy();
         }
         cdn = new MultiConnection(null,type.toUpperCase() + " CDN",provider.name == "tcdn"?[1935]:[1935,80,443],true);
         cdn.onFCSubscribe = onCdnSubscribe;
         dispatch("getCdn");
         cdn.addEventListener("connected",onCdnConnected);
         cdn.addEventListener("disconnected",onCdnDisconnected);
         cdn.addEventListener("failed",onCdnFailed);
         cdn.addEventListener("asyncError",onCdnAsyncError);
         cdn.addEventListener("rejected",onCdnReject);
         cdn.addEventListener("idleTimeout",onIdleTimeout);
         if(provider.name == "tcdn")
         {
            resolveTcdnUrl();
         }
         else
         {
            cdn.connect(provider.url);
            statistics.benchmark.providerConnectStart();
         }
         streamOwner = cdn;
         streamStatus = "connecting to cdn";
         return cdn;
      }
      
      private function stripTcdnLoader(param1:Event) : void {
         var _loc2_:URLLoader = param1.target as URLLoader;
         _loc2_.removeEventListener("complete",onTcdnLoadComplete);
         _loc2_.removeEventListener("ioError",onTcdnLoadIoError);
         _loc2_.removeEventListener("securityError",onTcdnLoaderSecurityError);
      }
      
      private function onTcdnLoaderSecurityError(param1:SecurityErrorEvent) : void {
         echo("tcdn urlLoader security error : " + param1);
         stripTcdnLoader(param1);
         providerSwitch(true);
      }
      
      private function onTcdnLoadIoError(param1:IOErrorEvent) : void {
         echo("tcdn urlLoader io error : " + param1);
         stripTcdnLoader(param1);
         providerSwitch(true);
      }
      
      private function onTcdnLoadComplete(param1:Event) : void {
         var _loc2_:* = null;
         var _loc6_:* = false;
         var _loc3_:* = null;
         var _loc4_:* = null;
         var _loc8_:* = null;
         var _loc9_:* = null;
         stripTcdnLoader(param1);
         var _loc5_:URLLoader = param1.target as URLLoader;
         var _loc7_:String = _loc5_.data;
         echo("urlLoader load complete : " + _loc7_);
         if(provider)
         {
            if(provider.name == "uhs_tcdn")
            {
               if(_loc7_.indexOf("://") != -1)
               {
                  _loc2_ = provider.url.split("://")[1].split("/");
                  _loc2_[0] = _loc7_.split("://")[1].split("/")[0];
                  uhsTcdnResolvedUrl = "http://" + _loc2_.join("/");
                  if(uhsTcdnResolvedUrl.substr(-1,1) != "/")
                  {
                     uhsTcdnResolvedUrl = uhsTcdnResolvedUrl + "/";
                  }
                  _loc6_ = false;
                  if(statistics)
                  {
                     _loc6_ = statistics.common.bufferEmpty;
                  }
                  if(stream is ChunkStream && stream.bufferLength < 0.2)
                  {
                     _loc6_ = true;
                  }
                  if(stream is ChunkStream)
                  {
                     if(!(stream as ChunkStream).pattern || (_loc6_))
                     {
                        playChunks();
                     }
                     else
                     {
                        changeUhsPattern(uhsTcdnResolvedUrl + provider.streamName);
                     }
                  }
                  else
                  {
                     createStream();
                  }
               }
               else
               {
                  echo("content has no ://, skipping provider");
                  providerSwitch(true);
               }
            }
            else if(cdn)
            {
               if(_loc7_.indexOf("rtmp://") != -1)
               {
                  _loc3_ = provider.url;
                  if(provider.name == "tcdn")
                  {
                     _loc4_ = _loc7_;
                     _loc4_ = _loc4_.split("rtmp://").join("");
                     _loc8_ = provider.url;
                     _loc8_ = _loc8_.split("rtmp://").join("");
                     _loc9_ = _loc8_.split("/");
                     _loc9_[0] = _loc4_;
                     _loc3_ = "rtmp://" + _loc9_.join("/");
                  }
                  if(streamOwner == cdn)
                  {
                     cdn.connect(_loc3_);
                     statistics.benchmark.providerConnectStart();
                  }
               }
               else
               {
                  providerSwitch(true,_loc7_);
               }
            }
            
         }
      }
      
      private function setupChunks() : Connection {
         echo("setupChunks");
         if(chunks)
         {
            destroyChunks();
         }
         var _loc1_:String = provider.url;
         if((provider as UhsProvider).varnishUrl)
         {
            _loc1_ = (provider as UhsProvider).varnishUrl;
         }
         if(uhsTcdnResolvedUrl && uhsTcdnUrl == provider.url)
         {
            _loc1_ = provider.url;
         }
         chunks = new Connection(_loc1_);
         streamOwner = chunks;
         chunks.addEventListener("connected",onChunkConnected);
         chunks.connect(null);
         statistics.benchmark.providerConnectStart();
         return chunks;
      }
      
      private function destroyChunks() : void {
         if(streamOwner == chunks)
         {
            if(stream)
            {
               destroyStream();
            }
            streamOwner = null;
         }
         uhsTcdnResolvedUrl = null;
         uhsTcdnUrl = null;
         chunks = null;
      }
      
      private function onChunkConnected(param1:Event) : void {
         chunks.removeEventListener("connected",onChunkConnected);
         statistics.benchmark.providerConnectSuccess();
         if(provider)
         {
            echo("streamProvider.name : " + provider.name);
            echo("streamProvider.url : " + provider.url);
            echo("uhsTcdnUrl : " + uhsTcdnUrl);
            if(provider.name.indexOf("_tcdn") == -1)
            {
               echo("onChunkConnected createStream");
               createStream();
            }
            else
            {
               echo("onChunkConnected resolveTcdnUrl");
               resolveTcdnUrl(true);
            }
         }
      }
      
      private function onIdleTimeout(param1:Event) : void {
         echo("onIdleTimeout");
         cdnIdleTimeout = true;
      }
      
      private function onCdnReject(param1:DynamicEvent) : void {
         Debug.echo("cdnRejected");
         streamStatus = "cdn rejected";
         providerSwitch();
      }
      
      private function onCdnAsyncError(... rest) : void {
         Debug.echo("onCdnAsyncError() >> " + (rest && rest[0]?rest[0] as AsyncErrorEvent:""));
         providerSwitch();
      }
      
      protected function onCdnConnected(... rest) : void {
         if(!provider)
         {
            return;
         }
         streamStatus = "connected to cdn";
         dispatch("cdnConnected");
         cdnSubscribes = {};
         statistics.benchmark.providerConnectSuccess();
         if(!(streamName.indexOf("ustream@internap") == -1) && !(provider.url.search(new RegExp("(ustream)$")) == -1))
         {
            echo("cdn connected @ internap -> createStream");
            createStream();
         }
         else if(provider.name == "tcdn")
         {
            echo("cdn connected @ tcdn");
            createStream();
         }
         else
         {
            if(!cdnSubscribeTimer)
            {
               cdnSubscribeTimer = new Timer(1000);
               cdnSubscribeTimer.addEventListener("timer",onCdnSubscribeTimer);
            }
            onCdnSubscribeTimer();
            cdnSubscribeTimer.start();
         }
         
      }
      
      private function onCdnSubscribeTimer(... rest) : void {
         cdnSubscribe(streamName);
      }
      
      protected function onCdnDisconnected(... rest) : void {
         echo("onCdnDisconnected() - planned: " + plannedDisconnected + " - cdnIdleTimeout: " + cdnIdleTimeout);
         if(cdnIdleTimeout && !_playing)
         {
            cdnTimer.start();
         }
         else if(provider is CdnProvider && !plannedDisconnected && !providerSwitch())
         {
            cdnTimer.start();
         }
         
         cdnIdleTimeout = false;
         plannedDisconnected = false;
         if(cdnSubscribeTimer && cdnSubscribeTimer.running)
         {
            cdnSubscribeTimer.reset();
         }
      }
      
      protected function onCdnFailed(... rest) : void {
         dispatch("cdnFailed");
      }
      
      private function onCdnSubscribe(param1:Object, ... rest) : void {
         var _loc3_:* = undefined;
         statistics.benchmark.providerSubscribeSuccess();
         dispatch("cdnSubscribed");
         Debug.explore(param1);
         if(cdnSubscribeTimer)
         {
            cdnSubscribeTimer.stop();
         }
         cdnSubscribes[param1.description] = true;
         streamStatus = "subscribed";
         var _loc4_:Boolean = provider.isTranscoded as Boolean;
         if(param1.code == "NetStream.Play.Start")
         {
            if(stream)
            {
               if(newStreamName)
               {
                  if(stream.bufferLength > 2 && netStreamPlayOptionClass && !_loc4_)
                  {
                     _loc3_ = new netStreamPlayOptionClass();
                     _loc3_.oldStreamName = streamName;
                     streamName = newStreamName;
                     _loc3_.streamName = newStreamName;
                     _loc3_.transition = "switch";
                     echo("streamName = " + streamName + " (play2)");
                     if(_streamOwner.connected)
                     {
                        dispatch("transitionStarted");
                        stream.play2(_loc3_);
                     }
                  }
                  else
                  {
                     streamName = newStreamName;
                     echo("streamName = " + streamName + " (play)");
                     stream.close();
                     extendedBuffer = false;
                     playedStreamName = streamName;
                     stream.play(playedStreamName);
                     newStreamName = null;
                  }
               }
            }
            else
            {
               createStream();
            }
         }
         else if(param1.code == "NetStream.Play.StreamNotFound")
         {
            streamStatus = "subscribe - NetStream.Play.StreamNotFound " + streamName;
            if(!providerSwitch() && provider)
            {
               provider.destroy();
            }
         }
         
         checkCdnSubscriptions();
      }
      
      private function cdnDestroy() : void {
         if(streamOwner == cdn)
         {
            if(stream)
            {
               destroyStream();
            }
            streamOwner = null;
         }
         if(cdn)
         {
            cdn.removeEventListener("connected",onCdnConnected);
            cdn.removeEventListener("disconnected",onCdnDisconnected);
            cdn.removeEventListener("failed",onCdnFailed);
            cdn.removeEventListener("asyncError",onCdnAsyncError);
            cdn.removeEventListener("rejected",onCdnReject);
            cdn.removeEventListener("idleTimeout",onIdleTimeout);
            cdn.kill();
            cdn = null;
            dispatch("cdnDestroy");
         }
         if(cdnTimer && cdnTimer.running)
         {
            cdnTimer.stop();
         }
         if(cdnSubscribeTimer && cdnSubscribeTimer.running)
         {
            cdnSubscribeTimer.stop();
         }
      }
      
      override protected function createStream(param1:String = "connectToFMS") : void {
         super.createStream(param1);
         if(!akamaiHd && !chunks)
         {
            stream.client.goOffline = onFoo;
            stream.client.cdnOff = onFoo;
            stream.client.cdnOn = onFoo;
            stream.client.embedLimitReached = onEmbedLimitReached;
            stream.client.ban = ban;
         }
         updateUMS();
         extendedBuffer = false;
         if(!(stream is ChunkStream))
         {
            _modules.netStream = stream;
         }
         dispatch("createStream");
      }
      
      private function ban(param1:*) : void {
         addAlertToDisplayParent();
         handleRejectAlert(param1);
         dispatch("banned",true,param1);
         destroy();
      }
      
      override protected function onStreamNetStatus(param1:NetStatusEvent) : void {
         var _loc4_:* = null;
         var _loc2_:* = null;
         var _loc3_:* = 0;
         super.onStreamNetStatus(param1);
         var _loc5_:* = param1.info.code;
         if("NetStream.Play.UnpublishNotify" !== _loc5_)
         {
            if("NetStream.Buffer.Full" !== _loc5_)
            {
               if("NetStream.Buffer.Empty" !== _loc5_)
               {
                  if("NetStream.Play.Transition" !== _loc5_)
                  {
                     if("NetStream.Failed" !== _loc5_)
                     {
                        if("NetStream.Play.Failed" !== _loc5_)
                        {
                           if("ChunkStream.Predict.ExcessLag" !== _loc5_)
                           {
                              if("ChunkStream.Load.IoError" !== _loc5_)
                              {
                                 if("ChunkStream.Load.UnrecoverableError" !== _loc5_)
                                 {
                                    if("ChunkStream.Load.FatalError" !== _loc5_)
                                    {
                                       if("ChunkStream.Load.ParserError" !== _loc5_)
                                       {
                                       }
                                    }
                                    _loc4_ = {};
                                    _loc4_["ChunkStream.Load.UnrecoverableError"] = "uhsChunkNotFound";
                                    _loc4_["ChunkStream.Load.FatalError"] = "uhsChunkSecurityError";
                                    _loc4_["ChunkStream.Load.ParserError"] = "uhsChunkMalformedData";
                                    _loc2_ = {"url":param1.info.reason};
                                    if(param1.info.code == "ChunkStream.Load.UnrecoverableError")
                                    {
                                       _loc3_ = 0;
                                       if(_modules.quality)
                                       {
                                          _loc3_ = _modules.quality.availableQualities.indexOf(_modules.quality.desiredQuality);
                                       }
                                       if(lastHashCounts && lastHashCounts.length > _loc3_ && lastHashCounts[_loc3_] > 3)
                                       {
                                          _loc2_.chunkRangeCount = lastHashCounts[_loc3_];
                                       }
                                    }
                                    statistics.failure.report(_loc4_[param1.info.code],provider.name,_loc2_);
                                    if(!chunksVarnishFailed)
                                    {
                                       chunksVarnishFailed = true;
                                       if(modules.dvr && modules.dvr.timeShift)
                                       {
                                          modules.dvr.seek(modules.dvr.seekPercent);
                                       }
                                       else
                                       {
                                          playChunks(true);
                                       }
                                    }
                                    else if(_modules.dvr && _modules.dvr.finished)
                                    {
                                       if(provider)
                                       {
                                          provider.destroy();
                                       }
                                    }
                                    else if(param1.info.code == "ChunkStream.Load.FatalError")
                                    {
                                       providerSwitch(true);
                                    }
                                    else if(!modules.stream.online && modules.dvr && modules.dvr.timeShift)
                                    {
                                       modules.dvr.seek(modules.dvr.seekPercent);
                                    }
                                    else
                                    {
                                       dispatch("pause",false,{"reason":"err"});
                                       dispatch("play");
                                       destroyStream();
                                    }
                                    
                                    
                                    
                                 }
                                 else if(Math.random() <= 0.01)
                                 {
                                    sendLogToUms("CHUNK_LOAD_UNRECOVERABLE_ERROR");
                                 }
                                 
                                 _loc4_ = {};
                                 _loc4_["ChunkStream.Load.UnrecoverableError"] = "uhsChunkNotFound";
                                 _loc4_["ChunkStream.Load.FatalError"] = "uhsChunkSecurityError";
                                 _loc4_["ChunkStream.Load.ParserError"] = "uhsChunkMalformedData";
                                 _loc2_ = {"url":param1.info.reason};
                                 if(param1.info.code == "ChunkStream.Load.UnrecoverableError")
                                 {
                                    _loc3_ = 0;
                                    if(_modules.quality)
                                    {
                                       _loc3_ = _modules.quality.availableQualities.indexOf(_modules.quality.desiredQuality);
                                    }
                                    if(lastHashCounts && lastHashCounts.length > _loc3_ && lastHashCounts[_loc3_] > 3)
                                    {
                                       _loc2_.chunkRangeCount = lastHashCounts[_loc3_];
                                    }
                                 }
                                 statistics.failure.report(_loc4_[param1.info.code],provider.name,_loc2_);
                                 if(!chunksVarnishFailed)
                                 {
                                    chunksVarnishFailed = true;
                                    if(modules.dvr && modules.dvr.timeShift)
                                    {
                                       modules.dvr.seek(modules.dvr.seekPercent);
                                    }
                                    else
                                    {
                                       playChunks(true);
                                    }
                                 }
                                 else if(_modules.dvr && _modules.dvr.finished)
                                 {
                                    if(provider)
                                    {
                                       provider.destroy();
                                    }
                                 }
                                 else if(param1.info.code == "ChunkStream.Load.FatalError")
                                 {
                                    providerSwitch(true);
                                 }
                                 else if(!modules.stream.online && modules.dvr && modules.dvr.timeShift)
                                 {
                                    modules.dvr.seek(modules.dvr.seekPercent);
                                 }
                                 else
                                 {
                                    dispatch("pause",false,{"reason":"err"});
                                    dispatch("play");
                                    destroyStream();
                                 }
                                 
                                 
                                 
                              }
                              else if(_modules && _modules.dvr && _modules.dvr.timeShift <= 5 && _modules.stream && !_modules.stream.hasLiveContent)
                              {
                                 if(provider)
                                 {
                                    provider.destroy();
                                 }
                              }
                              
                           }
                           else
                           {
                              playChunks(true);
                           }
                        }
                     }
                     dispatch("transitionCompleted");
                     dispatch("transitionFailed");
                     if(stream && newStreamName)
                     {
                        streamName = newStreamName;
                        playedStreamName = newStreamName;
                        stream.play(playedStreamName);
                        newStreamName = null;
                     }
                     updateUMS();
                  }
                  else
                  {
                     updateUMS();
                     dispatch("transitionCompleted");
                     newStreamName = null;
                  }
               }
               else if(extendedBuffer)
               {
                  bufferOutCounter = bufferOutCounter + 1;
                  extendedBuffer = false;
               }
               
            }
            else if(!extendedBuffer)
            {
               extendedBuffer = true;
            }
            
         }
         else if(!providerSwitch())
         {
            dispatch("moduleInfo",false,
               {
                  "info":{"stream":[]},
                  "source":"channel"
               });
         }
         
      }
      
      override protected function onStreamNotFound() : void {
         if(!akamaiHd && !providerSwitch() && provider)
         {
            provider.destroy();
         }
      }
      
      private function get extendedBuffer() : Boolean {
         return _extendedBuffer;
      }
      
      private function set extendedBuffer(param1:Boolean) : void {
         if(_modules && _modules.buffer)
         {
            _buffer = _modules.buffer.data;
            echo("extendedBuffer set buffer : " + _buffer);
            if(stream)
            {
               stream.bufferTime = _buffer;
               echo("extendedBuffer stream.bufferTime : " + stream.bufferTime);
            }
            _extendedBuffer = false;
         }
         else if(_buffer && stream)
         {
            _extendedBuffer = param1;
            stream.bufferTime = forcedBuffer == -1?_extendedBuffer?9:3.0:forcedBuffer;
         }
         
      }
      
      override public function set buffer(param1:Number) : void {
         .super.buffer = param1;
         if(param1)
         {
            extendedBuffer = false;
         }
      }
      
      override protected function set streamStatus(param1:String) : void {
         _streamStatus = param1;
         updateUMS();
      }
      
      override protected function set streamOwner(param1:Connection) : void {
         .super.streamOwner = param1;
         if(param1)
         {
            updateUMS();
         }
      }
      
      override public function set video(param1:Boolean) : void {
         _logoCanvas.visible = !(!param1 && _offairContent && _offairContent.pictures && Logic.hasInstance);
         screen.visible = param1;
         .super.video = param1;
      }
      
      private function logicOff(... rest) : void {
         dispatch("logicOff");
         fmsDestroy();
      }
      
      override protected function createModules(... rest) : void {
         if(!_modules)
         {
            super.createModules.apply(this,rest);
            _modules.addEventListener("createLayers",onCreateLayers);
            _modules.addEventListener("createBuffer",onCreateBuffer);
            _modules.addEventListener("createPpv",onCreatePpv);
            _modules.addEventListener("createSmooth",onCreateSmooth);
            _modules.addEventListener("createAgeVerification",onCreateAgeVerification);
            _modules.addEventListener("createAgeLock",onCreateAgeLock);
            _modules.addEventListener("createLogo",onCreateLogo);
            _modules.addEventListener("createRpinlock",onCreateRpinLock);
            _modules.addEventListener("createQuality",onCreateQuality);
            _modules.addEventListener("createDvr",onCreateDvr);
         }
      }
      
      override protected function createUms() : void {
         super.createUms();
         if(_brandId == "1")
         {
            ums.addEventListener("goOffline",goOffline);
         }
      }
      
      private function onCreateRpinLock(param1:Event) : void {
         _modules.removeEventListener("createRpinlock",onCreateRpinLock);
         var _loc2_:RpinLock = _modules.rpinLock;
         if(_loc2_)
         {
            _loc2_.addEventListener("destroy",onRpinLockDestroy);
         }
      }
      
      private function onRpinLockDestroy(param1:Event) : void {
         var _loc2_:RpinLock = param1.target as RpinLock;
         if(_loc2_)
         {
            _loc2_.removeEventListener("destroy",onRpinLockDestroy);
         }
         _modules.addEventListener("createRpinlock",onCreateRpinLock);
      }
      
      private function onCreateLogo(param1:Event) : void {
         var _loc2_:Logo = (param1.target as ViewerModuleManager).logo;
         if(_loc2_)
         {
            _loc2_.live = online;
         }
      }
      
      private function onCreateAgeVerification(param1:Event) : void {
         _modules.removeEventListener("createAgeVerification",onCreateAgeVerification);
         var _loc2_:AgeVerification = _modules.ageVerification;
         if(_loc2_)
         {
            _loc2_.addEventListener("lock",onAgeVerificationLock);
            _loc2_.addEventListener("destroy",onAgeVerificationDestroy);
         }
         destroyStream();
         _streamOwner = null;
      }
      
      private function onAgeVerificationLock(param1:Event) : void {
         var _loc2_:AgeVerification = param1.target as AgeVerification;
         if(_loc2_ && !_loc2_.lock && _modules.stream && _modules.stream.online && provider && !streamOwner)
         {
            connect();
         }
      }
      
      private function onAgeVerificationDestroy(param1:Event) : void {
         var _loc2_:AgeVerification = param1.target as AgeVerification;
         if(_loc2_)
         {
            _loc2_.removeEventListener("lock",onAgeVerificationLock);
            _loc2_.removeEventListener("destroy",onAgeVerificationDestroy);
         }
         _modules.addEventListener("createAgeVerification",onCreateAgeVerification);
         if(_modules.stream && _modules.stream.online && provider && !streamOwner)
         {
            connect();
         }
      }
      
      private function onCreateAgeLock(param1:Event) : void {
         _modules.removeEventListener("createAgeLock",onCreateAgeLock);
         var _loc2_:AgeLock = _modules.ageLock;
         if(_loc2_)
         {
            _loc2_.addEventListener("lock",onAgeLockLock);
            _loc2_.addEventListener("destroy",onAgeLockDestroy);
         }
         if(_streamOwner && stream && type == "channel")
         {
            destroyStream();
            plannedDisconnected = true;
            _streamOwner.close();
            _streamOwner = null;
         }
      }
      
      private function onAgeLockLock(param1:Event) : void {
         var _loc2_:AgeLock = param1.target as AgeLock;
         if(_loc2_ && !_loc2_.lock && _modules.stream && _modules.stream.online && provider && !streamOwner)
         {
            connect();
         }
      }
      
      private function onAgeLockDestroy(param1:Event) : void {
         var _loc2_:AgeLock = param1.target as AgeLock;
         if(_loc2_)
         {
            _loc2_.removeEventListener("lock",onAgeLockLock);
            _loc2_.removeEventListener("destroy",onAgeLockDestroy);
         }
         _modules.addEventListener("createAgeLock",onCreateAgeLock);
         if(_modules.stream && _modules.stream.online && provider && !streamOwner)
         {
            connect();
         }
      }
      
      private function onModuleLock(param1:Event) : void {
         echo("onModuleLock");
         var _loc2_:Module = param1.target as Module;
         if(_loc2_ && _loc2_.lock)
         {
            echo("module lock on " + _loc2_);
            if(_streamOwner)
            {
               plannedDisconnected = true;
               _streamOwner.close();
               _streamOwner = null;
               screen.attachNetStream(null);
               screen.clear();
            }
            if(fms)
            {
               echo("closing fms connection");
               fms.kill();
               fms = null;
            }
         }
      }
      
      private function onCreatePpv(param1:Event) : void {
         _modules.ppv.addEventListener("update",onPpvUpdate);
         _modules.ppv.addEventListener("destroy",onPpvDestroy);
         _modules.ppv.addEventListener("lock",onModuleLock);
      }
      
      private function onPpvDestroy(param1:Event) : void {
         var _loc2_:Ppv = param1.target as Ppv;
         if(_loc2_)
         {
            _loc2_.removeEventListener("update",onPpvUpdate);
            _loc2_.removeEventListener("destroy",onPpvDestroy);
            _loc2_.removeEventListener("lock",onModuleLock);
         }
      }
      
      private function onPpvUpdate(param1:Event) : void {
         if(!(_modules.ppv.status == "pre") && ads)
         {
            destroyAdverts();
         }
         updateUMS();
      }
      
      private function onCreateSmooth(... rest) : void {
         _modules.smooth.addEventListener("update",onSmoothUpdate);
      }
      
      private function onSmoothUpdate(... rest) : void {
         smooth = _modules.smooth.value;
      }
      
      private function onCreateBuffer(... rest) : void {
         _modules.buffer.addEventListener("update",onBufferUpdate);
      }
      
      private function onBufferUpdate(... rest) : void {
         if(buffer != _modules.buffer.data)
         {
            buffer = _modules.buffer.data;
         }
      }
      
      private function onCreateLayers(... rest) : void {
         layers.addChild(_modules.layers.display);
         layers.addEventListener("setSize",onLayersSetSize);
         _modules.layers.muted = _muted;
         _modules.layers.volume = _volume;
         _modules.layers.playing = _playing;
         _modules.layers.addEventListener("add",chkCohostlayer);
         _modules.layers.addEventListener("update",chkCohostlayer);
         _modules.layers.addEventListener("destroy",onLayersDestroy);
      }
      
      private function onLayersSetSize(param1:DynamicEvent) : void {
         trace("onMonitor set size");
         locker.resize(param1.width,param1.height);
         display.update();
      }
      
      private function onLayersDestroy(param1:Event = null) : void {
         echo("onLayersDestroy");
         buffer = buffer;
         if(takeover)
         {
            takeover = false;
            _offairContent = {};
         }
         var _loc2_:Layers = param1.target as Layers;
         if(_loc2_)
         {
            if(layers.contains(_loc2_.display))
            {
               layers.removeChild(_loc2_.display);
            }
            layers.removeEventListener("setSize",onLayersSetSize);
            _loc2_.removeEventListener("add",chkCohostlayer);
            _loc2_.removeEventListener("update",chkCohostlayer);
            _loc2_.removeEventListener("destroy",onLayersDestroy);
         }
      }
      
      private function chkCohostlayer(param1:DynamicEvent) : void {
         var _loc2_:* = null;
         if(_modules.cohost && _modules.cohost.psid && _modules.cohost.psid == param1.id && param1.layer.audio)
         {
            _loc2_ = {};
            _loc2_[_modules.cohost.psid] = {"audio":false};
            _modules.layers.infoGate({"update":_loc2_});
         }
         if(param1.layer.type == "viewer" || param1.layer.type == "channel")
         {
            buffer = 0;
         }
         if(param1.layer.takeover)
         {
            takeover = true;
            offairContentDisabled = true;
            _offairContent = {};
         }
      }
      
      private function updateUMS() : void {
         var _loc1_:* = null;
         if(ums)
         {
            _loc1_ = "";
            if(reportRpin)
            {
               _loc1_ = _loc1_ + (shared.rpin + " | ");
            }
            _loc1_ = _loc1_ + (_status == 2?"online":_status == 1?"dvr":"offline");
            _loc1_ = _loc1_ + (" | " + ("FMS:" + (fms?"1":"0") + " CDN:" + (cdn?"1":"0") + " UHS:" + (chunks?"1":"0") + " AHD:" + (akamaiHd?"1":"0")));
            if(streamOwner)
            {
               if(streamOwner.label && !(streamOwner.label.indexOf("://") == -1))
               {
                  _loc1_ = _loc1_ + (" | " + streamOwner.label);
               }
               else
               {
                  _loc1_ = _loc1_ + (" | " + streamOwner.url);
               }
            }
            if(streamName)
            {
               _loc1_ = _loc1_ + (" | " + streamName);
            }
            if(_streamStatus)
            {
               _loc1_ = _loc1_ + (" | " + _streamStatus);
            }
            ums.status = _loc1_;
            if(_modules.logicloadtest)
            {
               _modules.logicloadtest.status = _loc1_;
            }
         }
      }
      
      override protected function onPpvGoPaid(param1:DynamicEvent) : void {
         echo("onPpvGoPaid connect");
         if(online && gwResult && !gwResult.reject)
         {
            connect();
         }
      }
      
      override protected function onPpvGoFree(param1:Event = null) : void {
         echo("onPpvGoFree connect");
         if(online && gwResult && !gwResult.reject)
         {
            connect();
         }
      }
      
      override public function set volume(param1:Number) : void {
         if(_modules && _modules.layers)
         {
            _modules.layers.volume = param1;
         }
         .super.volume = param1;
         if(im360)
         {
            im360["volume"] = param1;
         }
      }
      
      override public function set playing(param1:Boolean) : void {
         .super.playing = param1;
         if(_modules && _modules.layers)
         {
            _modules.layers.playing = param1;
         }
      }
      
      override public function set muted(param1:Boolean) : void {
         .super.muted = param1;
         if(_modules && _modules.layers)
         {
            _modules.layers.muted = param1;
         }
      }
      
      override protected function needAdvert() : Boolean {
         return online && (!layer || layer && !layer.connected);
      }
      
      public function get offairContent() : Object {
         return _offairContent;
      }
      
      override public function get url() : String {
         var _loc1_:String = "http://www.ustream.tv/";
         if(Logic.hasInstance && Logic.instance.scid)
         {
            _loc1_ = _loc1_ + ("schannel/" + Logic.instance.scid + "/channel/" + _mediaId);
         }
         else
         {
            _loc1_ = _loc1_ + ("channel/" + mediaId);
         }
         if(_modules && _modules.rpinLock)
         {
            _loc1_ = "http://broadcastforfriends.com/video/" + _mediaId;
         }
         if((["11569027","6148198"] as Array).indexOf(_id) > -1)
         {
            _loc1_ = "http://www.wtmx.com/ustream.php";
         }
         return _loc1_;
      }
      
      public function get featured() : Boolean {
         return gwResult?gwResult.isFeaturedOnUstream:false;
      }
      
      public function get online() : Boolean {
         return _status > 0?true:false;
      }
      
      override protected function onMetaUpdate(param1:Event) : void {
         if(_modules.meta.data.offairContent)
         {
            _offairContent = _modules.meta.data.offairContent;
         }
         super.onMetaUpdate(param1);
         if(_status == 0)
         {
            dispatchEvent(new Event("offairContentInfo"));
         }
      }
      
      override protected function onRendererChange(param1:Event) : void {
         ignoreNextLowBandwidth = true;
         echo("ignoreNextLowBandwidth true");
         var _loc2_:Timer = new Timer(5000,1);
         _loc2_.addEventListener("timerComplete",onIgnoreClearTimer);
         _loc2_.start();
         if(stream && _playing)
         {
            if(stream is ChunkStream)
            {
               (stream as ChunkStream).reset();
            }
            else
            {
               extendedBuffer = false;
               stream.pause();
               stream.resume();
            }
         }
      }
      
      private function onIgnoreClearTimer(param1:TimerEvent) : void {
         var _loc2_:Timer = param1.target as Timer;
         _loc2_.removeEventListener("timerComplete",onIgnoreClearTimer);
         ignoreNextLowBandwidth = false;
         echo("ignoreNextLowBandwidth false (onIgnoreClearTimer)");
      }
      
      public function get status() : int {
         return _status;
      }
      
      override protected function onQosStreamStalled(param1:Event) : void {
         if(!ignoreNextLowBandwidth)
         {
            super.onQosStreamStalled(param1);
         }
         else
         {
            echo("onQosStreamStalled ignored");
            ignoreNextLowBandwidth = false;
         }
      }
      
      private function onCreateQuality(param1:Event) : void {
         var _loc2_:ViewerModuleManager = param1.target as ViewerModuleManager;
         if(_loc2_ && _loc2_.quality)
         {
            _loc2_.quality.addEventListener("adaptive",onQualityAdaptive);
            _loc2_.quality.addEventListener("destroy",onQualityDestroy);
         }
      }
      
      private function onQualityAdaptive(param1:Event) : void {
         checkCdnSubscriptions();
      }
      
      private function onQualityDestroy(param1:Event) : void {
         var _loc2_:Quality = param1.target as Quality;
         if(_loc2_)
         {
            _loc2_.removeEventListener("adaptive",onQualityAdaptive);
            _loc2_.removeEventListener("destroy",onQualityDestroy);
         }
      }
      
      private function checkCdnSubscriptions() : void {
         var _loc1_:* = false;
         var _loc2_:* = null;
         var _loc3_:* = 0;
         if(cdnSubscribes)
         {
            _loc1_ = _modules && _modules.quality && _modules.quality.adaptiveAvailable && _modules.quality.adaptiveMbr;
            _loc2_ = provider.streams as Array;
            if(_loc1_ && _loc2_)
            {
               _loc3_ = 0;
               while(_loc3_ < _loc2_.length)
               {
                  if(!cdnSubscribes[_loc2_[_loc3_].streamName])
                  {
                     cdnSubscribe(_loc2_[_loc3_].streamName);
                     break;
                  }
                  _loc3_++;
               }
            }
         }
      }
      
      private function cdnSubscribe(param1:String) : void {
         if(cdn && cdn.connected)
         {
            dispatch("cdnSubscribe");
            echo("call FCSubscribe @ " + cdn.uri + " streamName " + param1);
            streamStatus = "call subscribe";
            cdn.call("FCSubscribe",null,param1);
            statistics.benchmark.providerSubscribeStart();
         }
      }
      
      private function resolveTcdnUrl(param1:Boolean = false) : void {
         echo("loading tempCdn data");
         var _loc2_:String = This.secure?"https":"http";
         var _loc4_:String = _loc2_ + "://tcdn.ustream.tv/" + mediaId + "/";
         if(param1)
         {
            uhsTcdnUrl = provider.url;
            _loc4_ = _loc4_ + "?protocol=http";
            echo("resolveTcdnUrl uhs true, url : " + _loc4_);
         }
         var _loc3_:URLLoader = new URLLoader(new URLRequest(_loc4_));
         _loc3_.addEventListener("complete",onTcdnLoadComplete);
         _loc3_.addEventListener("ioError",onTcdnLoadIoError);
         _loc3_.addEventListener("securityError",onTcdnLoaderSecurityError);
      }
      
      protected function onCreateDvr(param1:Event) : void {
         var _loc2_:* = null;
         if(param1 && param1.target is ViewerModuleManager)
         {
            _loc2_ = (param1.target as ViewerModuleManager).dvr;
            if(_loc2_)
            {
               _loc2_.addEventListener("dvr.out",onDvrOut);
               _loc2_.addEventListener("destroy",onDvrDestroy);
            }
         }
      }
      
      protected function onDvrDestroy(param1:Event) : void {
         var _loc2_:* = null;
         if(param1 && param1.target is Dvr)
         {
            _loc2_ = param1.target as Dvr;
            _loc2_.removeEventListener("dvr.out",onDvrOut);
            _loc2_.removeEventListener("destroy",onDvrDestroy);
         }
         if(_active && !_modules.stream.online)
         {
            goOffline();
         }
      }
      
      private function onDvrOut(param1:DynamicEvent) : void {
         if(provider && _modules && _modules.stream && !_modules.stream.hasLiveContent)
         {
            provider = provider.destroy();
         }
      }
      
      override public function seek(param1:Number, param2:Boolean = false) : Boolean {
         echo("seek");
         if(_modules.dvr)
         {
            if(_modules.dvr.contentLength > 0)
            {
               _modules.dvr.seek(param1);
            }
         }
         updateStatus();
         return false;
      }
      
      override public function get progress() : Number {
         if(_modules && _modules.dvr)
         {
            return _modules.dvr.seekPercent;
         }
         return 1;
      }
      
      override public function get duration() : Number {
         if(_modules && _modules.dvr)
         {
            return _modules.dvr.contentLength;
         }
         return 0;
      }
      
      public function get timeShift() : Number {
         if(_modules && _modules.dvr)
         {
            return _modules.dvr.timeShift;
         }
         return 0;
      }
      
      override protected function onCreateStream(... rest) : void {
         super.onCreateStream.apply(this,rest);
         var _loc2_:Stream = _modules.stream;
         if(_loc2_)
         {
            _loc2_.addEventListener("statusChange",onStreamStatusChange);
         }
      }
      
      override protected function onStreamDestroy(param1:Event) : void {
         super.onStreamDestroy(param1);
         var _loc2_:Stream = param1.target as Stream;
         if(_loc2_)
         {
            _loc2_.removeEventListener("statusChange",onStreamStatusChange);
         }
      }
      
      private function onStreamStatusChange(param1:Event) : void {
         var _loc3_:* = false;
         var _loc2_:Stream = param1.target as Stream;
         if(_loc2_)
         {
            _loc3_ = _loc2_.status == 1;
            if(online && !_loc3_)
            {
               goOffline();
            }
            if(!online && (_loc3_))
            {
               goOnline();
               if(adfreeStatus == 0)
               {
                  getGeoAds();
               }
            }
         }
         updateStatus();
      }
      
      override protected function onStreamProviderCreated(param1:Event) : void {
         echo("onStreamProviderCreated");
         updateStatus();
         if(cdnTimer && cdnTimer.running)
         {
            cdnTimer.reset();
         }
         var _loc2_:Provider = (param1.target as Stream).streamProvider;
         if(_loc2_ is UhsProvider)
         {
            _loc2_.addEventListener("chunkIdChange",onUhsChunkIdChange);
            _loc2_.addEventListener("chunkRangeChange",onUhsChunkRangeChange);
         }
         super.onStreamProviderCreated(param1);
         if(provider.streamId)
         {
            streamId = provider.streamId;
         }
      }
      
      override protected function onStreamProviderDestroy(param1:Event) : void {
         super.onStreamProviderDestroy(param1);
         var _loc2_:Provider = param1.target as Provider;
         if(_loc2_ is UhsProvider)
         {
            _loc2_.removeEventListener("chunkIdChange",onUhsChunkIdChange);
            _loc2_.removeEventListener("chunkRangeChange",onUhsChunkRangeChange);
         }
         fmsDestroy();
         cdnDestroy();
         destroyAkamaiHd();
         destroyIm360();
         destroyChunks();
         updateStatus();
      }
      
      private function onUhsChunkIdChange(param1:Event) : void {
         echo("onUhsChunkIdChange");
         if(_streamOwner && !(stream is ChunkStream))
         {
            createStream();
         }
      }
      
      private function onUhsChunkRangeChange(param1:Event) : void {
         echo("onUhsChunkRangeChange");
         if(stream is ChunkStream)
         {
            updateChunkHashRange();
            if(!(stream as ChunkStream).playing && (_playing))
            {
               playChunks();
            }
         }
      }
      
      private function updateStatus() : void {
         echo("updateStatus");
         var _loc2_:Boolean = _modules && _modules.stream && _modules.stream.hasLiveContent;
         var _loc1_:* = progress < 1;
         echo("streamOnline : " + _loc2_);
         echo("dvrSeek : " + _loc1_);
         echo("progress : " + progress);
         if(_modules && _modules.stream)
         {
            if(_loc1_)
            {
               if(_status != 1)
               {
                  _status = 1;
                  dispatch("online",false,{"mediaId":_mediaId});
                  dispatch("status");
               }
            }
            else if(_status != (_loc2_?2:0))
            {
               _status = _loc2_?2:0;
               if(_loc2_)
               {
                  if(geoAds)
                  {
                     createAdverts(geoAds);
                  }
               }
               else
               {
                  destroyAdverts();
               }
               dispatch(_loc2_?"online":"offline",false,{"mediaId":_mediaId});
               dispatch("status");
            }
            
         }
         if(_modules && _modules.logo)
         {
            _modules.logo.live = _status == 2;
         }
      }
      
      override protected function onStreamProviderUrlChange(param1:Event) : void {
         var _loc5_:* = null;
         var _loc3_:* = null;
         var _loc6_:* = null;
         var _loc2_:* = null;
         var _loc4_:* = false;
         echo("onStreamProviderUrlChange");
         if(streamOwner && provider is UhsProvider)
         {
            if(!streamOwner.label)
            {
               echo("no streamowner.label",3);
               connect();
               return;
            }
            _loc5_ = streamOwner.label.split("://")[1].split("/")[0];
            _loc3_ = provider.url.split("://")[1].split("/")[0];
            _loc6_ = null;
            if((provider as UhsProvider).varnishUrl)
            {
               _loc6_ = (provider as UhsProvider).varnishUrl.split("://")[1];
               if(_loc6_)
               {
                  _loc6_ = _loc6_.split("/")[0];
               }
            }
            _loc2_ = null;
            if(uhsTcdnResolvedUrl && uhsTcdnUrl == provider.url)
            {
               _loc2_ = uhsTcdnResolvedUrl.split("://")[1].split("/")[0];
            }
            echo("ownerDomain : " + _loc5_);
            echo("providerDomain : " + _loc3_);
            echo("varnishDomain : " + _loc6_);
            echo("tcdnDomain : " + _loc2_);
            if(!(_loc5_ == _loc3_) && !(_loc5_ == _loc6_) && !(_loc5_ == _loc2_))
            {
               streamOwner.label = "";
               updateUMS();
               _loc4_ = false;
               if(statistics)
               {
                  _loc4_ = statistics.common.bufferEmpty;
               }
               if(stream is ChunkStream && stream.bufferLength < 0.2)
               {
                  _loc4_ = true;
               }
               if(!(stream is ChunkStream) || (stream is ChunkStream && (!(stream as ChunkStream).pattern || (_loc4_))))
               {
                  setupChunks();
               }
               else if(provider.name && provider.name.indexOf("_tcdn") == -1)
               {
                  changeUhsPattern(provider.url + provider.streamName);
               }
               else
               {
                  resolveTcdnUrl(true);
               }
               
            }
         }
         else
         {
            connect();
         }
      }
      
      private function changeUhsPattern(param1:String) : void {
         echo("changing pattern on stream",2);
         echo("ChunkStream bufferLength : " + stream.bufferLength);
         statistics.common.providerName = provider.name;
         var _loc2_:Array = param1.split("/");
         _loc2_.pop();
         streamOwner.label = _loc2_.join("/");
         updateUMS();
         echo("pattern = " + param1,2);
         echo("streamOwner.label = " + streamOwner.label,2);
         (stream as ChunkStream).pattern = param1;
         if(_modules.qos)
         {
            _modules.qos.reset();
         }
      }
      
      override protected function onStreamProviderNameChange(param1:Event) : void {
         var _loc2_:* = undefined;
         echo("onStreamProviderNameChange");
         if(provider is UhsProvider)
         {
            echo("playing new streamName : " + provider.streamName,0);
            playChunks();
         }
         else
         {
            if(provider is CdnProvider && ((provider as CdnProvider).needsSubscription) && (!cdnSubscribes || cdnSubscribes && !cdnSubscribes[provider.streamName]))
            {
               dispatch("cdnSubscribe");
               echo("call FCSubscribe @ " + cdn.uri + " streamName " + provider.streamName);
               streamStatus = "call subscribe";
               if(netStreamPlayOptionClass)
               {
                  newStreamName = provider.streamName;
               }
               else
               {
                  streamName = provider.streamName;
               }
               if(netStreamPlayOptionClass)
               {
                  cdn.call("FCSubscribe",null,provider.streamName);
                  statistics.benchmark.providerSubscribeStart();
               }
               else
               {
                  cdn.call("FCSubscribe",null,provider.streamName);
                  statistics.benchmark.providerSubscribeStart();
               }
            }
            else if(stream && stream.bufferLength > 2 && netStreamPlayOptionClass && !provider.isTranscoded)
            {
               echo("changing cdn stream");
               _loc2_ = new netStreamPlayOptionClass();
               _loc2_.oldStreamName = streamName;
               newStreamName = provider.streamName;
               streamName = provider.streamName;
               _loc2_.streamName = provider.streamName;
               _loc2_.transition = "switch";
               echo("streamName = " + streamName + " (play2)");
               if(_streamOwner.connected)
               {
                  dispatch("transitionStarted");
                  stream.play2(_loc2_);
               }
            }
            else
            {
               streamName = provider.streamName;
               echo("streamName = " + streamName + " (play)");
               if(stream)
               {
                  stream.close();
                  extendedBuffer = false;
                  playedStreamName = streamName;
                  stream.play(playedStreamName);
               }
               else if(_streamOwner.connected)
               {
                  createStream();
               }
               
            }
            
            if(provider is CdnProvider && ((provider as CdnProvider).needsSubscription) && (!cdnSubscribes || cdnSubscribes && !cdnSubscribes[provider.streamName]))
            {
            }
         }
         streamName = provider.streamName;
         updateUMS();
      }
      
      override protected function onUmsDisconnect(param1:Event) : void {
         createUmsFallback();
         super.onUmsDisconnect(param1);
      }
      
      protected function playChunks(param1:Boolean = false) : void {
         var _loc3_:* = null;
         var _loc4_:* = null;
         var _loc2_:* = null;
         if(stream is ChunkStream)
         {
            echo("playChunks");
            if(provider)
            {
               echo("varnishUrl : " + (provider as UhsProvider).varnishUrl);
               echo("url : " + (provider as UhsProvider).url);
               echo("uhsTcdnUrl : " + uhsTcdnUrl);
               echo("uhsTcdnResolvedUrl : " + uhsTcdnResolvedUrl);
               echo("chunksVarnishFailed : " + chunksVarnishFailed);
               if((provider as UhsProvider).varnishUrl && !chunksVarnishFailed)
               {
                  streamOwner.label = (provider as UhsProvider).varnishUrl;
               }
               else if(uhsTcdnResolvedUrl && uhsTcdnUrl == (provider as UhsProvider).url)
               {
                  streamOwner.label = uhsTcdnResolvedUrl;
               }
               else
               {
                  streamOwner.label = (provider as UhsProvider).url;
               }
               
               _loc3_ = new ChunkStreamPlayOptions();
               _loc3_.streamName = streamOwner.label + (provider as UhsProvider).streamName;
               statistics.common.streamUrl = _loc3_.streamName;
               _loc3_.index = (provider as UhsProvider).chunkId;
               _loc3_.byteOffset = (provider as UhsProvider).offset;
               _loc3_.duration = (provider as UhsProvider).chunkTime;
               _loc3_.timeOffset = Math.max(0,(provider as UhsProvider).offsetInMs);
               if(_modules.dvr)
               {
                  _modules.dvr.lastChunk = (provider as UhsProvider).chunkId;
               }
               updateChunkHashRange();
               if(_playing || !(stream as ChunkStream).pattern)
               {
                  echo("### 1 playing",0);
                  echo("### 2 dvr, timeshift, param",0);
                  if(!(stream as ChunkStream).playing || (param1))
                  {
                     echo("### 3 public play",0);
                     stream.play(_loc3_);
                  }
                  else if((stream as ChunkStream).pattern != _loc3_.streamName)
                  {
                     echo("### 4 public play2",0);
                     _loc4_ = (stream as ChunkStream).pattern.split("streams/")[0];
                     _loc2_ = _loc3_.streamName.split("streams/")[0];
                     if(_loc4_ == _loc2_)
                     {
                        _loc3_.transition = (stream as ChunkStream).switchEnabled?"switch":"swap";
                        _loc3_.oldStreamName = (stream as ChunkStream).pattern;
                        (stream as ChunkStream).play2(_loc3_);
                     }
                     else
                     {
                        (stream as ChunkStream).play(_loc3_);
                     }
                  }
                  
               }
            }
            else
            {
               echo("no provider",2);
            }
         }
         else
         {
            echo("stream is not ChunkStream",3);
         }
      }
      
      private var lastHashCounts:Vector.<int>;
      
      protected function updateChunkHashRange() : void {
         var _loc2_:* = null;
         var _loc1_:* = null;
         var _loc3_:* = 0;
         if(provider is UhsProvider)
         {
            echo("updateChunkHashRange");
            lastHashCounts = new Vector.<int>();
            if(provider.streams is Array)
            {
               _loc1_ = provider.streams as Array;
               _loc3_ = 0;
               while(_loc3_ < _loc1_.length)
               {
                  lastHashCounts.push(0);
                  if(_loc1_[_loc3_].chunkRange)
                  {
                     _loc8_ = 0;
                     _loc7_ = _loc1_[_loc3_].chunkRange;
                     for(_loc2_ in _loc1_[_loc3_].chunkRange)
                     {
                        (stream as ChunkStream).addHashRange(streamOwner.label + _loc1_[_loc3_].streamName,_loc2_,_loc1_[_loc3_].chunkRange[_loc2_]);
                        _loc4_ = lastHashCounts;
                        _loc5_ = lastHashCounts.length - 1;
                        _loc6_ = _loc4_[_loc5_] + 1;
                        _loc4_[_loc5_] = _loc6_;
                     }
                  }
                  _loc3_++;
               }
            }
            echo("lastHashCounts");
            Debug.explore(lastHashCounts);
            if((provider as UhsProvider).chunkRange)
            {
               _loc10_ = 0;
               _loc9_ = (provider as UhsProvider).chunkRange;
               for(_loc2_ in (provider as UhsProvider).chunkRange)
               {
                  (stream as ChunkStream).addHashRange(streamOwner.label + (provider as UhsProvider).streamName,_loc2_,(provider as UhsProvider).chunkRange[_loc2_]);
               }
            }
         }
      }
      
      override protected function handleStreamTypes(param1:String = "connectToFMS") : void {
         if(_streamOwner == chunks)
         {
            stream = new ChunkStream(streamOwner);
            stream.client = {};
            chunksVarnishFailed = false;
         }
         else
         {
            super.handleStreamTypes();
         }
      }
      
      override protected function handleStreamTypes2() : void {
         if(stream is ChunkStream)
         {
            playChunks();
            if(ums)
            {
               ums.status = _streamOwner.label;
            }
         }
         else
         {
            super.handleStreamTypes2();
         }
      }
      
      override protected function handleStreamTypes3() : void {
         if(stream is ChunkStream)
         {
            if(_modules.dvr && !_modules.dvr.playing)
            {
               seek(_modules.dvr.seekPercent);
            }
         }
         else
         {
            super.handleStreamTypes3();
         }
      }
   }
}
