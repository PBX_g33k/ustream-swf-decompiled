package tv.ustream.viewer.logic.media
{
   import flash.display.DisplayObject;
   import flash.utils.Timer;
   import tv.ustream.tween.Tween;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import flash.events.Event;
   import tv.ustream.viewer.logic.modules.Logo;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.IOErrorEvent;
   
   public class SlideShow extends Media
   {
      
      public function SlideShow(param1:Array = null, param2:Channel = null) {
         items = [];
         super("SlideShow");
         display.scaleMode = "auto";
         locker.addChild(display);
         locker.addChild(_logoCanvas);
         this.owner = param2;
         if(this.owner)
         {
            param2.modules.addEventListener("createLogo",onOwnerCreateLogo);
            param2.modules.addEventListener("destroyLogo",onOwnerDestroyLogo);
            if(param2.modules.logo)
            {
               onOwnerCreateLogo();
            }
         }
         _media = locker;
         locker.close();
         this.items = param1 || [];
         tweenIn = new Tween();
         tweenIn.equation = "easeInSine";
         tweenIn.duration = 20;
         tweenIn.addEventListener("tween",onTweenIn);
         tweenOut = new Tween();
         tweenOut.duration = 20;
         tweenOut.addEventListener("tween",onTweenOut);
         getNextTimer = new Timer(4000,0);
         getNextTimer.addEventListener("timer",next);
      }
      
      public var items:Array;
      
      private var index:Number = 0;
      
      private var _item:Object;
      
      private var lastItem:DisplayObject;
      
      private var getNextTimer:Timer;
      
      private var tweenIn:Tween;
      
      private var tweenOut:Tween;
      
      private var _playing:Boolean = false;
      
      private var _hasRecorded:Boolean = false;
      
      public var imagesOnly:Boolean = false;
      
      private var owner:Channel;
      
      private var isCurrentRecordItem:Boolean = false;
      
      public var manager:ViewerModuleManager;
      
      private var videoCounter:uint = 0;
      
      private function onOwnerCreateLogo(param1:Event = null) : void {
         var _loc2_:Logo = owner.modules.logo;
         _loc2_.addEventListener("update",onOwnerUpdateLogo);
         if(!manager)
         {
            manager = new ViewerModuleManager(this);
            manager.addEventListener("createLogo",onCreateLogo);
         }
         onOwnerUpdateLogo();
      }
      
      private function onOwnerUpdateLogo(param1:Event = null) : void {
         var _loc2_:Logo = owner.modules.logo;
         dispatch("moduleInfo",false,
            {
               "info":{"logo":(_loc2_ && _loc2_.logos.length?_loc2_.logos:false)},
               "source":"monitor"
            });
         if(manager && manager.logo)
         {
            manager.logo.live = owner.online;
         }
      }
      
      private function onOwnerDestroyLogo(param1:Event) : void {
         param1.target.logo.removeEventListener("update",onOwnerUpdateLogo);
         manager.removeEventListener("createLogo",onCreateLogo);
         manager = manager.destroy();
      }
      
      private function onCreateLogo(param1:Event) : void {
         manager.logo.canvas = _logoCanvas;
         manager.logo.live = owner.online;
      }
      
      public function next(... rest) : void {
         var _loc2_:* = NaN;
         if(items.length)
         {
            _loc2_ = index;
            index = index + 1;
            if(index + 1 >= items.length)
            {
               index = 0;
               if(_hasRecorded)
               {
                  index = _loc2_;
                  _hasRecorded = false;
                  _playing = false;
                  return;
               }
            }
            if(_loc2_ != index)
            {
               getItem();
               dispatch("next");
            }
            else
            {
               getNextTimer.start();
            }
         }
      }
      
      public function prev(... rest) : void {
         if(items.length)
         {
            index = index - 1;
            if(index - 1 < 0)
            {
               index = items.length - 1;
            }
            getItem();
            dispatch("prev");
         }
      }
      
      public function start() : void {
         getItem();
      }
      
      public function stop() : void {
         if(_item && _item.loader && !(_item.loader.loader.contentLoaderInfo.bytesLoaded == _item.loader.loader.contentLoaderInfo.bytesTotal) && _item.close)
         {
            _item.close();
         }
         if(getNextTimer.running)
         {
            getNextTimer.stop();
         }
         _playing = false;
      }
      
      override public function destroy(... rest) : * {
         stop();
         removeItem(true);
         if(Logic.hasInstance)
         {
            Logic.instance.secondPreroll = false;
         }
         return super.destroy();
      }
      
      private function onTweenIn(param1:Event) : void {
         _item.loader.alpha = tweenIn.value;
      }
      
      private function onTweenOut(param1:Event) : void {
         if(lastItem)
         {
            lastItem.alpha = tweenOut.value;
            if(!tweenOut.value && (display.contains(lastItem)))
            {
               display.removeChild(lastItem);
            }
         }
      }
      
      private function removeItem(param1:Boolean = false) : void {
         tweenIn.stop();
         if(_item)
         {
            _loc2_ = _item.type;
            if("image" !== _loc2_)
            {
               if("recorded" === _loc2_)
               {
                  lastItem = null;
                  dispatch("destroyRecorded");
               }
            }
            else if(display.contains(_item.loader))
            {
               if(lastItem && display.contains(lastItem))
               {
                  display.removeChild(lastItem);
               }
               if(param1)
               {
                  display.removeChild(_item.loader);
               }
               else
               {
                  lastItem = _item.loader;
                  tweenOut.begin = lastItem.alpha;
                  tweenOut.change = -1;
                  tweenOut.start();
               }
            }
            
         }
      }
      
      private function getItem() : void {
         stop();
         removeItem();
         if(!items.length)
         {
            return;
         }
         _playing = true;
         if(items[index] is String)
         {
            items[index] = 
               {
                  "type":"image",
                  "url":items[index]
               };
         }
         if(items[index].count != null)
         {
            if(!items[index].count)
            {
               items[index].count--;
            }
         }
         _item = items[index];
         if(_item.type == "image")
         {
            isCurrentRecordItem = false;
            _logoCanvas.visible = true;
            if(_item.loader)
            {
               showImage();
            }
            else
            {
               _item.loader = new Slide(_item.url);
               _item.loader.loader.contentLoaderInfo.addEventListener("complete",onItemComplete);
               _item.loader.loader.contentLoaderInfo.addEventListener("ioError",onItemIoError);
            }
         }
         else if(imagesOnly)
         {
            isCurrentRecordItem = false;
            _item = null;
            next();
         }
         else if(_item.type == "recorded")
         {
            videoCounter = videoCounter + 1;
            if(videoCounter > 1 && (Logic.hasInstance) && !Logic.instance.secondPreroll)
            {
               Logic.instance.secondPreroll = true;
            }
            _hasRecorded = true;
            _logoCanvas.visible = false;
            isCurrentRecordItem = true;
            if(lastItem && display.contains(lastItem))
            {
               display.removeChild(lastItem);
            }
            dispatch("createRecorded");
         }
         
         
      }
      
      private function showImage() : void {
         display.addChild(_item.loader);
         onLockerResize();
         _item.loader.alpha = 0;
         tweenIn.begin = 0;
         tweenIn.change = 1;
         tweenIn.start();
         if(_playing)
         {
            getNextTimer.start();
         }
      }
      
      private function onItemComplete(param1:Event) : void {
         if(active && _item && _item.loader && _item.loader.loader.contentLoaderInfo == param1.target)
         {
            showImage();
         }
      }
      
      private function onItemIoError(param1:IOErrorEvent) : void {
         next();
      }
      
      override protected function onLockerResize(param1:Event = null) : void {
         display.width = locker.width;
         display.height = locker.height;
         super.onLockerResize(param1);
      }
      
      public function get item() : Object {
         return _item;
      }
      
      public function nextRecorded() : void {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         if(items.length && !imagesOnly)
         {
            _loc1_ = index;
            if(_loc1_ >= items.length - 1)
            {
               _loc1_ = 0;
            }
            _loc2_ = _loc1_ + 1;
            while(_loc2_ < items.length)
            {
               if(!(items[_loc2_] is String))
               {
                  if(items[_loc2_].type == "recorded")
                  {
                     index = _loc2_;
                     break;
                  }
               }
               if(_loc2_ == items.length - 1)
               {
                  _loc2_ = -1;
               }
               _loc2_++;
            }
            if(_loc1_ == index)
            {
               index = index + 1;
               getNextTimer.start();
            }
            else
            {
               getItem();
               dispatch("next");
            }
         }
         else
         {
            prev();
         }
      }
      
      public function prevRecorded() : void {
         var _loc1_:* = 0;
         var _loc2_:* = 0;
         if(items.length && !imagesOnly)
         {
            _loc1_ = index;
            if(_loc1_ <= 0)
            {
               _loc1_ = items.length;
            }
            _loc2_ = _loc1_ - 1;
            while(_loc2_ >= 0)
            {
               if(!(items[_loc2_] is String))
               {
                  if(items[_loc2_].type == "recorded")
                  {
                     index = _loc2_;
                     break;
                  }
               }
               if(_loc2_ <= 0)
               {
                  _loc2_ = items.length;
               }
               _loc2_--;
            }
            if(_loc1_ == index)
            {
               index = index + 1;
            }
            getItem();
            dispatch("prev");
         }
         else
         {
            next();
         }
      }
      
      public function update() : void {
         display.update();
      }
      
      public function get hasRecorded() : Boolean {
         return _hasRecorded;
      }
      
      public function get playing() : Boolean {
         return _playing;
      }
   }
}
