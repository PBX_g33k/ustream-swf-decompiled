package tv.ustream.viewer.logic.media
{
   import flash.media.Camera;
   import tv.ustream.net.Connection;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.viewer.logic.modules.LogoShim;
   import flash.media.Microphone;
   import flash.events.EventDispatcher;
   
   public class Monitor extends Recorded
   {
      
      public function Monitor(param1:EventDispatcher, param2:Connection, param3:String) {
         super(null);
         this.streamName = param3;
         this.connection = param2;
         destroyStream();
         audio = false;
         _prerollDone = true;
         param1.addEventListener("setCamera",onMonitorSetCamera);
         param1.addEventListener("setLayers",onMonitorSetLayers);
         param1.addEventListener("setLive",onMonitorSetLive);
         param1.addEventListener("setLogo",onMonitorSetLogo);
         param1.addEventListener("setMode",onMonitorSetMode);
         param1.addEventListener("setStreamName",onMonitorSetStreamName);
         param1.addEventListener("setConnection",onMonitorSetConnection);
      }
      
      private var camera:Camera;
      
      private var forceRemoteStream:Boolean = false;
      
      private var _mode:String = "local";
      
      private var connection:Connection;
      
      private var clickurl:String;
      
      private function onMonitorSetSize(param1:DynamicEvent) : void {
         locker.resize(param1.width,param1.height);
      }
      
      private function onMonitorSetConnection(param1:DynamicEvent) : void {
         connection = param1.connection;
      }
      
      private function onMonitorSetStreamName(param1:DynamicEvent) : void {
         streamName = param1.streamName;
      }
      
      private function onMonitorSetCamera(param1:DynamicEvent) : void {
         camera = param1.camera;
         if(mode != "none")
         {
            mode = mode;
         }
      }
      
      private function onMonitorSetLayers(param1:DynamicEvent) : void {
         while(layers.numChildren)
         {
            layers.removeChildAt(0);
         }
         layers.addChild(param1.layers);
         param1.layers.addEventListener("setSize",onMonitorSetSize);
      }
      
      private function onMonitorSetLive(param1:DynamicEvent) : void {
      }
      
      private function onMonitorSetLogo(param1:DynamicEvent) : void {
         if(param1.data)
         {
            if(param1.data.clickurl != undefined)
            {
               clickurl = param1.data.clickurl;
            }
            else if(param1.data.clickUrl != undefined)
            {
               clickurl = param1.data.clickUrl;
            }
            
            if(param1.hide)
            {
               dispatch("moduleInfo",false,
                  {
                     "info":{"logo":false},
                     "source":"monitor"
                  });
            }
            else
            {
               dispatch("moduleInfo",false,
                  {
                     "info":{"logo":LogoShim.logoFromObject(param1.data)},
                     "source":"monitor"
                  });
            }
         }
      }
      
      private function onMonitorSetMode(param1:DynamicEvent) : void {
         forceRemoteStream = param1.forceRemoteStream;
         mode = param1.mode;
      }
      
      public function get screenHolder() : * {
         return screen;
      }
      
      private function get mode() : String {
         return _mode;
      }
      
      private function set mode(param1:String) : void {
         destroyStream();
         _mode = param1;
         var _loc2_:* = param1;
         if("local" !== _loc2_)
         {
            if("remote" !== _loc2_)
            {
               _loc2_ = true;
               audioOnlyAnimation.enable = _loc2_;
               audioOnlyAnimation.visible = _loc2_;
            }
            else if(!forceRemoteStream)
            {
               if(camera)
               {
                  screen.attachCamera(camera);
                  camera.setLoopback(true);
                  showScreen();
               }
            }
            else
            {
               puppet(connection,streamName);
            }
            
         }
         else if(camera)
         {
            screen.attachCamera(camera);
            camera.setLoopback(false);
            showScreen();
         }
         
      }
      
      override public function get url() : String {
         return clickurl;
      }
      
      public function set microphone(param1:Microphone) : void {
         echo("set microphone : " + [audioOnlyAnimation,param1]);
         if(audioOnlyAnimation && param1)
         {
            audioOnlyAnimation.microphone = param1;
         }
      }
      
      public function get mediaVisible() : Boolean {
         return locker.visible;
      }
      
      public function set mediaVisible(param1:Boolean) : void {
         echo("set mediaVisible : " + param1);
         if(locker)
         {
            locker.visible = param1;
         }
      }
      
      public function set audioOnlyVisible(param1:Boolean) : void {
         echo("set audioOnlyVisible : " + param1);
         if(audioOnlyAnimation)
         {
            audioOnlyAnimation.visible = param1;
         }
      }
      
      override protected function createUms() : void {
         echo("createUms skipped");
      }
   }
}
