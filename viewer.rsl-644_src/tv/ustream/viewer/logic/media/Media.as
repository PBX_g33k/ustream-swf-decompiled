package tv.ustream.viewer.logic.media
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.tools.Locker;
   import tv.ustream.tools.Display;
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.viewer.logic.Logic;
   import flash.events.Event;
   import tv.ustream.tools.This;
   import flash.external.ExternalInterface;
   
   public class Media extends Dispatcher
   {
      
      public function Media(param1:String = null) {
         var _loc2_:* = false;
         super(param1);
         locker = new Locker();
         locker.name = "locker";
         locker.addEventListener("resized",onLockerResize);
         locker.addEventListener("addedToStage",onLockerAddedToStage);
         display = new Display();
         _logoCanvas = new Sprite();
         _logoCanvas.name = "logoCanvas";
         locker.addChild(_logoCanvas);
         echo("rsl build info: 1015 (Tibor, 2014.06.04.  9:43:15,27)");
         if(This.referrer)
         {
            echo("This.referrer: " + This.referrer);
            _pageUrl = This.referrer;
         }
         if(!This.referrer)
         {
            _loc2_ = false;
            try
            {
               _loc2_ = ExternalInterface.call("eval","getMultilevelEmbed=function() { return window != window.top }; getMultilevelEmbed();");
            }
            catch(e:Error)
            {
               if((["facebook","facebook_app"] as Array).indexOf(campaignId) == -1)
               {
                  _urlFailure = "nojsaccess";
               }
            }
            if(_loc2_ && (["facebook","facebook_app"] as Array).indexOf(campaignId) == -1)
            {
               _pageUrl = "multilevel_embed";
               _urlFailure = "multilevel_embed";
            }
         }
         else if(This.referrer == "multilevel_embed")
         {
            _urlFailure = "multilevel_embed";
         }
         
      }
      
      protected var locker:Locker;
      
      protected var display:Display;
      
      protected var _media:Sprite;
      
      protected var _logoCanvas:Sprite;
      
      protected var embed:String;
      
      protected var _title:String;
      
      protected var _url:String;
      
      protected var _pageUrl:String;
      
      protected var _modules:ViewerModuleManager;
      
      protected var campaignId:String;
      
      protected var _urlFailure:String = "";
      
      private function onLockerAddedToStage(... rest) : void {
         display.stage.addEventListener("resize",onLockerResize);
         Logic.stage = display.stage;
      }
      
      protected function onLockerResize(param1:Event = null) : void {
         _logoCanvas.dispatchEvent(new Event("resize"));
      }
      
      public function get media() : Sprite {
         return _media;
      }
      
      public function get title() : String {
         return _title;
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function get pageUrl() : String {
         if(This.referrer && This.referrer.length)
         {
            return This.referrer;
         }
         if(_pageUrl && _pageUrl.length && !This.onSite(This.pageUrl))
         {
            return _pageUrl;
         }
         return This.pageUrl;
      }
      
      public function get modules() : ViewerModuleManager {
         return _modules;
      }
      
      public function get urlFailure() : String {
         return _urlFailure;
      }
      
      override public function destroy(... rest) : * {
         if(_modules)
         {
            _modules = _modules.destroy();
         }
         return super.destroy();
      }
      
      public function midEvent(param1:uint = 0) : void {
      }
      
      public function titlecardStatus(param1:String) : void {
         echo("titlecardStatus: " + param1);
         dispatch("titlecardStatus",false,{"status":param1});
      }
   }
}
