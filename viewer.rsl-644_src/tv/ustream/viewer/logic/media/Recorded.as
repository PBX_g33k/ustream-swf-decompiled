package tv.ustream.viewer.logic.media
{
   import tv.ustream.tools.IdValidator;
   import tv.ustream.tools.VideoContainer;
   import flash.net.NetConnection;
   import flash.net.Responder;
   import tv.ustream.net.ViewerUms;
   import tv.ustream.net.MultiConnection;
   import flash.utils.Timer;
   import flash.net.NetStream;
   import tv.ustream.net.Connection;
   import tv.ustream.viewer.logic.modules.layers.Layer;
   import flash.geom.Rectangle;
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.Quantcast;
   import tv.ustream.tools.ExtendedTimer;
   import tv.ustream.tools.StreamStats;
   import tv.ustream.statistics.Statistics;
   import flash.net.URLLoader;
   import tv.ustream.viewer.logic.ads.AdHolder;
   import tv.ustream.viewer.logic.ads.Ads;
   import tv.ustream.tools.Display;
   import tv.ustream.assets.AudioOnlyAnim;
   import tv.ustream.tools.PublicContainer;
   import tv.ustream.net.UmsStaticFallback;
   import tv.ustream.viewer.logic.modules.stream.Provider;
   import flash.events.MouseEvent;
   import tv.ustream.debug.Debug;
   import flash.system.System;
   import flash.events.Event;
   import flash.net.navigateToURL;
   import flash.net.URLRequest;
   import tv.ustream.viewer.logic.Logic;
   import tv.ustream.tools.This;
   import tv.ustream.tools.KeyboardManager;
   import tv.ustream.debug.LogSender;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.Snapshot;
   import flash.external.ExternalInterface;
   import tv.ustream.tools.Shell;
   import tv.ustream.tools.ContextMenuManager;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.viewer.logic.modules.Thumbnail;
   import tv.ustream.viewer.logic.modules.Quality;
   import tv.ustream.net.FragmentStream;
   import tv.ustream.viewer.logic.modules.Meta;
   import tv.ustream.viewer.logic.modules.Broadcaster;
   import flash.net.URLVariables;
   import tv.ustream.localization.Locale;
   import flash.events.TimerEvent;
   import tv.ustream.viewer.logic.modules.stream.ProgressiveProvider;
   import tv.ustream.gui2.Alert;
   import flash.media.SoundTransform;
   import flash.system.Security;
   import flash.events.ProgressEvent;
   import flash.events.NetStatusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.AsyncErrorEvent;
   import tv.ustream.viewer.logic.modules.Qos;
   import tv.ustream.tools.ContextMenuItems;
   import flash.events.EventDispatcher;
   import tv.ustream.viewer.logic.modules.ClosedCaption;
   import tv.ustream.net.ProgressiveStream;
   import tv.ustream.viewer.logic.modules.Stream;
   import flash.system.Capabilities;
   import flash.system.ApplicationDomain;
   import tv.ustream.tools.Shared;
   
   public class Recorded extends Media
   {
      
      public function Recorded(param1:String, param2:Boolean = true, param3:String = null, param4:String = null, param5:String = null, param6:Object = null) {
         var _loc7_:* = null;
         adsEventList = 
            {
               "adsBlockStatus":forwardAdsEvent,
               "adsVideoAdStarted":forwardAdsEvent,
               "adsVideoAdFinished":forwardAdsEvent,
               "adsMidrollNotification":forwardAdsEvent,
               "adsPlayContent":onAdsPlayContent,
               "adsPauseContent":onAdsPauseContent,
               "adsLogAdEvent":onAdsLogAdEvent,
               "adsPause":onAdsPause,
               "adsContentAudio":onAdsContentAudio,
               "adsCompanion":forwardAdsEvent,
               "adHidden":onAdHidden,
               "adShown":onAdShown,
               "adRemoved":onAdRemoved
            };
         gwCalls = 
            {
               "checkAdfree":"Viewer.authorizeAdFreeViewer",
               "getAdverts":"Viewer.getAdverts"
            };
         fmsParams = {};
         telemetry = 
            {
               "isQuantcast":true,
               "isGA":true,
               "isComscore":true
            };
         if(Logic.loaderInfo)
         {
            Debug.debug("recorded","RSL url: " + Logic.loaderInfo.url);
         }
         Debug.debug("recorded","touchscreenType: " + Capabilities.touchscreenType);
         Debug.debug("recorded","os: " + Capabilities.os,null,true);
         try
         {
            _loc7_ = ExternalInterface.call("function(){return navigator.userAgent}");
            Debug.debug("recorded","useragent: " + _loc7_,null,true);
         }
         catch(e:Error)
         {
         }
         Debug.debug("recorded","flash player version: " + Capabilities.version,null,true);
         try
         {
            netStreamPlayOptionClass = ApplicationDomain.currentDomain.getDefinition("flash.net::NetStreamPlayOptions") as Class;
         }
         catch(e:Error)
         {
            Debug.debug("recorded","Error: getting class definition of NetStreamPlayOptions failed :: " + e.message);
         }
         _sessionId = param4;
         this.password = param3;
         progressClip = new Sprite();
         cutSprite = new Sprite();
         if(param6)
         {
            if(param6.viewerBuildInfo)
            {
               _viewerBuildInfo = param6.viewerBuildInfo;
            }
            if(param6.sessionSource)
            {
               _sessionSource = param6.sessionSource;
            }
            if(param6.highlightId && !isNaN(param6.highlightId))
            {
               _highlightId = param6.highlightId;
            }
            if(param6.campaignId)
            {
               campaignId = param6.campaignId;
            }
            if(param6.userId)
            {
               _userId = param6.userId;
            }
            if(param6.user)
            {
               user = param6.user;
            }
            if(param6.pass)
            {
               pass = param6.pass;
            }
            if(param6.adFree)
            {
               Debug.debug("recorded","Premium Member");
               adfreeStatus = 1;
               if(Logic.hasInstance)
               {
                  Logic.instance.adFree = true;
               }
            }
            if(param6.ignoreCut)
            {
               ignoreCut = true;
            }
            if(param6.scid && Logic.hasInstance)
            {
               Debug.debug("recorded","scid: " + param6.scid);
               Logic.instance.scid = param6.scid;
            }
            else
            {
               Debug.debug("recorded","there is no scid!");
            }
            if(param6.hash)
            {
               _hash = param6.hash;
               if(Logic.hasInstance)
               {
                  Logic.instance.hash = _hash;
               }
            }
         }
         this.application = param5;
         if(param5)
         {
            fmsParams.application = param5;
         }
         if(Logic.hasInstance && (Logic.instance.isLimitedChannel))
         {
            shared = {};
         }
         else
         {
            shared = new Shared({"rpin":"rpin." + (Math.random() * Math.random()).substr(2)});
         }
         if(param1)
         {
            param1 = param1.replace(String.fromCharCode(13),"");
            param1 = param1.replace("\r","");
            param1 = param1.replace("\n","");
         }
         super(param1);
         viewerPlaying = param2;
         _autoPlay = param2;
         _playing = param2;
         screen = new VideoContainer();
         if(param6 && param6.enableStageVideo)
         {
            enableStageVideo = true;
            screen.stageVideoDisabled = false;
         }
         if(param6 && param6.cutter)
         {
            cutter = true;
         }
         screen.addEventListener("getSize",chkScreenSize);
         screen.addEventListener("rendererChange",onRendererChange);
         screen.addEventListener("ccDetected",onCCDetected);
         locker.addChildAt(screen,0);
         custom = new Display();
         locker.addChild(new Display()).visible = false;
         stats = new StreamStats(this,shared.rpin,screen);
         locker.addChild(stats);
         layers = new Sprite();
         locker.addChild(layers);
         layers.name = "layers";
         cover = new Sprite();
         cover.name = _type + "_cover";
         cover.addEventListener("click",onVideoContainerClick);
         locker.addChildAt(cover,1);
         if(param1 && (_type == "channel" || _type == "recorded"))
         {
            if(idValidator.parseId(param1))
            {
               hostBrandId = idValidator.hostBrandId;
               _brandId = idValidator.brandId || "1";
               _mediaId = idValidator.mediaId;
            }
            else
            {
               destroy();
               return;
            }
         }
         if(_type == "channel" && !((["13928451","8963955"] as Array).indexOf(_mediaId) == -1))
         {
            echo("disable qga");
            telemetry.isQuantcast = false;
            telemetry.isGA = false;
         }
         adaptvParams = {};
         adaptvParams.autoplay = param2;
         if(Locale.hasInstance)
         {
            adaptvParams.locale = Locale.instance.language;
         }
         if(_type == "recorded")
         {
            echo("set adaptvparams vid");
            adaptvParams.vid = _mediaId;
         }
         demo = new Sprite();
         locker.addChild(new Sprite()).name = "demo";
         if(Logic.hasInstance)
         {
            Logic.instance.display.addChild(Alert.instance);
            Logic.instance.addEventListener("checkAdfree",onCheckAdFree);
            Logic.instance.addEventListener("adBlockerReport",onAdblockerReport);
            if(Logic.instance.adFree)
            {
               adfreeStatus = 1;
            }
         }
         else
         {
            alertHolder = new Sprite();
            locker.addChild(new Sprite());
            alertHolder.addChild(Alert.instance);
            alertHolder.name = "alert_holder";
         }
         _controlOverlay = new PublicContainer();
         locker.addChild(new PublicContainer());
         locker.addChild(_logoCanvas);
         locker.excludeScrollRectChild = _controlOverlay;
         adHolder = new AdHolder();
         locker.addChild(new AdHolder()).name = "adHolder";
         if(_type == "monitor")
         {
            if(!audioOnlyAnimation)
            {
               audioOnlyAnimation = new AudioOnlyAnim();
            }
            display.addChild(audioOnlyAnimation);
         }
         display.addChild(locker);
         _media = display;
         _media.addEventListener("addedToStage",onAddedStageHandler);
         locker.close();
         gwUrl = (This.secure?"https://":"http://") + "rgw.ustream.tv/gateway.php";
         gw = new Connection("GW");
         gw.connect(gwUrl);
         gw.addEventListener("callFailed",onGwCallFailed);
         gwResponder = new Responder(onGwResult,onGwStatus);
         fmsTimer = new Timer(3000,1);
         fmsTimer.addEventListener("timer",connectToFms);
         _viewerVersion = Logic.isNewViewer?3:2.0;
         _statistics = new Statistics();
         _statistics.mediaId = _mediaId;
         _statistics.brandId = _brandId;
         if(Logic.hasInstance && Logic.instance.abr)
         {
            onAdblockerReport();
         }
         createModules();
         createUms();
         if(adfreeStatus == 1)
         {
            onCheckAdFree();
         }
         _contextMenuItems = new ContextMenuItems();
         _contextMenuItems.userName = ContextMenuManager.getInstance().addItem("username: - ",null,null,"idkfa");
         _contextMenuItems.userName.enabled = false;
         _contextMenuItems.forcedProviderChange = ContextMenuManager.getInstance().addItem("Change Provider",providerSwitch,null,"idkfa");
         _contextMenuItems.switchStageVideoEnabled = ContextMenuManager.getInstance().addItem(screen.stageVideoDisabled?"Turn on StageVideo":"Turn off StageVideo",stageVideoSwitch,null,"idkfa");
         _contextMenuItems.infoPanelSwitch = ContextMenuManager.getInstance().addItem(stats.visible?"Hide info layer":"Show info layer",switchStreamInfoPanel,null,"idkfa");
         _contextMenuItems.infoPanelCopy = ContextMenuManager.getInstance().addItem("Copy info",copyStreamInfoPanelContent,null,"idkfa");
         _contextMenuItems.infoPanelCopy.visible = false;
         _contextMenuItems.debugSwitch = ContextMenuManager.getInstance().addItem(Debug.enabled?"Turn off debug":"Turn on Debug",switchDebug,null,"idkfa");
         _contextMenuItems.userInitedLogSending = ContextMenuManager.getInstance().addItem("Send log to UMS",userInitiatedLogSending,null,"idkfa");
         _contextMenuItems.userInitedLogSending = ContextMenuManager.getInstance().addItem("Send log",userInitiatedLogSending,null,"log");
         _contextMenuItems.copyCidToClipboard = ContextMenuManager.getInstance().addItem("Copy CID / VID",copyCidToClipboard,null,"idkfa");
      }
      
      public static const PROVIDER_FAILED:String = "providerFailed";
      
      public static const PROVIDER_SWITCH:String = "providerSwitch";
      
      public static const SEEK_NOTIFY:String = "seekNotify";
      
      public static const SEEK_START:String = "seekStart";
      
      public static const SEEK_FAILED:String = "seekFailed";
      
      public static const INVALID_TIME:String = "invalidTime";
      
      public static const VOLUME_CHANGE:String = "volumeChange";
      
      public static const LOG_SENDING_STARTED:String = "logSendingStarted";
      
      public static const LOG_SENDING_COMPLETE:String = "logSendingComplete";
      
      public static const LOG_SENDING_FAILED:String = "logSendingFailed";
      
      public static const VIEW_MODE:String = "viewMode";
      
      protected static var idValidator:IdValidator = new IdValidator();
      
      protected var adsEventList:Object;
      
      protected var screen:VideoContainer;
      
      protected var gw:NetConnection;
      
      protected var gwResponder:Responder;
      
      protected var gwResult:Object;
      
      protected var gwUrl:String;
      
      protected var gwCalls:Object;
      
      protected var password:String;
      
      protected var varnish:Boolean = false;
      
      protected var varnishFailed:Boolean = false;
      
      protected var reConnect:Boolean = false;
      
      protected var _ums:ViewerUms;
      
      protected var fms:MultiConnection;
      
      protected var fmsTimer:Timer;
      
      private var fmsUrl:String;
      
      private var hasFms:Boolean = false;
      
      protected var fmsParams:Object;
      
      protected var fmsStreamOwner:Boolean = false;
      
      protected var stream:NetStream;
      
      protected var _streamStatus:String;
      
      protected var streamName:String;
      
      protected var resolvedStreamName:String;
      
      protected var _streamOwner:Connection;
      
      protected var hasStreamSize:Boolean = false;
      
      protected var _backupVolume:Number = -1;
      
      protected var _volume:Number = 1;
      
      protected var _muted:Boolean = false;
      
      private var _meta:Object;
      
      protected var _duration:Number = 0;
      
      private var isProgressive:Boolean = false;
      
      protected var _hasKeyData:Boolean = false;
      
      protected var _largeIntervals:Boolean = false;
      
      protected var autoSeek:Number = -1;
      
      protected var _audio:Boolean = true;
      
      protected var _video:Boolean = true;
      
      protected var hasAudio:Boolean = true;
      
      protected var hasVideo:Boolean = true;
      
      private var _layer:Layer;
      
      protected var _playing:Boolean = false;
      
      private var _autoPlay:Boolean;
      
      protected var viewerPlaying:Boolean = false;
      
      private var _streamRect:Rectangle;
      
      protected var detached:Boolean = false;
      
      protected var _mediaId:String;
      
      protected var _brandId:String = "1";
      
      protected var _sessionId:String;
      
      protected var _sessionSource:String;
      
      protected var hostBrandId:String = null;
      
      protected var rejected:Boolean = false;
      
      protected var application:String;
      
      protected var _highlightId:Number;
      
      protected var _buffer:Number = 5;
      
      protected var cover:Sprite;
      
      protected var quantcast:Quantcast;
      
      protected var layers:Sprite;
      
      protected var shared;
      
      private var demo:Sprite;
      
      protected var streamId:Number;
      
      protected var playTimer:Timer;
      
      public var isSlideShow:Boolean = false;
      
      private var isRevShare:Boolean = false;
      
      private var revShareData:Object;
      
      protected var plannedDisconnected:Boolean = false;
      
      private var videoWidth:Number = 0;
      
      private var videoHeight:Number = 0;
      
      protected var w84ModulesLock:Boolean = false;
      
      private var _offset:Number = 0;
      
      private var offsetIndex:Number = 0;
      
      private var previewTimer:ExtendedTimer;
      
      protected var stats:StreamStats;
      
      private var _statistics:Statistics;
      
      protected var redirectUrl:String;
      
      private var ignoreCut:Boolean = false;
      
      private var cutStart:Number = 0;
      
      private var cutEnd:Number = 1;
      
      private var cutSprite:Sprite;
      
      private var alertHolder:Sprite;
      
      private var defaultEmbedLimitMessage:String = "Currently this live show can only be viewed on Ustream.";
      
      protected var _takeover:Boolean = false;
      
      private var takeOverAudio:Boolean;
      
      private var takeOverVideo:Boolean;
      
      private var isComscoreCalled:Boolean = false;
      
      private var progressiveSize:Number = -1;
      
      private var user:String;
      
      private var pass:String;
      
      private var comScoreLoader:URLLoader;
      
      private var _userId:String;
      
      protected var adHolder:AdHolder;
      
      protected var ads:Ads;
      
      protected var geoAds:Object;
      
      private var geoAdsGw:NetConnection;
      
      private var geoAdsGwResponde:Responder;
      
      private var geoAdsTimeout:Timer;
      
      protected var adfreeStatus:uint = 0;
      
      protected var progressClip:Sprite;
      
      protected var akamaiHd:IAkamaiHd;
      
      protected var _pausedByAdvert:Boolean = false;
      
      protected var _hash:String;
      
      private var _viewerVersion:uint = 0;
      
      private var _adOffsetLock:Boolean = false;
      
      private var isLimitedGeoLock:Boolean = false;
      
      private var isMultiLevelEmbedReject:String = "";
      
      private var _clientIp:String;
      
      protected var custom:Display;
      
      private var _adIdle:uint = 0;
      
      private var _channelId:String;
      
      protected var getAdsTimer:Timer;
      
      private var _username:String;
      
      protected var audioOnlyAnimation:AudioOnlyAnim;
      
      protected var _controlOverlay:PublicContainer;
      
      protected var rejectAlertId:uint = 0;
      
      protected var needCreateStream:Boolean = false;
      
      protected var adaptvParams:Object;
      
      private var bufferFull:Boolean = false;
      
      private var screenSizeTimer:Timer;
      
      private var enableStageVideo:Boolean = false;
      
      private var cutter:Boolean = false;
      
      private var flv:String = null;
      
      private var _viewerBuildInfo:String;
      
      protected var _screenVisible:Boolean = true;
      
      protected var _prerollDone:Boolean = false;
      
      protected var _progressiveDone:Boolean = true;
      
      protected var netStreamPlayOptionClass:Class;
      
      private var umsFallback:UmsStaticFallback;
      
      private var telemetry:Object;
      
      protected var provider:Provider;
      
      protected var _playedStreamName:String;
      
      private var _embedRedirection:Boolean = false;
      
      private function onVideoContainerClick(param1:MouseEvent) : void {
         dispatch("onVideoClicked");
      }
      
      private function stageVideoSwitch() : void {
         screen.stageVideoDisabled = !screen.stageVideoDisabled;
         _contextMenuItems.switchStageVideoEnabled.caption = screen.stageVideoDisabled?"Turn on StageVideo":"Turn off StageVideo";
      }
      
      private function switchDebug() : void {
         if(Debug.enabled)
         {
            Debug.info("recorded","disabling debug");
            Debug.enabled = false;
            _contextMenuItems.debugSwitch.caption = "Turn on Debug";
         }
         else
         {
            Debug.enabled = true;
            Debug.info("recorded","enabling debug");
            _contextMenuItems.debugSwitch.caption = "Turn off Debug";
         }
      }
      
      private function switchStreamInfoPanel() : void {
         stats.visible = !stats.visible;
         _contextMenuItems.infoPanelCopy.visible = stats.visible;
         _contextMenuItems.infoPanelSwitch.caption = stats.visible?"Hide info layer":"Show info layer";
      }
      
      private function copyStreamInfoPanelContent() : void {
         if(stats)
         {
            System.setClipboard(stats.getStat());
         }
      }
      
      private function userInitiatedLogSending() : void {
         sendLogToUms("user-initiated (keyboard shortcut)",false);
      }
      
      protected function copyCidToClipboard() : void {
         System.setClipboard(_mediaId);
      }
      
      protected function createUms() : void {
         _ums = ViewerUms.requestInstance(this,userId,password);
         _ums.addEventListener("reject",onUmsReject);
         _ums.addEventListener("connected",onUmsConnect);
         _ums.addEventListener("disconnected",onUmsDisconnect);
         stats.ums = _ums;
      }
      
      protected function destroyUms() : void {
         if(_ums)
         {
            _ums.removeEventListener("reject",onUmsReject);
            _ums.removeEventListener("connected",onUmsConnect);
            _ums.removeEventListener("disconnected",onUmsDisconnect);
            _ums = _ums.destroy();
         }
      }
      
      protected function onUmsDisconnect(param1:Event) : void {
      }
      
      private function onUmsConnect(param1:Event) : void {
      }
      
      protected function onScreenClick(param1:Event) : void {
         navigateToURL(new URLRequest(url));
         corePause();
      }
      
      private function onCreateLogo(param1:Event) : void {
         _modules.logo.canvas = _logoCanvas;
         if(_type == "recorded")
         {
            _modules.logo.recorded = true;
         }
      }
      
      protected function onCheckAdFree(param1:Event = null) : void {
         var _loc2_:* = null;
         echo("ADFREE: onCheckAdFree");
         if(_id == "9221027")
         {
            echo("ignored");
            adfreeStatus = 0;
            if(ums.connected && (shared.countryCode || !shared.countryCode && _type == "recorded"))
            {
               getGeoAds();
            }
            return;
         }
         var _loc3_:String = Logic.hasInstance?Logic.instance.sessionId:_sessionId;
         if(This.onSite(pageUrl) & 2 && _loc3_)
         {
            _loc2_ = {};
            _loc2_.sessionId = _loc3_;
            echo(gwCalls.checkAdfree + " @ " + gwUrl + "?" + gwCalls.checkAdfree);
            Debug.debug("recorded","args",_loc2_);
            gw.connect(gwUrl + "?" + gwCalls.checkAdfree);
            gw.call(gwCalls.checkAdfree,gwResponder,_loc2_);
         }
         else
         {
            adfreeStatus = 0;
            echo("missing session");
            getGeoAds();
         }
      }
      
      public function get progressive() : Boolean {
         return isProgressive;
      }
      
      private function onAddedStageHandler(param1:Event) : void {
         Debug.debug("recorded","onAddedStageHandler");
         _media.removeEventListener("addedToStage",onAddedStageHandler);
         _media.addEventListener("removedFromStage",onRemovedFromStage);
         KeyboardManager.getInstance().addShortKeys(83,false,true,true,switchStreamInfoPanel);
         KeyboardManager.getInstance().addShortKeys(83,false,true,false,switchStreamInfoPanel);
         KeyboardManager.getInstance().addShortKeys(76,true,true,false,userInitiatedLogSending);
         Debug.stage = _media.stage;
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         Debug.debug("recorded","onRemovedFromStage");
         _media.removeEventListener("removedFromStage",onRemovedFromStage);
         _media.addEventListener("addedToStage",onAddedStageHandler);
      }
      
      protected function sendLogToUms(param1:String, param2:Boolean = true) : void {
         var _loc3_:LogSender = Debug.sendLogToUms(_ums,param1);
         if(_loc3_ && !param2)
         {
            _loc3_.addEventListener("start",onLogSenderEvent);
            _loc3_.addEventListener("failed",onLogSenderEvent);
            _loc3_.addEventListener("complete",onLogSenderEvent);
            _loc3_.addEventListener("destroy",onLogSenderEvent);
         }
      }
      
      private function onLogSenderEvent(param1:Event) : void {
         var _loc2_:LogSender = param1.target as LogSender;
         var _loc3_:* = param1.type;
         if("start" !== _loc3_)
         {
            if("complete" !== _loc3_)
            {
               if("failed" !== _loc3_)
               {
                  if("destroy" === _loc3_)
                  {
                     if(_loc2_)
                     {
                        _loc2_.removeEventListener("start",onLogSenderEvent);
                        _loc2_.removeEventListener("failed",onLogSenderEvent);
                        _loc2_.removeEventListener("complete",onLogSenderEvent);
                        _loc2_.removeEventListener("destroy",onLogSenderEvent);
                     }
                  }
               }
               else
               {
                  dispatch("logSendingFailed");
               }
            }
            else
            {
               dispatch("logSendingComplete",false,{"hash":(param1 as DynamicEvent).data.hash});
            }
         }
         else
         {
            dispatch("logSendingStarted");
         }
      }
      
      public function createBase64PngSnapshot() : void {
         Snapshot.getBase64(screen,onBase64,"image.png");
      }
      
      public function createBase64JpgSnapshot() : void {
         Snapshot.getBase64(screen,onBase64,"image.jpg");
      }
      
      private function onBase64(param1:String) : void {
         echo("Base64 snapshot created. length: " + param1.length);
         if(ExternalInterface.available)
         {
            Shell.instance.dispatch("Snapshot","onBase64Image",param1);
         }
      }
      
      override public function destroy(... rest) : * {
         if(!_active)
         {
            echo("### error: already dead");
            return;
         }
         destroyAdverts();
         destroyUmsFallback();
         if(Logic.hasInstance)
         {
            if(Logic.instance.hasEventListener("checkAdfree"))
            {
               Logic.instance.removeEventListener("checkAdfree",onCheckAdFree);
            }
            if(Logic.instance.hasEventListener("adBlockerReport"))
            {
               Logic.instance.removeEventListener("adBlockerReport",onAdblockerReport);
            }
         }
         _active = false;
         gw.removeEventListener("callFailed",onGwCallFailed);
         gw = null;
         gwResponder = null;
         fmsTimer.stop();
         fmsTimer.removeEventListener("timer",connectToFms);
         fmsTimer = null;
         destroyUms();
         fmsDestroy();
         destroyStream();
         stats.destroy();
         if(_media.parent && _media.parent.contains(_media))
         {
            _media.parent.removeChild(_media);
         }
         screen.removeEventListener("getSize",chkScreenSize);
         screen.removeEventListener("rendererChange",onRendererChange);
         screen.removeEventListener("ccDetected",onCCDetected);
         screen.removeEventListener("enterFrame",onScreenEnterFrame);
         cover.removeEventListener("click",onVideoContainerClick);
         screen.destroy();
         if(cover.hasEventListener("mouseDown"))
         {
            cover.removeEventListener("mouseDown",onScreenClick);
         }
         removeEventListener("getStreamSize",onProviderStreamSize);
         _active = true;
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.forcedProviderChange);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.switchStageVideoEnabled);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.infoPanelCopy);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.infoPanelSwitch);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.userInitedLogSending);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.copyCidToClipboard);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.debugSwitch);
         ContextMenuManager.getInstance().removeItem(_contextMenuItems.userName);
         super.destroy();
      }
      
      public function direct(param1:Object) : void {
         param1.status = "online";
         _id = param1.cid;
         onGwResult(param1);
      }
      
      override protected function onLockerResize(param1:Event = null) : void {
         screen.width = locker.width;
         screen.height = locker.height;
         if(custom.visible)
         {
            custom.width = locker.width;
            custom.height = locker.height;
         }
         stats.width = locker.width;
         stats.height = locker.height;
         cover.graphics.clear();
         cover.graphics.beginFill(16711680,0);
         cover.graphics.drawRect(0,0,locker.width,locker.height);
         cover.graphics.endFill();
         layers.width = locker.width;
         layers.height = locker.height;
         resizeAd();
         if(_modules && _modules.quality)
         {
            _modules.quality.setDisplaySize(locker.width,locker.height);
         }
         super.onLockerResize(param1);
      }
      
      protected function onPassword(... rest) : void {
         dispatch("password");
         destroy();
      }
      
      protected function createModules(... rest) : void {
         if(!_modules)
         {
            _modules = new ViewerModuleManager(this,password,_userId);
            _modules.addEventListener("createMeta",onCreateMeta);
            _modules.addEventListener("createAdvert",onCreateAdvert);
            _modules.addEventListener("createPpv",onCreatePpv);
            _modules.addEventListener("destroyPpv",onPpvDestroy);
            _modules.addEventListener("createBuffer",onCreateBuffer);
            _modules.addEventListener("lock",onModulesLockUpdate);
            _modules.addEventListener("createLogo",onCreateLogo);
            _modules.addEventListener("createStream",onCreateStream);
            _modules.addEventListener("createQos",onCreateQos);
            _modules.addEventListener("createClosedCaption",onCreateCC);
            _modules.addEventListener("createQuality",onCreateQuality);
            _modules.addEventListener("createBroadcaster",onCreateBroadcaster);
            _modules.addEventListener("createThumbnail",onCreateThumbnail);
            dispatch("createModules");
         }
      }
      
      private function onCreateThumbnail(param1:Event) : void {
         var _loc2_:Thumbnail = (param1.target as ViewerModuleManager).thumbnail;
         if(_loc2_)
         {
            _loc2_.addEventListener("destroy",onThumbnailDestroy);
            _loc2_.addEventListener("complete",onThumbnailComplete);
            _loc2_.addEventListener("hide",onThumbnailHide);
            _loc2_.addEventListener("show",onThumbnailShow);
         }
      }
      
      private function onThumbnailShow(param1:Event) : void {
         hideScreen();
      }
      
      private function onThumbnailHide(param1:Event) : void {
         showScreen();
      }
      
      private function onThumbnailDestroy(param1:Event) : void {
         var _loc2_:Thumbnail = param1.target as Thumbnail;
         if(_loc2_)
         {
            if(_loc2_.display)
            {
               display.removeChild(_loc2_.display);
            }
            _loc2_.removeEventListener("destroy",onThumbnailDestroy);
            _loc2_.removeEventListener("complete",onThumbnailComplete);
         }
      }
      
      private function onThumbnailComplete(param1:Event) : void {
         var _loc2_:Thumbnail = param1.target as Thumbnail;
         if(_loc2_)
         {
            display.addChildAt(_loc2_.display,0);
            locker.resize(_loc2_.display.content.width,_loc2_.display.content.height);
            display.update();
            dispatch("getStreamSize",false,
               {
                  "width":_loc2_.display.content.width,
                  "height":_loc2_.display.content.height
               });
         }
      }
      
      private function onCreateQuality(param1:Event) : void {
         var _loc2_:* = null;
         if(param1.target is ViewerModuleManager)
         {
            _loc2_ = (param1.target as ViewerModuleManager).quality;
            if(_loc2_)
            {
               _loc2_.setDisplaySize(locker.width,locker.height);
            }
         }
      }
      
      private function onUmsReject(param1:DynamicEvent) : void {
         echo("onUmsReject()");
         onRejected(param1.data);
      }
      
      private function resolveStreamName(param1:String) : void {
         var _loc2_:* = null;
         echo("resolveStreamName " + param1);
         resolvedStreamName = "";
         if(param1)
         {
            if(This.secure)
            {
               param1 = param1.split("http://").join("https://");
            }
            _loc2_ = new URLLoader();
            _loc2_.addEventListener("ioError",onServiceLoaderError);
            _loc2_.addEventListener("securityError",onServiceLoaderError);
            _loc2_.addEventListener("complete",onServiceLoaderComplete);
            _loc2_.load(new URLRequest(param1));
         }
         else if(stream is FragmentStream && netStreamPlayOptionClass)
         {
            play2();
         }
         else
         {
            connect();
         }
         
      }
      
      private function play2() : void {
         streamName = provider.streamName;
         var _loc2_:String = parseStreamName(resolvedStreamName?resolvedStreamName:streamName);
         var _loc1_:* = new netStreamPlayOptionClass();
         _loc1_.oldStreamName = playedStreamName;
         playedStreamName = _loc2_;
         _loc1_.streamName = _loc2_;
         _loc1_.transition = "swap";
         echo("playing " + _loc2_ + " (play2)");
         stream.play2(_loc1_);
         if(ums)
         {
            ums.status = resolvedStreamName?resolvedStreamName:streamName;
         }
      }
      
      private function onServiceLoaderComplete(param1:Event) : void {
         var _loc2_:URLLoader = param1.target as URLLoader;
         resolvedStreamName = _loc2_.data as String;
         echo("onServiceLoaderComplete " + resolvedStreamName);
         dropServiceLoader(_loc2_);
      }
      
      private function onServiceLoaderError(param1:Event) : void {
         echo("onServiceLoaderError " + param1.toString());
         dropServiceLoader(param1.target as URLLoader);
      }
      
      private function dropServiceLoader(param1:URLLoader) : void {
         if(param1)
         {
            param1.removeEventListener("ioError",onServiceLoaderError);
            param1.removeEventListener("securityError",onServiceLoaderError);
            param1.removeEventListener("complete",onServiceLoaderComplete);
         }
         if(!playing && !_autoPlay)
         {
            corePlay();
         }
         else if(stream is FragmentStream)
         {
            play2();
         }
         else
         {
            connect();
         }
         
      }
      
      private function onPpvDestroy(... rest) : void {
         if(!_modules.destroying)
         {
            onPpvGoFree();
         }
      }
      
      private function onCreatePpv(param1:Event) : void {
         _modules.ppv.addEventListener("goPaid",onPpvGoPaid);
         _modules.ppv.addEventListener("goFree",onPpvGoFree);
      }
      
      protected function onPpvGoFree(param1:Event = null) : void {
         echo("onPpvGoFree");
      }
      
      protected function onPpvGoPaid(param1:DynamicEvent) : void {
         Debug.debug("recorded","onPpvGoPaid",param1.data);
      }
      
      private function onModulesLockUpdate(... rest) : void {
         if(w84ModulesLock)
         {
            echo("w84ModulesLock connect");
            connect();
         }
         if(_modules.lock && stream)
         {
            destroyStream();
         }
      }
      
      protected function onCreateMeta(... rest) : void {
         _modules.meta.addEventListener("update",onMetaUpdate);
      }
      
      protected function onMetaUpdate(param1:Event) : void {
         var _loc2_:Meta = param1.target as Meta;
         if(_loc2_)
         {
            if(!_loc2_.data.hasOwnProperty("quantcast"))
            {
               telemetry.isQuantcast = false;
            }
            if(Logic.hasInstance && (Logic.instance.isLimitedChannel))
            {
               telemetry.isQuantcast = false;
            }
            if(telemetry.isQuantcast)
            {
               quantcast = new Quantcast(
                  {
                     "publisherId":"p-22sNmkTMKNC-A",
                     "videoId":id
                  },display);
            }
            if(_loc2_.data.length || _loc2_.data.videoLength || _loc2_.data.duration)
            {
               _duration = _loc2_.data.videoLength || _loc2_.data.video_length || _loc2_.data.duration;
            }
            if(_loc2_.data.cut)
            {
               processCut(_loc2_.data.cut);
            }
            if(_loc2_.data.highlight)
            {
               processHighlight(_loc2_.data.highlight);
            }
            if(_loc2_.data.userName)
            {
               _username = _loc2_.data.userName;
               _contextMenuItems.userName.caption = "userName: " + _loc2_.data.userName;
            }
            if(_loc2_.data.broadcasterType && _modules.logo)
            {
               _modules.logo.mobile = _loc2_.data.broadcasterType == "mobile";
            }
            if(_loc2_.data.mainCategoryName && !isComscoreCalled && (!Logic.hasInstance || (Logic.hasInstance && !Logic.instance.isLimitedChannel)))
            {
               comScoreBeacon();
            }
            if(_loc2_.data.title)
            {
               _title = _loc2_.data.title;
            }
            if(_loc2_.data.ownerChannelId)
            {
               _channelId = _loc2_.data.ownerChannelId;
            }
            if(_loc2_.data.pageUrl && !(_urlFailure == "multilevel_embed"))
            {
               _pageUrl = _loc2_.data.pageUrl;
               if(_pageUrl.lastIndexOf("/") == _pageUrl.length - 1)
               {
                  _pageUrl = _pageUrl.substr(0,_pageUrl.length - 1);
                  echo("remove /");
               }
            }
            if(_loc2_.data.ip)
            {
               clientIp = _loc2_.data.ip;
            }
            if(_loc2_.data.adFree == true)
            {
               adfreeStatus = 2;
               dispatch("adsCompanion",false,{"companion":false});
               onPrerollDone();
            }
            else
            {
               if(_loc2_.data.brand && _loc2_.data.brand == "Verizon")
               {
                  adaptvParams.verizon = true;
               }
               if(!This.pageUrl && !This.externalInterface && _loc2_.data.pageUrl)
               {
                  adaptvParams.nojs = true;
               }
            }
            if(!_loc2_.data.countryCode)
            {
               _loc2_.data.countryCode = "EN";
            }
            if(_loc2_.data.countryCode)
            {
               echo("meta.countryCode >> geoads");
               if(!shared.countryCode || !(shared.countryCode == _loc2_.data.countryCode))
               {
                  echo("different countryCodes: " + [shared.countryCode,_loc2_.data.countryCode]);
                  shared.countryCode = _modules.meta.data.countryCode;
               }
               if(adfreeStatus == 0)
               {
                  echo("check countryCode: " + (_loc2_.data.countryCode).toUpperCase());
                  if((_loc2_.data.countryCode).toUpperCase() == "JP")
                  {
                     checkEmbedRedirection();
                  }
                  getGeoAds();
               }
            }
            if(enableStageVideo)
            {
               screen.stageVideoDisabled = _loc2_.data.broadcasterDevice == "wowzapro";
            }
            else
            {
               screen.stageVideoDisabled = true;
            }
         }
      }
      
      private function checkEmbedRedirection() : void {
         var _loc5_:* = 0;
         var _loc6_:String = This.referrer?This.referrer:This.pageUrl;
         var _loc4_:Array = This.getReference(modules,"broadcaster.data.benefits") as Array;
         var _loc1_:Boolean = _loc4_ && !(_loc4_.indexOf("japan_embed_no_redirect") == -1);
         echo("checkEmbedRedirect: " + [This.onSite(_loc6_)]);
         var _loc2_:Array = ["static-shell.cerevo.com","shell.cerevo.com","secure.cerevo.com","s-dev.cerevo.com","s-dev-01.cerevo.com","s-dev-02.cerevo.com"];
         var _loc3_:String = This.getPageDomain(_loc6_);
         _loc5_ = 0;
         while(_loc5_ < _loc2_.length)
         {
            if(_loc3_.indexOf(_loc2_[_loc5_]) != -1)
            {
               _loc1_ = true;
               break;
            }
            _loc5_++;
         }
         if(This.onSite(This.pageUrl) <= 1 && This.onSite(_loc6_) == 0)
         {
            _embedRedirection = !_loc1_;
            if(!_loc1_)
            {
               viewerPlaying = false;
               _autoPlay = false;
               _playing = false;
               adaptvParams.autoplay = false;
               dispatch("forceAutoplay",false,{"autoplay":_autoPlay});
            }
         }
      }
      
      protected function onCreateBroadcaster(... rest) : void {
         _modules.broadcaster.addEventListener("update",onBroadcasterUpdate);
      }
      
      protected function onBroadcasterUpdate(param1:Event) : void {
         echo("onBroadcasterUpdate");
         var _loc2_:Broadcaster = param1.target as Broadcaster;
         if(_loc2_)
         {
            if(_loc2_.data.broadcasterType == "playstation")
            {
               adaptvParams.ps4 = true;
            }
            if(_loc2_.data.location)
            {
               adaptvParams.location = _loc2_.data.location;
            }
            if(_loc2_.data.featured)
            {
               adaptvParams.featured = _loc2_.data.featured;
            }
            if(_loc2_.data.proPackage && _loc2_.data.proPackage < 2)
            {
               echo("Turn off closed caption.");
               screen.ccEnabled = false;
               dispatchEvent(new DynamicEvent("moduleInfo",false,false,
                  {
                     "info":{"closedCaption":false},
                     "source":"channel"
                  }));
            }
         }
      }
      
      protected function getParsedUrlVariable(param1:String, param2:*) : String {
         var _loc3_:URLVariables = new URLVariables();
         _loc3_[param1] = param2;
         return _loc3_.toString() + "&";
      }
      
      protected function onGwResult(param1:Object) : void {
         if(active)
         {
            if(param1.success != false)
            {
               if(!(param1.adfree == null) && param1.adfree == true)
               {
                  echo("ADFREE: TRUE, remove adverts!");
                  adfreeStatus = 2;
                  dispatch("adsCompanion",false,{"companion":false});
                  destroyAdverts();
                  onPrerollDone();
                  return;
               }
               if(!(param1.adfree == null) && param1.adfree == false)
               {
                  echo("ADFREE: FALSE, create adverts!");
                  adfreeStatus = 0;
                  if(ums && ums.connected)
                  {
                     getGeoAds();
                  }
                  return;
               }
            }
            gwResult = param1;
            streamId = param1.streamId || 0;
            if(gwResult.preview && (!pageUrl || pageUrl && (This.onSite(pageUrl) == 0 || Logic.loaderInfo && Logic.loaderInfo.loaderURL && Logic.loaderInfo.loaderURL == pageUrl)))
            {
               previewTimer = new ExtendedTimer(gwResult.preview * 1000);
               previewTimer.addEventListener("timer",onPreviewTimer);
               dispatch("previewStart");
            }
         }
      }
      
      public function onPrerollDone(param1:String = "") : void {
         echo("onPrerollDone() - type: " + param1);
         setPrerollDone(param1 == "offline"?false:true);
         volume = _volume;
         if(_type == "recorded")
         {
            if(_playing)
            {
               if(!stream)
               {
                  if(_streamOwner && _streamOwner.connected && _brandId == "1" && !(_type == "monitor"))
                  {
                     createStream();
                  }
               }
               else
               {
                  corePlay();
               }
            }
         }
         dispatch("adsPrerollDone");
      }
      
      private function onPreviewTimer(... rest) : void {
         previewTimer.stop();
         var _loc2_:Object = {"rejectUrl":"http://www.ustream.tv/channel/" + mediaId};
         _loc2_.reason = defaultEmbedLimitMessage;
         if(Locale.hasInstance)
         {
            _loc2_.reason = Locale.instance.label("messageViewOnUstream");
         }
         Debug.debug("recorded","onPreviewTimer",_loc2_);
         addAlertToDisplayParent();
         handleRejectAlert(_loc2_);
         destroy();
      }
      
      protected function onGetAdsTimer(param1:TimerEvent) : void {
         echo("onGetAdsTimer()");
         getAdsTimer.stop();
         getAdsTimer.removeEventListener("timerComplete",onGetAdsTimer);
         getAdsTimer = null;
         if(adfreeStatus == 0 && !This.getReference(_modules,"meta.countryCode"))
         {
            getGeoAds();
         }
      }
      
      private function processCut(param1:Array) : void {
         cutStart = param1[0];
         cutEnd = param1[1];
         if(ignoreCut && (cutStart > 0 || cutEnd < 1))
         {
            dispatch("cut",false,
               {
                  "cutStart":cutStart,
                  "cutEnd":cutEnd
               });
            cutStart = 0;
            cutEnd = 1;
         }
         if(_modules.comments)
         {
            _modules.comments.processCut(cutStart,cutEnd);
         }
         if(_modules.meta)
         {
            _modules.meta.data.cut = [cutStart,cutEnd];
         }
      }
      
      private function processHighlight(param1:Array) : void {
         var _loc2_:Number = param1[0];
         var _loc3_:Number = param1[1];
         if(_loc2_ < cutStart || _loc2_ > cutEnd)
         {
            _loc2_ = 0.0;
         }
         else
         {
            _loc2_ = (_loc2_ - cutStart) / (cutEnd - cutStart);
         }
         if(_loc3_ < cutStart || _loc3_ > cutEnd)
         {
            _loc3_ = 1.0;
         }
         else
         {
            _loc3_ = (_loc3_ - cutStart) / (cutEnd - cutStart);
         }
         dispatch("hasHighlight",true,[_loc2_,_loc3_]);
      }
      
      private function onCutEnterFrame(param1:Event) : void {
         if((time - cutStart * _duration) / duration > 1)
         {
            playing = false;
            onStreamFinish();
            cutSprite.removeEventListener("enterFrame",onCutEnterFrame);
         }
      }
      
      protected function comScoreBeacon(param1:String = "") : void {
         if(_username == "AnimalPlanetLive")
         {
            echo("skipped APL comscore track!");
            return;
         }
         isComscoreCalled = true;
         var _loc5_:String = pageUrl;
         var _loc2_:String = "";
         var _loc6_:String = "";
         try
         {
            _loc2_ = ExternalInterface.call("function() { return document.referrer; }").toString();
            _loc6_ = ExternalInterface.call("function() { return document.title; }").toString();
            if("string" == "undefined" || _loc2_ == "null")
            {
               _loc2_ = "";
            }
            if("string" == "undefined" || _loc6_ == "null")
            {
               _loc6_ = "";
            }
         }
         catch(e:Error)
         {
            if(Logic.hasInstance && This.getReference(Logic.instance,"display.loaderInfo.url"))
            {
               _loc5_ = Logic.instance.display.loaderInfo.url;
            }
            echo("comScoreBeacon error: " + e);
         }
         if(_loc5_.length > 512)
         {
            _loc5_ = _loc5_.substr(0,512);
         }
         if(_loc2_.length > 512)
         {
            _loc2_ = _loc2_.substr(0,512);
         }
         var _loc3_:Array = This.getReference(this,"modules.meta.data.mainCategoryName") as Array;
         var _loc4_:String = _loc3_ && _loc3_.length?_loc3_.length > 1?_loc3_.join(","):_loc3_[0]:"";
         var _loc7_:String = new Array(This.secure?"https://sb":"http://b",".scorecardresearch.com/p","?c1=1","&c2=7716434","&c3=",This.onSite(_loc5_) & 2 && Locale.hasInstance && Locale.instance.language == "ja_JP"?"2":"1","&c4=",_loc3_ && _loc3_.length?getComscoreCategoryId((_loc3_[0] as String).toLowerCase()):999,"&c5=",escape(param1),"&c6=",escape(_loc4_),"&c7=",escape(_loc5_),"&c8=",escape(_loc6_),"&c9=",escape(_loc2_),"&rn=",Math.random(),"&cv=2.0").join("");
         if(_loc7_.length > 2080)
         {
            _loc7_ = _loc7_.substr(0,2080);
         }
         if(!comScoreLoader)
         {
            comScoreLoader = new URLLoader();
            comScoreLoader.addEventListener("securityError",onComscoreError);
            comScoreLoader.addEventListener("ioError",onComscoreError);
            comScoreLoader.addEventListener("asyncError",onComscoreError);
         }
         echo("comScoreBeacon() - url: " + _loc7_);
         comScoreLoader.load(new URLRequest(_loc7_));
      }
      
      private function getComscoreCategoryId(param1:String) : uint {
         var _loc2_:uint = 999;
         var _loc3_:* = param1;
         if("entertainment" !== _loc3_)
         {
            if("エンタメ" !== _loc3_)
            {
               if("sports" !== _loc3_)
               {
                  if("スポーツ" !== _loc3_)
                  {
                     if("pets, animals" !== _loc3_)
                     {
                        if("動物" !== _loc3_)
                        {
                           if("music" !== _loc3_)
                           {
                              if("音楽" !== _loc3_)
                              {
                                 if("education" !== _loc3_)
                                 {
                                    if("ハウツー" !== _loc3_)
                                    {
                                       if("gaming" !== _loc3_)
                                       {
                                          if("ゲーム" !== _loc3_)
                                          {
                                             if("spirituality" !== _loc3_)
                                             {
                                                if("宗教" !== _loc3_)
                                                {
                                                   if("events" !== _loc3_)
                                                   {
                                                      if("イベント" !== _loc3_)
                                                      {
                                                         if("technology" !== _loc3_)
                                                         {
                                                            if("技術" !== _loc3_)
                                                            {
                                                               if("news" !== _loc3_)
                                                               {
                                                                  if("ニュース" !== _loc3_)
                                                                  {
                                                                     if("24/7" !== _loc3_)
                                                                     {
                                                                        if("24時間" !== _loc3_)
                                                                        {
                                                                        }
                                                                     }
                                                                     _loc2_ = 183;
                                                                  }
                                                               }
                                                               _loc2_ = 175;
                                                            }
                                                         }
                                                         _loc2_ = 167;
                                                      }
                                                   }
                                                   _loc2_ = 157;
                                                }
                                             }
                                             _loc2_ = 148;
                                          }
                                       }
                                       _loc2_ = 137;
                                    }
                                 }
                                 _loc2_ = 126;
                              }
                           }
                           _loc2_ = 105;
                        }
                     }
                     _loc2_ = 95;
                  }
               }
               _loc2_ = 80;
            }
            return _loc2_;
         }
         _loc2_ = 71;
         return _loc2_;
      }
      
      private function onComscoreError(param1:Event) : void {
         echo("onComscoreError() - " + [param1.type,param1]);
      }
      
      protected function connect() : void {
         var _loc1_:* = null;
         echo("connect");
         if(stream)
         {
            destroyStream();
         }
         if(!_playing)
         {
            echo("not playing, skipped");
            return;
         }
         if(!_modules)
         {
            echo("connect() : no moduleManager");
            return;
         }
         if(!_modules.lock)
         {
            if(geoAds)
            {
               createAdverts(geoAds);
            }
            w84ModulesLock = false;
            if(provider)
            {
               isProgressive = provider is ProgressiveProvider;
               if(provider is ProgressiveProvider)
               {
                  _hasKeyData = true;
                  if(_duration > 0)
                  {
                     getProgressive();
                  }
                  else
                  {
                     echo("video has no duration",3);
                     _loc1_ = {};
                     _loc1_.url = provider.url;
                     if(_loc1_.url)
                     {
                        _loc1_.url = _loc1_.url + "/";
                        _loc1_.url = _loc1_.url + provider.streamName;
                     }
                     else if(resolvedStreamName)
                     {
                        _loc1_.url = resolvedStreamName;
                     }
                     else
                     {
                        _loc1_.url = provider.streamName;
                     }
                     
                     statistics.failure.report("vodInvalidDuration",provider.name,_loc1_);
                     onRejected({"unavailable":
                        {
                           "message":"Sorry, the video cannot be played because the file is corrupt.",
                           "messageToken":"flash.viewer.unableToPlayVideo"
                        }});
                  }
               }
               else
               {
                  streamName = provider.streamName;
                  fmsParams.videoName = provider.streamName;
                  fmsUrl = provider.url;
                  echo("## fmsUrl : " + fmsUrl);
                  connectToFms();
                  streamOwner = fms;
               }
            }
         }
         else
         {
            w84ModulesLock = true;
            dispatch("moduleLock");
         }
      }
      
      private function onGwStatus(param1:Object) : void {
         dispatch("gwStatus");
         Debug.debug("recorded","onGwStatus",param1);
      }
      
      protected function onGwCallFailed(... rest) : void {
         dispatch("gwError");
      }
      
      protected function connectToFms(... rest) : Connection {
         echo("connectToFms");
         if(fms)
         {
            fmsDestroy();
         }
         var _loc2_:String = _modules.stream && _modules.stream.logicUrl?_modules.stream.logicUrl:this.fmsUrl;
         fmsTimer.stop();
         hasFms = true;
         fms = new MultiConnection(null,type.toUpperCase() + " FMS",[1935,80,443],true);
         if(fmsStreamOwner)
         {
            streamOwner = fms;
         }
         fms.muted = false;
         fms.addEventListener("connected",onFmsConnected);
         fms.addEventListener("disconnected",onFmsDisconnected);
         fms.addEventListener("rejected",onFmsRejected);
         fms.addEventListener("failed",onFmsFailed);
         fms.addEventListener("balance",onFmsOverloaded);
         fms.addEventListener("streamNotFound",onFmsStreamNotFound);
         fmsParams.autoPlay = _autoPlay;
         if(application)
         {
            fmsParams.application = application;
         }
         if(password)
         {
            fmsParams.password = password;
         }
         if(user && !(user == "") && pass && !(pass == ""))
         {
            fmsParams.user = user;
            fmsParams.pass = pass;
         }
         if(Logic.loaderInfo && Logic.loaderInfo.loaderURL)
         {
            fmsParams.shimHost = Logic.loaderInfo.loaderURL;
         }
         if(sessionId)
         {
            fmsParams.sessionId = sessionId;
         }
         if(This.referrer)
         {
            fmsParams.referrer = This.referrer;
         }
         fmsParams.rpin = shared.rpin;
         Debug.debug("recorded","connect to " + _loc2_,fmsParams);
         fms.connect(_loc2_,fmsParams);
         if(provider && provider.name == "ustream")
         {
            statistics.benchmark.providerConnectStart();
         }
         dispatch("getConnection");
         return fms;
      }
      
      protected function onFmsConnected(... rest) : void {
         if(active)
         {
            dispatch("fmsConnected");
            if(streamOwner == fms)
            {
               createStream();
            }
            if(provider && provider.name == "ustream")
            {
               statistics.benchmark.providerConnectSuccess();
            }
         }
      }
      
      protected function onFmsDisconnected(... rest) : void {
         if(active && !rejected && !plannedDisconnected)
         {
            dispatch("fmsDisconnected");
            if(hasFms)
            {
               fmsStreamOwner = streamOwner == fms;
               if(fmsStreamOwner)
               {
                  destroyStream();
               }
               fmsTimer.start();
            }
         }
      }
      
      protected function onFmsRejected(param1:DynamicEvent) : void {
         if(fms)
         {
            rejected = true;
         }
         onRejected(param1.reason);
      }
      
      protected function onViewerLimitReached(... rest) : void {
         dispatch("viewerLimitReached");
         destroy();
      }
      
      protected function addAlertToDisplayParent() : void {
         if(alertHolder && alertHolder.contains(Alert.instance) && _media && _media.parent && _media.parent.stage)
         {
            alertHolder.removeChild(Alert.instance);
            _media.parent.addChild(Alert.instance);
         }
      }
      
      private function getEmbedLimitReachedReject() : Object {
         var _loc1_:Object = {};
         if(_brandId == "1")
         {
            _loc1_.reason = Locale.hasInstance?Locale.instance.label("messageViewOnUstream"):defaultEmbedLimitMessage;
            _loc1_.rejectUrl = "http://www.ustream.tv/channel/" + mediaId;
         }
         else
         {
            _loc1_.reason = Locale.hasInstance?Locale.instance.label("messageBroadcasterEmbedLimitReached"):"Broadcaster embed limit reached.";
         }
         return _loc1_;
      }
      
      protected function onEmbedLimitReached(... rest) : Boolean {
         var _loc2_:Boolean = !pageUrl || pageUrl && This.onSite(pageUrl) == 0;
         if(_loc2_)
         {
            addAlertToDisplayParent();
            handleRejectAlert(getEmbedLimitReachedReject());
            destroy();
         }
         return _loc2_;
      }
      
      protected function onRejected(param1:*, param2:String = null) : void {
         var _loc7_:* = NaN;
         var _loc9_:* = null;
         var _loc10_:* = null;
         var _loc8_:* = 0;
         var _loc5_:* = null;
         var _loc3_:* = null;
         var _loc4_:Object = 
            {
               "reason":param1,
               "rejectUrl":(param2?param2:null)
            };
         if(param1 is String)
         {
            if(param1 == "embedLimitExceeded")
            {
               if(!onEmbedLimitReached())
               {
                  dispatch("skippedReject");
               }
               else
               {
                  _loc4_.reason = param1;
               }
            }
            else
            {
               handleRejectAlert(_loc4_);
               dispatch("rejected",false,_loc4_);
            }
         }
         else if(param1 is Object)
         {
            _loc7_ = 0.0;
            _loc12_ = 0;
            _loc11_ = param1;
            for(_loc6_ in param1)
            {
               _loc7_++;
            }
            _loc9_ = ["0","reject"];
            _loc8_ = 0;
            while(_loc8_ < _loc9_.length)
            {
               if(_loc7_ == 1 && param1[_loc9_[_loc8_]] && param1[_loc9_[_loc8_]] is Object && !(param1[_loc9_[_loc8_]] is String))
               {
                  _loc14_ = 0;
                  _loc13_ = param1[_loc9_[_loc8_]];
                  for(_loc6_ in param1[_loc9_[_loc8_]])
                  {
                     param1[_loc6_] = param1[_loc9_[_loc8_]][_loc6_];
                  }
                  echo("kinull");
                  param1[_loc9_[_loc8_]] = null;
               }
               _loc8_++;
            }
            Debug.debug("recorded","reason: ",param1);
            if(param1.passwordLock && _loc7_ == 1)
            {
               onPassword();
            }
            else if(!((param1.authLock && param1.authLock == "uncheckable" && sessionId || param1.ipLock && param1.ipLock == "uncheckable") && varnish))
            {
               _loc10_ = ["unavailable","ban","pageLock","geoLock","appLock","embedViewerLimitLock","ipLock","viewerLimitLock","authLock","referrerLock","hashLock","sessionAuthLock"];
               _loc8_ = 0;
               while(_loc8_ < _loc10_.length)
               {
                  if(param1[_loc10_[_loc8_]])
                  {
                     _loc4_.reason = param1[_loc10_[_loc8_]];
                     _loc4_[_loc10_[_loc8_]] = param1[_loc10_[_loc8_]];
                  }
                  _loc8_++;
               }
               echo("onRejected(): " + [typeof _loc4_.reason,_loc4_.reason is String,_loc4_.reason is Object]);
               if(_loc4_.reason is String && _loc4_.reason == "channel embed limit reached")
               {
                  _loc5_ = getEmbedLimitReachedReject();
                  if(_loc5_.reason)
                  {
                     _loc4_.reason = _loc5_.reason;
                  }
                  if(_loc5_.rejectUrl)
                  {
                     _loc4_.rejectUrl = _loc5_.rejectUrl;
                  }
               }
               else if(_loc4_.reason is Object && !(_loc4_.reason is String) && !(_loc4_.reason.messageToken == null) && (Locale.hasInstance))
               {
                  if(_loc4_.reason.rejectUrl)
                  {
                     _loc4_.rejectUrl = _loc4_.reason.rejectUrl;
                  }
                  _loc3_ = Locale.instance.label(_loc4_.reason.messageToken,_loc4_.reason.messageParams?_loc4_.reason.messageParams:null);
                  echo("onRejected() - msg: " + [_loc3_,_loc4_.reason.messageToken]);
                  if(_loc4_["geoLock"] && _loc4_.reason.messageToken == "geoLock.limitReachedDescription" && _loc4_.reason.limit > 0)
                  {
                     _loc4_.geoLock.customLabels = ["geoLock.learnMore"];
                     _loc4_.rejectUrl = "https://www.ustream.tv/premium-membership";
                     _loc4_.rejectUrl = _loc4_.rejectUrl + ("?utm_campaign=PMP&utm_medium=FlashPlayer&utm_source=" + _loc4_.reason.countryCode);
                     _loc4_.rejectUrl = _loc4_.rejectUrl + ("&utm_content=" + _loc4_.reason.limit);
                     isLimitedGeoLock = true;
                  }
                  if(_loc4_["referrerLock"] && _loc4_.reason.messageToken == "refererLock.blacklistLock" && urlFailure == "multilevel_embed")
                  {
                     _loc3_ = Locale.instance.label("refererLock.multilevelEmbed");
                     _loc4_.rejectUrl = "https://www.ustream.tv/" + _type + "/" + _id;
                     _loc4_["referrerLock"].messageToken = "refererLock.multilevelEmbed";
                     isMultiLevelEmbedReject = _loc4_.reason.mediaTitle?_loc4_.reason.mediaTitle + "|" + _id:_id;
                  }
                  if(_loc3_ == "!" + _loc4_.reason.messageToken)
                  {
                     echo("missing translation");
                     _loc3_ = _loc4_.reason.message;
                  }
                  _loc4_.reason = _loc3_;
               }
               
               if(param1.rejectUrl)
               {
                  _loc4_.rejectUrl = param1.rejectUrl;
               }
               delete param1.passwordLock;
               addAlertToDisplayParent();
               handleRejectAlert(_loc4_);
               dispatch("rejected",false,_loc4_);
               Debug.debug("recorded","reason",param1);
               destroy();
            }
            
         }
         
      }
      
      protected function handleRejectAlert(param1:*) : void {
         var _loc2_:* = null;
         var _loc3_:* = null;
         var _loc6_:* = 0;
         var _loc4_:Object = param1 is String?{"reason":param1}:param1;
         redirectUrl = _loc4_.rejectUrl?_loc4_.rejectUrl:"";
         redirectUrl = _loc4_.url?_loc4_.url:redirectUrl;
         _loc4_.redirect = pageUrl == null || This.onSite(pageUrl) == 0?false:true;
         if(_loc4_.code && _loc4_.code == "BAN_DMCA" && _loc4_.redirect)
         {
            redirectUrl = pageUrl.indexOf("trunk.") != -1?"http://trunk.":"http://";
            redirectUrl = redirectUrl + ("www.ustream.tv/user-banned/" + modules.meta.username);
         }
         echo("handleRejectAlert - redirectUrl: " + redirectUrl + " - autoredirect: " + _loc4_.redirect + " - reason: " + _loc4_.reason);
         var _loc5_:* = true;
         if(urlFailure == "multilevel_embed" && isMultiLevelEmbedReject)
         {
            _loc5_ = false;
            isMultiLevelEmbedReject = "";
         }
         else if(isLimitedGeoLock)
         {
            _loc5_ = false;
         }
         else if(_loc4_.redirect && !(redirectUrl == ""))
         {
            if(This.onSite(This.pageUrl) & 2 && This.onSite(redirectUrl) & 2)
            {
               _loc5_ = false;
               redirectUrl = "";
            }
         }
         else
         {
            _loc5_ = false;
         }
         
         
         if(_loc5_)
         {
            onAlertClick();
         }
         else
         {
            _loc2_ = _loc4_.reason;
            if(Locale.hasInstance)
            {
               if(_loc4_.code && !(Locale.instance.label(_loc4_.code).substr(0,1) == "!"))
               {
                  _loc2_ = Locale.instance.label(_loc4_.code);
               }
               if(param1.hasOwnProperty("geoLock") && param1["geoLock"] && param1["geoLock"].customLabels)
               {
                  _loc3_ = [];
                  _loc6_ = 0;
                  while(_loc6_ < param1["geoLock"].customLabels.length)
                  {
                     _loc3_.push(Locale.instance.label(param1["geoLock"].customLabels[_loc6_]));
                     _loc6_++;
                  }
                  Alert.customButtonLabels = _loc3_;
               }
            }
            Alert.interaction = !(redirectUrl == "");
            rejectAlertId = Alert.show(_loc2_ || "",onAlertClick);
         }
      }
      
      protected function onAlertClick() : void {
         echo("redirect to " + redirectUrl);
         if(isLimitedGeoLock)
         {
            isLimitedGeoLock = false;
            Alert.interaction = false;
         }
         try
         {
            navigateToURL(new URLRequest(redirectUrl),"_blank");
         }
         catch(err:Error)
         {
            echo("onAlertClick() - navigateToURL error");
         }
         redirectUrl = "";
         rejectAlertId = 0;
      }
      
      protected function onFmsFailed(... rest) : void {
         dispatch("fmsFailed");
         if(type == "recorded")
         {
            if(id)
            {
               onFmsStreamNotFound();
            }
            else
            {
               destroy();
            }
         }
      }
      
      private function onFmsOverloaded(param1:DynamicEvent) : void {
         dispatch("fmsOverloaded");
         rejected = true;
         if(param1.logicUrl)
         {
            fmsStreamOwner = streamOwner == fms;
            fmsUrl = param1.logicUrl;
            fmsTimer.delay = 3000;
         }
         else
         {
            fmsTimer.delay = 30000 + Math.random() * 20000;
         }
         fmsTimer.start();
      }
      
      private function onFmsStreamNotFound(... rest) : void {
         fmsDestroy();
         if(!providerSwitch(true))
         {
            destroy();
         }
      }
      
      protected function fmsDestroy() : void {
         if(fms)
         {
            fms.removeEventListener("connected",onFmsConnected);
            fms.removeEventListener("disconnected",onFmsDisconnected);
            fms.removeEventListener("rejected",onFmsRejected);
            fms.removeEventListener("failed",onFmsFailed);
            fms.removeEventListener("balance",onFmsOverloaded);
            fms.removeEventListener("streamNotFound",onFmsStreamNotFound);
            if(streamOwner == fms)
            {
               destroyStream();
               streamOwner = null;
            }
            hasFms = false;
            fms.kill();
            fms = null;
            dispatch("fmsDestroy");
         }
         if(fmsTimer)
         {
            fmsTimer.stop();
         }
      }
      
      public function puppet(param1:Connection, param2:String, param3:Boolean = true) : void {
         streamOwner = param1;
         this.streamName = param2;
         _playing = true;
         createStream();
         if(_modules.logo)
         {
            _loc4_ = false;
            _modules.logo.recorded = _loc4_;
            _modules.logo.live = _loc4_;
         }
      }
      
      protected function getServerName(param1:String) : String {
         var _loc2_:uint = param1.indexOf("://") + 3;
         return param1.substr(_loc2_,param1.indexOf("/",_loc2_ + 1) - _loc2_);
      }
      
      protected function createStream(param1:String = "connectToFMS") : void {
         echo("createStream");
         var _loc2_:String = This.pageUrl;
         if(Shell.hasInstance && (cutter) && This.onSite(_loc2_) && _loc2_.indexOf("manage-show/videos") > -1)
         {
            adfreeStatus = 2;
         }
         if((!(_brandId == "1") || _type == "monitor" || adfreeStatus == 2) && !_prerollDone)
         {
            onPrerollDone();
         }
         if(_modules.lock)
         {
            return;
         }
         if(stream)
         {
            destroyStream();
         }
         handleStreamTypes();
         stream.addEventListener("netStatus",onStreamNetStatus);
         stream.addEventListener("ioError",onStreamIoError);
         stream.addEventListener("asyncError",onAsyncError);
         if(stream.client && !akamaiHd)
         {
            stream.client.onData = onStreamDataScript;
            stream.client.syncMeta = onSyncMeta;
         }
         if(type == "recorded")
         {
            buffer = _buffer;
         }
         stats.stream = stream;
         screen.attachNetStream(stream);
         dispatch("moduleInfo",false,
            {
               "info":{"qos":{"stream":stream}},
               "source":"media"
            });
         if(akamaiHd)
         {
            akamaiHd.setDisplayObject(screen);
         }
         showScreen();
         if(!takeover)
         {
            video = _video;
            audio = _audio;
         }
         volume = _volume;
         muted = _muted;
         progressiveSize = -1;
         if(!akamaiHd)
         {
            if(autoSeek > -1 && stream)
            {
               stream.soundTransform = new SoundTransform(0);
            }
            handleStreamTypes2();
            if(_type == "recorded" && !_prerollDone)
            {
               stream.pause();
            }
         }
         else
         {
            echo("akamaihd play");
            akamaiHd.play(provider);
         }
         if(quantcast)
         {
            quantcast.played(null);
         }
         if(_playing && (_autoPlay) && (_type == "channel" || (_type == "recorded" && (_prerollDone))))
         {
            play();
            statistics.benchmark.streamPlayStart();
            addEventListener("getStreamSize",onProviderStreamSize);
         }
         if(_type == "recorded")
         {
            dispatch("createStream");
         }
         echo("streamName : " + streamName);
         if(autoSeek != -1)
         {
            seek(autoSeek);
         }
         else if(cutStart)
         {
            seek(0);
         }
         
      }
      
      private function onSyncMeta(param1:Object) : void {
         if(param1 is String)
         {
            param1 = {"message":param1};
         }
         dispatch("syncMeta",true,param1);
      }
      
      protected function onStreamDataScript(param1:Object) : void {
         Debug.debug("recorded","onStreamDataScript() - afs: " + adfreeStatus,param1);
         if(param1.adCueTone && adfreeStatus == 0)
         {
            if(ads)
            {
               dispatch("adsLinearFeedAdBreak",false,param1.adCueTone);
            }
         }
      }
      
      private function createScreenSizeTimer() : void {
         echo("createScreenSizeTimer");
         if(!screenSizeTimer)
         {
            screenSizeTimer = new Timer(10000,1);
            screenSizeTimer.addEventListener("timerComplete",onScreenSizeTimerComplete);
         }
         screenSizeTimer.reset();
         screenSizeTimer.start();
      }
      
      private function onScreenSizeTimerComplete(param1:TimerEvent) : void {
         echo("onScreenSizeTimerComplete");
         if(stream && !_playing)
         {
            corePause("sst");
         }
         destroyScreenSizeTimer();
      }
      
      private function destroyScreenSizeTimer() : void {
         echo("destroyScreenSizeTimer");
         if(screenSizeTimer)
         {
            screenSizeTimer.removeEventListener("timerComplete",onScreenSizeTimerComplete);
            screenSizeTimer.stop();
            screenSizeTimer = null;
         }
      }
      
      private function parseStreamName(param1:String) : String {
         var _loc8_:* = null;
         var _loc6_:* = null;
         var _loc7_:* = null;
         var _loc4_:* = null;
         var _loc5_:* = 0;
         var _loc3_:* = null;
         var _loc2_:* = null;
         if(stream is FragmentStream)
         {
            _loc8_ = param1.split("?")[0];
            _loc6_ = param1.split("?")[1];
            if(_loc6_)
            {
               _loc7_ = _loc6_.split("&");
               _loc4_ = [];
               _loc5_ = 0;
               while(_loc5_ < _loc7_.length)
               {
                  _loc3_ = _loc7_[_loc5_].split("=")[0];
                  _loc2_ = _loc7_[_loc5_].split("=")[1];
                  if(!(_loc3_ == "ri") && !(_loc3_ == "rs") && !(_loc3_ == "int") && !(_loc3_ == "sr"))
                  {
                     _loc4_.push(_loc3_ + "=" + _loc2_);
                  }
                  _loc5_++;
               }
               if(_loc4_.length)
               {
                  echo("parseStreamName : " + _loc8_ + "?" + _loc4_.join("&"));
                  return _loc8_ + "?" + _loc4_.join("&");
               }
               echo("parseStreamName : " + _loc8_);
               return _loc8_;
            }
         }
         echo("parseStreamName : " + param1);
         return param1;
      }
      
      private function onProgressiveNoMetadata(param1:Event) : void {
         _hasKeyData = false;
         setProgressiveDone(true);
         getProgressive();
      }
      
      private function onFragmentExcessInterval(param1:Event) : void {
         _largeIntervals = true;
         setProgressiveDone(true);
         getProgressive();
      }
      
      private function onLastSecond(... rest) : void {
         echo("onLastSecond");
         if(_type == "recorded")
         {
            onStreamFinish();
         }
      }
      
      protected function destroyStream() : void {
         streamStatus = "";
         if(stream)
         {
            if(_modules && _modules.qos)
            {
               dispatch("moduleInfo",false,
                  {
                     "info":{"qos":false},
                     "source":"media"
                  });
            }
            if(stats)
            {
               stats.destroy();
            }
            if(progressClip.hasEventListener("enterFrame"))
            {
               progressClip.removeEventListener("enterFrame",getProgress);
            }
            stream.removeEventListener("netStatus",onStreamNetStatus);
            stream.removeEventListener("ioError",onStreamIoError);
            stream.removeEventListener("asyncError",onAsyncError);
            if(stream is FragmentStream)
            {
               stream.removeEventListener("FragmentStream.Metadata.NoKeyframes",onProgressiveNoMetadata);
               stream.removeEventListener("FragmentStream.Metadata.IntervalExceedsLimit",onFragmentExcessInterval);
               stream.removeEventListener("FragmentStream.Metadata.Missing",onProgressiveNoMetadata);
            }
            receiveAudio(false);
            stream.receiveAudio(false);
            stream.receiveVideo(false);
            stream.pause();
            stream.close();
            stream = null;
            dispatch("destroyStream");
         }
         screen.attachNetStream(null);
         screen.clear();
         hideScreen();
         if(autoSeek != -1)
         {
            screen.removeEventListener("enterFrame",onScreenEnterFrame);
            autoSeek = -1;
         }
         if(!_playing)
         {
            videoWidth = 0;
            videoHeight = 0;
         }
      }
      
      protected function streamInfo(param1:Object) : void {
         if(param1)
         {
            if(!(param1.audio == null) && !(param1.audio == _audio))
            {
               audio = param1.audio;
            }
            if(!(param1.video == null) && !(param1.video == _video))
            {
               video = param1.video;
            }
         }
      }
      
      private function chkScreenSize(... rest) : void {
         if(!(screen.videoWidth == videoWidth) || (!(mediaId == "3611918" || mediaId == "8095310") && !(screen.videoHeight == videoHeight)))
         {
            echo("chkScreenSize RESIZE TO " + [screen.videoWidth,screen.videoHeight] + " from " + [videoWidth,videoHeight]);
            videoWidth = screen.videoWidth;
            videoHeight = screen.videoHeight;
            if(mediaId == "3611918" || mediaId == "8095310")
            {
               _loc2_ = videoWidth * 9 / 16;
               screen.height = _loc2_;
               videoHeight = Math.round(_loc2_);
            }
            _streamRect = new Rectangle(0,0,videoWidth,videoHeight);
            locker.resize(videoWidth,videoHeight);
            if(geoAds)
            {
               createAdverts(geoAds);
            }
            hasStreamSize = true;
            if(!_playing && (bufferFull))
            {
               corePause("chk");
            }
            video = true;
            dispatch("getStreamSize",false,
               {
                  "width":_streamRect.width,
                  "height":_streamRect.height
               });
            display.update();
         }
      }
      
      public function getProgressive(param1:String = null) : void {
         echo("#getProgressive " + param1);
         if(_type == "channel")
         {
            return;
         }
         this.flv = param1;
         fms = new MultiConnection(null,"progressive");
         fms.muted = true;
         fms.connect(null);
         if(fms.connected)
         {
            onProgressiveConnected(null);
         }
         else
         {
            fms.addEventListener("connected",onProgressiveConnected);
         }
      }
      
      private function onProgressiveConnected(param1:Event) : void {
         if(param1 && param1.target is MultiConnection)
         {
            (param1.target as MultiConnection).removeEventListener("connected",onProgressiveConnected);
         }
         streamName = flv?flv:provider?provider.streamName:null;
         echo("#onProgressiveConnected " + streamName);
         var _loc2_:String = streamName.substr(0,streamName.indexOf("/",10));
         Security.loadPolicyFile(_loc2_ + "/crossdomain.xml");
         streamOwner = fms;
         createStream();
         dispatch("getProgressive");
         echo(streamName);
         if(!progressClip.hasEventListener("enterFrame"))
         {
            progressClip.addEventListener("enterFrame",getProgress);
         }
      }
      
      private function getProgress(... rest) : void {
         if(stream)
         {
            if(!(stream.bytesTotal == 0) && !(stream.bytesTotal == 4294967295) && !isNaN(stream.time))
            {
               if(progressiveSize < stream.bytesTotal)
               {
                  progressiveSize = stream.bytesTotal;
               }
            }
            else
            {
               progressiveSize = -1;
            }
            dispatchEvent(new ProgressEvent("progress",false,false,stream.bytesLoaded,stream.bytesTotal));
            if(stream.bytesLoaded / stream.bytesTotal == 1 && (progressClip.hasEventListener("enterFrame")))
            {
               progressClip.removeEventListener("enterFrame",getProgress);
            }
         }
      }
      
      public function play(... rest) : void {
         viewerPlaying = true;
         corePlay();
         setScreenVisibility();
      }
      
      protected function corePlay() : void {
         if(!cutSprite.hasEventListener("enterFrame") && cutEnd < 1)
         {
            cutSprite.addEventListener("enterFrame",onCutEnterFrame);
         }
         if(ads && !ads.started)
         {
            _autoPlay = true;
            dispatch("adsStart");
         }
         if(!ads || ads && ads.play())
         {
            if(ads)
            {
               dispatch("adsMediaStatus",false,{"playStatus":1});
            }
            if(detached)
            {
               attach();
            }
            _playing = true;
            _autoPlay = true;
            if(!_streamOwner)
            {
               connectToProvider();
            }
            if(stream)
            {
               handleStreamTypes3();
               if(quantcast)
               {
                  quantcast.resumed(null);
               }
            }
            _pausedByAdvert = false;
            if(_modules && _modules.layers)
            {
               _modules.layers.playing = true;
            }
            dispatch("play");
         }
         else
         {
            echo("play -> pause (w8 4 proxy)");
            corePause("ads");
         }
      }
      
      public function pause(... rest) : void {
         viewerPlaying = false;
         corePause();
      }
      
      protected function corePause(param1:String = "") : void {
         _pausedByAdvert = _pausedByAdvert || (_playing && param1 == "ads");
         echo("pause() :: pausedByAdvert = " + _pausedByAdvert);
         if(param1 == "chk")
         {
            destroyScreenSizeTimer();
         }
         _playing = false;
         dispatch("pause",false,{"reason":param1});
         if(stream)
         {
            if(ads)
            {
               dispatch("adsMediaStatus",false,{"playStatus":0});
            }
            stream.pause();
            if(quantcast)
            {
               quantcast.paused(null);
            }
         }
         if(_modules && _modules.layers)
         {
            _modules.layers.playing = false;
         }
      }
      
      public function togglePlaying() : void {
         playing = !_playing;
      }
      
      public function toggleMuted() : void {
         muted = !_muted;
      }
      
      public function seek(param1:Number, param2:Boolean = false) : Boolean {
         var _loc4_:* = false;
         var _loc3_:int = checkStreamDependencies();
         if(_loc3_ > -1)
         {
            if(_loc3_ & 2)
            {
               hideScreen();
            }
            if(_loc3_ & 1)
            {
               if(_backupVolume == -1)
               {
                  _backupVolume = volume;
               }
               if(stream)
               {
                  stream.soundTransform = new SoundTransform(0);
               }
            }
            autoSeek = param1;
            screen.addEventListener("enterFrame",onScreenEnterFrame);
            return false;
         }
         if(param2 && stream.bytesTotal > 0)
         {
            param1 = Math.min(cutStart + stream.bytesLoaded / stream.bytesTotal / (cutEnd - cutStart),param1);
            echo("inbuffer seek forced to " + Math.floor(param1 * 1000) / 1000);
         }
         stream.seek(_duration * (cutStart + (cutEnd - cutStart) * param1));
         return _loc4_;
      }
      
      private function onScreenEnterFrame(param1:Event) : void {
         var _loc2_:* = NaN;
         if(checkStreamDependencies() == -1)
         {
            if(autoSeek != -1)
            {
               _loc2_ = autoSeek;
               autoSeek = -1;
               seek(_loc2_);
            }
            if(_backupVolume != -1)
            {
               volume = _backupVolume;
               _backupVolume = -1;
            }
            showScreen();
            screen.removeEventListener("enterFrame",onScreenEnterFrame);
         }
      }
      
      private function checkStreamDependencies() : int {
         if(!stream || !_duration)
         {
            return 0;
         }
         return -1;
      }
      
      public function get offset() : Number {
         return duration?_offset / duration:0.0;
      }
      
      private function detach() : void {
         dispatch("detached");
         detached = true;
         screen.attachNetStream(null);
         if(_playing)
         {
            corePause();
         }
      }
      
      private function attach() : void {
         dispatch("attach");
         detached = false;
         screen.attachNetStream(stream);
      }
      
      protected function onStreamNetStatus(param1:NetStatusEvent) : void {
         echo("onStreamNetStatus " + param1.info.code);
         var _loc2_:* = param1.info.code;
         if("NetStream.Play.Stop" !== _loc2_)
         {
            if("NetStream.Play.StreamNotFound" !== _loc2_)
            {
               if("NetStream.Unpause.Notify" !== _loc2_)
               {
                  if("NetStream.Play.Transition" !== _loc2_)
                  {
                     if("NetStream.Play.Start" !== _loc2_)
                     {
                        if("NetStream.Buffer.Empty" !== _loc2_)
                        {
                           if("NetStream.Buffer.Full" !== _loc2_)
                           {
                              if("NetStream.Pause.Notify" !== _loc2_)
                              {
                                 if("NetStream.Seek.Notify" !== _loc2_)
                                 {
                                    if("NetStream.SeekStart.Notify" !== _loc2_)
                                    {
                                       if("FragmentStream.JumpStart.Notify" !== _loc2_)
                                       {
                                          if("NetStream.Seek.Failed" !== _loc2_)
                                          {
                                             if("NetStream.Seek.InvalidTime" !== _loc2_)
                                             {
                                                if("FragmentStream.Init.Started" !== _loc2_)
                                                {
                                                   if("FragmentStream.Init.Complete" !== _loc2_)
                                                   {
                                                      if("FragmentStream.Load.Error" === _loc2_)
                                                      {
                                                         providerSwitch(false);
                                                      }
                                                   }
                                                   else
                                                   {
                                                      setProgressiveDone(true);
                                                      if(video != (hasStreamSize || (!(stream.videoCodec == 0) && !(stream.videoCodec == 2048))))
                                                      {
                                                         video = hasStreamSize || (!(stream.videoCodec == 0) && !(stream.videoCodec == 2048));
                                                      }
                                                   }
                                                }
                                                else
                                                {
                                                   setProgressiveDone(false);
                                                }
                                             }
                                             else
                                             {
                                                dispatch("invalidTime");
                                                stream.seek(param1.info.details);
                                             }
                                          }
                                          else
                                          {
                                             dispatch("seekFailed");
                                          }
                                       }
                                       else
                                       {
                                          dispatch("seekStart");
                                          _statistics.benchmark.streamJumpStart();
                                       }
                                    }
                                    else
                                    {
                                       dispatch("seekStart");
                                       _statistics.benchmark.streamSeekStart();
                                    }
                                 }
                                 else
                                 {
                                    dispatch("seekNotify");
                                    if(detached)
                                    {
                                       attach();
                                    }
                                 }
                              }
                              else
                              {
                                 streamStatus = "NetStream.Pause.Notify";
                              }
                           }
                           else
                           {
                              statistics.common.bufferEmpty = false;
                              if(playTimer && !playTimer.running)
                              {
                                 playTimer.start();
                              }
                              if(type == "recorded")
                              {
                                 if(video != (hasStreamSize || (!(stream.videoCodec == 0) && !(stream.videoCodec == 2048))))
                                 {
                                    video = hasStreamSize || (!(stream.videoCodec == 0) && !(stream.videoCodec == 2048));
                                 }
                                 echo("hasVideo : " + hasVideo);
                                 echo("hasStreamSize : " + hasStreamSize);
                                 echo("video : " + video);
                                 echo("stream.videoCodec : " + stream.videoCodec);
                              }
                              bufferFull = true;
                              if(!_playing && videoWidth && videoHeight)
                              {
                                 echo("buffer full autoplay false pause");
                                 corePause("chk");
                              }
                              dispatch("bufferFull");
                              _statistics.benchmark.streamSeekComplete();
                              _statistics.benchmark.streamJumpComplete();
                           }
                        }
                        else
                        {
                           dispatch("bufferEmpty");
                           statistics.common.bufferEmpty = true;
                        }
                     }
                     else
                     {
                        streamStatus = "NetStream.Play.Start";
                        if(playTimer && !playTimer.running)
                        {
                           playTimer.start();
                        }
                        if(detached)
                        {
                           attach();
                        }
                        if(screenSizeTimer)
                        {
                           destroyScreenSizeTimer();
                        }
                        if(!_playing)
                        {
                           createScreenSizeTimer();
                        }
                     }
                  }
                  else
                  {
                     streamStatus = "NetStream.Play.Start";
                  }
               }
               else if(playTimer && !playTimer.running)
               {
                  playTimer.start();
               }
               
            }
            else
            {
               onStreamNotFound();
            }
         }
         else if(isProgressive && (_playing))
         {
            onStreamFinish();
         }
         
      }
      
      protected function onStreamNotFound() : void {
         streamStatus = "NetStream.Play.StreamNotFound " + streamName;
         destroyStream();
         if(!providerSwitch())
         {
            onStreamFinish();
         }
      }
      
      private function onStreamIoError(param1:IOErrorEvent) : void {
         streamStatus = "onStreamIoError";
         Debug.debug("recorded","ERROR : onStreamIoError",param1);
      }
      
      private function onAsyncError(param1:AsyncErrorEvent) : void {
         streamStatus = "onAsyncError";
         Debug.debug("recorded","ERROR : onAsyncError",param1);
      }
      
      protected function onMetaData(param1:Object) : void {
         _meta = param1;
         if(stream)
         {
            if(param1.duration)
            {
               _duration = param1.duration;
            }
            if(param1.videocodecid == undefined && !(param1.audiocodecid == undefined))
            {
               locker.resize(display.width,display.height);
               display.update();
            }
            if(!(param1.videocodecid == undefined) && !(param1.audiocodecid == undefined))
            {
               hasVideo = !(param1.videocodecid == 0);
               hasAudio = !(param1.audiocodecid == 0);
            }
         }
         dispatch("meta");
      }
      
      private function onPlayStatus(param1:Object) : void {
         echo("onPlayStatus " + param1);
         if(_type == "recorded")
         {
            if(param1.code == "NetStream.Play.Complete")
            {
               onStreamFinish(true);
            }
         }
      }
      
      protected function onStreamFinish(... rest) : void {
         dispatch("onStreamFinish");
         playing = false;
         if(playTimer)
         {
            playTimer.stop();
         }
         if(ads)
         {
            dispatch("adsMediaStatus",false,{"playStatus":2});
         }
         var _loc2_:Boolean = rest[0] != null?rest[0] as Boolean:false;
         if(_loc2_ && ads && ads.hasPostData())
         {
            ads.startPost();
         }
         else if(!ads || ads && ads.play())
         {
            if(ads != null)
            {
               ads.finished();
            }
            dispatch("finish");
         }
         
      }
      
      protected function showScreen() : void {
         _screenVisible = true;
         setScreenVisibility();
      }
      
      protected function hideScreen() : void {
         _screenVisible = false;
         setScreenVisibility();
      }
      
      public function get video() : Boolean {
         return _video;
      }
      
      public function set video(param1:Boolean) : void {
         _video = param1;
         if(!_takeover)
         {
            echo("set video : " + _video);
            if(!param1 && !playing)
            {
               corePause();
            }
            if(param1)
            {
               showScreen();
            }
            else
            {
               hideScreen();
            }
            dispatch("video");
         }
         else
         {
            takeOverVideo = _video;
         }
      }
      
      public function get smooth() : Boolean {
         return screen.smoothing;
      }
      
      public function set smooth(param1:Boolean) : void {
         screen.smoothing = param1;
      }
      
      public function get audio() : Boolean {
         return _audio;
      }
      
      public function set audio(param1:Boolean) : void {
         _audio = param1;
         if(!_takeover)
         {
            if(stream)
            {
               receiveAudio(param1 && !_muted);
            }
            dispatch("audio");
         }
         else
         {
            takeOverAudio = param1;
         }
      }
      
      public function get volume() : Number {
         return _volume;
      }
      
      public function set volume(param1:Number) : void {
         _volume = param1;
         _backupVolume = param1;
         if(stream && _audio)
         {
            stream.soundTransform = new SoundTransform(_prerollDone?param1 * (!_muted):0.0);
            dispatch("volumeChange",false,{"volume":param1});
         }
      }
      
      public function get muted() : Boolean {
         return _muted;
      }
      
      public function set muted(param1:Boolean) : void {
         _muted = param1;
         receiveAudio(!param1 && (_audio));
         dispatch("muted");
      }
      
      private function receiveAudio(param1:Boolean) : void {
         if(stream)
         {
            try
            {
               stream.soundTransform = new SoundTransform(_prerollDone && (param1)?_volume:0.0);
            }
            catch(e:Error)
            {
               echo("receiveAudio() :: ERROR: " + e.message);
            }
         }
         if(stream)
         {
            return;
         }
      }
      
      public function get playing() : Boolean {
         return _playing;
      }
      
      public function set playing(param1:Boolean) : void {
         if(param1)
         {
            play();
         }
         else
         {
            pause();
         }
      }
      
      public function get time() : Number {
         return stream?stream.time:0.0;
      }
      
      public function set seekTo(param1:Number) : void {
         seek(param1);
      }
      
      public function get progress() : Number {
         var _loc1_:* = NaN;
         if(duration && (time))
         {
            _loc1_ = time;
            _loc1_ = _loc1_ - cutStart * _duration;
            return Math.max(0,Math.min(1,_loc1_ / duration));
         }
         return -1;
      }
      
      public function get bufferStart() : Number {
         var _loc1_:* = NaN;
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         if(stream is FragmentStream && (stream as FragmentStream).duration)
         {
            _loc1_ = (stream as FragmentStream).duration;
            _loc2_ = cutStart * _loc1_;
            _loc3_ = cutEnd * _loc1_;
            return Math.max(0,((stream as FragmentStream).currentFragment.start - _loc2_) / (_loc3_ - _loc2_));
         }
         return 0;
      }
      
      public function get bufferProgress() : Number {
         var _loc1_:* = null;
         if(stream is FragmentStream)
         {
            _loc1_ = (stream as FragmentStream).currentFragment;
            return (_loc1_.end - _loc1_.start) / ((stream as FragmentStream).duration - _loc1_.start);
         }
         if(stream && stream.bytesTotal)
         {
            return stream.bytesLoaded / stream.bytesTotal;
         }
         return 0;
      }
      
      private function onCreateBuffer(... rest) : void {
         _modules.buffer.addEventListener("update",onBufferUpdate);
      }
      
      private function onBufferUpdate(... rest) : void {
         if(buffer != _modules.buffer.data)
         {
            buffer = _modules.buffer.data;
         }
      }
      
      public function get meta() : Object {
         return _meta;
      }
      
      public function get duration() : Number {
         return _duration * (cutEnd - cutStart);
      }
      
      public function get autoPlay() : Boolean {
         return _autoPlay;
      }
      
      public function set autoPlay(param1:Boolean) : void {
         _autoPlay = param1;
      }
      
      override public function get url() : String {
         if(This.getReference(Logic.instance,"channel.modules.rpinLock"))
         {
            return "http://broadcastforfriends.com/video/" + Logic.instance.channel.mediaId;
         }
         var _loc1_:String = "http://www.ustream.tv/";
         if(Logic.hasInstance && Logic.instance.scid)
         {
            _loc1_ = _loc1_ + ("schannel/" + Logic.instance.scid + "/recorded/" + _mediaId);
         }
         else
         {
            _loc1_ = _loc1_ + ("recorded/" + _mediaId);
            if(_highlightId)
            {
               _loc1_ = _loc1_ + ("/highlight/" + _highlightId);
            }
         }
         return _loc1_;
      }
      
      public function get brandId() : String {
         return _brandId;
      }
      
      public function get mediaId() : String {
         return _mediaId;
      }
      
      public function get bufferLength() : Number {
         return stream?stream.bufferLength:0.0;
      }
      
      public function get bufferTime() : Number {
         return stream?stream.bufferTime:0.0;
      }
      
      public function get keyframes() : Object {
         var _loc1_:Object = This.getReference(meta,"keyframes.times");
         return _loc1_;
      }
      
      public function get adverts() : Boolean {
         return !(ads == null);
      }
      
      public function get layer() : Layer {
         return _layer;
      }
      
      public function set layer(param1:Layer) : void {
      }
      
      public function get streamRect() : Rectangle {
         return _streamRect;
      }
      
      public function get streamBounds() : Rectangle {
         return screen.getBounds(media);
      }
      
      public function get hasKeyData() : Boolean {
         return _hasKeyData;
      }
      
      public function get buffer() : Number {
         return _buffer;
      }
      
      public function set buffer(param1:Number) : void {
         _buffer = param1;
         if(stream)
         {
            stream.bufferTime = _buffer;
            echo("set buffer " + _buffer);
         }
      }
      
      public function get highlightId() : Number {
         return _highlightId;
      }
      
      protected function get streamStatus() : String {
         return _streamStatus;
      }
      
      protected function set streamStatus(param1:String) : void {
         _streamStatus = param1;
      }
      
      protected function get streamOwner() : Connection {
         return _streamOwner;
      }
      
      protected function set streamOwner(param1:Connection) : void {
         _streamOwner = param1;
      }
      
      public function get sessionId() : String {
         return _sessionId;
      }
      
      public function get disabledGA() : Boolean {
         return !telemetry.isGA;
      }
      
      protected function needAdvert() : Boolean {
         return !(gwResult && gwResult.err_desc == "Nincs ilyen video") && (!layer || layer && !layer.connected);
      }
      
      protected function onPostRollDone(... rest) : void {
         dispatch("finish");
      }
      
      public function get takeover() : Boolean {
         return _takeover;
      }
      
      public function set takeover(param1:Boolean) : void {
         if(_takeover != param1)
         {
            echo("set takeover : " + param1);
            if(param1)
            {
               takeOverAudio = _audio;
               takeOverVideo = _video;
               video = false;
               audio = false;
               _takeover = param1;
            }
            else
            {
               _takeover = param1;
               audio = takeOverAudio;
               video = takeOverVideo;
            }
         }
      }
      
      protected function set clientIp(param1:String) : void {
         _clientIp = param1;
         if(ads)
         {
            ads.clientIp = param1;
         }
      }
      
      public function get hash() : String {
         return _hash;
      }
      
      public function get rpin() : String {
         return shared.rpin;
      }
      
      public function get userId() : String {
         return _userId;
      }
      
      public function get pausedByAdvert() : Boolean {
         return _pausedByAdvert;
      }
      
      public function logoOffset(param1:Number, param2:Number) : void {
         if(_modules && _modules.logo)
         {
            _modules.logo.logoOffset(param1,param2);
         }
      }
      
      override public function midEvent(param1:uint = 0) : void {
         echo("midEvent()");
         var _loc2_:Object = {"logData":{"media":(_type == "channel"?"c":"r") + "_" + _mediaId}};
         if(param1 == 0 && ads && ads.status == 9)
         {
            _loc2_.eventName = "MidWarnWatch";
            dispatch("adsPlayMidroll");
         }
         if(param1 == 1)
         {
            _loc2_.eventName = "MidWarnMore";
         }
      }
      
      protected function destroyAdverts(param1:Boolean = false) : void {
         if(ads)
         {
            echo("destroyAdverts()");
            ads.destruct();
            _loc4_ = 0;
            _loc3_ = adsEventList;
            for(_loc2_ in adsEventList)
            {
               ads.removeEventListener(_loc2_,adsEventList[_loc2_]);
            }
            ads = ads.destroy();
            geoAds = ads.destroy();
         }
         else if(geoAdsGw)
         {
            destructGeoAdsGw();
         }
         
         if(param1)
         {
            geoAds = null;
         }
         if(playTimer)
         {
            playTimer.stop();
            playTimer.removeEventListener("timer",onPlayTimer);
            playTimer = null;
         }
      }
      
      protected function createAdverts(param1:Object) : void {
         var _loc3_:* = false;
         var _loc4_:String = This.getReference(this,"modules.ppv.status") as String;
         echo("createAdverts() " + ["countryCode: " + shared.countryCode,"geoAdsGw: " + geoAdsGw,"geoAds: " + geoAds,"p: " + _loc4_]);
         if(shared.countryCode && geoAdsGw && !geoAds || Logic.hasInstance && (Logic.instance.isLimitedChannel) || This.getReference(_modules,"advert.disableAds") as Boolean)
         {
            onPrerollDone();
            return;
         }
         if((geoAds && !geoAds.empty || param1 && param1.revshare) && needAdvert() && !ads && (_loc4_ == null || _loc4_ == "pre") && adfreeStatus == 0)
         {
            Debug.debug("recorded","data",param1);
            echo("---------------------------------");
            _loc3_ = false;
            if(param1)
            {
               if(param1.pre && param1.pre.length)
               {
                  _loc3_ = true;
               }
               if(param1.mid && param1.mid.length)
               {
                  _loc3_ = true;
               }
               if(param1.post && param1.post.length)
               {
                  _loc3_ = true;
               }
               if(param1.partner && param1.partner.length)
               {
                  _loc3_ = true;
               }
            }
            if(!_loc3_)
            {
               echo("ERROR: there are no ads in data!");
               return;
            }
            destroyAdverts();
            ads = new Ads();
            _loc6_ = 0;
            _loc5_ = adsEventList;
            for(_loc2_ in adsEventList)
            {
               ads.addEventListener(_loc2_,adsEventList[_loc2_]);
            }
            ads.media = this;
            dispatch("adsInit",false,
               {
                  "clientIp":_clientIp,
                  "adaptvParams":adaptvParams
               });
            dispatch("adsAdHolder",false,{"adHolder":adHolder});
            if(This.onSite(This.pageUrl) & 2)
            {
               dispatch("adsDivId",false,{"divId":"channelFlashContent"});
            }
            if(gwResult && (gwResult.video_length || gwResult.videoLength))
            {
               dispatch("adsMediaDuration",false,{"duration":gwResult.video_length || gwResult.videoLength});
            }
            if(Logic.hasInstance && !(Logic.instance.viewMode == "embed"))
            {
               viewMode = Logic.instance.viewMode;
            }
            dispatch("adsData",false,{"adData":param1});
            if(!playTimer)
            {
               playTimer = new Timer(1000);
               playTimer.addEventListener("timer",onPlayTimer);
            }
            if(autoPlay)
            {
               dispatch("adsStart");
            }
         }
         else if((geoAds && geoAds.empty || !(_loc4_ == null) && !(_loc4_ == "pre")) && !_prerollDone)
         {
            onPrerollDone();
         }
         
      }
      
      private function createRevShare(param1:Object) : void {
         echo("createRevShare() - ads: " + !(ads == null) + " - adfreeStatus: " + adfreeStatus);
         if(adfreeStatus == 0)
         {
            Debug.debug("recorded","data",param1);
            if(!ads && param1)
            {
               param1.revshare = true;
               createAdverts(param1);
            }
            else if(ads && !ads.isRevshare)
            {
               dispatch("adsRevshare",false,{"data":param1});
            }
            
         }
      }
      
      private function forwardAdsEvent(param1:DynamicEvent) : void {
         if(param1.type != "adsMidrollNotification")
         {
            Debug.debug("recorded","forwardAdsEvent() - " + param1.type,param1.data);
         }
         dispatch(param1.type,param1.bubbles,param1.data,param1.type == "adsMidrollNotification");
      }
      
      private function onAdsPlayContent(param1:Event) : void {
         echo("onAdsPlayContent() - " + [_playing,viewerPlaying,_autoPlay]);
         if(viewerPlaying)
         {
            play();
         }
      }
      
      private function onAdsPauseContent(param1:Event) : void {
         echo("onAdsPauseContent() - " + [_playing,viewerPlaying,_autoPlay]);
         if(_playing)
         {
            corePause("ads");
         }
      }
      
      private function onAdsLogAdEvent(param1:DynamicEvent) : void {
         var _loc3_:* = null;
         var _loc2_:String = "onAdsLogAdEvent() - eventName: " + param1.data.eventName;
         if(param1.data.eventName == "adShow")
         {
            echo(_loc2_ + " - host: " + param1.host + " - adType: " + param1.adType);
            _loc3_ = "";
            _loc4_ = param1.adType;
            if("preroll" !== _loc4_)
            {
               if("midroll" !== _loc4_)
               {
                  if("postroll" !== _loc4_)
                  {
                     if("mid" === _loc4_)
                     {
                        _loc3_ = "12";
                     }
                  }
                  else
                  {
                     _loc3_ = "11";
                  }
               }
               else
               {
                  _loc3_ = "10";
               }
            }
            else
            {
               _loc3_ = "09";
            }
            if(!(_loc3_ == "") && (!Logic.hasInstance || (Logic.hasInstance && !Logic.instance.isLimitedChannel)))
            {
               comScoreBeacon(_loc3_ + "0100");
            }
         }
      }
      
      protected function getGeoAds() : void {
         var _loc1_:* = null;
         var _loc2_:* = null;
         if(geoAds)
         {
            echo("getGeoAds() - the request has been already responsed");
            createAdverts(geoAds);
         }
         else if(!geoAdsGw && !geoAds && (_mediaId) > 0 && (!Logic.hasInstance || (Logic.hasInstance && !Logic.instance.isLimitedChannel)) && (["3954133"] as Array).indexOf(_mediaId) == -1)
         {
            _loc1_ = 
               {
                  "brandId":_brandId,
                  "mediaId":_mediaId,
                  "mediaType":_type,
                  "countryCode":(shared.countryCode?shared.countryCode.toLowerCase():"us")
               };
            if(_type == "recorded" && _highlightId)
            {
               _loc1_.highlightId = _highlightId;
            }
            if(Locale.hasInstance)
            {
               _loc1_.locale = Locale.instance.language;
            }
            if(This.secure)
            {
               _loc2_ = "https://rgw.ustream.tv/gateway.php?Viewer.getAdverts";
            }
            else
            {
               _loc2_ = "http://cdngw.ustream.tv/rgw/" + gwCalls.getAdverts.replace(new RegExp("\\.","g"),"/") + "?";
               _loc2_ = _loc2_ + ("brandId=" + _brandId + "&mediaId=" + _mediaId + "&mediaType=" + _type + "&countryCode=" + (shared.countryCode?shared.countryCode.toLowerCase():"us"));
               if(_type == "recorded" && _highlightId)
               {
                  _loc2_ = _loc2_ + ("&highlightId=" + _highlightId);
               }
               if(Locale.hasInstance)
               {
                  _loc2_ = _loc2_ + ("&locale=" + Locale.instance.language);
               }
               _loc3_ = pageUrl;
               _loc1_.pageUrl = _loc3_;
               _loc2_ = _loc2_ + ("&" + getParsedUrlVariable("pageUrl",_loc3_));
               if(_loc2_.lastIndexOf("&") == _loc2_.length - 1)
               {
                  _loc2_ = _loc2_.substr(0,_loc2_.length - 1);
               }
            }
            geoAdsGwResponde = new Responder(onGeoAdsGwResult,onGeoAdsGwStatus);
            geoAdsGw = new Connection("GEOADGW-C");
            geoAdsGw.addEventListener("netStatus",onGeoAdsNetStatus);
            geoAdsGw.addEventListener("securityError",onGeoAdsError);
            geoAdsGw.addEventListener("callFailed",onGeoAdsError);
            geoAdsGw.connect(_loc2_);
            echo("getGeoAds()");
            echo(gwCalls.getAdverts + " @ " + _loc2_);
            Debug.debug("recorded","args",_loc1_);
            geoAdsGw.call(gwCalls.getAdverts,geoAdsGwResponde,_loc1_);
            geoAdsTimeout = new Timer(5000,1);
            geoAdsTimeout.addEventListener("timerComplete",onGeoAdsError);
            geoAdsTimeout.start();
         }
         
      }
      
      private function destructGeoAdsGw() : void {
         if(geoAdsGw)
         {
            echo("destructGeoAdsGw()");
            geoAdsGw.removeEventListener("netStatus",onGeoAdsNetStatus);
            geoAdsGw.removeEventListener("securityError",onGeoAdsError);
            geoAdsGw.removeEventListener("callFailed",onGeoAdsError);
            geoAdsGw.close();
            geoAdsGw = null;
            geoAdsGwResponde = null;
         }
         if(geoAdsTimeout)
         {
            geoAdsTimeout.stop();
            geoAdsTimeout.removeEventListener("timerComplete",onGeoAdsError);
            geoAdsTimeout = null;
         }
      }
      
      private function onGeoAdsError(... rest) : void {
         echo("onGeoAdsError() - e: " + rest);
         destructGeoAdsGw();
         onPrerollDone();
      }
      
      private function onGeoAdsGwStatus(param1:Object) : void {
         echo("onGeoAdsGwStatus() - status: " + param1.info.code);
         destructGeoAdsGw();
      }
      
      private function onGeoAdsNetStatus(param1:NetStatusEvent) : void {
         echo("onGeoAdsNetStatus() - status: " + param1.info.code);
         destructGeoAdsGw();
      }
      
      private function onGeoAdsGwResult(param1:Object) : void {
         Debug.debug("recorded","onGeoAdsGwResult()",param1);
         echo("response of getAdverts");
         if(_active && param1 && param1.success)
         {
            if(param1.ads)
            {
               geoAds = param1.ads as Object;
               if(param1.ads == "")
               {
                  dispatch("adsCompanion",false,{"companion":false});
                  onPrerollDone();
                  geoAds = {"empty":true};
               }
               else
               {
                  if(!geoAds.pre || geoAds.pre && !geoAds.pre.length)
                  {
                     dispatch("adsCompanion",false,{"companion":false});
                  }
                  createAdverts(geoAds);
               }
            }
            else
            {
               echo("call onPrerollDone()");
               onPrerollDone();
               geoAds = {};
               adfreeStatus = 2;
            }
            destructGeoAdsGw();
         }
      }
      
      private function onCreateAdvert(... rest) : void {
         echo("onCreateAdvert()");
         _modules.advert.addEventListener("update",onAdvertUpdate);
      }
      
      private function onAdvertUpdate(... rest) : void {
         var _loc2_:* = null;
         var _loc3_:* = null;
         echo("onAdvertUpdate()");
         if(This.getReference(_modules,"advert.revShare"))
         {
            Debug.debug("recorded","_modules.advert",_modules.advert);
            _loc2_ = (This.getReference(_modules,"advert.revShare.ads.mid") as Array)[0] as Object;
            _loc3_ = Locale.hasInstance?Locale.instance.language:"en_US";
            if(_loc2_.provider == "notification" && ((_loc2_.locales as Array).indexOf(_loc3_) == -1 || !(adfreeStatus == 0) || ads && !ads.hasMidrolls || geoAds && (geoAds.mid && !geoAds.mid.length || geoAds.empty) || ads && !ads.hasPreroll || geoAds && (geoAds.pre && !geoAds.pre.length || geoAds.empty) || _loc2_.isEmbedOnly && This.onSite(This.pageUrl) & 2))
            {
               echo("skipped notification: " + _loc3_ + " - " + _loc2_.locales + " - adfree: " + adfreeStatus);
               return;
            }
            isRevShare = true;
            _modules.advert.revShare.ads.revshare = true;
            createRevShare(_modules.advert.revShare.ads);
         }
         if(This.getReference(_modules,"advert.disableAds") && ads)
         {
            destroyAdverts(true);
         }
         if(This.getReference(_modules,"advert.init"))
         {
            getGeoAds();
         }
      }
      
      protected function resizeAd() : void {
         var _loc1_:* = NaN;
         var _loc2_:uint = 0;
         if(Logic.hasInstance && Logic.instance.adOffset && !_adOffsetLock)
         {
            if(Logic.instance.adOffset > 0 && (Logic.instance.autoAdOffset) && locker && locker.stage)
            {
               _loc1_ = locker.stage.stageHeight - (locker.y + locker.height);
               if(_loc1_ < 26)
               {
                  _loc2_ = 26 - _loc1_;
               }
            }
            else if(Logic.instance.adOffset > 0)
            {
               _loc2_ = Logic.instance.adOffset;
            }
            
         }
         if(adHolder && (!(adHolder.width == locker.width) || !(adHolder.height == locker.height - _loc2_)))
         {
            adHolder.width = locker.width;
            adHolder.height = locker.height - _loc2_;
            if(ads)
            {
               ads.setSize(locker.width,locker.height - _loc2_);
            }
         }
      }
      
      private function onAdHidden(param1:Event) : void {
         var _loc2_:* = 0.0;
         var _loc3_:* = ads.adHostName;
         if("yahooim" !== _loc3_)
         {
            if("googleads" !== _loc3_)
            {
               if("adaptv" !== _loc3_)
               {
                  if("adotube" === _loc3_)
                  {
                     _loc2_ = 20.0;
                  }
               }
               else
               {
                  _loc2_ = 20.0;
               }
            }
            else
            {
               _loc2_ = 22.0;
            }
         }
         else
         {
            _loc2_ = 17.0;
         }
         if(screen)
         {
            screen.adOffset = _loc2_;
         }
         super.dispatch("adSize",true,{"size":_loc2_});
      }
      
      private function onAdShown(param1:Event) : void {
         var _loc2_:* = 0.0;
         var _loc3_:* = ads.adHostName;
         if("yahooim" !== _loc3_)
         {
            if("googleads" !== _loc3_)
            {
               if("adaptv" !== _loc3_)
               {
                  if("adotube" === _loc3_)
                  {
                     _loc2_ = 70.0;
                  }
               }
               else
               {
                  _loc2_ = 60.0;
               }
            }
            else
            {
               _loc2_ = 71.0;
            }
         }
         else
         {
            _loc2_ = 70.0;
         }
         if(screen)
         {
            screen.adOffset = _loc2_;
         }
         super.dispatch("adSize",true,{"size":_loc2_});
      }
      
      private function onAdRemoved(param1:Event) : void {
         if(screen)
         {
            screen.adOffset = 0;
         }
         super.dispatch("adSize",true,{"size":0});
      }
      
      private function onAdsPause(param1:DynamicEvent) : void {
         echo("onAdsPause() - reason: " + param1.data.reason);
         _adIdle = param1.data.reason;
      }
      
      public function continueAd() : void {
         echo("continueAd()");
         dispatch("adsResume");
      }
      
      private function onAdsContentAudio(param1:DynamicEvent) : void {
         echo("onAdsContentAudio() - enable: " + param1.data.enable);
         if(stream)
         {
            stream.soundTransform = new SoundTransform(param1.data.enable?_volume:0.0);
         }
      }
      
      protected function onPlayTimer(param1:TimerEvent) : void {
         if(stream && ads)
         {
            ads.setPlayTime(stream.time);
         }
      }
      
      public function get adIdle() : uint {
         return _adIdle;
      }
      
      public function get adOffsetLock() : Boolean {
         return _adOffsetLock;
      }
      
      public function set adOffsetLock(param1:Boolean) : void {
         echo("set adOffsetLock: " + param1);
         _adOffsetLock = param1;
      }
      
      public function set adOffset(param1:uint) : void {
         if(Logic.hasInstance)
         {
            Logic.instance.adOffset = param1;
            resizeAd();
         }
      }
      
      public function set viewMode(param1:String) : void {
         echo("set viewMode: " + param1);
         dispatch("viewMode");
      }
      
      public function get prerollDone() : Boolean {
         return _prerollDone;
      }
      
      protected function setPrerollDone(param1:Boolean) : void {
         if(_prerollDone != param1)
         {
            _prerollDone = param1;
            setScreenVisibility();
         }
      }
      
      protected function setProgressiveDone(param1:Boolean) : void {
         if(_progressiveDone != param1)
         {
            _progressiveDone = param1;
            setScreenVisibility();
         }
      }
      
      private function setScreenVisibility() : void {
         var _loc2_:* = false;
         var _loc1_:* = NaN;
         if(screen)
         {
            screen.visible = _screenVisible && (_prerollDone || !_autoPlay) && (_progressiveDone);
            echo("screen.visible : " + screen.visible);
         }
         if(stream)
         {
            _loc2_ = screen.videoWidth > 0 || screen.videoHeight > 0;
            _loc1_ = !_muted && (_loc2_ && (screen.visible) || !_loc2_ || !_video)?_volume:0.0;
            stream.soundTransform = new SoundTransform(_loc1_);
         }
      }
      
      public function get hasStream() : Boolean {
         return !(stream == null);
      }
      
      public function get viewerVersion() : uint {
         return _viewerVersion;
      }
      
      protected function onRendererChange(param1:Event) : void {
         var _loc2_:* = false;
         echo("onRendererChange");
         if(stream && !(stream is FragmentStream))
         {
            _loc2_ = playing;
            corePause();
            seek(progress);
            if(_loc2_)
            {
               corePlay();
            }
         }
      }
      
      public function addOverlay(param1:*) : void {
         _controlOverlay.addChild(param1);
      }
      
      public function get stageVideo() : Boolean {
         return screen.stageVideo;
      }
      
      public function get statistics() : Statistics {
         return _statistics;
      }
      
      public function get ums() : ViewerUms {
         return _ums;
      }
      
      public function get channelId() : String {
         return _channelId;
      }
      
      public function get viewerBuildInfo() : String {
         return _viewerBuildInfo;
      }
      
      private function onCreateQos(param1:Event) : void {
         var _loc2_:* = null;
         _modules.addEventListener("destroyQos",onQosDestroy);
         var _loc3_:Qos = (param1.target as ViewerModuleManager).qos;
         if(_loc3_)
         {
            _loc3_.addEventListener("streamStalled",onQosStreamStalled);
            _loc3_.addEventListener("usedBandwidth",onQosUsedBandwidth);
            _loc3_.addEventListener("update",onQosUpdate);
            if(This.pageUrl.indexOf("#qos[") != -1)
            {
               _loc2_ = This.pageUrl.split("#qos[")[1].split("]")[0].split(":");
               if(!isNaN(_loc2_[0]))
               {
                  _loc3_.measureInterval = _loc2_[0];
                  Debug.debug("RECORDED","qos measure interval set to : " + _loc3_.measureInterval);
               }
               if(!isNaN(_loc2_[1]))
               {
                  qosMaxEmptyCount = _loc2_[1];
                  Debug.debug("RECORDED","qos max empty count set to : " + qosMaxEmptyCount);
               }
               if(!isNaN(_loc2_[2]))
               {
                  qosMaxEmptyTime = _loc2_[2];
                  Debug.debug("RECORDED","qos max empty time set to : " + qosMaxEmptyTime);
               }
            }
         }
      }
      
      private function onQosDestroy(param1:Event) : void {
         _modules.removeEventListener("destroyQos",onQosDestroy);
         var _loc2_:Qos = param1.target as Qos;
         if(_loc2_)
         {
            _loc2_.removeEventListener("streamStalled",onQosStreamStalled);
            _loc2_.removeEventListener("usedBandwidth",onQosUsedBandwidth);
            _loc2_.removeEventListener("update",onQosUpdate);
         }
      }
      
      private function onQosUsedBandwidth(param1:DynamicEvent) : void {
         var _loc2_:* = null;
         if(param1.data.url)
         {
            _loc2_ = param1.data.url;
         }
         else
         {
            _loc2_ = _streamOwner.label != "progressive"?_streamOwner.url:"";
            if(_loc2_.length && !(_loc2_.substr(-1,1) == "/"))
            {
               _loc2_ = _loc2_ + "/";
            }
            _loc2_ = _loc2_ + playedStreamName;
         }
         statistics.benchmark.bandwidthReport(_loc2_,param1.data.bytesLoaded,param1.data.time);
      }
      
      private var qosMaxEmptyCount:uint = 5;
      
      private var qosMaxEmptyTime:uint = 7000;
      
      private var _contextMenuItems:ContextMenuItems;
      
      private function onQosUpdate(param1:Event) : void {
         var _loc2_:Qos = param1.target as Qos;
         Debug.debug("RECORDED","onQosUpdate : " + _loc2_.stable);
         Debug.debug("RECORDED","module.emptyCount > qosMaxEmptyCount " + _loc2_.emptyCount + " " + qosMaxEmptyCount);
         Debug.debug("RECORDED","module.emptyRate * module.measureInterval > qosMaxEmptyTime " + _loc2_.emptyRate * _loc2_.measureInterval + " " + qosMaxEmptyTime);
         if(_loc2_.emptyCount > qosMaxEmptyCount || _loc2_.emptyRate * _loc2_.measureInterval > qosMaxEmptyTime)
         {
            onQosStreamStalled(param1);
         }
      }
      
      protected function onQosStreamStalled(param1:Event) : void {
         echo("onQosStreamStalled");
         if(!akamaiHd && (!_modules.quality || _modules.quality && (!_modules.quality.adaptiveMbr || (_modules.quality.adaptiveMbr && _modules.quality.desiredQuality == 0))))
         {
            providerSwitch(false);
         }
      }
      
      protected function providerSwitch(param1:Boolean = true, param2:String = null) : Boolean {
         echo("providerSwitch");
         var _loc3_:Array = This.getReference(this,"modules.stream.providerList") as Array;
         if(_loc3_ && _loc3_.length > 1)
         {
            if(resolvedStreamName)
            {
               resolvedStreamName = null;
            }
            dispatch(param1?"providerFailed":"providerSwitch",false,param2?{"preferred":param2}:null);
            return true;
         }
         return false;
      }
      
      protected function hasNonUhsProvider() : Boolean {
         var _loc1_:* = 0;
         var _loc2_:Array = This.getReference(this,"modules.stream.providerList") as Array;
         if(_loc2_ && _loc2_.length > 1)
         {
            _loc1_ = 0;
            while(_loc1_ < _loc2_.length)
            {
               if(_loc2_[_loc1_].name && _loc2_[_loc1_].name.indexOf("uhs") == -1)
               {
                  return true;
               }
               _loc1_++;
            }
         }
         return false;
      }
      
      override public function dispatchEvent(param1:Event) : Boolean {
         var _loc2_:* = null;
         if(param1 is DynamicEvent && param1.type == "moduleInfo")
         {
            _loc2_ = param1 as DynamicEvent;
            if(_loc2_.data.info && _loc2_.data.info.hasOwnProperty("streamInfo"))
            {
               streamInfo(_loc2_.data.info.streamInfo);
               delete _loc2_.data.info.streamInfo;
            }
            if(_loc2_.data.source == "ums" && _loc2_.data.info && !(_loc2_.data.info.stream == undefined))
            {
               destroyUmsFallback();
            }
         }
         return super.dispatchEvent(param1);
      }
      
      private function onProviderStreamSize(param1:Event) : void {
         if(param1 && param1.target is EventDispatcher)
         {
            param1.target.removeEventListener("getStreamSize",onProviderStreamSize);
         }
         statistics.benchmark.streamGetSizeAfterPlay();
      }
      
      protected function createUmsFallback() : void {
         if(!This.secure && !umsFallback)
         {
            Debug.debug("recorded","createUmsFallback");
            umsFallback = new UmsStaticFallback(mediaId);
            umsFallback.addEventListener("update",onUmsFallbackUpdate);
         }
      }
      
      private function destroyUmsFallback() : void {
         if(umsFallback)
         {
            Debug.debug("recorded","destroyUmsFallback");
            umsFallback.removeEventListener("update",onUmsFallbackUpdate);
            umsFallback = umsFallback.destroy();
         }
      }
      
      private function onUmsFallbackUpdate(param1:DynamicEvent) : void {
         echo("onUmsFallbackUpdate");
         if(param1.data.moduleInfo)
         {
            super.dispatchEvent(new DynamicEvent("moduleInfo",false,false,
               {
                  "info":param1.data.moduleInfo,
                  "source":"ums"
               }));
         }
      }
      
      private function onCCDetected(param1:Event) : void {
         var _loc2_:* = null;
         if(_modules && !_modules.closedCaption && This.getReference(_modules,"broadcaster.data.benefits") is Array && !((This.getReference(_modules,"broadcaster.data.benefits") as Array).indexOf("closed_caption") == -1) || _type == "recorded")
         {
            _loc2_ = {};
            _loc2_.closedCaption = {};
            _loc2_.closedCaption.enabled = screen.ccVisible;
            if(_type == "channel" && !((["17351930","15226679","17649362","16975426"] as Array).indexOf(_mediaId) == -1))
            {
               _loc2_.closedCaption.enabled = true;
               screen.ccVisible = true;
            }
            dispatchEvent(new DynamicEvent("moduleInfo",false,false,
               {
                  "info":_loc2_,
                  "source":"screen"
               }));
         }
      }
      
      private function onCreateCC(param1:Event) : void {
         var _loc2_:* = null;
         if(param1 && param1.target is ViewerModuleManager)
         {
            _loc2_ = (param1.target as ViewerModuleManager).closedCaption;
            if(_loc2_)
            {
               _loc2_.addEventListener("update",onCCUpdate);
               _loc2_.addEventListener("destroy",onCCDestroy);
            }
         }
      }
      
      private function onCCDestroy(param1:Event) : void {
         var _loc2_:* = null;
         if(param1 && param1.target is ClosedCaption)
         {
            _loc2_ = param1.target as ClosedCaption;
            _loc2_.removeEventListener("update",onCCUpdate);
            _loc2_.removeEventListener("destroy",onCCDestroy);
         }
      }
      
      private function onCCUpdate(param1:Event) : void {
         var _loc2_:* = null;
         if(param1 && param1.target is ClosedCaption)
         {
            _loc2_ = param1.target as ClosedCaption;
            if(screen)
            {
               screen.ccVisible = _loc2_.enabled;
            }
         }
      }
      
      protected function handleStreamTypes(param1:String = "connectToFMS") : void {
         if(akamaiHd)
         {
            stream = akamaiHd.stream;
         }
         else
         {
            stream = isProgressive && (_hasKeyData)?_largeIntervals || (cutter) || !This.compareVersion("10.3")?new ProgressiveStream(streamOwner,param1):new FragmentStream(streamOwner,param1):new NetStream(streamOwner,param1);
            if(provider is ProgressiveProvider && (provider as ProgressiveProvider).seek.param)
            {
               if(stream is ProgressiveStream || stream is FragmentStream)
               {
                  stream["seekParameter"] = (provider as ProgressiveProvider).seek.param;
               }
            }
            stream.checkPolicyFile = true;
            stream.client = isProgressive?
               {
                  "onMetaData":onMetaData,
                  "onPlayStatus":onPlayStatus
               }:
               {
                  "onMetaData":onMetaData,
                  "onPlayStatus":onPlayStatus,
                  "onLastSecond":onLastSecond
               };
            if(stream is FragmentStream)
            {
               stream.addEventListener("FragmentStream.Metadata.NoKeyframes",onProgressiveNoMetadata);
               stream.addEventListener("FragmentStream.Metadata.IntervalExceedsLimit",onFragmentExcessInterval);
               stream.addEventListener("FragmentStream.Metadata.Missing",onProgressiveNoMetadata);
            }
         }
      }
      
      protected function handleStreamTypes2() : void {
         playedStreamName = parseStreamName(resolvedStreamName?resolvedStreamName:streamName);
         stream.play(playedStreamName);
         if(ums)
         {
            ums.status = resolvedStreamName?resolvedStreamName:streamName;
         }
      }
      
      protected function handleStreamTypes3() : void {
         stream.resume();
      }
      
      protected function onCreateStream(... rest) : void {
         echo("onCreateStream");
         var _loc2_:Stream = _modules.stream;
         if(_loc2_)
         {
            _loc2_.addEventListener("providerCreated",onStreamProviderCreated);
            _loc2_.addEventListener("destroy",onStreamDestroy);
         }
      }
      
      protected function onStreamDestroy(param1:Event) : void {
         echo("onStreamDestroy");
         var _loc2_:Stream = param1.target as Stream;
         if(_loc2_)
         {
            _loc2_.removeEventListener("providerCreated",onStreamProviderCreated);
            _loc2_.removeEventListener("destroy",onStreamDestroy);
         }
      }
      
      protected function onStreamProviderCreated(param1:Event) : void {
         echo("onStreamProviderCreated");
         var _loc2_:Stream = param1.target as Stream;
         if(_loc2_)
         {
            if(provider)
            {
               stripStreamProvider();
            }
            provider = _loc2_.streamProvider;
            provider.addEventListener("destroy",onStreamProviderDestroy);
            provider.addEventListener("urlChange",onStreamProviderUrlChange);
            provider.addEventListener("streamChange",onStreamProviderNameChange);
         }
         statistics.common.providerName = provider.name;
         if(playing)
         {
            connectToProvider();
         }
      }
      
      private function connectToProvider() : void {
         if(provider is ProgressiveProvider && (provider as ProgressiveProvider).streamNameService && !resolvedStreamName)
         {
            resolveStreamName((provider as ProgressiveProvider).streamNameService);
         }
         else if(_modules && !_modules.lock)
         {
            echo("connect");
            connect();
         }
         
      }
      
      protected function onStreamProviderNameChange(param1:Event) : void {
         onProviderUpdate();
      }
      
      protected function onStreamProviderUrlChange(param1:Event) : void {
         onProviderUpdate();
      }
      
      protected function onStreamProviderDestroy(param1:Event) : void {
         var _loc2_:Number = progress;
         if(stream)
         {
            destroyStream();
         }
         if(!(this is Channel) && _loc2_ > 0)
         {
            autoSeek = _loc2_;
            echo("autoSeek set to " + autoSeek);
         }
         stripStreamProvider();
      }
      
      protected function stripStreamProvider() : void {
         if(provider)
         {
            provider.removeEventListener("destroy",onStreamProviderDestroy);
            provider.removeEventListener("urlChange",onStreamProviderUrlChange);
            provider.removeEventListener("streamChange",onStreamProviderNameChange);
            provider = null;
         }
      }
      
      protected function onProviderUpdate() : void {
         var _loc1_:* = NaN;
         var _loc4_:* = !(streamName == provider.streamName);
         var _loc2_:Boolean = !streamOwner || streamOwner && !(streamOwner.url == provider.url);
         var _loc3_:Boolean = stream is FragmentStream && netStreamPlayOptionClass;
         echo("nameMismatch : " + _loc4_);
         echo("urlMismatch : " + _loc2_);
         echo("play2Enabled : " + _loc3_);
         if(provider is ProgressiveProvider && (provider as ProgressiveProvider).seek.param)
         {
            if(stream is ProgressiveStream || stream is FragmentStream)
            {
               stream["seekParameter"] = (provider as ProgressiveProvider).seek.param;
            }
         }
         if(!stream || (isProgressive && (_loc4_) && !_loc3_) || (!isProgressive && (_loc2_ || (_loc4_))))
         {
            echo("reconnect/recreate");
            _loc1_ = progress;
            if(stream)
            {
               destroyStream();
            }
            if(_loc1_ > 0)
            {
               autoSeek = _loc1_;
               echo("autoSeek set to " + autoSeek);
            }
            resolveStreamName(provider is ProgressiveProvider?(provider as ProgressiveProvider).streamNameService:null);
         }
         else if(isProgressive && (_loc3_) && (_loc4_))
         {
            echo("play2");
            resolveStreamName(provider is ProgressiveProvider?(provider as ProgressiveProvider).streamNameService:null);
         }
         
      }
      
      public function set stageVideoDisabled(param1:Boolean) : void {
         enableStageVideo = !param1;
         screen.stageVideoDisabled = param1;
      }
      
      public function get embedRedirection() : Boolean {
         return _embedRedirection;
      }
      
      protected function get playedStreamName() : String {
         return _playedStreamName;
      }
      
      protected function set playedStreamName(param1:String) : void {
         _playedStreamName = param1;
         var _loc2_:String = _streamOwner.label != "progressive"?_streamOwner.url:"";
         if(_loc2_.length && !(_loc2_.substr(-1,1) == "/"))
         {
            _loc2_ = _loc2_ + "/";
         }
         _loc2_ = _loc2_ + playedStreamName;
         statistics.common.streamUrl = _loc2_;
      }
      
      public function get snapActive() : Boolean {
         return This.getReference(_modules,"thumbnail") is Thumbnail;
      }
      
      public function get sessionSource() : String {
         return _sessionSource;
      }
      
      protected function onAdblockerReport() : void {
         var _loc1_:* = null;
         echo("onAdblockerReport()");
         if(statistics)
         {
            Logic.instance.removeEventListener("adBlockerReport",onAdblockerReport);
            _loc1_ = Logic.instance.abr.split("%%%");
            if(!_loc1_ || !_loc1_.length)
            {
               _loc1_ = ["unknown","unknown"];
            }
            statistics.failure.report("adblocker",null,
               {
                  "useragent":_loc1_[0],
                  "platform":_loc1_[1]
               });
         }
      }
   }
}
