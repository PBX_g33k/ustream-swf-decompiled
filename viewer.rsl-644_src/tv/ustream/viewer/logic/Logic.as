package tv.ustream.viewer.logic
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.tools.IdValidator;
   import flash.display.LoaderInfo;
   import flash.display.Stage;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.This;
   import tv.ustream.tools.Shell;
   import tv.ustream.statistics.Benchmark;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.display.Sprite;
   import tv.ustream.viewer.logic.media.Channel;
   import tv.ustream.viewer.logic.media.SlideShow;
   import flash.ui.ContextMenuItem;
   import flash.events.Event;
   import tv.ustream.tools.ContextMenuManager;
   import tv.ustream.tools.Shared;
   import tv.ustream.tools.KeyboardManager;
   import flash.events.ContextMenuEvent;
   import flash.events.MouseEvent;
   import tv.ustream.gui2.Alert;
   import flash.utils.getTimer;
   import tv.ustream.tools.GoogleAnalytics;
   import tv.ustream.tools.StringUtils;
   import flash.errors.IllegalOperationError;
   
   public class Logic extends Dispatcher
   {
      
      public function Logic(param1:String = null, param2:String = null) {
         limitedChannelList = [7039229,10043378,9659421,9659430,9659435,9659443,9659449,9659456,9659459,9659461,9659462,9659464,9659466,9659468,9659471,9659473,9659475,9659478,9659480,9659483,9659485,9659488,9659491,9659493,9659497,9659499,9659501,102642];
         if(!hasInstance)
         {
            super("logic");
            _randomId = StringUtils.random(8);
            _instance = this;
            _sessionId = param1;
            this.application = param2;
            _display = new Display();
            _display.name = "Logic Display";
            _display.addEventListener("addedToStage",onDisplayAddedToStage);
            Shell.registerEntryPoint("logic",this);
            if(Shell.jsApiEnabled)
            {
            }
            return;
         }
         throw new IllegalOperationError("Logic already instantiated");
      }
      
      public static const ADBLOCKER_REPORT:String = "adBlockerReport";
      
      public static const CHECK_ADFREE:String = "checkAdfree";
      
      private static const MAXIMUM_AD_OFFSET:uint = 78;
      
      private static var idValidator:IdValidator = new IdValidator();
      
      private static var _loaderInfo:LoaderInfo;
      
      public static const CREATE_CHANNEL:String = "createChannel";
      
      public static const CREATE_RECORDED:String = "createRecorded";
      
      public static const CREATE_SLIDESHOW:String = "createSlideShow";
      
      private static var _instance:Logic;
      
      public static var keepLogoOverlay:Boolean = false;
      
      public static function get instance() : Logic {
         if(_instance)
         {
         }
         return _instance;
      }
      
      public static function get hasInstance() : Boolean {
         return _instance is Logic;
      }
      
      private static var _stage:Stage;
      
      public static var reloading:Boolean = false;
      
      public static function get loaderInfo() : LoaderInfo {
         return _loaderInfo;
      }
      
      public static function set loaderInfo(param1:LoaderInfo) : void {
         if(!_loaderInfo)
         {
            _loaderInfo = param1;
         }
         if(_loaderInfo)
         {
            Debug.echo("[ LOGIC ] set loaderinfo, url : " + loaderInfo.url);
            Debug.echo("[ LOGIC ] set loaderinfo, loaderURL : " + loaderInfo.loaderURL);
            This.secure = loaderInfo.loaderURL?loaderInfo.loaderURL.indexOf("https://") == 0:false;
            Debug.echo("[ LOGIC ] set secure : " + This.secure);
         }
      }
      
      public static function get debug() : Boolean {
         return Debug.enabled;
      }
      
      public static function set debug(param1:Boolean) : void {
         Debug.enabled = param1;
      }
      
      public static function get stage() : Stage {
         return _stage;
      }
      
      public static function set stage(param1:Stage) : void {
         var _loc3_:* = NaN;
         var _loc2_:* = null;
         if(!_stage)
         {
            Debug.echo("[ LOGIC ] media added to stage");
            _stage = param1;
            if(_stage.loaderInfo.parameters.enablejsapi == "true" || _stage.loaderInfo.parameters.enablejsapi == "1")
            {
               Debug.echo("[ LOGIC ] jsApi enabled");
               Shell.jsApiEnabled = true;
            }
            if(_stage.loaderInfo.parameters.ts)
            {
               _loc3_ = _stage.loaderInfo.parameters.ts;
               _loc2_ = new Date();
               Benchmark.logicInitiated(_loc2_.getTime() - _loc3_);
            }
         }
      }
      
      public static function get isNewViewer() : Boolean {
         var _loc1_:String = "";
         if(Logic.loaderInfo && Logic.loaderInfo.loaderURL)
         {
            _loc1_ = Logic.loaderInfo.loaderURL.substr(Math.max(Logic.loaderInfo.loaderURL.indexOf("://") + 3,0)).split("/")[0];
         }
         return Logic.loaderInfo && (This.onSite(Logic.loaderInfo.loaderURL.split(".swf").join("")) || _loc1_ == "ustvstaticcdn1-a.akamaihd.net") && Logic.loaderInfo.loaderURL.indexOf("/viewer.rsl") == -1 && (!(Logic.loaderInfo.loaderURL.indexOf("/viewer.") == -1) || !(Logic.loaderInfo.loaderURL.indexOf("/viewer:") == -1));
      }
      
      private var show:Recorded;
      
      private var _display:Sprite;
      
      private var _channel:Channel;
      
      private var _recorded:Recorded;
      
      private var _slideShow:SlideShow;
      
      private var _volume:Number = 1;
      
      private var _muted:Boolean = false;
      
      private var _playing:Boolean = true;
      
      private var _randomId:String;
      
      public function get randomId() : String {
         return _randomId;
      }
      
      public var application:String;
      
      private var _sessionId:String;
      
      private var _adFree:Boolean = false;
      
      private var wasOnline:Boolean = false;
      
      private var limitedChannelList:Array;
      
      private var _isLimitedChannel:Boolean = false;
      
      protected var _scid:String;
      
      protected var _hash:String;
      
      protected var _overlay:Boolean = false;
      
      protected var _viewMode:String = "embed";
      
      private var mediaPassword:String;
      
      private var mediaParams:Object;
      
      private var _adOffset:uint = 0;
      
      private var _userId:String;
      
      private var _autoAdOffset:Boolean = false;
      
      private var mouseOver:Boolean = false;
      
      private var _secondPreroll:Boolean = false;
      
      private var _channelCreation:int = -1;
      
      private var _recordedCreation:int = -1;
      
      public var seamlessSwitch:Boolean = false;
      
      private var _tempChannel:Channel;
      
      private var _turnOffSpecContextMenuItemsCMItem:ContextMenuItem;
      
      private var _abr:String;
      
      private function onDisplayAddedToStage(param1:Event) : void {
         removeEventListener("addedToStage",onDisplayAddedToStage);
         stage = _display.stage;
         if(Logic.loaderInfo && !(Logic.loaderInfo.loaderURL.indexOf("stageevents") == -1))
         {
            stage.addEventListener("fullScreen",onFullScreen);
            stage.addEventListener("mouseMove",onStageMouseMove);
            stage.addEventListener("mouseLeave",onStageMouseLeave);
         }
         var _loc4_:Object = 
            {
               "idkfa":[73,68,75,70,65],
               "log":[76,79,71]
            };
         ContextMenuManager.getInstance().init(_display.parent);
         ContextMenuManager.getInstance().addEventListener("menuSelect",onContextMenuCreated);
         ContextMenuManager.getInstance().addGroupCode("idkfa",(_loc4_.idkfa as Array).join("_"));
         ContextMenuManager.getInstance().addGroupCode("log",(_loc4_.log as Array).join("_"));
         var _loc2_:ContextMenuItem = ContextMenuManager.getInstance().addItem("Hide spec menu items",onOffSpecials,null,"idkfa");
         var _loc3_:Shared = new Shared();
         if(_loc3_.magicCodes)
         {
            _loc7_ = 0;
            _loc6_ = _loc4_;
            for(_loc5_ in _loc4_)
            {
               if(_loc3_.magicCodes[_loc5_])
               {
                  if((_loc4_[_loc5_] as Array).join("_") == _loc3_.magicCodes[_loc5_])
                  {
                     ContextMenuManager.getInstance().setGroupAvailability(_loc5_,true);
                  }
                  else
                  {
                     delete _loc3_.magicCodes[_loc5_];
                  }
               }
            }
         }
         else
         {
            _loc3_.magicCodes = {};
         }
         KeyboardManager.getInstance().init(stage);
         KeyboardManager.getInstance().addCombo(_loc4_.idkfa,onMagicCode);
         KeyboardManager.getInstance().addCombo(_loc4_.log,onMagicCode);
      }
      
      private function onContextMenuCreated(param1:ContextMenuEvent) : void {
         dispatchEvent(param1.clone());
      }
      
      private function onOffSpecials() : void {
         var _loc1_:Shared = new Shared();
         _loc1_.magicCodes["idkfa"] = "";
         ContextMenuManager.getInstance().toggleGroup("idkfa");
      }
      
      private function onMagicCode(param1:String) : void {
         var _loc2_:* = null;
         var _loc3_:String = ContextMenuManager.getInstance().getGroupByCode(param1);
         if(_loc3_)
         {
            _loc2_ = new Shared();
            if(!_loc2_.magicCodes)
            {
               _loc2_.magicCodes = {};
            }
            if(!ContextMenuManager.getInstance().groupIsAvailable(_loc3_))
            {
               _loc2_.magicCodes[_loc3_] = param1;
               ContextMenuManager.getInstance().setGroupAvailability(_loc3_,true);
            }
            else
            {
               delete _loc2_.magicCodes[_loc3_];
               ContextMenuManager.getInstance().setGroupAvailability(_loc3_,false);
            }
         }
      }
      
      private function onStageMouseLeave(param1:Event) : void {
         mouseOver = false;
         dispatch("mouseLeave",false,false);
      }
      
      private function onStageMouseMove(param1:MouseEvent) : void {
         if(!mouseOver)
         {
            mouseOver = true;
            dispatch("mouseOver",false,true);
         }
      }
      
      private function onFullScreen(param1:Event) : void {
         dispatch("fullScreen",false,stage.displayState);
      }
      
      override public function destroy(... rest) : * {
         if(_channel)
         {
            _channel = _channel.destroy();
         }
         if(_recorded)
         {
            _recorded = _recorded.destroy();
         }
         if(_slideShow)
         {
            _slideShow = _slideShow.destroy();
         }
         ContextMenuManager.getInstance().removeItem(_turnOffSpecContextMenuItemsCMItem);
         _isLimitedChannel = false;
         Alert.clear();
         KeyboardManager.destroy();
         super.destroy();
         _active = true;
         return null;
      }
      
      public function createChannel(param1:String, param2:Boolean = true, ... rest) : Channel {
         Debug.debugPrefix = "channel." + param1 + ".";
         parseArgs(rest);
         if(_userId)
         {
            mediaParams.userId = _userId;
         }
         else if(mediaParams.userId)
         {
            Debug.echo("%%% mediaParams.userId : " + mediaParams.userId + " " + typeof mediaParams.userId);
            _userId = mediaParams.userId;
         }
         
         if(!idValidator.parseId(param1))
         {
            return null;
         }
         if(_channel)
         {
            if(!seamlessSwitch || !_channel.online)
            {
               _channel = _channel.destroy();
            }
            else
            {
               if(_tempChannel)
               {
                  _tempChannel.destroy();
               }
               _tempChannel = _channel;
            }
         }
         if(_recorded)
         {
            _recorded = _recorded.destroy();
         }
         if(_slideShow)
         {
            _slideShow = _slideShow.destroy();
         }
         wasOnline = false;
         Alert.clear();
         var _loc4_:String = This.referrer || This.pageUrl;
         if(!(limitedChannelList.indexOf(param1) == -1) && This.onSite(_loc4_) <= 1)
         {
            _isLimitedChannel = true;
         }
         Debug.echo("isLimitedChannel: " + [_isLimitedChannel,This.onSite(_loc4_),_loc4_]);
         _channel = new Channel(param1,param2,mediaPassword,sessionId,application,mediaParams);
         if(!_tempChannel)
         {
            _display.addChild(_channel.media);
         }
         else if(!_playing)
         {
            _channel.pause();
         }
         
         _channel.addEventListener("destroy",onChannelDestroy);
         _channel.addEventListener("online",onChannelOnline);
         _channel.addEventListener("offline",onChannelOffline);
         _channel.addEventListener("video",onChannelVideoChange);
         _channel.addEventListener("offairContentInfo",onOffairContentInfo);
         _channel.addEventListener("play",onMediaPlay);
         _channel.addEventListener("pause",onMediaPause);
         _channel.addEventListener("getStreamSize",onChannelGetSize);
         _channel.addEventListener("adsVideoAdStarted",onChannelVideoAdStarted);
         _channel.addEventListener("Logo.Click",onCustomLogoClick);
         _channel.volume = _volume;
         _channel.muted = _muted;
         dispatch("createChannel");
         _channel.addEventListener("createModules",onCreateModules);
         _channelCreation = getTimer();
         return _channel;
      }
      
      private function onCustomLogoClick(param1:Event) : void {
         if(media)
         {
            gaTrack("Player","Custom watermark click",_channel?_channel.id:media.channelId);
         }
      }
      
      private function onChannelVideoAdStarted(param1:Event) : void {
         if(param1 && param1.target is Channel)
         {
            (param1.target as Channel).removeEventListener("adsVideoAdStarted",onChannelVideoAdStarted);
         }
         destroyTempChannel();
      }
      
      private function parseArgs(param1:Array) : void {
         mediaPassword = null;
         mediaParams = {};
         if(param1.length)
         {
            if(param1[0] is String)
            {
               mediaPassword = param1[0];
            }
            if(param1[1] is Object)
            {
               mediaParams = param1[1];
            }
         }
         if(This.onSite(This.pageUrl) & 2)
         {
            if(!mediaParams)
            {
               mediaParams = {};
            }
            mediaParams.enableStageVideo = mediaParams.cutter?false:true;
         }
      }
      
      private function onChannelDestroy(param1:Event) : void {
         var _loc2_:Channel = param1.target as Channel;
         if(_loc2_)
         {
            _loc2_.removeEventListener("play",onMediaPlay);
            _loc2_.removeEventListener("pause",onMediaPause);
            _loc2_.removeEventListener("destroy",onChannelDestroy);
            _loc2_.removeEventListener("online",onChannelOnline);
            _loc2_.removeEventListener("offline",onChannelOffline);
            _loc2_.removeEventListener("video",onChannelVideoChange);
            _loc2_.removeEventListener("offairContentInfo",onOffairContentInfo);
            _loc2_.removeEventListener("getStreamSize",onChannelGetSize);
            _loc2_.removeEventListener("createModules",onCreateModules);
            _loc2_.removeEventListener("adsVideoAdStarted",onChannelVideoAdStarted);
            _loc2_.removeEventListener("Logo.Click",onCustomLogoClick);
            if(_loc2_ != _tempChannel)
            {
               if(_slideShow)
               {
                  _slideShow = _slideShow.destroy();
               }
               _channel = null;
               _isLimitedChannel = false;
               _channelCreation = -1;
            }
            else
            {
               _tempChannel = null;
            }
         }
      }
      
      private function onMediaPause(param1:Event) : void {
         dispatch("pause");
      }
      
      private function onMediaPlay(param1:Event) : void {
         dispatch("play");
      }
      
      private function onChannelOnline(... rest) : void {
         wasOnline = true;
         if(_recorded)
         {
            _recorded = _recorded.destroy();
         }
         if(_slideShow)
         {
            _slideShow = _slideShow.destroy();
         }
         if(!_display.contains(_channel.media))
         {
            _display.addChild(_channel.media);
         }
         if(!_channel.media.visible)
         {
            _channel.media.visible = true;
         }
      }
      
      private function onChannelOffline(... rest) : void {
         if(!_channel.modules.layers)
         {
            if(!_channel.modules.lock)
            {
               createChannelSlideShow(wasOnline);
               _channel.media.visible = false;
            }
            else
            {
               _channel.modules.addEventListener("lock",onChannelLock);
               _channel.modules.addEventListener("destroy",onChannelModulesDestroy);
            }
         }
         destroyTempChannel();
         _channelCreation = -1;
      }
      
      private function onOffairContentInfo(... rest) : void {
         if(!_channel.modules.lock)
         {
            Debug.echo("onOffairContentInfo()");
            createChannelSlideShow(wasOnline);
         }
      }
      
      private function onChannelLock(param1:Event) : void {
         if(!_channel.modules.lock)
         {
            createChannelSlideShow(wasOnline);
            _channel.media.visible = false;
            onChannelModulesDestroy();
         }
      }
      
      private function onChannelModulesDestroy(param1:Event = null) : void {
         _channel.modules.removeEventListener("lock",onChannelLock);
         _channel.modules.removeEventListener("destroy",onChannelModulesDestroy);
      }
      
      private function onChannelVideoChange(param1:Event) : void {
         if(param1.target is Channel && !(param1.target as Channel).video)
         {
            destroyTempChannel();
         }
         if(_channel && _channel.online && _channel.video)
         {
            if(_slideShow)
            {
               _slideShow = _slideShow.destroy();
            }
            _channel.media.visible = true;
         }
         else if(_channel && _channel.online && !_channel.video && !_slideShow && !_channel.modules.layers)
         {
            if(channel && channel.online && !channel.video && !channel.prerollDone)
            {
               channel.addEventListener("adsPrerollDone",onChannelPrerollDone);
            }
            else
            {
               createChannelSlideShow(true);
            }
         }
         
      }
      
      private function createChannelSlideShow(param1:Boolean = false) : void {
         var _loc4_:* = null;
         var _loc2_:* = NaN;
         var _loc3_:* = null;
         if(_channel.offairContent && !_slideShow && (_channel.offairContent.videos && !param1 || _channel.offairContent.pictures))
         {
            echo("createChannelSlideShow");
            Debug.explore(_channel.offairContent);
            _loc4_ = _channel.offairContent.pictures || [];
            if(_channel.offairContent.videos && !param1)
            {
               _loc2_ = _channel.offairContent.videos.length - 1;
               while(_loc2_ >= 0)
               {
                  _loc4_.unshift(
                     {
                        "type":"recorded",
                        "id":_channel.offairContent.videos[_loc2_],
                        "count":1
                     });
                  _loc2_--;
               }
               if(_channel.offairContent.videos.length == 1)
               {
                  dispatch("onlyOneRecordedSlide");
               }
            }
            createSlideShow(_loc4_,param1);
            _slideShow.start();
            _slideShow.update();
         }
         if(!_channel.online)
         {
            _loc3_ = "";
            if(!_channel.offairContent || (!_channel.offairContent.pictures && !_channel.offairContent.videos))
            {
               _loc3_ = "none";
            }
            else
            {
               if(_channel.offairContent.pictures && _channel.offairContent.pictures.length)
               {
                  _loc3_ = "image";
               }
               if(_channel.offairContent.videos && _channel.offairContent.videos.length)
               {
                  if(_channel.offairContent.videos.length == 1)
                  {
                     _loc3_ = "video";
                  }
                  else if(_channel.offairContent.videos.length > 1)
                  {
                     _loc3_ = "videos";
                  }
                  
               }
            }
            gaTrack("Player","Offair",_loc3_);
         }
      }
      
      public function createRecorded(param1:String, param2:Boolean = true, ... rest) : Recorded {
         parseArgs(rest);
         if(_userId)
         {
            mediaParams.userId = _userId;
         }
         else if(mediaParams.userId)
         {
            Debug.echo("%%% mediaParams.userId : " + mediaParams.userId + " " + typeof mediaParams.userId);
            _userId = mediaParams.userId;
         }
         
         if(!_slideShow)
         {
            Debug.debugPrefix = "recorded." + param1 + ".";
         }
         if(!(mediaParams && mediaParams.skipIdValidator) && !idValidator.parseId(param1))
         {
            return null;
         }
         if(_channel)
         {
            _channel.media.visible = false;
         }
         if(_recorded)
         {
            _recorded = _recorded.destroy();
         }
         Alert.clear();
         _recorded = new Recorded(param1,param2 && (_playing),mediaPassword,sessionId,application,mediaParams);
         _recorded.isSlideShow = slideShow?true:false;
         if(_recorded.isSlideShow)
         {
            _recorded.addEventListener("play",onRecordedPlay);
         }
         _display.addChild(_recorded.media);
         _recorded.addEventListener("finish",onRecordedFinish);
         _recorded.addEventListener("destroy",onDestroyRecorded);
         _recorded.addEventListener("play",onMediaPlay);
         _recorded.addEventListener("pause",onMediaPause);
         _recorded.addEventListener("getStreamSize",onRecordedGetSize);
         _recorded.addEventListener("Logo.Click",onCustomLogoClick);
         _recorded.volume = _volume;
         _recorded.muted = _muted;
         dispatch("createRecorded");
         _recordedCreation = getTimer();
         return _recorded;
      }
      
      private function onRecordedPlay(param1:Event) : void {
         if(channel)
         {
            channel.autoPlay = true;
         }
      }
      
      private function onPlayRecorded(... rest) : void {
         if(_slideShow && !_slideShow.playing)
         {
            if(_slideShow.items.length == 1 && _slideShow.items[0].type == "recorded")
            {
               _recorded.removeEventListener("play",onPlayRecorded);
               _recorded.addEventListener("finish",onRecordedFinish);
               _recorded.seek(0);
            }
            else
            {
               _slideShow.next();
            }
         }
      }
      
      private function onRecordedFinish(... rest) : void {
         _recorded.removeEventListener("finish",onRecordedFinish);
         _recorded.addEventListener("play",onPlayRecorded);
         if(_slideShow)
         {
            _slideShow.next();
         }
      }
      
      private function onDestroyRecorded(... rest) : void {
         if(_channel && !_slideShow)
         {
            _channel.media.visible = true;
         }
         if(_display.contains(_recorded.media))
         {
            _display.removeChild(_recorded.media);
         }
         _recorded.removeEventListener("play",onMediaPlay);
         _recorded.removeEventListener("pause",onMediaPause);
         _recorded.removeEventListener("finish",onRecordedFinish);
         _recorded.removeEventListener("destroy",onDestroyRecorded);
         _recorded.removeEventListener("getStreamSize",onRecordedGetSize);
         _recorded.removeEventListener("Logo.Click",onCustomLogoClick);
         _recorded = null;
         _recordedCreation = -1;
      }
      
      private function createSlideShow(param1:Array = null, param2:Boolean = false) : SlideShow {
         if(_slideShow)
         {
            _slideShow = _slideShow.destroy();
         }
         _slideShow = new SlideShow(param1,_channel?_channel:null);
         _slideShow.imagesOnly = param2;
         _display.addChildAt(_slideShow.media,0);
         _slideShow.addEventListener("createRecorded",onSlideShowCreateRecorded);
         _slideShow.addEventListener("destroyRecorded",onSlideShowDestroyRecorded);
         _slideShow.addEventListener("destroy",onSlideShowDestroy);
         dispatch("createSlideShow");
         return _slideShow;
      }
      
      private function onChannelPrerollDone(param1:Event) : void {
         channel.removeEventListener("adsPrerollDone",onChannelPrerollDone);
         createChannelSlideShow(true);
      }
      
      private function onSlideShowCreateRecorded(... rest) : void {
         createRecorded(_slideShow.item.id,channel?channel.autoPlay:false,mediaPassword,mediaParams?mediaParams:null);
         dispatch("createSlideRecorded");
      }
      
      private function onSlideShowDestroyRecorded(... rest) : void {
         if(_recorded)
         {
            _recorded.destroy();
         }
         dispatch("destroySlideRecorded");
      }
      
      private function onSlideShowDestroy(... rest) : void {
         if(_channel)
         {
            _channel.media.visible = true;
         }
         if(_display.contains(_slideShow.media))
         {
            _display.removeChild(_slideShow.media);
         }
         _slideShow = null;
      }
      
      private function onCreateModules(... rest) : void {
         _channel.modules.addEventListener("createLayers",onCreateLayers);
         _channel.modules.addEventListener("destroyLayers",onDestroyLayers);
      }
      
      private function onCreateLayers(... rest) : void {
         if(_channel)
         {
            if(_slideShow)
            {
               _slideShow = _slideShow.destroy();
            }
            _channel.media.visible = true;
         }
      }
      
      private function onDestroyLayers(... rest) : void {
         if(_channel)
         {
            if(_channel.status > 0 && !_channel.video)
            {
               createChannelSlideShow(true);
            }
            else if(_channel.status == 0)
            {
               createChannelSlideShow();
               _channel.media.visible = false;
            }
            
         }
      }
      
      private function gaTrack(param1:String, param2:String, param3:String = null, param4:Number = 0) : void {
         if(!media.disabledGA)
         {
            GoogleAnalytics.track(param1,param2,param3,param4);
         }
      }
      
      private function onRecordedGetSize(param1:Event) : void {
         var _loc2_:* = 0;
         if(_recordedCreation != -1)
         {
            _loc2_ = getTimer() - _recordedCreation;
            gaTrack("Player","Benchmark","RecordedLoad",_loc2_);
            echo("GATrack : Player, Benchmark, RecordedLoad, " + _loc2_);
            _recordedCreation = -1;
         }
      }
      
      private function onChannelGetSize(param1:Event) : void {
         var _loc2_:* = 0;
         destroyTempChannel();
         if(_channelCreation != -1)
         {
            _loc2_ = getTimer() - _channelCreation;
            gaTrack("Player","Benchmark","ChannelLoad",_loc2_);
            echo("GATrack : Player, Benchmark, ChannelLoad, " + _loc2_);
            _channelCreation = -1;
         }
      }
      
      private function destroyTempChannel() : void {
         if(_tempChannel)
         {
            if(_display.contains(_tempChannel.media))
            {
               _display.removeChild(_tempChannel.media);
            }
            _tempChannel = _tempChannel.destroy();
            if(_channel)
            {
               _display.addChild(_channel.media);
               if(_channel.online)
               {
                  _channel.media.visible = true;
               }
            }
         }
      }
      
      public function get channel() : Channel {
         return _channel;
      }
      
      public function get display() : Sprite {
         return _display;
      }
      
      public function get recorded() : Recorded {
         return _recorded;
      }
      
      public function get slideShow() : SlideShow {
         return _slideShow;
      }
      
      public function get volume() : Number {
         return _volume;
      }
      
      public function set volume(param1:Number) : void {
         if(param1 < 0)
         {
            param1 = 0.0;
         }
         if(param1 > 1)
         {
            if(param1 > 100)
            {
            }
            param1 = param1 / 100;
         }
         _volume = param1;
         if(_channel)
         {
            _channel.volume = param1;
         }
         if(_tempChannel)
         {
            _tempChannel.volume = param1;
         }
         if(_recorded)
         {
            _recorded.volume = param1;
         }
      }
      
      public function get playing() : Boolean {
         return media?media["playing"]:false;
      }
      
      public function set playing(param1:Boolean) : void {
         _playing = param1;
         if(_channel)
         {
            _channel.playing = param1;
         }
         if(_tempChannel)
         {
            _tempChannel.playing = param1;
         }
         if(_recorded)
         {
            _recorded.playing = param1;
         }
      }
      
      public function get muted() : Boolean {
         return _muted;
      }
      
      public function set muted(param1:Boolean) : void {
         _muted = param1;
         if(_channel)
         {
            _channel.muted = param1;
         }
         if(_tempChannel)
         {
            _tempChannel.muted = param1;
         }
         if(_recorded)
         {
            _recorded.muted = param1;
         }
      }
      
      public function get media() : Recorded {
         return _recorded || _channel || null;
      }
      
      public function get sessionId() : String {
         return _sessionId;
      }
      
      public function set sessionId(param1:String) : void {
         echo("session set to " + param1);
         _sessionId = param1;
         if(channel && (channel.modules.ppv || mediaPassword))
         {
            createChannel(channel.id,channel.autoPlay,mediaPassword,mediaParams?mediaParams:null);
         }
         else if(recorded && recorded.modules.ppv)
         {
            createRecorded(recorded.id,recorded.autoPlay,mediaPassword,mediaParams?mediaParams:null);
         }
         
         if(_adFree)
         {
            dispatch("checkAdfree");
         }
      }
      
      public function get adFree() : Boolean {
         return _adFree;
      }
      
      public function set adFree(param1:Boolean) : void {
         if(param1 == true)
         {
            _adFree = true;
            if(_sessionId)
            {
               dispatch("checkAdfree");
            }
         }
      }
      
      public function get autoAdOffset() : Boolean {
         return _autoAdOffset;
      }
      
      public function get adOffset() : uint {
         return _adOffset;
      }
      
      public function set adOffset(param1:uint) : void {
         _adOffset = param1;
         if(_adOffset == 501)
         {
            _autoAdOffset = true;
         }
         if(_adOffset == 500)
         {
            _autoAdOffset = false;
            _adOffset = 0;
         }
         if(_adOffset > 78)
         {
            _adOffset = 78;
         }
      }
      
      public function set shareUse(param1:String) : void {
         if(param1)
         {
            Debug.echo("set shareUse: " + param1);
            gaTrack("Player","Share",param1);
         }
      }
      
      public function get userId() : String {
         return _userId;
      }
      
      public function set userId(param1:String) : void {
         if(_userId != param1)
         {
            Debug.echo("[ LOGIC ] userId set to: " + param1);
            _userId = param1;
            if(mediaParams)
            {
               mediaParams.userId = param1;
            }
            if(channel)
            {
               createChannel(channel.id,channel.autoPlay,mediaPassword,mediaParams?mediaParams:{"userId":_userId});
            }
            else if(recorded)
            {
               createRecorded(recorded.id,recorded.autoPlay,mediaPassword,mediaParams?mediaParams:{"userId":_userId});
            }
            
            if(_adFree)
            {
               dispatch("checkAdfree");
            }
         }
         else
         {
            Debug.echo("[ LOGIC ] userId is: " + param1);
         }
      }
      
      public function get isLimitedChannel() : Boolean {
         return _isLimitedChannel;
      }
      
      public function get scid() : String {
         return _scid;
      }
      
      public function set scid(param1:String) : void {
         _scid = param1;
         echo("scid: " + _scid);
      }
      
      public function get hash() : String {
         return _hash;
      }
      
      public function set hash(param1:String) : void {
         _hash = param1;
      }
      
      public function playerMode(param1:String) : void {
      }
      
      public function get overlay() : Boolean {
         return _overlay;
      }
      
      public function set overlay(param1:Boolean) : void {
         echo("overlay: " + param1 + " - idle: " + (media?media.adIdle:"null"));
         _overlay = param1;
         if(!param1 && media && media.adIdle == 1)
         {
            media.continueAd();
         }
      }
      
      public function get viewMode() : String {
         return _viewMode;
      }
      
      public function set viewMode(param1:String) : void {
         echo("set viewMode: " + [_viewMode,param1]);
         if(!(_viewMode == param1) && !((["explore","channel","discovery"] as Array).indexOf(param1) == -1))
         {
            _viewMode = param1;
            if(media)
            {
               media.viewMode = param1;
            }
         }
      }
      
      public function get secondPreroll() : Boolean {
         return _secondPreroll;
      }
      
      public function set secondPreroll(param1:Boolean) : void {
         _secondPreroll = param1;
      }
      
      public function get rsid() : String {
         var _loc2_:String = This.getReference(media,"modules.ums.randomId") as String;
         var _loc1_:String = "";
         if(_loc2_)
         {
            _loc1_ = _randomId + ":" + _loc2_;
         }
         return _loc1_;
      }
      
      public function get abr() : String {
         return _abr;
      }
      
      public function set abr(param1:String) : void {
         if(This.onSite(This.pageUrl) > 0)
         {
            _abr = param1;
            echo("abr: " + param1);
            if(media)
            {
               dispatch("adBlockerReport");
            }
         }
      }
   }
}
