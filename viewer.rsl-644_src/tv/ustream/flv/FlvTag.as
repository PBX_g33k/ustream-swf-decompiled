package tv.ustream.flv
{
   import flash.utils.ByteArray;
   
   public class FlvTag extends ByteArray
   {
      
      public function FlvTag() {
         super();
         objectEncoding = 0;
      }
      
      public static const TAG_HEADER_LENGTH:uint = 11;
      
      public static const TAG_TYPE_META:uint = 18;
      
      public static const TAG_TYPE_AUDIO:uint = 8;
      
      public static const TAG_TYPE_VIDEO:uint = 9;
      
      public function get type() : int {
         var _loc2_:Number = position;
         position = 0;
         var _loc1_:* = -1;
         if(length > 0)
         {
            _loc1_ = readUnsignedByte();
         }
         position = _loc2_;
         return _loc1_;
      }
      
      public function get timeStamp() : uint {
         var _loc2_:Number = position;
         position = 4;
         var _loc1_:uint = readUnsignedByte() << 16 | readUnsignedByte() << 8 | readUnsignedByte() << 0 | readUnsignedByte() << 24;
         position = _loc2_;
         return _loc1_;
      }
      
      public function get isKeyFrame() : Boolean {
         var _loc2_:* = NaN;
         var _loc1_:* = false;
         var _loc3_:* = 0;
         if(type == 9)
         {
            _loc2_ = position;
            position = 11;
            _loc1_ = false;
            _loc3_ = readUnsignedByte();
            if(_loc3_ >> 4 == 1)
            {
               _loc1_ = true;
            }
            position = _loc2_;
            return _loc1_;
         }
         return false;
      }
   }
}
