package tv.ustream.flv
{
   import flash.utils.ByteArray;
   
   public class FlvHeader extends ByteArray
   {
      
      public function FlvHeader() {
         super();
         writeByte(70);
         writeByte(76);
         writeByte(86);
         writeByte(1);
         writeByte(5);
         writeByte(0);
         writeByte(0);
         writeByte(0);
         writeByte(9);
         writeByte(0);
         writeByte(0);
         writeByte(0);
         writeByte(0);
      }
      
      public static const LENGTH:uint = 13;
   }
}
