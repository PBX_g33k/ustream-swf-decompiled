package tv.ustream.flv.flvParser
{
   import flash.utils.ByteArray;
   
   public class PacketHeader extends ByteArray
   {
      
      public function PacketHeader() {
         super();
      }
      
      public function get packetSize() : int {
         if(length < 4)
         {
            return -1;
         }
         position = 1;
         return readUnsignedByte() << 16 | readUnsignedByte() << 8 | readUnsignedByte();
      }
      
      public function get type() : int {
         position = 0;
         if(length > 0)
         {
            return readUnsignedByte();
         }
         return -1;
      }
   }
}
