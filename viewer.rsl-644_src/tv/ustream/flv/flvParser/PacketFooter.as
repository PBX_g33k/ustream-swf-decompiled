package tv.ustream.flv.flvParser
{
   import flash.utils.ByteArray;
   
   public class PacketFooter extends ByteArray
   {
      
      public function PacketFooter() {
         super();
      }
      
      public function get totalSize() : int {
         if(length < 4)
         {
            return -1;
         }
         position = 0;
         return readUnsignedByte() << 24 | readUnsignedByte() << 16 | readUnsignedByte() << 8 | readUnsignedByte();
      }
   }
}
