package tv.ustream.flv
{
   import flash.events.EventDispatcher;
   import flash.net.URLStream;
   import flash.utils.ByteArray;
   import tv.ustream.flv.flvParser.PacketHeader;
   import tv.ustream.flv.flvParser.PacketFooter;
   import tv.ustream.net.ThrottledURLStream;
   import flash.system.Capabilities;
   import flash.net.URLRequest;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.events.HTTPStatusEvent;
   import flash.events.ProgressEvent;
   import flash.utils.getTimer;
   import flash.events.Event;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.Debug;
   import tv.ustream.tools.StringUtils;
   
   public class FlvParser extends EventDispatcher
   {
      
      public function FlvParser(param1:String = null, param2:Boolean = true) {
         super();
         _randomUrl = StringUtils.random(3);
         this.metaDataOnly = param2;
         cache = new ByteArray();
         if(param1)
         {
            echo("parsing " + param1);
            load(param1);
         }
      }
      
      public static const META_TAG:String = "meta";
      
      public static const VIDEO_TAG:String = "video";
      
      public static const AUDIO_TAG:String = "audio";
      
      public static const EXCEEDED_METADATA_PARSE_LIMIT:String = "exceededMetadataParseLimit";
      
      public static const META_DATA:String = "metadata";
      
      public static const MALFORMED_DATA:String = "malformedData";
      
      private const FLV_HEADER_SIZE:uint = 13;
      
      private const PACKET_HEADER_SIZE:uint = 11;
      
      private const PACKET_FOOTER_SIZE:uint = 4;
      
      private const TYPE_METADATA:uint = 18;
      
      private const TYPE_AUDIO:uint = 8;
      
      private const TYPE_VIDEO:uint = 9;
      
      private var _url:String;
      
      private var stream:URLStream;
      
      private var cache:ByteArray;
      
      private var header:ByteArray;
      
      private var packetHeader:PacketHeader;
      
      private var packetBody:ByteArray;
      
      private var packetFooter:PacketFooter;
      
      private var packetCount:uint = 0;
      
      private var _metaData:Object;
      
      public var maxPacketsToParseForMeta:uint = 8;
      
      public var metaDataOnly:Boolean = true;
      
      private var _randomUrl:String;
      
      public function clearCache() : void {
         header = null;
         packetHeader = null;
         packetBody = null;
         packetFooter = null;
         cache = new ByteArray();
      }
      
      public function load(param1:String) : void {
         echo("load " + param1);
         if(!param1 || param1 && !param1.length)
         {
            throw new ArgumentError("invalid url");
         }
         else
         {
            if(!stream)
            {
               stream = new ThrottledURLStream();
               if(metaDataOnly && Capabilities.playerType == "ActiveX")
               {
                  ThrottledURLStream(stream).maxBytesPerProgress = 1024;
               }
               stream.addEventListener("httpStatus",onStreamStatus);
               stream.addEventListener("ioError",onStreamIoError);
               stream.addEventListener("securityError",onStreamSecurityError);
               stream.addEventListener("progress",onStreamProgress);
            }
            _url = param1;
            packetHeader = null;
            packetBody = null;
            packetFooter = null;
            _metaData = null;
            stream.load(new URLRequest(_url));
            return;
         }
      }
      
      private function onStreamIoError(param1:IOErrorEvent) : void {
         echo("onStreamIoError : " + param1);
      }
      
      private function onStreamSecurityError(param1:SecurityErrorEvent) : void {
         echo("onStreamSecurityError : " + param1);
      }
      
      private function onStreamStatus(param1:HTTPStatusEvent) : void {
         echo("onStreamStatus : " + param1.status);
      }
      
      private function onStreamProgress(param1:ProgressEvent) : void {
         var _loc2_:ByteArray = new ByteArray();
         if(stream.bytesAvailable)
         {
            stream.readBytes(_loc2_,0,stream.bytesAvailable);
            feed(_loc2_);
         }
      }
      
      private function closeStream() : void {
         var _loc1_:* = null;
         echo("close URLStream, is opened");
         if(stream)
         {
            stream.load(new URLRequest(""));
            if(stream.bytesAvailable)
            {
               _loc1_ = new ByteArray();
               stream.readBytes(_loc1_,0,stream.bytesAvailable);
            }
            stream.removeEventListener("httpStatus",onStreamStatus);
            stream.removeEventListener("ioError",onStreamIoError);
            stream.removeEventListener("securityError",onStreamSecurityError);
            stream.removeEventListener("progress",onStreamProgress);
            stream.close();
            stream = null;
         }
      }
      
      public function feed(param1:ByteArray) : void {
         var _loc2_:* = NaN;
         param1.position = 0;
         if(param1.bytesAvailable)
         {
            _loc2_ = cache.position;
            cache.position = cache.length;
            cache.writeBytes(param1);
            cache.position = _loc2_;
         }
         parseCache();
      }
      
      private function parseCache() : void {
         var _loc5_:* = 0;
         var _loc1_:* = null;
         var _loc2_:* = null;
         var _loc4_:* = true;
         var _loc3_:uint = getTimer();
         while(cache.length && _loc4_)
         {
            _loc4_ = false;
            if(!header && cache.bytesAvailable >= 13)
            {
               _loc5_ = cache.position;
               header = new ByteArray();
               cache.readBytes(header,0,13);
               header.position = 0;
               _loc1_ = header.readUTFBytes(3);
               if(_loc1_ != "FLV")
               {
                  header.position = 0;
                  header = new FlvHeader();
                  echo("no FLV header in feed, corrected",2);
                  cache.position = _loc5_;
               }
            }
            if(header && !packetHeader && cache.bytesAvailable >= 11)
            {
               packetHeader = new PacketHeader();
               cache.readBytes(packetHeader,0,11);
               if(!(packetHeader.type == 8) && !(packetHeader.type == 9) && !(packetHeader.type == 18))
               {
                  echo("tag type mismatch : " + packetHeader.type,2);
                  dispatchEvent(new Event("malformedData"));
                  closeStream();
                  clearCache();
               }
            }
            if(packetHeader && !packetBody && cache.bytesAvailable >= packetHeader.packetSize)
            {
               packetBody = new ByteArray();
               cache.readBytes(packetBody,0,packetHeader.packetSize);
            }
            if(packetHeader && packetBody && !packetFooter && cache.bytesAvailable >= 4)
            {
               packetFooter = new PacketFooter();
               cache.readBytes(packetFooter,0,4);
               if(packetHeader.length + packetBody.length == packetFooter.totalSize)
               {
                  parsePacket();
                  if(metaDataOnly && packetCount > maxPacketsToParseForMeta && !_metaData)
                  {
                     dispatchEvent(new Event("exceededMetadataParseLimit"));
                     closeStream();
                     clearCache();
                     return;
                  }
                  packetCount = packetCount + 1;
                  _loc4_ = true;
               }
               else
               {
                  echo("packetHeader.length : " + packetHeader.length);
                  echo("packetBody.length : " + packetBody.length);
                  echo("packetFooter.totalSize : " + packetFooter.totalSize);
                  echo("malformed data in flv, closing stream");
                  dispatchEvent(new Event("malformedData"));
                  closeStream();
                  clearCache();
               }
               _loc2_ = new ByteArray();
               cache.readBytes(_loc2_,0,cache.bytesAvailable);
               cache = _loc2_;
               cache.position = 0;
               packetHeader = null;
               packetBody = null;
               packetFooter = null;
            }
         }
      }
      
      private function parsePacket() : void {
         var _loc2_:* = null;
         var _loc1_:* = null;
         var _loc3_:FlvTag = new FlvTag();
         _loc3_.writeBytes(packetHeader,0,packetHeader.length);
         _loc3_.writeBytes(packetBody,0,packetBody.length);
         _loc3_.writeBytes(packetFooter,0,packetFooter.length);
      }
      
      private function echo(param1:String, param2:int = -1) : void {
         Debug.echo("[ FLVPARSER ][ " + _randomUrl + " ] " + param1,param2);
      }
      
      public function get metaData() : Object {
         return _metaData;
      }
      
      public function get url() : String {
         return _url;
      }
      
      public function get cacheSize() : uint {
         return cache?cache.length:0;
      }
   }
}
