package tv.ustream.tween
{
   import flash.events.EventDispatcher;
   import flash.display.Sprite;
   import flash.utils.Dictionary;
   import flash.events.Event;
   
   public class Tween extends EventDispatcher
   {
      
      public function Tween() {
         equation = equations[0];
         _value = begin;
         super();
         if(!sprite)
         {
            sprite = new Sprite();
         }
      }
      
      private static var sprite:Sprite;
      
      public static var equations:Array = ["easeNone","easeInQuad","easeOutQuad","easeInOutQuad","easeOutInQuad","easeInCubic","easeOutCubic","easeInOutCubic","easeOutInCubic","easeInQuart","easeOutQuart","easeInOutQuart","easeOutInQuart","easeInQuint","easeOutQuint","easeInOutQuint","easeOutInQuint","easeInSine","easeOutSine","easeInOutSine","easeOutInSine","easeInCirc","easeOutCirc","easeInOutCirc","easeOutInCirc","easeInExpo","easeOutExpo","easeInOutExpo","easeOutInExpo","easeInElastic","easeOutElastic","easeInOutElastic","easeOutInElastic","easeInBack","easeOutBack","easeInOutBack","easeOutInBack","easeInBounce","easeOutBounce","easeInOutBounce","easeOutInBounce"];
      
      private static var targets:Dictionary = new Dictionary(true);
      
      private static var frame:Sprite = new Sprite();
      
      public static function to(param1:Object, param2:Object, param3:Number = 30, param4:String = "easeNone") : void {
         if(!targets[param1])
         {
            targets[param1] = {};
         }
         if(equations.indexOf(param4) == -1)
         {
            param4 = "easeNone";
         }
         var _loc7_:* = 0;
         var _loc6_:* = param2;
         for(_loc5_ in param2)
         {
            if(param1.hasOwnProperty(_loc5_))
            {
               targets[param1][_loc5_] = 
                  {
                     "time":0,
                     "begin":param1[_loc5_],
                     "change":param2[_loc5_] - param1[_loc5_],
                     "duration":param3,
                     "ease":param4
                  };
            }
            else
            {
               delete param2[_loc5_];
            }
         }
         if(!frame.hasEventListener("enterFrame"))
         {
            frame.addEventListener("enterFrame",onStaticEnterFrame);
         }
      }
      
      private static function onStaticEnterFrame(... rest) : void {
         var _loc5_:* = 0.0;
         var _loc3_:* = 0.0;
         var _loc9_:* = 0;
         var _loc8_:* = targets;
         for(_loc4_ in targets)
         {
            _loc5_++;
            _loc3_ = 0.0;
            _loc7_ = 0;
            _loc6_ = targets[_loc4_];
            for(_loc2_ in targets[_loc4_])
            {
               _loc3_++;
               _loc4_[_loc2_] = Equations[targets[_loc4_][_loc2_].ease](targets[_loc4_][_loc2_].time,targets[_loc4_][_loc2_].begin,targets[_loc4_][_loc2_].change,targets[_loc4_][_loc2_].duration);
               targets[_loc4_][_loc2_].time++;
               if(targets[_loc4_][_loc2_].time > targets[_loc4_][_loc2_].duration)
               {
                  delete targets[_loc4_][_loc2_];
               }
            }
            if(!_loc3_)
            {
               delete targets[_loc4_];
               _loc5_--;
            }
         }
         if(!_loc5_)
         {
            frame.removeEventListener("enterFrame",onStaticEnterFrame);
         }
      }
      
      public var equation:String;
      
      private var time:Number = 0;
      
      public var begin:Number = 0;
      
      public var change:Number = 1;
      
      public var duration:Number = 100;
      
      private var _value:Number;
      
      public function start() : void {
         stop();
         time = 0;
         dispatchEvent(new Event("tweenStart"));
         sprite.addEventListener("enterFrame",onEnterFrame);
      }
      
      public function stop() : void {
         if(sprite.hasEventListener("enterFrame"))
         {
            sprite.removeEventListener("enterFrame",onEnterFrame);
            dispatchEvent(new Event("tweenStop"));
         }
      }
      
      private function onEnterFrame(... rest) : void {
         _value = Equations[equation](time,begin,change,duration);
         dispatchEvent(new Event("tween"));
         if(time < duration)
         {
            time = time + 1;
         }
         else
         {
            stop();
         }
      }
      
      public function get value() : Number {
         return _value;
      }
   }
}
