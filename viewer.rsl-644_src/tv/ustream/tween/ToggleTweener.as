package tv.ustream.tween
{
   import flash.events.EventDispatcher;
   import flash.display.Sprite;
   import flash.events.Event;
   
   public class ToggleTweener extends EventDispatcher
   {
      
      public function ToggleTweener(param1:Boolean, param2:Number = 30) {
         super();
         sprite = new Sprite();
         this.duration = param2;
         _state = param1;
         _tweenPosition = param1;
         _lastCounter = param1;
         _counter = param1;
         _equation = Equations.easeInOutSine;
      }
      
      public static const CHANGED:String = "changed";
      
      public static const STOPPED:String = "stopped";
      
      private var _state:Boolean;
      
      private var _step:Number;
      
      private var _counter:Number = 0;
      
      private var _lastCounter:Number = 0;
      
      private var _tweenPosition:Number = 0;
      
      private var sprite:Sprite;
      
      private var _equation:Function;
      
      public function get position() : Number {
         return _tweenPosition;
      }
      
      public function get state() : Boolean {
         return _state;
      }
      
      public function set state(param1:Boolean) : void {
         if(_state != param1)
         {
            _state = param1;
            if(!sprite.hasEventListener("enterFrame"))
            {
               sprite.addEventListener("enterFrame",frameHandler);
            }
         }
      }
      
      public function get duration() : Number {
         return 1 / _step;
      }
      
      public function set duration(param1:Number) : void {
         if(param1 <= 0)
         {
            throw new ArgumentError("Duration cannot be (lower than) zero");
         }
         else
         {
            _step = 1 / param1;
            return;
         }
      }
      
      private function frameHandler(param1:Event) : void {
         _counter = _counter + _step * (1 - 2 * (!_state));
         _counter = Math.max(0,Math.min(_counter,1));
         if(_counter != _lastCounter)
         {
            _tweenPosition = _equation(1 / _step * _counter,0,1,1 / _step);
            dispatchEvent(new Event("changed"));
         }
         else if(sprite.hasEventListener("enterFrame"))
         {
            dispatchEvent(new Event("stopped"));
            sprite.removeEventListener("enterFrame",frameHandler);
         }
         
         _lastCounter = _counter;
      }
   }
}
