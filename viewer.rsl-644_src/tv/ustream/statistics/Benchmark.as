package tv.ustream.statistics
{
   import tv.ustream.tools.Debug;
   import flash.utils.getTimer;
   
   public class Benchmark extends Reporter
   {
      
      public function Benchmark() {
         super();
         if(_logicInitiated != -1)
         {
            reportChange("lid",_logicInitiated);
            _logicInitiated = -1;
         }
      }
      
      private static var _logicInitiated:int = -1;
      
      public static function logicInitiated(param1:Number) : void {
         if(!isNaN(param1))
         {
            _logicInitiated = param1;
            Debug.echo("[ BENCHMARK ] vmStarted " + _logicInitiated);
         }
         else
         {
            Debug.echo("[ BENCHMARK ] vmStarted isNaN");
         }
      }
      
      private const LOGIC_INITIATED:String = "lid";
      
      private const UMS_CONNECTED:String = "ucd";
      
      private const FIRST_MODULE_INFO:String = "fmi";
      
      private const PROVIDER_CONNECTED:String = "pcd";
      
      private const PROVIDER_SUBSCRIBED:String = "psd";
      
      private const KEYFRAME_AFTER_PLAY:String = "kfap";
      
      private const SEEK_COMPLETE:String = "kfas";
      
      private const JUMP_COMPLETE:String = "kfaj";
      
      private const BANDWIDTH_REPORT:String = "bwrpt";
      
      private var _umsConnectStarted:int = -1;
      
      private var _umsConnectSuccess:int = -1;
      
      private var _umsModuleInit:int = -1;
      
      private var _providerConnectStart:int = -1;
      
      private var _providerConnectSuccess:int = -1;
      
      private var _providerSubscribeStart:int = -1;
      
      private var _providerSubscribeSuccess:int = -1;
      
      private var _streamPlayStart:int = -1;
      
      private var _streamGetSizeAfterPlay:int = -1;
      
      private var _streamSeekStart:int = -1;
      
      private var _streamSeekComplete:int = -1;
      
      private var _streamJumpStart:int = -1;
      
      private var _streamJumpComplete:int = -1;
      
      public function umsConnectStarted() : void {
         _umsConnectStarted = getTimer();
         _umsConnectSuccess = -1;
         _umsModuleInit = -1;
      }
      
      public function umsConnectSuccess() : void {
         _umsConnectSuccess = getTimer();
         if(_umsConnectStarted != -1)
         {
            reportChange("ucd",_umsConnectSuccess - _umsConnectStarted);
         }
         else
         {
            echo("ERROR: ums connection start was not set");
         }
      }
      
      public function umsModuleInfo() : void {
         if(_umsModuleInit == -1)
         {
            _umsModuleInit = getTimer();
            if(_umsConnectSuccess != -1)
            {
               reportChange("fmi",_umsModuleInit - _umsConnectSuccess);
            }
            else
            {
               echo("ERROR: ums connection success was not set");
            }
         }
      }
      
      public function providerConnectStart() : void {
         _providerConnectStart = getTimer();
         _providerConnectSuccess = -1;
      }
      
      public function providerConnectSuccess() : void {
         _providerConnectSuccess = getTimer();
         if(_providerConnectStart != -1)
         {
            reportChange("pcd",_providerConnectSuccess - _providerConnectStart);
         }
         else
         {
            echo("ERROR: provider connection start was not set");
         }
      }
      
      public function providerSubscribeStart() : void {
         _providerSubscribeStart = getTimer();
         _providerConnectSuccess = -1;
      }
      
      public function providerSubscribeSuccess() : void {
         _providerSubscribeSuccess = getTimer();
         if(_providerSubscribeStart != -1)
         {
            reportChange("psd",_providerSubscribeSuccess - _providerSubscribeStart);
         }
         else
         {
            echo("ERROR: subscription start was not set");
         }
      }
      
      public function streamPlayStart() : void {
         _streamPlayStart = getTimer();
         _streamGetSizeAfterPlay = -1;
      }
      
      public function streamGetSizeAfterPlay() : void {
         if(_streamGetSizeAfterPlay == -1)
         {
            _streamGetSizeAfterPlay = getTimer();
            if(_streamPlayStart != -1)
            {
               reportChange("kfap",_streamGetSizeAfterPlay - _streamPlayStart);
            }
            else
            {
               echo("ERROR: stream play start was not set");
            }
         }
      }
      
      public function streamSeekStart() : void {
         _streamSeekStart = getTimer();
         _streamSeekComplete = -1;
      }
      
      public function streamSeekComplete() : void {
         if(!(_streamSeekStart == -1) && _streamSeekComplete == -1)
         {
            _streamSeekComplete = getTimer();
            reportChange("kfas",_streamSeekComplete - _streamSeekStart);
            _streamSeekStart = -1;
         }
      }
      
      public function streamJumpStart() : void {
         _streamJumpStart = getTimer();
         _streamJumpComplete = -1;
      }
      
      public function streamJumpComplete() : void {
         if(!(_streamJumpStart == -1) && _streamJumpComplete == -1)
         {
            _streamJumpComplete = getTimer();
            reportChange("kfaj",_streamJumpComplete - _streamJumpStart);
            _streamJumpStart = -1;
         }
      }
      
      public function bandwidthReport(param1:String, param2:uint, param3:uint) : void {
         reportChange("bwrpt",[
            {
               "url":param1,
               "size":param2,
               "time":param3
            }]);
      }
   }
}
