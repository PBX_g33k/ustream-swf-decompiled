package tv.ustream.statistics
{
   import flash.net.URLLoader;
   import tv.ustream.tools.This;
   import flash.net.URLRequest;
   import tv.ustream.tools.StringUtils;
   import flash.events.Event;
   import flash.events.HTTPStatusEvent;
   
   public class Failure extends Reporter
   {
      
      public function Failure() {
         super();
         reportCache = [];
      }
      
      public static const STREAM_NOT_FOUND:String = "streamNotFound";
      
      public static const UNPUBLISH_NOTIFY:String = "unpublishNotify";
      
      public static const CONNECTION_ISSUES:String = "connectionIssues";
      
      public static const DISCONNECTED:String = "disconnected";
      
      public static const UHS_CHUNK_NOT_FOUND:String = "uhsChunkNotFound";
      
      public static const UHS_CHUNK_SECURITY_ERROR:String = "uhsChunkSecurityError";
      
      public static const UHS_CHUNK_MALFORMED_DATA:String = "uhsChunkMalformedData";
      
      public static const UMS_DISCONNECTED:String = "umsDisconnected";
      
      public static const UMS_CONNECTION_ISSUES:String = "umsConnectionIssues";
      
      public static const VOD_INVALID_DURATION:String = "vodInvalidDuration";
      
      public static const VOD_FLV_MALFORMED_DATA:String = "vodFlvMalformedData";
      
      public static const ADBLOCKER:String = "adblocker";
      
      private const CONNECTIVITY_UNKNOWN:int = -1;
      
      private const CONNECTIVITY_OKAY:int = 1;
      
      private const CONNECTIVITY_WONKY:int = 0;
      
      private var connectivity:int = -1;
      
      private var loader:URLLoader;
      
      private var checking:Boolean = false;
      
      private var reportCache:Array;
      
      public function report(param1:String, param2:String = null, param3:Object = null) : void {
         echo("report " + param1);
         var _loc5_:Object = {};
         _loc5_.type = param1;
         if(param2)
         {
            _loc5_.provider = param2;
         }
         _loc5_.details = {};
         if(param3)
         {
            _loc8_ = 0;
            _loc7_ = param3;
            for(_loc6_ in param3)
            {
               _loc5_.details[_loc6_] = param3[_loc6_];
            }
         }
         var _loc4_:Array = ["umsConnectionIssues","connectionIssues"];
         if(_loc4_.indexOf(param1) != -1)
         {
         }
         if(_loc4_.indexOf(param1) != -1)
         {
            dispatch("change",false,{"error":[_loc5_]});
            return;
         }
         dispatch("change",false,{"error":[_loc5_]});
      }
      
      private function initLoader() : void {
         destroyLoader();
         loader = new URLLoader();
         loader.addEventListener("httpStatus",onLoaderStatus);
         loader.addEventListener("ioError",onLoaderError);
         loader.addEventListener("securityError",onLoaderError);
         loader.addEventListener("complete",onLoaderComplete);
         var _loc1_:String = "http://static-cdn1.ustream.tv/crossdomain.xml";
         if(This.secure)
         {
            _loc1_ = "https://ustreamssl-a.akamaihd.net/crossdomain.xml";
         }
         loader.load(new URLRequest(_loc1_ + "?foo=" + StringUtils.random(8)));
         checking = true;
      }
      
      private function destroyLoader() : void {
         if(loader)
         {
            try
            {
               loader.close();
            }
            catch(err:Error)
            {
            }
            loader.addEventListener("httpStatus",onLoaderStatus);
            loader.addEventListener("ioError",onLoaderError);
            loader.addEventListener("securityError",onLoaderError);
            loader.addEventListener("complete",onLoaderComplete);
            loader = new URLLoader();
         }
         checking = false;
      }
      
      private function onLoaderComplete(param1:Event) : void {
         echo("onLoaderComplete");
         connectivity = 1;
         destroyLoader();
         dispatch("change",false,{"error":reportCache});
         reportCache = [];
      }
      
      private function onLoaderError(param1:Event) : void {
         echo("onLoaderError " + param1.toString());
         connectivity = 0;
         destroyLoader();
         reportCache = [];
      }
      
      private function onLoaderStatus(param1:HTTPStatusEvent) : void {
         echo("onLoaderStatus " + param1.status);
      }
   }
}
