package tv.ustream.statistics
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.tools.DynamicEvent;
   
   public class Statistics extends Dispatcher
   {
      
      public function Statistics() {
         super();
         cache = [];
         _aggregator = new Aggregator();
         _aggregator.addEventListener("dispatch",onAggregatorDispatch);
         _benchmark = new Benchmark();
         _benchmark.addEventListener("change",onItemChange);
         _common = new Common();
         _common.addEventListener("change",onItemChange);
         _failure = new Failure();
         _failure.addEventListener("change",onItemChange);
         _uhs = new Uhs();
         _uhs.addEventListener("change",onItemChange);
      }
      
      private const BRAND_ID:String = "bid";
      
      private const MEDIA_ID:String = "mid";
      
      private const APPLICATION:String = "app";
      
      private var _benchmark:Benchmark;
      
      private var _common:Common;
      
      private var _failure:Failure;
      
      private var _uhs:Uhs;
      
      private var _aggregator:Aggregator;
      
      private var cache:Array;
      
      public var mediaId:String;
      
      public var brandId:String;
      
      public function get benchmark() : Benchmark {
         return _benchmark;
      }
      
      public function get common() : Common {
         return _common;
      }
      
      public function get failure() : Failure {
         return _failure;
      }
      
      public function get uhs() : Uhs {
         return _uhs;
      }
      
      private function onItemChange(param1:DynamicEvent) : void {
         _aggregator.process(param1);
      }
      
      private function onAggregatorDispatch(param1:DynamicEvent) : void {
         var _loc2_:* = param1.target;
         if(uhs === _loc2_)
         {
            if((param1 as DynamicEvent).data.uhs)
            {
               if(brandId)
               {
                  (param1 as DynamicEvent).data.uhs["bid"] = brandId;
               }
               if(mediaId)
               {
                  (param1 as DynamicEvent).data.uhs["mid"] = mediaId;
               }
               (param1 as DynamicEvent).data.uhs["app"] = "channel";
            }
         }
         if(hasEventListener("change"))
         {
            dispatch("change",false,param1.data,false);
         }
         else
         {
            echo("storing to cache");
            cache.push(param1);
         }
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         super.addEventListener(param1,param2,param3,param4,param5);
         if(param1 == "change")
         {
            echo("dumping cache");
            while(cache.length)
            {
               dispatchEvent(cache[0]);
               cache.splice(0,1);
            }
         }
      }
      
      override public function destroy(... rest) : * {
         _benchmark = _benchmark.destroy();
         _common = _common.destroy();
         _failure = _failure.destroy();
         _uhs = _uhs.destroy();
         _aggregator = _aggregator.destroy();
         return super.destroy();
      }
      
      public function get aggregatorImmediateDelay() : uint {
         return _aggregator?_aggregator.immediateDelay:0;
      }
      
      public function set aggregatorImmediateDelay(param1:uint) : void {
         if(_aggregator)
         {
            _aggregator.immediateDelay = param1;
         }
      }
      
      public function get aggregatorDeferredDelay() : uint {
         return _aggregator?_aggregator.deferredDelay:0;
      }
      
      public function set aggregatorDeferredDelay(param1:uint) : void {
         if(_aggregator)
         {
            _aggregator.deferredDelay = param1;
         }
      }
   }
}
