package tv.ustream.statistics
{
   import tv.ustream.tools.Dispatcher;
   
   public class Reporter extends Dispatcher
   {
      
      public function Reporter() {
         super();
         cache = [];
      }
      
      private var cache:Array;
      
      protected function reportChange(param1:String, param2:*, ... rest) : void {
         var _loc5_:* = 0;
         var _loc4_:Object = {};
         _loc4_[type] = {};
         _loc4_[type][param1] = param2;
         if(rest && rest.length)
         {
            _loc5_ = 0;
            while(_loc5_ < rest.length)
            {
               _loc4_[type][rest[_loc5_]] = rest[_loc5_ + 1];
               _loc5_ = _loc5_ + 2;
            }
         }
         if(hasEventListener("change"))
         {
            dispatch("change",false,_loc4_,true);
         }
         else
         {
            cache.push(_loc4_);
         }
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         super.addEventListener(param1,param2,param3,param4,param5);
         if(param1 == "change")
         {
            while(cache.length)
            {
               dispatch("change",false,cache[0],true);
               cache.splice(0,1);
            }
         }
      }
   }
}
