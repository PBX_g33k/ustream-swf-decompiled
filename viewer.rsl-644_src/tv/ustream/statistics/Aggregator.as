package tv.ustream.statistics
{
   import tv.ustream.tools.Dispatcher;
   import flash.utils.Timer;
   import tv.ustream.tools.DynamicEvent;
   import flash.utils.getTimer;
   import tv.ustream.tools.ObjectUtils;
   import flash.events.TimerEvent;
   
   public class Aggregator extends Dispatcher
   {
      
      public function Aggregator() {
         super();
         _immediateTimer = new Timer(_immediateDelay * 1000);
         _immediateTimer.addEventListener("timer",onImmediateTimer);
         _immediateTimer.start();
         _deferredTimer = new Timer(_deferredDelay * 1000);
         _deferredTimer.addEventListener("timer",onDeferredTimer);
         _deferredTimer.start();
      }
      
      public static const DISPATCH:String = "dispatch";
      
      protected var _immediateDelay:uint = 1;
      
      protected var _deferredDelay:uint = 5;
      
      protected var _immediateTimer:Timer;
      
      protected var _deferredTimer:Timer;
      
      protected var _immediateData:Object;
      
      protected var _deferredData:Object;
      
      public function get immediateDelay() : uint {
         return _immediateDelay;
      }
      
      public function set immediateDelay(param1:uint) : void {
         _immediateDelay = param1;
         _immediateTimer.reset();
         if(param1)
         {
            _immediateTimer.delay = _immediateDelay * 1000;
            _immediateTimer.start();
         }
      }
      
      public function get deferredDelay() : uint {
         return _deferredDelay;
      }
      
      public function set deferredDelay(param1:uint) : void {
         _deferredDelay = param1;
         _deferredTimer.reset();
         if(param1)
         {
            _deferredTimer.delay = _deferredDelay * 1000;
            _deferredTimer.start();
         }
      }
      
      public function process(param1:DynamicEvent) : void {
         var _loc2_:Object = param1.data;
         if(_loc2_.common && !(_loc2_.common.bufempty == undefined))
         {
            if(!_immediateData)
            {
               _immediateData = {};
               _immediateData.common = {};
               _immediateData.common.bufempty = [];
            }
            if(_loc2_.common.strurl)
            {
               _immediateData.common.strurl = _loc2_.common.strurl;
               delete _loc2_.common.strurl;
            }
            (_immediateData.common.bufempty as Array).push(
               {
                  "value":_loc2_.common.bufempty,
                  "time":getTimer()
               });
            delete _loc2_.common.bufempty;
            if(!ObjectUtils.hasKeys(_loc2_.common))
            {
               delete _loc2_.common;
            }
         }
         if(ObjectUtils.hasKeys(_loc2_))
         {
            if(!_deferredData)
            {
               _deferredData = _loc2_;
            }
            else
            {
               ObjectUtils.merge(_loc2_,_deferredData);
            }
         }
      }
      
      private function onDeferredTimer(param1:TimerEvent) : void {
         var _loc3_:* = null;
         var _loc2_:* = 0;
         if(_deferredData)
         {
            if(_deferredData.benchmark && _deferredData.benchmark.bwrpt is Array)
            {
               _loc3_ = _deferredData.benchmark.bwrpt;
               _loc3_.sortOn("url");
               _loc2_ = 0;
               while(_loc2_ < _loc3_.length - 1)
               {
                  if(_loc3_[_loc2_].url == _loc3_[_loc2_ + 1].url)
                  {
                     _loc3_[_loc2_].size = _loc3_[_loc2_].size + _loc3_[_loc2_ + 1].size;
                     _loc3_[_loc2_].time = _loc3_[_loc2_].time + _loc3_[_loc2_ + 1].time;
                     _loc3_.splice(_loc2_ + 1,1);
                     _loc2_--;
                  }
                  _loc2_++;
               }
            }
            dispatch("dispatch",false,_deferredData,true);
            _deferredData = null;
         }
      }
      
      private function onImmediateTimer(param1:TimerEvent) : void {
         var _loc4_:* = null;
         var _loc5_:* = 0;
         var _loc6_:* = 0;
         var _loc3_:* = 0;
         var _loc2_:* = null;
         if(_immediateData)
         {
            if(_immediateData.common && _immediateData.common.bufempty is Array)
            {
               _loc4_ = _immediateData.common.bufempty as Array;
               _loc5_ = 0;
               _loc6_ = 0;
               _loc3_ = getTimer();
               while(_loc4_.length)
               {
                  _loc2_ = _loc4_.pop();
                  _loc7_ = _loc2_.value;
                  if(true !== _loc7_)
                  {
                     if(false === _loc7_)
                     {
                        _loc6_ = _loc6_ + (_loc3_ - _loc2_.time);
                     }
                  }
                  else
                  {
                     _loc5_ = _loc5_ + (_loc3_ - _loc2_.time);
                  }
                  _loc3_ = _loc2_.time;
               }
               _immediateData.common.bufempty = _loc5_ > _loc6_;
            }
            dispatch("dispatch",false,_immediateData,true);
            _immediateData = null;
         }
      }
   }
}
