package tv.ustream.statistics
{
   public class Uhs extends Reporter
   {
      
      public function Uhs() {
         super();
         _loadCompleteTimes = [];
         _loadFirstByteTimes = [];
      }
      
      private const CHUNK_INDEX:String = "ci";
      
      private const PREDICTED_INDEX:String = "pi";
      
      private const CHUNK_LOAD_AVERAGE:String = "cla";
      
      private const CHUNK_FIRSTBYTE_AVERAGE:String = "cfa";
      
      private var _chunkIndex:Number;
      
      private var _predictedIndex:Number;
      
      private var _loadCompleteTimes:Array;
      
      private var _loadFirstByteTimes:Array;
      
      public function get chunkIndex() : Number {
         return _chunkIndex;
      }
      
      public function set chunkIndex(param1:Number) : void {
         _chunkIndex = param1;
      }
      
      public function get predictedIndex() : Number {
         return _predictedIndex;
      }
      
      public function set predictedIndex(param1:Number) : void {
         _predictedIndex = param1;
      }
      
      public function setLoadCompleteTime(param1:Number) : void {
         _loadCompleteTimes.push(param1);
         while(_loadCompleteTimes.length > 5)
         {
            _loadCompleteTimes.splice(0,1);
         }
         reportChange("cla",chunkLoadAverage);
      }
      
      public function get chunkLoadAverage() : Number {
         var _loc2_:* = 0;
         if(_loadCompleteTimes.length == 0)
         {
            return 0;
         }
         var _loc1_:* = 0.0;
         _loc2_ = 0;
         while(_loc2_ < _loadCompleteTimes.length)
         {
            _loc1_ = _loc1_ + _loadCompleteTimes[_loc2_] / _loadCompleteTimes.length;
            _loc2_++;
         }
         return Math.floor(_loc1_);
      }
      
      public function setLoadFirstByteTime(param1:Number) : void {
         _loadFirstByteTimes.push(param1);
         while(_loadFirstByteTimes.length > 5)
         {
            _loadFirstByteTimes.splice(0,1);
         }
         reportChange("cfa",chunkFirstByteAverage);
      }
      
      public function get chunkFirstByteAverage() : Number {
         var _loc2_:* = 0;
         if(_loadFirstByteTimes.length == 0)
         {
            return 0;
         }
         var _loc1_:* = 0.0;
         _loc2_ = 0;
         while(_loc2_ < _loadFirstByteTimes.length)
         {
            _loc1_ = _loc1_ + _loadFirstByteTimes[_loc2_] / _loadFirstByteTimes.length;
            _loc2_++;
         }
         return Math.floor(_loc1_);
      }
   }
}
