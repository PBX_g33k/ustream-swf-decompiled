package tv.ustream.statistics
{
   public class Common extends Reporter
   {
      
      public function Common() {
         super();
      }
      
      private const MODULE_INFO_ID:String = "infoid";
      
      private const STREAM_NAME:String = "strname";
      
      private const STREAM_URL:String = "strurl";
      
      private const PROVIDER_NAME:String = "pvdname";
      
      private const BUFFER_EMPTY:String = "bufempty";
      
      private var _streamName:String;
      
      private var _streamUrl:String;
      
      private var _providerName:String;
      
      private var _moduleInfoId:uint;
      
      private var _bufferEmpty:Boolean;
      
      public function redispatch() : void {
         if(_streamName)
         {
            streamName = _streamName;
         }
         if(_providerName)
         {
            providerName = _providerName;
         }
         bufferEmpty = _bufferEmpty;
      }
      
      public function get moduleInfoId() : Number {
         return _moduleInfoId;
      }
      
      public function set moduleInfoId(param1:Number) : void {
         _moduleInfoId = param1;
         reportChange("infoid",_moduleInfoId);
      }
      
      public function get streamName() : String {
         return _streamName;
      }
      
      public function set streamName(param1:String) : void {
         _streamName = param1;
         reportChange("strname",_streamName);
      }
      
      public function get streamUrl() : String {
         return _streamUrl;
      }
      
      public function set streamUrl(param1:String) : void {
         _streamUrl = param1 is String?param1.split("_%_%")[0]:param1;
      }
      
      public function get providerName() : String {
         return _providerName;
      }
      
      public function set providerName(param1:String) : void {
         _providerName = param1;
         reportChange("pvdname",_providerName);
      }
      
      public function get bufferEmpty() : Boolean {
         return _bufferEmpty;
      }
      
      public function set bufferEmpty(param1:Boolean) : void {
         _bufferEmpty = param1;
         reportChange("bufempty",_bufferEmpty,"strurl",_streamUrl);
      }
   }
}
