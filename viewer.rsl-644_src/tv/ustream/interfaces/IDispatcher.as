package tv.ustream.interfaces
{
   import flash.events.IEventDispatcher;
   
   public interface IDispatcher extends IEventDispatcher
   {
      
      function get id() : String;
      
      function get type() : String;
      
      function get active() : Boolean;
      
      function setEchoPrefix(param1:String) : void;
      
      function destroy(... rest) : *;
      
      function eliminateEventListeners() : void;
   }
}
