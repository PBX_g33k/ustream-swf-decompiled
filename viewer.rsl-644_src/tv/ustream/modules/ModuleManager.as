package tv.ustream.modules
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.net.Connection;
   import tv.ustream.net.interfaces.ICommunicator;
   import tv.ustream.tools.DynamicEvent;
   import tv.ustream.tools.Debug;
   import flash.utils.getQualifiedClassName;
   import flash.utils.getDefinitionByName;
   import flash.events.Event;
   import flash.system.System;
   import flash.net.NetStream;
   import tv.ustream.viewer.logic.modules.ViewerModuleManager;
   import tv.ustream.tools.Shell;
   
   public class ModuleManager extends Dispatcher
   {
      
      public function ModuleManager(param1:* = null) {
         _modules = {};
         parseOrder = [];
         base = getQualifiedClassName(this).split("::")[0];
         if(param1)
         {
            this.logic = param1;
            param1.addEventListener("moduleInfo",onModuleInfoFromLogic);
         }
         addEventListener("connection",onUmsConnection);
         super();
         dispatch("create");
      }
      
      protected var _modules:Object;
      
      protected var parseOrder:Array;
      
      private var moduleInitRef:Function;
      
      private var moduleInfoRef:Function;
      
      private var _destroying:Boolean = false;
      
      private var _connection:Connection;
      
      private var _umsConnection:ICommunicator;
      
      protected var logic;
      
      private var base:String;
      
      private function onModuleInfoFromLogic(param1:DynamicEvent) : void {
         if(param1.info)
         {
            info(param1.info,param1.source);
         }
      }
      
      protected function init(param1:Object = null, param2:Object = null) : void {
         dispatch("init");
         Debug.explore(param1);
         if(moduleInitRef != null)
         {
            moduleInitRef(param1,param2);
         }
         var _loc3_:String = getQualifiedClassName(this);
         this.info(param2,_loc3_ + " - init");
      }
      
      protected function connectionInfo(param1:Object, param2:String = "") : void {
         if(param2 == "")
         {
            this.info(param1,"fms (" + _connection.label + ")");
         }
         else
         {
            this.info(param1,param2);
         }
      }
      
      protected function info(param1:Object, param2:String) : void {
         var _loc6_:* = null;
         var _loc5_:* = 0;
         var _loc3_:Object = {};
         var _loc4_:uint = 0;
         var _loc8_:* = 0;
         var _loc7_:* = param1;
         for(_loc6_ in param1)
         {
            if(!(_loc6_ == "viewers") && !(_loc6_ == "allViewers") && !(_loc6_ == "allTimeTotal"))
            {
               _loc3_[_loc6_] = param1[_loc6_];
               _loc4_++;
            }
         }
         if(_loc4_)
         {
            echo("info from " + param2);
            Debug.explore(_loc3_);
         }
         var _loc10_:* = 0;
         var _loc9_:* = param1;
         for(_loc6_ in param1)
         {
            if(parseOrder.indexOf(_loc6_) == -1)
            {
               parseModules(param1,_loc6_,param2);
            }
         }
         _loc5_ = 0;
         while(_loc5_ < parseOrder.length)
         {
            if(param1[parseOrder[_loc5_]] != undefined)
            {
               parseModules(param1,parseOrder[_loc5_],param2);
            }
            _loc5_++;
         }
         if(moduleInfoRef != null)
         {
            moduleInfoRef(param1,param2);
         }
      }
      
      protected function create(param1:String, param2:* = null) : Module {
         var _loc6_:* = null;
         var _loc7_:* = undefined;
         var _loc4_:* = null;
         var _loc3_:* = null;
         var _loc5_:String = param1.toLowerCase();
         if(!_modules[param1])
         {
            _loc6_ = param1.charAt(0).toUpperCase() + param1.substr(1);
            _loc7_ = getDefinitionByName(base + "::" + _loc6_);
            if(_loc7_)
            {
               _loc4_ = param2;
               if(param1 == "ums")
               {
                  if(!_loc4_ || typeof param2 == "boolean")
                  {
                     _loc4_ = {};
                  }
                  _loc4_.info = info;
               }
               _modules[_loc5_] = new _loc7_(logic,_loc4_,this);
               _modules[_loc5_].addEventListener("destroy",onDestroyModule);
               _modules[_loc5_].addEventListener("request",onRequestModule);
               _modules[_loc5_].addEventListener("lock",onLockModule);
               if(_connection && !_modules[_loc5_].needUms)
               {
                  _modules[_loc5_].connection = _connection;
               }
               else if(_umsConnection && _modules[_loc5_].needUms)
               {
                  _modules[_loc5_].connection = _umsConnection;
               }
               
               dispatch("create" + _loc6_);
            }
         }
         else if(param2)
         {
            _loc3_ = {};
            _loc3_[param1] = param2;
            info(_loc3_,param1);
         }
         
         return _modules[param1];
      }
      
      private function onLockModule(param1:Event) : void {
         dispatch("lock");
      }
      
      private function onRequestModule(param1:DynamicEvent) : void {
         if(!_modules[param1.requestModule])
         {
            create(param1.requestModule);
         }
      }
      
      private function onDestroyModule(param1:Event) : void {
         if(param1.target.type)
         {
            dispatch("destroy" + param1.target.type.charAt(0).toUpperCase() + param1.target.type.substr(1));
            _modules[param1.target.type].removeEventListener("destroy",onDestroyModule);
            _modules[param1.target.type].removeEventListener("request",onRequestModule);
            _modules[param1.target.type].removeEventListener("lock",onLockModule);
            _modules[param1.target.type] = null;
            delete _modules[param1.target.type];
            System.gc();
            System.gc();
         }
      }
      
      override public function destroy(... rest) : * {
         _destroying = true;
         if(logic)
         {
            logic.removeEventListener("moduleInfo",onModuleInfoFromLogic);
         }
         removeEventListener("connection",onUmsConnection);
         var _loc4_:* = 0;
         var _loc3_:* = _modules;
         for each(_loc2_ in _modules)
         {
            _loc2_.destroy();
         }
         if(_connection)
         {
            _connection.moduleInit = moduleInitRef;
            _connection.moduleInfo = moduleInfoRef;
         }
         return super.destroy();
      }
      
      public function set connection(param1:Connection) : void {
         _connection = param1;
         if(_connection)
         {
            moduleInitRef = _connection.moduleInit;
            moduleInfoRef = _connection.moduleInfo;
            _connection.moduleInit = init;
            _connection.moduleInfo = connectionInfo;
         }
         var _loc4_:* = 0;
         var _loc3_:* = _modules;
         for each(_loc2_ in _modules)
         {
            if(!_loc2_.needUms)
            {
               _loc2_.connection = _umsConnection;
            }
         }
      }
      
      public function set netStream(param1:NetStream) : void {
         if(param1 && param1.client)
         {
            param1.client.moduleInfo = streamInfo;
         }
      }
      
      private function streamInfo(param1:Object, ... rest) : void {
         info(param1,"stream");
      }
      
      public function get lock() : Boolean {
         var _loc3_:* = 0;
         var _loc2_:* = _modules;
         for each(_loc1_ in _modules)
         {
            if(_loc1_.lock)
            {
               echo("module lock: " + _loc1_.type);
               return true;
            }
         }
         return false;
      }
      
      private function onUmsConnection(param1:DynamicEvent) : void {
         echo("onUmsConnection");
         _umsConnection = param1.data?param1.data.connection:null;
         var _loc4_:* = 0;
         var _loc3_:* = _modules;
         for each(_loc2_ in _modules)
         {
            if(_loc2_ && _loc2_.needUms)
            {
               _loc2_.connection = _umsConnection;
            }
         }
      }
      
      private function parseModules(param1:Object, param2:String, param3:String = "") : void {
         if(hasOwnProperty(param2))
         {
            if(param1[param2] == "destroy" || param1[param2] === false || param1[param2] === null)
            {
               if(this[param2])
               {
                  this[param2].destroy();
               }
            }
            else
            {
               if(!this[param2] && (!(param2 == "allViewers") || (param2 == "allViewers" && param3 == "ums")) && (!(param2 == "poll") || (param2 == "poll" && (!(this is ViewerModuleManager) || (this is ViewerModuleManager && param3 == "ums")))))
               {
                  create(param2,param1[param2]);
               }
               dispatchEvent(new DynamicEvent(param2.toLowerCase() + "Info",false,false,
                  {
                     "info":param1[param2],
                     "source":param3
                  }));
            }
         }
         else if(Shell.hasInstance && (Shell.instance.hasEventListener(param2,"update")))
         {
            Shell.instance.dispatch(param2,"update",param1[param2]);
         }
         
      }
      
      public function get destroying() : Boolean {
         return _destroying;
      }
   }
}
