package tv.ustream.modules
{
   import tv.ustream.tools.Dispatcher;
   import tv.ustream.net.interfaces.ICommunicator;
   import tv.ustream.tools.DynamicEvent;
   import flash.utils.getQualifiedClassName;
   import tv.ustream.tools.Debug;
   
   public class Module extends Dispatcher
   {
      
      public function Module(param1:*, param2:* = null, param3:ModuleManager = null) {
         var _loc4_:String = getQualifiedClassName(this).toLowerCase();
         if(_loc4_.indexOf("::") != -1)
         {
            _loc4_ = _loc4_.split("::")[1];
         }
         if(param1)
         {
            this.logic = param1;
         }
         this.manager = param3;
         if(param3)
         {
            param3.addEventListener(_loc4_ + "Info",onInfo);
         }
         this.initValue = param2;
         super();
      }
      
      public static const UPDATE:String = "update";
      
      public static const LOCK:String = "lock";
      
      protected var _connection:ICommunicator;
      
      protected var manager:ModuleManager = null;
      
      protected var initValue;
      
      protected var logic;
      
      protected var _data;
      
      protected var mustBeOnline:Boolean = true;
      
      protected var _lock:Boolean = false;
      
      protected var infoSource:String = "";
      
      protected var lastInfoSource:String = "";
      
      protected var _needUms:Boolean = false;
      
      protected var _dying:Boolean = false;
      
      private function onInfo(param1:DynamicEvent) : void {
         infoSource = param1.source;
         info(param1.info);
         infoSource = "";
      }
      
      override public function destroy(... rest) : * {
         _dying = true;
         var _loc2_:String = getQualifiedClassName(this).toLowerCase();
         if(_loc2_.indexOf("::") != -1)
         {
            _loc2_ = _loc2_.split("::")[1];
         }
         if(manager)
         {
            manager.removeEventListener(_loc2_ + "Info",onInfo);
         }
         return super.destroy();
      }
      
      protected function call(param1:String, ... rest) : void {
         Debug.echo("CALL " + param1 + "(" + rest + ")" + " allow : " + (_connection && _connection.connected && active));
         Debug.explore(rest);
         if(_connection && _connection.connected && active)
         {
            _connection.call.apply(_connection,["callModule",null,type.toLowerCase(),param1].concat(rest));
         }
      }
      
      protected function info(param1:*) : void {
         _data = param1;
      }
      
      public function get data() : * {
         return _data;
      }
      
      public function set connection(param1:ICommunicator) : void {
         _connection = param1;
         if(!param1 && (mustBeOnline))
         {
            destroy();
         }
      }
      
      public function get lock() : Boolean {
         return _lock && !_dying;
      }
      
      public function get needUms() : Boolean {
         return _needUms;
      }
   }
}
