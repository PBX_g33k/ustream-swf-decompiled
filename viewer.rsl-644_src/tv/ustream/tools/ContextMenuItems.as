package tv.ustream.tools
{
   import flash.ui.ContextMenuItem;
   
   public class ContextMenuItems extends Object
   {
      
      public function ContextMenuItems() {
         super();
      }
      
      public var userName:ContextMenuItem;
      
      public var forcedProviderChange:ContextMenuItem;
      
      public var switchStageVideoEnabled:ContextMenuItem;
      
      public var infoPanelSwitch:ContextMenuItem;
      
      public var infoPanelCopy:ContextMenuItem;
      
      public var debugSwitch:ContextMenuItem;
      
      public var userInitedLogSending:ContextMenuItem;
      
      public var copyCidToClipboard:ContextMenuItem;
      
      public var turnOffSpecContextMenuItems:ContextMenuItem;
   }
}
