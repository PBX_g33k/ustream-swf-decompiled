package tv.ustream.tools
{
   import flash.net.NetStream;
   import flash.events.Event;
   import tv.ustream.net.ChunkStream;
   import flash.events.NetStatusEvent;
   
   public class BufferGraph extends BitmapGraph
   {
      
      public function BufferGraph(param1:NetStream = null, param2:uint = 15, param3:uint = 300, param4:uint = 100) {
         super(param3,param4);
         this.maxBufferLength = param2;
         this.stream = param1;
      }
      
      private var _stream:NetStream;
      
      private var maxBufferLength:uint;
      
      private var lastBufferLength:Number;
      
      private var _graphColor:uint = 65280;
      
      private var _bufferTimeColor:uint = 16776960;
      
      private var lastTimer:Number;
      
      public function get stream() : NetStream {
         return _stream;
      }
      
      public function set stream(param1:NetStream) : void {
         if(_stream != param1)
         {
            if(_stream)
            {
               _stream.removeEventListener("netStatus",onStreamNetStatus);
            }
            _stream = param1;
            clear();
            _graphColor = 65280;
            if(_stream)
            {
               addEventListener("enterFrame",onEnterFrame);
            }
            else
            {
               removeEventListener("enterFrame",onEnterFrame);
            }
            if(_stream)
            {
               _stream.addEventListener("netStatus",onStreamNetStatus);
            }
         }
      }
      
      public function get graphColor() : uint {
         return _graphColor;
      }
      
      public function set graphColor(param1:uint) : void {
         _graphColor = param1;
      }
      
      public function get bufferTimeColor() : uint {
         return _bufferTimeColor;
      }
      
      public function set bufferTimeColor(param1:uint) : void {
         _bufferTimeColor = param1;
      }
      
      private function onEnterFrame(param1:Event) : void {
         if(stream is ChunkStream)
         {
            graphColor = (stream as ChunkStream).loading?16711680:(stream as ChunkStream).predictedIndex > (stream as ChunkStream).chunkIndex?8388863:33023.0;
            draw((stream as ChunkStream).queueLength / maxBufferLength,-1,graphColor,true);
         }
         draw(stream.bufferLength / maxBufferLength,lastBufferLength / maxBufferLength,65280,true);
         draw(stream.bufferTime / maxBufferLength,-1,bufferTimeColor,false);
         lastBufferLength = stream.bufferLength;
      }
      
      private function onStreamNetStatus(param1:NetStatusEvent) : void {
         var _loc2_:* = param1.info.code;
         if("ChunkStream.Load.IoError" !== _loc2_)
         {
            if("ChunkStream.Feed.Keyframe" !== _loc2_)
            {
               if("ChunkStream.Feed.SyncInfo" === _loc2_)
               {
                  draw(1,-1,16777215,true,1);
               }
            }
            else
            {
               draw(1,-1,33023,true,1);
            }
         }
         else
         {
            draw(1,-1,16711680,true,1);
         }
      }
      
      override public function destroy() : * {
         stream = null;
         return super.destroy();
      }
   }
}
