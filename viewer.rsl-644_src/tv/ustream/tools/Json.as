package tv.ustream.tools
{
   public class Json extends Object
   {
      
      public function Json() {
         super();
      }
      
      private static const LEFT_ENCLOSERS:Array = ["\"","[","{"];
      
      private static const RIGHT_ENCLOSERS:Array = ["\"","]","}"];
      
      private static const WHITESPACES:Array = [" ","\n","\r","i"];
      
      public static function getTokenValue(param1:String, param2:String) : String {
         var _loc3_:* = 0;
         var _loc6_:* = null;
         var _loc5_:* = null;
         var _loc4_:* = null;
         var _loc7_:String = "\"" + param2 + "\"";
         if(param1.indexOf(_loc7_) != -1)
         {
            _loc3_ = param1.indexOf(_loc7_) + _loc7_.length;
            if(param1.substr(_loc3_,1) == ":")
            {
               _loc3_++;
               while(param1.substr(_loc3_,1) == " ")
               {
                  _loc3_++;
               }
               _loc6_ = param1.substr(_loc3_++,1);
               _loc5_ = "";
               if(LEFT_ENCLOSERS.indexOf(_loc6_) != -1)
               {
                  _loc5_ = _loc6_;
               }
               _loc4_ = param1.substr(_loc3_++,1);
               while(_loc5_.length || !_loc5_.length && WHITESPACES.indexOf(_loc4_) == -1 && !(RIGHT_ENCLOSERS.indexOf(_loc6_) == -1))
               {
                  _loc6_ = _loc6_ + _loc4_;
                  if(_loc5_.substr(-1,1) == "[" && _loc4_ == "]" || (_loc5_.substr(-1,1) == "{" && _loc4_ == "}") || (_loc5_.substr(-1,1) == "\"" && _loc4_ == "\""))
                  {
                     _loc5_ = _loc5_.substr(0,_loc5_.length - 1);
                  }
                  else if(_loc4_ == "[" || _loc4_ == "{" || _loc4_ == "\"")
                  {
                     _loc5_ = _loc5_ + _loc4_;
                  }
                  
                  _loc4_ = param1.substr(_loc3_++,1);
               }
               if(_loc6_.substr(0,1) == "\"" && _loc6_.substr(-1,1) == "\"")
               {
                  _loc6_ = _loc6_.substr(1,_loc6_.length - 2);
               }
               return _loc6_;
            }
            return null;
         }
         return null;
      }
      
      public static function purgeWhiteSpaces(param1:String) : String {
         return null;
      }
   }
}
