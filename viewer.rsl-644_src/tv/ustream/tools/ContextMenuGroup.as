package tv.ustream.tools
{
   import flash.ui.ContextMenuItem;
   
   class ContextMenuGroup extends Object
   {
      
      function ContextMenuGroup(param1:Boolean = false) {
         _items = {};
         super();
         _available = param1;
      }
      
      private static var itemCounter:uint = 0;
      
      private var _available:Boolean;
      
      private var _items:Object;
      
      public function get items() : Array {
         var _loc1_:Array = [];
         if(!_available)
         {
            return _loc1_;
         }
         var _loc4_:* = 0;
         var _loc3_:* = _items;
         for each(_loc2_ in _items)
         {
            _loc1_.push(_loc2_.item);
         }
         if(_loc1_.length)
         {
            ContextMenuItem(_loc1_[0]).separatorBefore = true;
         }
         return _loc1_;
      }
      
      public function get available() : Boolean {
         return _available;
      }
      
      public function set available(param1:Boolean) : void {
         _available = param1;
      }
      
      public function addItem(param1:String, param2:Function, param3:Array = null, param4:String = null) : ContextMenuItem {
         var _loc5_:ContextMenuItemData = new ContextMenuItemData(param1,param2,param3);
         if(param4 === null)
         {
            itemCounter = itemCounter + 1;
            param4 = "item" + (itemCounter + 1);
         }
         _items[param4] = _loc5_;
         return (_items[param4] as ContextMenuItemData).item;
      }
      
      public function getItemDataByName(param1:String) : ContextMenuItemData {
         if(_items[param1])
         {
            return _items[param1];
         }
         return null;
      }
      
      public function removeItemByName(param1:String) : void {
         if(_items[param1])
         {
            delete _items[param1];
         }
      }
      
      public function removeItem(param1:ContextMenuItem) : void {
         var _loc4_:* = 0;
         var _loc3_:* = _items;
         for(_loc2_ in _items)
         {
            if((_items[_loc2_] as ContextMenuItemData).item == param1)
            {
               delete _items[_loc2_];
               return;
            }
         }
      }
   }
}
