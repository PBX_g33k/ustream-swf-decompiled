package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.Event;
   import flash.geom.Rectangle;
   
   public class Drag extends EventDispatcher
   {
      
      public function Drag(param1:Sprite = null, param2:Sprite = null) {
         super();
         this.target = param1;
         this.hit = param2;
      }
      
      public var top:Number = -Infinity;
      
      public var right:Number = Infinity;
      
      public var bottom:Number = Infinity;
      
      public var left:Number = -Infinity;
      
      private var _hit:Sprite;
      
      private var target:Sprite;
      
      private var x:Number;
      
      private var y:Number;
      
      private var ox:Number;
      
      private var oy:Number;
      
      public var grid:Number = 1;
      
      private var _trackOnly:Boolean;
      
      private var _enableXScroll:Boolean = true;
      
      private var _enableYScroll:Boolean = true;
      
      private var parent;
      
      private var _mouseEnabled:Boolean = true;
      
      public function get hit() : Sprite {
         return _hit;
      }
      
      public function set hit(param1:Sprite) : void {
         _hit = param1;
         if(!target)
         {
            target = _hit;
         }
         _hit.addEventListener("mouseDown",mouseDown);
      }
      
      public function trackClick(param1:MouseEvent) : void {
         if(mouseEnabled)
         {
            x = target.width / 2;
            y = target.height / 2;
            _hit.stage.addEventListener("mouseMove",mouseMove);
            _hit.stage.addEventListener("mouseUp",mouseUp);
            _hit.stage.addEventListener("mouseLeave",mouseUp);
            ox = target.x;
            oy = target.y;
            dispatchEvent(new Event("startDrag"));
         }
      }
      
      public function mouseDown(param1:MouseEvent) : void {
         var _loc2_:* = null;
         if(mouseEnabled)
         {
            _loc2_ = target.getBounds(target.stage);
            if(enableXScroll)
            {
               x = target.stage.mouseX - _loc2_.x;
            }
            if(enableYScroll)
            {
               y = target.stage.mouseY - _loc2_.y;
            }
            if(_trackOnly)
            {
               x = _hit.width / 2;
               y = _hit.height / 2;
            }
            _hit.stage.addEventListener("mouseMove",mouseMove);
            _hit.stage.addEventListener("mouseUp",mouseUp);
            _hit.stage.addEventListener("mouseLeave",mouseUp);
            ox = target.x;
            oy = target.y;
            dispatchEvent(new Event("startDrag"));
         }
      }
      
      private function mouseUp(param1:Event) : void {
         if(mouseEnabled)
         {
            dispatchEvent(new DynamicEvent("stopDragD",false,false,
               {
                  "dx":ox - target.x,
                  "dy":oy - target.y
               }));
            dispatchEvent(new Event("stopDrag"));
            if(_hit.stage)
            {
               _hit.stage.removeEventListener("mouseMove",mouseMove);
               _hit.stage.removeEventListener("mouseUp",mouseUp);
               _hit.stage.removeEventListener("mouseLeave",mouseUp);
            }
         }
      }
      
      private function mouseMove(param1:MouseEvent) : void {
         var _loc5_:* = null;
         var _loc9_:* = null;
         var _loc2_:* = NaN;
         var _loc3_:* = NaN;
         var _loc6_:* = NaN;
         var _loc8_:* = NaN;
         var _loc4_:* = NaN;
         var _loc7_:* = NaN;
         if(mouseEnabled)
         {
            _loc5_ = target.getBounds(target.stage);
            _loc9_ = target.getBounds(target);
            _loc2_ = param1.stageX - x - (_loc5_.x - target.x);
            _loc3_ = param1.stageY - y - (_loc5_.y - target.y);
            _loc6_ = Math.max(left,Math.min(right,_loc2_ - _loc2_ % grid));
            _loc8_ = Math.max(top,Math.min(bottom,_loc3_ - _loc3_ % grid));
            if(!(target.x == _loc6_) || !(target.y == _loc8_))
            {
               _loc4_ = target.x;
               _loc7_ = target.y;
               if(enableXScroll)
               {
                  target.x = _loc6_;
               }
               if(enableYScroll)
               {
                  target.y = _loc8_;
               }
               param1.updateAfterEvent();
               dispatchEvent(new Event("drag"));
               dispatchEvent(new DynamicEvent("dragD",false,false,
                  {
                     "x":_loc6_,
                     "y":_loc8_,
                     "ox":_loc4_,
                     "oy":_loc7_
                  }));
            }
         }
      }
      
      public function get mouseEnabled() : Boolean {
         return _mouseEnabled;
      }
      
      public function set mouseEnabled(param1:Boolean) : void {
         _mouseEnabled = param1;
         hit.buttonMode = param1;
      }
      
      public function get trackOnly() : Boolean {
         return _trackOnly;
      }
      
      public function set trackOnly(param1:Boolean) : void {
         _trackOnly = param1;
      }
      
      public function get enableXScroll() : Boolean {
         return _enableXScroll;
      }
      
      public function set enableXScroll(param1:Boolean) : void {
         _enableXScroll = param1;
      }
      
      public function get enableYScroll() : Boolean {
         return _enableYScroll;
      }
      
      public function set enableYScroll(param1:Boolean) : void {
         _enableYScroll = param1;
      }
   }
}
