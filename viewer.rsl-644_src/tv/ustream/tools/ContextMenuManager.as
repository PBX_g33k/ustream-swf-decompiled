package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import flash.display.DisplayObjectContainer;
   import flash.ui.ContextMenu;
   import flash.events.Event;
   import flash.events.ContextMenuEvent;
   import flash.ui.ContextMenuItem;
   import flash.errors.IllegalOperationError;
   
   public class ContextMenuManager extends EventDispatcher
   {
      
      public function ContextMenuManager() {
         _groups = {};
         _groupCodes = {};
         super();
         if(_lock)
         {
            throw new IllegalOperationError("Singleton! use ContextMenuManager.getInstance() method.");
         }
         else
         {
            _groups["___default___"] = new ContextMenuGroup(true);
            try
            {
               _contextMenu = new ContextMenu();
               _contextMenu.hideBuiltInItems();
               _contextMenu.addEventListener("menuSelect",onMenuSelect);
            }
            catch(e:Error)
            {
               Debug.error("createContextMenu - error: " + e.toString());
               return;
            }
            return;
         }
      }
      
      public static const DEFAULT:String = "___default___";
      
      public static const IDKFA:String = "idkfa";
      
      public static const LOG:String = "log";
      
      private static var _lock:Boolean = true;
      
      private static var _instance:ContextMenuManager;
      
      public static function getInstance() : ContextMenuManager {
         if(!_instance)
         {
            _lock = false;
            _instance = new ContextMenuManager();
            _lock = true;
         }
         return _instance;
      }
      
      private var _groups:Object;
      
      private var _display:DisplayObjectContainer;
      
      private var _groupCodes:Object;
      
      private var _contextMenu:ContextMenu;
      
      public function init(param1:DisplayObjectContainer) : void {
         if(!_display)
         {
            _display = param1;
            _display.addEventListener("removedFromStage",onRemovedFromStage);
            _display.contextMenu = _contextMenu;
         }
      }
      
      public function addGroupCode(param1:String, param2:String) : void {
         if(param1 && !_groups[param1])
         {
            _groups[param1] = new ContextMenuGroup();
         }
         _groupCodes[param2] = param1;
      }
      
      public function getGroupByCode(param1:String) : String {
         return _groupCodes[param1];
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         _display.removeEventListener("removedFromStage",onRemovedFromStage);
         destroy();
      }
      
      private function onMenuSelect(param1:ContextMenuEvent) : void {
         var _loc2_:* = null;
         var _loc3_:Array = [];
         var _loc6_:* = 0;
         var _loc5_:* = _groups;
         for(_loc4_ in _groups)
         {
            _loc2_ = _groups[_loc4_] as ContextMenuGroup;
            if(_loc2_.available)
            {
               _loc3_ = _loc3_.concat(_loc2_.items);
            }
         }
         _contextMenu.customItems = _loc3_;
         dispatchEvent(param1.clone());
      }
      
      public function addItem(param1:String, param2:Function, param3:Array = null, param4:String = null, param5:String = null) : ContextMenuItem {
         if(param4)
         {
            if(!_groups[param4])
            {
               _groups[param4] = new ContextMenuGroup();
            }
         }
         else
         {
            param4 = "___default___";
         }
         var _loc6_:ContextMenuItem = (_groups[param4] as ContextMenuGroup).addItem(param1,param2,param3,param5);
         return _loc6_;
      }
      
      public function removeItem(param1:ContextMenuItem, param2:String = "___default___") : void {
         if(param2 && _groups[param2])
         {
            (_groups[param2] as ContextMenuGroup).removeItem(param1);
         }
      }
      
      public function removeItemByName(param1:String, param2:String = "___default___") : void {
         if(param2 && _groups[param2])
         {
            (_groups[param2] as ContextMenuGroup).removeItemByName(param1);
         }
      }
      
      public function getItemByName(param1:String, param2:String = "___default___") : ContextMenuItem {
         var _loc3_:* = null;
         if(param2 && _groups[param2])
         {
            _loc3_ = (_groups[param2] as ContextMenuGroup).getItemDataByName(param1);
            if(_loc3_)
            {
               return _loc3_.item;
            }
         }
         return null;
      }
      
      public function toggleGroup(param1:String) : void {
         if(param1 && _groups[param1])
         {
            (_groups[param1] as ContextMenuGroup).available = !(_groups[param1] as ContextMenuGroup).available;
         }
      }
      
      public function setGroupAvailability(param1:String, param2:Boolean) : void {
         if(param1 && _groups[param1])
         {
            (_groups[param1] as ContextMenuGroup).available = param2;
         }
      }
      
      public function groupIsAvailable(param1:String) : Boolean {
         if(param1 && _groups[param1])
         {
            return (_groups[param1] as ContextMenuGroup).available;
         }
         return false;
      }
      
      public function destroy() : void {
         _instance = null;
         _groups = null;
      }
   }
}
