package tv.ustream.tools
{
   import flash.utils.Timer;
   import flash.utils.getTimer;
   import flash.events.TimerEvent;
   
   public class ExtendedTimer extends Timer
   {
      
      public function ExtendedTimer(param1:uint, param2:uint = 0) {
         baseDelay = param1;
         lastCheckPoint = getTimer();
         lastPausePoint = getTimer();
         super(param1,param2);
         addEventListener("timer",onTimer);
      }
      
      protected var lastCheckPoint:Number;
      
      protected var lastPausePoint:Number;
      
      protected var baseDelay:uint;
      
      protected var isDelayReset:Boolean = false;
      
      override public function start() : void {
         if(isDelayReset)
         {
            lastCheckPoint = getTimer();
            isDelayReset = false;
         }
         super.start();
      }
      
      override public function reset() : void {
         lastCheckPoint = getTimer();
         lastPausePoint = getTimer();
      }
      
      private function onTimer(param1:TimerEvent) : void {
         lastPausePoint = getTimer();
         isDelayReset = true;
         this.delay = baseDelay;
      }
      
      public function pause(param1:uint = 0) : void {
         var _loc2_:* = null;
         var _loc3_:Number = getTimer() - lastPausePoint;
         this.delay = this.delay - (_loc3_ < 0?0:_loc3_);
         stop();
         if(param1)
         {
            _loc2_ = new Timer(param1,1);
            _loc2_.addEventListener("timerComplete",onPauseTimer);
            _loc2_.start();
         }
      }
      
      private function onPauseTimer(param1:TimerEvent) : void {
         param1.currentTarget.removeEventListener("timerComplete",onPauseTimer);
         resume();
      }
      
      public function resume() : void {
         lastPausePoint = getTimer();
         super.start();
      }
   }
}
