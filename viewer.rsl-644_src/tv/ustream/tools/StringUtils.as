package tv.ustream.tools
{
   public class StringUtils extends Object
   {
      
      public function StringUtils() {
         super();
      }
      
      private static const regs:Array = [new RegExp("\\%year\\%","gi"),new RegExp("\\%YEAR\\%","gi"),new RegExp("\\%month\\%","g"),new RegExp("\\%MONTH\\%","g"),new RegExp("\\%day\\%","g"),new RegExp("\\%DAY\\%","g"),new RegExp("\\%hour\\%","g"),new RegExp("\\%HOUR\\%","g"),new RegExp("\\%minutes\\%","g"),new RegExp("\\%MINUTES\\%","g"),new RegExp("\\%seconds\\%","g"),new RegExp("\\%SECONDS\\%","g"),new RegExp("\\%milliseconds\\%","g"),new RegExp("\\%MILLISECONDS\\%","g")];
      
      private static const dateFunc:Array = ["getFullYear","getMonth","getDate","getHours","getMinutes","getSeconds","getMilliseconds"];
      
      public static function leadingZero(param1:*, param2:Number = 2) : String {
         return multiply("0",param2 - (param1).length) + param1;
      }
      
      public static function multiply(param1:String, param2:int) : String {
         var _loc4_:* = NaN;
         var _loc3_:String = "";
         _loc4_ = 0.0;
         while(_loc4_ < param2)
         {
            _loc3_ = _loc3_ + param1;
            _loc4_++;
         }
         return _loc3_;
      }
      
      public static function getFormatDateString(param1:String, param2:Date = null, param3:Boolean = true) : String {
         var _loc7_:* = 0;
         var _loc5_:* = null;
         var _loc4_:Date = param2?param2:new Date();
         var _loc6_:* = 0;
         _loc7_ = 0;
         while(_loc7_ < regs.length)
         {
            _loc5_ = dateFunc[_loc7_ / 2];
            _loc6_ = _loc4_[_loc5_]();
            if(_loc7_ == 2 || _loc7_ == 3)
            {
               _loc6_++;
            }
            if(!param3 && _loc5_ == "getHours")
            {
               _loc6_ = _loc6_ % 12;
               if(!_loc6_)
               {
                  _loc6_ = 12;
               }
            }
            param1 = param1.replace(regs[_loc7_],_loc7_ % 2 > 0?_loc7_ > 1?leadingZero(_loc6_,_loc7_ > 11?3:2.0):_loc6_:_loc6_);
            _loc7_++;
         }
         var param1:String = param1.replace(new RegExp("\\%AMPM\\%","g"),_loc4_.getHours() < 12?"am":"pm");
         return param1;
      }
      
      public static function getDiffDate(param1:Date, param2:Boolean = false) : Array {
         var _loc5_:* = 0;
         var _loc4_:Array = new Array(31104000,2592000,86400,3600,60);
         var _loc3_:Array = [];
         var _loc7_:Date = new Date();
         if(param2)
         {
            param1.hours = param1.hours + (11 + param1.getTimezoneOffset() / 60);
         }
         var _loc6_:Number = Math.floor(Math.abs(_loc7_.time - param1.time) / 1000);
         _loc5_ = 0;
         while(_loc5_ < _loc4_.length)
         {
            _loc3_[_loc5_] = Math.floor(_loc6_ / _loc4_[_loc5_]);
            _loc6_ = _loc6_ - _loc3_[_loc5_] * _loc4_[_loc5_];
            _loc5_++;
         }
         _loc3_[_loc4_.length] = _loc6_;
         return _loc3_;
      }
      
      public static function getDiffDateText(param1:Date, param2:Boolean = false) : String {
         var _loc4_:* = 0;
         var _loc3_:String = "";
         var _loc6_:Array = getDiffDate(param1,param2);
         var _loc5_:Array = ["","week","day","hour","minute","second"];
         _loc4_ = 0;
         while(_loc4_ < _loc6_.length)
         {
            if(_loc6_[_loc4_] > 0)
            {
               if(_loc3_.length)
               {
                  _loc3_ = _loc3_ + ", ";
               }
               _loc3_ = _loc3_ + (_loc6_[_loc4_] + " " + _loc5_[_loc4_] + (_loc6_[_loc4_] > 1?"s":""));
            }
            _loc4_++;
         }
         return _loc3_;
      }
      
      public static function random(param1:uint, param2:Boolean = true, param3:Boolean = false, param4:Boolean = true) : String {
         var _loc6_:* = 0;
         var _loc7_:String = "";
         var _loc5_:String = "";
         if(param2)
         {
            _loc7_ = _loc7_ + "abcdefghijklmnopqrstuvwxyz";
         }
         if(param3)
         {
            _loc7_ = _loc7_ + "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         }
         if(param4)
         {
            _loc7_ = _loc7_ + "0123456789";
         }
         if(_loc7_.length)
         {
            _loc6_ = 0;
            while(_loc6_ < param1)
            {
               _loc5_ = _loc5_ + _loc7_.substr(Math.floor(_loc7_.length * Math.random()),1);
               _loc6_++;
            }
         }
         return _loc5_;
      }
      
      public static function thousands(param1:Number, param2:String = ",", param3:String = ".") : String {
         var _loc5_:* = 0;
         var _loc4_:String = "";
         var _loc6_:String = Math.floor(param1);
         _loc5_ = _loc6_.length - 1;
         while(_loc5_ >= 0)
         {
            _loc4_ = _loc6_.substr(_loc5_,1) + _loc4_;
            if(_loc5_ > 0 && _loc4_.length > 1 && (_loc6_.length - 1 - _loc5_) % 3 == 2)
            {
               _loc4_ = param2 + _loc4_;
            }
            _loc5_--;
         }
         if(param1 != Math.floor(param1))
         {
            _loc4_ = _loc4_ + (param3 + (param1 - Math.floor(param1)).substr(2));
         }
         return _loc4_;
      }
   }
}
