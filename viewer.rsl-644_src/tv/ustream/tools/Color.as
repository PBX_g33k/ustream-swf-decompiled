package tv.ustream.tools
{
   public class Color extends Object
   {
      
      public function Color() {
         super();
      }
      
      public static function random(param1:int = -1, param2:int = 1, param3:int = 0, param4:int = 255) : Number {
         if(param1 == -1)
         {
            param1 = Math.random() * 3;
         }
         var _loc6_:Number = (255 - param3) / param2 << param1 * 8;
         var _loc5_:* = 3;
         while(true)
         {
            _loc5_--;
            if(!_loc5_)
            {
               break;
            }
            if(param1 != _loc5_)
            {
               _loc6_ = _loc6_ + ((255 - param3 - (param4 - param3) * Math.random()) / param2 << _loc5_ * 8);
            }
         }
         return _loc6_;
      }
   }
}
