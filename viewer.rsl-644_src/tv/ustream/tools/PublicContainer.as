package tv.ustream.tools
{
   import flash.display.Sprite;
   import flash.utils.getQualifiedClassName;
   import flash.system.ApplicationDomain;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   
   public class PublicContainer extends Sprite
   {
      
      public function PublicContainer() {
         allowChildClasses = ["tv.ustream.viewer.document.ui::StreamOverlay"];
         super();
      }
      
      private var allowChildClasses:Array;
      
      private function validateClass(param1:Object) : Boolean {
         var _loc2_:String = getQualifiedClassName(param1);
         return ApplicationDomain.currentDomain.hasDefinition(_loc2_) && !(allowChildClasses.indexOf(_loc2_) == -1);
      }
      
      override public function addChild(param1:DisplayObject) : DisplayObject {
         return validateClass(param1)?super.addChild(param1):null;
      }
      
      override public function addChildAt(param1:DisplayObject, param2:int) : DisplayObject {
         return validateClass(param1)?super.addChildAt(param1,param2):null;
      }
      
      override public function get parent() : DisplayObjectContainer {
         return null;
      }
   }
}
