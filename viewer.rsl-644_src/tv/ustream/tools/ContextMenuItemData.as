package tv.ustream.tools
{
   import flash.ui.ContextMenuItem;
   import flash.events.ContextMenuEvent;
   
   class ContextMenuItemData extends Object
   {
      
      function ContextMenuItemData(param1:String, param2:Function, param3:Array = null) {
         super();
         _callbackParams = param3;
         _callBackFunction = param2;
         _item = new ContextMenuItem(param1);
         _item.addEventListener("menuItemSelect",onItemSelect);
      }
      
      private var _callBackFunction:Function;
      
      private var _item:ContextMenuItem;
      
      private var _callbackParams:Object;
      
      public function get item() : ContextMenuItem {
         return _item;
      }
      
      public function onItemSelect(param1:ContextMenuEvent) : void {
         if(_callBackFunction !== null)
         {
            if(_callbackParams === null)
            {
               _callBackFunction.apply();
            }
            else
            {
               _callBackFunction.apply(this,_callbackParams);
            }
         }
      }
   }
}
