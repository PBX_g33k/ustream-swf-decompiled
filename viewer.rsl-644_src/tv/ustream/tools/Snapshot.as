package tv.ustream.tools
{
   import flash.utils.Dictionary;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import flash.display.BitmapData;
   import flash.system.Security;
   import com.hurlant.util.Base64;
   import flash.utils.ByteArray;
   import flash.display.Stage;
   import flash.net.FileReference;
   
   public class Snapshot extends Object
   {
      
      public function Snapshot() {
         super();
      }
      
      public static const FORMAT_PNG:String = "image.png";
      
      public static const FORMAT_JPG:String = "image.jpg";
      
      private static const SECURITY_ERROR:String = "security_error";
      
      private static var callbackFunctions:Dictionary = new Dictionary();
      
      private static var errors:uint = 0;
      
      private static function getBitmapData(param1:VideoContainer, param2:Function) : void {
         video = param1;
         callbackFunction = param2;
         trace("4:Snapshot.getBitmapData stagevideo: " + !video.stageVideoDisabled);
         Debug.echo("[ Snapshot ] getBitmapData stagevideo: " + !video.stageVideoDisabled);
         if(video.stage == null)
         {
            return;
         }
         video.visible = false;
         if(!video.stageVideoDisabled)
         {
            video.stageVideoDisabled = true;
         }
         video.scrollRect = null;
         video.resize(video.videoWidth,video.videoHeight);
         var t:Timer = new Timer(2500,1);
         t.addEventListener("timerComplete",function(param1:TimerEvent):void
         {
            var _loc2_:Timer = param1.target as Timer;
            _loc2_.stop();
            trace("4:Snapshot.getBitmapData.timer callback");
            video.resize(video.videoWidth,video.videoHeight);
            callbackFunction.apply(null,[createBitmapData(video)]);
            video.stage.dispatchEvent(new Event("resize"));
         });
         t.start();
      }
      
      private static function createBitmapData(param1:VideoContainer) : Object {
         var _loc3_:* = null;
         var _loc4_:* = null;
         var _loc2_:* = null;
         Debug.echo("[ Snapshot ] createBitmapData video: " + param1.videoWidth + "x" + param1.videoHeight);
         trace("4:Snapshot.createBitmapData video: " + param1,param1.videoWidth,param1.videoHeight,param1.root);
         try
         {
            if(param1.videoHeight && (param1.videoWidth))
            {
               _loc3_ = new BitmapData(param1.videoWidth,param1.videoHeight,false,4.27819008E9);
               _loc3_.draw(param1);
            }
            else
            {
               Debug.echo("[ Snapshot ] invalid dimensions");
            }
            param1.visible = true;
         }
         catch(e:Error)
         {
            Debug.echo("[ Snapshot ] ERROR : " + e);
            if(e.message.indexOf("cannot access") > -1 && e.message.indexOf("http://") > -1)
            {
               _loc4_ = "http://";
               _loc2_ = e.message.split("http://");
               if(_loc2_.length > 2)
               {
                  _loc4_ = _loc4_ + (_loc2_[2].split("/")[0] + "/crossdomain.xml");
               }
               Security.loadPolicyFile(_loc4_);
               param1.visible = true;
               _loc6_ = "security_error";
               return _loc6_;
            }
         }
         param1.visible = true;
         return _loc3_;
      }
      
      public static function getBase64(param1:VideoContainer, param2:Function, param3:String = "image.png") : void {
         video = param1;
         callbackFunction = param2;
         format = param3;
         getBitmapData(video,function(param1:Object):void
         {
            bitmapData = param1;
            if(bitmapData is BitmapData)
            {
               errors = 0;
               _loc3_ = format;
               if("image.png" !== _loc3_)
               {
                  if("image.jpg" === _loc3_)
                  {
                     callbackFunction.apply(null,[Base64.encodeByteArray(jpeg(BitmapData(bitmapData),video.stage))]);
                  }
               }
               else
               {
                  img = Base64.encodeByteArray(png(BitmapData(bitmapData),video.stage));
                  trace("4:imgstring: " + img.length);
                  callbackFunction.apply(null,[img]);
               }
            }
            else if(bitmapData == "security_error")
            {
               if(errors < 4)
               {
                  errors = errors + 1;
                  t = new Timer(1200,1);
                  t.addEventListener("timerComplete",function(param1:TimerEvent):void
                  {
                     var _loc2_:Timer = param1.target as Timer;
                     _loc2_.stop();
                     getBase64(video,callbackFunction,format);
                  });
                  t.start();
               }
               else
               {
                  Debug.echo("Security Sandbox Error!");
                  callbackFunction.apply(null,[""]);
               }
            }
            else
            {
               Debug.echo("the ByteArray is empty");
               callbackFunction.apply(null,[""]);
            }
            
         });
      }
      
      private static function jpeg(param1:BitmapData, param2:Stage) : ByteArray {
         var _loc4_:* = null;
         var _loc3_:ByteArray = new ByteArray();
         try
         {
            _loc4_ = param2.loaderInfo.applicationDomain.getDefinition("flash.display::JPEGEncoderOptions");
            if(param1["encode"] && _loc4_)
            {
               param1["encode"](param1.rect,new _loc4_(80),_loc3_);
               Debug.echo("[ Snapshot ] jpeg byteArray: " + _loc3_.length);
            }
            else
            {
               Debug.echo("feature not available");
            }
         }
         catch(e:Error)
         {
            Debug.echo("[ Snapshot ] ERROR :" + e);
         }
         return _loc3_;
      }
      
      private static function png(param1:BitmapData, param2:Stage) : ByteArray {
         var _loc4_:* = null;
         var _loc3_:ByteArray = new ByteArray();
         try
         {
            _loc4_ = param2.loaderInfo.applicationDomain.getDefinition("flash.display::PNGEncoderOptions");
            if(param1["encode"] && _loc4_)
            {
               param1["encode"](param1.rect,new _loc4_(),_loc3_);
               Debug.echo("[ Snapshot ] png byteArray: " + _loc3_.length);
            }
            else
            {
               Debug.echo("feature not available");
            }
         }
         catch(e:Error)
         {
            Debug.echo("[ Snapshot ] ERROR : " + e);
         }
         return _loc3_;
      }
      
      public static function create(param1:VideoContainer, param2:String) : void {
         video = param1;
         fileName = param2;
         getBitmapData(video,function(param1:BitmapData):void
         {
            Debug.echo("[ Snapshot ] create.callback: " + param1);
            if(!param1)
            {
               Debug.echo("the ByteArray is empty");
            }
         });
      }
      
      private static function saveImage(param1:String, param2:ByteArray) : void {
         trace("4:Snapshot.saveImage: ",param1,param2.length);
         var _loc3_:FileReference = new FileReference();
         _loc3_.addEventListener("complete",onFsComplete);
         var param1:String = param1 + ("." + StringUtils.getFormatDateString("%YEAR%-%MONTH%-%DAY%.%HOUR%h%MINUTES%m%SECONDS%s") + ".jpg");
         Debug.echo("save " + param1);
         _loc3_.save(param2,param1);
      }
      
      private static function onFsComplete(param1:Event) : void {
         param1.target.removeEventListener("complete",onFsComplete);
         Debug.echo("saved");
      }
   }
}
