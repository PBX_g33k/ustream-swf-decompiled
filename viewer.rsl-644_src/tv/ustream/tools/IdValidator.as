package tv.ustream.tools
{
   public class IdValidator extends Dispatcher
   {
      
      public function IdValidator() {
         super();
      }
      
      public static function validateBrandId(param1:String) : Boolean {
         if(param1.indexOf(" ") != -1)
         {
            return false;
         }
         if(isNaN(param1))
         {
            return false;
         }
         return param1?true:false;
      }
      
      public static function validateMediaId(param1:String) : Boolean {
         if(param1.length == 0 || !(param1.indexOf(" ") == -1))
         {
            return false;
         }
         var _loc2_:RegExp = new RegExp("^[a-zA-Z0-9]+([a-zA-Z0-9-]+[a-zA-Z0-9])?$");
         return _loc2_.test(param1) && param1.length <= 30;
      }
      
      private var _mediaId:String;
      
      private var _brandId:String;
      
      private var _hostBrandId:String;
      
      public function parseId(param1:String) : Boolean {
         var _loc2_:* = null;
         if(param1)
         {
            _loc2_ = param1.split("/");
            _hostBrandId = null;
            _brandId = "1";
            _mediaId = null;
            if(_loc2_.length == 3 && !validateBrandId(_loc2_.shift()))
            {
               dispatchError("invalid hostBrandId : " + _hostBrandId);
               return false;
            }
            if(_loc2_.length == 2 && !validateBrandId(_loc2_.shift()))
            {
               dispatchError("invalid brandId : " + _brandId);
               return false;
            }
            if(!isNaN(_brandId) && (_brandId) > 2147483647)
            {
               dispatchError("invalid brandId(long) : " + _brandId);
               return false;
            }
            _mediaId = _loc2_[0];
            if(!validateMediaId(_loc2_[0]))
            {
               dispatchError("invalid mediaId : " + _mediaId);
               return false;
            }
            if(_brandId == "1" && (isNaN(_mediaId) || _mediaId == "0"))
            {
               dispatchError("invalid mediaId : " + _mediaId);
               return false;
            }
            if(_brandId == "1" && (!isNaN(_mediaId) && (_mediaId) > 2147483647))
            {
               dispatchError("invalid mediaId(long) : " + _mediaId);
               return false;
            }
            return true;
         }
         return false;
      }
      
      private function dispatchError(param1:String) : void {
         throw new IdError(param1);
      }
      
      public function get mediaId() : String {
         return _mediaId;
      }
      
      public function get brandId() : String {
         return _brandId;
      }
      
      public function get hostBrandId() : String {
         return _hostBrandId;
      }
   }
}
class IdError extends Error
{
   
   function IdError(param1:String, param2:int = 0) {
      super(param1,param2);
      name = "IdError";
   }
}
