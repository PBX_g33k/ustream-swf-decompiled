package tv.ustream.tools
{
   import flash.events.Event;
   
   public dynamic class DynamicEvent extends Event
   {
      
      public function DynamicEvent(param1:String, param2:Boolean = false, param3:Boolean = false, param4:Object = null) {
         this.data = param4;
         if(this.data)
         {
            _loc7_ = 0;
            _loc6_ = param4;
            for(_loc5_ in param4)
            {
               this[_loc5_] = param4[_loc5_];
            }
         }
         super(param1,param2,param3);
      }
      
      override public function clone() : Event {
         return new DynamicEvent(type,bubbles,cancelable,this.data);
      }
   }
}
