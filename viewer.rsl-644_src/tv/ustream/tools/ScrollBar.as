package tv.ustream.tools
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.filters.DropShadowFilter;
   
   public dynamic class ScrollBar extends MovieClip
   {
      
      public function ScrollBar(param1:Sprite = null) {
         super();
         doubleClickEnabled = true;
         if(this._grab)
         {
            grab = this._grab;
         }
         else if(param1)
         {
            grab = param1;
         }
         else
         {
            grab = new Sprite();
            draw(new Sprite());
            grab.filters = [new DropShadowFilter(0,0,0,1)];
            grab.buttonMode = true;
         }
         
         track = new Sprite();
         draw(new Sprite()).visible = false;
         drag = new Drag(grab,grab);
         drag.top = 0;
         drag.bottom = 0;
         drag.left = 0;
         drag.right = 0;
         drag.addEventListener("drag",onDrag);
         drag.addEventListener("startDrag",onStartDrag);
         drag.addEventListener("stopDrag",onStopDrag);
      }
      
      private var track:Sprite;
      
      private var grab:Sprite;
      
      private var _xPercent:Number = 0;
      
      private var _yPercent:Number = 0;
      
      private var _grabXpercent:Number = 1;
      
      private var _grabYpercent:Number = 1;
      
      private var _trackOnly:Boolean;
      
      private var _enableXScroll:Boolean;
      
      private var _enableYScroll:Boolean;
      
      public var drag:Drag;
      
      public var lockDrag:Boolean = false;
      
      private function onDrag(... rest) : void {
         xPercent = grab.x / (track.width - grab.width);
         yPercent = grab.y / (track.height - grab.height);
         dispatchEvent(new DynamicEvent("drag",false,false,
            {
               "xPercent":xPercent,
               "yPercent":yPercent
            }));
      }
      
      private function onStartDrag(... rest) : void {
         dispatchEvent(new Event("startdrag",true));
      }
      
      private function onStopDrag(... rest) : void {
         dispatchEvent(new Event("stopdrag",true));
      }
      
      private function draw(param1:Sprite) : Sprite {
         param1.graphics.clear();
         param1.graphics.beginFill(16777215);
         param1.graphics.drawRect(0,0,14,14);
         addChild(param1);
         return param1;
      }
      
      public function get trackOnly() : Boolean {
         return _trackOnly;
      }
      
      public function set trackOnly(param1:Boolean) : void {
         _trackOnly = param1;
         if(_trackOnly)
         {
            drag.trackOnly = param1;
            grab.width = 0;
            width = track.width;
         }
      }
      
      override public function get width() : Number {
         return track.width;
      }
      
      override public function set width(param1:Number) : void {
         track.width = param1;
         if(!lockDrag)
         {
            grab.width = Math.max(param1 * _grabXpercent,10);
         }
         drag.right = param1 - grab.width;
         grab.x = _xPercent * drag.right;
      }
      
      override public function get height() : Number {
         return track.height;
      }
      
      override public function set height(param1:Number) : void {
         track.height = param1;
         if(!lockDrag)
         {
            grab.height = Math.max(param1 * _grabYpercent,10);
         }
         drag.bottom = param1 - grab.height;
         grab.y = _yPercent * drag.bottom;
      }
      
      public function get xPercent() : Number {
         return _xPercent;
      }
      
      public function set xPercent(param1:Number) : void {
         if(param1.toString() != "NaN")
         {
            param1 = Math.min(Math.max(param1,0),1);
            grab.x = param1 * drag.right;
            _xPercent = param1;
            dispatchEvent(new DynamicEvent("xPercent",false,false,{"percent":param1}));
         }
      }
      
      public function get yPercent() : Number {
         return _yPercent;
      }
      
      public function set yPercent(param1:Number) : void {
         if(param1.toString() != "NaN")
         {
            param1 = Math.min(Math.max(param1,0),1);
            grab.y = param1 * drag.bottom;
            _yPercent = param1;
            dispatchEvent(new DynamicEvent("yPercent",false,false,{"percent":param1}));
         }
      }
      
      public function get grabXpercent() : Number {
         return _grabXpercent;
      }
      
      public function set grabXpercent(param1:Number) : void {
         _grabXpercent = param1;
         if(!lockDrag)
         {
            grab.width = Math.max(track.width * param1,10);
         }
         width = track.width;
      }
      
      public function get grabYpercent() : Number {
         return _grabYpercent;
      }
      
      public function set grabYpercent(param1:Number) : void {
         _grabYpercent = param1;
         if(!lockDrag)
         {
            grab.height = Math.max(track.height * param1,10);
         }
         height = track.height;
      }
      
      public function get dragEnabled() : Boolean {
         return drag.mouseEnabled;
      }
      
      public function set dragEnabled(param1:Boolean) : void {
         drag.mouseEnabled = param1;
      }
      
      public function get enableXScroll() : Boolean {
         return _enableXScroll;
      }
      
      public function set enableXScroll(param1:Boolean) : void {
         var _loc2_:* = param1;
         drag.enableXScroll = _loc2_;
         _enableXScroll = _loc2_;
      }
      
      public function get enableYScroll() : Boolean {
         return _enableYScroll;
      }
      
      public function set enableYScroll(param1:Boolean) : void {
         var _loc2_:* = param1;
         drag.enableYScroll = _loc2_;
         _enableYScroll = _loc2_;
      }
   }
}
