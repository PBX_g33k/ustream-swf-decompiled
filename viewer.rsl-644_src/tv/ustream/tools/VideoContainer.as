package tv.ustream.tools
{
   import flash.net.NetStream;
   import flash.geom.Rectangle;
   import flash.display.Sprite;
   import tv.ustream.cc.ICCDecoder;
   import flash.events.Event;
   import flash.media.Video;
   import flash.display.DisplayObject;
   import flash.media.Camera;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   
   public class VideoContainer extends Container
   {
      
      public function VideoContainer(param1:NetStream = null) {
         rect = new Rectangle();
         super();
         if(!stageVideoUsage)
         {
            stageVideoUsage = [];
         }
         this.stream = param1;
         placeHolder = new Sprite();
         addEventListener("addedToStage",onAddedToStage);
      }
      
      private static var stageVideoUsage:Array;
      
      public static const GET_SIZE:String = "getSize";
      
      public static const RENDERER_CHANGE:String = "rendererChange";
      
      public static const CC_DETECTED:String = "ccDetected";
      
      private var _stageVideoAvailable:Boolean = false;
      
      private var _stageVideoDisabled:Boolean = true;
      
      private var _stageVideoIncompatible:Boolean = false;
      
      private var video;
      
      private var _x:Number = 0;
      
      private var _y:Number = 0;
      
      private var _stream:NetStream;
      
      private var rect:Rectangle;
      
      private var placeHolder:Sprite;
      
      private var stageVideoIndex:int = -1;
      
      private var _videoWidth:Number;
      
      private var _videoHeight:Number;
      
      private var _ccDecoder:ICCDecoder;
      
      private var _ccVisible:Boolean = false;
      
      private var _ccEnabled:Boolean = true;
      
      private var loadingCcDecoder:Boolean = false;
      
      private var _adOffset:uint = 0;
      
      public function destroy() : void {
         echo("destroy");
         if(parent)
         {
            parent.removeChild(this);
         }
         if(video)
         {
            attachNetStream(null);
         }
         if(stageVideoIndex != -1)
         {
            stageVideoUsage[stageVideoIndex] = false;
         }
         placeHolder.removeEventListener("enterFrame",onEnterFrame);
      }
      
      private function onAddedToStage(... rest) : void {
         echo("onAddedToStage");
         removeEventListener("addedToStage",onAddedToStage);
         addEventListener("removedFromStage",onRemovedFromStage);
         placeHolder.addEventListener("enterFrame",onEnterFrame);
         setVideo();
         stage.addEventListener("stageVideoAvailability",onStageVideoAvailability);
      }
      
      private function onRemovedFromStage(param1:Event) : void {
         echo("onRemovedFromStage");
         placeHolder.removeEventListener("enterFrame",onEnterFrame);
         stage.removeEventListener("stageVideoAvailability",onStageVideoAvailability);
         removeEventListener("removedFromStage",onRemovedFromStage);
         addEventListener("addedToStage",onAddedToStage);
      }
      
      private function onStageVideoAvailability(param1:*) : void {
         echo("onStageVideoAvailability : " + param1.availability);
         stageVideoAvailable = param1.availability == "available";
      }
      
      private function onEnterFrame(... rest) : void {
         if(videoWidth && (videoHeight) && (!(_videoWidth == videoWidth) || !(_videoHeight == videoHeight)))
         {
            echo("getSize");
            dispatchEvent(new Event("getSize"));
            _videoWidth = videoWidth;
            _videoHeight = videoHeight;
         }
         updateStageVideo();
      }
      
      private function updateStageVideo() : void {
         var _loc1_:* = null;
         var _loc2_:* = null;
         if(!(video is Video))
         {
            _loc1_ = video.viewPort;
            _loc2_ = ancestorsVisible?placeHolder.getBounds(stage):new Rectangle(0,0,0,0);
            if(!(_loc1_.x == _loc2_.x) || !(_loc1_.y == _loc2_.y) || !(_loc1_.width == _loc2_.width) || !(_loc1_.height == _loc2_.height))
            {
               echo("placeRect : " + _loc2_.toString());
               video.viewPort = _loc2_;
            }
         }
      }
      
      private function get ancestorsVisible() : Boolean {
         var _loc2_:* = null;
         try
         {
            _loc2_ = parent;
         }
         catch(err:Error)
         {
            _loc4_ = true;
            return _loc4_;
         }
         var _loc1_:Boolean = this.visible && stage;
         while(_loc1_ && _loc2_)
         {
            _loc1_ = _loc1_ && (_loc2_.visible);
            try
            {
               _loc2_ = _loc2_.parent;
            }
            catch(err:Error)
            {
               _loc2_ = null;
               continue;
            }
         }
         return _loc1_;
      }
      
      private function onStageVideoRenderState(... rest) : void {
         resize(_width,_height);
      }
      
      public function attachNetStream(param1:NetStream) : void {
         _stageVideoIncompatible = false;
         setVideo();
         this.stream = param1;
         _videoHeight = 0;
         _videoWidth = 0;
         if(video)
         {
            video.attachNetStream(param1);
         }
      }
      
      public function attachCamera(param1:Camera) : void {
         _stageVideoIncompatible = true;
         setVideo();
         (video as Video).attachCamera(param1);
      }
      
      public function clear() : void {
         if(video is Video)
         {
            video.clear();
         }
      }
      
      public function setRect(param1:Rectangle) : void {
         rect = param1;
         _width = Math.floor(rect.width);
         _height = Math.floor(rect.height);
         placeHolder.graphics.clear();
         placeHolder.graphics.beginFill(16711680,0);
         placeHolder.graphics.drawRect(0,0,_width,_height);
         placeHolder.graphics.endFill();
         _x = rect.x;
         .super.x = rect.x;
         _y = rect.y;
         .super.y = rect.y;
         if(video)
         {
            if(video is Video)
            {
               video.width = _width;
               video.height = _height;
            }
            else
            {
               updateStageVideo();
            }
         }
         if(_ccDecoder)
         {
            _ccDecoder.setVideoBounds(new Rectangle(rect.x,rect.y,rect.width,rect.height - _adOffset));
         }
      }
      
      override public function resize(param1:Number, param2:Number) : void {
         echo("resize " + param1 + " " + param2);
         rect.width = param1;
         rect.height = param2;
         setRect(rect);
      }
      
      override public function get x() : Number {
         return rect.x;
      }
      
      override public function set x(param1:Number) : void {
         rect.x = param1;
         setRect(rect);
      }
      
      override public function get y() : Number {
         return rect.y;
      }
      
      override public function set y(param1:Number) : void {
         rect.y = param1;
         setRect(rect);
      }
      
      override public function get width() : Number {
         return rect.width;
      }
      
      override public function set width(param1:Number) : void {
         rect.width = param1;
         setRect(rect);
      }
      
      override public function get height() : Number {
         return rect.height;
      }
      
      override public function set height(param1:Number) : void {
         rect.height = param1;
         setRect(rect);
      }
      
      override public function set visible(param1:Boolean) : void {
         .super.visible = param1;
         updateStageVideo();
      }
      
      public function set adOffset(param1:uint) : void {
         if(param1 > 70)
         {
            param1 = 70;
         }
         echo("set adOffset: " + param1);
         _adOffset = param1;
         if(_ccDecoder)
         {
            _ccDecoder.setVideoBounds(new Rectangle(rect.x,rect.y,rect.width,rect.height - _adOffset));
         }
      }
      
      public function get videoWidth() : Number {
         return video?video.videoWidth:0;
      }
      
      public function get videoHeight() : Number {
         return video?video.videoHeight:0;
      }
      
      public function get smoothing() : Boolean {
         return video is Video?video.smoothing:false;
      }
      
      public function set smoothing(param1:Boolean) : void {
         if(video is Video)
         {
            video.smoothing = param1;
         }
      }
      
      public function get stageVideo() : Boolean {
         return !(video is Video);
      }
      
      private function get stageVideoAvailable() : Boolean {
         return _stageVideoAvailable;
      }
      
      private function set stageVideoAvailable(param1:Boolean) : void {
         if(_stageVideoAvailable != param1)
         {
            _stageVideoAvailable = param1;
            setVideo();
         }
      }
      
      public function get stageVideoDisabled() : Boolean {
         return _stageVideoDisabled;
      }
      
      public function set stageVideoDisabled(param1:Boolean) : void {
         if(_stageVideoDisabled != param1)
         {
            _stageVideoDisabled = param1;
            setVideo();
         }
      }
      
      protected function get stream() : NetStream {
         return _stream;
      }
      
      protected function set stream(param1:NetStream) : void {
         if(_stream)
         {
            if(_stream.client && _stream.client.onCaptionInfo)
            {
               delete _stream.client.onCaptonInfo;
            }
         }
         _stream = param1;
         if(_stream)
         {
            Debug.echo("### client : " + _stream.client);
            if(!_stream.client)
            {
               _stream.client = {};
            }
            if(!_stream.client.onCaptionInfo)
            {
               _stream.client.onCaptionInfo = onCaptionInfo;
               Debug.echo("### _stream.client.onCaptionInfo : " + _stream.client.onCaptionInfo);
            }
            else
            {
               throw new Error("stream client already has onCaptionInfo");
            }
         }
      }
      
      private function setVideo() : void {
         var _loc3_:* = 0;
         var _loc1_:Boolean = _stageVideoAvailable && !_stageVideoDisabled && !_stageVideoIncompatible;
         var _loc2_:* = !(video is Video);
         if(!video || !(_loc1_ == _loc2_))
         {
            if(video)
            {
               dispatchEvent(new Event("rendererChange"));
               if(video is Video && (contains(video)))
               {
                  removeChild(video);
               }
               video.removeEventListener("renderState",onStageVideoRenderState);
            }
            if(stageVideoIndex != -1)
            {
               stageVideoUsage[stageVideoIndex] = false;
            }
            stageVideoIndex = -1;
            if(_loc1_)
            {
               _loc3_ = 0;
               while(_loc3_ < stage["stageVideos"].length)
               {
                  if(!stageVideoUsage[_loc3_])
                  {
                     stageVideoIndex = _loc3_;
                     stageVideoUsage[stageVideoIndex] = true;
                     break;
                  }
                  _loc3_++;
               }
            }
            video = _loc1_ && !(stageVideoIndex == -1)?stage["stageVideos"][stageVideoIndex]:addChild(new Video());
            if(_loc1_)
            {
               addChild(placeHolder);
            }
            else if(contains(placeHolder))
            {
               removeChild(placeHolder);
            }
            
            if(_ccDecoder)
            {
               addChild(_ccDecoder.display);
            }
            if(video is Video)
            {
               video.smoothing = true;
            }
            else
            {
               video.addEventListener("renderState",onStageVideoRenderState);
            }
            echo("video : " + video + " ( stageVideoIndex : " + stageVideoIndex + " )");
            if(stream)
            {
               video.attachNetStream(stream);
            }
         }
      }
      
      private function echo(param1:String) : void {
         Debug.echo("[ VIDEOCONTAINER ] " + param1);
      }
      
      private function onCaptionInfo(param1:Object = null) : void {
         if(_ccEnabled)
         {
            if(_ccDecoder)
            {
               _ccDecoder.captionInfo(param1);
            }
            else if(!loadingCcDecoder)
            {
               setupCCDecoder();
               dispatchEvent(new Event("ccDetected"));
            }
            
         }
      }
      
      private function setupCCDecoder() : void {
         var _loc2_:* = null;
         var _loc1_:* = null;
         if(ApplicationDomain.currentDomain.hasDefinition("tv.ustream.cc::CCDecoder"))
         {
            initCCDecoder();
         }
         else
         {
            _loc2_ = "viewer.rslcc.swf";
            if(Security.sandboxType == "localTrusted")
            {
               _loc2_ = "../../../api/bin/" + _loc2_;
            }
            _loc1_ = LibLoader.instance.load(_loc2_,3,true);
            LibLoader.instance.addEventListener("complete:" + _loc1_,initCCDecoder);
            loadingCcDecoder = true;
         }
      }
      
      private function initCCDecoder(param1:Event = null) : void {
         echo("ccDecoder loaded");
         var _loc2_:Class = ApplicationDomain.currentDomain.getDefinition("tv.ustream.cc::CCDecoder") as Class;
         if(_loc2_)
         {
            echo("new ccDecoder");
            _ccDecoder = new _loc2_();
            _ccDecoder.enabled = _ccVisible;
            _ccDecoder.setVideoBounds(rect);
            addChild(_ccDecoder.display);
            loadingCcDecoder = false;
         }
      }
      
      public function get ccEnabled() : Boolean {
         return _ccEnabled;
      }
      
      public function set ccEnabled(param1:Boolean) : void {
         _ccEnabled = param1;
         if(!param1 && _ccDecoder && _ccDecoder.display && _ccDecoder.display.stage)
         {
            removeChild(_ccDecoder.display);
         }
         _ccDecoder = null;
      }
      
      public function get ccVisible() : Boolean {
         return _ccVisible;
      }
      
      public function set ccVisible(param1:Boolean) : void {
         _ccVisible = param1;
         if(_ccDecoder)
         {
            _ccDecoder.enabled = _ccVisible;
         }
      }
   }
}
