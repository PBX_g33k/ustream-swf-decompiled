package tv.ustream.tools
{
   import flash.display.Sprite;
   import flash.display.DisplayObject;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.events.Event;
   
   public final class Locker extends Sprite
   {
      
      public function Locker() {
         super();
      }
      
      private var _locked:Boolean = false;
      
      private var _scalable:Boolean = true;
      
      private var _width:Number = 320;
      
      private var _height:Number = 180;
      
      private var _scrollRectExcludeList:Array;
      
      public function close() : void {
         _locked = true;
      }
      
      public function get locked() : Boolean {
         return _locked;
      }
      
      override public function get numChildren() : int {
         return _locked?0:super.numChildren;
      }
      
      override public function addChild(param1:DisplayObject) : DisplayObject {
         return _locked?null:super.addChild(param1);
      }
      
      override public function addChildAt(param1:DisplayObject, param2:int) : DisplayObject {
         return _locked?null:super.addChildAt(param1,param2);
      }
      
      override public function getChildAt(param1:int) : DisplayObject {
         return _locked?null:super.getChildAt(param1);
      }
      
      override public function getChildByName(param1:String) : DisplayObject {
         return _locked?null:super.getChildByName(param1);
      }
      
      override public function getChildIndex(param1:DisplayObject) : int {
         return _locked?0:super.getChildIndex(param1);
      }
      
      override public function getObjectsUnderPoint(param1:Point) : Array {
         return _locked?null:super.getObjectsUnderPoint(param1);
      }
      
      override public function contains(param1:DisplayObject) : Boolean {
         return _locked?false:super.contains(param1);
      }
      
      override public function swapChildren(param1:DisplayObject, param2:DisplayObject) : void {
         if(!_locked)
         {
            super.swapChildren(param1,param2);
         }
      }
      
      override public function swapChildrenAt(param1:int, param2:int) : void {
         if(!_locked)
         {
            super.swapChildrenAt(param1,param2);
         }
      }
      
      override public function setChildIndex(param1:DisplayObject, param2:int) : void {
         if(!_locked)
         {
            super.setChildIndex(param1,param2);
         }
      }
      
      override public function removeChild(param1:DisplayObject) : DisplayObject {
         return _locked?null:super.removeChild(param1);
      }
      
      override public function removeChildAt(param1:int) : DisplayObject {
         return _locked?null:super.removeChildAt(param1);
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         if(!_locked)
         {
            super.addEventListener(param1,param2,param3,param4,param5);
         }
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         if(param1 != _width)
         {
            _width = param1;
            resize(param1,_height);
         }
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         if(param1 != _height)
         {
            _height = param1;
            resize(_width,param1);
         }
      }
      
      override public function get scaleX() : Number {
         return 1;
      }
      
      override public function get scaleY() : Number {
         return 1;
      }
      
      public function set excludeScrollRectChild(param1:DisplayObject) : void {
         if(!_scrollRectExcludeList)
         {
            _scrollRectExcludeList = [];
         }
         _scrollRectExcludeList.push(param1);
      }
      
      public function resize(param1:Number, param2:Number) : void {
         var _loc3_:* = 0;
         if(_scrollRectExcludeList && _scrollRectExcludeList.length)
         {
            _width = param1;
            _height = param2;
            _loc3_ = 0;
            while(_loc3_ < super.numChildren)
            {
               if(_scrollRectExcludeList.indexOf(super.getChildAt(_loc3_)) == -1)
               {
                  super.getChildAt(_loc3_).scrollRect = new Rectangle(0,0,param1,param2);
               }
               _loc3_++;
            }
         }
         else
         {
            _width = param1;
            _height = param2;
            scrollRect = new Rectangle(0,0,param1,param2);
         }
         dispatchEvent(new Event("resized",true));
      }
   }
}
