package tv.ustream.tools
{
   import flash.external.ExternalInterface;
   import flash.utils.getDefinitionByName;
   import tv.ustream.viewer.logic.Logic;
   import flash.errors.IllegalOperationError;
   
   public class Shell extends Object
   {
      
      public function Shell(param1:String = "", param2:Function = null) {
         var _loc3_:* = null;
         var _loc4_:* = null;
         super();
         if(!_instance)
         {
            if(param1)
            {
               application = param1;
            }
            _application = application;
            if(param2 != null)
            {
               callBack = param2;
            }
            _callBack = callBack;
            try
            {
               ExternalInterface.addCallback("ready",onReady);
            }
            catch(err:Error)
            {
            }
            _instance = this;
            listeners = [];
            if(jsApiEnabled)
            {
               initJsApi();
            }
            _loc3_ = {};
            if(Logic.loaderInfo)
            {
               if(Logic.loaderInfo.url.indexOf("viewer.rsl") != -1)
               {
                  Debug.echo("Logic.loaderInfo.url:  " + Logic.loaderInfo.url);
                  _loc4_ = Logic.loaderInfo.url.split("viewer.rsl")[1].substr(1);
                  _loc3_.rsl = -1;
                  if(_loc4_.split(".swf").length > 1)
                  {
                     _loc3_.rsl = parseInt(_loc4_.split(".swf")[0]);
                  }
               }
               if(!(Logic.loaderInfo.loaderURL.indexOf("viewer") == -1) && !(Logic.loaderInfo.loaderURL == Logic.loaderInfo.url))
               {
                  Debug.echo("Logic.loaderInfo.loaderURL:  " + Logic.loaderInfo.loaderURL);
                  _loc4_ = Logic.loaderInfo.loaderURL.split("viewer")[1].substr(1);
                  _loc3_.application = -1;
                  if(_loc4_.split(".swf").length > 1)
                  {
                     _loc3_.application = parseInt(_loc4_.split(".swf")[0]);
                  }
               }
            }
            Debug.echo("versioninfo");
            Debug.explore(_loc3_);
            call("ready",_loc3_);
            Debug.echo("[ SHELL ] created, application name: " + _application);
            return;
         }
         throw new IllegalOperationError("Shell already instantiated");
      }
      
      public static var application:String = "viewer";
      
      public static var callBack:Function;
      
      private static var _jsApiEnabled:Boolean = false;
      
      private static var _instance:Shell;
      
      public static function get hasInstance() : Boolean {
         return _instance?true:false;
      }
      
      private static var endPoints:Object;
      
      public static function get instance() : Shell {
         if(_instance)
         {
         }
         return _instance;
      }
      
      public static function get jsApiEnabled() : Boolean {
         return _jsApiEnabled;
      }
      
      public static function set jsApiEnabled(param1:Boolean) : void {
         _jsApiEnabled = param1;
         if(_jsApiEnabled && (hasInstance))
         {
            Shell.instance.initJsApi();
         }
      }
      
      public static function registerEntryPoint(param1:String, param2:*) : void {
         if(!endPoints)
         {
            endPoints = {};
         }
         endPoints[param1] = param2;
         Debug.echo("[ SHELL ] registered \'" + param1 + "\' to " + param2);
      }
      
      public static function unregisterEntryPoint(param1:String) : void {
         if(endPoints[param1])
         {
            endPoints[param1] = null;
         }
         else
         {
            Debug.echo("[ SHELL ] endpoint \'" + param1 + "\' is not registered");
         }
      }
      
      private var _application:String;
      
      private var firstCall:Boolean = true;
      
      private var _ready:Boolean = false;
      
      private var _callBack:Function;
      
      private var listeners:Array;
      
      private var jsApiInitialised:Boolean = false;
      
      public function onReady() : void {
         Debug.echo("[ SHELL ] ready");
         _ready = true;
         if(_callBack != null)
         {
            _callBack();
         }
      }
      
      public function resize(param1:Number, param2:Number) : void {
         call("resize",param1,param2);
      }
      
      public function call(param1:String, ... rest) : * {
         if(ExternalInterface.available && (firstCall || (ready)))
         {
            Debug.echo("[ SHELL ] call: ustream.flash." + _application + "." + ExternalInterface.objectID + "." + param1);
            if(firstCall)
            {
               Debug.echo("[ SHELL ] call: firstCall true");
            }
            firstCall = false;
            try
            {
               rest.unshift("ustream.flash." + _application + "." + ExternalInterface.objectID + "." + param1);
               _loc4_ = ExternalInterface.call.apply(this,rest);
               return _loc4_;
            }
            catch(e:Error)
            {
               Debug.echo("[ SHELL ] call failed \'" + e.message + "\'");
            }
            return null;
         }
         if(!ExternalInterface.available)
         {
            Debug.echo("[ SHELL ] call: ExternalInterface is not available");
         }
         if(!ready)
         {
            Debug.echo("[ SHELL ] call: not ready");
         }
         return null;
      }
      
      public function get ready() : Boolean {
         return _ready;
      }
      
      public function addCallBack(param1:String, param2:Function) : Boolean {
         try
         {
            ExternalInterface.addCallback(param1,param2);
         }
         catch(err:Error)
         {
            _loc4_ = false;
            return _loc4_;
         }
         return true;
      }
      
      public function dispatch(param1:String, param2:String, param3:Object = null) : void {
         if(listeners[param1] && listeners[param1][param2])
         {
            _loc6_ = 0;
            _loc5_ = listeners[param1][param2];
            for(_loc4_ in listeners[param1][param2])
            {
               Debug.echo("[ SHELL ] dispatch: " + _loc4_);
               call(_loc4_,param3);
            }
         }
      }
      
      private function initJsApi() : void {
         addCallBack("addListener",onAddEventListener);
         addCallBack("removeListener",onRemoveEventListener);
         addCallBack("setProperty",onSetProperty);
         addCallBack("getProperty",onGetProperty);
         addCallBack("callMethod",onCallMethod);
         jsApiInitialised = true;
         Debug.echo("[ SHELL ] jsapi initialised");
      }
      
      private function getTarget(param1:String) : Object {
         var _loc2_:* = null;
         var _loc4_:* = 0;
         var _loc3_:* = null;
         if(param1)
         {
            _loc2_ = param1.split(".");
            _loc4_ = 1;
            _loc3_ = endPoints?endPoints[_loc2_[0]]:null;
            if(_loc3_)
            {
               while(_loc3_ && _loc4_ < _loc2_.length)
               {
                  _loc3_ = _loc3_[_loc2_[_loc4_++]];
               }
            }
            else
            {
               Debug.echo("[ SHELL ] " + _loc2_[0] + " not found");
            }
            return _loc3_;
         }
         Debug.echo("[ SHELL ] ns is empty");
         return null;
      }
      
      private function onCallMethod(param1:String, param2:String, ... rest) : * {
         var _loc5_:* = null;
         var _loc6_:* = null;
         var _loc4_:* = undefined;
         Debug.echo("[ SHELL ] onCallMethod: " + param1 + ", " + param2);
         if(param1.indexOf("::") != -1)
         {
            _loc5_ = getDefinitionByName(param1) as Class;
            if(_loc5_[param2] is Function)
            {
               return (_loc5_[param2] as Function).apply(this,rest);
            }
         }
         else
         {
            _loc6_ = getTarget(param1);
            if(_loc6_ && _loc6_[param2] is Function)
            {
               Debug.echo("[ SHELL ] onCallMethod: " + param2 + " is a valid function");
               _loc4_ = (_loc6_[param2] as Function).apply(this,rest);
               if(_loc4_ is Number || _loc4_ is String || _loc4_ is Array)
               {
                  return _loc4_;
               }
               return null;
            }
         }
         Debug.echo("[ SHELL ] onCallMethod: call failed");
         return null;
      }
      
      private function onGetProperty(param1:String, param2:String) : * {
         Debug.echo("[ SHELL ] onGetProperty " + param1 + " " + param2);
         var _loc3_:Object = getTarget(param1);
         if(_loc3_ && _loc3_.hasOwnProperty(param2))
         {
            return _loc3_[param2];
         }
         if(!_loc3_)
         {
            Debug.echo("[ SHELL ] onGetProperty: no such path as \'" + param1 + "\'");
         }
         else
         {
            Debug.echo("[ SHELL ] onGetProperty: no such property as \'" + param2 + "\' on " + param1);
         }
         return null;
      }
      
      private function onSetProperty(param1:String, param2:String, param3:*) : void {
         Debug.echo("[ SHELL ] onSetProperty " + param1 + " " + param2 + " " + param3);
         var _loc4_:Object = getTarget(param1);
         if(_loc4_ && _loc4_.hasOwnProperty(param2))
         {
            _loc4_[param2] = param3;
         }
         else if(!_loc4_)
         {
            Debug.echo("[ SHELL ] onGetProperty: no such path as \'" + param1 + "\'");
         }
         else
         {
            Debug.echo("[ SHELL ] onSetProperty: no such property as \'" + param2 + "\' on " + param1);
         }
         
      }
      
      private function onRemoveEventListener(param1:String, param2:String, param3:String) : void {
         Debug.echo("[ SHELL ] onRemoveEventListener " + param1 + ", " + param2 + ", " + param3);
         if(listeners[param1] && listeners[param1][param2])
         {
            listeners[param1][param2][param3] = null;
         }
      }
      
      private function onAddEventListener(param1:String, param2:String, param3:String) : void {
         Debug.echo("[ SHELL ] onAddEventListener " + param1 + ", " + param2 + ", " + param3);
         if(!listeners[param1])
         {
            listeners[param1] = [];
         }
         if(!listeners[param1][param2])
         {
            listeners[param1][param2] = [];
         }
         listeners[param1][param2][param3] = 1;
      }
      
      public function hasEventListener(param1:String, param2:String) : Boolean {
         return listeners[param1] && listeners[param1][param2];
      }
   }
}
