package tv.ustream.tools
{
   import flash.utils.getDefinitionByName;
   import flash.display.Stage;
   
   public class Debug extends Object
   {
      
      public function Debug() {
         super();
      }
      
      public static const LEVEL_DEBUG:int = 0;
      
      public static const LEVEL_INFO:int = 1;
      
      public static const LEVEL_WARN:int = 2;
      
      public static const LEVEL_ERROR:int = 3;
      
      public static const LEVEL_FATAL:int = 4;
      
      public static const DEFAULT_LEVEL:int = 1;
      
      private static var _newDebug:Class;
      
      private static function newDebug() : Class {
         if(!_newDebug)
         {
            _newDebug = getDefinitionByName("tv.ustream.debug.Debug") as Class;
         }
         return _newDebug;
      }
      
      public static function echo(param1:String, param2:int = -1) : void {
      }
      
      public static function explore(param1:Object, param2:String = null, param3:uint = 0, param4:int = 0) : void {
         newDebug().debug("legacy","",newDebug().clone(param1,param2));
      }
      
      public static function debug(param1:String) : void {
         newDebug().debug("legacy",param1);
      }
      
      public static function info(param1:String) : void {
         newDebug().info("legacy",param1);
      }
      
      public static function warn(param1:String) : void {
         newDebug().warn("legacy",param1);
      }
      
      public static function error(param1:String) : void {
         newDebug().error("legacy",param1);
      }
      
      public static function fatal(param1:String) : void {
         newDebug().fatal("legacy",param1);
      }
      
      public static function set stage(param1:Stage) : void {
         newDebug().stage = param1;
      }
      
      public static function get debugPrefix() : String {
         return newDebug().debugPrefix;
      }
      
      public static function set debugPrefix(param1:String) : void {
         newDebug().debugPrefix = param1;
      }
      
      public static function set enabled(param1:Boolean) : void {
         newDebug().enabled = param1;
      }
      
      public static function get enabled() : Boolean {
         return newDebug().enabled;
      }
   }
}
