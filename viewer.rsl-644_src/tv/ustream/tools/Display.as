package tv.ustream.tools
{
   import flash.display.MovieClip;
   import flash.display.DisplayObject;
   
   public class Display extends MovieClip
   {
      
      public function Display() {
         super();
         name = "display";
         addEventListener("added",update);
         addEventListener("removed",update);
         addEventListener("update",update);
      }
      
      public static const SCALEMODE_NONE:String = "none";
      
      public static const SCALEMODE_IN:String = "in";
      
      public static const SCALEMODE_OUT:String = "out";
      
      public static const SCALEMODE_AUTO:String = "auto";
      
      private static const SCALEMODE_VALUES:Array = ["none","in","out","auto"];
      
      private var _width:Number = 100;
      
      private var _height:Number = 100;
      
      protected var _scaleMode:String = "in";
      
      public function update(... rest) : void {
         var _loc10_:* = null;
         var _loc7_:* = NaN;
         var _loc3_:* = NaN;
         var _loc6_:* = NaN;
         var _loc4_:* = NaN;
         var _loc5_:* = NaN;
         var _loc8_:* = null;
         var _loc2_:* = NaN;
         var _loc11_:* = false;
         var _loc9_:Number = _width / _height;
         _loc2_ = 0.0;
         while(_loc2_ < numChildren)
         {
            _loc10_ = getChildAt(_loc2_);
            _loc7_ = _loc10_.width / _loc10_.scaleX;
            _loc3_ = _loc10_.height / _loc10_.scaleY;
            _loc8_ = _scaleMode == "auto"?_loc7_ > _width || _loc3_ > _height?"in":"none":_scaleMode;
            _loc6_ = _loc7_ / _loc3_;
            if(_loc6_)
            {
               if(_loc8_ == "none")
               {
                  _loc12_ = 1;
                  _loc10_.scaleY = _loc12_;
                  _loc10_.scaleX = _loc12_;
               }
               else
               {
                  _loc11_ = _loc8_ == "out";
                  if(_loc11_ && _loc9_ < _loc6_ || (!_loc11_ && _loc9_ > _loc6_))
                  {
                     _loc5_ = _height;
                     _loc4_ = _height * _loc6_;
                  }
                  else
                  {
                     _loc4_ = _width;
                     _loc5_ = _width / _loc6_;
                  }
                  try
                  {
                     _loc10_["resize"](_loc4_,_loc5_);
                  }
                  catch(e:*)
                  {
                     _loc10_.width = _loc4_;
                     _loc10_.height = _loc5_;
                  }
               }
               _loc10_.x = (_width - _loc10_.width) / 2;
               _loc10_.y = (_height - _loc10_.height) / 2;
            }
            _loc2_++;
         }
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         if(_width != param1)
         {
            _width = param1;
            update("width");
         }
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         if(_height != param1)
         {
            _height = param1;
            update("height");
         }
      }
      
      public function get scaleMode() : String {
         return _scaleMode;
      }
      
      public function set scaleMode(param1:String) : void {
         if(!(_scaleMode == param1) && !(SCALEMODE_VALUES.indexOf(param1) == -1))
         {
            _scaleMode = param1;
            update("scalemode");
         }
      }
   }
}
