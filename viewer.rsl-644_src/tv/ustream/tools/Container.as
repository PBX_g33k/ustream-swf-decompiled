package tv.ustream.tools
{
   import flash.display.Sprite;
   
   public class Container extends Sprite
   {
      
      public function Container() {
         super();
         init();
         render();
      }
      
      protected var _width:Number = 100;
      
      protected var _height:Number = 100;
      
      protected function render(... rest) : void {
      }
      
      protected function init() : void {
      }
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         if(_width != param1)
         {
            _width = param1;
            render();
         }
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         if(_height != param1)
         {
            _height = param1;
            render();
         }
      }
      
      public function resize(param1:Number, param2:Number) : void {
         if(!(_width == param1) || !(_height == param2))
         {
            _width = param1;
            _height = param2;
            render();
         }
      }
   }
}
