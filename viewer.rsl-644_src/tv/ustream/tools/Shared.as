package tv.ustream.tools
{
   import flash.utils.Proxy;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.events.NetStatusEvent;
   import flash.utils.flash_proxy;
   import flash.net.SharedObject;
   
   public dynamic class Shared extends Proxy
   {
      
      public function Shared(param1:Object = null, param2:String = null, param3:String = "viewer") {
         defaults = param1;
         folder = param2;
         name = param3;
         super();
         if(!so)
         {
            try
            {
               so = SharedObject.getLocal(name,"/");
               so.addEventListener("netStatus",onSoNetstatus);
            }
            catch(err:Error)
            {
               Debug.echo("[ SHARED ] creating local storage failed");
               so = {};
               so.data = {};
               so.flush = function():void
               {
               };
            }
         }
         root = so.data;
         if(folder)
         {
            if(!root[folder])
            {
               root[folder] = {};
            }
            root = root[folder];
         }
         if(defaults)
         {
            _loc8_ = 0;
            _loc7_ = defaults;
            for(property in defaults)
            {
               if(root[property] == undefined)
               {
                  root[property] = defaults[property];
               }
            }
         }
         if(!Shared.flushTimer)
         {
            Shared.flushTimer = new Timer(500);
            Shared.flushTimer.addEventListener("timer",onFlushTimerTick,false,0,true);
         }
      }
      
      private static var so;
      
      private static var flushTimer:Timer;
      
      protected static function onFlushTimerTick(param1:TimerEvent) : void {
         flushTimer.stop();
         try
         {
            so.flush();
         }
         catch(e:*)
         {
         }
      }
      
      private var root:Object;
      
      private function onSoNetstatus(param1:NetStatusEvent) : void {
      }
      
      override flash_proxy function getProperty(param1:*) : * {
         return root[param1];
      }
      
      override flash_proxy function setProperty(param1:*, param2:*) : void {
         root[param1] = param2;
         if(!Shared.flushTimer.running)
         {
            Shared.flushTimer.reset();
            Shared.flushTimer.start();
         }
      }
   }
}
