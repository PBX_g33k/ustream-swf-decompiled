package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import tv.ustream.debug.Debug;
   import flash.display.LoaderInfo;
   import flash.display.Loader;
   import flash.events.Event;
   import flash.utils.getQualifiedClassName;
   import flash.events.ErrorEvent;
   import tv.ustream.viewer.logic.Logic;
   import flash.external.ExternalInterface;
   import flash.errors.IllegalOperationError;
   
   public class UncaughtErrorHandler extends EventDispatcher
   {
      
      public function UncaughtErrorHandler() {
         super();
         if(__lock__)
         {
            throw new IllegalOperationError("Singleton! Use UncaughtErrorHandler.getInstance() or UncaughtErrorHandler.instance.");
         }
         else
         {
            _eventQueue = new Vector.<DynamicEvent>();
            return;
         }
      }
      
      public static const UNCAUGHT_ERROR:String = "UncaughtError";
      
      public static const UNCAUGHT_ERROR_EVENT:String = "UncaughtErrorEvent";
      
      public static const UNCAUGHT_THROWN_OBJECT:String = "UncaughtThrownObject";
      
      private static const NAME:String = "uncaughtErrorHandler";
      
      private static var __lock__:Boolean = true;
      
      private static var _instance:UncaughtErrorHandler;
      
      public static function getInstance() : UncaughtErrorHandler {
         if(!_instance)
         {
            __lock__ = false;
            _instance = new UncaughtErrorHandler();
            __lock__ = true;
         }
         return _instance;
      }
      
      public static function get instance() : UncaughtErrorHandler {
         return getInstance();
      }
      
      private var _eventQueue:Vector.<DynamicEvent>;
      
      private var _preventDefaults:Boolean = false;
      
      public function add(param1:Object, param2:String = null) : IEventDispatcher {
         var _loc3_:IEventDispatcher = getDispatcher(param1);
         if(_loc3_ && !_loc3_.hasEventListener("uncaughtError"))
         {
            _loc3_.addEventListener("uncaughtError",onUncaughtError);
            tv.ustream.debug.Debug.error("uncaughtErrorHandler","Add dispatcher. " + _loc3_ + ", " + (param2?param2:""));
         }
         return _loc3_;
      }
      
      public function remove(param1:Object) : void {
         var _loc2_:IEventDispatcher = getDispatcher(param1);
         if(_loc2_ && _loc2_.hasEventListener("uncaughtError"))
         {
            _loc2_.removeEventListener("uncaughtError",onUncaughtError);
            tv.ustream.debug.Debug.debug("uncaughtErrorHandler","Remove dispatcher.");
         }
      }
      
      public function set preventDefaults(param1:Boolean) : void {
         _preventDefaults = param1;
      }
      
      private function getDispatcher(param1:Object) : IEventDispatcher {
         if(param1 is LoaderInfo && (param1.hasOwnProperty("uncaughtErrorEvents")) || (param1 is Loader && (param1.hasOwnProperty("uncaughtErrorEvents"))))
         {
            return IEventDispatcher(param1["uncaughtErrorEvents"]);
         }
         return null;
      }
      
      private function onUncaughtError(param1:Event) : void {
         var _loc2_:* = null;
         var _loc4_:* = null;
         var _loc3_:* = null;
         if(_preventDefaults)
         {
            param1.preventDefault();
            param1.stopImmediatePropagation();
         }
         if(param1["error"])
         {
            _loc2_ = getQualifiedClassName(param1["error"]);
            if(param1["error"] is Error)
            {
               _loc4_ = param1["error"] as Error;
               logger("UncaughtError",_loc2_,_loc4_.name,_loc4_.message,_loc4_.getStackTrace(),param1);
            }
            else if(param1["error"] is ErrorEvent)
            {
               _loc3_ = param1["error"] as ErrorEvent;
               logger("UncaughtErrorEvent",_loc2_,_loc3_.type,_loc3_.text,"",param1);
            }
            else
            {
               logger("UncaughtThrownObject",_loc2_,"","","",param1);
            }
            
         }
         else
         {
            tv.ustream.debug.Debug.error("uncaughtErrorHandler","error property undefined!");
         }
      }
      
      private function gaTrack(param1:String, param2:String, param3:String = null, param4:int = 0) : void {
         if(Logic.hasInstance && (Logic.instance.media.disabledGA))
         {
            return;
         }
         tv.ustream.debug.Debug.debug("uncaughtErrorHandler","gaTrack ERROR: " + param1 + ", " + param2 + (param3?", " + param3:"") + (param4?", " + param4 + " (type: " + "number" + ")":""));
         if(ExternalInterface.available)
         {
            try
            {
               ExternalInterface.call("ustream.track",param1,param2,param3,param4);
            }
            catch(e:*)
            {
               tv.ustream.debug.Debug.error("uncaughtErrorHandler","gaTrack error: " + e);
            }
         }
         else
         {
            tv.ustream.debug.Debug.error("uncaughtErrorHandler","!ExternalInterface.available");
         }
      }
      
      private function logger(param1:String, param2:String, param3:String, param4:String, param5:String, param6:Event) : void {
         var _loc9_:* = null;
         var _loc10_:String = "unknown";
         var _loc11_:String = "Error";
         if(param5)
         {
            _loc9_ = param5.split("at ");
            _loc10_ = _loc9_.length > 1?(_loc9_[1]).substring(0,(_loc9_[1]).indexOf("[") > -1?(_loc9_[1]).indexOf("["):(_loc9_[1]).length):"unknown";
            _loc11_ = "ErrorDebug";
         }
         var _loc7_:Array = param4.split("#");
         tv.ustream.debug.Debug.debug("uncaughtErrorHandler","Error code : " + (_loc7_[1]).substr(0,4));
         gaTrack("Player",_loc11_,(param2 != param3?param2 + "." + param3:param2) + "/" + (_loc7_.length > 1?"Error#" + (_loc7_[1]).substr(0,4) + "/":"") + _loc10_ + "/" + (param6 && param6.target is LoaderInfo?LoaderInfo(param6.target).url + "/":"n.a./") + (Logic.hasInstance?Logic.instance.media.type + "|" + Logic.instance.media.id:""));
         var _loc8_:DynamicEvent = new DynamicEvent(param1,false,false,
            {
               "code":(_loc7_[1]).substr(0,4),
               "message":param4
            });
         if(hasEventListener(param1))
         {
            dispatchEvent(_loc8_);
         }
         else
         {
            _eventQueue.push(_loc8_);
         }
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         var _loc6_:* = 0;
         var _loc7_:* = null;
         super.addEventListener(param1,param2,param3,param4,param5);
         _loc6_ = 0;
         while(_loc6_ < _eventQueue.length)
         {
            if(_eventQueue[_loc6_].type == param1)
            {
               _loc7_ = _eventQueue[_loc6_];
               dispatchEvent(_loc7_);
               _eventQueue.splice(_loc6_--,1);
            }
            _loc6_++;
         }
      }
   }
}
