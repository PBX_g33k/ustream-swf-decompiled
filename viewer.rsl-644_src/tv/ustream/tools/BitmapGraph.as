package tv.ustream.tools
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.geom.Rectangle;
   import flash.geom.Point;
   import flash.events.Event;
   import flash.utils.getTimer;
   
   public class BitmapGraph extends Bitmap
   {
      
      public function BitmapGraph(param1:uint = 300, param2:uint = 100) {
         _width = param1;
         _height = param2;
         data = new BitmapData(_width * overSampling,_height * overSampling,true,0);
         drawCache = [];
         resizeData(_width,_height);
         super(data,"auto",true);
         scaleY = 1 / overSampling;
         scaleX = 1 / overSampling;
         addEventListener("enterFrame",onEnterFrame,false,100);
      }
      
      private var _width:int;
      
      private var _height:int;
      
      private var data:BitmapData;
      
      private var overSampling:uint = 2;
      
      private var shift:uint = 1;
      
      private var lastFrameTimer:uint;
      
      private var drawCache:Array;
      
      override public function get width() : Number {
         return _width;
      }
      
      override public function set width(param1:Number) : void {
         _width = Math.max(0,param1);
         resizeData(_width,_height);
      }
      
      override public function get height() : Number {
         return _height;
      }
      
      override public function set height(param1:Number) : void {
         _height = Math.max(0,param1);
         resizeData(_width,_height);
      }
      
      private function resizeData(param1:uint, param2:uint) : void {
         var _loc3_:* = null;
         var _loc4_:* = null;
         if(!(data.width == param1 * overSampling) || !(data.height == param2 * overSampling))
         {
            _loc3_ = new BitmapData(Math.max(1,param1) * overSampling,Math.max(1,param2) * overSampling,true,0);
            _loc4_ = new Rectangle();
            _loc4_.width = Math.min(_loc3_.width,data.width);
            _loc4_.height = Math.min(_loc3_.height,data.height);
            _loc4_.x = data.width - _loc4_.width;
            _loc4_.y = data.height - _loc4_.height;
            _loc3_.copyPixels(data,_loc4_,new Point(_loc3_.width - _loc4_.width,_loc3_.height - _loc4_.height));
            data = _loc3_;
            bitmapData = _loc3_;
         }
      }
      
      protected function clear() : void {
         data.fillRect(new Rectangle(0,0,_width,_height),0);
      }
      
      protected function draw(param1:Number, param2:Number = -1, param3:uint = 65280, param4:Boolean = true, param5:Number = 0.25) : void {
         var param1:Number = Math.max(0,Math.min(1,param1));
         if(param2 == -1)
         {
            param2 = param1;
         }
         drawCache[drawCache.length - 1].drawList.push(
            {
               "value":param1,
               "lastValue":param2,
               "color":param3,
               "filled":param4,
               "alpha":param5
            });
      }
      
      private function onEnterFrame(param1:Event) : void {
         cleanDrawCache();
         if(visible)
         {
            processDrawCache();
         }
         var _loc2_:uint = stage?stage.frameRate:30.0;
         var _loc3_:Number = 1000 / (getTimer() - lastFrameTimer);
         shift = overSampling * Math.round(_loc2_ / _loc3_);
         lastFrameTimer = getTimer();
         var _loc4_:Object = {};
         _loc4_.shift = shift;
         _loc4_.drawList = [];
         drawCache.push(_loc4_);
      }
      
      protected function cleanDrawCache() : void {
         var _loc2_:* = 0;
         var _loc1_:uint = 0;
         _loc2_ = drawCache.length - 1;
         while(_loc2_ >= 0)
         {
            _loc1_ = _loc1_ + drawCache[_loc2_].shift;
            if(_loc1_ >= data.width)
            {
               _loc2_--;
               break;
            }
            _loc2_--;
         }
         if(_loc2_ > 0)
         {
            drawCache.splice(0,_loc2_);
         }
      }
      
      protected function processDrawCache() : void {
         var _loc6_:* = 0;
         var _loc2_:* = null;
         var _loc7_:* = null;
         var _loc1_:* = 0;
         var _loc3_:* = NaN;
         var _loc4_:* = NaN;
         if(!drawCache.length)
         {
            return;
         }
         var _loc5_:uint = 0;
         _loc6_ = 0;
         while(_loc6_ < drawCache.length)
         {
            _loc5_ = _loc5_ + drawCache[_loc6_].shift;
            _loc6_++;
         }
         data.scroll(-_loc5_,0);
         data.fillRect(new Rectangle(data.width - _loc5_,0,_loc5_,data.height),0);
         if(!drawCache.length)
         {
            return;
         }
         _loc5_ = 0;
         while(drawCache.length)
         {
            _loc2_ = drawCache.pop();
            _loc6_ = 0;
            while(_loc6_ < _loc2_.drawList.length)
            {
               _loc7_ = _loc2_.drawList[_loc6_];
               _loc1_ = 0;
               while(_loc1_ < _loc2_.shift)
               {
                  _loc3_ = Math.floor((_loc7_.lastValue + (_loc7_.value - _loc7_.lastValue) / _loc2_.shift * _loc1_) * data.height);
                  if(_loc7_.filled)
                  {
                     data.fillRect(new Rectangle(data.width - _loc5_ - _loc2_.shift + _loc1_,data.height - Math.min(data.height,_loc3_),1,Math.min(data.height,_loc3_)),(Math.floor(255 * _loc7_.alpha) << 24) + _loc7_.color);
                  }
                  if(data.height - _loc3_ + _loc2_.shift / 2 >= 0)
                  {
                     _loc4_ = data.height - _loc3_ - overSampling / 2;
                     data.fillRect(new Rectangle(data.width - _loc5_ - _loc2_.shift + _loc1_,Math.max(0,_loc4_),1,overSampling - (Math.max(0,_loc4_) - _loc4_)),4.27819008E9 + _loc7_.color);
                  }
                  _loc1_++;
               }
               _loc6_++;
            }
            _loc5_ = _loc5_ + _loc2_.shift;
         }
      }
      
      public function destroy() : * {
         removeEventListener("enterFrame",onEnterFrame);
         if(parent)
         {
            parent.removeChild(this);
         }
         data.dispose();
      }
   }
}
