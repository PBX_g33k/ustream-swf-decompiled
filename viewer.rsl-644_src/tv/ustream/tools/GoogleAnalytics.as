package tv.ustream.tools
{
   import tv.ustream.debug.Debug;
   import flash.external.ExternalInterface;
   
   public class GoogleAnalytics extends Object
   {
      
      public function GoogleAnalytics() {
         super();
      }
      
      public static function track(param1:String, param2:String, param3:String = null, param4:Number = 0) : void {
         tv.ustream.debug.Debug.debug("googleAnalytics","track: " + param1 + "/" + param2 + (param3?"/" + param3:"") + (param4?"/" + param4:""));
         if(ExternalInterface.available)
         {
            try
            {
               ExternalInterface.call("ustream.track",param1,param2,param3,param4);
            }
            catch(e:*)
            {
               tv.ustream.debug.Debug.error("googleAnalytics","track error:" + e);
            }
         }
      }
   }
}
