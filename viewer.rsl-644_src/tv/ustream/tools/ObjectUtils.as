package tv.ustream.tools
{
   public class ObjectUtils extends Object
   {
      
      public function ObjectUtils() {
         super();
      }
      
      public static function hasKeys(param1:Object) : Boolean {
         if(!param1)
         {
            return false;
         }
         var _loc4_:* = 0;
         var _loc3_:* = param1;
         for(_loc2_ in param1)
         {
            return true;
         }
         return false;
      }
      
      public static function merge(param1:Object, param2:Object) : void {
         var _loc3_:* = 0;
         if(param1 && param2)
         {
            if(param1 is Array && param2 is Array)
            {
               _loc3_ = 0;
               while(_loc3_ < param1.length)
               {
                  if(param1[_loc3_] is Array)
                  {
                     param2.push([]);
                     merge(param1[_loc3_],param2[param2.length - 1]);
                  }
                  else if(typeof (param1[_loc3_] == "object"))
                  {
                     param2.push({});
                     merge(param1[_loc3_],param2[param2.length - 1]);
                  }
                  else
                  {
                     param2.push(param1[_loc3_]);
                  }
                  
                  _loc3_++;
               }
            }
            else if(typeof param1 == "object")
            {
               _loc6_ = 0;
               _loc5_ = param1;
               for(_loc4_ in param1)
               {
                  if(param1[_loc4_] is Array)
                  {
                     if(!(param2[_loc4_] is Array))
                     {
                        param2[_loc4_] = [];
                     }
                     merge(param1[_loc4_],param2[_loc4_]);
                  }
                  else if(typeof param1[_loc4_] == "object")
                  {
                     if(typeof param2[_loc4_] != "object")
                     {
                        param2[_loc4_] = {};
                     }
                     merge(param1[_loc4_],param2[_loc4_]);
                  }
                  else
                  {
                     param2[_loc4_] = param1[_loc4_];
                  }
                  
               }
            }
            
         }
      }
   }
}
