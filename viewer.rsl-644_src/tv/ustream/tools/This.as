package tv.ustream.tools
{
   import flash.external.ExternalInterface;
   import flash.system.Capabilities;
   
   public class This extends Object
   {
      
      public function This() {
         super();
      }
      
      public static const SITE_TYPE_EMBED:uint = 1;
      
      public static const SITE_TYPE_STAGE:uint = 2;
      
      public static const SITE_TYPE_BRANCH:uint = 4;
      
      public static const SITE_TYPE_TAG:uint = 8;
      
      public static const SITE_TYPE_LOCAL:uint = 32;
      
      public static var externalInterface:Boolean = true;
      
      public static var secure:Boolean = false;
      
      private static var _pageUrl:String = "";
      
      private static var _referrer:String = "";
      
      private static var _loaderUrl:String = "";
      
      public static function getReference(param1:Object, param2:String) : Object {
         var _loc3_:* = null;
         if(!param1)
         {
            return null;
         }
         var _loc4_:Array = param2.split(".");
         _loc4_.reverse();
         while(_loc4_.length)
         {
            _loc3_ = _loc4_.pop();
            param1 = param1.hasOwnProperty(_loc3_)?param1[_loc3_]:null;
            if(!param1)
            {
               return null;
            }
         }
         return param1;
      }
      
      public static function onSite(param1:String) : uint {
         var _loc4_:uint = 0;
         var _loc2_:* = false;
         if(!param1 || param1 && !(param1.indexOf(".swf") == -1))
         {
            return _loc4_;
         }
         var _loc3_:String = getPageDomain(param1);
         if(_loc3_.substr(-10,10) == "ustream.tv" || _loc3_.substr(-13,13) == "ustream.tv.lh")
         {
            _loc4_ = _loc4_ | 2;
         }
         if(_loc4_ && (!(_loc3_.indexOf("embed.") == -1) || !(param1.indexOf("ustream.tv/embed/") == -1)))
         {
            _loc4_ = 1;
         }
         if(_loc4_ & 2)
         {
            if(!(_loc3_.indexOf(".branches.") == -1) || (!(_loc3_.indexOf(".dev.") == -1) && !(_loc3_.indexOf("-branches-") == -1)))
            {
               _loc4_ = _loc4_ | 4;
            }
            if(!(_loc3_.indexOf(".tags.") == -1) || (!(_loc3_.indexOf(".dev.") == -1) && !(_loc3_.indexOf("-tags-") == -1)))
            {
               _loc4_ = _loc4_ | 8;
            }
            if(_loc3_.substr(-3,3) == ".lh")
            {
               _loc4_ = _loc4_ | 32;
            }
         }
         return _loc4_;
      }
      
      public static function flashPath(param1:String) : String {
         var _loc2_:* = null;
         if(onSite(param1) & 4 || onSite(param1) & 8)
         {
            _loc2_ = (param1.indexOf("://") == -1?param1:param1.split("://")[1]).split("/")[0];
            return (secure?"https://":"http://") + _loc2_ + "/flash/";
         }
         return "http://www.ustream.tv/flash/";
      }
      
      public static function get referrer() : String {
         return _referrer;
      }
      
      public static function set referrer(param1:String) : void {
         if(pageUrl && (onSite(pageUrl) & 2 || onSite(pageUrl) & 1))
         {
            _referrer = param1;
         }
      }
      
      public static function set loaderUrl(param1:String) : void {
         if(!_loaderUrl)
         {
            _loaderUrl = param1;
         }
      }
      
      public static function get pageUrl() : String {
         var _loc1_:* = null;
         try
         {
            _loc1_ = "getPageUrl=function(){ return window.location.href }; getPageUrl()";
            _pageUrl = ExternalInterface.call("eval",_loc1_);
         }
         catch(e:Error)
         {
            externalInterface = false;
            trace("js error");
         }
         if(!_pageUrl)
         {
            _pageUrl = "";
         }
         return _pageUrl;
      }
      
      public static function compareVersion(param1:String) : Boolean {
         var _loc2_:* = null;
         var _loc4_:* = 0;
         var _loc3_:Array = Capabilities.version.split(" ")[1].split(",");
         if(param1.indexOf(".") != -1)
         {
            _loc2_ = param1.split(".");
         }
         else if(param1.indexOf(",") != -1)
         {
            _loc2_ = param1.split(",");
         }
         else
         {
            _loc2_ = new Array(param1);
         }
         
         _loc4_ = 0;
         while(_loc4_ < _loc2_.length)
         {
            if((_loc3_[_loc4_]) < (_loc2_[_loc4_]))
            {
               return false;
            }
            if((_loc3_[_loc4_]) <= (_loc2_[_loc4_]))
            {
               _loc4_++;
               continue;
            }
            break;
         }
         return true;
      }
      
      public static function getPageDomain(param1:String) : String {
         var _loc2_:String = param1.indexOf("://") == -1?param1:param1.split("://")[1];
         if(!_loc2_)
         {
            return "";
         }
         _loc2_ = _loc2_.split("/")[0];
         return _loc2_;
      }
   }
}
