package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import flash.system.LoaderContext;
   import flash.display.Loader;
   import flash.utils.Timer;
   import flash.events.IOErrorEvent;
   import flash.events.Event;
   import flash.net.URLRequest;
   import flash.system.ApplicationDomain;
   import flash.system.Security;
   import flash.system.SecurityDomain;
   import flash.errors.IllegalOperationError;
   
   public class LibLoader extends EventDispatcher
   {
      
      public function LibLoader() {
         super();
         if(!_instance)
         {
            loadQueue = [];
            _references = {};
            loaderContext = new LoaderContext(true,ApplicationDomain.currentDomain);
            if(Security.sandboxType != "localTrusted")
            {
               loaderContext.securityDomain = SecurityDomain.currentDomain;
            }
            loader = new Loader();
            loader.contentLoaderInfo.addEventListener("complete",onLoaderComplete);
            loader.contentLoaderInfo.addEventListener("ioError",onLoaderError);
            UncaughtErrorHandler.instance.add(loader,this.toString());
            retryTimer = new Timer(1000,1);
            retryTimer.addEventListener("timerComplete",loadNext);
            _instance = this;
            return;
         }
         throw new IllegalOperationError("LibLoader already instantiated");
      }
      
      protected static var _instance:LibLoader;
      
      public static function get instance() : LibLoader {
         if(_instance)
         {
         }
         return _instance;
      }
      
      private const RETRY_DELAY:int = 1000;
      
      private var loaderContext:LoaderContext;
      
      private var loader:Loader;
      
      private var retryTimer:Timer;
      
      protected var loadQueue:Array;
      
      protected var _references:Object;
      
      public function get references() : Object {
         return _references;
      }
      
      private function onLoaderError(param1:IOErrorEvent) : void {
         if(loadQueue[0].retryCount < loadQueue[0].maxRetries)
         {
            _loc2_ = loadQueue[0].retryCount + 1;
            loadQueue[0].retryCount++;
            Debug.echo("[ LIBLOADER ] " + loadQueue[0].url + " failed, retry #" + _loc2_);
            retryTimer.reset();
            retryTimer.start();
         }
         else
         {
            dispatchEvent(new Event("ioError:" + loadQueue[0].id));
            dispatchEvent(new DynamicEvent("ioError",false,false,{"id":loadQueue[0].id}));
            Debug.echo("[ LIBLOADER ] " + loadQueue[0].url + " failed, omitted");
            loadQueue.splice(0,1);
            if(loadQueue.length)
            {
               loadNext();
            }
         }
      }
      
      private function onLoaderComplete(param1:Event) : void {
         dispatchEvent(new Event("complete:" + loadQueue[0].id));
         dispatchEvent(new DynamicEvent("complete",false,false,
            {
               "id":loadQueue[0].id,
               "reference":loader.content
            }));
         if(loadQueue[0].storeReference)
         {
            _references[loadQueue[0].id] = loader.content;
         }
         UncaughtErrorHandler.instance.add(loader.content.loaderInfo,loader.content.toString());
         loadQueue.splice(0,1);
         if(loadQueue.length)
         {
            loadNext();
         }
      }
      
      private function loadNext(... rest) : void {
         Debug.echo("[ LIBLOADER ] loading " + loadQueue[0].url);
         loader.load(new URLRequest(loadQueue[0].url),loaderContext);
      }
      
      public function load(param1:String, param2:uint = 3, param3:Boolean = false) : String {
         var _loc4_:String = StringUtils.random(8);
         Debug.echo("[ LIBLOADER ] added " + param1 + " as " + _loc4_);
         if(param1.indexOf("/") == -1)
         {
            param1 = This.flashPath(This.pageUrl) + param1;
         }
         loadQueue.push(
            {
               "id":_loc4_,
               "url":param1,
               "retryCount":0,
               "maxRetries":param2,
               "storeReference":param3
            });
         if(loadQueue.length == 1)
         {
            loadNext();
         }
         return _loc4_;
      }
   }
}
