package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import tv.ustream.interfaces.IDispatcher;
   import flash.events.Event;
   import tv.ustream.debug.Debug;
   import flash.utils.getQualifiedClassName;
   
   public class Dispatcher extends EventDispatcher implements IDispatcher
   {
      
      public function Dispatcher(param1:String = null) {
         super();
         _id = param1;
      }
      
      public static const DESTROY:String = "destroy";
      
      protected var _id:String;
      
      protected var _type:String = "";
      
      protected var prefix:String = "";
      
      protected var _active:Boolean = true;
      
      protected var _silent:Boolean = false;
      
      private var listeners:Array;
      
      protected function dispatch(param1:String, param2:Boolean = false, param3:* = null, param4:Boolean = false) : void {
         if(active)
         {
            if(!param4)
            {
               echo("dispatch : " + param1);
            }
            dispatchEvent(param3?new DynamicEvent(param1,param2,param2,param3):new Event(param1,param2,param2));
            if(Shell.hasInstance && (Shell.jsApiEnabled))
            {
               Shell.instance.dispatch(type,param1,param3);
            }
         }
      }
      
      public function setEchoPrefix(param1:String) : void {
         prefix = param1;
      }
      
      protected function echo(param1:String, param2:int = -1) : void {
         var _loc3_:* = null;
         if(!_silent)
         {
            _loc3_ = (prefix == ""?"":prefix + "_") + type.toLocaleUpperCase();
            if(_id)
            {
               _loc3_ = _loc3_ + (":" + _id);
            }
         }
         if(!_silent)
         {
            return;
         }
      }
      
      override public function dispatchEvent(param1:Event) : Boolean {
         if(hasEventListener(param1.type) || (param1.bubbles))
         {
            return super.dispatchEvent(param1);
         }
         return true;
      }
      
      public function destroy(... rest) : * {
         dispatch("destroy");
         _active = false;
         eliminateEventListeners();
         return null;
      }
      
      public function get id() : String {
         return _id;
      }
      
      public function get type() : String {
         if(!_type)
         {
            _type = getQualifiedClassName(this).toLowerCase();
            if(_type.indexOf("::") != -1)
            {
               _type = _type.split("::")[1];
            }
         }
         return _type;
      }
      
      public function get active() : Boolean {
         return _active;
      }
      
      public function get silent() : Boolean {
         return _silent;
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         if(!listeners)
         {
            listeners = [];
         }
         listeners.push(
            {
               "type":param1,
               "listener":param2,
               "useCapture":param3,
               "priority":param4,
               "useWeakReference":param5
            });
         super.addEventListener(param1,param2,param3,param4,param5);
      }
      
      override public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void {
         var _loc4_:* = 0;
         if(!listeners)
         {
            listeners = [];
         }
         _loc4_ = 0;
         while(_loc4_ < listeners.length)
         {
            if(listeners[_loc4_] && listeners[_loc4_].type == param1 && listeners[_loc4_].listener == param2 && listeners[_loc4_].useCapture == param3)
            {
               listeners.splice(_loc4_,1);
               break;
            }
            _loc4_++;
         }
         super.removeEventListener(param1,param2,param3);
      }
      
      public function eliminateEventListeners() : void {
         var _loc1_:* = null;
         while(listeners && listeners.length)
         {
            _loc1_ = listeners.pop();
            super.removeEventListener(_loc1_.type,_loc1_.listener,_loc1_.useCapture);
         }
      }
      
      public function hasSpecificEventListener(param1:String, param2:Function) : Boolean {
         var _loc3_:* = 0;
         if(!listeners)
         {
            listeners = [];
         }
         _loc3_ = 0;
         while(_loc3_ < listeners.length)
         {
            if(listeners[_loc3_].type == param1 && listeners[_loc3_].listener == param2)
            {
               return true;
            }
            _loc3_++;
         }
         return false;
      }
   }
}
