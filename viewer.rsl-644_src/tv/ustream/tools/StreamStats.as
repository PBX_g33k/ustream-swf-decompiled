package tv.ustream.tools
{
   import flash.display.Sprite;
   import flash.net.NetStream;
   import tv.ustream.viewer.logic.media.Recorded;
   import flash.text.TextField;
   import tv.ustream.net.ViewerUms;
   import flash.utils.Timer;
   import flash.text.TextFormat;
   import flash.filters.DropShadowFilter;
   import tv.ustream.net.FragmentStream;
   import flash.events.TimerEvent;
   import tv.ustream.net.ChunkStream;
   import tv.ustream.debug.Debug;
   import tv.ustream.viewer.logic.Logic;
   import flash.system.Capabilities;
   
   public class StreamStats extends Sprite
   {
      
      public function StreamStats(param1:Recorded = null, param2:String = "", param3:* = null) {
         var _loc4_:* = null;
         stats = {};
         super();
         if(param1)
         {
            this.recorded = param1;
         }
         if(param2)
         {
            this.rpin = param2;
         }
         _ums = param1.ums;
         if(param3)
         {
            _video = param3;
         }
         if(Logic.loaderInfo && !(Logic.loaderInfo.url.indexOf("viewer.rsl") == -1))
         {
            _loc4_ = Logic.loaderInfo.url.split("viewer.rsl")[1].substr(1);
            if(_loc4_.split(".swf").length > 1)
            {
               rslVersion = _loc4_.split(".swf")[0];
            }
            else
            {
               rslVersion = "local";
            }
         }
         mouseEnabled = false;
         mouseChildren = false;
         visible = false;
         fpInfo = "fplayer > version: " + Capabilities.version + ", debug: " + Capabilities.isDebugger;
      }
      
      private var _visible:Boolean;
      
      private var _stream:NetStream;
      
      private var recorded:Recorded;
      
      private var tf:TextField;
      
      private var rpin:String = "";
      
      private var _ums:ViewerUms;
      
      private var _video;
      
      private var stats:Object;
      
      private var statsTimer:Timer;
      
      private var fpInfo:String;
      
      private var rslVersion:String = "";
      
      public var streamUri:String = "";
      
      private var bufferGraph:BufferGraph;
      
      private var fragmentMap:FragmentMap;
      
      public function set ums(param1:ViewerUms) : void {
         _ums = param1;
      }
      
      override public function get visible() : Boolean {
         return _visible;
      }
      
      override public function set visible(param1:Boolean) : void {
         _visible = param1;
         .super.visible = param1 || false;
         if(_visible && !hasEventListener("enterFrame"))
         {
            addEventListener("enterFrame",render);
         }
         else if(!_visible && (hasEventListener("enterFrame")))
         {
            removeEventListener("enterFrame",render);
         }
         
         if(!tf)
         {
            tf = new TextField();
            tf.defaultTextFormat = new TextFormat("arial",11,16777215);
            tf.selectable = false;
            tf.multiline = true;
            tf.wordWrap = true;
            tf.filters = [new DropShadowFilter(0,0,0,1,3,3,2,3)];
         }
         if(!bufferGraph)
         {
            bufferGraph = new BufferGraph(_stream);
            addChild(new BufferGraph(_stream));
         }
         bufferGraph.visible = _visible;
         if(_stream is FragmentStream)
         {
            if(!fragmentMap)
            {
               fragmentMap = new FragmentMap(_stream as FragmentStream);
               addChild(new FragmentMap(_stream as FragmentStream));
            }
            fragmentMap.visible = _visible;
         }
         if(super.visible)
         {
            addChild(tf);
         }
         else if(contains(tf))
         {
            removeChild(tf);
         }
         
         if(_visible)
         {
            if(!statsTimer)
            {
               statsTimer = new Timer(500);
               statsTimer.addEventListener("timer",onStats);
            }
            stats.fps = 
               {
                  "current":0,
                  "avg":0,
                  "min":9999999,
                  "max":0,
                  "count":0,
                  "sum":0
               };
            stats.vbps = 
               {
                  "current":0,
                  "avg":0,
                  "min":9999999,
                  "max":0,
                  "count":0,
                  "sum":0
               };
            stats.abps = 
               {
                  "current":0,
                  "avg":0,
                  "min":9999999,
                  "max":0,
                  "count":0,
                  "sum":0
               };
            statsTimer.start();
         }
         else if(statsTimer && statsTimer.running)
         {
            statsTimer.stop();
         }
         
         render();
      }
      
      private function onStats(param1:TimerEvent) : void {
         if(_stream)
         {
            stats.fps.current = Math.floor(_stream.currentFPS * 10) / 10;
            if(stats.fps.current < stats.fps.min)
            {
               stats.fps.min = stats.fps.current;
            }
            if(stats.fps.current > stats.fps.max)
            {
               stats.fps.max = stats.fps.current;
            }
            stats.fps.sum = stats.fps.sum + stats.fps.current;
            stats.fps.count++;
            stats.fps.avg = Math.floor(stats.fps.sum / stats.fps.count * 10) / 10;
            try
            {
               stats.vbps.current = Math.floor(_stream.info.videoBytesPerSecond * 8 / 1024 * 10) / 10;
            }
            catch(e:Error)
            {
               stats.vbps.current = 0;
            }
            if(stats.vbps.current < stats.vbps.min)
            {
               stats.vbps.min = stats.vbps.current;
            }
            if(stats.vbps.current > stats.vbps.max)
            {
               stats.vbps.max = stats.vbps.current;
            }
            stats.vbps.sum = stats.vbps.sum + stats.vbps.current;
            stats.vbps.count++;
            stats.vbps.avg = stats.vbps.sum > 0?Math.floor(stats.vbps.sum / stats.vbps.count * 10) / 10:0.0;
            stats.vbps.total = stats.vbps.sum > 0?Math.floor(stats.vbps.sum / 8 * 10) / 10:0.0;
            try
            {
               stats.abps.current = Math.floor(_stream.info.audioBytesPerSecond * 8 / 1024 * 10) / 10;
            }
            catch(e:Error)
            {
               stats.abps.current = 0;
            }
            if(stats.abps.current < stats.abps.min)
            {
               stats.abps.min = stats.abps.current;
            }
            if(stats.abps.current > stats.abps.max)
            {
               stats.abps.max = stats.abps.current;
            }
            stats.abps.sum = stats.abps.sum + stats.abps.current;
            stats.abps.count++;
            stats.abps.avg = stats.abps.sum > 0?Math.floor(stats.abps.sum / stats.abps.count * 10) / 10:0.0;
            stats.abps.total = stats.abps.sum > 0?Math.floor(stats.abps.sum / 8 * 10) / 10:0.0;
         }
      }
      
      private function render(... rest) : void {
         tf.text = "rsl version: STAGE" + (rslVersion != ""?" / " + rslVersion:"");
         if(_visible)
         {
            createTfText();
         }
      }
      
      private function createTfText() : void {
         var _loc2_:* = null;
         tf.appendText(" / mediaId: " + recorded.mediaId + " / bInfo: " + "1015" + " (" + "Tibor" + ", " + "2014.06.04.  9:43:15,27" + ")" + (recorded.viewerBuildInfo?" / " + recorded.viewerBuildInfo.split(" ")[0]:""));
         var _loc1_:Date = new Date();
         tf.appendText("\n" + _loc1_.toUTCString() + " / " + _loc1_.toString() + "\n");
         if(_stream)
         {
            try
            {
               tf.appendText("\nbuffer (length/time): " + _stream.bufferLength + "/" + _stream.bufferTime);
               if(stream is FragmentStream)
               {
                  _loc2_ = stream as FragmentStream;
                  tf.appendText("\nprogressive");
                  tf.appendText("\n   fragments memory : " + StringUtils.thousands(_loc2_.memoryUsage.packets));
                  tf.appendText("- limit : " + StringUtils.thousands(_loc2_.maxMemoryUsage));
                  tf.appendText("\n   loadbuffer memory : " + StringUtils.thousands(_loc2_.memoryUsage.buffer));
                  tf.appendText("\n   bandwidth : " + StringUtils.thousands(Math.floor(_loc2_.bytesPerSec / 1024)) + "KiB/s");
                  tf.appendText("\n   packets remaining : " + _loc2_.getBufferRemainingTime() + " secs");
               }
               else
               {
                  tf.appendText("\n" + (_stream.bytesLoaded / _stream.bytesTotal * 1000) / 1000 + "% (" + _stream.bytesLoaded + " / " + _stream.bytesTotal + " )");
               }
               tf.appendText("\nvideoCodec: " + getVideoCodecName(_stream.videoCodec) + "(" + _stream.videoCodec + ")" + " / audioCodec: " + getAudioCodecName(_stream.audioCodec) + "(" + _stream.audioCodec + ")" + "\ndroppedFrames: " + _stream.info.droppedFrames + " currentBPS: " + Math.floor(stream.info.currentBytesPerSecond) + " playbackBPS: " + Math.floor(stream.info.playbackBytesPerSecond) + "\nfps> " + ["c:" + stats.fps.current," avg: " + stats.fps.avg," min: " + stats.fps.min," max: " + stats.fps.max] + "\nvideo kbps> " + ["c:" + stats.vbps.current," avg: " + stats.vbps.avg," min: " + stats.vbps.min," max: " + stats.vbps.max," total: " + stats.vbps.total] + "\naudio kbps> " + ["c:" + stats.abps.current," avg: " + stats.abps.avg," min: " + stats.abps.min," max: " + stats.abps.max," total: " + stats.abps.total]);
               if(streamUri)
               {
                  tf.appendText("\nstreamUri : " + streamUri);
               }
            }
            catch(e:*)
            {
            }
            if(_stream is ChunkStream)
            {
               tf.appendText("\nchunkIndex: " + (_stream as ChunkStream).chunkIndex + " > " + (_stream as ChunkStream).predictedIndex + ((_stream as ChunkStream).loading?" (loading)":"") + "\nmodule index diff : n/a" + ", prediction diff: " + ((_stream as ChunkStream).chunkIndex - (_stream as ChunkStream).predictedIndex));
            }
         }
         if(_stream)
         {
            if(_video)
            {
               try
               {
                  tf.appendText("\nmedia dimension: " + _video.videoWidth + "x" + _video.videoHeight);
                  if(_video is VideoContainer)
                  {
                     tf.appendText(" stageVideo is " + ((_video as VideoContainer).stageVideo?"enabled":"disabled"));
                  }
               }
               catch(e:*)
               {
               }
            }
            if(_video)
            {
               tf.appendText((rpin != ""?"\n\nrpin: " + rpin:"") + (_ums?"\nstatus: " + _ums.status:"") + (_ums && _ums.ip?"\nconnect" + (_ums.connected?"ed":"ing") + " to: " + _ums.ip:""));
               tf.appendText("\nDebug.enabled: " + tv.ustream.debug.Debug.enabled);
               return;
            }
            tf.appendText((rpin != ""?"\n\nrpin: " + rpin:"") + (_ums?"\nstatus: " + _ums.status:"") + (_ums && _ums.ip?"\nconnect" + (_ums.connected?"ed":"ing") + " to: " + _ums.ip:""));
            tf.appendText("\nDebug.enabled: " + tv.ustream.debug.Debug.enabled);
            return;
         }
         if(_video)
         {
            tf.appendText("\nmedia dimension: " + _video.videoWidth + "x" + _video.videoHeight);
            if(_video is VideoContainer)
            {
               tf.appendText(" stageVideo is " + ((_video as VideoContainer).stageVideo?"enabled":"disabled"));
            }
         }
         tf.appendText((rpin != ""?"\n\nrpin: " + rpin:"") + (_ums?"\nstatus: " + _ums.status:"") + (_ums && _ums.ip?"\nconnect" + (_ums.connected?"ed":"ing") + " to: " + _ums.ip:""));
         tf.appendText("\nDebug.enabled: " + tv.ustream.debug.Debug.enabled);
      }
      
      private function getVideoCodecName(param1:uint) : String {
      }
      
      private function getAudioCodecName(param1:uint) : String {
      }
      
      public function get stream() : NetStream {
         return _stream;
      }
      
      public function set stream(param1:NetStream) : void {
         _stream = param1;
         if(bufferGraph)
         {
            bufferGraph.stream = _stream;
         }
         if(fragmentMap)
         {
            if(_stream is FragmentStream)
            {
               fragmentMap.stream = _stream as FragmentStream;
            }
            else
            {
               fragmentMap.stream = null;
            }
         }
         if(_visible)
         {
            visible = true;
         }
      }
      
      override public function set width(param1:Number) : void {
         if(tf)
         {
            tf.width = param1;
         }
         if(bufferGraph)
         {
            bufferGraph.width = param1;
            bufferGraph.x = param1 - bufferGraph.width;
         }
         if(fragmentMap)
         {
            fragmentMap.width = param1;
            fragmentMap.x = param1 - fragmentMap.width;
         }
      }
      
      override public function set height(param1:Number) : void {
         if(tf)
         {
            tf.height = param1;
         }
         if(bufferGraph)
         {
            bufferGraph.y = param1 - bufferGraph.height;
         }
         if(fragmentMap)
         {
            if(bufferGraph)
            {
               fragmentMap.y = bufferGraph.y - fragmentMap.height;
            }
            else
            {
               fragmentMap.y = param1 - fragmentMap.height;
            }
         }
      }
      
      public function getStat() : String {
         createTfText();
         return tf.text;
      }
      
      public function destroy() : void {
         if(statsTimer)
         {
            statsTimer.stop();
            statsTimer.removeEventListener("timer",onStats);
            statsTimer = null;
         }
         if(hasEventListener("enterFrame"))
         {
            removeEventListener("enterFrame",render);
         }
         if(bufferGraph)
         {
            bufferGraph = bufferGraph.destroy();
         }
         if(fragmentMap)
         {
            fragmentMap.stream = null;
         }
      }
   }
}
