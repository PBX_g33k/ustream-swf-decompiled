package tv.ustream.tools
{
   import flash.events.EventDispatcher;
   import flash.display.Stage;
   import flash.utils.Timer;
   import flash.events.TimerEvent;
   import flash.events.KeyboardEvent;
   import flash.text.TextField;
   import flash.errors.IllegalOperationError;
   
   public class KeyboardManager extends EventDispatcher
   {
      
      public function KeyboardManager() {
         _combos = {};
         _shortKeys = {};
         _keyHistory = [];
         super();
         if(_lock)
         {
            throw new IllegalOperationError("Singleton! use KeyboardManager.getInstance() method.");
         }
         else
         {
            return;
         }
      }
      
      private static var _lock:Boolean = true;
      
      private static var _instance:KeyboardManager;
      
      public static function getInstance() : KeyboardManager {
         if(!_instance)
         {
            _lock = false;
            _instance = new KeyboardManager();
            _lock = true;
         }
         return _instance;
      }
      
      public static function destroy() : void {
         if(_instance)
         {
            _instance.destroy();
         }
      }
      
      private var _combos:Object;
      
      private var _shortKeys:Object;
      
      private var _stage:Stage;
      
      private var _ctrlDown:Boolean = false;
      
      private var _shiftDown:Boolean = false;
      
      private var _altDown:Boolean = false;
      
      private var _lastKeyDown:int = -1;
      
      private var _keyHistory:Array;
      
      private var _timer:Timer;
      
      private var _looksLikeCombo:Boolean;
      
      public function init(param1:Stage) : void {
         if(!_stage)
         {
            _stage = param1;
            _stage.addEventListener("keyDown",onKeyDown);
            _stage.addEventListener("keyUp",onKeyUp);
         }
         _timer = new Timer(1200,1);
         _timer.addEventListener("timer",onTimer);
         _looksLikeCombo = false;
      }
      
      private function onTimer(param1:TimerEvent) : void {
         _timer.reset();
         _keyHistory = [];
         if(_lastKeyDown >= 0 && _shortKeys[_lastKeyDown + "_" + _shiftDown + "_" + _ctrlDown + "_" + _altDown])
         {
            callShortKey(_shortKeys[_lastKeyDown + "_" + _shiftDown + "_" + _ctrlDown + "_" + _altDown]);
         }
         _lastKeyDown = -1;
      }
      
      private function onKeyDown(param1:KeyboardEvent) : void {
         var _loc2_:* = null;
         var _loc3_:* = null;
      }
      
      private function callShortKey(param1:Object) : void {
         _timer.reset();
         if(param1)
         {
            (param1.cb as Function).apply(null,param1.data?[param1.data]:null);
         }
      }
      
      private function onKeyUp(param1:KeyboardEvent) : void {
      }
      
      private function likeCombo(param1:String) : Boolean {
         var _loc4_:* = 0;
         var _loc3_:* = _combos;
         for(_loc2_ in _combos)
         {
            if(_loc2_.indexOf(param1) == 0)
            {
               return true;
            }
         }
         return false;
      }
      
      public function destroy() : void {
         if(_stage)
         {
            _stage.removeEventListener("keyDown",onKeyDown);
            _stage.removeEventListener("keyUp",onKeyUp);
            _stage = null;
         }
         _timer.reset();
         _timer.removeEventListener("timer",onTimer);
      }
      
      public function addShortKeys(param1:uint, param2:Boolean, param3:Boolean, param4:Boolean, param5:Function, param6:Object = null) : void {
         var _loc7_:String = param1 + "_" + param2 + "_" + param3 + "_" + param4;
         trace("3:shortkey: " + _loc7_);
         _shortKeys[_loc7_] = 
            {
               "cb":param5,
               "data":param6
            };
      }
      
      public function addCombo(param1:Array, param2:Function, param3:Object = null) : void {
         var _loc4_:String = param1.join("_");
         _combos[_loc4_] = 
            {
               "cb":param2,
               "data":param3
            };
      }
      
      public function clearAll() : void {
         _combos = {};
         _shortKeys = {};
      }
   }
}
