package tv.ustream.tools
{
   import flash.display.Bitmap;
   import tv.ustream.net.FragmentStream;
   import flash.geom.Rectangle;
   import flash.events.Event;
   import flash.display.BitmapData;
   
   public class FragmentMap extends Bitmap
   {
      
      public function FragmentMap(param1:FragmentStream = null) {
         super(null,"auto",true);
         resize(640,10);
         if(param1)
         {
            this.stream = param1;
         }
      }
      
      private var _stream:FragmentStream;
      
      override public function get width() : Number {
         return bitmapData?bitmapData.width:0;
      }
      
      override public function set width(param1:Number) : void {
         resize(param1?param1:1.0,height?height:1.0);
      }
      
      override public function get height() : Number {
         return bitmapData?bitmapData.height:0;
      }
      
      override public function set height(param1:Number) : void {
         resize(width?width:1.0,param1?param1:1.0);
      }
      
      public function get stream() : FragmentStream {
         return _stream;
      }
      
      public function set stream(param1:FragmentStream) : void {
         unmountStream();
         _stream = param1;
         mountStream();
      }
      
      private function mountStream() : void {
         if(_stream)
         {
            _stream.addEventListener("FragmentStream.Packet.Added",onPacketChange);
            _stream.addEventListener("FragmentStream.Packet.Dropped",onPacketChange);
            _stream.addEventListener("FragmentStream.Packet.Updated",onPacketChange);
         }
      }
      
      private function unmountStream() : void {
         if(_stream)
         {
            _stream.removeEventListener("FragmentStream.Packet.Added",onPacketChange);
            _stream.removeEventListener("FragmentStream.Packet.Dropped",onPacketChange);
            _stream.removeEventListener("FragmentStream.Packet.Updated",onPacketChange);
            bitmapData.fillRect(new Rectangle(0,0,width,height),268476656);
         }
      }
      
      private function onPacketChange(param1:Event) : void {
         if(!hasEventListener("enterFrame"))
         {
            addEventListener("enterFrame",onEnterFrame);
         }
      }
      
      private function onEnterFrame(param1:Event = null) : void {
         var _loc4_:* = 0;
         var _loc5_:* = NaN;
         var _loc3_:* = NaN;
         var _loc2_:BitmapData = bitmapData;
         _loc2_.fillRect(new Rectangle(0,0,width,height),268476656);
         var _loc6_:Array = _stream.getBufferMap();
         _loc4_ = 0;
         while(_loc4_ < _loc6_.length)
         {
            _loc5_ = _loc6_[_loc4_].time;
            _loc3_ = _loc6_[_loc4_].length;
            _loc2_.fillRect(new Rectangle(width * _loc5_,0,width * _loc3_,height),2.147524848E9);
            _loc2_.fillRect(new Rectangle(width * _loc5_,0,1,height),4.27823128E9);
            _loc4_++;
         }
         removeEventListener("enterFrame",onEnterFrame);
      }
      
      override public function set bitmapData(param1:BitmapData) : void {
      }
      
      private function resize(param1:uint, param2:uint) : void {
         .super.bitmapData = new BitmapData(param1,param2,true,0);
         if(_stream)
         {
            onEnterFrame();
         }
      }
   }
}
