package com.google.ads.ima.api
{
   public class AudioMimeTypes extends Object
   {
      
      public function AudioMimeTypes() {
         super();
      }
      
      public static const MP4:String = "audio/mp4";
      
      public static var DEFAULT_MIMETYPES:Array = [AudioMimeTypes.MP4];
   }
}
