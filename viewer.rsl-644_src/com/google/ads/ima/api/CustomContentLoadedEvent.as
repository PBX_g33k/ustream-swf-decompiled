package com.google.ads.ima.api
{
   import flash.events.Event;
   
   public class CustomContentLoadedEvent extends Event
   {
      
      public function CustomContentLoadedEvent(param1:Object, param2:Object = null) {
         super(CUSTOM_CONTENT_LOADED);
         customContentAdValue = param1;
         userRequestContextValue = param2;
      }
      
      public static var CUSTOM_CONTENT_LOADED:String = "customContentLoaded";
      
      public function displayCompanions() : void {
         customContentAdValue.displayCompanions();
      }
      
      public function get userRequestContext() : Object {
         return userRequestContextValue;
      }
      
      override public function clone() : Event {
         return new CustomContentLoadedEvent(customContentAdValue,userRequestContextValue);
      }
      
      ima_internal_api function getCustomContentAd() : Object {
         return customContentAdValue;
      }
      
      private var userRequestContextValue:Object;
      
      public function get content() : String {
         return customContentAdValue.content;
      }
      
      private var customContentAdValue:Object;
   }
}
