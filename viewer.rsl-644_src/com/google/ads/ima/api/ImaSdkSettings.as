package com.google.ads.ima.api
{
   public interface ImaSdkSettings
   {
      
      function set numRedirects(param1:uint) : void;
      
      function set companionBackfill(param1:String) : void;
      
      function set uniqueAds(param1:Boolean) : void;
      
      function set competitiveExclusion(param1:String) : void;
   }
}
