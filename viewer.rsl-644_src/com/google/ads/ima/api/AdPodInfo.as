package com.google.ads.ima.api
{
   public class AdPodInfo extends Object
   {
      
      public function AdPodInfo() {
         super();
      }
      
      public var totalAds:Number = 1;
      
      public var adPosition:Number = 1;
      
      public var isBumper:Boolean;
   }
}
