package com.google.ads.ima.api
{
   public class ViewModes extends Object
   {
      
      public function ViewModes() {
         super();
      }
      
      public static const NORMAL:String = "normal";
      
      public static const IGNORE:String = null;
      
      public static const FULLSCREEN:String = "fullscreen";
      
      public static const THUMBNAIL:String = "thumbnail";
   }
}
