package com.google.ads.ima.api
{
   public class VideoDeliveryTypes extends Object
   {
      
      public function VideoDeliveryTypes() {
         super();
      }
      
      public static var DEFAULT:String = VideoDeliveryTypes.PROGRESSIVE;
      
      public static const STREAMING:String = "streaming";
      
      public static const PROGRESSIVE:String = "progressive";
   }
}
