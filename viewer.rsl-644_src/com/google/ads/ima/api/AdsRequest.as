package com.google.ads.ima.api
{
   public class AdsRequest extends Object
   {
      
      public function AdsRequest() {
         super();
      }
      
      public var adTagUrl:String;
      
      public var disableCompanionAds:Boolean = false;
      
      public var linearAdSlotHeight:Number = 0;
      
      public var adsResponse:String;
      
      public var nonLinearAdSlotWidth:Number = 0;
      
      public var language:String = null;
      
      public var youTubeVideoAdStartDelay:int;
      
      public var linearAdSlotWidth:Number = 0;
      
      public var youTubeAdType:String;
      
      public var youTubeExperimentAndLaunchIds:String;
      
      public var forcedExperimentId:int;
      
      public var contentId:String = null;
      
      public var youTubeExperimentIds:String;
      
      public var isYouTube:Boolean = false;
      
      public var nonLinearAdSlotHeight:Number = 0;
   }
}
