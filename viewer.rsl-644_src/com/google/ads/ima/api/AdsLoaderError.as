package com.google.ads.ima.api
{
   class AdsLoaderError extends Error implements AdError
   {
      
      function AdsLoaderError(param1:String = "", param2:int = 0) {
         super(param1,param2);
      }
      
      public function get adSystems() : Array {
         return adSystemsValue;
      }
      
      public function set adSystems(param1:Array) : void {
         adSystemsValue = param1;
      }
      
      public function set errorType(param1:String) : void {
         this.type = param1;
      }
      
      public function get adIds() : Array {
         return adIdsValue;
      }
      
      public function get innerError() : Error {
         return flashError;
      }
      
      public function get errorMessage() : String {
         return super.message;
      }
      
      public function get errorType() : String {
         return type;
      }
      
      private var adIdsValue:Array;
      
      public function set innerError(param1:Error) : void {
         this.flashError = param1;
      }
      
      public function set adIds(param1:Array) : void {
         adIdsValue = param1;
      }
      
      public function get errorCode() : int {
         return super.errorID;
      }
      
      private var adSystemsValue:Array;
      
      private var flashError:Error;
      
      private var type:String;
   }
}
