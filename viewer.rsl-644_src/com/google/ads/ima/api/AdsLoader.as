package com.google.ads.ima.api
{
   import flash.events.EventDispatcher;
   import com.google.ads.ima.wrappers.ImaSdkSettingsWrapper;
   import com.google.ads.ima.wrappers.Wrappers;
   import flash.utils.Dictionary;
   import flash.events.Event;
   import flash.events.ErrorEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.display.Loader;
   import com.google.utils.HttpSecure;
   import flash.events.IEventDispatcher;
   import com.google.ads.ima.wrappers.AdsLoaderWrapper;
   import com.google.ads.ima.apidependency.SdkStatisticsLoggerImpl;
   import com.google.ads.ima.apidependency.LogIds;
   import flash.system.Security;
   import flash.system.ApplicationDomain;
   import flash.net.URLRequest;
   import flash.utils.getQualifiedClassName;
   
   public class AdsLoader extends EventDispatcher
   {
      
      public function AdsLoader() {
         queuedRequests = [];
         queuedListeners = [];
         sdkLoaderFactory = createSdkLoader;
         super();
         allowTrustedDomains();
      }
      
      private static const REQUEST_ADS_METHOD:String = "requestAds";
      
      private static const SDK_HOST:String = "s0.2mdn.net";
      
      private static const DESTROY_METHOD:String = "destroy";
      
      private static const CONTENT_COMPLETE_METHOD:String = "contentComplete";
      
      private static const QUEUED_REQUEST_TYPE_METHOD:String = "method";
      
      private static const SDK_MAJOR_VERSION:String = "3";
      
      private static var settingsValue:ImaSdkSettingsWrapper;
      
      private static const DOUBLECLICK_MEDIA_SERVER:String = "m1.2mdn.net";
      
      private static const SDK_LOCATION:String = "http://" + SDK_HOST + "/instream/flash/v3/adsapi_";
      
      private static const SDK_LOADER_CLASSNAME:String = "com.google.ads.ima.sdkloader::VersionedSdkLoader";
      
      private static const QUEUED_REQUEST_TYPE_PROPERTY:String = "property";
      
      private function get wrappers() : Wrappers {
         return wrappersValue;
      }
      
      private function dispatchSdkLoadError(param1:String) : void {
         var _loc2_:Dictionary = new Dictionary();
         _loc2_["errMsg"] = param1;
         var _loc3_:AdsLoaderError = new AdsLoaderError(param1);
         _loc3_.errorType = AdErrorTypes.AD_LOAD_ERROR;
         var _loc4_:AdErrorEvent = new AdErrorEvent(_loc3_);
         dispatchEvent(_loc4_);
      }
      
      private function removeSdkLoadListeners() : void {
         loader.removeEventListener(Event.COMPLETE,sdkLoadedHandler);
         loader.removeEventListener(ErrorEvent.ERROR,onSdkLoadError);
         loader.contentLoaderInfo.removeEventListener(Event.COMPLETE,sdkLoaderSwfLoadCompleteHandler);
         loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,onSdkLoadError);
         loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR,onSdkLoadError);
      }
      
      private function setWrapperProperty(param1:String, param2:*) : void {
         if(param1 in adsLoaderWrapper)
         {
            adsLoaderWrapper[param1] = param2;
         }
         else
         {
            dispatchSdkLoadError("Internal error: No such property: " + param1);
         }
      }
      
      override public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void {
         var _loc4_:Object = null;
         if(isLocallyDispatchedEvent(param1))
         {
            super.removeEventListener(param1,param2);
         }
         if(adsLoaderWrapper != null)
         {
            adsLoaderWrapper.removeEventListener(param1,param2,param3);
         }
         else
         {
            for each(_loc4_ in queuedListeners)
            {
               if(param1 == _loc4_.type && param2 == _loc4_.listener)
               {
                  queuedListeners.splice(queuedListeners.indexOf(_loc4_),1);
                  break;
               }
            }
         }
      }
      
      protected function isLocallyDispatchedEvent(param1:String) : Boolean {
         return param1 == AdErrorEvent.AD_ERROR;
      }
      
      private var queuedListeners:Array;
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
         if(isLocallyDispatchedEvent(param1))
         {
            super.addEventListener(param1,param2,param3,param4,param5);
         }
         if(adsLoaderWrapper != null)
         {
            adsLoaderWrapper.addEventListener(param1,param2,param3,param4,param5);
         }
         else
         {
            queuedListeners.push(
               {
                  "type":param1,
                  "listener":param2,
                  "useCapture":param3,
                  "priority":param4,
                  "useWeakReference":param5
               });
         }
      }
      
      private var wrappersValue:Wrappers;
      
      public function contentComplete(param1:String = null) : void {
         invokeRemoteMethod(CONTENT_COMPLETE_METHOD,param1);
      }
      
      private var loader:Loader;
      
      private function get sdkUrl() : String {
         var _loc1_:String = SDK_MAJOR_VERSION + ".swf";
         var _loc2_:String = SDK_LOCATION + _loc1_;
         return HttpSecure.getInstance().useCorrectProtocolForUrl(_loc2_);
      }
      
      private var queuedRequests:Array;
      
      private function createAdsLoaderWrapper(param1:Object) : IEventDispatcher {
         if(settingsValue != null)
         {
            settingsValue.invokeDelayedMethods(param1.settings);
         }
         return new AdsLoaderWrapper(wrappers,param1,this);
      }
      
      var sdkLoaderFactory:Function;
      
      private function invokeRemoteMethod(param1:String, ... rest) : void {
         if(adsLoaderWrapper != null)
         {
            invokeWrapperMethod(param1,rest);
         }
         else
         {
            queuedRequests.push(
               {
                  "requestType":QUEUED_REQUEST_TYPE_METHOD,
                  "method":param1,
                  "args":rest
               });
            load();
         }
      }
      
      public function requestAds(param1:AdsRequest, param2:Object = null) : void {
         SdkStatisticsLoggerImpl.instance.reportApi(LogIds.AdsLoader_requestAds);
         invokeRemoteMethod(REQUEST_ADS_METHOD,param1,param2);
      }
      
      public function get settings() : ImaSdkSettings {
         if(settingsValue == null)
         {
            settingsValue = new ImaSdkSettingsWrapper(wrappers,null);
         }
         return settingsValue;
      }
      
      private function processQueuedListeners() : void {
         var _loc1_:Object = null;
         for each(_loc1_ in queuedListeners)
         {
            adsLoaderWrapper.addEventListener(_loc1_.type,_loc1_.listener,_loc1_.useCapture,_loc1_.priority,_loc1_.useWeakReference);
         }
         queuedListeners = [];
      }
      
      private function addSdkLoadListeners() : void {
         loader.addEventListener(Event.COMPLETE,sdkLoadedHandler);
         loader.addEventListener(ErrorEvent.ERROR,onSdkLoadError);
         loader.addEventListener(ErrorEvent.ERROR,onSdkLoadError);
         loader.contentLoaderInfo.addEventListener(Event.COMPLETE,sdkLoaderSwfLoadCompleteHandler);
         loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,onSdkLoadError);
         loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR,onSdkLoadError);
      }
      
      private function allowTrustedDomains() : void {
         Security.allowDomain(SDK_HOST);
         Security.allowDomain(DOUBLECLICK_MEDIA_SERVER);
         Security.allowInsecureDomain(DOUBLECLICK_MEDIA_SERVER);
      }
      
      private function createSdkLoader() : Loader {
         try
         {
            return new SdkSwfLoader(ApplicationDomain.currentDomain);
         }
         catch(error:Error)
         {
         }
         return new SdkSwfLoader(null);
      }
      
      private function sdkLoadedHandler(param1:Event) : void {
         removeSdkLoadListeners();
         var _loc2_:Object = param1;
         wrappersValue = new Wrappers(_loc2_.remoteApplicationDomain);
         adsLoaderWrapper = createAdsLoaderWrapper(_loc2_.adsLoader);
         if(adsLoaderWrapper != null)
         {
            processQueuedListeners();
            processQueuedRequests();
         }
         else
         {
            dispatchSdkLoadError("Internal error: remote wrapper is null");
         }
      }
      
      private function load() : void {
         var _loc1_:URLRequest = null;
         if(loader == null)
         {
            loader = sdkLoaderFactory();
            addSdkLoadListeners();
            _loc1_ = new URLRequest(sdkUrl);
            loader.load(_loc1_);
         }
      }
      
      private var adsLoaderWrapper:IEventDispatcher;
      
      private function processQueuedRequests() : void {
         var _loc1_:Object = null;
         for each(_loc1_ in queuedRequests)
         {
            switch(_loc1_.requestType)
            {
               case QUEUED_REQUEST_TYPE_METHOD:
                  invokeWrapperMethod(_loc1_.method,_loc1_.args);
                  continue;
               case QUEUED_REQUEST_TYPE_PROPERTY:
                  setWrapperProperty(_loc1_.propertyName,_loc1_.propertyValue);
                  continue;
               default:
                  continue;
            }
         }
         queuedRequests = [];
      }
      
      private function sdkLoaderSwfLoadCompleteHandler(param1:Event) : void {
         var loadedClassName:String = null;
         var event:Event = param1;
         loader.contentLoaderInfo.removeEventListener(Event.COMPLETE,sdkLoaderSwfLoadCompleteHandler);
         try
         {
            loadedClassName = getQualifiedClassName(loader.content);
            if(loadedClassName != SDK_LOADER_CLASSNAME)
            {
               handleSdkLoadError("SDK could not be loaded from " + sdkUrl);
            }
         }
         catch(error:SecurityError)
         {
            handleSdkLoadError("SDK could not be loaded from " + sdkUrl);
         }
      }
      
      private function handleSdkLoadError(param1:String) : void {
         removeSdkLoadListeners();
         dispatchSdkLoadError(param1);
      }
      
      public function destroy() : void {
         if(loader != null)
         {
            invokeRemoteMethod(DESTROY_METHOD);
            removeSdkLoadListeners();
            if(loader.parent != null)
            {
               loader.parent.removeChild(loader);
            }
            if(loader.hasOwnProperty("unloadAndStop"))
            {
               loader["unloadAndStop"]();
            }
            else
            {
               loader.unload();
            }
            loader = null;
            queuedRequests = [];
            queuedListeners = [];
         }
      }
      
      private function invokeWrapperMethod(param1:String, param2:Array) : void {
         var _loc3_:Function = adsLoaderWrapper[param1];
         if(_loc3_ != null)
         {
            _loc3_.apply(adsLoaderWrapper,param2);
         }
         else
         {
            dispatchSdkLoadError("Internal error: No such method: " + param1);
         }
      }
      
      private function onSdkLoadError(param1:ErrorEvent) : void {
         handleSdkLoadError(param1.text);
      }
   }
}
