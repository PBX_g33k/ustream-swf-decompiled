package com.google.ads.ima.api
{
   public class AdErrorTypes extends Object
   {
      
      public function AdErrorTypes() {
         super();
      }
      
      public static const AD_PLAY_ERROR:String = "adPlayError";
      
      public static const AD_LOAD_ERROR:String = "adLoadError";
   }
}
