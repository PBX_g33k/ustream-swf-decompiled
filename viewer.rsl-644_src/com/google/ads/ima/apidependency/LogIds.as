package com.google.ads.ima.apidependency
{
   public class LogIds extends Object
   {
      
      public function LogIds() {
         super();
      }
      
      public static const VastVideoAdTracker_firstQuartile:uint = 41;
      
      public static const VastAdTrackerImpression:uint = 57;
      
      public static const DoubleclickInStreamSDK_onFlashInFlashAdLoaded:uint = 11;
      
      public static const VastVideoAdTracker_videoStop:uint = 40;
      
      public static const DoubleclickAdImpl_getCompanionAdUrl:uint = 44;
      
      public static const AdsLoaderImpl_adsResponseReceivedHandler:uint = 6;
      
      public static const YouTubeChromelessPlaybackImpl_playerErrorHandler:uint = 51;
      
      public static const BaseLoader_sdkLoaderSwfLoadCompleteHandler:uint = 3;
      
      public static const AdData:uint = 58;
      
      public static const ResourceLoadLogger_errorHandler:uint = 21;
      
      public static const DoubleclickInStreamSDK_createAd:uint = 22;
      
      public static const DoubleclickInStreamSDK_onVastAdLoaded:uint = 12;
      
      public static const AdsLoader_requestAds:uint = 1;
      
      public static const VastVideoAdTracker_videoStart:uint = 35;
      
      public static const VastVideoAdTracker_videoMute:uint = 43;
      
      public static const AdsResponseFactory_logAdsResponseType:uint = 53;
      
      public static const DoubleclickInStreamSDK_onVastWrapperAdLoaded:uint = 13;
      
      public static const VastVideoAdTracker_videoPause:uint = 39;
      
      public static const ErrorUtils_dispatchAdError:uint = 19;
      
      public static const ResourceLoadLogger_logTimeout:uint = 20;
      
      public static const DoubleclickInStreamSDK_onCustomAdLoaded:uint = 15;
      
      public static const VastVideoAdTracker_videoComplete:uint = 36;
      
      public static const AbstractAdsmanager_adErrorEventHandler:uint = 55;
      
      public static const GptCompanionsService_reportGptPresence:uint = 50;
      
      public static const AdsLoader_dispatchSdkLoadError:uint = 2;
      
      public static const DoubleclickInStreamSDK_onSdkLoaded:uint = 8;
      
      public static const DoubleclickInStreamSDK_dispatchAdsLoaded:uint = 16;
      
      public static const AdsManagerImpl_onAdError:uint = 18;
      
      public static const DoubleclickInStreamSDK_onAdLoadError:uint = 17;
      
      public static const DoubleclickInStreamSDK_onSdkLoadError:uint = 9;
      
      public static const AdsLoaderImpl_requestAdsInternal:uint = 56;
      
      public static const VastVideoAdTracker_videoClick:uint = 37;
      
      public static const AdsResponseFactory_adError:uint = 52;
      
      public static const VastWrapperRedirectTiming:uint = 59;
      
      public static const VastVideoAdTracker_videoMidpoint:uint = 38;
      
      public static const UserChoiceAdTracker_sendLogEvent:uint = 23;
      
      public static const DoubleclickVastVideoAdImpl_getCompanionAds:uint = 24;
      
      public static const AdsLoaderImpl_adErrorEventHandler:uint = 54;
      
      public static const AdsLoaderImpl_requestAds:uint = 5;
      
      public static const BaseLoader_sdkLoadedHandler:uint = 4;
      
      public static const DoubleclickInStreamSDK_onVastNonLinearAdLoaded:uint = 14;
      
      public static const VastVideoAdTracker_thirdQuartile:uint = 42;
      
      public static const DoubleclickInStreamSDK_onDartInStreamAdLoaded:uint = 10;
      
      public static const DoubleclickInStreamSDK_loadSdk:uint = 7;
   }
}
