package com.google.ads.ima.apidependency
{
   public class LoggablePoint extends Object
   {
      
      public function LoggablePoint(param1:uint, param2:Boolean, param3:Boolean, param4:String = null) {
         super();
         this.idValue = param1;
         this.youtubeValue = param2;
         this.logAlwaysValue = param3;
         this.experimentIdValue = param4 == ""?null:param4;
      }
      
      public function toString() : String {
         return "id:" + id + " youtube:" + logForYoutubeOnly + " logAlways:" + logAlways + " experimentId:" + experimentId;
      }
      
      public function get logForYoutubeOnly() : Boolean {
         return youtubeValue;
      }
      
      private var youtubeValue:Boolean;
      
      public function get logAlways() : Boolean {
         return logAlwaysValue;
      }
      
      private var idValue:uint;
      
      public function get experimentId() : String {
         return experimentIdValue;
      }
      
      private var experimentIdValue:String;
      
      public function get id() : uint {
         return idValue;
      }
      
      private var logAlwaysValue:Boolean;
      
      public function equals(param1:LoggablePoint) : Boolean {
         return id == param1.id && logForYoutubeOnly == param1.logForYoutubeOnly && logAlways == param1.logAlways && experimentId == param1.experimentId;
      }
   }
}
