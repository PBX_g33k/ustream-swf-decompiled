package com.google.ads.ima.common
{
   public class MathUtils extends Object
   {
      
      public function MathUtils() {
         super();
      }
      
      public static function isRandomlySelected(param1:Number) : Boolean {
         if(param1 <= 0)
         {
            return false;
         }
         if(param1 >= 1)
         {
            return true;
         }
         return Math.random() <= param1;
      }
   }
}
