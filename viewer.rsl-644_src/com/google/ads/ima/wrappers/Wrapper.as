package com.google.ads.ima.wrappers
{
   import flash.events.EventDispatcher;
   import flash.utils.Dictionary;
   
   class Wrapper extends EventDispatcher
   {
      
      function Wrapper(param1:Wrappers, param2:Object, param3:Object = null) {
         remoteMethodResultsDictionary = new Dictionary();
         super();
         wrappersValue = param1;
         remoteMethodResultsDictionary = new Dictionary();
         remoteInstanceValue = param2;
         localInstanceValue = param3;
      }
      
      public function get localInstance() : Object {
         return localInstanceValue;
      }
      
      private var remoteMethodResultsDictionary:Dictionary;
      
      protected function get wrappers() : Wrappers {
         return wrappersValue;
      }
      
      private var localInstanceValue:Object;
      
      protected function get remoteMethodResultsStore() : Dictionary {
         return remoteMethodResultsDictionary;
      }
      
      private var remoteInstanceValue:Object;
      
      private var wrappersValue:Wrappers;
      
      public function get remoteInstance() : Object {
         return remoteInstanceValue;
      }
      
      public function set remoteInstance(param1:Object) : void {
         remoteInstanceValue = param1;
      }
   }
}
