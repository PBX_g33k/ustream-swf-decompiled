package com.google.ads.ima.wrappers
{
   import com.google.ads.ima.api.AdError;
   import flash.utils.Dictionary;
   
   class AdErrorWrapper extends Error implements AdError
   {
      
      function AdErrorWrapper(param1:Wrappers, param2:Object, param3:Object = null) {
         super();
         remoteMethodResultsDictionary = new Dictionary();
         this.remoteInstanceReference = param2;
         this.localInstanceReference = param3;
         wrappersValue = param1;
      }
      
      public function get adIds() : Array {
         return remoteInstance.adIds;
      }
      
      private var remoteMethodResultsDictionary:Dictionary;
      
      public function get localInstance() : Object {
         return localInstanceReference;
      }
      
      public function get adSystems() : Array {
         return remoteInstance.adSystems;
      }
      
      public function get errorType() : String {
         return remoteInstance.errorType;
      }
      
      private var localInstanceReference:Object;
      
      private var remoteInstanceReference:Object;
      
      private var wrappersValue:Wrappers;
      
      protected function get remoteMethodResultsStore() : Dictionary {
         return remoteMethodResultsDictionary;
      }
      
      public function get errorCode() : int {
         return remoteInstance.errorCode;
      }
      
      public function get innerError() : Error {
         return remoteInstance.innerError;
      }
      
      public function get remoteInstance() : Object {
         return remoteInstanceReference;
      }
      
      public function get errorMessage() : String {
         return remoteInstance.errorMessage;
      }
   }
}
