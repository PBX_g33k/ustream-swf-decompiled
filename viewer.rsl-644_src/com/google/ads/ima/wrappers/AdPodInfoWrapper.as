package com.google.ads.ima.wrappers
{
   import com.google.ads.ima.api.AdPodInfo;
   
   class AdPodInfoWrapper extends AdPodInfo
   {
      
      function AdPodInfoWrapper(param1:Wrappers, param2:Object, param3:Object = null) {
         super();
         this.remoteInstance = param2;
         this.localInstance = param3;
         wrappersValue = param1;
         totalAds = param2.totalAds;
         adPosition = param2.adPosition;
         isBumper = param2.isBumper;
      }
      
      private var localInstance:Object;
      
      private var wrappersValue:Wrappers;
      
      private var remoteInstance:Object;
   }
}
