package com.google.utils
{
   import flash.display.DisplayObject;
   import flash.events.EventDispatcher;
   import flash.events.Event;
   import flash.system.Capabilities;
   import flash.utils.getQualifiedClassName;
   
   public class Trace extends Object
   {
      
      public function Trace() {
         super();
      }
      
      public static function debug(param1:String) : void {
         trace("debug",param1);
         if(useConsoleLogForDebug)
         {
            ExternalInterfaceUtils.makeExternalCall("console.debug",param1);
         }
      }
      
      public static function traceUncaughtErrors(param1:DisplayObject) : void {
         addUncaughtErrorEventlistener(param1);
         if(param1.hasOwnProperty("loaderInfo"))
         {
            addUncaughtErrorEventlistener(param1.loaderInfo);
         }
      }
      
      public static var useConsoleLogForDebug:Boolean = false;
      
      public static function traceObject(param1:Object, param2:String = "") : void {
         Trace.info(buildObjectTrace(param1,param2));
      }
      
      public static function error(param1:String) : void {
         trace("error",param1);
         ExternalInterfaceUtils.makeExternalCall("console.error",param1);
      }
      
      private static function addUncaughtErrorEventlistener(param1:EventDispatcher) : void {
         if((param1) && (param1.hasOwnProperty("uncaughtErrorEvents")))
         {
            Object(param1).uncaughtErrorEvents.addEventListener("uncaughtError",uncaughtErrorEventHandler,false,0,true);
         }
      }
      
      private static function uncaughtErrorEventHandler(param1:Event) : void {
         var _loc3_:Error = null;
         var _loc2_:String = "Uncaught error: " + param1;
         if(param1.hasOwnProperty("error"))
         {
            if(param1["error"] is Error)
            {
               _loc3_ = param1["error"] as Error;
               _loc2_ = "Uncaught error: " + _loc3_;
            }
            else
            {
               _loc2_ = "Uncaught error: " + param1["error"];
            }
         }
         if(Capabilities.isDebugger)
         {
            Trace.error(_loc2_);
         }
         else
         {
            Trace.error(_loc2_ + "\n" + "Use debug flash player to get full trace");
         }
      }
      
      public static function warning(param1:String) : void {
         trace("warning",param1);
         ExternalInterfaceUtils.makeExternalCall("console.warning",param1);
      }
      
      public static function info(param1:String) : void {
         trace("info",param1);
         if(useConsoleLogForDebug)
         {
            ExternalInterfaceUtils.makeExternalCall("console.info",param1);
         }
      }
      
      public static function buildObjectTrace(param1:Object, param2:String = "") : String {
         var _loc4_:String = null;
         var _loc3_:String = "";
         for(_loc4_ in param1)
         {
            if(typeof param1[_loc4_] == "object")
            {
               _loc3_ = _loc3_ + (param2 + _loc4_ + ": [" + getQualifiedClassName(param1[_loc4_]) + "]\n");
               _loc3_ = _loc3_ + buildObjectTrace(param1[_loc4_],"  " + param2);
            }
            else
            {
               _loc3_ = _loc3_ + (param2 + _loc4_ + ": " + param1[_loc4_] + "\n");
            }
         }
         return _loc3_;
      }
   }
}
