package com.google.utils
{
   import flash.utils.Dictionary;
   
   public class SingletonManager extends Object
   {
      
      public function SingletonManager(param1:Class) {
         instanceMap = new Dictionary();
         super();
         baseClass = param1;
         defaultClass = baseClass;
      }
      
      private var defaultClass:Class;
      
      private var baseClass:Class;
      
      public function getInstance(param1:Class = null) : Object {
         var param1:Class = param1 == null?defaultClass:param1;
         var _loc2_:Object = instanceMap[param1];
         if(_loc2_ == null)
         {
            _loc2_ = new param1();
            if(!(_loc2_ is baseClass))
            {
               throw new Error(param1 + " must extend " + baseClass + " in " + "order to be accessed through the same SingletonManager.");
            }
            else if(_loc2_ != instanceMap[param1])
            {
               throw new Error("The constructor of " + param1 + " did not " + "validate the new instance using validateAndStoreInstance.");
            }
            
         }
         defaultClass = param1;
         return _loc2_;
      }
      
      public function validateAndStoreInstance(param1:Object) : void {
         var _loc2_:Class = param1.constructor as Class;
         if(instanceMap[_loc2_] != null)
         {
            throw new Error(_loc2_ + " is a singleton. Access it using the " + "correct static method.");
         }
         else
         {
            instanceMap[_loc2_] = param1;
            return;
         }
      }
      
      private var instanceMap:Dictionary;
   }
}
