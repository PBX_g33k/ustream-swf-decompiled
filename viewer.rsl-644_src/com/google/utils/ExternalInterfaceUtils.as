package com.google.utils
{
   import flash.external.ExternalInterface;
   
   public class ExternalInterfaceUtils extends Object
   {
      
      public function ExternalInterfaceUtils() {
         super();
      }
      
      public static function executeJavascriptFunction(param1:String) : Object {
         var jsFunctionCode:String = param1;
         try
         {
            if(ExternalInterface.available)
            {
               return ExternalInterface.call(jsFunctionCode) as Object;
            }
         }
         catch(error:SecurityError)
         {
         }
         return null;
      }
      
      public static function getUserAgent() : String {
         return makeExternalCall("navigator.userAgent.toString") as String;
      }
      
      public static function exposeFunctionToJavascript(param1:String, param2:Function) : void {
         var functionName:String = param1;
         var closure:Function = param2;
         try
         {
            if(ExternalInterface.available)
            {
               ExternalInterface.addCallback(functionName,closure);
            }
         }
         catch(error:SecurityError)
         {
         }
      }
      
      public static function makeExternalCall(param1:String, ... rest) : Object {
         var functionName:String = param1;
         var args:Array = rest;
         try
         {
            if(ExternalInterface.available)
            {
               args.unshift(functionName);
               return ExternalInterface.call.apply(null,args) as Object;
            }
         }
         catch(error:SecurityError)
         {
         }
         return null;
      }
   }
}
