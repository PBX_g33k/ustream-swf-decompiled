package com.adobe.net
{
   import flash.utils.ByteArray;
   
   public class URIEncodingBitmap extends ByteArray
   {
      
      public function URIEncodingBitmap(param1:String) {
         var _loc4_:* = 0;
         var _loc2_:* = 0;
         var _loc5_:* = 0;
         super();
         var _loc3_:ByteArray = new ByteArray();
         _loc4_ = 0;
         while(_loc4_ < 16)
         {
            this.writeByte(0);
            _loc4_++;
         }
         _loc3_.writeUTFBytes(param1);
         _loc3_.position = 0;
         while(_loc3_.bytesAvailable)
         {
            _loc2_ = _loc3_.readByte();
            if(_loc2_ <= 127)
            {
               this.position = _loc2_ >> 3;
               _loc5_ = this.readByte();
               _loc5_ = _loc5_ | 1 << (_loc2_ & 7);
               this.position = _loc2_ >> 3;
               this.writeByte(_loc5_);
            }
         }
      }
      
      public function ShouldEscape(param1:String) : int {
         var _loc4_:* = 0;
         var _loc2_:* = 0;
         var _loc3_:ByteArray = new ByteArray();
         _loc3_.writeUTFBytes(param1);
         _loc3_.position = 0;
         _loc2_ = _loc3_.readByte();
         if(_loc2_ & 128)
         {
            return 0;
         }
         if(_loc2_ < 31 || _loc2_ == 127)
         {
            return _loc2_;
         }
         this.position = _loc2_ >> 3;
         _loc4_ = this.readByte();
         if(_loc4_ & 1 << (_loc2_ & 7))
         {
            return _loc2_;
         }
         return 0;
      }
   }
}
