package com.worlize.websocket
{
   import flash.events.EventDispatcher;
   import com.adobe.net.URI;
   import flash.net.Socket;
   import flash.utils.ByteArray;
   import flash.utils.Timer;
   import com.adobe.net.URIEncodingBitmap;
   import com.hurlant.util.Base64;
   import flash.events.TimerEvent;
   import flash.events.Event;
   import flash.events.ProgressEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import com.adobe.crypto.SHA1;
   import com.adobe.utils.StringUtil;
   
   public class WebSocket extends EventDispatcher
   {
      
      public function WebSocket(param1:String, param2:String, param3:* = null, param4:uint = 10000, param5:int = 0) {
         var _loc6_:* = 0;
         URIpathExcludedBitmap = new URIEncodingBitmap(" %?#");
         config = new WebSocketConfig();
         super(null);
         _uri = new URI(param1);
         if(param3 is String)
         {
            _protocols = [param3];
         }
         else
         {
            _protocols = param3;
         }
         if(_protocols)
         {
            _loc6_ = 0;
            while(_loc6_ < _protocols.length)
            {
               _protocols[_loc6_] = StringUtil.trim(_protocols[_loc6_]);
               _loc6_++;
            }
         }
         _origin = param2;
         this.timeout = param4;
         this.handshakeTimeout = param4;
         _desiredUnsecurePort = param5;
         init();
      }
      
      private static const MODE_UTF8:int = 0;
      
      private static const MODE_BINARY:int = 0;
      
      private static const MAX_HANDSHAKE_BYTES:int = 10240;
      
      public static var logger:Function = function(param1:String):void
      {
      };
      
      private var _bufferedAmount:int = 0;
      
      private var _readyState:int;
      
      private var _uri:URI;
      
      private var _protocols:Array;
      
      private var _serverProtocol:String;
      
      private var _host:String;
      
      private var _port:uint;
      
      private var _resource:String;
      
      private var _secure:Boolean;
      
      private var _origin:String;
      
      private var _useNullMask:Boolean = false;
      
      private var _desiredUnsecurePort:uint = 0;
      
      private var rawSocket:Socket;
      
      private var socket:Socket;
      
      private var timeout:uint;
      
      private var fatalError:Boolean = false;
      
      private var nonce:ByteArray;
      
      private var base64nonce:String;
      
      private var serverHandshakeResponse:String;
      
      private var serverExtensions:Array;
      
      private var currentFrame:WebSocketFrame;
      
      private var frameQueue:Vector.<WebSocketFrame>;
      
      private var fragmentationOpcode:int = 0;
      
      private var fragmentationSize:uint = 0;
      
      private var waitingForServerClose:Boolean = false;
      
      private var closeTimeout:int = 5000;
      
      private var closeTimer:Timer;
      
      private var handshakeBytesReceived:int;
      
      private var handshakeTimer:Timer;
      
      private var handshakeTimeout:int = 10000;
      
      private var URIpathExcludedBitmap:URIEncodingBitmap;
      
      public var config:WebSocketConfig;
      
      public var debug:Boolean = false;
      
      private function init() : void {
         parseUrl();
         validateProtocol();
         frameQueue = new Vector.<WebSocketFrame>();
         fragmentationOpcode = 0;
         fragmentationSize = 0;
         currentFrame = new WebSocketFrame();
         fatalError = false;
         closeTimer = new Timer(closeTimeout,1);
         closeTimer.addEventListener("timer",handleCloseTimer);
         handshakeTimer = new Timer(handshakeTimeout,1);
         handshakeTimer.addEventListener("timer",handleHandshakeTimer);
         socket = new Socket();
         rawSocket = new Socket();
         socket.timeout = timeout;
         if(secure)
         {
         }
         rawSocket.addEventListener("connect",handleSocketConnect);
         rawSocket.addEventListener("ioError",handleSocketIOError);
         rawSocket.addEventListener("securityError",handleSocketSecurityError);
         socket.addEventListener("close",handleSocketClose);
         socket.addEventListener("socketData",handleSocketData);
         _readyState = 3;
      }
      
      private function validateProtocol() : void {
         var _loc6_:* = null;
         var _loc1_:* = 0;
         var _loc2_:* = null;
         var _loc4_:* = 0;
         var _loc5_:* = 0;
         var _loc3_:* = null;
         if(_protocols)
         {
            _loc6_ = ["(",")","<",">","@",",",";",":","\\","\"","/","[","]","?","=","{","}"," ",String.fromCharCode(9)];
            _loc1_ = 0;
            while(_loc1_ < _protocols.length)
            {
               _loc2_ = _protocols[_loc1_];
               _loc4_ = 0;
               while(_loc4_ < _loc2_.length)
               {
                  _loc5_ = _loc2_.charCodeAt(_loc4_);
                  _loc3_ = _loc2_.charAt(_loc4_);
                  if(_loc5_ < 33 || _loc5_ > 126 || !(_loc6_.indexOf(_loc3_) === -1))
                  {
                     throw new WebSocketError("Illegal character \'" + String.fromCharCode(_loc3_) + "\' in subprotocol.");
                  }
                  else
                  {
                     _loc4_++;
                     continue;
                  }
               }
               _loc1_++;
            }
         }
      }
      
      public function connect() : void {
         if(_readyState === 3 || _readyState === 2)
         {
            _readyState = 0;
            generateNonce();
            handshakeBytesReceived = 0;
            rawSocket.connect(_host,_port);
            if(debug)
            {
               logger("Connecting to " + _host + " on port " + _port);
            }
         }
      }
      
      private function parseUrl() : void {
         _host = _uri.authority;
         var _loc2_:String = _uri.scheme.toLocaleLowerCase();
         if(_loc2_ === "wss")
         {
            _secure = true;
            _port = 443;
         }
         else if(_loc2_ === "ws")
         {
            _secure = false;
            _port = _desiredUnsecurePort != 0?_desiredUnsecurePort:80;
         }
         else
         {
            throw new Error("Unsupported scheme: " + _loc2_);
         }
         
         var _loc4_:uint = parseInt(_uri.port,10);
         if(!isNaN(_loc4_) && !(_loc4_ === 0))
         {
            _port = _loc4_;
         }
         var _loc1_:String = URI.fastEscapeChars(_uri.path,URIpathExcludedBitmap);
         if(_loc1_.length === 0)
         {
            _loc1_ = "/";
         }
         var _loc3_:String = _uri.queryRaw;
         if(_loc3_.length > 0)
         {
            _loc3_ = "?" + _loc3_;
         }
         _resource = _loc1_ + _loc3_;
      }
      
      private function generateNonce() : void {
         var _loc1_:* = 0;
         nonce = new ByteArray();
         _loc1_ = 0;
         while(_loc1_ < 16)
         {
            nonce.writeByte(Math.round(Math.random() * 255));
            _loc1_++;
         }
         nonce.position = 0;
         base64nonce = Base64.encodeByteArray(nonce);
      }
      
      public function get readyState() : int {
         return _readyState;
      }
      
      public function get bufferedAmount() : int {
         return _bufferedAmount;
      }
      
      public function get uri() : String {
         var _loc1_:* = null;
         _loc1_ = _secure?"wss://":"ws://";
         _loc1_ = _loc1_ + _host;
         if(_secure && !(_port === 443) || (!_secure && !(_port === 80)))
         {
            _loc1_ = _loc1_ + (":" + _port.toString());
         }
         _loc1_ = _loc1_ + _resource;
         return _loc1_;
      }
      
      public function get protocol() : String {
         return _serverProtocol;
      }
      
      public function get extensions() : Array {
         return [];
      }
      
      public function get host() : String {
         return _host;
      }
      
      public function get port() : uint {
         return _port;
      }
      
      public function get resource() : String {
         return _resource;
      }
      
      public function get secure() : Boolean {
         return _secure;
      }
      
      public function get connected() : Boolean {
         return readyState === 1;
      }
      
      public function set useNullMask(param1:Boolean) : void {
         _useNullMask = param1;
      }
      
      public function get useNullMask() : Boolean {
         return _useNullMask;
      }
      
      private function verifyConnectionForSend() : void {
         if(_readyState === 0)
         {
            throw new WebSocketError("Invalid State: Cannot send data before connected.");
         }
         else
         {
            return;
         }
      }
      
      public function sendUTF(param1:String) : void {
         verifyConnectionForSend();
         var _loc2_:WebSocketFrame = new WebSocketFrame();
         _loc2_.opcode = 1;
         _loc2_.binaryPayload = new ByteArray();
         _loc2_.binaryPayload.writeMultiByte(param1,"utf-8");
         fragmentAndSend(_loc2_);
      }
      
      public function sendBytes(param1:ByteArray) : void {
         verifyConnectionForSend();
         var _loc2_:WebSocketFrame = new WebSocketFrame();
         _loc2_.opcode = 2;
         _loc2_.binaryPayload = param1;
         fragmentAndSend(_loc2_);
      }
      
      public function ping(param1:ByteArray = null) : void {
         verifyConnectionForSend();
         var _loc2_:WebSocketFrame = new WebSocketFrame();
         _loc2_.fin = true;
         _loc2_.opcode = 9;
         if(param1)
         {
            _loc2_.binaryPayload = param1;
         }
         sendFrame(_loc2_);
      }
      
      private function pong(param1:ByteArray = null) : void {
         verifyConnectionForSend();
         var _loc2_:WebSocketFrame = new WebSocketFrame();
         _loc2_.fin = true;
         _loc2_.opcode = 10;
         _loc2_.binaryPayload = param1;
         sendFrame(_loc2_);
      }
      
      private function fragmentAndSend(param1:WebSocketFrame) : void {
         var _loc4_:* = 0;
         var _loc3_:* = 0;
         var _loc6_:* = 0;
         var _loc5_:* = null;
         var _loc2_:* = 0;
         if(param1.opcode > 7)
         {
            throw new WebSocketError("You cannot fragment control frames.");
         }
         else
         {
            _loc7_ = config.fragmentationThreshold;
            if(config.fragmentOutgoingMessages && param1.binaryPayload && param1.binaryPayload.length > _loc7_)
            {
               param1.binaryPayload.position = 0;
               _loc4_ = param1.binaryPayload.length;
               _loc3_ = Math.ceil(_loc4_ / _loc7_);
               _loc6_ = 1;
               while(_loc6_ <= _loc3_)
               {
                  _loc5_ = new WebSocketFrame();
                  _loc5_.opcode = _loc6_ === 1?param1.opcode:0;
                  _loc5_.fin = _loc6_ === _loc3_;
                  _loc2_ = _loc6_ === _loc3_?_loc4_ - _loc7_ * (_loc6_ - 1):_loc7_;
                  param1.binaryPayload.position = _loc7_ * (_loc6_ - 1);
                  _loc5_.binaryPayload = new ByteArray();
                  param1.binaryPayload.readBytes(_loc5_.binaryPayload,0,_loc2_);
                  sendFrame(_loc5_);
                  _loc6_++;
               }
            }
            else
            {
               param1.fin = true;
               sendFrame(param1);
            }
            return;
         }
      }
      
      private function sendFrame(param1:WebSocketFrame, param2:Boolean = false) : void {
         param1.mask = true;
         param1.useNullMask = _useNullMask;
         var _loc3_:ByteArray = new ByteArray();
         param1.send(_loc3_);
         sendData(_loc3_);
      }
      
      private function sendData(param1:ByteArray, param2:Boolean = false) : void {
         if(!connected)
         {
            return;
         }
         param1.position = 0;
         socket.writeBytes(param1,0,param1.bytesAvailable);
         socket.flush();
         param1.clear();
      }
      
      public function close(param1:Boolean = true) : void {
         var _loc3_:* = null;
         var _loc2_:* = null;
         if(!socket.connected && _readyState === 0)
         {
            _readyState = 2;
            try
            {
               socket.close();
            }
            catch(e:Error)
            {
            }
         }
         if(socket.connected)
         {
            _loc3_ = new WebSocketFrame();
            _loc6_ = false;
            _loc3_.mask = _loc6_;
            _loc6_ = _loc6_;
            _loc3_.rsv3 = _loc6_;
            _loc6_ = _loc6_;
            _loc3_.rsv2 = _loc6_;
            _loc3_.rsv1 = _loc6_;
            _loc3_.fin = true;
            _loc3_.opcode = 8;
            _loc3_.closeStatus = 1000;
            _loc2_ = new ByteArray();
            _loc3_.mask = true;
            _loc3_.send(_loc2_);
            sendData(_loc2_,true);
            if(param1)
            {
               waitingForServerClose = true;
               closeTimer.stop();
               closeTimer.reset();
               closeTimer.start();
            }
            dispatchClosedEvent();
         }
      }
      
      private function handleCloseTimer(param1:TimerEvent) : void {
         if(waitingForServerClose)
         {
            if(socket.connected)
            {
               socket.close();
            }
         }
      }
      
      private function handleSocketConnect(param1:Event) : void {
         if(debug)
         {
            logger("Socket Connected");
         }
         if(secure)
         {
            if(debug)
            {
               logger("starting SSL/TLS");
            }
         }
         socket.endian = "bigEndian";
         sendHandshake();
      }
      
      private function handleSocketClose(param1:Event) : void {
         if(debug)
         {
            logger("Socket Disconnected");
         }
         dispatchClosedEvent();
      }
      
      private function handleSocketData(param1:ProgressEvent = null) : void {
         var _loc2_:* = null;
         if(_readyState === 0)
         {
            readServerHandshake();
            return;
         }
         while(socket.connected && (currentFrame.addData(socket,fragmentationOpcode,config)) && !fatalError)
         {
            if(currentFrame.protocolError)
            {
               drop(1002,currentFrame.dropReason);
               return;
            }
            if(currentFrame.frameTooLarge)
            {
               drop(1009,currentFrame.dropReason);
               return;
            }
            if(!config.assembleFragments)
            {
               _loc2_ = new WebSocketEvent("frame");
               _loc2_.frame = currentFrame;
               dispatchEvent(_loc2_);
            }
            processFrame(currentFrame);
            currentFrame = new WebSocketFrame();
         }
      }
      
      private function processFrame(param1:WebSocketFrame) : void {
         var _loc9_:* = null;
         var _loc6_:* = 0;
         var _loc4_:* = null;
         var _loc7_:* = 0;
         var _loc2_:* = null;
         var _loc8_:* = 0;
         var _loc5_:* = null;
         var _loc3_:* = null;
         if(param1.rsv1 || (param1.rsv2) || (param1.rsv3))
         {
            drop(1002,"Received frame with reserved bit set without a negotiated extension.");
            return;
         }
      }
      
      private function handleSocketIOError(param1:IOErrorEvent) : void {
         if(debug)
         {
            logger("IO Error: " + param1);
         }
         dispatchEvent(param1);
         dispatchClosedEvent();
      }
      
      private function handleSocketSecurityError(param1:SecurityErrorEvent) : void {
         if(debug)
         {
            logger("Security Error: " + param1);
         }
         dispatchEvent(param1.clone());
         dispatchClosedEvent();
      }
      
      private function sendHandshake() : void {
         var _loc2_:* = null;
         serverHandshakeResponse = "";
         var _loc3_:String = host;
         if(_secure && !(_port === 443) || (!_secure && !(_port === 80)))
         {
            _loc3_ = _loc3_ + (":" + _port.toString());
         }
         var _loc1_:String = "";
         _loc1_ = _loc1_ + ("GET " + resource + " HTTP/1.1\r\n");
         _loc1_ = _loc1_ + ("Host: " + _loc3_ + "\r\n");
         _loc1_ = _loc1_ + "Upgrade: websocket\r\n";
         _loc1_ = _loc1_ + "Connection: Upgrade\r\n";
         _loc1_ = _loc1_ + ("Sec-WebSocket-Key: " + base64nonce + "\r\n");
         if(_origin)
         {
            _loc1_ = _loc1_ + ("Origin: " + _origin + "\r\n");
         }
         _loc1_ = _loc1_ + "Sec-WebSocket-Version: 13\r\n";
         if(_protocols)
         {
            _loc2_ = _protocols.join(", ");
            _loc1_ = _loc1_ + ("Sec-WebSocket-Protocol: " + _loc2_ + "\r\n");
         }
         _loc1_ = _loc1_ + "\r\n";
         if(debug)
         {
            logger(_loc1_);
         }
         socket.writeMultiByte(_loc1_,"us-ascii");
         handshakeTimer.stop();
         handshakeTimer.reset();
         handshakeTimer.start();
      }
      
      private function failHandshake(param1:String = "Unable to complete websocket handshake.") : void {
         _readyState = 2;
         if(socket.connected)
         {
            socket.close();
         }
         handshakeTimer.stop();
         handshakeTimer.reset();
         var _loc2_:WebSocketErrorEvent = new WebSocketErrorEvent("connectionFail");
         _loc2_.text = param1;
         dispatchEvent(_loc2_);
         var _loc3_:WebSocketEvent = new WebSocketEvent("closed");
         dispatchEvent(_loc3_);
      }
      
      private function failConnection(param1:String) : void {
         _readyState = 2;
         if(socket.connected)
         {
            socket.close();
         }
         var _loc2_:WebSocketErrorEvent = new WebSocketErrorEvent("connectionFail");
         _loc2_.text = param1;
         dispatchEvent(_loc2_);
         var _loc3_:WebSocketEvent = new WebSocketEvent("closed");
         dispatchEvent(_loc3_);
      }
      
      private function drop(param1:uint = 1002, param2:String = null) : void {
         var _loc3_:* = null;
         if(!connected)
         {
            return;
         }
         fatalError = true;
         var _loc4_:String = "WebSocket: Dropping Connection. Code: " + param1.toString(10);
         if(param2)
         {
            _loc4_ = _loc4_ + (" - " + param2);
         }
         logger(_loc4_);
         frameQueue = new Vector.<WebSocketFrame>();
         fragmentationSize = 0;
         if(param1 !== 1000)
         {
            _loc3_ = new WebSocketErrorEvent("abnormalClose");
            _loc3_.text = "Close reason: " + param1;
            dispatchEvent(_loc3_);
         }
         sendCloseFrame(param1,param2,true);
         dispatchClosedEvent();
         socket.close();
      }
      
      private function sendCloseFrame(param1:uint = 1000, param2:String = null, param3:Boolean = false) : void {
         var _loc4_:WebSocketFrame = new WebSocketFrame();
         _loc4_.fin = true;
         _loc4_.opcode = 8;
         _loc4_.closeStatus = param1;
         if(param2)
         {
            _loc4_.binaryPayload = new ByteArray();
            _loc4_.binaryPayload.writeUTFBytes(param2);
         }
         sendFrame(_loc4_,param3);
      }
      
      private function readServerHandshake() : void {
         var _loc15_:* = null;
         var _loc3_:* = null;
         var _loc1_:* = null;
         var _loc5_:* = null;
         var _loc2_:* = null;
         var _loc6_:* = false;
         var _loc4_:* = false;
         var _loc14_:* = false;
         var _loc9_:* = false;
         var _loc7_:* = -1;
         while(_loc7_ === -1 && (readHandshakeLine()))
         {
            if(handshakeBytesReceived > 10240)
            {
               failHandshake("Received more than 10240 bytes during handshake.");
               return;
            }
            _loc7_ = serverHandshakeResponse.search(new RegExp("\\r?\\n\\r?\\n"));
         }
         if(_loc7_ === -1)
         {
            return;
         }
         if(debug)
         {
            logger("Server Response Headers:\n" + serverHandshakeResponse);
         }
         serverHandshakeResponse = serverHandshakeResponse.slice(0,_loc7_);
         var _loc16_:Array = serverHandshakeResponse.split(new RegExp("\\r?\\n"));
         var _loc8_:String = _loc16_.shift();
         var _loc12_:Array = _loc8_.match(new RegExp("^(HTTP\\/\\d\\.\\d) (\\d{3}) ?(.*)$","i"));
         if(_loc12_.length === 0)
         {
            failHandshake("Unable to find correctly-formed HTTP status line.");
            return;
         }
         var _loc13_:String = _loc12_[1];
         var _loc17_:int = parseInt(_loc12_[2],10);
         var _loc10_:String = _loc12_[3];
         if(debug)
         {
            logger("HTTP Status Received: " + _loc17_ + " " + _loc10_);
         }
         if(_loc17_ !== 101)
         {
            failHandshake("An HTTP response code other than 101 was received.  Actual Response Code: " + _loc17_ + " " + _loc10_);
            return;
         }
         serverExtensions = [];
         try
         {
            while(_loc16_.length > 0)
            {
               _loc8_ = _loc16_.shift();
               _loc15_ = parseHTTPHeader(_loc8_);
               _loc3_ = _loc15_.name.toLocaleLowerCase();
               _loc1_ = _loc15_.value.toLocaleLowerCase();
               if(_loc3_ === "upgrade" && _loc1_ === "websocket")
               {
                  _loc6_ = true;
               }
               else if(_loc3_ === "connection" && _loc1_ === "upgrade")
               {
                  _loc4_ = true;
               }
               else if(_loc3_ === "sec-websocket-extensions" && _loc15_.value)
               {
                  _loc5_ = _loc15_.value.split(",");
                  serverExtensions = serverExtensions.concat(_loc5_);
               }
               else if(_loc3_ === "sec-websocket-accept")
               {
                  _loc2_ = SHA1.hashToBase64(base64nonce + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
                  if(debug)
                  {
                     logger("Expected Sec-WebSocket-Accept value: " + _loc2_);
                  }
                  if(_loc15_.value === _loc2_)
                  {
                     _loc9_ = true;
                  }
               }
               else
               {
                  if(_loc3_ === "sec-websocket-protocol")
                  {
                     if(_protocols)
                     {
                        _loc19_ = _protocols;
                        for each(_loc11_ in _protocols)
                        {
                           if(_loc11_ == _loc15_.value)
                           {
                              _serverProtocol = _loc11_;
                           }
                        }
                     }
                     continue;
                  }
                  continue;
               }
               
               
               
            }
         }
         catch(e:Error)
         {
            failHandshake("There was an error while parsing the following HTTP Header line:\n" + _loc8_);
            return;
         }
         if(!_loc6_)
         {
            failHandshake("The server response did not include a valid Upgrade: websocket header.");
            return;
         }
         if(!_loc4_)
         {
            failHandshake("The server response did not include a valid Connection: upgrade header.");
            return;
         }
         if(!_loc9_)
         {
            failHandshake("Unable to validate server response for Sec-Websocket-Accept header.");
            return;
         }
         if(_protocols && !_serverProtocol)
         {
            failHandshake("The server can not respond in any of our requested protocols");
            return;
         }
         if(debug)
         {
            logger("Server Extensions: " + serverExtensions.join(" | "));
         }
         handshakeTimer.stop();
         handshakeTimer.reset();
         serverHandshakeResponse = null;
         _readyState = 1;
         currentFrame = new WebSocketFrame();
         frameQueue = new Vector.<WebSocketFrame>();
         dispatchEvent(new WebSocketEvent("open"));
         handleSocketData();
      }
      
      private function handleHandshakeTimer(param1:TimerEvent) : void {
         failHandshake("Timed out waiting for server response.");
      }
      
      private function parseHTTPHeader(param1:String) : Object {
         var _loc2_:Array = param1.split(new RegExp("\\: +"));
         return _loc2_.length === 2?
            {
               "name":_loc2_[0],
               "value":_loc2_[1]
            }:null;
      }
      
      private function readHandshakeLine() : Boolean {
         var _loc1_:* = null;
         while(socket.bytesAvailable)
         {
            _loc1_ = socket.readMultiByte(1,"us-ascii");
            handshakeBytesReceived = handshakeBytesReceived + 1;
            serverHandshakeResponse = serverHandshakeResponse + _loc1_;
            if(_loc1_ == "\n")
            {
               return true;
            }
         }
         return false;
      }
      
      private function dispatchClosedEvent() : void {
         var _loc1_:* = null;
         if(handshakeTimer.running)
         {
            handshakeTimer.stop();
         }
         if(_readyState !== 2)
         {
            _readyState = 2;
            _loc1_ = new WebSocketEvent("closed");
            dispatchEvent(_loc1_);
         }
      }
   }
}
