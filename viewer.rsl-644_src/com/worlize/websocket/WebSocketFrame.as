package com.worlize.websocket
{
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   
   public class WebSocketFrame extends Object
   {
      
      public function WebSocketFrame() {
         super();
      }
      
      private static const NEW_FRAME:int = 0;
      
      private static const WAITING_FOR_16_BIT_LENGTH:int = 1;
      
      private static const WAITING_FOR_64_BIT_LENGTH:int = 2;
      
      private static const WAITING_FOR_PAYLOAD:int = 3;
      
      private static const COMPLETE:int = 4;
      
      private static var _tempMaskBytes:Vector.<uint> = new Vector.<uint>(4);
      
      public var fin:Boolean;
      
      public var rsv1:Boolean;
      
      public var rsv2:Boolean;
      
      public var rsv3:Boolean;
      
      public var opcode:int;
      
      public var mask:Boolean;
      
      public var useNullMask:Boolean;
      
      private var _length:int;
      
      public var binaryPayload:ByteArray;
      
      public var closeStatus:int;
      
      public var protocolError:Boolean = false;
      
      public var frameTooLarge:Boolean = false;
      
      public var dropReason:String;
      
      private var parseState:int = 0;
      
      public function get length() : int {
         return _length;
      }
      
      public function addData(param1:IDataInput, param2:int, param3:WebSocketConfig) : Boolean {
         var _loc6_:* = 0;
         var _loc4_:* = 0;
         var _loc5_:* = 0;
         if(param1.bytesAvailable >= 2)
         {
            if(parseState === 0)
            {
               _loc6_ = param1.readByte();
               _loc4_ = param1.readByte();
               fin = _loc6_ & 128;
               rsv1 = _loc6_ & 64;
               rsv2 = _loc6_ & 32;
               rsv3 = _loc6_ & 16;
               mask = _loc4_ & 128;
               opcode = _loc6_ & 15;
               _length = _loc4_ & 127;
               if(mask)
               {
                  protocolError = true;
                  dropReason = "Received an illegal masked frame from the server.";
                  return true;
               }
               if(opcode > 7)
               {
                  if(_length > 125)
                  {
                     protocolError = true;
                     dropReason = "Illegal control frame larger than 125 bytes.";
                     return true;
                  }
                  if(!fin)
                  {
                     protocolError = true;
                     dropReason = "Received illegal fragmented control message.";
                     return true;
                  }
               }
               if(_length === 126)
               {
                  parseState = 1;
               }
               else if(_length === 127)
               {
                  parseState = 2;
               }
               else
               {
                  parseState = 3;
               }
               
            }
            if(parseState === 1)
            {
               if(param1.bytesAvailable >= 2)
               {
                  _length = param1.readUnsignedShort();
                  parseState = 3;
               }
            }
            else if(parseState === 2)
            {
               if(param1.bytesAvailable >= 8)
               {
                  _loc5_ = param1.readUnsignedInt();
                  if(_loc5_ > 0)
                  {
                     frameTooLarge = true;
                     dropReason = "Unsupported 64-bit length frame received.";
                     return true;
                  }
                  _length = param1.readUnsignedInt();
                  parseState = 3;
               }
            }
            
            if(parseState === 3)
            {
               if(_length > param3.maxReceivedFrameSize)
               {
                  frameTooLarge = true;
                  dropReason = "Received frame size of " + _length + "exceeds maximum accepted frame size of " + param3.maxReceivedFrameSize;
                  return true;
               }
               if(_length === 0)
               {
                  binaryPayload = new ByteArray();
                  parseState = 4;
                  return true;
               }
               if(param1.bytesAvailable >= _length)
               {
                  binaryPayload = new ByteArray();
                  binaryPayload.endian = "bigEndian";
                  param1.readBytes(binaryPayload,0,_length);
                  binaryPayload.position = 0;
                  parseState = 4;
                  return true;
               }
            }
         }
         return false;
      }
      
      private function throwAwayPayload(param1:IDataInput) : void {
         var _loc2_:* = 0;
         if(param1.bytesAvailable >= _length)
         {
            _loc2_ = 0;
            while(_loc2_ < _length)
            {
               param1.readByte();
               _loc2_++;
            }
            parseState = 4;
         }
      }
      
      public function send(param1:IDataOutput) : void {
         var _loc2_:* = 0;
         var _loc3_:* = null;
         var _loc5_:* = 0;
         var _loc7_:* = 0;
         if(this.mask && !this.useNullMask)
         {
            _loc2_ = Math.ceil(Math.random() * 4.294967295E9);
            _tempMaskBytes[0] = _loc2_ >> 24 & 255;
            _tempMaskBytes[1] = _loc2_ >> 16 & 255;
            _tempMaskBytes[2] = _loc2_ >> 8 & 255;
            _tempMaskBytes[3] = _loc2_ & 255;
         }
         var _loc6_:* = 0;
         var _loc4_:* = 0;
         if(fin)
         {
            _loc6_ = _loc6_ | 128;
         }
         if(rsv1)
         {
            _loc6_ = _loc6_ | 64;
         }
         if(rsv2)
         {
            _loc6_ = _loc6_ | 32;
         }
         if(rsv3)
         {
            _loc6_ = _loc6_ | 16;
         }
         if(mask)
         {
            _loc4_ = _loc4_ | 128;
         }
         _loc6_ = _loc6_ | opcode & 15;
         if(opcode === 8)
         {
            _loc3_ = new ByteArray();
            _loc3_.endian = "bigEndian";
            _loc3_.writeShort(closeStatus);
            if(binaryPayload)
            {
               binaryPayload.position = 0;
               _loc3_.writeBytes(binaryPayload);
            }
            _loc3_.position = 0;
            _length = _loc3_.length;
         }
         else if(binaryPayload)
         {
            _loc3_ = binaryPayload;
            _loc3_.endian = "bigEndian";
            _loc3_.position = 0;
            _length = _loc3_.length;
         }
         else
         {
            _loc3_ = new ByteArray();
            _length = 0;
         }
         
         if(opcode >= 8)
         {
            if(_length > 125)
            {
               throw new Error("Illegal control frame longer than 125 bytes");
            }
            else if(!fin)
            {
               throw new Error("Control frames must not be fragmented.");
            }
            
         }
         if(_length <= 125)
         {
            _loc4_ = _loc4_ | _length & 127;
         }
         else if(_length > 125 && _length <= 65535)
         {
            _loc4_ = _loc4_ | 126;
         }
         else if(_length > 65535)
         {
            _loc4_ = _loc4_ | 127;
         }
         
         
         param1.writeByte(_loc6_);
         param1.writeByte(_loc4_);
         if(_length > 125 && _length <= 65535)
         {
            param1.writeShort(_length);
         }
         else if(_length > 65535)
         {
            param1.writeUnsignedInt(0);
            param1.writeUnsignedInt(_length);
         }
         
         if(this.mask)
         {
            if(this.useNullMask)
            {
               param1.writeUnsignedInt(0);
               param1.writeBytes(_loc3_,0,_loc3_.length);
            }
            else
            {
               param1.writeUnsignedInt(_loc2_);
               _loc5_ = 0;
               _loc7_ = _loc3_.bytesAvailable;
               while(_loc7_ >= 4)
               {
                  param1.writeUnsignedInt(_loc3_.readUnsignedInt() ^ _loc2_);
                  _loc7_ = _loc7_ - 4;
               }
               while(_loc7_ > 0)
               {
                  param1.writeByte(_loc3_.readByte() ^ _tempMaskBytes[_loc5_]);
                  _loc5_ = _loc5_ + 1;
                  _loc7_ = _loc7_ - 1;
               }
            }
         }
         else
         {
            param1.writeBytes(_loc3_,0,_loc3_.length);
         }
      }
   }
}
