package com.adotube.events
{
   import flash.events.Event;
   
   public class AdotubeEvent extends Event
   {
      
      {
         trace("[AdotubeLibrary] v1.2");
      }
      
      public function AdotubeEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false) {
         if(param1)
         {
            super(param1,param3,param4);
            this.param = param2;
         }
      }
      
      public static const INIT_VARS:String = "initVars";
      
      public static const LOAD_OML:String = "loadOML";
      
      public static const LAYOUT_SIZE_CHANGED:String = "onLayoutSizeChanged";
      
      public static const PLAYER_DURATION:String = "playerDuration";
      
      public static const PLAYBACK_TIME_CHANGED:String = "PlaybackTimeChanged";
      
      public static const PLAYER_STATE_CHANGED:String = "playerStateChanged";
      
      public static const DESTROY:String = "destroy";
      
      public static const VIDEO_RESIZED:String = "videoResize";
      
      public static const PLAYER_VOLUME_CHANGED:String = "volumeChanged";
      
      public static const OML_LOADED:String = "onOMLLoaded";
      
      public static const REQUEST_PLAYER_PAUSE:String = "requestPlayerPause";
      
      public static const REQUEST_PLAYER_PLAY:String = "requestPlayerPlay";
      
      public static const REQUEST_PLAYER_STREAM:String = "requestPlayerStream";
      
      public static const REQUEST_VIDEO_RESIZE:String = "requestVideoResize";
      
      public static const REQUEST_PLAYER_DISABLE_CONTROLS:String = "requestPlayerDisableControls";
      
      public static const REGISTER_CONTAINER_EVENT:String = "registerContainerEvent";
      
      public static const INTERACTIVE_PLUGIN_COMPLETED:String = "interactivePluginCompleted";
      
      public var param:Object;
      
      override public function clone() : Event {
         return new AdotubeEvent(type,this.param,bubbles,cancelable);
      }
      
      override public function toString() : String {
         return formatToString("AdotubeEvent","param","type","bubbles","cancelable");
      }
      
      public function getParams() : Object {
         return this.param;
      }
   }
}
