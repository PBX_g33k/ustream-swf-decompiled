package 
{
   public class Version extends Object
   {
      
      public function Version() {
         super();
      }
      
      public static const SVN_VERSION:String = "7670";
      
      public static const SVN_DATE:String = "2014/06/03 15:39:11";
      
      public static const BUILD_COMPUTER:String = "THISNOWALL";
      
      public static const BUILD_USER:String = "Tibor";
      
      public static const BUILD_DATE:String = "2014.06.04.  9:43:15,27";
      
      public static const BUILD_NUMBER:String = "1015";
   }
}
