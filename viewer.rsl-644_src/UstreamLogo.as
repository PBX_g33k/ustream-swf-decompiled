package 
{
   import flash.display.MovieClip;
   
   public dynamic class UstreamLogo extends MovieClip
   {
      
      public function UstreamLogo() {
         super();
         addFrameScript(0,frame1);
      }
      
      public var recorded:MovieClip;
      
      public var mobile:MovieClip;
      
      public var ustream:MovieClip;
      
      function frame1() : * {
         recorded.visible = false;
         live.visible = false;
         mobile.visible = false;
      }
      
      public var live:MovieClip;
   }
}
