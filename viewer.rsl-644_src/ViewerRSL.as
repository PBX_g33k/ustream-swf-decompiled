package 
{
   import flash.display.Sprite;
   import flash.events.Event;
   import tv.ustream.viewer.logic.Logic;
   import mx.core.FontAsset;
   import tv.ustream.assets.AudioOnlyAnim;
   import tv.ustream.viewer.logic.ads.VideoAdTimer;
   import tv.ustream.viewer.logic.ads.IAdverts;
   import tv.ustream.irc.Irc;
   import flash.system.Security;
   import tv.ustream.data.*;
   import tv.ustream.data2.*;
   import tv.ustream.data2.renderers.*;
   import tv.ustream.gui2.*;
   import tv.ustream.gui2.icons.*;
   import tv.ustream.localization.*;
   import tv.ustream.tools.*;
   import tv.ustream.tween.*;
   import tv.ustream.viewer.logic.media.*;
   
   public class ViewerRSL extends Sprite
   {
      
      public function ViewerRSL() {
         super();
         Logic.loaderInfo = loaderInfo;
         Irc.loaderInfo = loaderInfo;
         try
         {
            Security.allowDomain("*");
         }
         catch(e:Error)
         {
            trace("[ViewerRSL] allowDomain error: " + e);
         }
         try
         {
            Security.allowInsecureDomain("*");
         }
         catch(e:Error)
         {
            trace("[ViewerRSL] allowInsecureDomain error: " + e);
         }
         if(loaderInfo.parameters.enablejsapi == "true" || loaderInfo.parameters.enablejsapi == "1")
         {
            trace("jsApi enabled");
            Shell.jsApiEnabled = true;
         }
         UncaughtErrorHandler.instance.add(loaderInfo,this.toString());
         if(loaderInfo.bytesLoaded && loaderInfo.bytesLoaded == loaderInfo.bytesTotal)
         {
            onComplete();
         }
         else
         {
            loaderInfo.addEventListener("complete",onComplete);
         }
      }
      
      public static const BRANCH_NAME:String = "STAGE";
      
      private function onComplete(param1:Event = null) : void {
         trace("onComplete");
         if(Shell.jsApiEnabled && !loaderInfo.loader)
         {
            trace("standalone mode");
            if(stage)
            {
               onAddedToStage();
            }
            else
            {
               addEventListener("addedToStage",onAddedToStage);
            }
         }
         else
         {
            trace("no jsapi or loader is present");
         }
      }
      
      private function onAddedToStage(param1:Event = null) : void {
         removeEventListener("addedToStage",onAddedToStage);
         stage.align = "TL";
         stage.scaleMode = "noScale";
         stage.addEventListener("resize",onStageResize);
         Shell.application = "chromelessViewer";
         addChild(Logic.instance.display);
         onStageResize();
      }
      
      private function onStageResize(param1:Event = null) : void {
         Logic.instance.display.width = stage.stageWidth;
         Logic.instance.display.height = stage.stageHeight;
      }
   }
}
